<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */

include("inc/init.inc.php");



head("Nyomdai anyagok");
?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
		<li><a href="?create=0"  class="<? if($_GET[create] == 0 || $_GET[create] == '') echo "current"?>">Nyomdai anyagok</a></li>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<div class='cleaner'></div>
<table>
<tr class='header'>
	<td>#</td>
	<td>Filenév</td>
	<td>Létrehozva</td>
	<td>Méret</td>
</tr>
<?
	$dh = opendir('documents/media');
		while (($file = readdir($dh)) !== false) {
		
		if($file <> '.' && $file <> '..')
        	$files[] = $file;
	}
	closedir($dh);
	
	rsort($files);
	$files = array_reverse($files,true);
	$i = 1;
		foreach($files as $f)
		{	
			$time = date ("Y. m. d. H:i:s",filemtime("documents/media/$f"));
			$imgsize = round(filesize("documents/media/$f")/1024/1024,2);
			echo "<tr><td align='center'>$i</td><td><a href='documents/media/$f' target='_blank'>$f</a></td><td align='center'>$time</td><td align='right' width='70'>$imgsize MB</td></tr>";
			
			$i++;
		}



echo "</table>
<div style='font-size:10px;padding:10px 0 10px 0;text-align:center;'>Az oldalon található nyomdai anyagokat kizárólag a Hotel Outlet Kft. engedélyével lehet felhasználni. Az egyes logók módosítása, újrafelhasználása, közzététele tilos. </div>
</div></div>";

foot();
?>