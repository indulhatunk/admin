<?
header('Content-type: text/html; charset=utf-8');


//error handler function
function customError($errno, $errstr, $errfile, $errline)
{
	if(!preg_match("/".strtoupper(undefined)."/i", $errstr))
		echo "\n\n>>> $errstr / $errline <<<\n\n";
	return true;  
}
//set error handler
set_error_handler("customError");



include("lib/functions.inc.php");
$db = new OCI8DB;
$dbconn = $db->dbconn();

$xml = simplexml_load_file("clean.xml");


echo "File is in memory now \n";
echo "-------------------------\n";


$travelOffers = $xml->traveloffer;

$itemCount = count($travelOffers); 



for($i = 0; $i < $itemCount; $i++) { 
	echo $i." / $itemCount\n";
 	$data = array();
 	$prices = array();
 	//echo "kepfileok:".count($travelOffers[$i]->gallery->kepfilenev)."<br/>";
 	
 	$data[id] = (string)$travelOffers[$i]->id;
 	$data[partnerid] = (int)$travelOffers[$i]->partnerid;
 	$data[name] = (string)str_replace("'","''",$travelOffers[$i]->travel_name);
	//$data[szallas_name] = (string)$travelOffers[$i]->szallas_name;
	$data[country] = (string)$travelOffers[$i]->country;
	$data[region] = (string)$travelOffers[$i]->region;
	$data[destination] = (string)$travelOffers[$i]->destination;
	$data[category_standard] = (string)$travelOffers[$i]->category_standard;
	$data[attribute] = (string)$travelOffers[$i]->attribute;
	$data[inprice] = (string)$travelOffers[$i]->inprice;
	$data[outprice] = (string)$travelOffers[$i]->outprice;

	
	$data[image_num] = count($data[gallery]);
	$data[description] = (string)$travelOffers[$i]->descriptions->description;
	$data[description_hash] = md5($data[description]);
	$data[dcount] = strlen($travelOffers[$i]->descriptions->description);
	
	$z=0;
	foreach($travelOffers[$i]->prices->idopont as $price)
	{
		
		$prices[$z][id] = (int)$price[id];//(string)trim($price->megjegyzes);
		$prices[$z][fromdate] = (string)$price[fromdate];
		$prices[$z][todate] = (string)$price[todate];
		$prices[$z][day_num] = (string)$price[idotartam];
		$prices[$z][price] = (int)$price[price];
		$prices[$z][bed_number] = (int)$price[agyszam];
		
		$prices[$z][foglalasi_dij] = (int)$price[foglalasi_dij];
        $prices[$z][transzfer_dij] =  (int)$price[transzfer_dij];
        $prices[$z][szervidij] =  (int)$price[szervidij];
        $prices[$z][vizum_dij] =  (int)$price[vizum_dij];
        $prices[$z][repteri_illetek] =  (int)$price[repteri_illetek];
        $prices[$z][storno_biztositas] =  (int)$price[storno_biztositas];
        $prices[$z][bpp] =  (int)$price[bpp];
        $prices[$z][kerozin_potdij] =  (int)$price[kerozin_potdij];
        $prices[$z][egyeni_idopont] =  (int)$price[egyeni_idopont];
         $prices[$z][description] =  (string)trim($price->megjegyzes);            
	
		$z++;
	}
	
	//$tourArr = parseQuery("SELECT * FROM TOUR WHERE NAME = '$data[name]'");
	$tourArr = parseQuery("SELECT ID FROM TOUR WHERE ALEPH_ID = '$data[id]'");

	$tourArr = $tourArr[0];
	if($tourArr[ID] <> '')
	{
 		/*
 		*/
 	//	$stid = oci_parse($dbconn,"UPDATE TOUR SET DESCRIPTION = :desc_bv,	DESCRIPTION_HASH = '$data[description_hash]'
 	//	WHERE ALEPH_ID = '$data[id]' AND DESCRIPTION_HASH <> '$data[description_hash]'");
	//	oci_bind_by_name($stid, ":desc_bv", $data[description]);
		
	//	 if(oci_execute($stid) == false) { 
	//	 	echo "ERROR exception: $data[name]\n";
	//	 }
	//	else {
	//		oci_commit($dbconn);
			echo "updated $data[name] $tourArr[ID]\n";
			echo "arak: ".count($prices)."\n";
			foreach($prices as $prc)
			{
				//echo "prices\n";
				$priceCheckSt = oci_parse($dbconn,"SELECT ID,PRICE,OFFER_ID FROM PRICES WHERE ALEPH_ID = '$prc[id]'");
				oci_execute($priceCheckSt);
				$priceArr = oci_fetch_assoc($priceCheckSt);
				
				if($priceArr[ID] <> '' && $priceArr[PRICE] == $prc[price])
				{
				//	echo "do nothing $priceArr[OFFER_ID]\n";
				}
				elseif($priceArr[ID] <> '' && $priceArr[PRICE] <> $prc[price])
				{
				//	echo "update price $priceArr[OFFER_ID]\n";
				}
				else
				{
				
				$priceInsertQuery = "INSERT INTO PRICES (
 					ID,
 					OFFER_ID, 
 					FROM_DATE, 
 					TO_DATE, 
 					PRICE, 
 					ADDITIONAL_PRICE, 
 					BED_NUMBER, 
 					BPP, 
 					DURATION, 
 					KEROSENE_FEE, 
 					AIRPORT_FEE, 
 					STORNO_INSURANCE, 
 					SERVICE_FEE,
 					TRANSFER_FEE,
 					VISA_FEE,
 					DESCRIPTION,
 					DAY_NUM,
 					ALEPH_ID
 				) 
 				VALUES
 				(
 					PRICES_SEQ.nextval, 
 					$tourArr[ID], 
 					trunc(to_date('$prc[fromdate]','yyyy-mm-dd')), 
 					trunc(to_date('$prc[todate]','yyyy-mm-dd')), 
					'$prc[price]',
 					'',
 					'$prc[bed_number]',
 					'$prc[bpp]' ,
 					'$prc[day_num]',
 					'$prc[kerozin_potdij]',
 					'$prc[repteri_illetek]',
 					'$prc[storno_biztositas]',
 					'$prc[szervizdij]',
 					'$prc[transzfer_dij]',
 					'$prc[vizum_dij]',
 					'$prc[description]',
 					'$prc[day_num]',
 					'$prc[id]'
 				);\n";
				
					//echo "insert price\n";
					
				
				$priceSt = oci_parse($dbconn,"INSERT INTO PRICES (
 					ID,
 					OFFER_ID, 
 					FROM_DATE, 
 					TO_DATE, 
 					PRICE, 
 					ADDITIONAL_PRICE, 
 					BED_NUMBER, 
 					BPP, 
 					DURATION, 
 					KEROSENE_FEE, 
 					AIRPORT_FEE, 
 					STORNO_INSURANCE, 
 					SERVICE_FEE,
 					TRANSFER_FEE,
 					VISA_FEE,
 					DESCRIPTION,
 					DAY_NUM,
 					ALEPH_ID
 				) 
 				VALUES
 				(
 					PRICES_SEQ.nextval, 
 					$tourArr[ID], 
 					trunc(to_date('$prc[fromdate]','yyyy-mm-dd')), 
 					trunc(to_date('$prc[todate]','yyyy-mm-dd')), 
					'$prc[price]',
 					'',
 					'$prc[bed_number]',
 					'$prc[bpp]' ,
 					'$prc[day_num]',
 					'$prc[kerozin_potdij]',
 					'$prc[repteri_illetek]',
 					'$prc[storno_biztositas]',
 					'$prc[szervizdij]',
 					'$prc[transzfer_dij]',
 					'$prc[vizum_dij]',
 					'$prc[description]',
 					'$prc[day_num]',
 					'$prc[id]'
 				)");
				oci_execute($priceSt);
				oci_commit($dbconn);
				/*
				$file = "priceInsert.sql";
			$fh = fopen($file, 'a') or die("can't open file");
			fwrite($fh, $priceInsertQuery);
			fclose($fh);
				
				}*/
				//print_r($prc);
			//	echo "-------\n";
			echo "insert price $priceArr[OFFER_ID]\n";
			
 
			}
			
		}
 		
 		/*
 		*/
 	}	
 	else
 	{
 		echo "insert $data[name]\n";
 		
 		$cityQuery = parseQuery("SELECT * FROM CITY WHERE ALEPH_ID = '$data[destination]'");
 		$cityQuery = $cityQuery[0]; 
 		
 		$partnerQuery = parseQuery("SELECT * FROM PARTNER WHERE ALEPH_ID = '$data[partnerid]'");
 		$partnerQuery = $partnerQuery[0]; 

 		
 		//print_r($partnerQuery);
 		/*insert sql query */
 		$stid = oci_parse($dbconn,"INSERT INTO TOUR (
 			ID,
 			PARTNER_ID, 
 			NAME, 
 			COUNTRY_ID, 
 			REGION_ID, 
 			CITY_ID, 
 			DESCRIPTION, 
 			CATEGORY, 
 			IMAGE_NUM, 
 			INPRICE, 
 			OUTPRICE, 
 			ALEPH_ID, 
 			DESCRIPTION_HASH
 		) 
 		VALUES
 		(
 			TOUR_SEQ.nextval, 
 			$partnerQuery[ID], 
 			'$data[name]', 
 			'$cityQuery[COUNTRY_ID]',
 			'$cityQuery[REGION_ID]',
 			'$cityQuery[ID]',
 			:desc_bv,
 			'$data[category]',
 			'$data[image_num]',
 			'$data[inprice]',
 			'$data[outprice]',
 			'$data[id]',
 			'$data[description_hash]'
 		)");
		oci_bind_by_name($stid, ":desc_bv", $data[description]);
		
		 if(oci_execute($stid) == false || $partnerQuery[ID] == '') { 
		 	echo "ERROR exception: $data[name]";
		 }
		else {
			oci_commit($dbconn);
			echo "inserted $data[name]\n";
			
			foreach($prices as $prc)
			{
				$priceSt = oci_parse($dbconn,"INSERT INTO PRICES (
 					ID,
 					OFFER_ID, 
 					FROM_DATE, 
 					TO_DATE, 
 					PRICE, 
 					ADDITIONAL_PRICE, 
 					BED_NUMBER, 
 					BPP, 
 					DURATION, 
 					KEROSENE_FEE, 
 					AIRPORT_FEE, 
 					STORNO_INSURANCE, 
 					SERVICE_FEE,
 					TRANSFER_FEE,
 					VISA_FEE,
 					DESCRIPTION,
 					DAY_NUM,
 					ALEPH_ID
 				) 
 				VALUES
 				(
 					PRICES_SEQ.nextval, 
 					TOUR_SEQ.currval, 
 					trunc(to_date('$prc[fromdate]','yyyy-mm-dd')), 
 					trunc(to_date('$prc[todate]','yyyy-mm-dd')), 
					'$prc[price]',
 					'',
 					'$prc[bed_number]',
 					'$prc[bpp]' ,
 					'$prc[day_num]',
 					'$prc[kerozin_potdij]',
 					'$prc[repteri_illetek]',
 					'$prc[storno_biztositas]',
 					'$prc[szervizdij]',
 					'$prc[transzfer_dij]',
 					'$prc[vizum_dij]',
 					'$prc[description]',
 					'$prc[day_num]',
 					'$prc[id]'
 				)");
				oci_execute($priceSt);
				oci_commit($dbconn);

				//print_r($prc);
			//	echo "-------\n";
			}
			
		}
 	}
 	
 	$picData = parseQuery("SELECT PARTNER_ID,ID FROM TOUR WHERE ALEPH_ID = '$data[id]'");
	$picData = $picData[0];
	$pictureCount = 1;
	
	if(!empty($picData))
	{
	$pictureScript = '';
	$pictureScript.="mkdir -p tour/$picData[PARTNER_ID]/$picData[ID]/orig/;\n";
	foreach($travelOffers[$i]->gallery->kepfilenev as $images)
	{
		if($pictureCount > 0)
			$pictureCount = "0$pictureCount";
		else
			$pictureCount = $pictureCount;
		
		
		$pictureScript.= "wget '". $travelOffers[$i]->gallery[url]."".$images."' -O tour/$picData[PARTNER_ID]/$picData[ID]/orig/$pictureCount.jpg;\n";
		$data[gallery][] = $travelOffers[$i]->gallery[url]."".$images;
		
		$pictureCount++;
	}
	
	$file = "pictureScript.sh";
	$fh = fopen($file, 'a') or die("can't open file");
	fwrite($fh, $pictureScript);
	fclose($fh);
 	}

} 

 print_r($notExists);

$xml = null;

echo "<<< DONE >>>\n";
echo "</pre>";
die;
?>