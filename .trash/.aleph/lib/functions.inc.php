<?
class OCI8DB {

function dbconn() {
	$conn = oci_connect('coredb', 'coredb', 'static.pihenni.hu/xe','AL32UTF8');
	if (!$conn) {
		trigger_error("Could not connect to database", E_USER_ERROR);
		return false;
	}
	return $conn;
}

}


function clean_url($name) {
		$special = array(
		'á',
		'í',
		'ű', 
		'ő',
		'ü',
		'ö',
		'ú',
		'ó',
		'é',
		'Á',
		'Í',
		'Ű', 
		'Ő',
		'Ü',
		'Ö',
		'Ú',
		'Ó',
		'É',
		'/',
		'!',
		'&',
		'*',
		'=',
		'?',
		'(',
		')',
		'%',
		'.',
		'"',
		'\'',
		',',
		';',
		'`',
		' ',
		'+',
		':',
		'\'',
		'bull',
		'#',
		']',
		'[',
		'û',
		'Õ',
		); 
		
		$to = array(
		'a',
		'i',
		'u', 
		'o',
		'u',
		'o',
		'u',
		'o',
		'e',
		'A',
		'I',
		'U', 
		'O',
		'U',
		'O',
		'U',
		'O',
		'E',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-bull',
		'-',
		'-',
		'-',
		'-',
		'-',
		);
		$special = str_replace($special,$to,trim($name));
		$special = str_replace("-----","-",$special);
		$special = str_replace("----","-",$special);
		$special = str_replace("---","-",$special);
		$special = str_replace("--","-",$special);
		
		/*$special = str_replace("++","+",$special);
		$special = str_replace("+-+","+",$special);
		$special = str_replace("","+",$special);
		*/
		$last = substr($special, -1);
		if($last == "-")
			$special = substr($special, 0, -1);
			
	return strtolower($special);
}

function parseQuery($query) {
	global $dbconn;
	$st = oci_parse($dbconn, $query);
	oci_execute($st, OCI_DEFAULT);
	
	//echo "$query<hr/>";

	$result = array();
	while($arr = oci_fetch_assoc($st)) 
	{
		$result[] = $arr;
			
	}
	//pre($result);
			
	if(empty($result) == TRUE)
	{
		//echo "empty <hr/>";
		$result = array();
	}
		
	return $result;
}

?>