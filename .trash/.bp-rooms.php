<?
error_reporting(0);
header('Content-type: text/html; charset=utf-8');
include("inc/config.inc.php");
include("inc/functions.inc.php");
include("inc/mysql.class.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();


$db = new OCI8DB;
$dbconn = $db->dbconn();

$memcached = new Memcache;
$memcached->connect('127.0.0.1', 11211) or die ("Could not connect"); //connect to memcached server

$add = $_GET[add];
$edit = $_GET[edit];
$editid = $_REQUEST[editid];

if($_POST[NAME] <> '' && $editid == '')
{


$data = $_POST;



$stid = oci_parse($dbconn,"INSERT INTO ACCOMODATION_ROOM (
 			ID,
 			NAME, 
 			NAME_EN, 
 			NAME_FR,
 			NAME_DE, 
 			PARTNER_ID
 		) 
 		VALUES
 		(
 			OFFERS_SEQ.nextval, 
 			
 			'$data[NAME]', 
 			'$data[NAME_EN]', 
 			'$data[NAME_FR]', 
 			'$data[NAME_DE]', 
 			'$data[PARTNER_ID]'
 			
 		)");
		
		 if(oci_execute($stid) == false) { 
		 	echo "ERROR exception: $data[name]";
		 	print_r(oci_error($stid));
		 }
		else {
			
			oci_commit($dbconn);
			$msg = "Sikeresen létrehozta a szobát!";
			
			//print_r($data);
		}	

}
if($_POST[NAME] <> '' && $editid > 0)
{

$partners = "SELECT * FROM PARTNER WHERE ID = $_POST[PARTNER_ID]";
$partner = cachedSQL($partners);	

$data = $_POST;

$data[ADDRESS] = $partner[0][ADDRESS];
$data[CATEGORY] = $partner[0][CATEGORY];


$data[DESCRIPTION] = $data[DESCRIPTION]."";
$data[DESCRIPTION_EN] = $data[DESCRIPTION_EN]."";
$data[DESCRIPTION_DE] = $data[DESCRIPTION_DE]."";
$data[DESCRIPTION_FR] = $data[DESCRIPTION_FR]."";

$stid = oci_parse($dbconn,"UPDATE ACCOMODATION_ROOM SET
 			
 			NAME = '$data[NAME]', 
 			NAME_EN = '$data[NAME_EN]',  
 			NAME_FR = '$data[NAME_FR]', 
 			NAME_DE = '$data[NAME_DE]'
 			WHERE ID = '$editid'");
		
		
		 if(oci_execute($stid) == false) { 
		 	echo "ERROR exception: $data[name]";
		 	print_r(oci_error($stid));
		 }
		else {
			
			oci_commit($dbconn);
			$msg = "Sikeresen frissítette az szobát!";
			
			//print_r($data);
		}	
		
}
//address
//area id 
if($edit > 0)
{
	$edit = "SELECT * FROM ACCOMODATION_ROOM WHERE ID = $edit";
	$edit = cachedSQL($edit,3);	
	$edit = $edit[0];
}

userlogin();
head();

echo message($msg);
?>



<h1>Budapesti szállodai szobák kezelése</h1>

<center>
	<a href="?add=1&partner=<?=$_GET[partner]?>" class="yellow button" style='color:black'>Új szoba létrehozása</a>
</center>
<div class='cleaner'></div>

<? if($add == 1){?>
<fieldset style='width:750px;'>
	<form method="post" id="sform" action="bp-rooms.php?partner=<?=$_GET[partner]?>">
		<input type='hidden' name='PARTNER_ID' value='<?=$_GET[partner]?>'/>
		<?
		if($edit[ID] > 0)
		{
			echo "<input type='hidden' name='editid' value='$edit[ID]' />";
		}
		
	$partners = "SELECT * FROM PARTNER WHERE ID = $_GET[partner]";
	$partners = cachedSQL($partners,2);	
		

		?>
		
	<ul>
		<li><label>Partner neve:</label><label><?=str_replace("_",'',$partners[0][NAME])?></label>
		</li>
		<li><label>Szoba MAGYAR neve:</label><input type='text' name='NAME' value='<?=$edit[NAME]?>' style='width:200px;'/></li>
		<li><label>Szoba ANGOL neve:</label><input type='text' name='NAME_EN' value='<?=$edit[NAME_EN]?>' style='width:200px;'/></li>
		<li><label>Szoba NÉMET neve:</label><input type='text' name='NAME_DE' value='<?=$edit[NAME_DE]?>' style='width:200px;'/></li>
		<li><label>Szoba FRANCIA neve:</label><input type='text' name='NAME_FR' value='<?=$edit[NAME_FR]?>' style='width:200px;'/></li>

		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</form>
</fieldset>
<hr/>
<? } ?>
<center>
<table>
<?
	$partners = "SELECT * FROM ACCOMODATION_ROOM  WHERE PARTNER_ID = $_GET[partner]";
	$partners = cachedSQL($partners,2);	
			
	foreach($partners as $partner)
	{
		echo "<tr>";
			echo "<td>$partner[NAME]</td>";
			echo "<td>$partner[NAME_EN]</td>";
			echo "<td>$partner[NAME_DE]</td>";
			echo "<td>$partner[NAME_FR]</td>";
			echo "<td><a href='bp-rooms.php?edit=$partner[ID]&add=1&partner=$partner[PARTNER_ID]'>[szerkeszt]</a></td>";
		echo "</tr>";
	}	
?>
</table>
</center>
<?
foot();
?>