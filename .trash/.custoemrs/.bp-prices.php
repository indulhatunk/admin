<?
error_reporting(0);
header('Content-type: text/html; charset=utf-8');
include("inc/config.inc.php");
include("inc/functions.inc.php");
include("inc/mysql.class.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();


$db = new OCI8DB;
$dbconn = $db->dbconn();

$memcached = new Memcache;
$memcached->connect('127.0.0.1', 11211) or die ("Could not connect"); //connect to memcached server

$add = $_GET[add];
$edit = $_GET[edit];
$editid = $_REQUEST[editid];


if($_POST[PRICE] > 0 && $editid == '')
{


$data = $_POST;



$stid = oci_parse($dbconn,"INSERT INTO PRICES (
 			ID,
 			OFFER_ID, 
 			ROOM_NAME,
 			ROOM_NAME_EN,
 			ROOM_NAME_DE,
 			ROOM_NAME_FR,
 			PRICE,
 			DESCRIPTION, 
 			DESCRIPTION_EN,
 			DESCRIPTION_FR,
 			DESCRIPTION_DE,
 			FROM_DATE,
 			TO_DATE
 		) 
 		VALUES
 		(
 			PRICES_SEQ.nextval, 
 			
 			'$data[OFFER_ID]', 
 			'$data[ROOM_NAME]', 
 			'$data[ROOM_NAME_EN]', 
 			'$data[ROOM_NAME_DE]', 
 			'$data[ROOM_NAME_FR]', 
 			'$data[PRICE]', 
 			'$data[DESCRIPTION]', 
 			'$data[DESCRIPTION_EN]', 
 			'$data[DESCRIPTION_FR]', 
 			'$data[DESCRIPTION_DE]',
 			'1-JAN-2001',
 			'31-DEC-2020'
 			
 		)");
		
		 if(oci_execute($stid) == false) { 
		 	echo "ERROR exception: $data[name]";
		 	print_r(oci_error($stid));
		 }
		else {
			
			oci_commit($dbconn);
			$msg = "Sikeresen létrehozta az árat!";
			
			//print_r($data);
		}	

}
if($_POST[PRICE] <> '' && $editid > 0)
{


$data = $_POST;



$stid = oci_parse($dbconn,"UPDATE PRICES SET
 			
 			ROOM_NAME = '$data[ROOM_NAME]', 
 			ROOM_NAME_EN = '$data[ROOM_NAME_EN]', 
 			ROOM_NAME_DE = '$data[ROOM_NAME_DE]', 
 			ROOM_NAME_FR = '$data[ROOM_NAME_FR]', 
 			PRICE = '$data[PRICE]', 
 			DESCRIPTION = '$data[DESCRIPTION]', 
 			DESCRIPTION_EN = '$data[DESCRIPTION_EN]', 
 			DESCRIPTION_FR = '$data[DESCRIPTION_FR]', 
 			DESCRIPTION_DE = '$data[DESCRIPTION_DE]'
 			WHERE ID = '$editid'");
		
		
		 if(oci_execute($stid) == false) { 
		 	echo "ERROR exception: $data[name]";
		 	print_r(oci_error($stid));
		 }
		else {
			
			oci_commit($dbconn);
			$msg = "Sikeresen frissítette az szobát!";
			
			//print_r($data);
		}	
		
}
//address
//area id 
if($edit <> '')
{
	$edit = "SELECT * FROM PRICES WHERE ID = $edit";
	$edit = cachedSQL($edit,3);	
	$edit = $edit[0];

}

userlogin();
head();

echo message($msg);
?>



<h1>Budapesti szállodai szobárak kezelése</h1>

<center>
	<a href="?add=1&partner=<?=$_GET[partner]?>&offer=<?=$_GET[offer]?>" class="yellow button" style='color:black'>Új ár létrehozása</a>
</center>
<div class='cleaner'></div>

<? if($add == 1){?>
<fieldset style='width:750px;'>
	<form method="post" id="sform" action="bp-prices.php?partner=<?=$_GET[partner]?>&offer=<?=$_GET[offer]?>">
		<input type='hidden' name='OFFER_ID' value='<?=$_GET[offer]?>'/>
		<?
		if($edit[ID] > 0)
		{
			echo "<input type='hidden' name='editid' value='$edit[ID]' />";
		}
		
	$partners = "SELECT * FROM PARTNER WHERE ID = $_GET[partner]";
	$partners = cachedSQL($partners,2);	
		

		?>
		
	<ul>
		<li><label>Partner neve:</label><label><?=str_replace("_",'',$partners[0][NAME])?></label></li>
		
		<li><label>Ár:</label><input type='text' name='PRICE' value='<?=$edit[PRICE]?>' style='width:200px;'/></li>

		<li><label>Szoba neve MAGYARUL:</label><input type='text' name='ROOM_NAME' value='<?=$edit[ROOM_NAME]?>' style='width:200px;'/></li>
		<li><label>Szoba neve ANGOLUL:</label><input type='text' name='ROOM_NAME_EN' value='<?=$edit[ROOM_NAME_EN]?>' style='width:200px;'/></li>
		<li><label>Szoba neve NÉMETÜL:</label><input type='text' name='ROOM_NAME_DE' value='<?=$edit[ROOM_NAME_DE]?>' style='width:200px;'/></li>
		<li><label>Szoba neve FRANCIÁUL:</label><input type='text' name='ROOM_NAME_FR' value='<?=$edit[ROOM_NAME_FR]?>' style='width:200px;'/></li>

		<li><label>MAGYAR megnevezés:</label><input type='text' name='DESCRIPTION' value='<?=$edit[DESCRIPTION]?>' style='width:200px;'/></li>
		<li><label>ANGOL megnevezés:</label><input type='text' name='DESCRIPTION_EN' value='<?=$edit[DESCRIPTION_EN]?>' style='width:200px;'/></li>
		<li><label>NÉMET megnevezés:</label><input type='text' name='DESCRIPTION_DE' value='<?=$edit[DESCRIPTION_DE]?>' style='width:200px;'/></li>
		<li><label>FRANCIA megnevezés:</label><input type='text' name='DESCRIPTION_FR' value='<?=$edit[DESCRIPTION_FR]?>' style='width:200px;'/></li>

		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</form>
</fieldset>
<hr/>
<? } ?>
<center>
<table>
<?
	$partners = "SELECT * FROM PRICES  WHERE OFFER_ID = '$_GET[offer]'";
	
	$partners = cachedSQL($partners,2);	
			
	foreach($partners as $partner)
	{
		
		echo "<tr>";
			echo "<td>$partner[ROOM_NAME]</td>";
			echo "<td>".formatPrice($partner[PRICE])."</td>";
			echo "<td>$partner[DESCRIPTION]</td>";
			echo "<td>$partner[DESCRIPTION_EN]</td>";
			echo "<td>$partner[DESCRIPTION_DE]</td>";
			echo "<td>$partner[DESCRIPTION_FR]</td>";
			echo "<td><a href='bp-prices.php?edit=$partner[ID]&add=1&partner=$_GET[partner]&offer=$_GET[offer]'>[szerkeszt]</a></td>";
		echo "</tr>";
	}	
?>
</table>
</center>
<?
foot();
?>