<?
error_reporting(0);
header('Content-type: text/html; charset=utf-8');
include("inc/config.inc.php");
include("inc/functions.inc.php");
include("inc/mysql.class.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();


$db = new OCI8DB;
$dbconn = $db->dbconn();

$memcached = new Memcache;
$memcached->connect('127.0.0.1', 11211) or die ("Could not connect"); //connect to memcached server

$add = $_GET[add];
$edit = (int)$_GET[edit];
$editid = $_REQUEST[editid];

if($_POST[NAME] <> '' && $editid == '')
{


$partners = "SELECT * FROM PARTNER WHERE ID = $_POST[PARTNER_ID]";
$partner = cachedSQL($partners);	

$data = $_POST;

$data[ADDRESS] = $partner[0][ADDRESS];
$data[CATEGORY] = $partner[0][CATEGORY];


$data[DESCRIPTION] = $data[DESCRIPTION]."";
$data[DESCRIPTION_EN] = $data[DESCRIPTION_EN]."";
$data[DESCRIPTION_DE] = $data[DESCRIPTION_DE]."";
$data[DESCRIPTION_FR] = $data[DESCRIPTION_FR]."";

$stid = oci_parse($dbconn,"INSERT INTO ACCOMODATION (
 			ID,
 			PARTNER_ID, 
 			NAME, 
 			ADDRESS,
 			COUNTRY_ID, 
 			REGION_ID, 
 			CITY_ID, 
 			CATEGORY,
 			PACKAGE,
 			EXPIRATION_DATE,
 			DESCRIPTION,
 			DESCRIPTION_EN,
 			DESCRIPTION_DE,
 			DESCRIPTION_FR
 		) 
 		VALUES
 		(
 			OFFERS_SEQ.nextval, 
 			$data[PARTNER_ID], 
 			'$data[NAME]', 
 			'$data[ADDRESS]',
 			'$data[COUNTRY_ID]',
 			'$data[REGION_ID]',
 			'$data[CITY_ID]',
 			'$data[CATEGORY]',
 			1,
 			'31-DEC-2021',
 			:desc_bv,
 			:desc_bv_en,
 			:desc_bv_de,
 			:desc_bv_fr 
 			
 		)");
		oci_bind_by_name($stid, ":desc_bv", $data[DESCRIPTION]);
		oci_bind_by_name($stid, ":desc_bv_en", $data[DESCRIPTION_EN]);
		oci_bind_by_name($stid, ":desc_bv_de", $data[DESCRIPTION_DE]);
		oci_bind_by_name($stid, ":desc_bv_fr", $data[DESCRIPTION_FR]);

		
		 if(oci_execute($stid) == false) { 
		 	echo "ERROR exception: $data[name]";
		 	print_r(oci_error($stid));
		 }
		else {
			
			oci_commit($dbconn);
			$msg = "Sikeresen létrehozta az ajánlatot!";
			
			//print_r($data);
		}	

}
if($_POST[NAME] <> '' && $editid > 0)
{

$partners = "SELECT * FROM PARTNER WHERE ID = $_POST[PARTNER_ID]";
$partner = cachedSQL($partners);	

$data = $_POST;

$data[ADDRESS] = $partner[0][ADDRESS];
$data[CATEGORY] = $partner[0][CATEGORY];


$data[DESCRIPTION] = $data[DESCRIPTION]."";
$data[DESCRIPTION_EN] = $data[DESCRIPTION_EN]."";
$data[DESCRIPTION_DE] = $data[DESCRIPTION_DE]."";
$data[DESCRIPTION_FR] = $data[DESCRIPTION_FR]."";

$stid = oci_parse($dbconn,"UPDATE ACCOMODATION SET
 			
 			PARTNER_ID = $data[PARTNER_ID], 
 			NAME = '$data[NAME]', 
 			ADDRESS = '$data[ADDRESS]',
 			COUNTRY_ID = '$data[COUNTRY_ID]',
 			REGION_ID = '$data[REGION_ID]',
 			CITY_ID = '$data[CITY_ID]',
 			CATEGORY = '$data[CATEGORY]',
 			EXPIRATION_DATE  = '31-DEC-2021',
 			DESCRIPTION  = :desc_bv,
 			DESCRIPTION_EN  = :desc_bv_en,
 			DESCRIPTION_DE  = :desc_bv_de,
 			DESCRIPTION_FR  = :desc_bv_fr,
 			PACKAGE = 1
 			
 			WHERE ID = '$editid'");
		oci_bind_by_name($stid, ":desc_bv", $data[DESCRIPTION]);
		oci_bind_by_name($stid, ":desc_bv_en", $data[DESCRIPTION_EN]);
		oci_bind_by_name($stid, ":desc_bv_de", $data[DESCRIPTION_DE]);
		oci_bind_by_name($stid, ":desc_bv_fr", $data[DESCRIPTION_FR]);

		
		 if(oci_execute($stid) == false) { 
		 	echo "ERROR exception: $data[name]";
		 	print_r(oci_error($stid));
		 }
		else {
			
			oci_commit($dbconn);
			$msg = "Sikeresen frissítette az ajánlatot!";
			
			//print_r($data);
		}	
		
}
//address
//area id 
if($edit > 0)
{
	$edit = "SELECT * FROM ACCOMODATION WHERE ID = $edit";
	$edit = cachedSQL($edit);	
	$edit = $edit[0];
}

userlogin();
head();

echo message($msg);
?>

<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
	
	
	
	/*tabs */
	$().ready(function() {
	
	$(".tab_content").hide(); //Hide all content
			$(".tabs a:first").addClass("active").show(); //Activate first tab
			$(".tab_content:first").show(); //Show first tab content
			//On Click Event
			$(".tabs a").click(function() {
				
				$(".tabs a").removeClass("active"); //Remove any "active" class
				$(this).addClass("active"); //Add "active" class to selected tab
				$(".tab_content").hide(); //Hide all tab content

				var activeTab = $(this).attr("href"); //Find the href attribute value to identify the active tab + content
				$(activeTab).fadeIn(); //Fade in the active ID content
				return false;
			});
			$(".tabs a").click(function() {
				return false;
			});
		});	
			

</script>



<h1>Budapesti szállodák kezelése</h1>

<center>
	<a href="?add=1" class="yellow button" style='color:black'>Új ajánlat készítése</a>
</center>
<div class='cleaner'></div>

<? if($add == 1){?>
<fieldset style='width:750px;'>
	<form method="post" id="sform" action="bp.php">
		<input type='hidden' name='CITY_ID' value='2525'/>
		<input type='hidden' name='REGION_ID' value='2519'/>
		<input type='hidden' name='COUNTRY_ID' value='125' />
		<input type='hidden' name='PACKAGE' value='0' />
		<?
			if($edit[ID] > 0)
			{
		 		echo "<input type='hidden' name='editid' value='$edit[ID]' />";
			}
		?>
		
	<ul>
		<li><label>Partner neve:</label>
		<select name="PARTNER_ID" style='width:200px;'>
				<?
				$partners = "SELECT * FROM PARTNER WHERE COUNTRY_ID = 125 AND CITY_ID = 2525 AND NAME LIKE '%\_%' escape '\' ORDER BY NAME ASC";
				$partners = cachedSQL($partners);	
				
				foreach($partners as $partner)
				{
					if($edit[PARTNER_ID] == $partner[ID])
						$selected = 'selected';
					else
						$selected  = '';
					echo "<option value='$partner[ID]' $selected>".str_replace("_",'',$partner[NAME])."</option>";
				}			
				?>
			</select>
		</li>
		<li><label>Ajánlat neve:</label><input type='text' name='NAME' value='<?=$edit[NAME]?>' style='width:200px;'/></li>
		<li><label>Aktív</label>
		<select name="AKTIV" style='width:200px;'>
				<option value='1'>Igen</option>
				<option value='0'>Nem</option>
			</select>
		</li>
		
		<li>
			<div class='tabs'>
				<a href="#hun">Magyar leírás</a>	
				<a href="#eng">Angol leírás</a>
				<a href="#ger">Német leírás</a>
				<a href="#fre">Francia leírás</a>
			</div>
			<div class='tab_body'>
				<div id="hun" class='tab_content'>
					<textarea name="DESCRIPTION" rows="20" cols="20" style="width: 740px" class="tinymce">
					<? if($edit[DESCRIPTION] <> '') echo $edit[DESCRIPTION]->load();?>
					</textarea>
				</div>
				<div id="eng" class='tab_content'>
					<textarea name="DESCRIPTION_EN" rows="20" cols="20" style="width: 740px" class="tinymce">
					<? if($edit[DESCRIPTION_EN] <> '') echo  $edit[DESCRIPTION_EN]->load();?>
					</textarea>
				</div>
				<div id="ger" class='tab_content'>
					<textarea name="DESCRIPTION_DE" rows="20" cols="20" style="width: 740px" class="tinymce">
					<? if($edit[DESCRIPTION_DE] <> '') echo  $edit[DESCRIPTION_DE]->load();?>
					</textarea>
				</div>
				<div id="fre" class='tab_content'>
					<textarea name="DESCRIPTION_FR" rows="20" cols="20" style="width: 740px" class="tinymce">
					<? if($edit[DESCRIPTION_FR] <> '') echo  $edit[DESCRIPTION_FR]->load();?>
					</textarea>
				</div>
				
			</div>	
			
		</li>
		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</form>
</fieldset>
<hr/>
<? } ?>
<center>
<table>
<?
	$partners = "SELECT * FROM PARTNER WHERE COUNTRY_ID = 125 AND CITY_ID = 2525 AND NAME LIKE '%\_%' escape '\' ORDER BY NAME ASC";
	$partners = cachedSQL($partners);	
			
	foreach($partners as $partner)
	{
		echo "<tr class='header'>";
			echo "<td>".str_replace("_",'',$partner[NAME])."</td>";
			echo "<td colspan=2></td>";
		echo "</tr>";
		
		$offers = "SELECT * FROM ACCOMODATION WHERE PARTNER_ID = $partner[ID] AND AKTIV = 1 AND (PACKAGE = 1 OR PACKAGE IS NOT NULL)";
		$offers = cachedSQL($offers,1);	
			
		foreach($offers as $offer)
		{
			echo "<tr>";
				echo "<td>$offer[NAME]</td>";
				echo "<td><a href='?edit=$offer[ID]&add=1'>[szerkeszt]</a></td>";
				echo "<td><a href='bp-prices.php?offer=$offer[ID]&add=1&partner=$offer[PARTNER_ID]'>[árak]</a></td>";
			echo "</tr>";

		}
	}	
?>
</table>
</center>
<?
foot();
?>