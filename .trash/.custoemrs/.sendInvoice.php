<?
header('Content-type: text/html; charset=utf-8');
include("inc/config.inc.php");
include("inc/functions.inc.php");
include("inc/mysql.class.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

head();
$headers = "From: info@indulhatunk.hu\r\n" .
"Reply-To: info@indulhatunk.hu\r\n" .
"Content-type: text/html; charset=utf-8\r\n".
"X-Mailer: PHP/" . phpversion();	
	
$mth = date("n") -1;
$subject = "Indulhatunk.hu $mth. havi VEGLEGES elszamolas";


$payID = (int)$_POST[id];
if($payID > 0)
{
	$data[paid] = 1;
	$mysql->query_update("accounting",$data,"id=$payID");
	//die;
}

$sure = $_GET[sure];
$send = $_GET[send];

if($sure <> 1 && $send == 1) {
	echo "<b>Biztosan ki szeretné küldeni a havi elszámolást? <a href=\"?sure=1&send=1\">Igen!</a> | <a href=\"../customers.php\">Nem!</a></b>";
	die;
}
echo "<h1><a href=\"http://admin.indulhatunk.info\">Adminisztrációs felület</a> &raquo; Elszámolás kiküldése kezelése</h1>";

if($CURUSER[userclass] == 255)
{
	echo "<a href=\"?send=1\">Elszámolás kiküldése</a> | <a href=\"transfer.php\" target='_blank'>Netbankba</a> | <a href=\"?showletters=1\" >Leveleket mutasd!</a> | <a href=\"?showPlus=1\"><font color='green'>Pozitiv egyenlegueket mutasd!</font></a> | <a href=\"sendInvoice.php\"><font color='red'>Negativ egyenlegueket mutasd!</font></a>  | <a href=\"sendInvoice.php?showPlus=2\"><font>Mindet mutasd!</font></a>  | <a href=\"sendInvoice.php?showUnpaid=1\"><font>Kiegyenlitetleneket mutasd!</font></a> ";
echo "<hr/>";
}
//die; 


//vatera
$vateraDate = $mysql->query("SELECT * FROM customers WHERE invoice_created = 1   GROUP BY invoice_date AND month(customers.paid_date) = 3 ORDER BY invoice_date DESC  LIMIT 1");
$vateraDate = mysql_fetch_assoc($vateraDate);
$vateraDate = $vateraDate[invoice_date];

$vateraList = $mysql->query("SELECT partners.email,customers.invoice_number,partners.coredb_id,partners.yield,partners.hotel_name,customers.pid,customers.invoice_number, sum(orig_price) AS vateraSum,count(customers.pid) as vateraCount FROM customers
							INNER JOIN partners ON partners.pid = customers.pid WHERE customers.invoice_date = '$vateraDate' AND month(customers.paid_date) = 3 GROUP BY customers.pid");
$vateraData = array();
while($vateraArr = mysql_fetch_assoc($vateraList))
{
	$data[coredb_id] = $vateraArr[coredb_id];
	$data[name] = $vateraArr[hotel_name];
	$data[invoice_date] = $vateraDate;
	$data['yield'] = $vateraArr['yield'];
	$data[vatera_count] = $vateraArr[vateraCount];
	$data[vatera_total] = $vateraArr[vateraSum];
	$data[email] = $vateraArr[email];
	$data[invoice_number_vatera] = $vateraArr[invoice_number];
	$data[hash] = md5($vateraDate.$vateraArr[coredb_id]);
	$data[hash_vatera] = md5($vateraArr[invoice_number]."#FWf#CEFA#SDF343sf#");
	//in house payment total vatera
	$inHouseQuery = $mysql->query("SELECT sum(orig_price) AS sumInHousePrice FROM customers WHERE invoice_number = '$vateraArr[invoice_number]' AND payment <> 5 AND payment <> 6");
	$inHouse = mysql_fetch_assoc($inHouseQuery);
	$data[vatera_inhouse] = $inHouse[sumInHousePrice];
	//
	//in hotel payment total vatera 6
	$inHotelQuery = $mysql->query("SELECT sum(orig_price) AS sumInHotelPrice FROM customers WHERE invoice_number = '$vateraArr[invoice_number]' AND payment = 6");
	$inHotel = mysql_fetch_assoc($inHotelQuery);
	$data[vatera_hotel] = $inHotel[sumInHotelPrice];
	//
	//in check payment total vatera 5
	$inCheckQuery = $mysql->query("SELECT sum(orig_price) AS sumInCheck FROM customers WHERE invoice_number = '$vateraArr[invoice_number]' AND payment = 5");
	$inCheck = mysql_fetch_assoc($inCheckQuery);
	$data[vatera_check] = $inCheck[sumInCheck];
	//
	$vateraQr = mysql_query("SELECT * FROM accounting WHERE coredb_id = $vateraArr[coredb_id] AND invoice_date ='$vateraDate'");
	$varr = mysql_fetch_assoc($vateraQr);
	if(mysql_num_rows($vateraQr) == 1)
	{
		$mysql->query_update('accounting',$data,"hash='$varr[hash]'");
	}
	else
	{
		$mysql->query_insert('accounting',$data);
	}
}

//vatera end
// lmb start
$lmbList = $mysql->query("SELECT partners.email,lmb.invoice_number,partners.yield,partners.hotel_name,lmb.partner_id,lmb.invoice_number, sum(total_price) AS lmbSum,count(lmb.partner_id) as lmbCount FROM lmb
							INNER JOIN partners ON partners.coredb_id = lmb.partner_id WHERE lmb.invoice_date = '$vateraDate' GROUP BY lmb.partner_id");
$data = array();
while($lmbArr = mysql_fetch_assoc($lmbList))
{
	$data[coredb_id] = $lmbArr[partner_id];
	$data[name] = $lmbArr[hotel_name];
	$data[invoice_date] = $vateraDate;
	$data['yield'] = $lmbArr['yield'];
	$data[email] = $lmbArr[email];
	$data[lmb_count] = $lmbArr[lmbCount];
	$data[lmb_total] = $lmbArr[lmbSum];
	$data[hash] = md5($vateraDate.$lmbArr[partner_id]);
	$data[invoice_number_lmb] = $lmbArr[invoice_number];
	$data[hash_lmb] = md5($lmbArr[invoice_number]."#FWf#CEFA#SDF343sf#");
	$vateraQr = mysql_query("SELECT * FROM accounting WHERE coredb_id = $lmbArr[partner_id] AND invoice_date = '$data[invoice_date]'");
	$varr = mysql_fetch_assoc($vateraQr);
	//echo "<pre>";
	//print_r($varr);
	if(mysql_num_rows($vateraQr) == 1)
	{
		$mysql->query_update('accounting',$data,"hash='$varr[hash]'");
	}
	else
	{
		$mysql->query_insert('accounting',$data);
	}
}



//lmb end

//EDIFACT PAYORD start
/*
$payordQr = $mysql->query("SELECT * FROM accounting INNER JOIN partners ON accounting.coredb_id = partners.coredb_id  ORDER BY name ASC"); // AND invoice_date =  '2010-10-08'

while($payordArr = mysql_fetch_assoc($payordQr))
{
	/*
	$id = $payordArr[id];
	
	$lmbTotal = $payordArr[lmb_total]*($payordArr['yield']/100)*1.25;
	$vateraTotal = $payordArr[vatera_total]*($payordArr['yield']/100)*1.25;
	
	$bothTotal = round($vateraTotal+$lmbTotal);
	
	
	$payord = array();
	$payord[] = "PAYORD";
	$payord[] = "DO";
	$payord[] = DATE("Ymd")."".str_pad($payordArr[id], 6, "0", STR_PAD_LEFT);
	$payord[] = str_pad(str_replace("-","","16000060000000041873494"), 47, " ", STR_PAD_RIGHT);
	$payord[] = 0;
	$payord[] = str_pad("indulhatunk.hu kft", 32, " ", STR_PAD_RIGHT); 
	$payord[] = str_pad('', 117, "0", STR_PAD_LEFT); //spacer
	$payord[] = str_pad(str_replace("-","",$payordArr[account_no]), 47, " ", STR_PAD_RIGHT);
	$payord[] = 0;
	$payord[] = str_pad('', 64, "0", STR_PAD_LEFT); //spacer
	$payord[] = str_pad(substr($payordArr[company_name],0,32), 32, " ", STR_PAD_RIGHT);
	$payord[] = str_pad('', 108, "0", STR_PAD_LEFT); //spacer
 	$payord[] = "HU";
	$payord[] = str_pad('', 119, "0", STR_PAD_LEFT); //spacer
	$payord[] = str_pad("VTL elszamolas julius", 96, " ", STR_PAD_RIGHT); 
	$payord[] = str_pad('', 6, " ", STR_PAD_RIGHT); 
	$payord[] = str_pad('', 4, " ", STR_PAD_RIGHT); 
	$payord[] = str_pad('', 107, "0", STR_PAD_LEFT); //spacer

	$payord[] = "HUF";
	$payord[] = 0;
	$payord[] = str_pad($bothTotal, 13, "0", STR_PAD_LEFT);
	$payord[] = str_pad('', 12, "0", STR_PAD_LEFT); //spacer
	$payord[] = DATE("Ymd");
	$payord[] = ' ';
	$payord[] = str_pad('', 94, "0", STR_PAD_LEFT); //spacer
	$payord[] = "00";
	$payord[] = "\r\n";

	//var_dump($payord);
	
	$line[] = implode($payord,"");
	
	

	$lmbTotal = $payordArr[lmb_total]*($payordArr['yield']/100)*1.25;
	$vateraTotal = $payordArr[vatera_total]*($payordArr['yield']/100)*1.25;
	
	$bothTotal = round($vateraTotal+$lmbTotal);


$accountNumber = "160000060000000038168594";

if($payordArr[account_no] <> 0)
{
$transactions.="<Transaction>
<Originator>
<Account>
<AccountNumber>$accountNumber</AccountNumber>
</Account>
</Originator>
<Beneficiary>
<Name>".str_replace("&","-",$payordArr[company_name])."</Name>
<Account>
<AccountNumber>".str_replace("-","",$payordArr[account_no])."</AccountNumber>
</Account>
</Beneficiary>
<Amount Currency=\"HUF\">$bothTotal.00</Amount>
<RequestedExecutionDate>".date("Y")."-".date("m")."-".date("d")."</RequestedExecutionDate>
<RemittanceInfo>
<Text>$payordArr[invoice_number_vatera] sz. szla kiegyenl.</Text>
</RemittanceInfo>
</Transaction>";

}
$textTransactions.= "$payordArr[company_name];$payordArr[hotel_name];$payordArr[account_no];$bothTotal;$payordArr[invoice_number_vatera] sz. szla kiegyenl.\r\n";

}
*/
	//$edifact = implode($line);
//echo "<pre>";
//	print_r($textTransactions);
//echo "<pre/>";
/*$xmlFile ="<?xml version=\"1.0\" encoding=\"utf-8\"?>
<HUFTransactions>
$transactions
</HUFTransactions>";

/*	$file = "edifact_payord.xml";
	$fh = fopen($file, 'w') or die("can't open file");
	fwrite($fh, $xmlFile);
	fclose($fh);


	$file = "edifact_payord.txt";
	$fh = fopen($file, 'w') or die("can't open file");
	fwrite($fh, utf8_decode($textTransactions));
	fclose($fh);
//EDIFACT PAYORD end
*/
if($_GET[showUnpaid] == 1)
	$unpaid = "AND paid = 0";
else
	$unpaid = '';
	
if($CURUSER[userclass] <> 255)
{
	$unpaid = "AND paid = 0";
}
	

if($_GET[showletters] == 1 || $_GET[send] == 1)
{
	$dt = mysql_fetch_assoc($mysql->query("SELECT max(invoice_date) as last_date FROM accounting LIMIT 1")); // AND invoice_date =  '2010-10-08'
	$accountingArr = $mysql->query("SELECT * FROM accounting where invoice_date =  '$dt[last_date]'"); // AND invoice_date =  '2010-10-08'
}
else
{
	$accountingArr = $mysql->query("SELECT * FROM accounting WHERE id > 0 $unpaid ORDER BY name ASC"); // AND invoice_date =  '2010-10-08'
}
echo "<table border='1' cellpadding='3' cellspacing='0'>";
	echo "<tr class='grey'>";
		echo "<td>#</td>";
		echo "<td>Datum</td>";
		echo "<td>Partner</td>";
		echo "<td>LMB db</td>";
		echo "<td>LMB total</td>";
		echo "<td>LMB jutalek</td>";
		echo "<td>Vatera  db</td>";
		echo "<td>Vatera total</td>";
		echo "<td>Vatera jutalek</td>";
		echo "<td>Egyenleg</td>";
		echo "<td>VTL szamla</td>";
		echo "<td>LMB szamla</td>";
		echo "<td>&#9745;</td>";
	echo "<tr/>";
$i=1;
while($partnerArr = mysql_fetch_assoc($accountingArr))
{
	$lmbTotal = 0;
	$vateraTotal = 0;
	$bothTotal = 0;
	$remainder = '';
	$balanceTotal = 0;
	
	$minusArr = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total FROM checkout_accounts WHERE partner_id = $partnerArr[coredb_id] ")); //AND comment NOT LIKE '%11 havi%'
	
	$pr = mysql_fetch_assoc($mysql->query("SELECT * FROM partners where coredb_id = $partnerArr[coredb_id]"));
	if($minusArr[total] < 0)
	{
		$remainder = "<b><font color=red>Kiegyenlítetlen hátralék: ".formatPrice($minusArr[total])." </font></b>(Korábbi számláit megtekintheti az admin.indulhatunk.info oldalon belépve.)<br/><br/>";
	}
	else
		$remainder = "Előző havi egyenleg: <b><font>".formatPrice($minusArr[total])." </font></b>(Korábbi számláit megtekintheti az admin.indulhatunk.info oldalon belépve.)<br/><br/>";
	if($partnerArr[lmb_count] > 0)
	{
		$lmbTotal = $partnerArr[lmb_total]*($partnerArr['yield']/100)*1.25;
	$lmb = "A már az előző levelünkben leegyeztetett forgalom:<br/><br/>
			<b>$partnerArr[lmb_count] db</b> foglalás történt <b>".formatPrice($partnerArr[lmb_total])."</b> értékben.<br/><br/>
			A szerződésünk értelmében a számla végösszege ".formatPrice($partnerArr[lmb_total])." Ft * $partnerArr['yield']% + 25% Áfa <br/><br/>
			Összesen: <b>".formatPrice($lmbTotal)."</b>";
	}
	else
	{
		$lmb = "Az adott hónapban nem történt értékesítés.";
	}
	
	if($partnerArr[vatera_count] > 0)
	{
	$vateraTotal = $partnerArr[vatera_total]*($partnerArr['yield']/100)*1.25;
	$vatera = "Az elmúlt időszakban a Vaterán, TeszVeszen és a LicitTravelen összesen <b>$partnerArr[vatera_count] db</b> utalványt értékesítettünk <b>".formatPrice($partnerArr[vatera_total])."</b> értékben.<br/><br/>
				<b>A kiállított utalványainkat</b><br/>
				A szállodában:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>".formatPrice($partnerArr[vatera_hotel])."</b> értékben<br/>
				Üdülés Csekkel:&nbsp;&nbsp;&nbsp;<b>".formatPrice($partnerArr[vatera_check])."</b>  értékben <br/>
				Nálunk:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>".formatPrice($partnerArr[vatera_inhouse])."</b>  értékben fizettek.<br/><br/>
				A szerződésünk értelmében a számla végösszege ".formatPrice($partnerArr[vatera_total])." * $partnerArr['yield']% + 25% Áfa <br/>
				Összesen: <b>".formatPrice($vateraTotal)."</b>";
	}
	else
	{
		$vatera = "Az adott hónapban nem történt értékesítés.";
	}
	
	$bothTotal = $vateraTotal+$lmbTotal;
	
	$balanceTotal = $vateraTotal+$lmbTotal+($minusArr[total]*-1);
	
	if(($partnerArr[vatera_inhouse]-$balanceTotal)>0)
	{
		$total = $partnerArr[vatera_inhouse]-$balanceTotal;
		$totalOrig = $partnerArr[vatera_inhouse]-$bothTotal;
		$totalMessage = "<b>".formatPrice($total)."</b>-ot utaltunk el az Önök számlájukra.";
		
		
		$month = date('n')-1;
		$title = $month." havi elszámolás";
		$titleTransfer = $month." havi elszámolás elutalva";
		$q = $mysql->query("SELECT * FROM checkout_accounts WHERE partner_id = $pr[coredb_id] and comment = '$title' and value = ".round($total)); //partner_id = $pr[coredb_id] and comment = '$title' and value = ".round($total)."");
		
	//	echo "SELECT * FROM checkout_accounts WHERE partner_id = $pr[coredb_id] and comment = '$title' and value = ".round($total)."<hr/>";
		if(mysql_num_rows($q) == 0)
		{
			$d[partner_id] = $pr[coredb_id];
			$d[date] = 'now()';
			$d[type] = 0;
			$d[comment] = $title;
			$d[value] = round($total);
		//	$mysql->query_insert("checkout_accounts",$d);

			$dt2[partner_id] = $pr[coredb_id];
			$dt2[date] = 'now()';
			$dt2[type] = 1;
			$dt2[comment] = $titleTransfer;
			$dt2[value] = round($total*-1);
		//	$mysql->query_insert("checkout_accounts",$dt2);
	//	echo "insert plusminus<br/>";
		}
	}
	else
	{
		$total = 0;
		$total = $partnerArr[vatera_inhouse]-$balanceTotal;
		$totalOrig = $partnerArr[vatera_inhouse]-$bothTotal;
		$totalMessage = "<b>".formatPrice($total*-1)."</b>-ot kérjük átutalni szíveskedjen az alábbi bankszámlánkra.<br/><br/>
						Indulhatunk.hu Kft<br/>
						Erste Bank<br/>
						12345678-12345678-12345678";
		$accountsTotal  = round($total);
		
		$month = date('n')-1;
		$title = $month." havi elszámolás";
		$q2 = $mysql->query("SELECT * FROM checkout_accounts WHERE partner_id = $pr[coredb_id] and comment = '$title' and (value = $accountsTotal or value = ". $accountsTotal/2 .")");
		//echo "SELECT * FROM checkout_accounts WHERE partner_id = $pr[coredb_id] and comment = '$title' and (value = $accountsTotal or value = $accountsTotal*2)<hr/>";
		if(mysql_num_rows($q2) == 0)
		{
			$d3[partner_id] = $pr[coredb_id];
			$d3[date] = 'now()';
			$d3[type] = 0;
			$d3[comment] = $title;
			$d3[value] = $accountsTotal;
		//	$mysql->query_insert("checkout_accounts",$d3);
			
		//	echo "minus minus<hr/>";
		}
	}
	
	if($partnerArr[invoice_number_vatera] <> '')
		$vateraInvoice = "A $partnerArr[invoice_number_vatera] számú jutalékszámlát az alábbi linkre kattintva töltheti le: <a href=\"https://admin.indulhatunk.info/invoices/dl/$partnerArr[hash_vatera]\">$partnerArr[invoice_number_vatera] sz. számla letöltése</a><br/><br/>";
	else
		$vateraInvoice = '';
		
	if($partnerArr[invoice_number_lmb] <> '')
		$lmbInvoice = "A $partnerArr[invoice_number_lmb] számú jutalékszámlát az alábbi linkre kattintva töltheti le: <a href=\"https://admin.indulhatunk.info/invoices/dl/$partnerArr[hash_lmb]\">$partnerArr[invoice_number_lmb] sz. számla letöltése</a><br/><br/>";
	else
		$lmbInvoice = '';
		
		if($partnerArr[paid] == 1)
			{
				$rowClass='green';
			}
		else
			$rowClass = '';
		$row =  "<tr class=\"$rowClass\"><form method='post' action=\"sendInvoice.php?showPlus=$showPlus\"><input type='hidden' name='id' value=\"$partnerArr[id]\">";
		$row.= "<td>$i</td>";
		$row.= "<td width='68'>$partnerArr[invoice_date]</td>";
		$row.= "<td>$pr[company_name] <br/>$partnerArr[name]</td>";
		$row.= "<td align='right'>$partnerArr[lmb_count]</td>";
		$row.= "<td align='right'>".formatPrice($partnerArr[lmb_total])."</td>";
		$row.= "<td align='right'>".formatPrice($lmbTotal)."</td>";
		$row.= "<td align='right'>$partnerArr[vatera_count]</td>";
		$row.= "<td align='right'>".formatPrice($partnerArr[vatera_total])."</td>";
		$row.= "<td align='right' align='right'>".formatPrice($vateraTotal)."</td>";
		
		if($totalOrig < 0)
			$tot = "<font color='red'>".formatPrice($totalOrig)."</font>";
		else
			$tot = formatPrice($totalOrig);
		$row.= "<td align='right'><b>$tot</b></td>";
		$row.= "<td><a href=\"http://admin.indulhatunk.info/invoices/dl/$partnerArr[hash_vatera]\">$partnerArr[invoice_number_vatera]</a></td>";
		$row.= "<td><a href=\"http://admin.indulhatunk.info/invoices/dl/$partnerArr[hash_lmb]\">$partnerArr[invoice_number_lmb]</a></td>";
		$row.= "<td><input type=\"checkbox\" name=\"paid\" onChange='submit()'/></td>";
	
		$row.= "</form><tr/>";
		
	$showPlus = $_GET[showPlus];
	
	if($total < 0 && $showPlus <> 1) {
		echo $row;
		$i++;
	}
	elseif($total > 0 && $showPlus == 1)
	{
		echo $row;
		$i++;
	}
	elseif($showPlus == 2)
	{
		echo $row;
		$i++;
	}
	
	$dat[total] = round($totalOrig);
		
	$mysql->query_update("accounting",$dat,"id=$partnerArr[id]");
	
	$mixedLetter ="
<b>Kedves Partnerünk!</b><br/><br/>

Az alábbiakban részletezzük az aktuális elszámolásunkat.<br/>
Mellékletben a jutalék számla.<br/><br/>

<b>LastMinuteBelföld</b><br/>
$lmb<br/><br/>
<b>Utalványok:</b><br/>
$vatera<br/><br/>



<b>Elszámolás:</b><br/>
Jutalék számlák összesen: <b>".formatPrice($bothTotal)."</b><br/>
Nálunk összegyűlt pénz: <b>".formatPrice($partnerArr[vatera_inhouse])."</b><br/>
$remainder
Az egyenlegeink összevezetése után  <br/><br/>

$totalMessage<br/><br/>

$vateraInvoice
$lmbInvoice
Üdvözlettel:<br/>
Forró Tamás<br/>";
	$letters.="$partnerArr[name]<hr/>$mixedLetter";
	//echo "<hr/>";
	if($send == 1 && $sure == 1)
	{
		echo "elküldve $partnerArr[email]<hr/>";
		echo "$mixedLetter<hr/>";
		mail($partnerArr[email],$subject, $mixedLetter,iconv('utf-8','ISO-8859-2',$headers));
	}
}
echo "</table>";

$showLetters = $_GET[showletters];
if($showLetters ==1)
	echo $letters;

foot();



?>