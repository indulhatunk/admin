<?
/*
 * customers.php 
 *
 * customers page
 *
*/
error_reporting(E_ERROR | E_PARSE);

/* bootstrap file */
include("inc/init.inc.php");
userlogin();




//search 
$s = trim($_POST[search]);


head("Vásárlók kezelése");
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					<? 
		
	if($CURUSER[userclass] < 90)
		$export = "<li><a href=\"https://admin.indulhatunk.info/export.php\">$lang[export]</a></li>";
	else
		$export = '';
		
	if($CURUSER[userclass] == 5)
	{
		if($_GET[add] == 1)	
			$aclass = 'current';
		else
			$aclass = '';	
			
		if($_GET[editid] > 0)
			$addlink = "<li><a href=\"?add=1\" class='$aclass'>Vásárló szerkesztése</a></li>";
		else
			$addlink = "<li><a href=\"?add=1\" class='$aclass'>Új vásárló</a></li>";
	}
		
		
		if(($_GET[type] == 'white' || !isset($_GET[type]) ||  $_GET[type] == '') && $_GET[add] <> 1)
			$class1 = 'current';
		if($_GET[type] == 'green')
			$class2 = 'current';
		if($_GET[type] == 'red')
			$class3 = 'current';
		if($_GET[type] == 'blue')
			$class4 = 'current';
		if($_GET[type] == 'grey')
			$class5 = 'current';

		echo "$addlink
		<li><a href=\"?year=$year&month=$month&type=white\" class=\"$class1\">$lang[active]</a></li>
		<li><a href=\"?year=$year&month=$month&type=green\"  class=\"$class2\">$lang[paid_printed]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=red\"  class=\"$class3\">$lang[notpaid_printed]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=blue\"  class=\"$class4\">$lang[paid_notprinted]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=grey\"  class=\"$class5\">$lang[billed]</a></li>
		     $export";
		 
		 if($CURUSER[userclass] > 50)
		 {
		 	echo "<li><a href=\"deleted.php\" class=\"\">Törölt elemek</a></li>";
		 	echo "<li><a href=\"inactive.php\" class=\"\">Inaktív vásárlók</a></li>";
			echo "<li><a href=\"emails.php\" class=\"\">Feldolgozatlan levelek</a></li>";

		 }
					?>
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>



<fieldset class='searchform'>




<form method="post" id="sform" action="customers.php">
<input type="text" name="search"  value="<?=$cookie?>" /><a href="#" class='searchbutton' id='submit'>Ok</a>
<? if($CURUSER[userclass] > 50) {

if($_POST[showinactive] == 'on')
{
	$showchecked = "checked";
}
 ?>
<br/>
<div style='margin:0px 0 0 0'>
	<label for="showinactive"><input type='checkbox' name='showinactive' id="showinactive" <?=$showchecked?>/> Töröltek között </label>
</div>
<? } ?>
</form>
</fieldset>
<?
//submenu
if($CURUSER["userclass"] > 50)
{	
	if($CURUSER["userclass"] == 255)
	{
		$stat = "<a href=\"?statistics=1\" class=\"show\">$lang[statistics]</a>";
	}
	else 
		$stat = "<a href=\"?type=orange\" class=\"orange\">$lang[postpaid]</a>";
	echo "<div class='subMenu'><a href=\"customers.php?add=1\">$lang[new_customer]</a> <a href=\"vouchers/print.php\" class='grey'>$lang[print_vouchers]</a> <a href=\"vouchers/print_envelope.php\" class='grey'>$lang[print_envelopes]</a> <a href=\"vouchers/print_post.php\" class='grey'>$lang[print_post]</a>  $stat <div class='cleaner'></div></div>";
}

//show statistics
if($CURUSER["userclass"] == 255 && $_GET[statistics] == 1)
{

$dateNow = date("n");
$dateMinus = $dateNow-1;
$yearMinus = date("Y");

if($dateMinus == 0)
{
	$dateMinus = 12;
	$yearMinus = date("Y")-1;
}	
	//echo "<h3>Indulhatunk.hu Kft.</h3>";
	getLongStats();
	getStatistics($dateMinus,$yearMinus);
	getStatistics($dateNow,date("Y"));
	getShortStats();

	foot();
	die;
}

if($msg <> '')
	echo message("$msg");

	$string = generateNumber();
	
if($add == 1 && $CURUSER["userclass"] >= 5) {

	if($editid > 0) 
		$formData = '';
	else
		$formData = unserialize(stripslashes($_COOKIE[customerForm]));
	
?>
<div class="partnerForm">
	<form method="POST" id="cform" action="customers.php?month=<?=$month?>&year=<?=$year?>&type=<?=$type?>">
	
		<?if($editarr[offer_id] =="") {?>
			<input type="hidden" name="offer_id" value="<?=$string?>">
		<?} else {?>
			<input type="hidden" name="editid" value="<?=$editarr[cid]?>">
			<input type="hidden" name="offer_id" value="<?=$editarr[offer_id]?>">
		<?}?>
		
		<? if($CURUSER[userclass] == 5) { ?>
			<input type="hidden" name='reseller_id' value='<?=$CURUSER[pid]?>'/> 
			
			
			<input type="hidden" name='type' value='8'/> 
			<input type="hidden" name='orig_price' value='<?=$editarr[orig_price]?>'/> 
			<input type="hidden" name='payment' value='<? if($editarr[payment] > 0) echo $editarr[payment]; else echo "2";?>'/> 
			<input type="hidden" name='shipment' value='<?=$editarr[shipment]?>'/> 

			<input type="hidden" name='check_count' value='<?=$editarr[check_count]?>'/> 
			<input type="hidden" name='check_value' value='<?=$editarr[check_value]?>'/> 
			<input type="hidden" name='hotel_comment' value='<?=$editarr[hotel_comment]?>'/> 

			<input type="hidden" name='email_id' value='<?=$editarr[email_id]?>'/> 

			
		<? } else { ?>
			<!-- <input type="hidden" name='reseller_id' value='<?=$editarr[reseller_id]?>'/>-->
		<? } ?>
		
		
	<fieldset id='customerForm'>
	<legend>Vásárlók kezelése</legend>
	
	<table width='100%'>
	
		
		<?if($editarr[offer_id] =="") {?>
			<tr>
				<td class='lablerow'>Azonosító:</td>
				<td><?=$string?></td>
			</tr>
		<?}	else {?>
			<tr>
				<td class='lablerow'>Azonosító:</td>
				<td><?=$editarr[offer_id]?></td>
			</tr>
		<?}?>
		
		
		<? if($CURUSER[userclass] > 50) { ?>
			<tr>
				<td class='lablerow'>Cég:</td>
				<td>
					<select name='company_invoice'>
						<option value='hoteloutlet' <? if($editarr[company_invoice] == 'hoteloutlet' || $editarr[company_invoice] == '') echo "selected";?>>Hotel Outlet Kft.</option>
						<option value='indulhatunk' <? if($editarr[company_invoice] == 'indulhatunk') echo "selected";?>>Indulhatunk.hu Kft.</option>
					</select>
				</td>
			</tr>
		<? } ?>
		
		<? if($CURUSER[userclass] > 50) { ?>
		<tr><td colspan='2'>
		<select name="offers_id">
				<?
				
				if($editarr[cid] == '')
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW()  ORDER BY partners.hotel_name ASC");
				}
				else
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id ORDER BY partners.hotel_name ASC");
				}
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
							
						$shortid = substr($partnerArr[outer_name], 0,3);
						
						$shortname = mb_substr($partnerArr[shortname], 0,30,'UTF-8');
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $shortname - $shortid - ".formatPrice($partnerArr[actual_price])."</option>";
					}	
				?>
			</select>
		</td>
			</tr>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] > 0) { 

			$partnerArr = mysql_fetch_assoc($mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.id = $editarr[offers_id] LIMIT 1"));

		?>	
			<tr>
				<td class='lablerow'>Ajánlat:</td>
				<td><? echo "$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name]" ?></td>
			</tr>
			<input type="hidden" name="offers_id" value="<?=$editarr[offers_id]?>"/>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] == 0) { ?>
		<tr><td colspan='2'>
		
		
		<select name="offers_id" style='width:680px;'>
				<?
				
					$partnerQuery = $mysql->query("SELECT offers.reseller_price, offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW() AND reseller_enabled = 1 AND pdf_title <> '' AND reseller_from_date <= NOW() ORDER BY partners.hotel_name ASC");
			
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
							
					
						$shortname = mb_substr($partnerArr[shortname],0, 20, "UTF-8");
		
						$shortid = substr($partnerArr[outer_name], 0,3);
						


						
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $shortname - $shortid - ".formatPrice($partnerArr[reseller_price])."</option>";
					}	
				?>
			</select>
		</td>
			</tr>
		
		<? } ?>
		<? 	if($editarr[cid] <> '' && $CURUSER[userclass] > 50)
			{ ?>
		<tr><td colspan='2'>
		<select name="sub_pid">
			<option value=''>Válasszon</option>
				<?
				
				$partnerQuery = $mysql->query("SELECT * FROM partners WHERE (country = '' or country = 'hu') AND hotel_name <> '' ORDER BY partners.hotel_name ASC");
				
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[pid] == $editarr[sub_pid] || $partnerArr[pid] == $formData[sub_pid])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name]</option>";
					}
				
				?>
			</select>
		</td>
			</tr>
			
			<tr>
				<td class='lablerow'>Viszonteladó ID*:</td>
				<td><input type="text" name="reseller_id" value="<?=$editarr[reseller_id]?>" id=''/></td>
			</tr>
			
		<? } else { ?>
			<input type="hidden" name="sub_pid" value="<?=$editarr[sub_pid]?>"/>
		<? } ?>
		
		<tr>
				<td class='lablerow'>Vevő neve:</td>
				<td><input type="text" name="name" value="<?=$editarr[name]?>" id='customername'/></td>
			</tr>
		<tr>
				<td class='lablerow'>Cím:</td>
				<td><input type="text" name="zip" value="<?=$editarr[zip]?>" style='width:35px;' class='numeric'/><input type="text" name="city" style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" name="address" class="small" style='width:295px;' value="<?=$editarr[address]?>" /></td>
			</tr>
		
		
		<tr id='invoicebutton' ><td colspan='2' <? if($editarr[invoice_name] <> '') echo "style='display:none'"?>><div style='text-align:center; padding:5px; '><a href='#' id='showinvdata'><b>Eltérő számlázási adatok</b></a></div></td></tr>
		
		<tr class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><td class='lablerow'>Számlázási név:</td>
				<td><input type="text" name="invoice_name" value="<?=$editarr[invoice_name]?>"/></td>
			</tr>
		<tr class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><td class='lablerow'>Számlázási Cím:</td>
				<td><input type="text" name="invoice_zip"value="<?=$editarr[invoice_zip]?>" style='width:35px;'/><input type="text" style='width:140px;margin:0 1px 0 1px' class="small1" value="<?=$editarr[invoice_city]?>" name="invoice_city"/><input type="text" name="invoice_address" class="small" style='width:295px;' value="<?=$editarr[invoice_address]?>" /></td>
			</tr>
		
		
		<tr>
				<td class='lablerow'>Telefonszám*:</td>
				<td><input type="text" name="phone" value="<?=$editarr[phone]?>" id='customerphone'/></td>
			</tr>
		<tr>
				<td class='lablerow'>E-mail cím*:</td>
				<td><input type="text" name="email" value="<?=$editarr[email]?>" id='customeremail'/></td>
			</tr>

		<? if($CURUSER[userclass] > 50) { ?>
		<tr>
				<td class='lablerow'>Licit:</td>
				<td><input type="text" name="bid_name" value="<?=$editarr[bid_name]?><?=$formData[bid_name]?>"/></td>
			</tr>
		<tr>
				<td class='lablerow'>Licit típus:</td>
				<td>
			<select name="type">
				<option value="4" <?if($editarr[type]==4)echo"selected";?>>Outlet</option>
				<option value="1" <?if($editarr[type]==1)echo"selected";?>>Vatera</option>
				<option value="2" <?if($editarr[type]==2)echo"selected";?>>Teszvesz</option>
				<option value="6" <?if($editarr[type]==6)echo"selected";?>>Lealkudtuk</option>
				<option value="7" <?if($editarr[type]==7)echo"selected";?>>Grando</option>
				<option value="3" <?if($editarr[type]==3)echo"selected";?>>Licittravel</option>
				<option value="5" <?if($editarr[type]==5)echo"selected";?>>Pult</option>
				<option value="8" <?if($editarr[type]==8)echo"selected";?>>Viszonteladó</option>
			</select>
		</td>
			</tr>
		<tr>
				<td class='lablerow'>Eladási ár:</td>
				<td><input type="text" name="orig_price" value="<?=$editarr[orig_price]?><?=$formData[orig_price]?>" style='width:50px;' class='numeric'/> Ft</td>
			</tr>
		<!--<tr>
				<td class='lablerow'>Promóciós kód:</td>
				<td>
			<select name="promotion_code">
				<option value=''>Válasszon</option>
				<?
					$partnerQuery = $mysql->query("SELECT * FROM promotion ORDER BY name ASC");
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[code] == $editarr[promotion_code])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[code]\" $selected>$partnerArr[name]</option>";
					}
				
				?>
			</select>
		</td>
			</tr>-->
		<tr>
				<td class='lablerow'>Fizetés módja:</td>
				<td>
			<select name="payment">
				<option value="2" <?if($editarr[payment]==2)echo"selected";?>>Készpénz</option>
				<option value="1" <?if($editarr[payment]==1)echo"selected";?>>Átutalás</option>
				<option value="3" <?if($editarr[payment]==3)echo"selected";?>>Utánvét</option>
				<option value="9" <?if($editarr[payment]==9)echo"selected";?>>Bankkártya</option>
				<option value="4" <?if($editarr[payment]==4)echo"selected";?>>Futár</option>
				<option value="5" <?if($editarr[payment]==5)echo"selected";?>>Üdülési csekk</option>
				<option value="6" <?if($editarr[payment]==6)echo"selected";?>>Helyszinen</option>
				<option value="7" <?if($editarr[payment]==7)echo"selected";?>>Online</option>
				<option value="8" <?if($editarr[payment]==8)echo"selected";?>>Facebook</option>
				<option value="10" <?if($editarr[payment]==10)echo"selected";?>>OTP SZÉP kártya</option>
				<option value="11" <?if($editarr[payment]==11)echo"selected";?>>MKB SZÉP kártya</option>
				<option value="12" <?if($editarr[payment]==12)echo"selected";?>>K&H SZÉP kártya</option>


			</select>
		</td>
			</tr>
		<tr>
				<td class='lablerow'>Szállítás módja:</td>
				<td>
			<select name="shipment">
				<option value="0">Válassza ki az átvételi módot</option>
				<option value="2" <?if($editarr[shipment]==2)echo"selected";?>>Online kérem</option>
				<option value="3" <?if($editarr[shipment]==3)echo"selected";?>>Személyes átvétel</option>
				<option value="4" <?if($editarr[shipment]==4)echo"selected";?>>Ajánlott levélként: +395 Ft</option>
				<option value="5" <?if($editarr[shipment]==5)echo"selected";?>>Postai utánvéttel: +1 450 Ft</option>
			</select>

		</td>
			</tr>
		<tr>
				<td class='lablerow'>Postaköltség:</td>
				<td><input type="text" name="post_fee" value="<?=$editarr[post_fee]?><?=$formData[post_fee]?>" style='width:50px;' class='numeric'/> Ft</td>
			</tr>
		
		<!--<tr>
				<td class='lablerow'>Üdülésicsekk info:</td>
				<td><textarea name="checks"><?=$editarr[checks]?></textarea></td>
			</tr>-->
		<tr>
				<td class='lablerow'>Üdülésicsekkek értéke:</td>
				<td><input type="text" name="check_value" value="<?=$editarr[check_value]?>" style='width:50px' class='numeric'/> Ft</td>
			</tr>
		<tr>
				<td class='lablerow'>Üdülésicsekkek száma:</td>
				<td><input type="text" name="check_count" value="<?=$editarr[check_count]?>"  style='width:50px' class='numeric'/> darab</td>
			</tr>
			
			<tr>
				<td class='lablerow'>Külföldi fizetés dátuma:</td>
				<td><input type="text" name="foreign_paid_date" value="<?=$editarr[foreign_paid_date]?>" class="maskeddate" style='width:90px'/></td>
			</tr>
			
<? } ?>
		<tr>
				<td class='lablerow'>Megjegyzés:</td>
				<td><textarea name="comment"><?=$editarr[comment]?></textarea></td>
			</tr>
		
		<? if($CURUSER[userclass] > 50) { ?>
		<tr>
				<td class='lablerow'>Hotel megjegyzés:</td>
				<td><textarea name="hotel_comment"><?=$editarr[hotel_comment]?></textarea></td>
			</tr>
		
		<tr>
				<td class='lablerow'>Érvényesség:</td>
				<td><textarea name="validity"><?=$editarr[validity]?><?=$formData[validity]?></textarea></td>
			</tr>
		<tr>
				<td class='lablerow'>Nem tartalmazza:</td>
				<td><input type="text" name="not_include" value="<?=$editarr[not_include]?><?=$formData[not_include]?>"/></td>
			</tr>
		<? } ?>
		
		<tr>
				<td class='lablerow'>Ár elrejtése?</td>
				<td>
		<select name='gift'>
			<option value='0'>Nem</option>
			<option value='1' <?if($editarr[gift]==1)echo"selected";?>>Igen</option>
			</select>
		</td>
			</tr>
		
		
		<? if($CURUSER[userclass] > 50) { ?>
			
			<tr>
				<td class='lablerow'>Email ID:</td>
				<td><input type="text" name="email_id" value="<?=$editarr[email_id]?>" class='numeric' style='width:50px;'/></td>
			</tr>
			<tr>
				<td class='lablerow'>Számlaszám:</td>
				<td><input type="text" name="user_invoice_number" value="<?=$editarr[user_invoice_number]?>"/></td>
			</tr>
			<tr>
				<td class='lablerow'>Számla dátuma:</td>
				<td><input type="text" name="user_invoice_date" value="<?=$editarr[user_invoice_date]?>"/></td>
			</tr>
			<tr>
				<td class='lablerow'>Számla elkezdve:</td>
				<td><input type="text" name="user_invoice_started" value="<?=$editarr[user_invoice_started]?>"/></td>
			</tr>
		<? } ?>

		<?if($editarr[inactive] == 1 && $CURUSER[userclass] > 50){?>
		<tr>
				<td class='lablerow'>Törölt:</td>
				<td>
			<select name="inactive">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[inactive]==1)echo"selected";?>>Igen</option>
			</select>
		</td>
			</tr>
		<?
		}
		if($CURUSER["userclass"] == 255)
		{
		?>		
		<tr>
				<td class='lablerow'>Fizetve:</td>
				<td>
			<select name="paid">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[paid]==1)echo"selected";?>>Igen</option>
			</select>
		</td>
			</tr>			

		<? } ?>

	
	<?
	if($editarr[offer_id] <>"") {
		$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $editarr[offers_id]");
		$offerArr = mysql_fetch_assoc($offerQr);
		
		if($CURUSER[userclass] == 5 || $editarr[type] == 8)
			$offerArr[actual_price] = $offerArr[reseller_price];
		else	
			$offerArr[actual_price] = $offerArr[actual_price];
	?>
	<input type="hidden" name="days" value="<?=$offerArr[days]?>"/>
	<input type="hidden" name="actual_price" value="<?=$offerArr[actual_price]?>"/>

	<tr>
		<td colspan='2' class='header'>Felár információk</td>
	</tr>
	<tr>
				<td class='lablerow'>Ajánlat alapára:</td>
				<td> <?=formatPrice($offerArr[actual_price]);?> </td>
			</tr>
	<tr>
				<td class='lablerow'>Éjszakák száma:</td>
				<td> <?=$offerArr[days]?> éj </td>
			</tr>

	<?
		if($offerArr[plus_half_board]<>0) {?>
	<tr>
				<td class='lablerow'>Félpanzió</td>
				<td><input type="checkbox" name="plus_half_board" class="plus_half_board" value="<?=$offerArr[plus_half_board]?>" <?if($editarr[plus_half_board]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_half_board])?>/éj</td>
				<td></td>
			</tr>
<?}?>

<?if($offerArr[plus_weekend_plus]<>0) {?>
<tr>
				<td class='lablerow'>Hétvégi felár</td>
				<td><input type="checkbox" name="plus_weekend_plus" class="plus_weekend_plus" value="<?=$offerArr[plus_weekend_plus]?>" <?if($editarr[plus_weekend_plus]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_weekend_plus]*2)?></td>
			</tr>
<?}?>

<?if($offerArr[plus_bed]<>0) {?>
<tr>
				<td class='lablerow'>Pótágy felár</td>
				<td><input type="checkbox" name="plus_bed" class="plus_bed" value="<?=$offerArr[plus_bed]?>" <?if($editarr[plus_bed]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_bed_plus_food]<>0) {?>
<tr>
				<td class='lablerow'>Félpanzió + pótágy felár</td>
				<td><input type="checkbox" name="plus_bed_plus_food" class="plus_bed_plus_food" value="<?=$offerArr[plus_bed_plus_food]?>"  <?if($editarr[plus_bed_plus_food]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed_plus_food])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_child2_value]<>0) {?>
<tr>
				<td class='lablerow'>1. gyermek kora:</td>
				<td> <input type="hidden" name="prev_child1" value="0" class="prev_child1" <?=$disabled?>/>
	<select name="child1_value" class="child1_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>"  <? if($editarr[plus_child1_value]==1 || ($editarr[plus_child1_value] >1 && $editarr[plus_child1_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	</td>
			</tr><?}
else
{
?>
<input type='hidden' name='child1_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_child2_value]<>0) {?>
<tr>
				<td class='lablerow'>2. gyermek kora: </td>
				<td> <input type="hidden" name="prev_child2" value="0" class="prev_child2"/>
	<select name="child2_value" class="child2_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child2_value]==1 || ($editarr[plus_child2_value] >1 && $editarr[plus_child2_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	</td>
			</tr><?}
else
{
?>
<input type='hidden' name='child2_value' value='999'/>
<?
}
?>
<?if($offerArr[plus_child2_value]<>0) {?>
<tr>
				<td class='lablerow'>3. gyermek: </td>
				<td>	
	<select name="child3_value" class="child3_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child3_value]==1 || ($editarr[plus_child3_value] >1 && $editarr[plus_child3_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig </option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett</option>
	</select>
</td>
			</tr>
<?}
else
{
?>
<input type='hidden' name='child3_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_room1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room1_name]?> szoba felár</td>
				<td> <input type="checkbox" class="room1_value" name="room1_value" value="<?=$offerArr[plus_room1_value]?>" <?if($editarr[plus_room1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room1_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_room2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room2_name]?> szoba felár</td>
				<td> <input type="checkbox" class="room2_value" name="room2_value" value="<?=$offerArr[plus_room2_value]?>" <?if($editarr[plus_room2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room2_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_room3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room3_name]?> szoba felár</td>
				<td> <input type="checkbox" class="room3_value" name="room3_value" value="<?=$offerArr[plus_room3_value]?>" <?if($editarr[plus_room3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room3_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other1_name]?> felár</td>
				<td> <input type="checkbox" class="other1_value" name="other1_value" value="<?=$offerArr[plus_other1_value]?>" <?if($editarr[plus_other1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other1_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other2_name]?> felár</td>
				<td> <input type="checkbox"  class="other2_value" name="other2_value" value="<?=$offerArr[plus_other2_value]?>" <?if($editarr[plus_other2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other2_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other3_name]?> felár</td>
				<td> <input type="checkbox" class="other3_value" name="other3_value" value="<?=$offerArr[plus_other3_value]?>" <?if($editarr[plus_other3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other3_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other4_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other4_name]?> felár</td>
				<td> <input type="checkbox" class="other4_value" name="other4_value" value="<?=$offerArr[plus_other4_value]?>" <?if($editarr[plus_other4_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other4_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other5_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other5_name]?> felár</td>
				<td> <input type="checkbox" class="other5_value" name="other5_value" value="<?=$offerArr[plus_other5_value]?>" <?if($editarr[plus_other5_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other5_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other6_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other6_name]?> felár</td>
				<td> <input type="checkbox" class="other6_value" name="other6_value" value="<?=$offerArr[plus_other6_value]?>" <?if($editarr[plus_other6_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other6_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_single1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single1_name]?> felár</td>
				<td> <input type="checkbox" class="plus_single1_value" name="plus_single1_value" value="<?=$offerArr[plus_single1_value]?>" <?if($editarr[plus_single1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single1_value])?>/csomag</td>
			</tr>
<?}?>
<?if($offerArr[plus_single2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single2_name]?> felár</td>
				<td> <input type="checkbox" class="plus_single2_value" name="plus_single2_value" value="<?=$offerArr[plus_single2_value]?>" <?if($editarr[plus_single2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single2_value])?>/csomag</td>
			</tr>
<?}?>
<?if($offerArr[plus_single3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single3_name]?> felár</td>
				<td> <input type="checkbox" class="plus_single3_value" name="plus_single3_value" value="<?=$offerArr[plus_single3_value]?>" <?if($editarr[plus_single3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single3_value])?>/csomag</td>
			</tr>
<?}?>


<?if( $offerArr[days]>0) {?>

<tr>
				<td class='lablerow'>Maradjon még egy éjszakát</td>
				<td><input type="hidden" name="plus_day_temp" class="plus_day_temp" value="<?=$offerArr[actual_price]/$offerArr[days]?>"  /><input type="checkbox" name="plus_days" class="plus_days" value="<?=$offerArr[actual_price]/$offerArr[days]?>" <?if($editarr[plus_days]>0)echo"checked"?> <?=$disabled?>/><label><span id="plusDays"><? if($CURUSER[userclass] > 50) echo "+ ".formatPrice($offerArr[actual_price]/$offerArr[days])?></span></td>
			</tr>
<?
}

}


if($editarr[invoice_created] <> 1) { 

if($CURUSER[userclass] < 50 && $editarr[paid] == 1)
{
?>
	<tr><td colspan='2'><center><b>Az vásárló fizetés után már nem módosítható!</b></center></td></tr>
<?
}
else {

	if($editarr[pid] > 0) { 
?>
	<tr><td colspan='2' align='center'><input type="submit" value="Mentés" id="customersubmit"/></td></tr>
<?
	} else { ?>

	<tr><td colspan='2' align='center'><input type="submit" value="Mentés és felárak hozzáadása &raquo;" id="customersubmit"/></td></tr>
	
	<?
	}
}
 }
 elseif($editarr[invoice_created] == 1 && $CURUSER[userclass] > 50) { ?>
	<tr><td colspan='2' align='center'><input type="submit" value="Mentés" id="customersubmit"/></td></tr>

 <? } ?>
	</table>




<? /* ?>
	<ul>
		<?if($editarr[offer_id] =="") {?>
			<li><label>Azonosító:</label><?=$string?></li>
		<?}	else {?>
			<li><label>Azonosító:</label><?=$editarr[offer_id]?></li>
		<?}?>
		
		<? if($CURUSER[userclass] > 50) { ?>
		<li>
		<select name="offers_id">
				<?
				
				if($editarr[cid] == '')
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW()  ORDER BY partners.hotel_name ASC");
				}
				else
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id ORDER BY partners.hotel_name ASC");
				}
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name] - ".formatPrice($partnerArr[actual_price])."</option>";
					}	
				?>
			</select>
		</li>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] > 0) { 

			$partnerArr = mysql_fetch_assoc($mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.id = $editarr[offers_id] LIMIT 1"));

		?>	
			<li><label>Ajánlat:</label><? echo "$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name]" ?></li>
			<input type="hidden" name="offers_id" value="<?=$editarr[offers_id]?>"/>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] == 0) { ?>
			<select name="offers_id">
				<?
				
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW() AND reseller_enabled = 1  ORDER BY partners.hotel_name ASC");
			
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name] - ".formatPrice($partnerArr[actual_price])."</option>";
					}	
				?>
			</select>
		</li>
		
		<? } ?>
		<? 	if($editarr[cid] <> '' && $CURUSER[userclass] > 50)
			{?>
		<li>
		<select name="sub_pid">
			<option value=''>Válasszon</option>
				<?
				
				$partnerQuery = $mysql->query("SELECT * FROM partners WHERE (country = '' or country = 'hu') AND hotel_name <> '' ORDER BY partners.hotel_name ASC");
				
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[pid] == $editarr[sub_pid] || $partnerArr[pid] == $formData[sub_pid])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name]</option>";
					}
				
				?>
			</select>
		</li>
		<? } else { ?>
			<input type="hidden" name="sub_pid" value="<?=$editarr[sub_pid]?>"/>
		<? } ?>
		<li><label>Vevő neve*:</label><input type="text" name="name" value="<?=$editarr[name]?>"/></li>
		<li><label>Cím:</label><input type="text" name="zip" value="<?=$editarr[zip]?>" style='width:35px;' class='numeric'/><input type="text" name="city" style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" name="address" class="small" style='width:295px;' value="<?=$editarr[address]?>" /></li>
		
		
		<li id='invoicebutton'  <? if($editarr[invoice_name] <> '') echo "style='display:none'"?>><div style='text-align:center; margin:5px; padding:5px; border-top:1px solid #c4c4c4; border-bottom:1px solid #c4c4c4;'><a href='#' id='showinvdata'><b>Eltérő számlázási adatok</b></a></div></li>
		
		<li class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><label>Számlázási név:</label><input type="text" name="invoice_name" value="<?=$editarr[invoice_name]?>"/></li>
		<li class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><label>Számlázási Cím:</label><input type="text" name="invoice_zip"value="<?=$editarr[invoice_zip]?>" style='width:35px;'/><input type="text" style='width:140px;margin:0 1px 0 1px' class="small1" value="<?=$editarr[invoice_city]?>" name="invoice_city"/><input type="text" name="invoice_address" class="small" style='width:295px;' value="<?=$editarr[invoice_address]?>" /></li>
		
		
		<li><label>Telefonszám*:</label><input type="text" name="phone" value="<?=$editarr[phone]?>"/></li>
		<li><label>E-mail cím*:</label><input type="text" name="email" value="<?=$editarr[email]?>"/></li>

		<? if($CURUSER[userclass] > 50) { ?>
		<li><label>Licit:</label><input type="text" name="bid_name" value="<?=$editarr[bid_name]?><?=$formData[bid_name]?>"/></li>
		<li><label>Licit típus:</label>
			<select name="type">
				<option value="4" <?if($editarr[type]==4)echo"selected";?>>Outlet</option>
				<option value="1" <?if($editarr[type]==1)echo"selected";?>>Vatera</option>
				<option value="2" <?if($editarr[type]==2)echo"selected";?>>Teszvesz</option>
				<option value="6" <?if($editarr[type]==6)echo"selected";?>>Lealkudtuk</option>
				<option value="7" <?if($editarr[type]==7)echo"selected";?>>Grando</option>
				<option value="3" <?if($editarr[type]==3)echo"selected";?>>Licittravel</option>
				<option value="5" <?if($editarr[type]==5)echo"selected";?>>Pult</option>
				<option value="8" <?if($editarr[type]==8)echo"selected";?>>Viszonteladó</option>
			</select>
		</li>
		<li><label>Eladási ár:</label><input type="text" name="orig_price" value="<?=$editarr[orig_price]?><?=$formData[orig_price]?>" style='width:50px;' class='numeric'/> Ft</li>
		<!--<li><label>Promóciós kód:</label>
			<select name="promotion_code">
				<option value=''>Válasszon</option>
				<?
					$partnerQuery = $mysql->query("SELECT * FROM promotion ORDER BY name ASC");
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[code] == $editarr[promotion_code])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[code]\" $selected>$partnerArr[name]</option>";
					}
				
				?>
			</select>
		</li>-->
		<li><label>Fizetés módja:</label>
			<select name="payment">
				<option value="2" <?if($editarr[payment]==2)echo"selected";?>>Készpénz</option>
				<option value="1" <?if($editarr[payment]==1)echo"selected";?>>Átutalás</option>
				<option value="3" <?if($editarr[payment]==3)echo"selected";?>>Utánvét</option>
				<option value="9" <?if($editarr[payment]==9)echo"selected";?>>Bankkártya</option>
				<option value="4" <?if($editarr[payment]==4)echo"selected";?>>Futár</option>
				<option value="5" <?if($editarr[payment]==5)echo"selected";?>>Üdülési csekk</option>
				<option value="6" <?if($editarr[payment]==6)echo"selected";?>>Helyszinen</option>
				<option value="7" <?if($editarr[payment]==7)echo"selected";?>>Online</option>
				<option value="8" <?if($editarr[payment]==8)echo"selected";?>>Facebook</option>
				<option value="10" <?if($editarr[payment]==10)echo"selected";?>>OTP SZÉP kártya</option>
				<option value="11" <?if($editarr[payment]==11)echo"selected";?>>MKB SZÉP kártya</option>
				<option value="12" <?if($editarr[payment]==12)echo"selected";?>>K&H SZÉP kártya</option>


			</select>
		</li>
		<li><label>Szállítás módja:</label>
			<select name="shipment">
				<option value="0">Válassza ki az átvételi módot</option>
				<option value="2" <?if($editarr[shipment]==2)echo"selected";?>>Online kérem</option>
				<option value="3" <?if($editarr[shipment]==3)echo"selected";?>>Személyes átvétel</option>
				<option value="4" <?if($editarr[shipment]==4)echo"selected";?>>Ajánlott levélként: +395 Ft</option>
				<option value="5" <?if($editarr[shipment]==5)echo"selected";?>>Postai utánvéttel: +1 450 Ft</option>
			</select>

		</li>
		<li><label>Postaköltség:</label><input type="text" name="post_fee" value="<?=$editarr[post_fee]?><?=$formData[post_fee]?>" style='width:50px;' class='numeric'/> Ft</li>
		<? } ?>
		<!--<li><label>Üdülésicsekk info:</label><textarea name="checks"><?=$editarr[checks]?></textarea></li>-->
		<li><label>Üdülésicsekkek értéke:</label><input type="text" name="check_value" value="<?=$editarr[check_value]?>" style='width:50px' class='numeric'/> Ft</li>
		<li><label>Üdülésicsekkek száma:</label><input type="text" name="check_count" value="<?=$editarr[check_count]?>"  style='width:50px' class='numeric'/> darab</li>

		<li><label>Megjegyzés:</label><textarea name="comment"><?=$editarr[comment]?></textarea></li>
		<li><label>Hotel megjegyzés:</label><textarea name="hotel_comment"><?=$editarr[hotel_comment]?></textarea></li>
		
		<? if($CURUSER[userclass] > 50) { ?>
		<li><label>Érvényesség:</label><textarea name="validity"><?=$editarr[validity]?><?=$formData[validity]?></textarea></li>
		<li><label>Nem tartalmazza:</label><input type="text" name="not_include" value="<?=$editarr[not_include]?><?=$formData[not_include]?>"/></li>
		<? } ?>
		
		<li><label>Ár elrejtése?</label>
		<select name='gift'>
			<option value='0'>Nem</option>
			<option value='1' <?if($editarr[gift]==1)echo"selected";?>>Igen</option>
			</select>
		</li>
		
		
		<? if($CURUSER[userclass] > 50) { ?>
			
			<li><label>Email ID:</label><input type="text" name="email_id" value="<?=$editarr[email_id]?>" class='numeric' style='width:50px;'/></li>
			<li><label>Számlaszám:</label><input type="text" name="user_invoice_number" value="<?=$editarr[user_invoice_number]?>"/></li>
			<li><label>Számla dátuma:</label><input type="text" name="user_invoice_date" value="<?=$editarr[user_invoice_date]?>"/></li>
			<li><label>Számla elkezdve:</label><input type="text" name="user_invoice_started" value="<?=$editarr[user_invoice_started]?>"/></li>
		<? } ?>

		<?if($editarr[inactive] == 1 && $CURUSER[userclass] > 50){?>
		<li><label>Törölt:</label>
			<select name="inactive">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[inactive]==1)echo"selected";?>>Igen</option>
			</select>
		</li>
		<?
		}
		if($CURUSER["userclass"] == 255)
		{
		?>		
		<li><label>Fizetve:</label>
			<select name="paid">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[paid]==1)echo"selected";?>>Igen</option>
			</select>
		</li>			

		<? } ?>
		<hr/>
	
	<?
	if($editarr[offer_id] <>"") {
		$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $editarr[offers_id]");
		$offerArr = mysql_fetch_assoc($offerQr);
		
		if($CURUSER[userclass] == 5 || $editarr[type] == 8)
			$offerArr[actual_price] = $offerArr[reseller_price];
		else	
			$offerArr[actual_price] = $offerArr[actual_price];
	?>
	<input type="hidden" name="days" value="<?=$offerArr[days]?>"/>
	<input type="hidden" name="actual_price" value="<?=$offerArr[actual_price]?>"/>


	<li><label>Ajánlat alapára:</label> <?=formatPrice($offerArr[actual_price]);?> </li>
	<li><label>Éjszakák száma:</label> <?=$offerArr[days]?> éj </li>

	<?
		if($offerArr[plus_half_board]<>0) {?>
	<li><label>Félpanzió</label><input type="checkbox" name="plus_half_board" class="plus_half_board" value="<?=$offerArr[plus_half_board]?>" <?if($editarr[plus_half_board]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_half_board])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_weekend_plus]<>0) {?>
<li><label>Hétvégi felár</label><input type="checkbox" name="plus_weekend_plus" class="plus_weekend_plus" value="<?=$offerArr[plus_weekend_plus]?>" <?if($editarr[plus_weekend_plus]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_weekend_plus]*2)?></label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_bed]<>0) {?>
<li><label>Pótágy felár</label><input type="checkbox" name="plus_bed" class="plus_bed" value="<?=$offerArr[plus_bed]?>" <?if($editarr[plus_bed]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_bed_plus_food]<>0) {?>
<li><label>Félpanzió + pótágy felár</label><input type="checkbox" name="plus_bed_plus_food" class="plus_bed_plus_food" value="<?=$offerArr[plus_bed_plus_food]?>"  <?if($editarr[plus_bed_plus_food]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed_plus_food])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>1. gyermek kora:</label> <input type="hidden" name="prev_child1" value="0" class="prev_child1" <?=$disabled?>/>
	<select name="child1_value" class="child1_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>"  <? if($editarr[plus_child1_value]==1 || ($editarr[plus_child1_value] >1 && $editarr[plus_child1_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	<div class='cleaner'></div>
</li>
<?}
else
{
?>
<input type='hidden' name='child1_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>2. gyermek kora: </label> <input type="hidden" name="prev_child2" value="0" class="prev_child2"/>
	<select name="child2_value" class="child2_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child2_value]==1 || ($editarr[plus_child2_value] >1 && $editarr[plus_child2_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	<div class='cleaner'></div>
</li>
<?}
else
{
?>
<input type='hidden' name='child2_value' value='999'/>
<?
}
?>
<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>3. gyermek: </label>	
	<select name="child3_value" class="child3_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child3_value]==1 || ($editarr[plus_child3_value] >1 && $editarr[plus_child3_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig </option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett</option>
	</select>
<div class='cleaner'></div></li>
<?}
else
{
?>
<input type='hidden' name='child3_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_room1_value]<>0) {?>
<li><label><?=$offerArr[plus_room1_name]?> szoba felár</label> <input type="checkbox" class="room1_value" name="room1_value" value="<?=$offerArr[plus_room1_value]?>" <?if($editarr[plus_room1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room1_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_room2_value]<>0) {?>
<li><label><?=$offerArr[plus_room2_name]?> szoba felár</label> <input type="checkbox" class="room2_value" name="room2_value" value="<?=$offerArr[plus_room2_value]?>" <?if($editarr[plus_room2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room2_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_room3_value]<>0) {?>
<li><label><?=$offerArr[plus_room3_name]?> szoba felár</label> <input type="checkbox" class="room3_value" name="room3_value" value="<?=$offerArr[plus_room3_value]?>" <?if($editarr[plus_room3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room3_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other1_value]<>0) {?>
<li><label><?=$offerArr[plus_other1_name]?> felár</label> <input type="checkbox" class="other1_value" name="other1_value" value="<?=$offerArr[plus_other1_value]?>" <?if($editarr[plus_other1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other1_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other2_value]<>0) {?>
<li><label><?=$offerArr[plus_other2_name]?> felár</label> <input type="checkbox"  class="other2_value" name="other2_value" value="<?=$offerArr[plus_other2_value]?>" <?if($editarr[plus_other2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other2_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other3_value]<>0) {?>
<li><label><?=$offerArr[plus_other3_name]?> felár</label> <input type="checkbox" class="other3_value" name="other3_value" value="<?=$offerArr[plus_other3_value]?>" <?if($editarr[plus_other3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other3_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other4_value]<>0) {?>
<li><label><?=$offerArr[plus_other4_name]?> felár</label> <input type="checkbox" class="other4_value" name="other4_value" value="<?=$offerArr[plus_other4_value]?>" <?if($editarr[plus_other4_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other4_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other5_value]<>0) {?>
<li><label><?=$offerArr[plus_other5_name]?> felár</label> <input type="checkbox" class="other5_value" name="other5_value" value="<?=$offerArr[plus_other5_value]?>" <?if($editarr[plus_other5_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other5_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other6_value]<>0) {?>
<li><label><?=$offerArr[plus_other6_name]?> felár</label> <input type="checkbox" class="other6_value" name="other6_value" value="<?=$offerArr[plus_other6_value]?>" <?if($editarr[plus_other6_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other6_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_single1_value]<>0) {?>
<li><label><?=$offerArr[plus_single1_name]?> felár</label> <input type="checkbox" class="plus_single1_value" name="plus_single1_value" value="<?=$offerArr[plus_single1_value]?>" <?if($editarr[plus_single1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single1_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>
<?if($offerArr[plus_single2_value]<>0) {?>
<li><label><?=$offerArr[plus_single2_name]?> felár</label> <input type="checkbox" class="plus_single2_value" name="plus_single2_value" value="<?=$offerArr[plus_single2_value]?>" <?if($editarr[plus_single2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single2_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>
<?if($offerArr[plus_single3_value]<>0) {?>
<li><label><?=$offerArr[plus_single3_name]?> felár</label> <input type="checkbox" class="plus_single3_value" name="plus_single3_value" value="<?=$offerArr[plus_single3_value]?>" <?if($editarr[plus_single3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single3_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>


<?if( $offerArr[days]>0) {?>

<li><label>Maradjon még egy éjszakát</label><input type="hidden" name="plus_day_temp" class="plus_day_temp" value="<?=$offerArr[actual_price]/$offerArr[days]?>"  /><input type="checkbox" name="plus_days" class="plus_days" value="<?=$offerArr[actual_price]/$offerArr[days]?>" <?if($editarr[plus_days]>0)echo"checked"?> <?=$disabled?>/><label><span id="plusDays"><!--+ <?=formatPrice($offerArr[actual_price]/$offerArr[days])?>--></span></label><div class='cleaner'></div></li>
<?
}

}


if($editarr[invoice_created] <> 1) { 

if($CURUSER[userclass] < 50 && $editarr[paid] == 1)
{
?>
	<li><center><b>Az vásárló fizetés után már nem módosítható!</b></center></li>
<?
}
else {
?>
	<li><input type="submit" value="Mentés" /></li>
<?
}
 } ?>
<? */ ?>


	</fieldset>
	</form>
</div>

</div></div>
<?
foot();
die;
}


echo "<center><select id='monthChange'>";
$listerQuery = $mysql->query("SELECT MONTH(added) AS listerMonth, YEAR(added) AS listerYear FROM customers WHERE cid > 0 AND inactive = 0 $extraSelect GROUP BY YEAR(added),MONTH(added)");

	echo "<option value='0'>$lang[select]</option>";
while($listerArr = mysql_fetch_assoc($listerQuery)) {
	
	if($listerArr[listerMonth] == $month && $listerArr[listerYear] == $year) {
		$bold = "selected";
	}
	else
		$bold = "";
	echo "<option value='year=$listerArr[listerYear]&month=$listerArr[listerMonth]&type=$filter' $bold>$listerArr[listerYear]. ".$lang['months'][$listerArr[listerMonth]]."</option>";
}
echo "</select></center>";
echo "<hr/>";


if($filter <> 'white' &&  $filter <> 'green' && $filter <> 'red' && $filter <> 'blue' && $filter <> 'yellow' && $filter <> '' && $filter <> 'grey' && $filter <> 'orange')// || 
{
	die(":)");
}
if($filter == "white" || $filter == '')
	$filterSelect = "AND paid = 0 AND voucher = 0 AND name <> 'Pult'";
if($filter == "green")
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created <> 1";
if($filter == "blue")
	$filterSelect = "AND paid = 1 AND voucher = 0";
if($filter == "red")
	$filterSelect = "AND paid = 0 AND voucher = 1 AND type <> 5";
if($filter == "yellow")
	$filterSelect = "AND paid = 0 AND type = 5";
if($filter == "grey" && $CURUSER[userclass] <> 5)
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created = 1";
if($filter == "grey" && $CURUSER[userclass] == 5)
	$filterSelect = "AND reseller_invoice <> ''";
if($filter == "orange")
	$filterSelect = "AND paid = 0 AND voucher = 0 AND payment = 3";
	
if($CURUSER[disable_group_customers] == 1 && $filter == '')
{
	$filterSelect = '';
}	
//pagination 
$pg = (int)$_GET[page];

 $pq = "SELECT cid FROM customers WHERE MONTH(added)=$month AND YEAR(added)=$year AND inactive = 0 $filterSelect $extraSelect  ORDER BY added DESC";
 $pq = mysql_query($pq);
 $rows = mysql_num_rows($pq);
 $perpage = 25; 
 $last = ceil($rows/$perpage);
 if($pg == '' && $pg == 0)
 {
 	$qrStart = 0;
 }
 elseif($pg == 1)
 {
 	$qrStart = 1;
 }
 else
 {
 	$qrStart = ($pg-1)*$perpage-1;
 	
  }
  
  //$qrStart = 0;
  //$perpage = 25; 
//pagination

//if searchquery is not empty
if($s <> '')
{

	if($CURUSER[userclass] > 50)
	{
		$ownqr = mysql_fetch_assoc($mysql->query("SELECT * FROM own_vouchers WHERE id > 0  AND (name LIKE '%$s%' OR email like '%$s%') AND paid = 0"));

		if($ownqr[name] <> '')
		{
			echo message("<a href='/own_vouchers.php'>Vásárló a saját eladások között ($ownqr[name]) &raquo;</a>");
		}
	}


	if($_POST[showinactive] == 'on')
	{
		$showdeleted = 'inactive = 1';
	}
	else
	{
		$showdeleted = 'inactive = 0';
	}
	$qr = "SELECT * FROM customers WHERE cid > 0 AND $showdeleted  $extraSelect AND (offer_id like '%$s%' OR user_invoice_number like '%$s%' OR name like '%$s%' OR invoice_name like '%$s%' OR comment like '%$s%' OR bidder_name like '%$s%' OR email like '%$s%' ) ORDER BY added DESC LIMIT $qrStart, $perpage";
}
else
	$qr = "SELECT * FROM customers WHERE MONTH(added)=$month AND YEAR(added)=$year AND inactive = 0 $filterSelect $extraSelect  ORDER BY added DESC LIMIT $qrStart, $perpage";
	
$query = mysql_query($qr);


if(mysql_num_rows($query) <= 0)
{
	echo "<div class='redmessage'>$lang[no_results]</div>";
	foot();
	die;
}	

?>
<? if($CURUSER[userclass] >= 5) { ?>

<div id='sumtotal'>
	<form>
	<table style='width:250px'>
		<tr>
			<td colspan='1'>Fizetendő:</td>
			<td colspan='1' id='sumtvalue' align='right'></td>
		</tr>
		<tr>
			<td>Készpénz:</td>
			<td align='right'><input type='text' id='ptotal' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td>ÜCS/SZÉP</td>
			<td align='right'><input type='text' id='paidtotal' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td>Visszajáró:</td>
			<td id='chtotal' align='right' style='font-size:16px; font-weight:bold;'></td>
		</tr>
		<tr>
			<td id='numbers' colspan='2' align='center'></td>
		</tr>

	</table>
	</form>
	
</div>


<? } ?>
<?
echo "<table class=\"general\" width='100%'>";
echo "<tr class=\"header\">";

	//if($CURUSER[userclass] <> 5) {
		echo "<td width='20'></td>";
	//}
	
	if($CURUSER[userclass] > 50)
		echo "<td width='20'></td>";
	
	echo "<td width='85'>$lang[id]</td>";
	
	if($CURUSER[userclass] >= 5) {
		echo "<td width='70'>$lang[date]</td>";
	}

	echo "<td width='200'>$lang[customer_name]</td>";
	echo "<td>$lang[partner]</td>";
	echo "<td width='60'>$lang[price]</td>";
	echo "<td>$lang[payment]</td>";
	
	if($CURUSER[userclass] < 5)
		echo "<td>$lang[customer_left]</td>";
		
	if($CURUSER[userclass] >= 5) {
		echo "<td>$lang[paid]?</td>";
		echo "<td>$lang[tools]?</td>";
		
		if($CURUSER[userclass] > 50)
			echo "<td width='45'>$lang[post]</td>";
			
		echo "<td>$lang[bill]</td>";
		echo "<td colspan='2'>$lang[comment]</td>";
	}
echo "</tr>";
while($arr = mysql_fetch_assoc($query)) {

	if($arr[type] == 1) 
		$type = "Vatera";
	elseif($arr[type] == 2) 
		$type = "Teszvesz";
	elseif($arr[type] == 3) 
		$type = "Licittravel";
	elseif($arr[type] == 4) 
		$type = "Outlet";
	elseif($arr[type] == 5) 
		$type = "Pult";
	elseif($arr[type] == 6) 
		$type = "Lealkudtuk";
	elseif($arr[type] == 7) 
		$type = "Grando";
	elseif($arr[type] == 8) 
		$type = "Viszonteladó";
	
	
	//do not display postponed because of offers
	$offerQuery = $mysql->query("SELECT abroad FROM offers WHERE id = '$arr[offers_id]' LIMIT 1");
	$offerArr = mysql_fetch_assoc($offerQuery);
	

	if($arr[postpone] == 1 && $offerArr[abroad] <> 1 && $CURUSER[userclass] <> 5)
		$payment = $lang[t_check];
	elseif($arr[payment] == 1  || $arr[checkpaper_id] > 0) 
		$payment = $lang[transfer];
	elseif($arr[payment] == 2) 
		$payment = $lang[cash];
	elseif($arr[payment] == 3) 
		$payment = $lang[postpaid];
	elseif($arr[payment] == 4) 
		$payment = $lang[delivery];
	elseif($arr[payment] == 5) 
		$payment = $lang[t_check];
	elseif($arr[payment] == 6) 
		$payment = $lang[place];
	elseif($arr[payment] == 7) 
		$payment = $lang[online];
	elseif($arr[payment] == 8) 
		$payment = $lang[facebook];
	elseif($arr[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($arr[payment] == 10) 
		$payment = "OTP SZÉP kártya";
	elseif($arr[payment] == 11) 
		$payment = "MKB SZÉP kártya";
	elseif($arr[payment] == 12) 
		$payment = "K&H SZÉP kártya";
		
	if($arr[paid] == 0 && $arr[voucher] <> 1 && $arr[name] <> 'Pult') {
		$class = "";
		$paid = $lang[no];
	} 

	elseif($arr[voucher] == 0 && $arr[paid] == 1) {
		$class = "blue";
	}	
	elseif($arr[voucher] == 1 && $arr[paid] == 0) {
		$class = "red";
	}	
	elseif($arr[paid] == 1 && $arr[voucher] == 1 && $arr[invoice_created] <> 1) {
		$class = "green";
		$paid = $lang[yes];	
	}
	elseif($arr[invoice_created] == 1 && $arr[paid] == 1 && $arr[voucher] == 1){
		$class = "grey";
	}
	elseif($arr[type] == 5 && $arr[address] == '')
	{
		$class = "yellow";
	}
	
	if($arr[inactive] == 1)
		$class = 'purple';

	$partnerQuery = $mysql->query("SELECT company_name,hotel_name FROM partners WHERE pid = '$arr[pid]' LIMIT 1");
	$partnerArr = mysql_fetch_assoc($partnerQuery);

	
	$realPartnerArr = '';
	if($arr[sub_pid] > 0)
	{
		$realPartnerQuery = $mysql->query("SELECT company_name,hotel_name FROM partners WHERE pid = '$arr[sub_pid]' LIMIT 1");
		$realPartnerArr = mysql_fetch_assoc($realPartnerQuery);
	}
	if($CURUSER[userclass] >= 5) {
		echo "<form method=\"post\" action=\"customers.php\">";
		echo "<input type=\"hidden\" name=\"cid\" value=\"$arr[cid]\">";
		echo "<input type=\"hidden\" name=\"paidBill\" value=\"1\">";
		echo "<input type=\"hidden\" name=\"payment\" value=\"$arr[payment]\">";
		
		echo "<input type=\"hidden\" name=\"comment\" value=\"$arr[offer_id] voucher fizetve $arr[name] által\">";
		echo "<input type=\"hidden\" name=\"value\" value=\"$arr[orig_price]\">";
		echo "<input type=\"hidden\" name=\"email\" value=\"$arr[email]\">";

		echo "<input type=\"hidden\" name=\"postage\" value=\"$arr[post_fee]\">";

		echo "<input type=\"hidden\" name=\"voucher_id\" value=\"$arr[offer_id]\">";
		
		echo "<input type=\"hidden\" name=\"partner_id\" value=\"$arr[pid] voucher fizetve $arr[name] által\">";
		echo "<input type=\"hidden\" name=\"check_comment\" value=\"$arr[checks]\">";
	}
	echo "<tr class=\"$class\">";
	
	
	if($CURUSER["userclass"] >= 5) {
	

			if($arr[checked] == 1)
				$checked = "<a target='_blank' href=\"http://vasarlas.szallasoutlet.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/check1.png' alt='".$arr[checked_date]." @ ".$arr[checked_ip]."' title='".$arr[checked_date]." @ ".$arr[checked_ip]."' width='20'/></b></a>";
			else
				$checked = "<a target='_blank' href=\"http://vasarlas.szallasoutlet.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/cross1.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>";
				
			if($CURUSER[userclass] == 5)
				$checked = '';
				
			if($CURUSER[userclass] > 50)
				$createinvoice = "<a href=\"/info/manage_invoice.php?cid=$arr[cid]\" rel='facebox iframe'><b><img src='images/storno.png' alt='Számlázási funkciók' title='Számlázási funkciók' width='20'/></b></a>";
			else
				$creatinvoice = "";
				
			$editlink = "$checked
				<a href=\"?add=1&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>
				<a href=\"?delete=$arr[cid]&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/trash.png' alt='töröl' title='töröl' width='20'/></b></a>
				<a href=\"?copy=$arr[cid]&offer_id=$arr[offer_id]\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/copy.png' alt='másol' title='másol' width='20'/></b></a> 
				$createinvoice
				";

		echo "<td>$editlink</td>";
		
	
				
	}
	
	
		if($arr[company_invoice] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		else
			$logo = 'ologo_small.png';
			
			
	if($arr[pid] == 3003 || $arr[pid] == 3121)
		$tvcampaign = "<br/><img src='/images/film.png' width='20' alt='TV kampány utalvány' title='TV kampány utalvány'/>";
	else
		$tvcampaign = '';
			
	if($arr[gift] == 1 && $CURUSER[userclass] >5)
		$gift = "<br/><img src='/images/heart.png' width='20' alt='Az utalvány ajándék' title='Az utalvány ajándék'/>";
	else
		$gift = '';
				
	if($arr[postpone] == 1 && $CURUSER[userclass] > 50 && $arr[postpone_cleared] <> 1)
		$delay = "<br/><img src='/images/delayed.png' width='20' alt='$arr[postpone_date]' title='$arr[postpone_date]'/>";
	else
		$delay = '';


		if($CURUSER[userclass] <> 5)
			echo "<td><img src='/images/$logo'/>$tvcampaign $gift $delay</td>";
			
		echo "<td>".str_replace("SZO-","",$arr[offer_id])."</td>";
	
		if($CURUSER[userclass] >= 5) {
			echo "<td>".date('y.m.d. H:i',strtotime($arr[added]))."</td>";
		}

		if($arr[inactive] == 1)
			$deletetext = " [ TÖRÖLT ] ";
		else
			$deletetext = '';
			
		echo "<td><a href='info/customer.php?cid=$arr[cid]' rel='facebox'><b>".$deletetext."$arr[name]</b></a></td>";

	$extrapartner ='';
	
	if($arr[sub_pid] > 0)
		$extrapartner = "<br/>".$realPartnerArr[hotel_name];
	
	echo "<td><a href='/info/offer.php?id=$arr[offers_id]&type=offer' rel='facebox'><b>$partnerArr[hotel_name]$extrapartner</b></a></td>"; //preview
	
	echo "<td align='right'>".str_replace(" ","&nbsp;",formatPrice($arr[orig_price]))."</td>";
	
	
	if($CURUSER[userclass] == 5 && $arr[paid] == 0)
		echo "<td align='center'>-</td>";
	else
		echo "<td align='center'>$payment</td>";

	if($CURUSER[userclass] < 5)
	{
		echo "<td align='center'>";
		if($arr[customer_left] == '0000-00-00 00:00:00')
		{
			echo "<input type='button' value='$lang[customer_left]' class='alertbox' id='$arr[cid]'/>";
		}
		echo "</td>";
	}
	if($CURUSER["userclass"] >= 5) {
	
		if($arr[invoice_created] == 1) {
				$disabled = "disabled";
		}	
		elseif ($arr[paid] == 1) // && $arr[voucher] == 1
		{
			$disabled = "disabled";
		}
		else {
			$disabled = "";
		}
		echo "<td>";
			echo "<select  onchange=\"jQuery.facebox({ ajax: 'info/pay.php?cid=$arr[cid]&voucher=$arr[voucher]&company=$arr[company_invoice]&returnto=$_SERVER[REQUEST_URI]' });\" $disabled>";
			echo "<option value=\"0\">$lang[no]</option>";
			echo "<option value=\"1\"";
				if($arr[paid] == 1)
					echo "selected";
			echo ">$lang[yes]</option>";
			echo "</select>";
			echo "</td>";
	
			echo "<input type=\"hidden\" name='voucher' value=\"$arr[voucher]\"/>";
			if($arr[user_invoice_number] == '')
				$asklink = " [<a href=\"invoice/create_ask.php?cid=$arr[cid]&email=$arr[email]\" rel='facebox'>díjbekérő</a>]";
			else
				$asklink = '';
			
			if($CURUSER[userclass] < 50)
				$clearancelink = '';
			elseif($arr[paid] == 1 && ($arr[payment] == 2 || $arr[payment] == 5))
				$clearancelink = " [<a href=\"/vouchers/print_checkout.php?sure=1&type=&cid=$arr[offer_id]\">bizonylat</a>]";
			else
				$clearancelink = '';
				
			
		
			if($CURUSER[userclass] > 50)
				$notify = " [<a href=\"vouchers/sendletter.php?cid=$arr[cid]\" rel='facebox'>$lang[notify]</a>]";
			else
				$notify = "";
				
			if($arr[user_invoice_ask] <> '')
				$asklink = " [<a href=\"invoices/vatera/".str_replace("/","_",$arr[user_invoice_ask]).".pdf\" target='_blank'><b>díjbekérő</b></a>]";

				$print = "[<a href=\"vouchers/print.php?cid=$arr[cid]&paid=$arr[paid]\" rel='facebox'>$lang[print]</a>]<br/>
						 [<a href=\"vouchers/online.php?cid=$arr[cid]&email=$arr[email]&realID=$arr[offer_id]&paid=$arr[paid]\" rel='facebox'>$lang[o_online]</a>]
						$notify
						 $asklink
						 $clearancelink
						 ";
			
				echo "<td>$print</td>";
			
			$post_invoice ="";
			if($arr[post_invoice_number] <> "") {
				if($month <10) {
					$zeroMonth = "0".$month;
				}
				else
					$zeroMonth = $month;
				
				$post_invoice = "<a href=\"invoices/vatera/".str_replace("/","_",$arr[post_invoice_number]).".pdf\" target=\"_blank\">$arr[post_invoice_number]</a>";
				
			}
		
			if($CURUSER[userclass] > 50) 
				echo "<td align='right'>$arr[post_fee] Ft $post_invoice</td>";
				
			if($arr[user_invoice_number] <> "") {
				$date = explode("-",$arr[user_invoice_date]);
				$year = $date[0];
				$month = $date[1];
				
				if($CURUSER[userclass] > 50)
					$companyinvoice = "<a href=\"invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf\" target=\"_blank\"  class='nobr'>".str_replace("M2012/",'',$arr[invoice_number])."</a>";
				else
					$companyinvoice = "";
					
					echo "<td align='right'>
						<a href=\"invoices/vatera/".str_replace("/","_",$arr[user_invoice_number]).".pdf\" target=\"_blank\"  class='nobr'>".str_replace("M2012/",'',$arr[user_invoice_number])."</a> 
						<a href=\"invoices/vatera/".str_replace("/","_",$arr[user_final_invoice_number]).".pdf\" target=\"_blank\"  class='nobr'>".str_replace("M2012/",'',$arr[user_final_invoice_number])."</a> 
						$companyinvoice</td>";
			}
			else {
				echo "<td align='center'>-</td>"; //<a href=\"invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf\" target=\"_blank\" style='color:#c4c4c4' class='nobr'>".str_replace("M2012/",'',$arr[invoice_number])."</a>
			}
			
			echo "<td>$arr[comment]</td>";
			echo "<td  class='sumbox'><input type='checkbox'  class='sumvalue' value='$arr[orig_price]' printid='$arr[cid]'/></td>";
	}
	echo "</tr>";
	if($CURUSER["userclass"] >= 5) {
		echo "</form>";
	}
}
echo "</table>";

/* end of pagination */
if($s == '')
{
	echo "<hr/>";
	for($g=1;$g<=$last;$g++)
 	{
 	$e = $g*$perpage;
 	$st = $e-($perpage-1);
 	if($pg == $g)
 		$bold = "class='bold'";
 	elseif($pg == '' && $g==1)
 	{
 		$bold = "class='bold'";
 	}
	else
 		$bold = '';
 	echo "<a href=\"?year=$year&month=$month&type=$filter&page=$g\" $bold>$st - $e</a> &raquo; ";
}
/* end of pagination */
 }
?>
</div></div>
<?
foot();
?>