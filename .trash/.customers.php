<?
/*
 * customers.php 
 *
 * customers page
 *
*/
error_reporting(E_ERROR | E_PARSE);

/* bootstrap file */
include("inc/init.inc.php");
include("invoice/create_invoice_user.php");
//userlogin
userlogin();

/*
@header('Content-type: text/html; charset=utf-8');
include("inc/config.inc.php");
include("inc/mysql.class.php");
include("inc/functions.inc.php");
$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();
*/


if($_POST[hotel_comment] <> '' && $_POST[cid] > 0)
{
	if(validString($_POST[hotel_comment]) == true && validString($_POST[cid]) == true)
	{
	$msg = "A megjegyzést elmentettük!";
	
		if($CURUSER[userclass] < 10)
			$limit = "AND pid = $CURUSER[pid]";
			
		$mysql->query("UPDATE customers SET hotel_comment = '$_POST[hotel_comment]' WHERE cid = '$_POST[cid]' $limit");
		
		writelog("$CURUSER[username] added hotel comment for $_POST[cid]");
	}
}

$s = trim($_POST[search]);
if($s <> '')
{
	setcookie ("search", $s, time() + 3600);
}
$cookie = $_COOKIE[search];


if($CURUSER[userclass] < 100)
	$extraFilter = "AND (pid = '$CURUSER[pid]' or sub_pid = '$CURUSER[pid]')";
else
	$extraFilter = '';

$month = (int)$_GET[month];
$year = (int)$_GET[year];
$filter = $_GET[type];

if($month == "") {
	$month = date("n");
}
if($year == "") {
	$year = date("Y");
}

if($CURUSER["vatera"] <> 1 && $CURUSER[pid] <> 850) {
	$extraSelect = "AND (pid = '$CURUSER[pid]' or sub_pid = '$CURUSER[pid]') ";
} 
elseif ($CURUSER[pid] == 850)
{
	$extraSelect = "AND type = 5";
}
else
{

}
$add = $_GET[add];
$editid = (int)$_GET[editid];

$editid = (int)$_GET[editid];

$delete = (int)$_GET[delete];
$sure = $_GET[sure];

$editIDPost = (int)$_POST[editid];

$data[offer_id] = trim($_POST[offer_id]);
$data[offers_id] = trim($_POST[offers_id]);
$data[name] = trim($_POST[name]);
$data[phone] = trim($_POST[phone]);
$data[address] = trim($_POST[address]);
$data[zip] = trim($_POST[zip]);
$data[city] = trim($_POST[city]);
$data[email] = trim($_POST[email]);
$data[bid_id] = trim($_POST[bid_id]);

$data[invoice_name] = trim($_POST[invoice_name]);
$data[invoice_address] = trim($_POST[invoice_address]);
$data[invoice_zip] = trim($_POST[invoice_zip]);
$data[invoice_city] = trim($_POST[invoice_city]);

$data[sub_pid] = (int)$_POST[sub_pid];

$data[bid_name] = trim($_POST[bid_name]);
$data[validity] = trim($_POST[validity]);
$data[not_include] = trim($_POST[not_include]);

$data[payment] = trim($_POST[payment]);
$data[comment] = trim($_POST[comment]);
$data[hotel_comment] = trim($_POST[hotel_comment]);
$data[checks] = trim($_POST[checks]);

//$data[paid] = $_POST[paid];
$data[orig_price] = trim($_POST[orig_price]);
$data[type] = trim($_POST[type]);
$data[post_fee] = trim($_POST[post_fee]);

$data[check_count] = trim($_POST[check_count]);
$data[check_value] = trim($_POST[check_value]);


$data[email_id] = trim($_POST[email_id]);

$data[promotion_code] = trim($_POST[promotion_code]);

$data[gift] = trim($_POST[gift]);


if($delete > 0 && $sure <> 1) {
	die("<h2>$lang[delete_item]</h2> <br/> <a href=\"?delete=$delete&sure=1\" class='button green'>$lang[yes]!</a>  <a href=\"customers.php\" class='button red'>$lang[no]!</a><div class='cleaner'></div>");
}
if($_GET[copy] > 0 && $_GET[sure] <> 1)
{
	die("<h2>Biztosan másolni szeretné a <b><font color='red'>$_GET[offer_id]</font></b> sorszámú utalványt?</h1><br/>  <a href=\"?copy=$_GET[copy]&offer_id=$_GET[offer_id]&sure=1\" class='button green'>$lang[yes]</a> <a href=\"customers.php\" class='button red'>$lang[no]</a><div class='cleaner'></div>");
}
if($_GET[copy] > 0 && $_GET[sure] == 1)
{
	
	
	$copydata = $mysql->query("SELECT * FROM customers WHERE cid = $_GET[copy]");
	$copydata = mysql_fetch_assoc($copydata);
	
	if($copydata[company_invoice] == 'hoteloutlet')
		$companyname = 'hoteloutlet';
	else
		$companyname = '';
		
	$copydata[offer_id] = generateNumber($companyname);
	$copydata[added] = 'NOW()';
	$copydata[paid] = 0;
	$copydata[cid] = '';
	$copydata[paid_date] = '';
	$copydata[due_date] = '';
	$copydata[invoice_created] = '';
	$copydata[user_invoice_ask] = '';
	$copydata[invoice_number] = '';
	$copydata[invoice_date] = '';
	$copydata[voucher] = '';
	$copydata[inactive] = 0;
	$copydata[checked] = 0;
	$copydata[checked_date] = '';
	$copydata[checked_ip] = '';
	$copydata[checks] = 0;
	$copydata[checkout] = 0;
	$copydata[promotion_code] = '';
	
	$copydata[post_invoice_created] = '';
	$copydata[post_invoice_number] = '';
	
	$copydata[notification1] = '';
	$copydata[notification2] = '';
	$copydata[notification3] = '';
	$copydata[notification4] = '';
	$copydata[customer_left] = '';
	$copydata[reviewed] = '';
	$copydata[print_date] = '';
	$copydata[print_id] = '';
	
	$copydata[creditcard] = '';
	$copydata[check_count] = '';
	$copydata[check_value] = '';
	
	$copydata[user_invoice_number] = '';
	$copydata[user_invoice_date] = '';
	$copydata[user_invoice_started] = '';
	$copydata[check_arrival] = '';
	$copydata[checkpaper_id] = '';
	
	$copydata[invoice_cleared] = '';
	$copydata[user_storno_invoice_number] = '';
	$copydata[user_storno_invoice_date] = '';
	
	$mysql->query_insert("customers",$copydata);
	writelog("$CURUSER[username] copied VTL_ID: $_GET[offer_id] to $copydata[offer_id]");
	
	
	$copy_id = mysql_insert_id();
	$msg = "Sikeres másolás $copy_id";
	
	//header("location: customers.php?add=1&editid=23158");


}
if($delete > 0 && $sure == 1) {
	writelog("$CURUSER[username] deleted VTL_ID: $delete");
	mysql_query("update customers SET inactive = 1, inactive_date = NOW() where cid = $delete") or die(mysql_error());
	
	head($lang[manage_customers]);
	echo message("<a href='customers.php'>$lang[deleted]</a> <br/> <a href='customers.php' class='button grey' style='margin:10px 0 0 115px;'>Vissza!</a><div class='cleaner'></div>");
	foot();
	die();
}
if($editid >0) {
	$customerQuery = $mysql->query("SELECT * FROM customers WHERE cid = $editid $extraSelect");
	$editarr = mysql_fetch_assoc($customerQuery);
}
if($editIDPost == "" && $data[name] <> "") {

	$data[added] = 'NOW()';

	$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $data[offers_id]");
	$offerArr = mysql_fetch_assoc($offerQr);
	
	if($offerArr[company_invoice] == 'hoteloutlet')
	{
		$data[offer_id] = str_replace("VTL-","SZO-",trim($_POST[offer_id]));
		$data[company_invoice] = $offerArr[company_invoice];
	}
	else
	{
		$data[offer_id] = trim($_POST[offer_id]);
		$data[company_invoice] = $offerArr[company_invoice];
	}
 
	
	$data[validity] = trim($offerArr[validity]);
	$data[not_include] = trim($offerArr[not_include]);
	$data[pid] = trim($offerArr[partner_id]);
	$data[bid_name] = trim($offerArr[name]);
	$data[sub_pid] = trim($offerArr[sub_partner_id]);
	
	
	if(trim($_POST[promotion_code]) <> '')
		$data[orig_price] = trim($_POST[orig_price]);
	else
		$data[orig_price] = trim($offerArr[actual_price]);

	
	$data[hash] = md5($data[name].$data[offer_id].'4fAFVE4f_aranymetszes_994');


	if($data[type] == 5)
	{
	/* inactive part 
		for($z=1;$z<=20;$z++)
		{
			$data[offer_id] = generateNumber();
			$data[name] = "Pult";
			$mysql->query_insert("customers",$data);
			$msg = "Sikeresen létrehozott 20 Pult vouchert! <a href='vouchers/print.php?cid=pult'>Nyomtatás</a>";
		}
	*/
	}
	else {
	
	
	
		$data[hash] = md5($data[name].$data[offer_id].'4fAFVE4f_aranymetszes_994');
		
		$mysql->query_insert("customers",$data);
		$insertID = mysql_insert_id();
		
		/****/
		$message = "Köszönjük hogy nálunk vásárolt.<br/><br/>
			 Szánjon 1 percet az alábbi oldalra, mert rengeteg plusz munkától és<br/>
			 telefonálgatástól kímélheti meg magát.:<br/><br/>
			<a href=\"http://vasarlas.szallasoutlet.hu/customers/$data[hash]/$insertID\">http://vasarlas.szallasoutlet.hu/customers/$i$data[hash]/$insertID</a><br/><br/>
			Az utalvány sorszáma: $data[offer_id]";
		
		sendEmail("Indul6unk: vásárlás véglegesítése",$message,$data[email],"$data[name]");
		
		$msg = $lang[customer_added];

	}
		
	setcookie("customerForm", serialize($data), time()+3600*24*10);
		
	}
elseif($editIDPost > 0 && $data[name] <> "") {


	$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $data[offers_id]");
	$offerArr = mysql_fetch_assoc($offerQr);
	
	$item = mysql_fetch_assoc($mysql->query("SELECT orig_price, offer_id FROM customers WHERE cid = $editIDPost LIMIT 1"));

	
	$prevorigprice = $item[orig_price];

/* update offer data */
$data[shipment]  = (int)$_REQUEST[shipment];
	
	$data[plus_half_board]  = (int)$_REQUEST[plus_half_board];
	$data[plus_weekend_plus]  = (int)$_REQUEST[plus_weekend_plus];
	$data[plus_bed]  = (int)$_REQUEST[plus_bed];
	$data[plus_bed_plus_food]  = (int)$_REQUEST[plus_bed_plus_food];
	$data[plus_child1_value]  = (int)$_REQUEST[child1_value];
	
	$data[inactive] = $_POST[inactive];
	
		if($data[plus_child1_value] == 999)
			$data[plus_child1_value] = 0;
		elseif($data[plus_child1_value] == 0)
			$data[plus_child1_value] = 1;
		else
			$data[plus_child1_value] = (int)$_REQUEST[child1_value];
	
	$data[plus_child2_value]  = (int)$_REQUEST[child2_value];
		if($data[plus_child2_value] == 999)
			$data[plus_child2_value] = 0;
		elseif($data[plus_child2_value] == 0)
			$data[plus_child2_value] = 1;
		else
			$data[plus_child2_value] = (int)$_REQUEST[child2_value];
			
			
			
	$data[plus_child3_value]  = (int)$_REQUEST[child3_value];
		if($data[plus_child3_value] == 999)
			$data[plus_child3_value] = 0;
		elseif($data[plus_child3_value] == 0)
			$data[plus_child3_value] = 1;
		else
			$data[plus_child3_value] = (int)$_REQUEST[child3_value];
			
	$data[plus_room1_value]  = (int)$_REQUEST[room1_value];
	$data[plus_room2_value]  = (int)$_REQUEST[room2_value];
	$data[plus_room3_value]  = (int)$_REQUEST[room3_value];
	$data[plus_other1_value]  = (int)$_REQUEST[other1_value];
	$data[plus_other2_value]  = (int)$_REQUEST[other2_value];
	$data[plus_other3_value]  = (int)$_REQUEST[other3_value];
	$data[plus_other4_value]  = (int)$_REQUEST[other4_value];
	$data[plus_other5_value]  = (int)$_REQUEST[other5_value];
	$data[plus_other6_value]  = (int)$_REQUEST[other6_value];
	
	
	$data[plus_single1_value]  = (int)$_REQUEST[plus_single1_value];
	$data[plus_single2_value]  = (int)$_REQUEST[plus_single2_value];
	$data[plus_single3_value]  = (int)$_REQUEST[plus_single3_value];
	
	$data[plus_days]  = (int)$_REQUEST[plus_days];
	
	$data[actual_price]  = (int)$offerArr[actual_price];
	
	
	if(trim($_POST[promotion_code]) <> '')
		$data[actual_price] = trim($_POST[orig_price]);
	else
		$data[actual_price] = trim($offerArr[actual_price]);



	$dayNum = (int)$_REQUEST[days];
	
	if($data[plus_days] > 0)
	{
	$data[plus_days] = 
		 	round($data[actual_price]/$dayNum) + 
			 $data[plus_half_board]*1 +
			 $data[plus_bed]*1 +
			 $data[plus_bed_plus_food]*1 +
			 $data[plus_child1_value] *1 +
			 $data[plus_child2_value] *1 +
			 $data[plus_child3_value] *1 +
			 $data[plus_room1_value]*1 +
			 $data[plus_room2_value]*1 +
			 $data[plus_room3_value]*1 +
			 $data[plus_other1_value]*1 +
			 $data[plus_other2_value]*1 +
			 $data[plus_other3_value]*1 +
			 $data[plus_other4_value]*1 +
			 $data[plus_other5_value]*1 +
			 $data[plus_other6_value]*1;
	}
	
	$totalPrice =
			 $data[actual_price] + 
			 $data[plus_half_board]*$dayNum +
			 $data[plus_weekend_plus] *2 +
			 $data[plus_bed]*$dayNum +
			 $data[plus_bed_plus_food]*$dayNum +
			 $data[plus_child1_value] *$dayNum +
			 $data[plus_child2_value] *$dayNum +
			 $data[plus_child3_value] *$dayNum +
			 $data[plus_room1_value]*$dayNum +
			 $data[plus_room2_value]*$dayNum +
			 $data[plus_room3_value]*$dayNum +
			 $data[plus_other1_value]*$dayNum +
			 $data[plus_other2_value]*$dayNum +
			 $data[plus_other3_value]*$dayNum +
			 $data[plus_other4_value]*$dayNum +
			 $data[plus_other5_value]*$dayNum +
			 $data[plus_other6_value]*$dayNum +
		  	 $data[plus_single1_value] +
		  	 $data[plus_single2_value] +
		  	 $data[plus_single3_value] +
		  	 
			 $data[plus_days];

	$data[orig_price] = round($totalPrice,-1);
/*update offer data */

	if($prevorigprice <> $data[orig_price])
	{
		$data[extra_price] = abs($prevorigprice-$data[orig_price]);
		$data[extra_price_generated] = 1;
		writelog("$CURUSER[username] added extra service VTL_ID: $voucherid[offer_id]");

	}
	//remove
	$data[user_invoice_number] = trim($_POST[user_invoice_number]);
	$data[user_invoice_date] = trim($_POST[user_invoice_date]);
	$data[user_invoice_started] = trim($_POST[user_invoice_started]);
	
	
	$data[validity] = trim($offerArr[validity]);
	$data[not_include] = trim($offerArr[not_include]);
	$data[pid] = trim($offerArr[partner_id]);
	$data[bid_name] = trim($offerArr[name]);
	
	if($CURUSER[userclass] == 255)
	{
		$data[paid] = $_REQUEST[paid];
		writelog("$CURUSER[username] set paid = $data[paid] at VTL_ID: $item[offer_id]");
		
	}
//	$data[orig_price] = trim($offerArr[actual_price]);
	
	$mysql->query_update("customers",$data,"cid=$editIDPost");
	
	$voucherid = mysql_fetch_assoc($mysql->query("SELECT offer_id FROM customers WHERE cid = $editIDPost"));
	writelog("$CURUSER[username] edited VTL_ID: $voucherid[offer_id]");
	
	$msg = $lang[customer_edited];
}

head("Vásárlók kezelése");
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					<? 
		
	if($CURUSER[userclass] < 90)
		$export = "<li><a href=\"https://admin.indulhatunk.info/export.php\">$lang[export]</a></li>";
	else
		$export = '';
		
		if($_GET[type] == 'white' || !isset($_GET[type]))
			$class1 = 'current';
		if($_GET[type] == 'green')
			$class2 = 'current';
		if($_GET[type] == 'red')
			$class3 = 'current';
		if($_GET[type] == 'blue')
			$class4 = 'current';
		if($_GET[type] == 'grey')
			$class5 = 'current';

		echo "
		<li><a href=\"?year=$year&month=$month&type=white\" class=\"$class1\">$lang[active]</a></li>
		<li><a href=\"?year=$year&month=$month&type=green\"  class=\"$class2\">$lang[paid_printed]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=red\"  class=\"$class3\">$lang[notpaid_printed]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=blue\"  class=\"$class4\">$lang[paid_notprinted]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=grey\"  class=\"$class5\">$lang[billed]</a></li>
		     $export";
		 
		 if($CURUSER[userclass] > 50)
		 {
		 	echo "<li><a href=\"deleted.php\" class=\"\">Törölt elemek</a></li>";
		 	echo "<li><a href=\"inactive.php\" class=\"\">Inaktív vásárlók</a></li>";
			echo "<li><a href=\"emails.php\" class=\"\">Feldolgozatlan levelek</a></li>";

		 }
					?>
						<!--<li><a href='?type=white' class='<? if($_GET[type] == 'white' || empty($_GET)) echo "current";?>'>Elintézendő</a></li>
				-->
				
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>
<?
mysql_query("SET NAMES 'utf8'"); 
?>

<fieldset class='searchform'>
<form method="post" id="sform" action="customers.php">
<input type="text" name="search"  value="<?=$cookie?>" /><a href="#" class='searchbutton' id='submit'>Ok</a>
<? if($CURUSER[userclass] > 50) {

if($_POST[showinactive] == 'on')
{
	$showchecked = "checked";
}
 ?>
<br/>
<div style='margin:0px 0 0 0'>
	<label for="showinactive"><input type='checkbox' name='showinactive' id="showinactive" <?=$showchecked?>/> Töröltek között </label>
</div>
<? } ?>
</form>
</fieldset>
<?
//echo "<h2><a href=\"http://admin.indulhatunk.info\">$lang[home]</a> &raquo; $lang[manage_customers]</h2>";

//echo message($msg);

if($CURUSER["vatera"] == 1)
{	
	if($CURUSER["userclass"] == 255)
	{
		$stat = "<a href=\"?statistics=1\" class=\"show\">$lang[statistics]</a>";
	}
	else 
		$stat = "<a href=\"?type=orange\" class=\"orange\">$lang[postpaid]</a>";
	echo "<div class='subMenu'><a href=\"customers.php?add=1\">$lang[new_customer]</a> <a href=\"vouchers/print.php\" class='grey'>$lang[print_vouchers]</a> <a href=\"vouchers/print_envelope.php\" class='grey'>$lang[print_envelopes]</a> <a href=\"vouchers/print_post.php\" class='grey'>$lang[print_post]</a>  $stat <div class='cleaner'></div></div>";

}

if($_SERVER[REQUEST_URI] == '/customers.php')
	$preUrl = "/customers.php?";
else
	$preUrl = $_SERVER[REQUEST_URI];
	



if($CURUSER["userclass"] == 255 && $_GET[statistics] == 1)
{


$dateNow = date("n");
$dateMinus = $dateNow-1;
$yearMinus = date("Y");

if($dateMinus == 0)
{
	$dateMinus = 12;
	$yearMinus = date("Y")-1;
}	
	//echo "<h3>Indulhatunk.hu Kft.</h3>";
	getLongStats();
	getStatistics($dateMinus,$yearMinus);
	getStatistics($dateNow,date("Y"));
	getShortStats();
	
	/*echo "<h3>Hotel Outlet Kft.</h3>";

	getLongStats('hoteloutlet');
	getStatistics($dateMinus,$yearMinus,'hoteloutlet');
	getStatistics($dateNow,date("Y"),'hoteloutlet');
	getShortStats('hoteloutlet');
	*/
	foot();
	die;

}

if($msg <> '')
	echo "<div class='notify'>$msg</div>";

	
	$abc= array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	$num= array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	$string = generateNumber();


	
if($add == 1 && ($CURUSER["vatera"] == 1 || $CURUSER[pid] == 850)) {


if($editid >0) 
	$formData = '';
else
	$formData = unserialize(stripslashes($_COOKIE[customerForm]));
	
if($CURUSER[pid] == 850)
{
	$type = 'yellow';
}

?>
<div class="partnerForm">
	<form method="POST" action="customers.php?month=<?=$month?>&year=<?=$year?>&type=<?=$type?>">
		<?if($editarr[offer_id] =="") {?>
		<input type="hidden" name="offer_id" value="<?=$string?>">
		<?}
		else {?>
		<input type="hidden" name="editid" value="<?=$editarr[cid]?>">
		<input type="hidden" name="offer_id" value="<?=$editarr[offer_id]?>">
		<?}?>
	<fieldset id='customerForm'>
		<legend>Vásárló szerkesztése</legend>
	<ul>
		<?if($editarr[offer_id] =="") {?>
			<li><label>Azonosító:</label><?=$string?></li>
		<?}
		else {?>
				<li><label>Azonosító:</label><?=$editarr[offer_id]?></li>
		<?}?>
		
		<li>
		<select name="offers_id">
				<?
				
				if($editarr[cid] == '')
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW()  ORDER BY partners.hotel_name ASC");
				}
				else
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id ORDER BY partners.hotel_name ASC");
				}
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name] - ".formatPrice($partnerArr[actual_price])."</option>";
					}
				
				?>
			</select>
		</li>
		<? 	if($editarr[cid] <> '')
				{?>
		<li>
		<select name="sub_pid">
			<option value=''>Válasszon</option>
				<?
				
				$partnerQuery = $mysql->query("SELECT * FROM partners WHERE (country = '' or country = 'hu') AND hotel_name <> '' ORDER BY partners.hotel_name ASC");
				
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[pid] == $editarr[sub_pid] || $partnerArr[pid] == $formData[sub_pid])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name]</option>";
					}
				
				?>
			</select>
		</li>
		<? } ?>
		<li><label>Vevő neve:</label><input type="text" name="name" value="<?=$editarr[name]?>"/></li>
		<li><label>Cím:</label><input type="text" name="zip" value="<?=$editarr[zip]?>" style='width:30px;' class='numeric'/><input type="text" name="city" style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" name="address" class="small" style='width:320px;' value="<?=$editarr[address]?>" /></li>
		<li><label>Számlázási név:</label><input type="text" name="invoice_name" value="<?=$editarr[invoice_name]?>"/></li>
		<li><label>Számlázási Cím:</label><input type="text" name="invoice_zip"value="<?=$editarr[invoice_zip]?>" style='width:30px;'/><input type="text" style='width:140px;margin:0 1px 0 1px' class="small1" value="<?=$editarr[invoice_city]?>" name="invoice_city"/><input type="text" name="invoice_address" class="small" style='width:320px;' value="<?=$editarr[invoice_address]?>" /></li>
		
		
		<li><label>Telefonszám:</label><input type="text" name="phone" value="<?=$editarr[phone]?>"/></li>
		<li><label>E-mail cím:</label><input type="text" name="email" value="<?=$editarr[email]?>"/></li>
		<li><label>Licit:</label><input type="text" name="bid_name" value="<?=$editarr[bid_name]?><?=$formData[bid_name]?>"/></li>
		<li><label>Licit típus:</label>
			<select name="type">
				<option value="4" <?if($editarr[type]==4)echo"selected";?>>Outlet</option>
				<option value="1" <?if($editarr[type]==1)echo"selected";?>>Vatera</option>
				<option value="2" <?if($editarr[type]==2)echo"selected";?>>Teszvesz</option>
				<option value="6" <?if($editarr[type]==6)echo"selected";?>>Lealkudtuk</option>
				<option value="7" <?if($editarr[type]==7)echo"selected";?>>Grando</option>
				<option value="3" <?if($editarr[type]==3)echo"selected";?>>Licittravel</option>
				<option value="5" <?if($editarr[type]==5)echo"selected";?>>Pult</option>
				<option value="8" <?if($editarr[type]==8)echo"selected";?>>Viszonteladó</option>
			</select>
		</li>
		<li><label>Eladási ár:</label><input type="text" name="orig_price" value="<?=$editarr[orig_price]?><?=$formData[orig_price]?>" style='width:50px;' class='numeric'/> Ft</li>
		<li><label>Promóciós kód:</label>
			<select name="promotion_code">
				<option value=''>Válasszon</option>
				<?
					$partnerQuery = $mysql->query("SELECT * FROM promotion ORDER BY name ASC");
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[code] == $editarr[promotion_code])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[code]\" $selected>$partnerArr[name]</option>";
					}
				
				?>
			</select>
		</li>
		<li><label>Fizetés módja:</label>
			<select name="payment">
				<option value="2" <?if($editarr[payment]==2)echo"selected";?>>Készpénz</option>
				<option value="1" <?if($editarr[payment]==1)echo"selected";?>>Átutalás</option>
				<option value="3" <?if($editarr[payment]==3)echo"selected";?>>Utánvét</option>
				<option value="9" <?if($editarr[payment]==9)echo"selected";?>>Bankkártya</option>
				<option value="4" <?if($editarr[payment]==4)echo"selected";?>>Futár</option>
				<option value="5" <?if($editarr[payment]==5)echo"selected";?>>Üdülési csekk</option>
				<option value="6" <?if($editarr[payment]==6)echo"selected";?>>Helyszinen</option>
				<option value="7" <?if($editarr[payment]==7)echo"selected";?>>Online</option>
				<option value="8" <?if($editarr[payment]==8)echo"selected";?>>Facebook</option>
				<option value="10" <?if($editarr[payment]==10)echo"selected";?>>OTP SZÉP kártya</option>
				<option value="11" <?if($editarr[payment]==11)echo"selected";?>>MKB SZÉP kártya</option>
				<option value="12" <?if($editarr[payment]==12)echo"selected";?>>K&H SZÉP kártya</option>


			</select>
		</li>
		<li><label>Szállítás módja:</label>
			<select name="shipment">
				<option value="0">Válassza ki az átvételi módot</option>
				<option value="2" <?if($editarr[shipment]==2)echo"selected";?>>Online kérem</option>
				<option value="3" <?if($editarr[shipment]==3)echo"selected";?>>Személyes átvétel</option>
				<option value="4" <?if($editarr[shipment]==4)echo"selected";?>>Ajánlott levélként: +395 Ft</option>
				<option value="5" <?if($editarr[shipment]==5)echo"selected";?>>Postai utánvéttel: +1 450 Ft</option>
			</select>

		</li>
		<li><label>Postaköltség:</label><input type="text" name="post_fee" value="<?=$editarr[post_fee]?><?=$formData[post_fee]?>" style='width:50px;' class='numeric'/> Ft</li>

		<li><label>Üdülésicsekk info:</label><textarea name="checks"><?=$editarr[checks]?></textarea></li>
		<li><label>Üdülésicsekkek értéke:</label><input type="text" name="check_value" value="<?=$editarr[check_value]?>" style='width:50px' class='numeric'/> Ft</li>
		<li><label>Üdülésicsekkek száma:</label><input type="text" name="check_count" value="<?=$editarr[check_count]?>"  style='width:50px' class='numeric'/> darab</li>

		<li><label>Megjegyzés:</label><textarea name="comment"><?=$editarr[comment]?></textarea></li>
		<li><label>Hotel megjegyzés:</label><textarea name="hotel_comment"><?=$editarr[hotel_comment]?></textarea></li>

		<li><label>Érvényesség:</label><textarea name="validity"><?=$editarr[validity]?><?=$formData[validity]?></textarea></li>
		<li><label>Nem tartalmazza:</label><input type="text" name="not_include" value="<?=$editarr[not_include]?><?=$formData[not_include]?>"/></li>
	
		<li><label>Ajándék (Nincs ár)?</label>
		<select name='gift'>
			<option value='0'>Nem</option>
			<option value='1' <?if($editarr[gift]==1)echo"selected";?>>Igen</option>
			</select>
		</li>
		
		<li><label>Email ID:</label><input type="text" name="email_id" value="<?=$editarr[email_id]?>" class='numeric' style='width:50px;'/></li>
		<li><label>Számlaszám:</label><input type="text" name="user_invoice_number" value="<?=$editarr[user_invoice_number]?>"/></li>
		<li><label>Számla dátuma:</label><input type="text" name="user_invoice_date" value="<?=$editarr[user_invoice_date]?>"/></li>
		<li><label>Számla elkezdve:</label><input type="text" name="user_invoice_started" value="<?=$editarr[user_invoice_started]?>"/></li>


		<?if($editarr[inactive] == 1){?>
		<li><label>Törölt:</label>
			<select name="inactive">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[inactive]==1)echo"selected";?>>Igen</option>
			</select>
		</li>
		<?
		}
		if($CURUSER["userclass"] == 255)
		{
		?>		
		<li><label>Fizetve:</label>
			<select name="paid">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[paid]==1)echo"selected";?>>Igen</option>
			</select>
		</li>			

		<? } ?>
		<hr/>
	
	<?
	if($editarr[offer_id] <>"") {
		$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $editarr[offers_id]");
		$offerArr = mysql_fetch_assoc($offerQr);
	?>
	<input type="hidden" name="days" value="<?=$offerArr[days]?>"/>
	<input type="hidden" name="actual_price" value="<?=$offerArr[actual_price]?>"/>

	<?
		if($offerArr[plus_half_board]<>0) {?>
	<li><label>Félpanzió</label><input type="checkbox" name="plus_half_board" class="plus_half_board" value="<?=$offerArr[plus_half_board]?>" <?if($editarr[plus_half_board]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_half_board])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_weekend_plus]<>0) {?>
<li><label>Hétvégi felár</label><input type="checkbox" name="plus_weekend_plus" class="plus_weekend_plus" value="<?=$offerArr[plus_weekend_plus]?>" <?if($editarr[plus_weekend_plus]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_weekend_plus]*2)?></label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_bed]<>0) {?>
<li><label>Pótágy felár</label><input type="checkbox" name="plus_bed" class="plus_bed" value="<?=$offerArr[plus_bed]?>" <?if($editarr[plus_bed]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_bed_plus_food]<>0) {?>
<li><label>Félpanzió + pótágy felár</label><input type="checkbox" name="plus_bed_plus_food" class="plus_bed_plus_food" value="<?=$offerArr[plus_bed_plus_food]?>"  <?if($editarr[plus_bed_plus_food]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed_plus_food])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>1. gyermek kora:</label> <input type="hidden" name="prev_child1" value="0" class="prev_child1" <?=$disabled?>/>
	<select name="child1_value" class="child1_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>"  <? if($editarr[plus_child1_value]==1 || ($editarr[plus_child1_value] >1 && $editarr[plus_child1_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	<div class='cleaner'></div>
</li>
<?}
else
{
?>
<input type='hidden' name='child1_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>2. gyermek kora: </label> <input type="hidden" name="prev_child2" value="0" class="prev_child2"/>
	<select name="child2_value" class="child2_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child2_value]==1 || ($editarr[plus_child2_value] >1 && $editarr[plus_child2_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	<div class='cleaner'></div>
</li>
<?}
else
{
?>
<input type='hidden' name='child2_value' value='999'/>
<?
}
?>
<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>3. gyermek: </label>	
	<select name="child3_value" class="child3_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child3_value]==1 || ($editarr[plus_child3_value] >1 && $editarr[plus_child3_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig </option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett</option>
	</select>
<div class='cleaner'></div></li>
<?}
else
{
?>
<input type='hidden' name='child3_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_room1_value]<>0) {?>
<li><label><?=$offerArr[plus_room1_name]?> szoba felár</label> <input type="checkbox" class="room1_value" name="room1_value" value="<?=$offerArr[plus_room1_value]?>" <?if($editarr[plus_room1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room1_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_room2_value]<>0) {?>
<li><label><?=$offerArr[plus_room2_name]?> szoba felár</label> <input type="checkbox" class="room2_value" name="room2_value" value="<?=$offerArr[plus_room2_value]?>" <?if($editarr[plus_room2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room2_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_room3_value]<>0) {?>
<li><label><?=$offerArr[plus_room3_name]?> szoba felár</label> <input type="checkbox" class="room3_value" name="room3_value" value="<?=$offerArr[plus_room3_value]?>" <?if($editarr[plus_room3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room3_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other1_value]<>0) {?>
<li><label><?=$offerArr[plus_other1_name]?> felár</label> <input type="checkbox" class="other1_value" name="other1_value" value="<?=$offerArr[plus_other1_value]?>" <?if($editarr[plus_other1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other1_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other2_value]<>0) {?>
<li><label><?=$offerArr[plus_other2_name]?> felár</label> <input type="checkbox"  class="other2_value" name="other2_value" value="<?=$offerArr[plus_other2_value]?>" <?if($editarr[plus_other2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other2_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other3_value]<>0) {?>
<li><label><?=$offerArr[plus_other3_name]?> felár</label> <input type="checkbox" class="other3_value" name="other3_value" value="<?=$offerArr[plus_other3_value]?>" <?if($editarr[plus_other3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other3_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other4_value]<>0) {?>
<li><label><?=$offerArr[plus_other4_name]?> felár</label> <input type="checkbox" class="other4_value" name="other4_value" value="<?=$offerArr[plus_other4_value]?>" <?if($editarr[plus_other4_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other4_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other5_value]<>0) {?>
<li><label><?=$offerArr[plus_other5_name]?> felár</label> <input type="checkbox" class="other5_value" name="other5_value" value="<?=$offerArr[plus_other5_value]?>" <?if($editarr[plus_other5_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other5_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other6_value]<>0) {?>
<li><label><?=$offerArr[plus_other6_name]?> felár</label> <input type="checkbox" class="other6_value" name="other6_value" value="<?=$offerArr[plus_other6_value]?>" <?if($editarr[plus_other6_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other6_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_single1_value]<>0) {?>
<li><label><?=$offerArr[plus_single1_name]?> felár</label> <input type="checkbox" class="plus_single1_value" name="plus_single1_value" value="<?=$offerArr[plus_single1_value]?>" <?if($editarr[plus_single1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single1_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>
<?if($offerArr[plus_single2_value]<>0) {?>
<li><label><?=$offerArr[plus_single2_name]?> felár</label> <input type="checkbox" class="plus_single2_value" name="plus_single2_value" value="<?=$offerArr[plus_single2_value]?>" <?if($editarr[plus_single2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single2_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>
<?if($offerArr[plus_single3_value]<>0) {?>
<li><label><?=$offerArr[plus_single3_name]?> felár</label> <input type="checkbox" class="plus_single3_value" name="plus_single3_value" value="<?=$offerArr[plus_single3_value]?>" <?if($editarr[plus_single3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single3_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>


<?if( $offerArr[days]>0) {?>

<li><label>Maradjon még egy éjszakát</label><input type="hidden" name="plus_day_temp" class="plus_day_temp" value="<?=$offerArr[actual_price]/$offerArr[days]?>"  /><input type="checkbox" name="plus_days" class="plus_days" value="<?=$offerArr[actual_price]/$offerArr[days]?>" <?if($editarr[plus_days]>0)echo"checked"?> <?=$disabled?>/><label>+ <span id="plusDays"><?=formatPrice($offerArr[actual_price]/$offerArr[days])?></span></label><div class='cleaner'></div></li>
<?}

}
?>
	<li><input type="submit" value="Mentés" /></li>
	</ul>
	</fieldset>
	</form>
</div>

</div></div>
<?
foot();
die;
}


echo "<center><select id='monthChange'>";
$listerQuery = $mysql->query("SELECT MONTH(added) AS listerMonth, YEAR(added) AS listerYear FROM customers WHERE cid > 0 AND inactive = 0 $extraSelect GROUP BY YEAR(added),MONTH(added)");

	echo "<option value='0'>$lang[select]</option>";
while($listerArr = mysql_fetch_assoc($listerQuery)) {
	
	if($listerArr[listerMonth] == $month && $listerArr[listerYear] == $year) {
		$bold = "selected";
	}
	else
		$bold = "";
	echo "<option value='year=$listerArr[listerYear]&month=$listerArr[listerMonth]&type=$filter' $bold>$listerArr[listerYear]. ".$lang['months'][$listerArr[listerMonth]]."</option>";
}
echo "</select></center>";
echo "<hr/>";




if($filter <> 'white' &&  $filter <> 'green' && $filter <> 'red' && $filter <> 'blue' && $filter <> 'yellow' && $filter <> ''&& $filter <> 'grey' && $filter <> 'orange')// || 
{
	die(":)");
}
if($filter == "white" || $filter == '')
	$filterSelect = "AND paid = 0 AND voucher = 0 AND name <> 'Pult'";
if($filter == "green")
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created <> 1";
if($filter == "blue")
	$filterSelect = "AND paid = 1 AND voucher = 0";
if($filter == "red")
	$filterSelect = "AND paid = 0 AND voucher = 1 AND type <> 5";
if($filter == "yellow")
	$filterSelect = "AND paid = 0 AND type = 5";
if($filter == "grey")
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created = 1";
if($filter == "orange")
	$filterSelect = "AND paid = 0 AND voucher = 0 AND payment = 3";
				


//pagination 
$pg = (int)$_GET[page];

 $pq = "SELECT cid FROM customers WHERE MONTH(added)=$month AND YEAR(added)=$year AND inactive = 0 $filterSelect $extraSelect  ORDER BY added DESC";
 $pq = mysql_query($pq);
 $rows = mysql_num_rows($pq);
 $perpage = 25; 
 $last = ceil($rows/$perpage);
 if($pg == '' && $pg == 0)
 {
 	$qrStart = 0;
 }
 elseif($pg == 1)
 {
 	$qrStart = 1;
 }
 else
 {
 	$qrStart = ($pg-1)*$perpage-1;
 	
  }
  
  //$qrStart = 0;
  //$perpage = 25; 
//pagination

//if searchquery is not empty
if($s <> '')
{
	if($_POST[showinactive] == 'on')
	{
		$showdeleted = 'inactive = 1';
	}
	else
	{
		$showdeleted = 'inactive = 0';
	}
	$qr = "SELECT * FROM customers WHERE cid > 0 AND $showdeleted  $extraSelect AND (offer_id like '%$s%' OR user_invoice_number like '%$s%' OR name like '%$s%' OR invoice_name like '%$s%' OR comment like '%$s%' OR bidder_name like '%$s%' OR email like '%$s%' ) ORDER BY added DESC LIMIT $qrStart, $perpage";
//	echo "kereses $qr";
}
else
	$qr = "SELECT * FROM customers WHERE MONTH(added)=$month AND YEAR(added)=$year AND inactive = 0 $filterSelect $extraSelect  ORDER BY added DESC LIMIT $qrStart, $perpage";
	
//$qr = "SELECT * FROM customers WHERE MONTH(added)=$month AND YEAR(added)=$year AND inactive = 0 $filterSelect $extraSelect ORDER BY added DESC";
$query = mysql_query($qr);


if(mysql_num_rows($query) <= 0)
{
	echo "<h1>$lang[no_results]</h1>";
	foot();
	die;
}	
echo "<table class=\"general\" width='100%'>";

echo "<tr class=\"header\">";
	if($CURUSER["vatera"] == 1) {
		echo "<td width='0' colspan='2'></td>";
	}
		//echo "<td></td>";
	if($CURUSER[pid] == 850)
	{
		echo "<td width='20'>-</td>";
	}
	echo "<td width='20'></td>";
	echo "<td width='85'>$lang[id]</td>";
	if($CURUSER["vatera"] == 1) {
		echo "<td width='70'>$lang[date]</td>";
	}

	echo "<td width='200'>$lang[customer_name]</td>";
	//if($CURUSER["vatera"] == 1) {
		echo "<td>$lang[partner]</td>";
	
	//	}
//	if($CURUSER[language] <> 'en')
//		echo "<td>$lang[item_desc]</td>";
	echo "<td width='60'>$lang[price]</td>";
	
	echo "<td>$lang[payment]</td>";
	
	if($CURUSER[vatera] <> 1)
		echo "<td>$lang[customer_left]</td>";
	if($CURUSER["vatera"] == 1) {
	
		echo "<td>$lang[paid]?</td>";
		echo "<td>$lang[tools]?</td>";
		echo "<td width='45'>$lang[post]</td>";
		echo "<td>$lang[bill]</td>";
		echo "<td>$lang[comment]</td>";
		

	}
echo "</tr>";
while($arr = mysql_fetch_assoc($query)) {
	if($arr[type] == 1) 
		$type = "Vatera";
	elseif($arr[type] == 2) 
		$type = "Teszvesz";
	elseif($arr[type] == 3) 
		$type = "Licittravel";
	elseif($arr[type] == 4) 
		$type = "Outlet";
	elseif($arr[type] == 5) 
		$type = "Pult";
	elseif($arr[type] == 6) 
		$type = "Lealkudtuk";
	elseif($arr[type] == 7) 
		$type = "Grando";
	
	
	//do not display postponed because of offers
	$offerQuery = $mysql->query("SELECT abroad FROM offers WHERE id = '$arr[offers_id]' LIMIT 1");
	$offerArr = mysql_fetch_assoc($offerQuery);
	

	if($arr[postpone] == 1 && $offerArr[abroad] <> 1)
		$payment = $lang[t_check];
	elseif($arr[payment] == 1  || $arr[checkpaper_id] > 0) 
		$payment = $lang[transfer];
	elseif($arr[payment] == 2) 
		$payment = $lang[cash];
	elseif($arr[payment] == 3) 
		$payment = $lang[postpaid];
	elseif($arr[payment] == 4) 
		$payment = $lang[delivery];
	elseif($arr[payment] == 5) 
		$payment = $lang[t_check];
	elseif($arr[payment] == 6) 
		$payment = $lang[place];
	elseif($arr[payment] == 7) 
		$payment = $lang[online];
	elseif($arr[payment] == 8) 
		$payment = $lang[facebook];
	elseif($arr[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($arr[payment] == 10) 
		$payment = "OTP SZÉP kártya";
	elseif($arr[payment] == 11) 
		$payment = "MKB SZÉP kártya";
	elseif($arr[payment] == 12) 
		$payment = "K&H SZÉP kártya";
		
	if($arr[paid] == 0 && $arr[voucher] <> 1 && $arr[name] <> 'Pult') {
		$class = "";
		$paid = $lang[no];
	} 

	elseif($arr[voucher] == 0 && $arr[paid] == 1) {
		$class = "blue";
	//	$vouch = "Igen";	
	}	
	elseif($arr[voucher] == 1 && $arr[paid] == 0) {
		$class = "red";
	//	$vouch = "Igen";	
	}	
	elseif($arr[paid] == 1 && $arr[voucher] == 1 && $arr[invoice_created] <> 1) {
		$class = "green";
		$paid = $lang[yes];	
	}
	elseif($arr[invoice_created] == 1 && $arr[paid] == 1 && $arr[voucher] == 1){
		$class = "grey";
	}
	elseif($arr[type] == 5 && $arr[address] == '')
	{
		$class = "yellow";
	}
	
	if($arr[inactive] == 1)
		$class = 'purple';

	
	$partnerQuery = $mysql->query("SELECT company_name,hotel_name FROM partners WHERE pid = '$arr[pid]' LIMIT 1");
	$partnerArr = mysql_fetch_assoc($partnerQuery);
	

	
	$realPartnerArr = '';
	if($arr[sub_pid] > 0)
		{
		$realPartnerQuery = $mysql->query("SELECT company_name,hotel_name FROM partners WHERE pid = '$arr[sub_pid]' LIMIT 1");
		$realPartnerArr = mysql_fetch_assoc($realPartnerQuery);
		}
	if($CURUSER["vatera"] == 1) {
		echo "<form method=\"post\" action=\"customers.php\">";
		echo "<input type=\"hidden\" name=\"cid\" value=\"$arr[cid]\">";
		echo "<input type=\"hidden\" name=\"paidBill\" value=\"1\">";
		echo "<input type=\"hidden\" name=\"payment\" value=\"$arr[payment]\">";
		
		echo "<input type=\"hidden\" name=\"comment\" value=\"$arr[offer_id] voucher fizetve $arr[name] által\">";
		echo "<input type=\"hidden\" name=\"value\" value=\"$arr[orig_price]\">";
		echo "<input type=\"hidden\" name=\"email\" value=\"$arr[email]\">";

		echo "<input type=\"hidden\" name=\"postage\" value=\"$arr[post_fee]\">";

		echo "<input type=\"hidden\" name=\"voucher_id\" value=\"$arr[offer_id]\">";
		
		echo "<input type=\"hidden\" name=\"partner_id\" value=\"$arr[pid] voucher fizetve $arr[name] által\">";
		echo "<input type=\"hidden\" name=\"check_comment\" value=\"$arr[checks]\">";


	}
	echo "<tr class=\"$class\">";
	
	
	if($CURUSER["vatera"] == 1) {
	

			if($arr[checked] == 1)
				$checked = "<a target='_blank' href=\"http://vasarlas.szallasoutlet.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/check1.png' alt='".$arr[checked_date]." @ ".$arr[checked_ip]."' title='".$arr[checked_date]." @ ".$arr[checked_ip]."' width='20'/></b></a>";
			else
				$checked = "<a target='_blank' href=\"http://vasarlas.szallasoutlet.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/cross1.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>";
				
			$editlink = "$checked
				<a href=\"?add=1&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>
				<a href=\"?delete=$arr[cid]&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/trash.png' alt='töröl' title='töröl' width='20'/></b></a>
				<a href=\"?copy=$arr[cid]&offer_id=$arr[offer_id]\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/copy.png' alt='másol' title='másol' width='20'/></b></a> 
				<a href=\"/info/manage_invoice.php?cid=$arr[cid]\" rel='facebox iframe'><b><img src='images/storno.png' alt='Számlázási funkciók' title='Számlázási funkciók' width='20'/></b></a> 
				";

		echo "<td>$editlink</td>";
		
		if($arr[company_invoice] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		else
			$logo = 'ologo_small.png';
			
		echo "<td><img src='/images/$logo'/></td>";
			
			
			
			
	}
	if($arr[pid] == 3003 || $arr[pid] == 3121)
		echo "<td><img src='/images/film.png' width='20' alt='TV kampány utalvány' title='TV kampány utalvány'/></td>";
	else
		echo "<td align='center'>-</td>";
	/**** ****/
			if($arr[company_invoice] == 'hoteloutlet')
			{	
				
				if($arr[payment] == 5)
				{
					$invoice = '-';
				}
				elseif($arr[invoice] <> '')
				{	
					$invoice = '-';
				}
				else
				{
				}
				//	$invoice = "<a href='#'  title='Az adott tételt a szálloda számlázza!' alt='Az adott tételt a szálloda számlázza!'><img src='/images/dollar.png' title='Az adott tételt a szálloda számlázza!' alt='Az adott tételt a szálloda számlázza!' width='20'/></a>";

					
			}
			else
			{
				$invoice = '-';
			}
			
			
			if($arr[gift] == 1 && $CURUSER[vatera] == 1)
				$gift = "<br/><img src='/images/heart.png' width='20' alt='Az utalvány ajándék' title='Az utalvány ajándék'/>";
			else
				$gift = '';
				
			
			if($arr[postpone] == 1 && $CURUSER[userclass] > 50 && $arr[postpone_cleared] <> 1)
				$delay = "<br/><img src='/images/delayed.png' width='20' alt='$arr[postpone_date]' title='$arr[postpone_date]'/>";
			else
				$delay = '';


//			echo "<td width='30' align='center'>$invoice $gift $delay</td>";


			/**** ****/
			
			
	if($CURUSER[pid] == 850)
	{
	if($arr[checked] == 1)
				$checked = "<a target='_blank' href=\"http://vasarlas.szallasoutlet.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/check1.png' alt='".$arr[checked_date]." @ ".$arr[checked_ip]."' title='".$arr[checked_date]." @ ".$arr[checked_ip]."' width='20'/></b></a>";
			else
				$checked = "<a target='_blank' href=\"http://vasarlas.szallasoutlet.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/cross1.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>";
			
			$editlink = "$checked<a href=\"?add=1&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\"><b><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a> </b></a> ";
		echo "<td>$editlink</td>";	
	}
		echo "<td>".str_replace("VTL-","",$arr[offer_id])."</td>";
		if($CURUSER["vatera"] == 1) {
			echo "<td>".date('y.m.d. H:i',strtotime($arr[added]))."</td>";
		}

	/*	if($arr[bidder_name] <> '')
			$bname = "($arr[bidder_name])";
		else
			$bname = '';
	*/		
		if($arr[inactive] == 1)
			$deletetext = " [ TÖRÖLT ] ";
		else
			$deletetext = '';
			
			
	
		echo "<td><a href='info/customer.php?cid=$arr[cid]' rel='facebox'><b>".$deletetext."$arr[name] $bname</b></a></td>";

	//	if($CURUSER["vatera"] == 1) {
	$extrapartner ='';
	if($arr[sub_pid] > 0)
		$extrapartner = "<br/>".$realPartnerArr[hotel_name];
			echo "<td><a href='/preview.php?tid=$arr[offers_id]&type=offer' rel='facebox'><b>$partnerArr[hotel_name]$extrapartner</b></a></td>";
	//	}
		//echo "<td>$arr[bid_id]</td>";
		
	//	if($CURUSER[language] <> 'en')
	//		echo "<td>".str_replace("|","<br/>",$arr[bid_name])."</td>";
		echo "<td align='right'>".str_replace(" ","&nbsp;",formatPrice($arr[orig_price]))."</td>";
		echo "<td align='center'>$payment</td>";
		if($CURUSER[vatera] <> 1)
		{
		echo "<td align='center'>";
		if($arr[customer_left] == '0000-00-00 00:00:00')
		{
			echo "<input type='button' value='$lang[customer_left]' class='alertbox' id='$arr[cid]'/>";
		}
		echo "</td>";
		}
		if($CURUSER["vatera"] == 1) {
				if($arr[invoice_created] == 1) {
					$disabled = "disabled";
				}	
				elseif ($arr[paid] == 1 && $arr[voucher] == 1)
				{
					$disabled = "disabled";
				}
				else {
					$disabled = "";
				}
			
			echo "<td>";
		
			echo "<select  onchange=\"jQuery.facebox({ ajax: 'info/pay.php?cid=$arr[cid]&voucher=$arr[voucher]&company=$arr[company_invoice]&returnto=$_SERVER[REQUEST_URI]' });\" $disabled>";
			echo "<option value=\"0\">$lang[no]</option>";
			echo "<option value=\"1\"";
				if($arr[paid] == 1)
					echo "selected";
			echo ">$lang[yes]</option>";

			echo "</select>";
			echo "</td>";
			echo "<input type=\"hidden\" name='voucher' value=\"$arr[voucher]\"/>";
			
			if($arr[user_invoice_number] == '')
				$asklink = " [<a href=\"invoice/create_ask.php?cid=$arr[cid]&email=$arr[email]\" rel='facebox'>díjbekérő</a>]";
			else
				$asklink = '';
				
			if($arr[paid] == 1 && ($arr[payment] == 2 || $arr[payment] == 5))
				$clearancelink = " [<a href=\"/vouchers/print_checkout.php?sure=1&type=&cid=$arr[offer_id]\">bizonylat</a>]";
			else
				$clearancelink = '';
				
				
			if($arr[user_invoice_ask] <> '')
				$asklink = " [<a href=\"invoices/vatera/".str_replace("/","_",$arr[user_invoice_ask]).".pdf\" target='_blank'>díjbekérő</a>]";

				
				
					$print = "[<a href=\"vouchers/print.php?cid=$arr[cid]&paid=$arr[paid]\" rel='facebox'>$lang[print]</a>]<br/>
						 [<a href=\"vouchers/online.php?cid=$arr[cid]&email=$arr[email]&realID=$arr[offer_id]&paid=$arr[paid]\" rel='facebox'>$lang[o_online]</a>]
						 [<a href=\"vouchers/sendletter.php?cid=$arr[cid]\" rel='facebox'>$lang[notify]</a>]
						 $asklink
						 $clearancelink
						 ";
			
				echo "<td>$print</td>";
			$post_invoice ="";
			if($arr[post_invoice_number] <> "") {
				if($month <10) {
					$zeroMonth = "0".$month;
				}
				else
					$zeroMonth = $month;
				
				$post_invoice = "<a href=\"invoices/vatera/".str_replace("/","_",$arr[post_invoice_number]).".pdf\" target=\"_blank\">$arr[post_invoice_number]</a>";
				
				}
			echo "<td aling='right'>$arr[post_fee] Ft $post_invoice</td>";
			if($arr[user_invoice_number] <> "") {
				$date = explode("-",$arr[user_invoice_date]);
				$year = $date[0];
				$month = $date[1];
				echo "<td align='right'><a href=\"invoices/vatera/".str_replace("/","_",$arr[user_invoice_number]).".pdf\" target=\"_blank\">".str_replace("M2012/",'',$arr[user_invoice_number])."</a> 
						(<a href=\"invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf\" target=\"_blank\">".str_replace("M2012/",'',$arr[invoice_number])."</a>)</td>";
			}
			else {
				//if($arr[paid] == 1 && $arr[voucher] == 1)
				//	echo "<td><a href=\"invoice/create_invoice.php?customer=$arr[cid]\">Számla készítése</a></td>";
				//else
				echo "<td>(<a href=\"invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf\" target=\"_blank\">".str_replace("M2012/",'',$arr[invoice_number])."</a>)</td>";
			}
			
			echo "<td>$arr[comment]</td>";
			
		
			

		}
	echo "</tr>";
	if($CURUSER["vatera"] == 1) {
		echo "</form>";
	}
}
echo "</table>";

 //pagination
	if($s == '')
	{
 echo "<hr/>";
 for($g=1;$g<=$last;$g++)
 {
 	$e = $g*$perpage;
 	$st = $e-($perpage-1);
 	if($pg == $g)
 		$bold = "class='bold'";
 	elseif($pg == '' && $g==1)
 	{
 		$bold = "class='bold'";
 	}
	else
 		$bold = '';
 	echo "<a href=\"?year=$year&month=$month&type=$filter&page=$g\" $bold>$st - $e</a> &raquo; ";
 }

 }
?>
</div></div>
<?
// echo "</center>";
foot();
?>