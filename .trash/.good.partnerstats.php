<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();

if($CURUSER[userclass] > 255)
	header("location: index.php");
	
head('Eladási statisztikák');


?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?type=0" class="<? if($_GET[type] == 0) echo "current";?>">Fizetési statisztika</a></li>
		<li><a href="?type=1" class="<? if($_GET[type] == 1) echo "current";?>">Eladási statisztika</a></li>
		<li><a href="?type=4" class="<? if($_GET[type] == 4) echo "current";?>">Fizetési statisztika éves</a></li>

		<li><a href="?type=3" class="<? if($_GET[type] == 3) echo "current";?>">Eladási statisztika éves</a></li>

	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>

<? /*
<form method='get'>
<select name='company' onchange='submit()' style='width:430px;'>
	<option value='0'>Cég neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(company_name) <> '' order by company_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[company_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[company_name]</option>";
		}
	?>
</select>
<select name='hotel' onchange='submit()'  style='width:430px;'>
	<option value='0'>Hotel neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(hotel_name) <> '' order by hotel_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[hotel_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[hotel_name]</option>";
		}
	?>
</select>
</form>
<hr/>
*/

///brand new stats
if($_GET[type] >= 3)
{

	
if($_GET[type] == 3)
{
	$orderbyfield = "added";
}
elseif($_GET[type] == 4)
{
	$orderbyfield = "paid_date";
}


$seller = $_GET[seller];

if($_GET[seller] == '')
	$seller = '';
else
	$seller = "AND (type = '1' OR type = '2' OR type = '6' OR type = '7') ";
	
	
if($_GET[paid] == 1)
	$extrapaid = "AND paid = 1";
$month = array("","január","február","március","április","május","június","július","augusztus","szeptember","október","november","december");


	$years = array(2010,2011,2012);
	
	echo "<h2>Havi bontásban</h2>";


	echo "<table width='300'>";
		
	echo "<tr class='header'>";
	echo "<td></td>";
	foreach($years as $year)
	{
		echo "<td colspan='4'>$year</td>";
	}
	for($i=1;$i<=12;$i++)
	{
		echo "<tr>";
		echo "<td width='60'>$month[$i]</td>";
		
		for($z=0;$z<count($years);$z++)
		{
			$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE month($orderbyfield) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 0 $seller $extrapaid"));

			$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE month($orderbyfield) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 0 $seller $extrapaid"));

			$inactive = mysql_fetch_assoc($mysql->query("SELECT count(orig_price) AS count FROM customers WHERE month($orderbyfield) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 1 $seller"));

			$icurdate = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE month($orderbyfield) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 1 $seller"));
			$check = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE month($orderbyfield) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect  AND payment = 5 AND paid =1"));


			if($curdate[count] == 0)
			{
				$curdate[count] = '';
				$totals[count] = '';
			}
			else
			{
				$curdate[count] = $curdate[count]."&nbsp;db";
				$totals[count] = formatPrice($totals[count],0,1);
			}
			
			if($inactive[count] == 0)
			{
				$inactive[count] = '';
				$icurdate[count] = '';
			}
			else
			{
				$inactive[count] = $inactive[count]."&nbsp;db";
				$icurdate[count] = formatPrice($icurdate[count],0,1);
			}
			
			echo "<td align='right'>$curdate[count]</td>";
			echo "<td align='right'>$totals[count]</td>";
			
			echo "<td align='right'>$inactive[count]</td>";
			echo "<td align='right'>$icurdate[count]</td>";
			
			echo "<td align='right' class='grey'>$check[count]</td>";

		}
		echo "</tr>";
		
	}
	echo "</tr>";
	echo "</table>";

echo "<h2>Heti bontásban</h2>";
	echo "<table width='300'>";
		
	echo "<tr class='header'>";
	echo "<td></td>";
	foreach($years as $year)
	{
		echo "<td colspan='4'>$year</td>";
	}
	for($i=1;$i<=53;$i++)
	{
		echo "<tr>";
		echo "<td width='60'>$i. hét</td>";
		
		for($z=0;$z<count($years);$z++)
		{
			$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE week($orderbyfield,3) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 0 $seller $extrapaid"));

			$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE week($orderbyfield,3) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 0 $seller $extrapaid"));

			$inactive = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE week($orderbyfield,3) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 1 $seller"));

			$icurdate = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE week($orderbyfield,3) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND inactive = 1 $seller"));


			$check = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE week($orderbyfield,3) = '$i' AND year($orderbyfield) = '".$years[$z]."' $extraselect AND payment = 5 AND paid =1"));


			if($curdate[count] == 0)
			{
				$curdate[count] = '';
				$totals[count] = '';
			}
			else
			{
				$curdate[count] = $curdate[count]."&nbsp;db";
				$totals[count] = formatPrice($totals[count],0,1);
			}
			
				if($inactive[count] == 0)
			{
				$inactive[count] = '';
				$icurdate[count] = '';
			}
			else
			{
				$inactive[count] = $inactive[count]."&nbsp;db";
				$icurdate[count] = formatPrice($icurdate[count],0,1);
			}


			echo "<td align='right'>$curdate[count]</td>";
			echo "<td align='right'>$totals[count]</td>";
			
			echo "<td align='right'>$inactive[count]</td>";
			echo "<td align='right'>$icurdate[count]</td>";
			echo "<td align='right' class='grey'>$check[count]</td>";
			
		}
		
		echo "</tr>";
		
	}
	echo "</tr>";
	echo "</table>";
		
	echo "</div></div>";
	foot();
die;
}

//brand new stats end 
$paidtypes = array(
	
	'Teljes' => '',
	'Üdülési csekk' => 5,
	'Helyszínen' => 6,
	'Átutalás' => 1,
	'Bankkártya' => 9,
	'Készpénz' => 2,
	'OTP Szép kártya' => 10,
	'K&H Szép kártya' => 11,
	'MKB Szép kártya' => 12,
	'Bankkártya online' => 13	
	);
	
$soldtypes = array(
	'Összesen' => '',
	'Lealkudtuk' => 6,
	'Grando' => 7,
	'Vatera' => 1,
	'Teszvesz' => 2,
	'Licittravel' => 3,
	'Outlet' => 4,
	'Viszonteladó' => 8,
);

$today = date("Y-m-d");

//$today =  date("Y-m-d",strtotime(" +1 days"));
$week = (int)date("W");
$month = date("n");
$year = date("Y");

$days = array();
$days[] = $today;
$days[] = date("Y-m-d",strtotime(" -1 days"));
$days[] = date("Y-m-d",strtotime(" -7 days"));
$days[] = date("Y-m-d",strtotime(" -49 days"));
$days[] = date("Y-m-d",strtotime(" -364 days"));



$weeks = array();

$origweek = $week;

$weeks[] = $week;



$weeks[] = $week-1;

if($week <=4)
	$week = 53+$week;
	
$weeks[] = $week-4;
$weeks[] =	$origweek;

$months = array();
$months[] = $month;

if($month == 1)
	$months[] = 12;
else
	$months[] = $month-1;
	
$months[] = $month;

//debug($months);

$emonth = $month;
	if($emonth == 12)
		$emonth = 0;
		
$months[] = $emonth+1;
$months[] = $emonth+2;


$prevmonths = array();
$prevweeks = array();


?>
<table>

<?

if($_GET[company] > 0)
{
	$clr = "AND customers.pid = $_GET[company]";
	$pid = $_GET[company];
}
elseif($_GET[hotel] > 0)
{
	$clr = "AND customers.pid = $_GET[hotel]";
	$pid = $_GET[hotel];
}
else
{
	$clr = '';
	$pid = '';
}
	
	

if($pid > 0)
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT company_name, hotel_name FROM partners WHERE pid = $pid"));
	
	echo "<tr class='header'><td colspan='30'>$partner[company_name], $partner[hotel_name]</td></tr>";
}
else
{
	echo "<tr class='header'><td colspan='30'>Teljes statisztika</td></tr>";
}
	
	
?>
<tr class='header'>
	<td rowspan='2'>-</td>
	<td colspan='2' class='rightborder'>Ma</td>
	<td colspan='2' class='rightborder'>Tegnap</td>
	<td colspan='2' class='rightborder'>1 hete</td>
	<td colspan='2' class='rightborder'>4 hete</td>
	<td colspan='2' class='rightborder'>52 hete</td>
	<td colspan='2' class='rightborder'>Ehéten</td>
	<td colspan='2' class='rightborder'>1 hete</td>
	<td colspan='2' class='rightborder'>4 hete</td>
	<td colspan='2' class='rightborder'>52 hete</td>
	<td colspan='2' class='rightborder'>Ehónap</td>
	<td colspan='2' class='rightborder'>Előző hónap</td>
	<td colspan='2' class='rightborder'>12 hónapja</td>
	<td colspan='2' class='rightborder'>11 hónapja</td>
	<td colspan='2' class='rightborder'>10 hónapja</td>

</tr>

<!-- -->

<tr class='centered bold'>
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>

	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
		<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>


<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>


	
	
 
</tr>


<!-- -->
<!-- -->
<?

if($_GET[type] == 1)
{
	$basearray = $soldtypes;
	$criteriafield = "type";
	$orderbyfield = "added";
	$einactive = "";
}
else
{
	$basearray = $paidtypes;
	$criteriafield = "payment";
	$orderbyfield = "paid_date";
	$einactive = "AND inactive = 0";

}



	

foreach($basearray as $title => $criteria) { 
	
$prevmonths = array();
$prevweeks = array();	

if($criteria <> '')	
	$extraselect = "AND $criteriafield = $criteria";
		
	
?>
<tr class='right'>
	<td class='header'><?=$title?></td>

	<? foreach($days as $day) {
	



	$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE $orderbyfield >= '$day 00:00:00' AND $orderbyfield <= '$day 23:59:59' $extraselect $clr $einactive"));
	
	$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE $orderbyfield >= '$day 00:00:00' AND $orderbyfield <= '$day 23:59:59' $extraselect $clr $einactive"));


	 ?>
	
	<td class='lightgrey'><?=$curdate[count]?></td>
	<td class='lightgrey rightborder'><?=formatPrice($totals[count],0,1)?></td>
	
	<? } ?>
	
	<? foreach($weeks as $week) {
	
	
	if(in_array($week,$prevweeks) || $week > date("W"))
		$yearselect = $year-1;
	else
		$yearselect = $year;
		
//	if($criteria <> '')
		$prevweeks[] = $week;
	
	
	if($week >= 52)
		$weekerror = "AND $orderbyfield > '$yearselect-08-08 00:00:00'";
//	elseif($week == 1)
//		$weekerror = "AND $orderbyfield > '2012-12-25 00:00:00' ";
	else
		$weekerror = '';
		
	//echo "SELECT count(cid) AS count FROM customers WHERE week($orderbyfield,3) = '$week' AND year($orderbyfield) = '$yearselect' $extraselect $clr $einactive $weekerror<hr/>";
	
	$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE week($orderbyfield,3) = '$week' AND year($orderbyfield) = '$yearselect' $extraselect $clr $einactive $weekerror"));
		
		
	$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE week($orderbyfield,3) = '$week' AND year($orderbyfield) = '$yearselect' $extraselect $clr $einactive $weekerror"));
	
	
	 ?>
		<td><?=$curdate[count]?></td>
		<td class='rightborder'><?=formatPrice($totals[count],0,1)?></td>
	<? } ?>	

	<? foreach($months as $month) {
	
	
	if(in_array($month,$prevmonths) || $month > date("m"))
		$yearselect = $year-1;
	else
		$yearselect = $year;
		
	//if($criteria <> '')
		$prevmonths[] = $month;
	
	
//	echo "SELECT count(cid) AS count FROM customers WHERE month($orderbyfield) = '$month' AND year($orderbyfield) = '$yearselect' $extraselect AND inactive = 0<hr/>";
	$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE month($orderbyfield) = '$month' AND year($orderbyfield) = '$yearselect' $extraselect $einactive"));
	
	$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE month($orderbyfield) = '$month' AND year($orderbyfield) = '$yearselect' $extraselect $einactive"));

	 ?>
		<td class='lightgrey'><?=$curdate[count]?></td>
		<td class='lightgrey rightborder'><?=formatPrice($totals[count],0,1)?></td>
	<? } ?>	

</tr>
<!-- -->
<? } 
?>
</table>
<hr/>
<?
if($_GET[type] == 1)
{
$query = $mysql->query("SELECT * FROM customers  ORDER BY added DESC LIMIT 200");
echo "<h3>Utolsó 200 db eladott tétel</h3>";

//show last 200 items

}
else
{
	$query = $mysql->query("SELECT * FROM customers WHERE inactive_date <> '0000-00-00 00:00:00' AND inactive = 1 ORDER BY inactive_date DESC LIMIT 200");
	echo "<h3>Utolsó 200 db törölt tétel</h3>";

}


echo "<table>";


echo "<tr class='header'>";
			echo "<td colspan='2'></td>";
			echo "<td>Dátum</td>";
			echo "<td>Vásárló</td>";
			echo "<td>Partner</td>";
			echo "<td>Típus</td>";
			echo "<td>Fiz. mód.</td>";
			echo "<td>Eladási ár</td>";
		echo "</tr>";
		
		

$i=1;
	while($arr = mysql_fetch_assoc($query))
	{
	
	if($arr[postpone] == 1 && $offerArr[abroad] <> 1)
		$payment = $lang[t_check];
	elseif($arr[payment] == 1  || $arr[checkpaper_id] > 0) 
		$payment = $lang[transfer];
	elseif($arr[payment] == 2) 
		$payment = $lang[cash];
	elseif($arr[payment] == 3) 
		$payment = $lang[postpaid];
	elseif($arr[payment] == 4) 
		$payment = $lang[delivery];
	elseif($arr[payment] == 5) 
		$payment = $lang[t_check];
	elseif($arr[payment] == 6) 
		$payment = $lang[place];
	elseif($arr[payment] == 7) 
		$payment = $lang[online];
	elseif($arr[payment] == 8) 
		$payment = $lang[facebook];
	elseif($arr[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($arr[payment] == 10) 
		$payment = "OTP SZÉP kártya";
	elseif($arr[payment] == 11) 
		$payment = "MKB SZÉP kártya";
	elseif($arr[payment] == 12) 
		$payment = "K&H SZÉP kártya";
		
	if($arr[type] == 1) 
		$type = "Vatera";
	elseif($arr[type] == 2) 
		$type = "Teszvesz";
	elseif($arr[type] == 3) 
		$type = "Licittravel";
	elseif($arr[type] == 4) 
		$type = "Outlet";
	elseif($arr[type] == 5) 
		$type = "Pult";
	elseif($arr[type] == 6) 
		$type = "Lealkudtuk";
	elseif($arr[type] == 7) 
		$type = "Grando";
	elseif($arr[type] == 8) 
		$type = "Viszonteladó";

	if($arr[company_invoice] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		else
			$logo = 'ologo_small.png';
			



	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid]"));
	
	if($arr[sub_pid] > 0)
	{
		$spartner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[sub_pid]"));
		$spartner = " / ".$spartner[hotel_name];
	}
	else
	{
		$spartner = '';
	}
	
	if($arr[paid] == 1)
		$class = 'green';
	elseif($arr[inactive] == 1)
		$class = 'purple';
	else
		$class = '';
		echo "<tr class='$class'>";
			echo "<td align='center'>$i.</td>";
			echo "<td><img src='/images/$logo'/></td>";
			echo "<td>$arr[added]</td>";
			echo "<td><a href='/info/customer.php?cid=$arr[cid]' rel='facebox'><b>$arr[offer_id]</b></a></td>";
			echo "<td><a href='/preview.php?tid=$arr[offers_id]&type=offer' rel='facebox'><b>$partner[hotel_name] $spartner</b></a></td>";
			echo "<td>$type</td>";
			echo "<td>$payment</td>";
			echo "<td>".formatPrice($arr[orig_price])."</td>";
		echo "</tr>";
		$i++;
	}	
echo "</table>";
?>


</div>

</div>
<?


foot();
?>