<? 
include("inc/init.inc.php");
header("Content-Type: text/html; charset=UTF-8");


function sendnewsletter($subject,$body,$to,$name='',$fromemail = 'utak@indulhatunk.hu', $fromname = 'Indulhatunk.hu utazási portál' )
{
	global $outletmail;
	global $outletmailNew;
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("$fromname") . '?='; 
	
	$headers .= "From: $from <$fromemail>\r\n";


	/*$file = "newsletters/outlet/index.php";
	$fh = fopen($file, 'r');
	$data = fread($fh, filesize($file));
	fclose($fh);*/
	

	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	$t = explode(",",$to);
	
	foreach($t as $email)
	{
		mail(trim($email),$subject, $body ,iconv('utf-8','utf-8',$headers));
	}
}


$subject = 'Utazása mellé most egy belföldi wellness hétvégét adunk ajándékba!';


$url = 'http://indulhatunk.hu';



$offers= array(

	1 => array(
		'offer_url' => "http://utazas-torokorszag.indulhatunk.hu/grand-newport-bodrum-hotel-267061403",
		'offer_image' => "http://admin.indulhatunk.info/images/1.jpg",
		'offer_name' => "Grand Newport Bodrum Hotel",
		'offer_country' => "Törökország",
		'offer_country_image' => "turkish",
		'offer_region' => "Törökország, Bodrum",
		'offer_country_url' => "",
		'offer_category' => 4,
		'offer_board' => "All inclusive",
		'offer_days' => "7 éj / 8 nap",
		'discountprice' => "122900",
		'offer_mode' => "Repülő",
		'offer_price' => "122 900 Ft  / fő / 7 éjtől",
		'offer_from' =>"2012. június 18.",
 	),
 	
 	2 => array(
		'offer_url' => "http://utazas-gorogorszag.indulhatunk.hu/princess-flora-hotel-267134257",
		'offer_image' => "http://admin.indulhatunk.info/images/2.jpg",
		'offer_name' => "Princess Flora Hotel",
		'offer_country' => "Görögország",
		'offer_country_image' => "greece",
		'offer_region' => "Görögorország, Rodosz",
		'offer_country_url' => "",
		'offer_category' => 3,
		'offer_board' => "All inclusive",
		'offer_days' => "7 éj / 8 nap",
		'discountprice' => "129900",
		'offer_mode' => "Repülő",
		'offer_price' => "129 900 Ft  / fő / 7 éjtől",
		'offer_from' =>"2012. június 21.",
 	),

6 => array(
		'offer_url' => "http://utazas-egyiptom.indulhatunk.hu/grand-plaza-hotel-267109220",
		'offer_image' => "http://admin.indulhatunk.info/images/5.jpg",
		'offer_name' => "Grand Plaza Hotel",
		'offer_country' => "Egyiptom",
		'offer_country_image' => "egypt",
		'offer_region' => "Egyiptom, Hurghada",
		'offer_country_url' => "",
		'offer_category' => 4,
		'offer_board' => "All inclusive",
		'offer_days' => "7 éj / 8 nap",
		'discountprice' => "107900",
		'offer_mode' => "Repülő",
		'offer_price' => "107 900 Ft  / fő / 7 éjtől",
		'offer_from' =>"2012. június 26.",
 	),


5 => array(
		'offer_url' => "http://utazas-tunezia.azbaszna.be/marhaba-royal-salem-136334815",
		'offer_image' => "http://admin.indulhatunk.info/images/3.jpg",
		'offer_name' => "Marhaba Royal Salem",
		'offer_country' => "Tunézia",
		'offer_country_image' => "tunisia",
		'offer_region' => "Tunézia, Sousse",
		'offer_country_url' => "",
		'offer_category' => 4,
		'offer_board' => "All inclusive",
		'offer_days' => "7 éj / 8 nap",
		'discountprice' => "84990",
		'offer_mode' => "Repülő",
		'offer_price' => "84 900 Ft  / fő / 7 éjtől",
		'offer_from' =>"2012. június 8.",
 	),


	3 => array(
		'offer_url' => "http://utazas-gorogorszag.indulhatunk.hu/marbella-beach-hotel-267134242",
		'offer_image' => "http://static.indulhatunk.hu/uploaded_images/tour/1227356/267134242/215_215/01.jpg",
		'offer_name' => "Marbella Beach Hotel",
		'offer_country' => "Görögország",
		'offer_country_image' => "greece",
		'offer_region' => "Görögorország, Korfu",
		'offer_country_url' => "",
		'offer_category' => 5,
		'offer_board' => "Félpanzió",
		'offer_days' => "7 éj / 8 nap",
		'offer_mode' => "Repülő",
		'discountprice' => "207900",
		'offer_price' => "207 900 Ft  / fő / 7 éjtől",
		'offer_from' =>"2012. június 11.",
 	),



 	4 => array(
		'offer_url' => "http://utazas-torokorszag.indulhatunk.hu/long-beach-resort-spa-267068240",
		'offer_image' => "http://admin.indulhatunk.info/images/4.jpg",
		'offer_name' => "Long Beach Resort & Spa",
		'offer_country' => "Törökország",
		'offer_country_image' => "turkish",
		'offer_region' => "Törökország, Alanya",
		'offer_country_url' => "",
		'offer_category' => 5,
		'offer_board' => "Ultra all inclusive",
		'offer_days' => "7 éj / 8 nap",
		'offer_mode' => "Repülő",
		'discountprice' => "162900",
		'offer_price' => "162 900 Ft  / fő / 7 éjtől",
		'offer_from' =>"2012. június 6.",
 	),

	
);

function generateoffer($data)
{
	

	$discountprice = formatPrice($data[discountprice]*0.2);
	
	$stars = '';
	for($i=1;$i<=$data[offer_category];$i++)
	{
		$stars.="<img src='http://www.indulhatunk.hu/images/star.png'>";
	}
	
	$content = "
				<div style='padding:10px 20px 10px 20px;'>
					<!-- -->
					<table cellpadding='0' cellspacing='0' border='0' width='720'>
					<tr bgcolor='006595'>
						<td>
							<div style='padding:5px; color:white; font-weight:bold;'>
								<img src='http://admin.indulhatunk.info/images/$data[offer_country_image].png' style='float:left;'/>
								<div style='float:left; padding:3px 0 0 5px;'>$data[offer_country]</div>
								<div style='clear:both'></div>
						</td>
					</tr>
					<tr>
						<td>
							<div style='width:px; height:px; border:1px solid #cddfe7;padding:10px; margin:1px 0 0 0;'>
								<div style='float:left;'>
									<a href='$data[offer_url]?utm_source=indulhatunk_newsletter&utm_medium=email&utm_campaign=indulhatunk-1'><img src='$data[offer_image]' width='207' style='border:3px solid #cddfe7;' target='_blank'/></a>
								</div>
								<div style='float:left;margin:5px 0 0 10px;'>
									<a href='$data[offer_url]?utm_source=indulhatunk_newsletter&utm_medium=email&utm_campaign=indulhatunk-1' style='color:#0079b3;font-weight:bold; text-decoration:none;font-size:16px;'>$data[offer_name]</a>
									<div style='font-size:10px; color:#252525;padding:0 0 10px 0; border-bottom:1px solid #d4dee3;width:473px'>$data[offer_region]</div>
									
									<div style='font-size:12px;color:#252525;font-weight:bold;padding:5px 0 5px 0;'>Akciós ár: <span style='color:#fd6700;font-size:18px;'>$data[offer_price]</span></div>
									
									
										<div class='tourtable'>
							<table width='470'>
								<tr>
									<td>Úticél:
										<div class='bluetext'>$data[offer_region]</div></td>
									<td>Kategória:
										<div class='bluetext'>$stars</div></td>
									<td>Ellátás:
										<div class='bluetext'>$data[offer_board]</div></td>
								</tr>
								<tr>
									<td>Időtartam:
										<div class='bluetext'>$data[offer_days]</div></td>
									<td>Indulás:
										<div class='bluetext'>$data[offer_from]</div></td>
									<td>Utazás módja:
										<div class='bluetext'>$data[offer_mode]</div></td>
								</tr>
								<tr>
									<td colspan='2'>
									<div><a href='http://www.szallasoutlet.hu/?utm_source=indulhatunk_newsletter&utm_medium=email&utm_campaign=indulhatunk-1' target='_blank' style='text-decoration:none; color:#252525'><img src='http://admin.indulhatunk.info/images/osmall.png' style='float:left; width:90px;'/> <div style='float:left;font-size:12px;padding:15px 0 0 10px;'>levásárolható kedvezmény: </a></div>
								</div>
								<div style='clear:both;'></div>
									</td>
									<td align='center'><span style='font-size:16px;color:#0079b3;font-weight:bold;'>$discountprice</span></td>
								</tr>
							</table>
							</div>
							
								
									
								</div>
								<div style='clear:both;'></div>
							<center><a href='$data[offer_url]?utm_source=indulhatunk_newsletter&utm_medium=email&utm_campaign=indulhatunk-1' target='_blank'><img src='http://admin.indulhatunk.info/images/indulhatunk.png'/></a></center>
							</div>
							
						</td>
					</tr>
					
					</table>
					
					
					
					<!-- -->
				</div> ";


		return $content;
}
	$content = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
	<title>Indulhatunk.hu - A legjobb ajánlatok egy helyen!</title>
	<style>
		body, html { padding:0; margin:0; font-family:arial; }
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
		.tourtable { font-size:11px; margin:5px 0 0px 0; }
		
		.tourtable td { background-color:#f0f7fc; padding:5px; font-weight:bold; } 
		.bluetext { font-size:12px; color:#0079b3; font-weight:Bold; padding:3px 0 0 0;} 
	</style>
</head>
 <body bgcolor='#f0f7fc'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#f0f7fc' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:720px;margin:0 auto;'>
	<table width='720' cellpadding='0' cellspacing='0' border='0'>
		<tr align='center' style='text-align:center'>
			<td height='30'><center><a href='http://indulhatunk.com/hirlevel/' style='font-size:10px;text-align:center;'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért » </a></center></td>
		</tr>
		<tr>
			<td height='357'>
			
			<a href='http://www.indulhatunk.hu/?utm_source=indulhatunk_newsletter&utm_medium=email&utm_campaign=1' target='_blank'><img style='display:block' src='http://admin.indulhatunk.info/images/indheader.png' alt='Indulhatunk.hu - a legjobb ajánlatok egy helyen' title='Indulhatunk.hu - a legjobb ajánlatok egy helyen'/></a></td>
		</tr>
	
		<tr bgcolor='white' width='720'>
			<td>";
			
		//	for($i=1;$i<6;$i++) { 
			
						
			foreach($offers as $offer)		
				$content.=generateoffer($offer);		
			//	}
				
		$content.= "
			</td>
		</tr>
		<tr>
			<td align='center'><div style='padding:10px 0 0 0;font-size:10px'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a  <a href='http://indulhatunk.com/unsubscribe.php?id=#HASH#'>linkre kattintva</a> mondhatja le.<br/><br/></div></td>
		</tr>
	</table>
	</div>
	
</td>
</tr>
	
</table>

</body><html>";

/*
$query = $mysql->query("SELECT trim(email) as email FROM lmb GROUP BY trim(email) ORDER BY trim(email) ASC");
$g = 1;
while($arr = mysql_fetch_assoc($query))
{
	echo "$g $arr[email]\n";
	
	$content = str_replace("#HASH#",md5($arr[email]),$content);
	$content = str_replace("#EMAIL#",$arr[email],$content);
	sendnewsletter($subject,$content,"$arr[email]");
	//sendnewsletter($subject,$content,"mail@isuperg.com");
	$g++;
}
*/

echo $content;
?>