<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

userlogin();


$h = 2;

if($CURUSER["userclass"] < 50) {
	header("location: customers.php");
}
$cleared = $_GET[cleared];

if($cleared > 0)
	$clr = "AND customers.invoice_cleared <> '0000-00-00 00:00:00' AND customers.invoice_arrived_status = $cleared";
elseif($_GET[company] > 0)
	$clr = "AND customers.pid = $_GET[company]";
elseif($_GET[hotel] > 0)
	$clr = "AND customers.pid = $_GET[hotel]";
else
	$clr = "AND (customers.invoice_cleared = '0000-00-00 00:00:00') AND year(invoice_date) = 2012 AND customers.company_invoice = 'hoteloutlet'";


head("Számlák kezelése");
?>

<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
					
		<li><a href="?cleared=0"  class="<? if($_GET[cleared] == 0 || $_GET[cleared] == '') echo "current";?>">Elintézendő számlák</a></li>
		<li><a href="?cleared=1"  class="<? if($_GET[cleared] == 1) echo "current";?>">Díjbekérő, engedélyezett</a></li>
		<li><a href="?cleared=2"  class="<? if($_GET[cleared] == 2) echo "current";?>">Bemutatott számlák</a></li>
		<li><a href="?cleared=3"  class="<? if($_GET[cleared] == 3) echo "current";?>">Előlegszámlák</a></li>
		<li><a href="?cleared=4"  class="<? if($_GET[cleared] == 4) echo "current";?>">Végszámlák</a></li>
		
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<form method='get' action=''>
<select name='company' onchange='submit()' style='width:430px;'>
	<option value='0'>Cég neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(company_name) <> '' order by company_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[company_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[company_name]</option>";
		}
	?>
</select>
<select name='hotel' onchange='submit()'  style='width:430px;'>
	<option value='0'>Hotel neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(hotel_name) <> '' order by hotel_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[hotel_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[hotel_name]</option>";
		}
	?>
</select>
</form>
</center>
<hr/>
<?



$query = $mysql->query("SELECT * FROM customers WHERE cid > 0 AND customers.paid = 1 AND 
	customers.paid_date > '2011-04-03' AND 
	customers.invoice_created = 1 AND
	customers.facebook = 0 AND customers.inactive = 0  $clr AND pid <> 3003 AND pid <> 3121   GROUP BY invoice_number ORDER BY pid,invoice_date ASC");


	
if(mysql_num_rows($query) == 0)
	echo "<div class='redmessage'>Az adott partnerhez még nem készült számla</div>";
else
{
	echo "<table border=1>";
	echo "<tr class='header'>";
	echo "<td colspan='3'>Cég neve</td>";
	echo "<td>Adószám</td>";

	echo "<td>Kiállítandó</td>";
	echo "<td>Számlaszám</td>";
	echo "<td>Kiegyenlítve</td>";
	echo "<td>-</td>";
	echo "</tr>";
}


$mysql->query("UPDATE partners SET vtl_due = 0");



while($arr = mysql_fetch_assoc($query))
{
	$mysql->query("UPDATE partners SET vtl_due = 1 WHERE pid = $arr[pid]");
	
	if($prevpid <> $arr[pid] && $prevpid <> '')
	{
		echo "<tr><td colspan='8' style='background-color:black;height:1px;font-size:1px'></td></tr>";
	}
	$prevpid = $arr[pid];
	
	if($arr[invoice_arrived_status] == 1)
		$cls = 'orange';
	elseif($arr[invoice_arrived_status] == 2)
		$cls = 'blue';
	elseif($arr[invoice_arrived_status] == 3)
		$cls = 'green';
	elseif($arr[invoice_arrived_status] == 4)
		$cls = 'grey';
	else
		$cls = '';

	if($arr[invoice_arrived_status] == 1)
		$option1 = 'selected';
	else 
		$option1 = '';
	
	if($arr[invoice_arrived_status] == 2)
		$option2 = 'selected';
	else 
		$option2 = '';
		
	if($arr[invoice_arrived_status] == 3)
		$option3 = 'selected';
	else 
		$option3 = '';
		
	if($arr[invoice_arrived_status] == 4)
		$option4 = 'selected';
	else 
		$option4 = '';
		
		
	
	if($arr[company_invoice] == 'indulhatunk')
		$logo = 'ilogo_small.png';
	else
		$logo = 'ologo_small.png';
		
	$week = date("W",strtotime($arr[invoice_date]))-1;
	
		$year = date("Y",strtotime($arr[invoice_date]));
		
		$month = date("m",strtotime($arr[invoice_date]));
	
	//echo "$year-$month<hr/";
	
	if($week == 0)
		$week = 52;
				
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid]"));
	
	$total = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE invoice_number = '$arr[invoice_number]' AND invoice_number <> '' AND payment <> 6"));
	
	
	echo "$week. hét\t$partner[hotel_name]\t$arr[invoice_date]\t$arr[invoice_number]\t$total[total] >>>> $arr[invoice_cleared]<hr/>";
	
	
	echo "<tr class='$cls'>";
	echo "<td><img src='/images/$logo'/></td>";
	echo "<td align='center' width='30'>$week. hét</td>";
	echo "<td>$partner[company_name]<br/>$partner[hotel_name]</td>";
	
	if (strpos($partner[tax_no],'-1-') !== false) 
		$cls = 'red';
	else
		$cls = '';
	echo "<td width='95' class='$cls'>$partner[tax_no]</td>";
	echo "<td align='right'>".formatPrice($total[total],0,1)."</td>";
	echo "<td align='center'><a href='/invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf' target='_blank'>$arr[invoice_number]</a></td>";
	echo "<td  align='center'>
	
				<select  class='cleared_status' id='$arr[invoice_number]'>
					<option value='0'>elintézendő</option>
					<option value='1' $option1>díjbekérő</option>
					<option value='2' $option2>bemutatott (e-mail, fax)</option>
					<option value='3' $option3>előlegszáma</option>
					<option value='4' $option4>végszámla</option>
				</select>
			</td>";
	echo "<td><a href='/info/show_accounting.php?invoice_number=$arr[invoice_number]&pid=$arr[pid]' rel='facebox'><img src='/images/getinfo.png' width='25'/></a></td>";
	echo "</tr>";
	
	if($partner[pid] <> 3003)
		$weektotals[$week][] = $total[total];
		
	if($year == 2012)
	$totals[$arr[company_invoice]] = $totals[$arr[company_invoice]]+$total[total];
	
	
}

	echo "</table>";
	
debug($totals);
/*** show tax ***/
/*
 * General stats for invoices
*/
	if($CURUSER[userclass] == 255) {
		echo "<hr/><h2>ÁFA kimutatás </h2><table>";
		
		echo "<tr class='header'>";
				echo "<td>Hét</td>";
				echo "<td>Kiállítandó</td>";
				echo "<td>ÁFA</td>";
			echo "</tr>";
		
		//sort totals	
		@krsort($weektotals);
		
		$odd = 1;
		foreach($weektotals as $key => $week)
		{
			if($odd%2 == 0)
				$class = 'grey';
			else
				$class = '';
				
			$sum = array_sum($week);
			echo "<tr class='$class'>";
				echo "<td>$key. hét</td>";
				echo "<td align='right'>".formatPrice($sum)."</td>";
				echo "<td align='right'>".formatPrice($sum-($sum/1.18))."</td>";
			echo "</tr>";
			
			$sumsum = $sumsum+$sum;
			$odd++;
		}	
		$z++;	
		echo "<tr  class='header'>";
				echo "<td>Összesen</td>";
				echo "<td align='right'>".formatPrice($sumsum)."</td>";
				echo "<td align='right'>".formatPrice($sumsum-($sumsum/1.18))."</td>";
			echo "</tr>";
		echo "</table>";
	}
/* */

echo "</div></div>";

foot();
?>