<?
header('Content-type: text/html; charset=utf-8');


include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");


$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

$data[ip] = $_SERVER[REMOTE_ADDR];
$data[hash] = $_GET[affiliate];
$data[added] = 'NOW()';

$mysql->query_insert("followup",$data);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="hu-HU"> 
<head>
	<script src="http://admin.indulhatunk.info/inc/js/jquery.js" type="text/javascript"></script>
	<link href="style.css" media="screen" rel="stylesheet" type="text/css"/>
		<title>Indulhatunk.hu ajánlat</title>

  <link href="facebox/src/facebox.css" media="screen" rel="stylesheet" type="text/css" />
  <script src="facebox/lib/jquery.js" type="text/javascript"></script>
  <script src="facebox/src/facebox.js" type="text/javascript"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'facebox/src/loading.gif',
        closeImage   : 'facebox/src/closelabel.png'
      })
    })
  </script>

</script>
</head>
<body>
	<div class="container">
	 	<div id="header">
	 		<div class='headertext'>
	 			<b>Hozza ki a legtöbbet<br/> szállodájából! </b>
	 			Növeljük együtt eladott<br/> vendégéjszakái számát!
	 		</div>
	 	</div>
	 	<div id='buttons'>
	 		<a href='#'><b>Üdülési csekk</b>
<span class='small'>elszámolások intézése.</span></a>
	 		<a href='#' class='center'>
	 		<b>Alacsonyabb áfa</b>
<span class='small'>Számláink 18%-os áfát tartalmaznak.</span>
</a>
	 		<a href='#'>
	 		<b>Marketingtámogatás</b>
<span class='small'>Egy szignifikáns plusz Partnereink részére</span>
	 		</a>
	 		<div class='cleaner'></div>
	 	</div>
	 	<div class='content'>
	 		<div class='leftside'>
	 			<div class='title'>Együttműködési javaslatunk:</div>
	 			<div class='offer'>
	 			<b>A www.szallasoutlet.hu weboldal a belföldi, turisztikával foglalkozó outlet oldalak között Magyarországon piacvezető. </b><br/><br/>
				Üzemeltetője, az Indulhatunk.hu Kft. kiemelten jó kapcsolatainak és értékesítési technikáinak köszönhetően valóban különlegesen kedvezményes árakon kínál belföldi üdüléseket visszatérő és folyamatosan növekvő ügyfélkörének. <br/><br/>
				Ajánlataink több fórumon, saját tulajdonban lévő és aukciós oldalakon is sikeresen szerepelnek, fejlődésünk töretlen, jelenleg 15-20.000 vendégéjszakát értékesítünk belföldön havonta, és ez a szám folyamatosan növekszik.
	 			</div>
	 		</div>
	 		<div class='rightside'>
	 			<img src='images/outlet.png'/>
	 		</div>
	 		<div class='cleaner'></div>
	 		<div class='morebutton'><a href='#services' id="more" rel='facebox'>Ismerje meg szolgáltatásainkat</a></div>
	 		
	 		<div id="services" style='display:none;color:black;'>
	 			<span style='color:black'>
	 				<h1 style='font-size:14px;margin:0 0 0 25px;'>Marketingtámogatás – egy szignifikáns plusz Partnereink részére</h1>
				
<ul style='font-size:12px;width:600px;' id="morelist">	 			
	
<li><b>Partnereink csomagajánlatai megjelennek a Vatera és a TeszVesz nyitóoldalain.</b> Emberek milliói használják őket minden nap, ez a két weboldal együttesen 30.000.000.000 Ft forgalmat bonyolít évente. Ezen kívül saját értékesítési felületünkön, a turisztikai outlet oldalak között piacvezető www.szallasoutlet.hu weboldalon is megjelenik Partnerünk ajánlata.</li>
<li>A Vetarával közös brandünk oldalán, a  <b>www.lealkudtuk.hu-n az Indulhatunk.hu Kft. az egyedüli szolgáltató cég, mely turisztikai termékeket értékesíthet.</b></li>
<li><b>A Vatera és a TeszVesz nagy tömegeket ér el, ez nagyságrendekkel több vásárlót jelent, mint amennyit bármelyik utazásszervező cég tud biztosítani Magyarországon.</b> Értékesítési technikánk egyik sajátossága, hogy egyszerre csak néhány, 3-4 szálloda ajánlata szerepel az aukciós oldalakon. Ez a konkurenciával szemben hihetetlen előnyt biztosít Partnerünknek: mivel egyszerre csak néhány ajánlat szerepel a weboldalakon, a Vendég nem vész el az ajánlatok között. Ez azt jelenti, hogy Partnerünk nagyobb árbevételt könyvelhet el, mint ha egyként szerepelne több száz szálloda között egy utazásszervező kínálatában. A Vatera, a TeszVesz és a www.szallasoutlet.hu Magyarország leglátogatottabb turisztikai outlet honlapjai közé tartoznak.</li>
<li>A Vaterán szereplő egyik <a target="_blank" href="http://utazas-szabadido.vatera.hu/szallas_utazas/maria_hotel_balatonmariafurdo_2_ej_2_fo_qcf_1451145205.html
">ajánlatunkat</a> példaként ajánljuk figyelmébe. Figyelemre méltó, hogy a látogatók száma a licitzárásig meghaladta a 32.000-et, illetve az érdeklődők aktivitását is érdemes megnézni (kérdések-válaszok): egyáltalán nem szokatlan az ilyen mértékű érdeklődés Partnereink csomagjai iránt.</li>
<li><b>1.700.000 db hírlevél hetente és ez a mennyiség folyamatosan nő.</b> Minden Vatera, TeszVesz hírlevélben a top ajánlatok között megtalálhatóak a mi ajánlataink is.</li>
<li><b>Rendszeresen bővítjük ügyfélkörünket</b></li>
<li><b>Folyamatosan fejlesztjük és bővítjük a piacot. Hamarosan külföldi Vendégek részére is foglalhatóvá tesszük Partnereink csomagjait.</b></li>

</ul>
</span>
	 		</div>
	 		
	 		<div class='bluetitle'>Együttműködésünk keretein belül Partnereinknek a következő előnyöket ajánljuk:</div>
	 		
	 		<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>1</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>A foglalásokkal kapcsolatos adminisztratív teendők teljes körű elvégzése: befizettetések, kapcsolattartás a Vendéggel, számlázás, CRM tevékenység.</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->
	 		
	 		<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>2</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'><span style='font-size:20px;text-transform:uppercase;font-weight:bold;'>Üdülési csekk elszámolások intézése.</span></div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->

<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>3</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>
	 				Partnereink igényeinek megfelelően heti elszámolási rendszer: hetente fizetünk, partnerünk mindössze heti egy számlát állít ki felénk, azaz havonta maximum négy darabot. Partnerünk most jut bevételhez – a Vendég később érkezik.
	 				</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->

<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>4</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>Partnerünk minimalizálhatja, illetve maximalizálhatja az általunk eladandó csomagok mennyiségét. Flexibilisek vagyunk e tekintetben: amennyiben Partnerünk úgy látja, hetente változtathatja az eladandó szobák számát, az árat és a limitet is, illetve jogosult meghatározni a vendég érkezési idejét minden egyes foglalásnál. Ilyen módon elő-és utószezoni időszakban hétköznapokon, illetve egyéb alacsony töltöttségű időszakokban is maximalizálhatja bevételeit.</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->
	 		
	 		<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>5</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>Partnerünk joga a felárak meghatározása: úgy, mint hétvégi felár, félpanzió-, teljes panzió felár, szobatípus upgrade felár, gyermekfelár, pótágyfelár, illetve felárak kiemelt időszakokra (pl. húsvét). Ilyen módon Partnerünk növelheti eladási árait, és újabb extra bevételekhez juthat.</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->
<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>6</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>Alacsonyabb Áfa: új megállapodásunk szerint a számlák alacsonyabb, 18% áfát tartalmaznak majd, ezzel is csökkentve a 25% -os adóteher alá eső tételek külön könyvelését és számlázását.</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->

<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>7</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>Partnereink csomagajánlatai megjelennek a Vatera és a TeszVesz nyitóoldalain.</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->

<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>8</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>Országos marketingkampány keretein belül útjára indult a Vaterával közös brandünk, a www.lealkudtuk.hu.Az Indulhatunk.hu Kft. az egyedüli szolgáltató cég, mely a Vaterán és a TeszVeszen turisztikai termékeket értékesít.</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->

<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop'>
	 				<div class='number'>9</div>
	 			</div>
	 			<div class='itemcenter'>
	 			
	 				<div class='compensate'>1.700.000 db hírlevél hetente – tisztán az Ön célcsoportja számára a Vaterán és a Tesz Veszen keresztül – és ez a mennyiség folyamatosan nő. Minden hírlevélben a top ajánlatok között megtalálhatóak a mi ajánlataink is.</div>


	 			</div>
	 			<div class='itembottom'></div>
	 		</div>
	 		<!-- item -->
	 		
	 		
	 	</div>
	</div>
	
	<div style='width:100%; background:url(images/bg.png) no-repeat; background-position:0 1000px;'>
	
	<div id='footer'>
	<div class="container">
		<div class='footertitle'>Amit leendő Partnerünktől várunk:</div>
	<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop2'>
	 				<div class='number2'>1</div>
	 			</div>
	 			<div class='itemcenter2'>
	 			
	 				<div class='compensate'>Speciális árak – Partnereink a rack rate-nél jelentősen alacsonyabb árakat biztosítanak számunkra- hétköznapokra és alacsony foglaltsági időszakokra vonatkoztatva.</div>


	 			</div>
	 			<div class='itembottom2'></div>
	 		</div>
	 		<!-- item -->
	 			<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop2'>
	 				<div class='number2'>2</div>
	 			</div>
	 			<div class='itemcenter2'>
	 			
	 				<div class='compensate'>
	 				Igazi érték a megnövekedett foglaltsági mutatókért - mindenki elégedett, ha a Vendég elégedett: legtöbb Partnerünk nem tünteti fel legalacsonyabb árait saját weboldalán együttműködésünk időtartama alatt. Vendégeinknek így különösen megéri a közös együttműködésünk keretein belül megállapított árakon lefoglalni üdülését. Így igazán a legjobb árakon tudunk számukra minőséget kínálni. 
	 				</div>


	 			</div>
	 			<div class='itembottom2'></div>
	 		</div>
	 		<!-- item -->
	 			<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop2'>
	 				<div class='number2'>3</div>
	 			</div>
	 			<div class='itemcenter2'>
	 			
	 				<div class='compensate'>Minél nagyobb a kedvezmény mértéke – annál többet tudunk kihozni együttműködésünkből.</div>
	 			</div>
	 			<div class='itembottom2'></div>
	 		</div>
	 		<!-- item -->
	 			<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop2'>
	 				<div class='number2'>4</div>
	 			</div>
	 			<div class='itemcenter2'>
	 			
	 				<div class='compensate'>A csomagajánlatok különböző időintervallumokat fedhetnek le – ez a szerződéstől, illetve a piactól függ.  A foglalási időszak, amit Partnerünk ajánl, minimálisan 2-3 hónapot takarhat, de felső limit nincs. Akár egy évig, vagy még tovább is érvényesek lehetnek a számunkra adott speciális árak. </div>
	 			</div>
	 			<div class='itembottom2'></div>
	 		</div>
	 		<!-- item -->
	 			<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop2'>
	 				<div class='number2'>5</div>
	 			</div>
	 			<div class='itemcenter2'>
	 			
	 				<div class='compensate'>Partnerünk minimalizálhatja, illetve maximalizálhatja az általunk eladandó csomagok mennyiségét. Flexibilisek vagyunk e tekintetben: amennyiben Partnerünk úgy látja, hetente változtathatja az eladandó szobák számát, az árat és a limitet is. </div>
	 			</div>
	 			<div class='itembottom2'></div>
	 		</div>
	 		<!-- item -->
	 			<!-- item -->
	 		<div class='item'>
	 			<div class='itemtop2'>
	 				<div class='number2'>6</div>
	 			</div>
	 			<div class='itemcenter2'>
	 			
	 				<div class='compensate'>	És egy plusz előny, melyből a legjobbat hozhatjuk ki együtt: Partnerünk a szerződés megkötésével egyidejűleg egy adatlapot tölt ki. Ebben természetesen meghatározhat felárakat, úgy, mint hétvégi felár, félpanzió-, teljes panzió felár, szobatípus upgrade felár, gyermekfelár, pótágyfelár, illetve felárak kiemelt időszakokra (pl. húsvét). Ilyen módon Partnerünk növelheti eladási árait, és újabb extra bevételekhez juthat. </div>
	 			</div>
	 			<div class='itembottom2'></div>
	 		</div>
	 		<!-- item -->
	 		
	 		
	 					<div class='footertext'>
				<i>Hozzon jó döntést:</i><br/><br/>
				
				<b>Szerződött Partnerünk számára tehát jelentős marketingtámogatást és kényelmes plusz szolgáltatásokat nyújtunk, melyeket nem kell megfizetnie, sőt: árbevételt könyvelhet el még a foglalások tekintetében leggyengébb időszakokban is, mikor normál esetben visszaesik a foglalások mennyisége.
				</b>
				<br/><br/>
Növelje eladott vendégéjszakái számát, minimalizálja költségeit, és maximalizálja segítségünkkel marketingje hatékonyságát - megéri.<br/><br/> 

Amennyiben kérdése, kérése lenne, kérem, keressen minket az alábbi elérhetőségek bármelyikén:

			</div>
			<div class='contact'>
				Parányi Petra<br/>
				<a href="mailto:petra.paranyi@indulhatunk.hu">petra.paranyi@indulhatunk.hu</a><br/>
				0670-930-7816<br/><br/>
			</div>
		</div>
		
		
		</div>

	</div>
</body>
</html