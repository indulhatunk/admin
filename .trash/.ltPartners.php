<?
/*
 * partners.php 
 *
 * partners page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();

if($CURUSER[userclass] < 5)
	header("location:index.php");
	
$edit = $_POST[editid];
$editid = (int)$_GET[edit];

if($editid > 0) {
	$query = $mysql->query("SELECT * FROM partners WHERE pid = $editid");
	$editarr = mysql_fetch_assoc($query);
}
elseif($editid == 0 && $_GET[add] == 1)
{
	$maxpartner = mysql_fetch_assoc($mysql->query("SELECT max(partner_id) as max FROM partners"));
	$editarr[partner_id] = $maxpartner["max"]+1;
}
$data[coredb_id] = $_POST[coredb_id];
$data[coredb_package] = $_POST[coredb_package];


$data[foreign_bank] = $_POST[foreign_bank];
$data[foreign_bank_address] = $_POST[foreign_bank_address];
$data[foreign_swift] = $_POST[foreign_swift];
$data[foreign_iban] = $_POST[foreign_iban];
$data[foreign_account_no] = $_POST[foreign_account_no];
$data[foreign_account_name] = $_POST[foreign_account_name];
$data[partner_type] = 'licittravel';


$data[company_name] = $_POST[company_name];
$data[hotel_name] = $_POST[hotel_name];
$data[url] = clean_url2($_POST[hotel_name]);

$data[tax_no] = $_POST[tax_no];
$data[partner_id] = $_POST[partner_id];
$data[contact] = $_POST[contact];
$data[address] = $_POST[address];
$data[zip] = $_POST[zip];
$data[city] = $_POST[city];


$data[invoice_name] = $_POST[invoice_name];
$data[invoice_zip] = $_POST[invoice_zip];
$data[invoice_city] = $_POST[invoice_city];
$data[invoice_address] = $_POST[invoice_address];

$data[phone] = $_POST[phone];
$data[reception_phone] = $_POST[reception_phone];
$data[email] = $_POST[email];
$data[website] = $_POST[website];

$data['yield'] = $_POST['yield'];
$data[yield_vtl] = $_POST[yield_vtl];
$data[username] = $_POST[username];
$data[password] = $_POST[password];
$data[account_no] = $_POST[account_no];
$data[reception_email] = $_POST[reception_email];
$data[vat_extra_service] = $_POST[vat_extra_service];

$data[calculated_arrest] = $_POST[calculated_arrest];
$data[calculated_multiplier] = $_POST[calculated_multiplier];

if($_POST[property_check] == 'on')
	$data[property_check] = 1;
else
	$data[property_check] = 0;

if($_POST[property_wellness] == 'on')
	$data[property_wellness] = 1;
else
	$data[property_wellness] = 0;
	
if($_POST[property_mountain] == 'on')
	$data[property_mountain] = 1;
else
	$data[property_mountain] = 0;
	
if($_POST[property_water] == 'on')
	$data[property_water] = 1;
else
	$data[property_water] = 0;
	
if($_POST[property_castle] == 'on')
	$data[property_castle] = 1;
else
	$data[property_castle] = 0;
	
if($_POST[property_child] == 'on')
	$data[property_child] = 1;
else
	$data[property_child] = 0;

$data[property_main] = $_POST[property_main];

if($edit == "" && $data[company_name] <> "") {
	$mysql->query_insert("partners",$data);
	$msg = "Sikeresen felvitte a partnert!";
}
elseif($edit > 0) {
	$mysql->query_update("partners",$data,"pid=$edit");
	$msg = "Sikeresen szerkesztette a partnert!";
}
head("Partnerek kezelése");
mysql_query("SET NAMES 'utf8'"); 

if($_GET[type] == 'maybe')
{
	$extraPartner = 'AND last_contract <  DATE_SUB(CURDATE(), INTERVAL 365 DAY) AND last_contract >=  DATE_SUB(CURDATE(), INTERVAL 548 DAY) ';

}
elseif($_GET[type] == 'inactive')
{
	$extraPartner = 'AND last_contract <  DATE_SUB(CURDATE(), INTERVAL 548 DAY) ';

}
else
{
	$extraPartner = 'AND last_contract >=  DATE_SUB(CURDATE(), INTERVAL 365 DAY)';
}


$searchquery = $_POST[search];

if($searchquery == '')
{
	$qr = "SELECT * FROM partners WHERE userclass < 100 $extraPartner ORDER BY partner_id ASC";
}
else
{
	$qr = "SELECT * FROM partners WHERE company_name LIKE '%$searchquery%' OR hotel_name LIKE '%$searchquery%' OR email LIKE '%$searchquery%'   ORDER BY partner_id ASC";
}
$query = mysql_query($qr); 
?>
<fieldset style="border:1px solid black;-moz-opacity:0.8;opacity:0.80;width:215px;height:24px;padding:5px;position:fixed;top:10px;right:10px;background-color:white;">
<form method="post" id="sform" action="partners.php">
<input type="text" name="search"  style='height:20px;width:150px;margin:1px 5px 0 0;padding:0;float:left;' value="<?=$cookie?>" /><a href="#"; style='display:block; background:none; color:black; border:1px solid black; height:10px;padding:5px; margin:1px 0 0 0;float:left;width:30px;float:left;' id='submit'>Ok</a>
</form>
</fieldset>
<?
//echo "<h1><a href=\"http://admin.indulhatunk.info\">Vatera adminisztrációs felület</a> &raquo; Partnerek kezelése</h1>";


//echo "<div class='subMenu'><a href=\"partners.php?add=1\">Új partner felvitele</a> <a href=\"new_contracts.php\" class='grey'>Folyamatban</a> <a href=\"contracts.php?type=outlet\" class='green'>Outlet</a><a href=\"contracts.php?type=lmb\" class='yellow'>LMB</a> <a href=\"contracts.php?type=facebook\" class='blue'>FaceBook</a>   <div class='cleaner'></div></div>";

//echo "<div class='subMenu'><a href=\"partners.php\">Partner</a> <a href=\"partners.php?type=maybe\" class='grey'>Majdnem döglött akták</a> <a href=\"partners.php?type=inactive\" class='red'>Döglött akták</a> <a href=\"https://admin.indulhatunk.hu/imap/contract_date.php\" class='yellow'>Frissítés</a>  <div class='cleaner'></div></div>";

if($msg <> '')
	echo "<div class='notify'>$msg</div>";

$add = $_REQUEST[add];

if($add == 1 || $editid > 0)

{
?>
<script>
$(document).ready(function() {
	$(".accountNo").mask("99999999-99999999-99999999");
});
</script>
<div class="partnerForm" <?=$show?>>
	<form method="POST" action="partners.php">
		<input type="hidden" name="editid" value="<?=$editarr[pid]?>">
	<fieldset style='width:520 px;'>
		<legend>Partnerek kezelése</legend>
	<ul>
		<li><label>CoreDB ID:</label><input type="text" name="coredb_id" value="<?=$editarr[coredb_id]?>"/></li>
		<li><label>CoreDB Fo csomag:</label><input type="text" name="coredb_package" value="<?=$editarr[coredb_package]?>"/></li>

		<li><label>Ügyfélszám:</label><input type="text" name="partner_id" value="<?=$editarr[partner_id]?>"/></li>
		<li><label>Cég neve:*</label><input type="text" name="company_name" value="<?=$editarr[company_name]?>"/></li>
		<li><label>Hotel neve:</label><input type="text" name="hotel_name" value="<?=$editarr[hotel_name]?>"/></li>
		<li><label>Adószám:</label><input type="text" name="tax_no" value="<?=$editarr[tax_no]?>"/></li>
		<li><label>Kapcsolattartó neve:</label><input type="text" name="contact" value="<?=$editarr[contact]?>"/></li>
		<li><label>Bankszámlaszám:</label><input type="text" name="account_no" id="account_no" class="accountNo" value="<?=$editarr[account_no]?>"/></li>
		<li><label>Weboldal:</label><input type="text" name="website" value="<?=$editarr[website]?>"/></li>

	<hr/>
		<li><label>Bank neve:</label><input type="text" name="foreign_bank"  value="<?=$editarr[foreign_bank]?>"/></li>
		<li><label>Bank címe:</label><input type="text" name="foreign_bank_address"  value="<?=$editarr[foreign_bank_address]?>"/></li>
		<li><label>Bank SWIFT:</label><input type="text" name="foreign_swift" value="<?=$editarr[foreign_swift]?>"/></li>
		<li><label>Bank IBAN:</label><input type="text" name="foreign_iban" value="<?=$editarr[foreign_iban]?>"/></li>
		<li><label>Külföldi számlaszám:</label><input type="text" name="foreign_account_no" value="<?=$editarr[foreign_account_no]?>"/></li>
		<li><label>Számlatulajdonos:</label><input type="text" name="foreign_account_name"  value="<?=$editarr[foreign_account_name]?>"/></li>
	<hr/>
		<li><label>Cím:</label><input type="text" name="zip" value="<?=$editarr[zip]?>"   style='width:30px;' class='numeric'/><input type="text" name="city"  style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" name="address" class="small2" style='width:320px;' value="<?=$editarr[address]?>" /></li>
		<li><label>Telefonszám:</label><input type="text" name="phone" value="<?=$editarr[phone]?>"/></li>
		<li><label>Recepció:</label><input type="text" name="reception_phone" value="<?=$editarr[reception_phone]?>"/></li>
		<li><label>Recepció e-mail:</label><input type="text" name="reception_email" value="<?=$editarr[reception_email]?>"/></li>
	<hr/>
		<li><label>Számlázási név:</label><input type="text" name="invoice_name" value="<?=$editarr[invoice_name]?>"/></li>
		<li><label>Számlázási cím:</label><input type="text" name="invoice_zip" value="<?=$editarr[invoice_zip]?>"  style='width:30px;' class='numeric'/><input type="text" name="invoice_city" style='width:140px;margin:0 1px 0 1px'  value="<?=$editarr[invoice_city]?>" /><input type="text" name="invoice_address" class="small2" style='width:320px;' value="<?=$editarr[invoice_address]?>" /></li>
	<hr/>

		<li><label>E-mail cím:</label><input type="text" name="email" value="<?=$editarr[email]?>"/></li>
		<li><label>Jutalék LMB:</label><input type="text" name="yield" value="<?=$editarr['yield']?>"/></li>
		<li><label>Jutalék VTL:</label><input type="text" name="yield_vtl" value="<?=$editarr[yield_vtl]?>"/></li>
		
		<li><label>Kalkulált árrés:</label><input type="text" name="calculated_arrest" value="<?=$editarr[calculated_arrest]?>"/></li>
		<li><label>Kalk. árfolyamszorzó:</label><input type="text" name="calculated_multiplier" value="<?=$editarr[calculated_multiplier]?>"/></li>

		<li><label>Felhasználónév:</label><input type="text" name="username" value="<?=$editarr[username]?>"/></li>
		<li><label>Jelszó:</label><input type="text" name="password" value="<?=$editarr[password]?>"/></li>
		
		<li><label>Fő típusa:</label>
			<select name="property_main">
				<option value="1" <?if($editarr[property_main]==1)echo"selected";?>>Üdülésicsekk</option>
				<option value="2" <?if($editarr[property_main]==2)echo"selected";?>>Wellness</option>
				<option value="3" <?if($editarr[property_main]==3)echo"selected";?>>Gyermekbarát</option>
				<option value="4" <?if($editarr[property_main]==4)echo"selected";?>>Hegyvidék</option>
				<option value="5" <?if($editarr[property_main]==5)echo"selected";?>>Vízpart</option>
				<option value="6" <?if($editarr[property_main]==6)echo"selected";?>>Kastélyszálló</option>

			</select>
		</li>
		<li><label>Félpanzió ÁFA kategória:</label>
			<select name="vat_extra_service">
				<option value="0" <?if($editarr[vat_extra_service]==0)echo"selected";?>>18%</option>
				<option value="1" <?if($editarr[vat_extra_service]==1)echo"selected";?>>25%</option>
			</select>
		</li>

		<li><label>Üdülésicsekk:</label><input type="checkbox" name="property_check" <?if($editarr[property_check]==1)echo "checked";?>/></li>
		<li><label>Wellness:</label><input type="checkbox" name="property_wellness" <?if($editarr[property_wellness]==1)echo "checked";?>/></li>
		<li><label>Gyermekbarát:</label><input type="checkbox" name="property_child" <?if($editarr[property_child]==1)echo "checked";?>/></li>
		<li><label>Hegyvidék:</label><input type="checkbox" name="property_mountain" <?if($editarr[property_mountain]==1)echo "checked";?>/></li>
		<li><label>Vízpart:</label><input type="checkbox" name="property_water" <?if($editarr[property_water]==1)echo "checked";?>/></li>
		<li><label>Kastélyszálló:</label><input type="checkbox" name="property_castle" <?if($editarr[property_castle]==1)echo "checked";?>/></li>
		
		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</fieldset>
	</form>
</div>
<?
foot();
die;
}
?>

<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href="?add=1">Új partner felvitele</a></li> <!-- href must be unique and match the id of target div -->
						<li><a href="partners.php" class="default-tab current">Partnerek</a></li>
						<li><a href="new_contracts.php">Folyamatban</a></li>
						<li><a href="contracts.php?type=outlet">Outlet</a></li>
						<li><a href="contracts.php?type=lmb">LMB</a></li>
						<li><a href="contracts.php?type=facebook">FaceBook</a></li>
						<li><a href="partners.php?type=maybe">Majdnem döglött akták</a></li>
						<li><a href="partners.php?type=inactive">Dögött akták</a></li>
						<li><a href="https://admin.indulhatunk.hu/imap/contract_date.php" rel='facebox'>Frissítés</a></li>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>

<?
echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td>-</td>";
//	echo "<td>CoreDB ID</td>";
//	echo "<td>CoreDB Csomag</td>";
	echo "<td>No.</td>";
	echo "<td>Cég neve</td>";
//	echo "<td>Adószám</td>";
	echo "<td>Kapcsolattartó</td>";
//	echo "<td>Bankszamlaszam</td>";
//	echo "<td>Cím</td>";
	echo "<td>Telefonszám</td>";
	//echo "<td width='50'>E-mail</td>";
	echo "<td>LMB</td>";
	echo "<td>VTL</td>";
	//echo "<td>Felhasználónév</td>";
	//echo "<td>Jelszó</td>";
	echo "<td>Info</td>";
	//echo "<td>Voucherek</td>";
echo "</tr>";


while($arr = mysql_fetch_assoc($query)) {


$nowdate = strtotime(date("Y-m-d"));
$thendate = strtotime($arr[last_contract]); 
$datediff = ($nowdate - $thendate); 
$days = round($datediff / 86400);
	
	if($days >= 300)
		$class = 'red';
	elseif($days < 300 && $days >= 180)
		$class = 'blue';
	elseif($days < 180 && $days >= 80)
		$class = 'green';
	else
		$class = '';
		
	if($_GET[type] == 'maybe')
		$class = 'grey';
	if($_GET[type] == 'inactive')
		$class = 'red';
	echo "<tr class=\"$class\">";

		echo "<td><a href=\"?edit=$arr[pid]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='30'/></a></td>";
		//echo "<td>$arr[coredb_id]</td>";
		//echo "<td>$arr[coredb_package]</td>";
		if($arr[coredb_id] == 0)
			$coreDB = 0;
		else
			$coreDB = '';
		echo "<td align='right'>$coreDB <br/>$arr[partner_id]</td>";
		echo "<td><a href='info/partner.php?cid=$arr[pid]' rel='facebox'><b>$arr[company_name] $arr[hotel_name]</b></a> </td>";
	//	echo "<td>$arr[tax_no]</td>";
		echo "<td>$arr[contact]</td>";
	//	echo "<td>$arr[account_no]</td>";
	//	echo "<td>$arr[zip] $arr[city] $arr[address]</td>";
		echo "<td>$arr[phone]</td>";
//		echo "<td width='50'>$arr[email]</td>";
		echo "<td align='right'>$arr['yield']%</td>";
		echo "<td align='right'>$arr[yield_vtl]%</td>";
		//echo "<td>$arr[username]</td>";
		//echo "<td>$arr[password]</td>";
		
		if($arr[coredb_id] > 0)
		{
			$addpartner = '';
		}
		else
		{
			$addpartner = '<br/><a href="createcoredb.php">[coredb-be]</a>';
		}
		echo "<td><a href='contracts.php?pid=$arr[partner_id]'>[szerződések]</a><br/><a href='own_vouchers.php?pid=$arr[partner_id]'>[voucherek]</a><br/><a href='fullhouse.php?pid=$arr[pid]'>[teltház]</a>$addpartner</td>";
	//	echo "<td></td>";
	echo "</tr>";
	
}
echo "</table>";
?>
</div>
</div>
<?
foot();
?>