<?header('Content-type: text/html; charset=utf-8');?>
<style>
body { border:0; margin:0; font-family:arial; font-size:12px;}

#container { width:521px; }

a img { border:0; }

.cleaner { clear:both; }

.headerleft, .headercenter, .headerright { float:left; }

.headercenter { width:299px; height:153px; background-color:#2278b6; color:white; font-weight:bold; }

.left { float:left;}

#footer { color:#77a6af; }

.footercenter { width:482px; height:219px; }

.footer { background-color:#23383d; width:520px; }

.contentcenter { width:469px; color:#23383d;}

.item { width:417px; margin:0 auto; }

.separator { padding:10px 0 10px 0; }

.image img { border:4px solid #2278b6; }

.description { width:275px; padding:0 0 0 10px; }

.description a { color:#2278b6; font-weight:bold; text-decoration:none; font-size:14px;}

.blue {  color:#2278b6; font-size:14px; font-weight:bold; }

.red { color:red; font-size:14px; font-weight:bold; }

.big { font-size:16px; }
</style>
<body>
<div id="container">
	<!-- header start -->
	<div id="header">
			<div><a href="#"><img src="http://admin.indulhatunk.info/newsletter/images/headertop.png"/></a></div>
			
			<div>
				<div class="headerleft"><img src="http://admin.indulhatunk.info/newsletter/images/headerleft.png"/></div>
				
				<div class="headercenter">Lorem ipsum akarmi</div>
				
				<div class="headerright"><img src="http://admin.indulhatunk.info/newsletter/images/headerright.png"/></div>
				
				<div class="cleaner"></div>
			</div>
			<div><img src="http://admin.indulhatunk.info/newsletter/images/headerbottom.png"/></div>
	</div>
	<!-- header end -->
	<!-- content start -->
	<div id="content">
		<div>
			<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/contentleft.png"/></div>
			<div class="left contentcenter">
				<!-- item -->
				<div class="item">
					<div class="left image"><a href="#"><img src="http://admin.indulhatunk.info/newsletter/images/image.png"/></a></div>
					<div class="left description">
						<a href="#">Tisza Balneum Thermal Hotel****</a><br/><br/>
						A Tisza-tó régió első konferencia és wellness szállodája.
						A tökéletes harmónia és a nyugalom várja a Hotel Balneumban közvetlenül a Tisza-tó partján, Tiszafüreden!
						<br/><br/>
						<span class="blue">Eredeti ár: <strike>100.000 Ft</strike></span><br/>
						<span class="red">Akciós ár: <span class="big">36.000 Ft</span></span>
					</div>
					<div class="cleaner"></div>
					<div class="separator"><img src="http://admin.indulhatunk.info/newsletter/images/separator.png"/></div>
				</div>	
				
				<!-- item end-->
			</div>
			<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/contentright.png"/></div>
			
			<div class="cleaner"></div>
		</div>
		<div><img src="http://admin.indulhatunk.info/newsletter/images/contentbottom.png"/></div>
		<div class="cleaner"></div>
	</div>
	<!-- content end -->
	<!-- footer start -->
	<div id="footer">
		<div><a href="#"><img src="http://admin.indulhatunk.info/newsletter/images/footertop.png"/></a></div>
		<div class="footer">
			<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/footerleft.png"/></div>
			<div class="left footercenter">fsdfdsafasfasfs</div>
			<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/footerright.png"/></div>
			<div class="cleaner"></div>
		</div>
		<div><a href="#"><img src="http://admin.indulhatunk.info/newsletter/images/footerbottom.png"/></a></div>
	</div>
	<!-- footer end -->
</div>
</body>