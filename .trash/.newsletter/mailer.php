<?
die;
header('Content-type: text/html; charset=utf-8');

function sendEmail($subject,$body,$to)
{
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= "From: LastMinuteBelfold <lmb@indulhatunk.info>\r\n";

	// Mail it
	mail($to,iconv('utf-8','ISO-8859-2',$subject), $body ,iconv('utf-8','ISO-8859-2',$headers));
}


include("../inc/config.inc.php");
include("../inc/mysql.class.php");





//die; 
$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();





$body = '<style>
body { border:0; margin:0; font-family:arial; font-size:12px;}

#container { width:521px; }

a img { border:0; }

.cleaner { clear:both; }

.headerleft, .headercenter, .headerright { float:left; }

.headercenter { width:299px; height:153px; background-color:#2278b6; color:white; font-size:24px;}
.headercenter1 { font-size:16px; font-weight:bold;}
.left { float:left;}

#footer { color:#77a6af; }

.footercenter { width:482px; height:219px; font-size:11px;}

.footer { background-color:#23383d; width:520px; }

.footer a { color:#77a6af; text-decoration:none; }

.contentcenter { width:469px; color:#23383d;}

.item { width:417px; margin:0 auto; }

.separator { padding:10px 0 10px 0; }

.image img { border:4px solid #2278b6; height: 124px; width: 124px }

.description { width:275px; padding:0 0 0 10px; }

.description a { color:#2278b6; font-weight:bold; text-decoration:none; font-size:14px;}

.blue { color:#2278b6; font-size:14px; font-weight:bold; }

.red { color:red; font-size:14px; font-weight:bold; }

.big { font-size:16px; }</style>
<body>
<div id="container">
<!- header start ->
<div id="header">
<div><a href="http://admin.indulhatunk.info/newsletter/lmb12_28.php"><img src="http://admin.indulhatunk.info/newsletter/images/headertop.png"></a></div>

<div>
<div class="headerleft"><img src="http://admin.indulhatunk.info/newsletter/images/headerleft.png"></div>

<div class="headercenter">Évzáró bulik, Újévi partik, melyeket kár lenne kihagyni
<div class="headercenter1"><p>Hihetetlen last minute ajánlatok várnak az ország legjobb wellness szállodáiba.<br> Üdülési csekked is beválthatod!</p></div></div>



<div class="headerright"><img src="http://admin.indulhatunk.info/newsletter/images/headerright.png"></div>

<div class="cleaner"></div>
</div>
<div><img src="http://admin.indulhatunk.info/newsletter/images/headerbottom.png"></div>
</div>
<!- header end ->
<!- content start ->
<div id="content">
<div>
<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/contentleft.png"></div>
<div class="left contentcenter">
<!- item ->
<div class="item">
<div class="left image"><a href="http://www.last-minute-belfold.hu/M%C3%A9g-3-napod-van/Zalakaros/"  target="_blank"><img src="http://static.last-minute-belfold.hu/uploaded_images/accomodation/55961/41733/137_137/01.jpg"></a></div>
<div class="left description">
<a href="http://www.last-minute-belfold.hu/M%C3%A9g-3-napod-van/Zalakaros/" target="_blank">Zalakaros</a><br><br>
<strong>Fogynak a szobák! </strong><br><br>
Ne várj az utolsó pillanatig, mert lehet, hogy addigra elfogynak abban a szállodában a szabad szobák, ahova szeretnél menni.


<br><br>
<span class="blue">Eredeti ár: <strike>57.000 Ft</strike></span><br>
<span class="red">Akciós ár: <span class="big">37.050 Ft</span></span>
</div>
<div class="cleaner"></div>
<div class="separator"><img src="http://admin.indulhatunk.info/newsletter/images/separator.png"></div>
</div>
<div class="item">
<div class="left image"><a href="http://www.last-minute-belfold.hu/M%C3%A9g-3-napod-van/Balaton/"  target="_blank"><img src="http://static.last-minute-belfold.hu/uploaded_images/accomodation/825560/825815/137_137/01.jpg"></a></div>
<div class="left description">
<a href="http://www.last-minute-belfold.hu/M%C3%A9g-3-napod-van/Balaton/" target="_blank">Balaton</a><br><br>
Partnereink az utolsó napokban a szabad szobáikat folyamatosan növekvő kedvezményekkel értékesítik oldalunkon, de csak a készlet erejéig.
<br><br>
<span class="blue">Eredeti ár: <strike>54.900 Ft</strike></span><br>
<span class="red">Akciós ár: <span class="big">35.685 Ft</span></span>
</div>
<div class="cleaner"></div>
<div class="separator"><img src="http://admin.indulhatunk.info/newsletter/images/separator.png"></div>
</div> <div class="item">
<div class="left image"><a href="http://www.last-minute-belfold.hu/M%C3%A9g-3-napod-van/" target="_blank"><img src="http://static.last-minute-belfold.hu/uploaded_images/accomodation/250561/250676/137_137/01.jpg"></a></div>
<div class="left description">
<a href="http://www.last-minute-belfold.hu/M%C3%A9g-3-napod-van/" target="_blank">Last Minute Belföld</a><br><br>
Csak nálunk foglalhatod le az utolsó szabad szállodai szobákat, az aktuális kedvezményes csomagajánlatoknál jóval nagyobb kedvezmények mellett.
<br><br>
<span class="blue">Eredeti ár: <strike>49.500 Ft</strike></span><br>
<span class="red">Akciós ár: <span class="big">24.750 Ft</span></span>
</div>
<div class="cleaner"></div>
<div class="separator"><img src="http://admin.indulhatunk.info/newsletter/images/separator.png"></div>
Cégünk folyamatosan azon dolgozik, hogy megtaláljuk Neked a legjobb ajánlatokat, amiket aztán jelentős kedvezményekkel az utolsó héten megvehetsz.
</div>

<!- item end->
</div>
<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/contentright.png"></div>

<div class="cleaner"></div>
</div>
<div><img src="http://admin.indulhatunk.info/newsletter/images/contentbottom.png"></div>
<div class="cleaner"></div>
</div>
<!- content end ->
<!- footer start ->
<div id="footer">
<div><a href="#"><img src="http://admin.indulhatunk.info/newsletter/images/footertop.png"></a></div>
<div class="footer">
<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/footerleft.png"></div>
<div class="left footercenter"><strong>Jó tudni!</strong> <br>Nálunk foglalsz, de a szállodában fizetsz.<br>
A visszaigazolást már a szállodától kapod, itt csak lekéred.
<br><br>
<strong>Kíváncsiak vagyunk a véleményedre</strong> <br>Van ötleted, hogy hogyan lehetne javítani a hírlevelünkön, vagy mely szállodákba szeretnél foglalni? Küldj nekünk emailt. vtl@indulhatunk.hu
<br><br>
<strong>Adatvédelem és titoktartás</strong> <br>
Ezt a hírlevelet azért küldtük Neked, mert ezt korábban engedélyezted. Amennyiben többet nem akarsz hasonló emailt kapni, egyszerűen kattints az alábbi linkre. Szeretnénk kihangsúlyozni, hogy cégünk nem adja tovább adataid harmadik félnek. <a href="http://admin.indulhatunk.info/newsletter" target="_blank">Leíratkozás</a>
 <br>
 <br>
<center>Szerzői jog 2010 LsatMinuteBelföld. Minden jog fentartva. <br>Ezt az emailt az indulhatunk.hu kft küldte, 2141 Csömoör Szőlő köz 3.
</center></div>
<div class="left"><img src="http://admin.indulhatunk.info/newsletter/images/footerright.png"></div>
<div class="cleaner"></div>
</div>
<div><a href="#"><img src="http://admin.indulhatunk.info/newsletter/images/footerbottom.png"></a></div>
</div>
<!- footer end -->
</div>
</body>';
$subject = "Sporoljon az ev vegi ajanlatinkkal";


$qr = $mysql->query("SELECT email FROM customers GROUP BY email");

while($arr = mysql_fetch_assoc($qr))
{
	echo $arr[email]."<hr/>";
	sendEmail($subject,$body,$arr[email]);

}
//group by;





?>