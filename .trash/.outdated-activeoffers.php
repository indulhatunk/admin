<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER[userclass] < 50)
	header("location: index.php");
	
head("Aktív ajánlatok listázása");

	$qr = $mysql->query("SELECT offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE offers.start_date <= NOW() AND offers.end_date > NOW() $extrareseller ORDER BY partners.hotel_name ASC");
?>

<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="#" class='current'>Vatera ajánlatok</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>


<?
echo "<table>";
	echo "<tr class='header'>";
			echo "<td>Hotel</td>";
			echo "<td width='260'>Ajánlat</td>";
			echo "<td>Vatera -tól</td>";
			echo "<td>Vatera -ig</td>";
			echo "<td>Érvényesség</td>";
			echo "<td>Alapár</td>";
		echo "</tr>";
		
	while($arr = mysql_fetch_assoc($qr))
	{
		if($arr[sub_partner_id] > 0)
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[sub_partner_id]"));
		else
			$partner = $arr;
			
		echo "<tr>";
			echo "<td>$partner[hotel_name]</td>";
			echo "<td width='260'>$arr[name]</td>";
			echo "<td>".str_replace("0000-00-00",'',str_replace("00:00:00",'',$arr[vatera_from]))."</td>";
			echo "<td>".str_replace("0000-00-00",'',str_replace("00:00:00",'',$arr[vatera_to]))."</td>";
			echo "<td>$arr[expiration_date]</td>";
			echo "<td align='right'>".formatPrice($arr[actual_price])."</td>";
		echo "</tr>";
	}
echo "</table></div></div>";
foot();


?>