<?
/*
 * weekly.php 
 *
 * the weekly stats page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

$h = 2;

if($CURUSER["userclass"] <> 255) {
	header("location: customers.php");
}

//last_week & year
if($_GET[week] == '' || $_GET[year] == '')
{
	$week = date("W")-1;
	$year = date("Y");
	
	if($week == 0)
	{
		$week = 52;
		$year = $year-1;
	}
}
else
{
	$week = $_GET[week];
	$year = $_GET[year];
}
//tax changes

	if($week == 52 && $year == 2011)
		$tax = 1.27;
	elseif($year >= 2012)
		$tax = 1.27;
	else
		$tax = 1.25;
		
$checkArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(check_value) AS whiteSum FROM customers  WHERE paid = 1 AND week(customers.check_arrival,3) = $week AND YEAR(customers.check_arrival) = $year  AND customers.inactive=0  AND customers.facebook = 0 AND company_invoice = 'indulhatunk' "));//AND invoice_created <> 1 
$checkyield = $checkArr[whiteSum]*0.073;


	$itemQuery = $mysql->query("
			SELECT 
				customers.transfer_date,
				customers.invoice_cleared,
				customers.invoice_number,
				partners.coredb_id,
				partners.foreign_swift,
				partners.account_no,
				partners.hotel_name,
				partners.pid,
				partners.company_name,
				partners.tax_no,
				partners.phone,
				partners.email,
				partners.zip,
				partners.address,
				partners.city,
				partners.yield_vtl 
			FROM 
				customers
		INNER JOIN partners ON partners.pid = customers.pid 
		WHERE 
			customers.paid = 1 AND 
			(
				(week(customers.paid_date,3) = $week AND
				YEAR(paid_date) = $year AND
				customers.payment <> 5)  
			OR 
			(
				week(customers.check_arrival,3) = $week AND
				YEAR(check_arrival) = $year AND
				customers.payment = 5
			) 
			) 
			AND
				customers.paid_date > '2011-04-02 00:00:00' AND
				customers.facebook = 0 AND
				customers.inactive = 0  AND 
				customers.company_invoice = 'indulhatunk'
		   GROUP BY customers.pid ORDER BY partners.company_name ASC");
		    

	$header.= "<table border='1'>";
	$header.= "<tr class='header'>";
	$header.= "<td colspan='2'>Cég neve</td>";
	$header.= "<td align='right'>DB</td>";
	$header.= "<td align='right'>Bevétel</td>";
	$header.= "<td align='right'>Helyszínen</td>";
	$header.= "<td align='right'>Jutalék</td>";
	$header.= "<td align='right'>Utalandó</td>";
	$header.= "<td align='right'>Számlaszám</td>";
	$header.= "<td align='right' width='70'>ÜCS</td>";
	$header.= "<td align='right' width='70'>ÜCS jut.</td>";
	$header.= "</tr>";

$h = 1;
$totalrows = 0;
while($partnerArr = mysql_fetch_assoc($itemQuery)) {

	$itemsQuery = $mysql->query("SELECT orig_price,payment,check_value,name,offer_id,plus_days,offers_id,pid FROM customers WHERE 
		pid = $partnerArr[pid] AND 
		customers.paid = 1 AND 
		(
			(
				week(customers.paid_date,3) = $week AND
				YEAR(paid_date) = $year AND
				customers.payment <> 5
			)  
			OR 
			(
				week(customers.check_arrival,3) = $week AND
				YEAR(check_arrival) = $year AND
				customers.payment = 5
			) 
			) 
			AND
				customers.facebook = 0 AND
				 customers.inactive = 0 AND 
				customers.company_invoice = 'indulhatunk'");
	
	$i=0;
	$total = 0;
	$place = 0;
	$subtotal = 0;	
	$days = 0;
	$chk = 0;
	$chkreal= 0;
	$checkcompensation = 0;
	$customers = '';
	$placerow = '';
	$placerowtop = '';
	
	while($itemArr = mysql_fetch_assoc($itemsQuery)) {
		

		//helyszini fizetes
		if($itemArr[payment] == 6)
		{	
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
			$place = $place+$itemArr[orig_price];
			//do not add payment to place			
			$price = 0;
		}
		
		//udulesi csekk fizetes
		elseif($itemArr[payment] == 5)
		{
			$checkcompensation = $checkcompensation+($itemArr[orig_price]*((7.5)/100));
			$subtotal=$subtotal+($itemArr[orig_price]*(($partnerArr[yield_vtl])/100));
			
			$chk = $chk+$itemArr[check_value];	
			$chkreal = $chk*0.925;
			
			$chktotal = $chktotal + $itemArr[check_value];
			$chkrealtotal = $chkrealtotal + $itemArr[check_value]*0.925;
			
			
			$days = $days+$day[days];
			//get nights
			$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
		
			if($itemArr[plus_days] > 0)
				$days = $days+1;

		}
		else
		{
			
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
		
			$days = $days+$day[days];
			//get nights
		$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
		
		if($itemArr[plus_days] > 0)
			$days = $days+1;
		}
		
		$total = $total+$itemArr[orig_price];
		$totaltotal = $totaltotal + $itemArr[orig_price];
		$i++;
		$g++;
		
		if($itemArr[payment] == 6)
		{
			$placerow.='<tr>';
			$placerow.= "<td>$i</td>";
			$placerow.= "<td>$itemArr[offer_id]</td>";
			$placerow.= "<td>$itemArr[name]</td>";
			$placerow.= '</tr>';
		}
		else
		{
			
			$customers.= '<tr>';
			$customers.= "<td>$i</td>";
			$customers.= "<td>$itemArr[offer_id]</td>";
			$customers.= "<td>$itemArr[name]</td>";
			$customers.= '</tr>';
		}
	}	
	
	$tr = $total-$place-$subtotal*$tax;
	
	if($tr >= 0)
	{
	
//	if($partnerArr[transfer_date] <> '0000-00-00 00:00:00')
//		$class = 'green';
//	else
//		$class = ''; 
		
	$row = "<tr class='$class'><td>".$partnerArr[company_name]."</td>";
	$row.= "<td>$partnerArr[hotel_name]</td>";
	$row.= "<td align='center'>$i</td>"; //count of items
	$row.= "<td align='right'>".formatPrice($total,0,1)."</td>";
	$row.= "<td align='right'>".formatPrice($place,0,1)."</td>";
	$row.= "<td align='right'>".formatPrice(round($subtotal*$tax,2),0,1)."</td>";
	$row.= "<td align='right'>".formatPrice($tr,0,1)."</td>";
	$row.= "<td align='right'><a href='/invoices/vatera/".str_replace("/","_",$partnerArr[invoice_number]).".pdf' target='_blank'>$partnerArr[invoice_number]</a></td>";
	$row.= "<td align='right'>".formatPrice($chk,0,1)."</td>";
	$row.= "<td align='right'>".formatPrice($chkreal,0,1)."</td>";
	
	$row.= "</tr>";
	
	$rows.= $row;
	
	$net = $net+$subtotal;
	$gross = $gross+$subtotal*$tax;
	$totalplace = $totalplace+$place;
	
		if($gross >= 0 && $partnerArr[foreign_swift] == '')
		{
			
			if($partnerArr[pid] <> 3003)
			{
					$transaction_item[value] = $tr;
					$transaction_item[account_no] = $partnerArr[account_no];
					$transaction_item[partner_id] = $partnerArr[coredb_id];
					$transaction_item[partner_name] = removeSpecialChars(trim($partnerArr[company_name]));
					$transaction_item[partner_address] = removeSpecialChars($partnerArr[address]);
					$transaction_item[comment] = "$week heti elszamolas";
					$tritems[] = $transaction_item;
			}
		}	
		$invoice_text = "A számla pénzügyi teljesítést NEM IGÉNYEL";
		$letter_text = formatPrice(abs($tr))."-ot fogunk elutalni az Önök számlájára:";

	}
	//if some has to pay for us
	else
	{
		$rows.= "<tr class='red'><td>".$partnerArr[company_name]." </td>";
		$rows.= "<td>$partnerArr[hotel_name]  ($partnerArr[pid])</td>";
		$rows.= "<td align='center'>$i</td>";
		$rows.= "<td align='right'>".formatPrice($place,0,1)."</td>";
		$rows.= "<td align='right'>".formatPrice($place,0,1)."</td>";
		$rows.= "<td align='right'>". formatPrice(round($subtotal*$tax,2),0,1)."</td>";
		$rows.= "<td align='right'>".formatPrice(abs($tr+$place),0,1)."</td>";		
		$rows.= "<td  align='right'><a href='/invoices/vatera/".str_replace("/","_",$partnerArr[invoice_number]).".pdf' target='_blank'>$partnerArr[invoice_number]</a></td>";
		$rows.= "<td  align='right'>".formatPrice($chk,0,1)."</td>";
		$rows.= "<td  align='right'>".formatPrice($chkreal,0,1)."</td>";

		$rows.= "</tr>";
		$total = $total-$place;
		$invoice_text = "A számla pénzügyi teljesítést IGÉNYEL";
		$letter_text = "<b><font color='red'>".formatPrice(abs($tr))."-ot kérjük átutalni szíveskedjen az alábbi bankszámlánkra:</font></b><br/><br/>
			Indulhatunk.hu Kft<br/>
			Erste Bank<br/>
			11600006 - 00000000 - 41873494";
	}
	//because we don't make invoice for those clients who paid on place
	$minustotal = $total - $place;
	if($days == 0)
		$d = 1;
	else
		$d = $days; 
				
	if($place > 0)
		$pltext = "Helyszínen fizetve: <b>".formatPrice($place)."</b><br/>";
	else
		$pltext = '';
	
	$format = 'Y.m.d.'; 
	$datetime = date ("Y-m-d"); 
	$dtm = date ( $format, strtotime ( '-7 day' . $datetime ) );

	$singlegross = round($minustotal/$d);
	$mysql->query("UPDATE customers SET invoice_single_price_gross = $singlegross WHERE invoice_number = '$partnerArr[invoice_number]' ");
	
	if($placerow <> '')
	{
		$placerowtop = "<tr style='font-weight:bold;text-align:center;'>
						<td colspan='3'><b>Helyszínen fizet:</b></td>
						</tr>";
	}
	$body = "
		<font size='5'><b>Az alábbi elszámolás az INDULHATUNK KFT részéről érkezik!</b></font><br/>Kérjük, hogy a számlákat is erre a cégre állítsák majd ki.<br/><br/>
		Az alábbiakban részletezzük a $week. heti elszámolásunkat.<br/><br/>
		Az elmúlt időszakban összesen <b>$i db</b> utalványt értékesítettünk <b>".formatPrice($total)."</b> értékben.<br/><br/>

		<b>Elszámolás:</b><br/>

Jutalék számlák összesen: <b>".formatPrice(round($subtotal*$tax,2))."</b><br/>
$pltext
Nálunk összegyűlt pénz: <b>".formatPrice($total-$place)."</b><br/><br/>


A $partnerArr[invoice_number] számú jutalékszámlát az alábbi linkre kattintva töltheti le: <a href='https://admin.indulhatunk.hu/invoices/dl/".md5($partnerArr[invoice_number]."#FWf#CEFA#SDF343sf#")."'><b>$partnerArr[invoice_number] sz. számla letöltése</b></a><br/><br/>

<b>Az egyenlegeink összevezetése után:</b><br/><br/>
$letter_text <br/><br/>


	<table border='1'  cellspacing='0' cellpadding='0' style='font-size:12px; color:black;'>
		<tr style='font-weight:bold;text-align:center;'>
			<td>TESZOR szám</td>
			<td>Teljesítés dátuma</td>
			<td>Megnevezés</td>
			<td>Menny.</td>
			<td>Menny. egys.</td>
			<td>ÁFA</td>
			<td>Nettó egys.</td>
			<td>Bruttó egys.</td>
			<td>Nettó össz.</td>
			<td>Bruttó össz.</td>
		</tr>
		<tr>
			<td>55.10.10</td>
			<td>$dtm</td>
			<td>Szállás</td>
			<td align='center'>$days</td>
			<td align='center'>éjszaka</td>
			<td align='right'>18 %</td>
			<td align='right'>".formatPrice(($minustotal/$d)/1.18)."</td>
			<td align='right'>".formatPrice($minustotal/$d)."</td>
			<td align='right'>".formatPrice($minustotal/1.18)."</td>
			<td align='right'>".formatPrice($minustotal)."</td>
		</tr>
	</table>
<br/>
Kérjük, hogy mihamarabb készítsék el számlájukat, melyet postán, faxon vagy emailban várunk az <a href='mailto:attila.forro@indulhatunk.hu'>attila.forro@indulhatunk.hu</a> címre.<br/>
A gyorsabb ügyintézés érdekében az a levél tárgyában hivatkozzon az alábbi számlaszámra: $partnerArr[invoice_number].
<br/><br/>

A $week. héten alábbi vevők lesznek az utalással elszámolva:

<br/><br/>
<table border='1'  cellspacing='0' cellpadding='0' style='font-size:12px; color:black;'>
		<tr style='font-weight:bold;text-align:center;'>
			<td>#</td>
			<td>Sorszám</td>
			<td>Vásárló neve</td>
		</tr>
		$customers
		$placerowtop
		$placerow
</table>
<br/><br/>
Üdvözlettel:<br/>
Forró Tamás<br/>
<a href='http://indulhatunk.hu'>indulhatunk.hu Kft</a>
";

	if($_GET[showletter] == 1)
		$rows.= "<tr class='grey'><td colspan='10'>$body</td></tr>";

	
	if($_GET[mail] == 1 && $_GET[sure] == 1)
	{
		$message = array();
		$message[pid] = $partnerArr[pid];
		$message[added_by] = $CURUSER[username];
		$message[added] = 'NOW()';
		$message[message] = $body;
		$message[message_email] = $partnerArr[email];
		$mysql->query_insert("messages",$message);
 		sendEmail("$week. heti elszámolás a(z) $partnerArr[hotel_name] részére",$body,"mail@isuperg.com",$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		sendEmail("$week. heti elszámolás a(z) $partnerArr[hotel_name] részére",$body,"billing@indulhatunk.hu",$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		sendEmail("$week. heti elszámolás a(z) $partnerArr[hotel_name] részére",$body,$partnerArr[email],$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		echo "mail sent<br/>";
	}
}

$g= $g;

$footer.= "<tr class='blue'>";
	$footer.= "<td colspan='8'>Üdülési csekk</td>";
	$footer.= "<td align='right'>".formatPrice($checkArr[whiteSum],0,1)."</td>";
	$footer.= "<td align='right'>".formatPrice($checkyield,0,1)."</td>";
$footer.= "</tr>";
	
	
$footer.= "<tr class='header'>";
	$footer.= "<td colspan='2'>Összesen</td>";
	$footer.= "<td align='right'>$g</td>";
	$footer.= "<td align='right'>".formatPrice($totaltotal,0,1)."</td>";
	$footer.= "<td align='right'>".formatPrice($totalplace,0,1)."</td>";
	$footer.= "<td align='right'>".formatPrice($gross-$checkyield,0,1)."</td>";

	$trans = round($totaltotal-$totalplace-$gross);
	$transfer = formatPrice($trans,0,1);

	$footer.= "<td  align='right'>$transfer</td>";
	$footer.= "<td  align='center'>-</td>";
	
	$footer.= "<td  align='center'>".formatPrice($chktotal,0,1)."</td>";
	$footer.= "<td  align='center'>".formatPrice($chktotal-$chkrealtotal,0,1)."</td>";
	$footer.= "</tr>";
$footer.= "</table>";

//add our own yield to the transfer array
	$tr = $gross-$checkyield;
		
	$transaction_item[value] = $tr;
	$transaction_item[account_no] = "11600006-00000000-38168594";
	$transaction_item[partner_id] = 1;
	$transaction_item[partner_name] = "Indulhatunk.hu Kft.";
	$transaction_item[partner_address] = "Váci utca 9.";
	$transaction_item[comment] = "$week heti elszamolas";
	$tritems[] = $transaction_item;


if($_GET[showtransfer] == 1)
{
    $data = generateTransferFile('indulhatunk',$tritems)."</textarea>";
	header("Content-type: text/plain");
	header("Content-Disposition: attachment;filename=atutal-$week.121");
	header('Pragma: no-cache');
	header('Expires: 0');
	echo $data;
	die;
}
else
{

//last_week & year
if($_GET[week] == '' || $_GET[year] == '')
{
	$week = date("W")-1;
	$year = date("Y");
	
	if($week == 0)
	{
		$week = 52;
		$year = $year-1;
	}	
}
else
{
	$week = $_GET[week];
	$year = $_GET[year];
}


head("$week. heti VTL elszámolás");


if($_GET[mail] == 1 && $_GET[sure] <> 1)
{
	echo message("Biztosan el szeretné küldeni a heti VTL (INDULHATUNK) leveleket? <a href='?showletter=1&mail=1&sure=1'><b>Igen</b></a> | <a href='?showletter=1'><b>Nem</b></a>");
	foot();
	die;
}

echo "<h1>".$week.". heti elszámolás</h1>";
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					
		<li><a href="weekly.php" class="current">Indulhatunk.hu Kft.</a></li>
		<li><a href="weekly2.php"  class="">Hotel Outlet Kft.</a></li>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<?
echo "<div style='float:left'><form method='get' action='weekly.php'>";
	echo "<select name='year'>";
	
	if($year == 2010)
		$selected1 = 'selected';
		
	if($year == 2011)
		$selected2 = 'selected';
	
	if($year == 2012)
		$selected3 = 'selected';
		
		echo "<option value='2011' $selected2>2011</option>";
		echo "<option value='2012' $selected3>2012</option>";
	echo "</select>";
	
	if($year == '')
		$year = date("Y");
	echo "<select name='week'>";
		for($i=1;$i<53;$i++)
		{
			if($week == $i)
				$weekselected = 'selected';
			else
				$weekselected = '';
				
			if($year == 2011 && $i >= 14)
				echo "<option value='$i' $weekselected>$i</option>";
			elseif($year > 2011)
				echo "<option value='$i' $weekselected>$i</option>";
			else
				echo "";
		}
	echo "</select>";
	echo "<input type='submit' value='mutasd'/>";
	echo "</form></div><div style='float:left;padding:5px 0 0 10px;'><a href='weekly.php?showtransfer=1&year=$year&week=$week'><b>$week. heti átutalási megbízás letöltése</b></a> | <a href='?showletter=1&year=$year&week=$week'><b>leveleket megtekint</b></a> | <a href='?showletter=1&mail=1'><b>Leveleket elküld</b></a>   </div><div class='cleaner'></div><hr/>";

	echo $header;
	echo $rows;
	echo $footer;
?>
</div></div>
<?
foot();
}
?>