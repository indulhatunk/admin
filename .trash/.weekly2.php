<?
/*
 * weekly.php 
 *
 * the weekly stats page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();


echo "<table border='1'>";

$h = 2;
if($CURUSER["userclass"] <> 255) {
	header("location: customers.php");
}

//last_week & year
if($_GET[week] == '' || $_GET[year] == '')
{
	$week = date("W")-1;
	$year = date("Y");
	
	if($week == 0)
	{
		$week = 52;
		$year = $year-1;
	}
}
else
{
	$week = $_GET[week];
	$year = $_GET[year];
}
//tax changes

if($week == 52 && $year == 2011)
	$tax = 1.27;
elseif($year >= 2012)
	$tax = 1.27;
else
	$tax = 1.25;
		
		

$week2minus = $week - 2;

$year2minus = $year;
$checkArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(check_value) AS whiteSum FROM customers  WHERE paid = 1 AND week(customers.check_arrival,3) = $week2minus AND YEAR(customers.check_arrival) = $year   AND customers.inactive=0  AND customers.facebook = 0 AND company_invoice = 'hoteloutlet' "));//AND invoice_created <> 1 
$checkyield = $checkArr[whiteSum]*0.073;

	$itemQuery = $mysql->query("SELECT
			 customers.invoice_cleared,
			 customers.transfer_date,
			 customers.invoice_cleared,
			 customers.invoice_number,
			 partners.coredb_id,
			 partners.foreign_swift,
			 partners.account_no,
			 partners.hotel_name,
			 partners.pid,
			 partners.pp_disable,
			 partners.company_name,
			 partners.tax_no,
			 partners.phone,
			 partners.email,
			 partners.zip,
			 partners.address,
			 partners.city,
			 partners.yield_vtl 
			FROM 
				customers
		INNER JOIN partners ON partners.pid = customers.pid 
		WHERE 
			customers.paid = 1 AND 
			(
				(
					week(customers.paid_date,3) = $week AND
					YEAR(paid_date) = $year AND
					customers.payment <> 5
				)  
				OR 
				(
					week(customers.check_arrival,3) = $week2minus AND
					YEAR(check_arrival) = $year AND
					customers.payment = 5
				) 
			
			) AND
			customers.paid_date > '2011-04-02 00:00:00' AND
			customers.facebook = 0 AND
			customers.inactive = 0  AND 
			customers.company_invoice = 'hoteloutlet'
			
		    GROUP BY customers.pid ORDER BY partners.company_name ASC");
		    

$header.= "<table border='1'>";

	$header.= "<tr class='header'>";
	$header.= "<td colspan='2'>Cég neve</td>";
	$header.= "<td align='right'>DB</td>";
	$header.= "<td align='right'>Összesen</td>";
	$header.= "<td align='right'>Helyszínen</td>";
	$header.= "<td align='right'>Jutalék</td>";
	$header.= "<td align='right'>Fizetendő</td>";
	$header.= "<td align='right'>Számlaszám</td>";
	$header.= "<td align='right' width='70'>ÜCS</td>";
	$header.= "<td align='right' width='70'>ÜCS jut.</td>";
	$header.= "</tr>";

$h = 1;
$totalrows = 0;
while($partnerArr = mysql_fetch_assoc($itemQuery)) {

	//query to list users with 18% tax content
	$itemsQuery = $mysql->query("
	
		SELECT * FROM customers
		WHERE 
			pid = $partnerArr[pid] AND 
			customers.paid = 1 AND 
			(
				(
					week(customers.paid_date,3) = $week AND
					YEAR(paid_date) = $year AND 
					customers.payment <> 5
				)  
				OR 
				(
					week(customers.check_arrival,3) = $week2minus AND
					YEAR(check_arrival) = $year AND
					customers.payment = 5
				) 
			) 
			AND
				customers.facebook = 0 AND
				customers.inactive = 0 AND 
				customers.company_invoice = 'hoteloutlet' AND
				(payment = 5 OR invoice_name <> '')");
	
	$i=0;
	$total = 0;
	$place = 0;
	$subtotal = 0;	
	$days = 0;
	$chk = 0;
	$chkreal= 0;
	$checkcompensation = 0;
	$customers = '';
	$placerow = '';
	$placerowtop = '';
	$currtotal = 0;
	$ttincome = 0;
	$invoice_number = '';
	
	while($itemArr = mysql_fetch_assoc($itemsQuery)) {
		
	$invoice_date = $itemArr[invoice_date];
		//place
		if($itemArr[payment] == 6)
		{	
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
			$place = $place+$itemArr[orig_price];
			$price = 0;
			
		if($_GET[debug] == 1)
		{	echo "<tr>
				<td>$partnerArr[hotel_name]</td>
				<td>$itemArr[offer_id]</td>
				<td>$itemArr[paid_date]</td>
				<td></td>
				<td>$itemArr[name]</td>
				<td>Helyszinen</td>
				<td>18%</td>
				<td>$itemArr[invoice_number]</td>
			</tr>";
		}
		
		}
		elseif($itemArr[payment] == 5)
		{
			$checkcompensation = $checkcompensation+($itemArr[orig_price]*((7.5)/100));
			$subtotal=$subtotal+($itemArr[orig_price]*(($partnerArr[yield_vtl])/100));
			
			$chk = $chk+$itemArr[check_value];	
			$chkreal = $chk*0.925;
			
			$chktotal = $chktotal + $itemArr[check_value];
			$chkrealtotal = $chkrealtotal + $itemArr[check_value]*0.925;
			
			if($_GET[debug] == 1)
			{
			echo "<tr>
				<td>$partnerArr[hotel_name]</td>
				<td>$itemArr[offer_id]</td>
				<td>$itemArr[paid_date]</td>
				<td></td>
				<td>$itemArr[name]</td>
				<td>UCS</td>
				<td>18%</td>
				<td>$itemArr[invoice_number]</td>
			</tr>";
			}
			
			$days = $days+$day[days];
			//get nights
			$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
			//echo "$partnerArr[hotel_name] | $itemArr[offer_id] <br/>";
		
			if($itemArr[plus_days] > 0)
				$days = $days+1;

		}
		else
		{
			if($_GET[debug] == 1)
			{
			echo "<tr>
				<td>$partnerArr[hotel_name]</td>
				<td>$itemArr[offer_id]</td>
				<td>$itemArr[paid_date]</td>
				<td></td>
				<td>$itemArr[name]</td>
				<td>EGYEB</td>
				<td>18%</td>
				<td>$itemArr[invoice_number]</td>
			</tr>";
			}
			
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
			$days = $days+$day[days];
			//get nights
			$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
			if($itemArr[plus_days] > 0)
			$days = $days+1;
		}
		
		$total = $total+$itemArr[orig_price];
		$totaltotal = $totaltotal + $itemArr[orig_price];
		$i++;
		$g++;
		
		if($itemArr[payment] == 6)
		{
			$placerow .='<tr>';
			$placerow .= "<td>$i</td>";
			$placerow .= "<td>$itemArr[offer_id]</td>";
			$placerow .= "<td>$itemArr[name]</td>";
			$placerow .= '</tr>';
		}
		else
		{
		if($itemArr[payment] == 5 || $itemArr[invoice_name] <> '')
				$tx = '18%';
			else
				$tx = 'ANK';
		$customers .='<tr>';
			$customers .= "<td>$i</td>";
			$customers .= "<td>$itemArr[offer_id]</td>";
			$customers .= "<td>$itemArr[name]</td>";
			$customers .= "<td>$tx</td>";
		$customers .= '</tr>';
		
		}
	
	}	
	
	$currtotal = $total;
	$dtotal = $days;
	$ttotal = $totaltotal;
	/*******/
	//query to determine users with 0% invoice
	$itemsQuery = "
		SELECT * FROM customers WHERE 
			pid = $partnerArr[pid] AND 
			customers.paid = 1 AND 
			(
				(
					week(customers.paid_date,3) = $week AND
					YEAR(paid_date) = $year AND
					customers.payment <> 5
				)
			)
			AND
				customers.facebook = 0 AND
				customers.inactive = 0 AND 
				customers.company_invoice = 'hoteloutlet' AND
				(payment <> 5 AND invoice_name = '') ORDER BY customers.cid ASC";
	
	//debug($itemsQuery);
	
//	$howmanyPostponed = mysql_num_rows($mysql->query("SELECT * FROM customers WHERE 	YEAR(customers.postpone_date) = $year2minus AND pid = $partnerArr[pid] "));
	
	
	$itemsQuery = $mysql->query($itemsQuery);	
	$icnt = mysql_num_rows($itemsQuery);
	
	$itemcount = 1;
	
	
	
	
	$fourtypercent = round(($icnt)*0.4);
	//echo "$icnt db / $fourtypercent db <hr/>";
		
	while($itemArr = mysql_fetch_assoc($itemsQuery)) {
	
	

	//if postponed, than add to the existing array
	if($itemArr[postpone_date] <> '0000-00-00 00:00:00')
	{
	
		/*
		//add postponed items to total
	//	echo "postponed $itemArr[offer_id] <Hr/>";
		
		//postpone array
		if($itemArr[payment] == 6)
		{	
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
			$place = $place+$itemArr[orig_price];
			$price = 0;
		}
		elseif($itemArr[payment] == 5)
		{
			$checkcompensation = $checkcompensation+($itemArr[orig_price]*((7.5)/100));
			$subtotal=$subtotal+($itemArr[orig_price]*(($partnerArr[yield_vtl])/100));
			
			$chk = $chk+$itemArr[check_value];	
			$chkreal = $chk*0.925;
			
			$chktotal = $chktotal + $itemArr[check_value];
			$chkrealtotal = $chkrealtotal + $itemArr[check_value]*0.925;
			
			
			$days = $days+$day[days];
			//get nights
			$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
			//echo "$partnerArr[hotel_name] | $itemArr[offer_id] <br/>";
		
			if($itemArr[plus_days] > 0)
				$days = $days+1;

		}
		else
		{
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
			$days = $days+$day[days];
			//get nights
			$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
			if($itemArr[plus_days] > 0)
				$days = $days+1;
			}
			$total = $total+$itemArr[orig_price];
			$totaltotal = $totaltotal + $itemArr[orig_price];
			$i++;
			$g++;
		
		if($itemArr[payment] == 6)
		{
			$placerow .='<tr>';
			$placerow .= "<td>$i</td>";
			$placerow .= "<td>$itemArr[offer_id]</td>";
			$placerow .= "<td>$itemArr[name]</td>";
			$placerow .= '</tr>';
			
			
			echo "<tr  style='background-color:#c4c4c4'>
				<td>$partnerArr[hotel_name]</td>
				<td>$itemArr[offer_id]</td>
				<td>$itemArr[paid_date]</td>
				<td>$itemArr[postpone_date]</td>
				<td>$itemArr[name]</td>
				<td>HELYSZINEN</td>
				<td>18%</td>
				<td>$itemArr[invoice_number]</td>
				<td></td>
			</tr>";
		}
		else
		{
			
		if($itemArr[payment] == 5 || $itemArr[invoice_name] <> '')
		{
			//do nothing 
			echo "NEED ACTION $arr[offer_id]<hr/>";
		}
		else
		{	
			$tx = 'ANK';		
		$customers .='<tr>';
			$customers .= "<td>$i</td>";
			$customers .= "<td>$itemArr[offer_id]</td>";
			$customers .= "<td>$itemArr[name]</td>";
			$customers .= "<td>$tx</td>";
		$customers .= '</tr>';
		
		echo "<tr style='background-color:#c4c4c4'>
				<td>$partnerArr[hotel_name]</td>
				<td>$itemArr[offer_id]</td>
				<td>$itemArr[paid_date]</td>
				<td>$itemArr[postpone_date]</td>
				<td>$itemArr[name]</td>
				<td></td>
				<td>ANK</td>
				<td>$itemArr[invoice_number]</td>
				<td></td>
			</tr>";
			
			
		}
		}
		//postone array end*/
	}
	else
	{
	if($itemcount <= $fourtypercent) {
	
	
	if($itemArr[payment] <> 5 || $itemArr[invoice_name] == '')
		{
		if($itemArr[payment] == 6)
		{	
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
			$place = $place+$itemArr[orig_price];
			
			//$itemArr[orig_price] = 0;
			$price = 0;
		}
		elseif($itemArr[payment] == 5)
		{
			$checkcompensation = $checkcompensation+($itemArr[orig_price]*((7.5)/100));
			$subtotal=$subtotal+($itemArr[orig_price]*(($partnerArr[yield_vtl])/100));
			
			$chk = $chk+$itemArr[check_value];	
			$chkreal = $chk*0.925;
			
			$chktotal = $chktotal + $itemArr[check_value];
			$chkrealtotal = $chkrealtotal + $itemArr[check_value]*0.925;
			
			
			$days = $days+$day[days];
			//get nights
			$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
			//echo "$partnerArr[hotel_name] | $itemArr[offer_id] <br/>";
		
			if($itemArr[plus_days] > 0)
				$days = $days+1;

		}
		else
		{
			$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
			$days = $days+$day[days];
			//get nights
			$day = mysql_fetch_assoc($mysql->query("SELECT days FROM offers WHERE id = $itemArr[offers_id]"));
			if($itemArr[plus_days] > 0)
				$days = $days+1;
			}
			$total = $total+$itemArr[orig_price];
			$totaltotal = $totaltotal + $itemArr[orig_price];
			$i++;
			$g++;
		
		if($itemArr[payment] == 6)
		{
			$placerow .='<tr>';
			$placerow .= "<td>$i</td>";
			$placerow .= "<td>$itemArr[offer_id]</td>";
			$placerow .= "<td>$itemArr[name]</td>";
			$placerow .= '</tr>';
			
			if($_GET[debug] == 1)
			{
			echo "<tr style='background-color:orange'>
				<td>$partnerArr[hotel_name]</td>
				<td>$itemArr[offer_id]</td>
				<td>$itemArr[paid_date]</td>
				<td>$itemArr[postpone_date]</td>
				<td>$itemArr[name]</td>
				<td>PLACE</td>
				<td>ANK</td>
				<td>$itemArr[invoice_number]</td>
				<td></td>
			</tr>";
			}
		}
		else
		{
			
		if($itemArr[payment] == 5 || $itemArr[invoice_name] <> '')
		{
			//echo "$itemArr[offer_id]<hr/>";
		}
		else
		{	
			$tx = 'ANK';		
		$customers .='<tr>';
			$customers .= "<td>$i</td>";
			$customers .= "<td>$itemArr[offer_id]</td>";
			$customers .= "<td>$itemArr[name]</td>";
			$customers .= "<td>$tx</td>";
		$customers .= '</tr>';
		
		
		if($_GET[debug] == 1)
		{
		echo "<tr style='background-color:orange'>
				<td>$partnerArr[hotel_name]</td>
				<td>$itemArr[offer_id]</td>
				<td>$itemArr[paid_date]</td>
				<td>$itemArr[postpone_date]</td>
				<td>$itemArr[name]</td>
				<td></td>
				<td>ANK</td>
				<td>$itemArr[invoice_number]</td>
				<td></td>
			</tr>";
		}
		}
		}
	}
	
	if($itemArr[invoice_number] <> '')
		$invoice_number = $itemArr[invoice_number];
	else
		$invoice_number = $invoice_number;

	}
	else
	{
		if($itemArr[payment] == 5 || $itemArr[invoice_name] <> '')
		{
		}
		else
		{
			if($partnerArr[pp_disable] <> 1)
			{
			
			//	echo "$itemArr[offer_id]<hr/>";
		//	echo "$itemArr[paid_date] | UPDATE customers SET postpone = 1, postpone_date = NOW() WHERE cid = $itemArr[cid] AND postpone <> 1<hr/>";
			$mysql->query("UPDATE customers SET postpone = 1, postpone_date = NOW() WHERE cid = $itemArr[cid] AND postpone <> 1 AND payment <> 6");
			}
			else
			{
				echo "DISABLE $partnerArr[company_name]<hr/>";
			}
		}
	}
		$itemcount++;	
	}
	}
	/*******/
	$tr = $total-$place-$subtotal*$tax;
	
	if($tr >= 0)
	{
	
	if($partnerArr[transfer_date] <> '0000-00-00 00:00:00')
		$class = 'green';
	else
		$class = ''; 
		
	$row = "<tr class='$class'><td>".$partnerArr[company_name]."</td>";
	$row.= "<td>$partnerArr[hotel_name]</td>";
	$row.= "<td  align='right'>$i</td>";
	$row.= "<td align='right'>".formatPrice($total)."</td>";
	$row.= "<td align='right'>".formatPrice($place)."</td>";
	$row.= "<td  align='right'>". formatPrice(round($subtotal*$tax,2))."</td>";
	$row.= "<td  align='right'>".formatPrice($tr)."</td>";
	//$rows.= "<td  align='right' class='grey'>".formatPrice($tr-($tr-$place))."</td>";
	$row.= "<td  align='right'><a href='/invoices/vatera/".str_replace("/","_",$partnerArr[invoice_number]).".pdf' target='_blank'>$partnerArr[invoice_number]</a></td>";
	//$rows.= "<td  align='right'><input type='button' value='Kiegyenlítve'/></td>";
	
	$row.= "<td  align='right'>".formatPrice($chk)."</td>";
	$row.= "<td  align='right'>".formatPrice($chkreal)."</td>";
	
	if($partnerArr[invoice_cleared] == '0000-00-00 00:00:00')
		$partnerArr[invoice_cleared] = '';
		
		$row.= "<td  align='right'>$partnerArr[invoice_cleared]</td>";
		
	$row.= "</tr>";
	
	$rows.= $row;
	
	$net = $net+$subtotal;
	$gross = $gross+$subtotal*$tax;
	$totalplace = $totalplace+$place;
	

		if($gross >= 0 && $partnerArr[foreign_swift] == '')
		{
			
			//echo $partnerArr[invoice_cleared]." ".$partnerArr[invoice_number] ."<br/><br/>";
			
			if($partnerArr[invoice_cleared] <> '0000-00-00 00:00:00' && $partnerArr[transfer_date] == '0000-00-00 00:00:00')
			{
			//atutalas
			}
		}	
		$invoice_text = "A számla pénzügyi teljesítést NEM IGÉNYEL";
		$letter_text = formatPrice(abs($tr))."-ot fogunk elutalni az Önök számlájára:";

	}
	else
	{
		$rows.= "<tr class='red'><td>".$partnerArr[company_name]." </td>";
		$rows.= "<td>$partnerArr[hotel_name]  ($partnerArr[pid])</td>";
		$rows.= "<td align='right'>$i</td>";
		$rows.= "<td align='right'>".formatPrice($place)."</td>";
		$rows.= "<td align='right'>".formatPrice($place)."</td>";
		$rows.= "<td align='right'>". formatPrice(round($subtotal*$tax,2))."</td>";
		$rows.= "<td align='right'>".formatPrice(abs($tr+$place))."</td>";
		
		$rows.= "<td align='right'>-</td>";
		
		$rows.= "<td  align='right'><a href='/invoices/vatera/".str_replace("/","_",$partnerArr[invoice_number]).".pdf' target='_blank'>$partnerArr[invoice_number]</a></td>";
		//$rows.= "<td  align='right'><a href='#' target='_blank'>Kiegyenlítve</a></td>";
		
		$rows.= "<td  align='right'>".formatPrice($chk)."</td>";
		$rows.= "<td  align='right'>".formatPrice($chkreal)."</td>";

		$rows.= "</tr>";
		$total = $total-$place;
		$invoice_text = "A számla pénzügyi teljesítést IGÉNYEL";
		$letter_text = "<b><font color='red'>".formatPrice(abs($tr))."-ot kérjük átutalni szíveskedjen az alábbi bankszámlánkra:</font></b><br/><br/>

Hotel Outlet Kft<br/>
Erste Bank<br/>
11600006 - 00000000 - 49170300";
	}
	//because we don't make invoice for those clients who paid on place
	$minustotal = $total - $place;
	if($days == 0)
		$d = 1;
	else
		$d = $days; 
				
	if($place > 0)
		$pltext = "Helyszínen fizetve: <b>".formatPrice($place)."</b><br/>";
	else
		$pltext = '';
	
	$format = 'Y.m.d.'; 
	$datetime = date ("Y-m-d"); 
	$dtm = date ( $format, strtotime ( '-7 day' . $invoice_date ) );

	$singlegross = round($minustotal/$d);
	//$mysql->query("UPDATE customers SET invoice_single_price_gross = $singlegross WHERE invoice_number = '$partnerArr[invoice_number]' ");
	
	if($placerow <> '')
	{
		$placerowtop = "<tr style='font-weight:bold;text-align:center;'>
			<td colspan='3'><b>Helyszínen fizet:</b></td>
		</tr>";
	}
	
	if($dtotal == 0)
	{
		$dtotal = 1;
		$days2 = 0;
	}
	else
	{
		$dtotal = $dtotal;
		$days2 = $dtotal;
	}
		
	$body = "
	<b>Kedves Partnerünk!</b><br/><br/>

<b>Az alábbi elszámolás a HOTEL OUTLET KFT részéről érkezik.</b><br/>
Kérjük, hogy a számlákat is erre a cégnévre állítsák majd ki.<br/><br/>

<b>FONTOS! VÁLTOZÁS a Hotel Outlet Kft elszámolásaival és az utalásokkal kapcsolatban</b><br/><br/>

<b>Elszámolások és számlázás, utalványszámok:</b><br/><br/>

Az áfatörvény alapján <br/><br/>

'Nem minősül szolgáltatásnyújtásnak az ellenérték megfizetése, ha az pénzzel vagy pénzhelyettesítő eszközzel, annak névértékén való elfogadásával történik.'<br/><br/>

E jogszabályhelyből következik, hogy pénzhelyettesítő eszköz -utalvány- ellenérték fejében történő értékesítése, mivel ilyenkor pénz ellenében pénzhelyettesítő eszköz<br/>
 kerül átadásra, nem minősül az Áfa-törvény hatálya alá tartozó ügyletnek, ezért az utalványok értékesítéséről <b>'Áfa körön kívüli' jelzésű bizonylatot kell kibocsátani.</b><br/><br/>

továbbá <br/><br/>

'Az Áfa-törvény hatálya alá tartozó ügylet akkor jelenik meg, amikor az utalványt felhasználták, vagyis azzal, mint pénz-helyettesítő eszközzel kiegyenlítik az igénybe vett<br/>
 szolgáltatás általános forgalmi adót is tartalmazó ellenértékét.'<br/><br/>

Ezért az elszámolásunk két tételt tartalmaz.<br/>
1. 18%-os és 25%-os Áfa-tartalmú elszámolás: ez esetben a szálloda nekünk számláz előlegszámlát és végszámlát is a kért formátumban <b>(eddig szokásos gyakorlat)</b>.<br/>
2. ANK-os Áfa-tartalmú elszámolás: ez esetben a szálloda ANK áfatartalmú ('Áfa körön kívüli') 'utalvány' számlát állít ki részünkre. <b>A végszámlát normális Áfával közvetlen a <br/>
vendég részére állítja ki, melynek fizetendő végösszegéből az utalvány értékét levonja (mint az üdülési csekk esetén)</b>.
<br/><br/>

Admin felületünkön is történt változás. A <b>dollár jellel megjelölt tételeket kell közvetlenül a szállóvendégek részére kiszámlázni, az összes többit pedig a részünkre.</b><br/>

Az indulhatunk.hu kft-ből átszervezzük az összes jutalékos rendszerű együttműködéseinket a Hotel Outlet Kft-ba, melyekre már megkötöttük Önökkel is a szerződéseket.<br/>
Sajnos az átállás miatt közösen kell egy picit jobban odafigyelnünk, hiszen lesznek olyan tételek, melyeket még az indulhatunk.hu kft felé várunk és lesznek már olyanok is, amiket már nem.<br/><br/>

<b>Segítségül a régi sorszámokat 'VTL' megváltoztattuk 'SZO'-ra, így jobban lehet majd a végszámláknál látni, hogy a 'VTL' kezdetűeket az indulhatunk.hu kft felé kell számlázni, míg az 'SZO' kezdetűeket már a Hotel Outlet Kft felé kell majd számlázni.</b><br/><br/>

<b>Utalások:</b><br/>
Ezután is minden héten csütörtökön van az utalás, azzal a változtatással, hogy csak a részünkre hibátlanul elküldött számlák alapján fogunk teljesíteni.<br/>
A számláknak nem kell postán az utalás napjáig megérkezni, de faxon, vagy e-mailen keresztül meg kell kapnunk.<br/>


	<hr/>
Az alábbiakban részletezzük a $week. heti elszámolásunkat.<br/><br/>

Az elmúlt időszakban összesen <b>$i db</b> utalványt értékesítettünk <b>".formatPrice($total)."</b> értékben.<br/><br/>

<b>Elszámolás:</b><br/>

Jutalék számlák összesen: <b>".formatPrice(round($subtotal*$tax,2))."</b><br/>
$pltext
Nálunk összegyűlt pénz: <b>".formatPrice($total-$place)."</b><br/><br/>


A $partnerArr[invoice_number] számú jutalékszámlát az alábbi linkre kattintva töltheti le: <a href='https://admin.indulhatunk.hu/invoices/dl/".md5($partnerArr[invoice_number]."#FWf#CEFA#SDF343sf#")."'><b>$partnerArr[invoice_number] sz. számla letöltése</b></a><br/><br/>

<b>Az egyenlegeink összevezetése után:</b><br/><br/>
$letter_text <br/><br/>


	<table border='1'  cellspacing='0' cellpadding='0' style='font-size:12px; color:black;'>
		<tr style='font-weight:bold;text-align:center;'>
			<td>TESZOR szám</td>
			<td>Teljesítés dátuma</td>
			<td>Megnevezés</td>
			<td>Menny.</td>
			<td>Menny. egys.</td>
			<td>ÁFA</td>
			<td>Nettó egys.</td>
			<td>Bruttó egys.</td>
			<td>Nettó össz.</td>
			<td>Bruttó össz.</td>
		</tr>
		<tr>
			<td>55.10.10</td>
			<td>$dtm</td>
			<td>Szállás</td>
			<td align='center'>$days2</td>
			<td align='center'>éjszaka</td>
			<td align='right'>18 %</td>
			<td align='right'>".formatPrice(($currtotal/$dtotal)/1.18)."</td>
			<td align='right'>".formatPrice($currtotal/$dtotal)."</td>
			<td align='right'>".formatPrice($currtotal/1.18)."</td>
			<td align='right'>".formatPrice($currtotal)."</td>
		</tr>
		<tr>
			<td>55.10.10</td>
			<td>$dtm</td>
			<td>Szállás</td>
			<td align='center'>$days</td>
			<td align='center'>éjszaka</td>
			<td align='right'>ANK</td>
			<td align='right'>".formatPrice((($minustotal-$currtotal)/$d))."</td>
			<td align='right'>".formatPrice(($minustotal-$currtotal)/$d)."</td>
			<td align='right'>".formatPrice(($minustotal-$currtotal))."</td>
			<td align='right'>".formatPrice(($minustotal-$currtotal))."</td>
		</tr>
	</table>
<br/>


Kérjük, hogy mihamarabb készítsék el számlájukat, melyet postán, faxon vagy emailban várunk az <a href='mailto:attila.forro@indulhatunk.hu'>attila.forro@indulhatunk.hu</a> címre.<br/>
A gyorsabb ügyintézés érdekében az a levél tárgyában hivatkozzon az alábbi számlaszámra: $partnerArr[invoice_number].
<br/><br/>

A $week. héten alábbi vevők lesznek az utalással elszámolva:

<br/><br/>
<table border='1'  cellspacing='0' cellpadding='0' style='font-size:12px; color:black;'>
		<tr style='font-weight:bold;text-align:center;'>
			<td>#</td>
			<td>Sorszám</td>
			<td>Vásárló neve</td>
		</tr>
		$customers
		$placerowtop
		$placerow
</table>
<br/><br/>
Üdvözlettel:<br/>
Forró Tamás<br/>
<a href='http://indulhatunk.hu'>Hotel Outlet Kft</a>
";

	if($_GET[showletter] == 1)
		$rows.= "<tr class='grey'><td colspan='10'>$body</td></tr>";

	
	if($_GET[mail] == 1 && $_GET[sure] == 1)
	{
		$message = array();
		$message[pid] = $partnerArr[pid];
		$message[added_by] = $CURUSER[username];
		$message[added] = 'NOW()';
		$message[message] = $body;
		$message[message_email] = $partnerArr[email];
		$mysql->query_insert("messages",$message);
 		sendEmail("$week. heti elszámolás a(z) $partnerArr[hotel_name] részére",$body,"mail@isuperg.com",$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		sendEmail("$week. heti elszámolás a(z) $partnerArr[hotel_name] részére",$body,"billing@indulhatunk.hu",$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		sendEmail("$week. heti elszámolás a(z) $partnerArr[hotel_name] részére",$body,$partnerArr[email],$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		echo "mail sent<br/>";
	}

}

$g= $g;

$footer.= "<tr class='blue'>";
	$footer.= "<td colspan='9'>Üdülési csekk</td>";
	$footer.= "<td  align='right'>".formatPrice($checkArr[whiteSum])."</td>";
	$footer.= "<td  align='right'>".formatPrice($checkyield)."</td>";
$footer.= "</tr>";
	
	
$footer.= "<tr class='header'>";
	$footer.= "<td colspan='2'>Összesen</td>";
	$footer.= "<td  align='right'>$g</td>";
	$footer.= "<td  align='right'>".formatPrice($totaltotal)."</td>";
	$footer.= "<td  align='right'>".formatPrice($totalplace)."</td>";
	$footer.= "<td  align='right'>".formatPrice($gross-$checkyield)."</td>";

	$trans = round($totaltotal-$totalplace-$gross);
	$transfer = formatPrice($trans);

	$footer.= "<td  align='right'>$transfer</td>";
	$footer.= "<td  align='center'>-</td>";
	$footer.= "<td  align='center'>-</td>";
	
	$footer.= "<td  align='center'>".formatPrice($chktotal)."</td>";
	$footer.= "<td  align='center'>".formatPrice($chktotal-$chkrealtotal)."</td>";
	$footer.= "</tr>";
$footer.= "</table>";

	//own yield
			


if($_GET[showtransfer] == 1)
{



}
else
{


//last_week & year
if($_GET[week] == '' || $_GET[year] == '')
{
	$week = date("W")-1;
	$year = date("Y");
	
	if($week == 0)
	{
		$week = 52;
		$year = $year-1;
	}
}
else
{
	$week = $_GET[week];
	$year = $_GET[year];
}

echo "</table><hr/>";

head("$week. heti Hotel Outlet elszámolás");


if($_GET[mail] == 1 && $_GET[sure] <> 1)
	{
	echo "<h1>Biztosan ki szeretné küldeni a heti VTL leveleket? <a href='?showletter=1&mail=1&sure=1'><b>Igen</b></a> | <a href='?showletter=1'><b>Nem</b></a></h1>";
	foot();
	die;
	}

echo "<h1>".$week.". heti elszámolás</h1>";

echo "<div style='float:left'><form method='get'>";
	echo "<select name='year'>";
	
	if($year == 2010)
		$selected1 = 'selected';
		
	if($year == 2011)
		$selected2 = 'selected';
	
	if($year == 2012)
		$selected3 = 'selected';
		
		echo "<option value='2011' $selected2>2011</option>";
		echo "<option value='2012' $selected3>2012</option>";
	echo "</select>";
	
	if($year == '')
		$year = date("Y");
	echo "<select name='week'>";
		for($i=1;$i<53;$i++)
		{
			if($week == $i)
				$weekselected = 'selected';
			else
				$weekselected = '';
				
			if($year == 2011 && $i >= 14)
				echo "<option value='$i' $weekselected>$i</option>";
			elseif($year > 2011)
				echo "<option value='$i' $weekselected>$i</option>";
			else
				echo "";
		}
	echo "</select>";
	echo "<input type='submit' value='mutasd'/>";
echo "</form></div><div style='float:left;padding:5px 0 0 10px;'><a href='weekly-new.php?showtransfer=1&year=$year&week=$week'><b>$week. heti átutalási megbízás letöltése</b></a> | <a href='?showletter=1&year=$year&week=$week'><b>leveleket megtekint</b></a> | <a href='?showletter=1&mail=1'><b>Leveleket elküld</b></a>  | <a href='?transfer_list=1'><b>Átutalási lista</b></a> </div><div class='cleaner'></div><hr/>";

	echo $header;
	echo $rows;
	echo $footer;
foot();
}


?>