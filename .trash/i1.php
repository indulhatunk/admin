<?header('Content-type: text/html; charset=utf-8');?>
<head>
<style>
	body { margin:0 auto; background-color: #f0f0f0; font-family:Tahoma; font-size:12px; color:#363636; }
	
		
	h1 { font-size: 20px; text-align: center; color: #0079b3; margin:0; padding:10px 0 10px 0; }
	
	h2 { }
	

	a img { border:none; }
	
	a {text-decoration:underline; font-weight:bold; color:#0079b3}
	
</style>
<meta charset="UTF-8">
<title>Indulhatunk cégcsoport bemutatkozás</title>
</head>
<body bgcolor='#f0f0f0'>
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
<tbody>
<tr>
<td align="center">
<table border="0" cellspacing="0" cellpadding="0" style="width: 682px;">
<tbody>
<tr>
<td height="133"><img src="http://www.szallasoutlet.hu/images/partner_header.jpg" /></td>
</tr>
<tr>
<td valign="top"><div style="width: 718px; background-color: white; padding: 0px 20px 20px 20px; margin: 0; border-right: 1px solid #dddddd; border-left: 1px solid #dddddd; border-top:0">
                      <h1 style="font-size: 20px; text-align: center; color: #0079b3; padding: 10px 0 10px 0;">Tisztelt Partnerünk!</h1><h2 style="font-size: 14px; text-align: center; color: #004b6f; margin: 0 0 10px 0; padding: 0 0 10px 0; border-bottom: 1px solid #ededed;">Tegye Ön is hatékonyabbá online értékesítését!</h2>
                      <div style="font-size: 12px; text-align: center;">
                        <p style="text-align: left">
                          Cégcsoportunk célja a <strong>SzállásOutlet</strong> együttműködés egyszerűbbé tétele mellett az emberi erőforrás szükséglet garantált csökkentése mind Szállodai partnereink, mind Cégünk számára.<br>
                          <br>
                          Tavalyi évben bevezettük a <strong>QR-kódos voucher-elfogadási rendszert</strong>, mellyel jelentősen egyszerűbbé váltak az online felületen foglaló Ügyfeleink megrendeléseinek, szállodába érkezésének, illetve lemondásainak kezelése mind pénzügyi, mind elszámolási szempontokból.
                        </p>
                        <p style="text-align: left">
                          <strong>A munkamennyiség csökkenésével természetesen jutalékunkat is csökkenteni tudtuk átszerződő Partnereink számára, ami talán az egyik legkézenfekvőbb bizonyítéka a hatékony működésnek.</strong>
                        </p>
                        <p style="text-align: left">
                          Az utóbbi közel egy évben, annak bevezetése óta rendszerünk bizonyított, Partnereink és Ügyfeleink egyaránt elégedettebbnek bizonyulnak, és a gyermekbetegségek kiküszöbölésével mindannyiunk munkája egyszerűbbé és átláthatóbbá vált.
                        </p>
                        <p style="color: #007bab; text-align: left">
                          <strong>Ezúton szeretnénk tájékoztatni Önöket, hogy a 17/1999. (II.5) Kormányrendelet értelmében Ügyfeleink számára 14 nap áll rendelkezésre online megrendelésüktől való indoklás nélküli elállásra.</strong>
                        </p>
                        <p style="text-align: left">
                          <strong>A QR-kód elfogadó rendszerrel nem rendelkező Partnereink részére</strong> a fenti elállási határidő lejártát követően áll módunkban az egyes megrendelések esetén azok ellenértékét kifizetni.<br>
                          <strong>Természetesen QR-kód leolvasóval rendelkező, szerződött Partnereinkre</strong> a fenti 14 napos utalási átfutási idő nem érvényes.<br>
                          <br>
                          Az Önök szálláshelye még nem tagja az alacsonyabb jutalékkal és hatékonyabb voucher-elfogadási rendszerrel dolgozó Partneri körünknek, kérjük tekintse meg egyszerűbb és gazdaságosabb elfogadási rendszerünkre szóló szerződésünket a lap alján.<br>
                          <br>
                          <strong>Kérdése lenne? Hívjon minket bátran! Mindenre válaszolunk!</strong><br>
                          <br>
                        </p>
                      </div>
                      <div style="padding: 20px 0 40px 0;">
                        <div style="text-align: center">
                          <a href="http://www.szallasoutlet.hu" target="_blank"><img src="http://www.szallasoutlet.hu/images/o_logo.jpg"></a>
                        </div>
                        <ul style="font-size: 12px; padding: 10px; margin: 0; list-style: disc;">
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <strong>Hatékonyság, növekedés, garancia.</strong>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;"><strong>Segíti az előértékesítést és a kevésbé kiemelt időszakok sikeres értékesítését.</strong></span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636; font-weight: bold">Szállások akár 50% kedvezménnyel!</span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;">a legütőképesebb kombináció a piacon, kiemelkedő látogatottsággal - a <a href="http://lealkudtuk.hu/" target="_blank">Vatera Sztárajánlatok</a> - SzállásOutlet segítségével megalapozzuk foglaltságát!</span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <strong><span style="color: #363636;">teljesítményarányos díjazás - garantáltan ingyen hirdetünk</span></strong><span style="color: #363636;"></span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;">több mint 300 000 teljesült vendégéjszaka a 2013-as évben</span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;">Ön több mint 2 500 000 embert ér el rajtunk keresztül</span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;">széleskörű megjelenési lehetőségek, online és offline értékesítés saját, folyamatosan fejlődő irodahálózattal</span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;">a kedvezményes alapárnál akár 30%-kal magasabb eladási ár</span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;"><strong>előértékesítési lehetőség</strong></span>
                          </li>
                          <li style="padding: 0 0 10px 0; color: #d03c35;">
                            <span style="color: #363636;">külföldi megjelenési lehetőség</span>
                          </li>
                        </ul>
                      </div>
                       <div style="padding: 2px 0 40px 0;">
                        <div style="text-align: center"><a href="http://admin.indulhatunk.hu/documents/szallasoutlet_szerzodes.pdf" target="_blank"><img src="http://www.szallasoutlet.hu/images/ind1.jpg" /></a>
                        </div></div>
                      <div style="font-size: 12px; text-align: center; font-weight: bold; color: #0079b3;text-align: center;">
                        <p>
                          <strong>A kitöltött, aláírt szerződést juttassa vissza részünkre az alábbi elérhetőségek egyikén</strong>:
                        </p>
                        <p style="font-size:16px">
                          <strong>Parányi Petra</strong><br>
                          <strong>SzállásOutlet üzletágvezető</strong>
                        </p>
                        <p style="font-size:16px">
                          <strong>tel.: +36 70 930 7816<br>
                          fax: +36 1 266 3257<br></strong>&nbsp;<a href="mailto:petra.paranyi@indulhatunk.hu">petra.paranyi@indulhatunk.hu</a>
                        </p>
                      </div>
                      <div style="font-size: 12px; text-align: center; font-weight: bold; color: #0079b3;">
                        <a href="http://admin.indulhatunk.hu/documents/indulhatunk_cegcsoport.pdf" target="_blank">Tekintse meg az Indulhatunk.hu Cégcsoport tájékoztatóját a teljes belföldi termékpalettánkért! &raquo;</a>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td height="102">
                    <img src="http://www.szallasoutlet.hu/images/partner_footer.jpg">
                  </td>
                </tr>
                <tr>
                  <td>
                    <div style="color: #004b6f; text-align: center; padding: 5px; font-size: 11px;">
                      A j&ouml;vő kulcsa - <a href="http://www.indulhatunk.hu" target="_blank">indulhatunk.hu</a> &copy;2014
                    </div>
                  </td>
                </tr>
              
            </table>
          </td>
        </tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>