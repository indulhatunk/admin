<?
/*
 * index.php 
 *
 * the main login page
 *
*/
header('Content-type: text/html; charset=utf-8');

/* bootstrap file */
include("mysql.class.php");

$mysql = new Database("87.229.24.177", "invoice", "zhQDnAmuQ7rpYcwB", "invoice");
$mysql->connect();



function formatPrice($price,$noformat = 0,$nbsp = 0)
{	
	if($noformat == 1)
		$price =  number_format($price, 0, ',', ' '). "";
	elseif($noformat == 2)
		$price =  number_format($price, 0, ',', '.'). " Ft";
	else
		$price =  number_format($price, 0, ',', ' '). " Ft";
		
	if($nbsp == 1)
		$price = str_replace(" ","&nbsp;",$price);
		
	return $price;
}




echo "<h1>Indulhatunk.hu kiállított számlák, teljesítés szerinti havi bontásban</h1>";
echo "<table border='1' cellpadding='3'>";


	echo "<tr>";
		echo "<td>Áfa %</td>";
		echo "<td>Sz.</td>";
		echo "<td>Nettó</td>";
		echo "<td>Bruttó</td>";
		echo "<td>Áfa</td>";
	echo "</tr>";	
	
for($i=1;$i<=12;$i++)
{
	
	
	if($i%2 == 0)
		$bgcolor = 'bgcolor="#c4c4c4"';
	else
		$bgcolor = '';
	$totals = mysql_fetch_assoc($mysql->query("SELECT sum(netto_ossz) as netto_ossz, sum(netto) as netto, sum(brutto) as brutto, sum(brutto_ossz) as brutto_ossz FROM invoice_2010 WHERE MONTH(telj) = $i AND YEAR(telj) = 2011"));


	$zero = mysql_fetch_assoc($mysql->query("SELECT sum(netto_ossz) as netto_ossz, sum(netto) as netto, sum(brutto) as brutto, sum(brutto_ossz) as brutto_ossz FROM invoice_2010 WHERE MONTH(telj) = $i  AND YEAR(telj) = 2011 AND afa_kulcs = 0"));

	echo "<tr $bgcolor>";
		echo "<td rowspan='3'>2011. $i. hónap</td>";
		echo "<td>TAM</td>";
		echo "<td align='right'>".formatPrice($zero[netto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($zero[brutto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($zero[brutto_ossz]-$zero[netto_ossz])."</td>";
	echo "</tr>";	
	
	
	$eighteen = mysql_fetch_assoc($mysql->query("SELECT sum(netto_ossz) as netto_ossz, sum(netto) as netto, sum(brutto) as brutto, sum(brutto_ossz) as brutto_ossz FROM invoice_2010 WHERE MONTH(telj) = $i  AND YEAR(telj) = 2011 AND afa_kulcs = 18"));

	echo "<tr $bgcolor>";
		echo "<td>18%</td>";
		echo "<td align='right'>".formatPrice($eighteen[netto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($eighteen[brutto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($eighteen[brutto_ossz]-$eighteen[netto_ossz])."</td>";
	echo "</tr>";	
	
	$twentyfive = mysql_fetch_assoc($mysql->query("SELECT sum(netto_ossz) as netto_ossz, sum(netto) as netto, sum(brutto) as brutto, sum(brutto_ossz) as brutto_ossz FROM invoice_2010 WHERE MONTH(telj) = $i  AND YEAR(telj) = 2011 AND afa_kulcs = 25"));


	echo "<tr $bgcolor>";
		echo "<td>25%</td>";
		echo "<td align='right'>".formatPrice($twentyfive[netto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($twentyfive[brutto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($twentyfive[brutto_ossz]-$twentyfive[netto_ossz])."</td>";
	echo "</tr>";	
	
	
	echo "<tr $bgcolor>";
		echo "<td>Összesen</td>";
			echo "<td>-</td>";
		echo "<td align='right'>".formatPrice($totals[netto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($totals[brutto_ossz])."</td>";
		echo "<td align='right'>".formatPrice($totals[brutto_ossz]-$totals[netto_ossz])."</td>";
	echo "</tr>";	




	$totaltax = $totaltax + ($totals[brutto_ossz]-$totals[netto_ossz]); 
	$totalgross = $totalgross + $totals[brutto_ossz];
}

	echo "<tr>";
		echo "<td>Évi összesen:</td>";
		echo "<td>-</td>";
		echo "<td align='right'>".formatPrice($totalgross-$totaltax)."</td>";
		echo "<td align='right'>".formatPrice($totalgross)."</td>";
		echo "<td align='right'>".formatPrice($totaltax)."</td>";
	echo "</tr>";	
	
	
echo "</table>";
?>