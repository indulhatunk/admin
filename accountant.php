<?
/*
 * invoice_list.php 
 *
 * invoice_list page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

error_reporting(0);

//check if user is logged in or not
userlogin();

//generate special id, to prevent duplicate ID in the NTX app
$qr = $mysql->query("SELECT * FROM log_invoice WHERE short_id = '' ORDER BY id DESC");
while($arr = mysql_fetch_assoc($qr))
{
	$id = generateInvoiceID();
	echo "$arr[id] $arr[invoice_number] $id\n";
	$mysql->query("UPDATE log_invoice SET short_id = '$id' WHERE id = $arr[id]");
}


$accents_search     = array('à','â','ã','ª','ä','å','Á','À','Â','Ã','Ä','é','è',
'ê','ë','É','È','Ê','Ë','í','ì','î','ï','Í','Ì','Î','Ï','œ','ò','ó','ô','õ','º','ø',
'Ø','Ó','Ò','Ô','Õ','ú','ù','û','Ú','Ù','Û','ç','Ç','Ñ','ñ','ó','Ė','ė'); 

$accents_replace    = array('a','a','a','a','a','a','A','A','A','A','A','e','e',
'e','e','E','E','E','E','i','i','i','i','I','I','I','I','oe','o','o','o','o','o','o',
'O','O','O','O','O','u','u','u','U','U','U','c','C','N','n','o','E','e'); 



if($CURUSER[userclass] < 50)
	header("location:index.php");
	
	
	if(class_exists('TSzlaTetelek') != true)
	{
	
	class TSzlaTetelek {
		public $tetel;
	}
	
	}
	


if(strtoupper($_GET[download]) <> 'ON')
{
head("Számla listák");


?>

<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?all=1" class='current'>Számlák</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>

<?
if($_GET[showinfo] == 1)
{

	?>
	<form method='get'>
	<input type='hidden' name='showinfo' value='1'>
	<input type='text' name='to_date' class='maskeddate'/> 

	<select name='company'>
			<? echo getCompany($_GET[company], 'select', 0); ?>

	<!--	<option value='szallasoutlet'>SzállásOutlet Kft.</option>
		<option value='hoteloutlet'>Hotel Outlet Kft.</option>
		<option value='horizonttravel'>Horizont Travel Kft.</option>
		<option value='mkmedia'>MK Média Kft.</option>
		<option value='indulhatunk'>Indulhatunk.hu Kft.</option>
		<option value='indusz'>Indulhatunk Utazásszervező Kft.</option> -->
	</select>
	<select name='type'>
		<option value='invoice'>be nem érkezett számlák</option>
		<option value='voucher'>fel nem használt utalványok</option>
	</select>
	
	<input type='submit' value='Letöltés'/>
	
</form>
<hr/>
	<?
	
	echo "<table>";
	
	if($_GET[type] == 'voucher')
	{
		if($_GET[to_date] <> '')
		{
			$todate2 = "AND user_invoice_date <= '$_GET[to_date]'";
		}
		///if($_GET[from_date] <> '')
		//	$fromdate = "AND invoice_cleared >= '$_GET[from_date] 23:59:59'";

		
		$query = $mysql->query("SELECT * FROM customers WHERE invoice_created = 1 AND pid <> 3121 AND pid <> 3452 AND pid <> 3001 AND (user_final_invoice_number = '' OR year(user_final_invoice_date) = 2014) $todate2 AND company_invoice = '$_GET[company]' AND payment <> 6 and payment < 10 and invoice_name = ''  AND year(added) = 2013 ORDER BY paid_date ASC");
		
		$i = 1;
		while($arr = mysql_fetch_assoc($query))
		{
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));
			if($arr[user_final_invoice_date] == '0000-00-00 00:00:00')
				$arr[user_final_invoice_date] = '-';
				
			if($partner[has_foreign] == 0)
			{
			echo "<tr>";
				echo "<td>$i</td>";
				echo "<td>$arr[offers_id]</td>";
				echo "<td>$arr[name]</td>";
				echo "<td align='center'>$arr[paid_date]</td>";
				echo "<td align='center'>$arr[user_final_invoice_date]</td>";
				echo "<td><a href='/invoices/vatera/".str_replace("/",'_',$arr[invoice_number]).".pdf' target='_blank'>$arr[invoice_number]</a></td>";
				echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
			echo "</tr>";
			
			$total = $total + $arr[orig_price];
			$i++;
			}
		}
			echo "<tr class='header'>";
				echo "<td colspan='6'>Összesen</td>";
				echo "<td align='right'>".formatPrice($total)."</td>";
			echo "</tr>";
			
	}
	
	
	if($_GET[type] == 'invoice')
	{
		if($_GET[to_date] <> '')
		{
			$todate2 = "AND invoice_date <= '$_GET[to_date]'";
		}
		///if($_GET[from_date] <> '')
		//	$fromdate = "AND invoice_cleared >= '$_GET[from_date] 23:59:59'";

		
		$query = $mysql->query("SELECT pid, invoice_date, invoice_number, sum(orig_price) as total, invoice_cleared FROM customers WHERE invoice_created = 1 AND pid <> 3121 AND pid <> 3452 AND pid <> 3001 AND (invoice_cleared = '0000-00-00 00:00:00' or year(invoice_cleared) = 2014 ) $todate2 AND company_invoice = '$_GET[company]'  GROUP BY invoice_number ORDER BY pid, invoice_date ASC");
		
		while($arr = mysql_fetch_assoc($query))
		{
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));
			if($arr[invoice_cleared] == '0000-00-00 00:00:00')
				$arr[invoice_cleared] = '-';
				
			if($partner[has_foreign] == 0)
			{
			echo "<tr>";
				echo "<td>$partner[hotel_name]</td>";
				echo "<td align='center'>$arr[invoice_date]</td>";
				echo "<td align='center'>$arr[invoice_cleared]</td>";
				echo "<td><a href='/invoices/vatera/".str_replace("/",'_',$arr[invoice_number]).".pdf' target='_blank'>$arr[invoice_number]</a></td>";
				echo "<td align='right'>".formatPrice($arr[total])."</td>";
			echo "</tr>";
			
			$total = $total + $arr[total];
			}
		}
			echo "<tr class='header'>";
				echo "<td colspan='4'>Összesen</td>";
				echo "<td align='right'>".formatPrice($total)."</td>";
			echo "</tr>";
			
	}
	

	echo "</table>";

	echo "</div></div>";
	foot();
	die;
}



?>
<form method='get'>
	<input type='text' name='from_date' class='maskeddate' value='<?=$_GET[from_date]?>'/> 
	<input type='text' name='to_date' class='maskeddate' value='<?=$_GET[to_date]?>'/> 

	<select name='company'>
		<? echo getCompany($_GET[company], 'select', 0); ?>
	</select>
	<select name='type'>
		<option value='all'>számla lista</option>
		<option value='check'>üdülésicsekk lista</option>
		<option value='credit'>bankkártya lista</option>
	</select>
	
	<select name='field'>
		<option value='added'>kelt alapján</option>
		<option value='telj'>teljesítés alapján</option>
	</select>
	
	<input type='checkbox' name='download' />Letöltés
	<input type='submit' value='Letöltés'/>
	
</form>
<div class='cleaner'></div>
<hr/>
<div style='width:880px;overflow:auto'>
<pre>
<?
//foot();

}
else
{
//	header('Content-type: text/plain; charset=utf-8');

	header('Content-Type: text/IMP; charset=iso-8859-2');
	//mb_internal_encoding("UTF-8");
	header("Content-Disposition: attachment;filename=MX12SZLA_$_GET[company]_".date("Y_m_d").".IMP"); //".date("Y-m-d_h-i-s").
	header('Pragma: no-cache');
	header('Expires: 0');
		
}
//echo "<pre>";

if($_GET[type] == 'all')
{


$from_date = $_GET[from_date];
$to_date = $_GET[to_date];


if($_GET[field] == 'telj')
	$field = 'telj';
else
	$field = 'added';
	
if($from_date <> '' && $_GET[type] == 'all')
{
	$extraselect = "AND $field >= '$from_date' AND $field <= '$to_date'";
}

elseif($from_date <> '' && $_GET[type] == 'check')
{
	$extraselect = "AND user_invoice_date >= '$from_date' AND user_invoice_date <= '$to_date'";
}
$sql = "SELECT * FROM log_invoice WHERE company = '$_GET[company]' AND year($field) >= 2012 $extraselect ORDER BY invoice_number ASC";
//print $sql; die();
$query = $mysql->query($sql);

//echo "SELECT * FROM log_invoice WHERE company = '$_GET[company]' AND year($field) >= 2012 $extraselect ORDER BY invoice_number ASC";

//echo "SELECT * FROM log_invoice WHERE company = '$_GET[company]' AND year(added) = 2012 $extraselect ORDER BY invoice_number ASC";

function formatdate2($date)
{
	$date = str_replace("-",'',$date);
	
	return $date;
}

if($_GET[company] == 'hoteloutlet')
	$company = "HOTEL OUTLET KFT.";
elseif($_GET[company] == 'szallasoutlet')
	$company = "SZÁLLÁSOUTLET KFT.";
elseif($_GET[company] == 'horizonttravel')
	$company = "HORIZONT TRAVEL KFT.";
elseif($_GET[company] == 'professional')
	$company = "PROFESSIONAL OUTLET KFT.";
elseif($_GET[company] == 'indusz')
	$company = "INDULHATUNK UTAZÁSSZERVEZŐ KFT";
elseif($_GET[company] == 'mkmedia')
	$company = "MK MÉDIA KFT";
elseif($_GET[company] == 'indulhatunk') //else
	$company = "INDULHATUNK MÉDIA KFT.";


$invoice_list_header = array(
	'A0',
	$company,
	'Z1X6',
	date("Y-m-d H:i:s")
);

$header = implode("\t",$invoice_list_header);

$content.="$header\r\n";
while($arr = mysql_fetch_assoc($query))
{	
	$request = unserialize($arr[request]);
	
	$data = array();
	
	if($_GET[items] == 'on')
	{
		
		
	
			
	
	}
	else
	{
	
	$data[record] = 11;
	
	//echo $request[data][szla_xml][fej][fiz_mod]."<hr/>";
	
	$fmod = $request[data][szla_xml][fej][fiz_mod];
	
	
	if($request[data][szla_xml][fej][fiz_mod] == 2 ) //|| $request[data][szla_xml][fej][fiz_mod] == 3
	{
		$data[megjeloles] = 'K'; //atutalas
		$fiz_hat = formatdate2($request[data][szla_xml][fej][fiz_hat]);
		$kelt = formatdate2($request[data][szla_xml][fej][kelt]);
	}
	else
	{
		$data[megjeloles] = 'P'; //p penztar
		$fiz_hat = formatdate2($request[data][szla_xml][fej][telj]);
		$kelt = formatdate2($request[data][szla_xml][fej][telj]);
	}
	
	$specialid = explode("-",trim($request[data][szla_xml][fej][vevo][adoszam]));
	
	
	$specialid = $specialid[0];
	
	
	
	$tomb[$request[data][szla_xml][fej][tomb]][] = str_replace($request[data][szla_xml][fej][tomb]."/","",$arr[invoice_number]);
		
	if($request[data][szla_xml][fej][telj] == '2013-12-31' && $request[data][szla_xml][fej][kelt] == '2014-01-14')
		$kelt = '20131231';
		
	if($request[data][szla_xml][fej][telj] == '2013-12-31' && $request[data][szla_xml][fej][kelt] == '2014-03-17')
		$kelt = '20131231';
		
	if($request[data][szla_xml][fej][telj] == '2013-12-31' && $request[data][szla_xml][fej][kelt] == '2014-03-16')
		$kelt = '20131231';
		
	if($request[data][szla_xml][fej][telj] == '2013-12-31' && $request[data][szla_xml][fej][kelt] == '2014-01-18')
		$kelt = '20131231';
		
	if($request[data][szla_xml][fej][telj] == '2013-12-31' && $request[data][szla_xml][fej][kelt] == '2014-01-19')
		$kelt = '20131231';
	
	if($request[data][szla_xml][fej][telj] == '2013-12-31' && $request[data][szla_xml][fej][kelt] == '2014-01-20')
		$kelt = '20131231';
		
	if($request[data][szla_xml][fej][telj] == '2013-12-31' && $request[data][szla_xml][fej][kelt] == '2014-01-21')
		$kelt = '20131231';
		
	if($request[data][szla_xml][fej][telj] == '2014-06-30' && $request[data][szla_xml][fej][kelt] == '2014-07-15')
		$kelt = '20140630';

		
			
	if(strlen($specialid) == 8)
	{
		$specialid = substr($specialid,0,6);
		$addr = str_replace("\t",' ',$request[data][szla_xml][fej][vevo][cim][utca]);
		$house = '';
	}
	else
	{
		$specialid = $arr[short_id];	
		
		$addr = explode(" ",str_replace("\t",' ',$request[data][szla_xml][fej][vevo][cim][utca]));
		$addr = $addr[0]." $specialid";
	}
	$data[bizonylat] =$arr[invoice_number];
	$data[kelt] = $kelt;
	$data[telj] = formatdate2($request[data][szla_xml][fej][telj]);
	$data[fiz_hat] = $fiz_hat;
	$data[partner_id] = $specialid;
	$data[partner_name] =  str_replace("\t",' ',$request[data][szla_xml][fej][vevo][nev]);
	$data[partner_tax] = trim($request[data][szla_xml][fej][vevo][adoszam]);//$request[data][szla_xml][fej][vevo][adoszam] adoszamszam
	$data[partner_eu] = '';
	$data[partner_other_id] = '';
	$data[szla_irsz] = str_replace("\t",' ',$request[data][szla_xml][fej][vevo][cim][irsz]);
	$data[szla_orszag] = 'Magyarország';
	$data[szla_varos] = str_replace("\t",' ',$request[data][szla_xml][fej][vevo][cim][varos]);
	$data[szla_kozterulet] = $addr;
	$data[szla_hazszam] = '';
	$data[szla_epulet] = '';
	$data[szla_lepcsohaz] = '';
	$data[szla_emelet] = '';
	$data[szla_ajto] = '';
	$data[szla_megjegyzes] = '';
	
	
	$invoice_type = $data[megjeloles];
		 
	$row = implode("\t",$data);
	//$content.="$row\r\n";
	
	$content.= str_replace($accents_search, $accents_replace, $row)."\r\n";


	$data = '';
	
		foreach($request[data][szla_xml][tetelek]->tetel as $item)
		{
				
	
		
			
				
			if($request[data][szla_xml][fej][tipus] == 'S')
			{
				$isstorno = 1;
				$item[mennyiseg] = -1*$item[mennyiseg];
			}
			else
				$isstorno = 0;

			if($item[afa_kulcs] == 'AHK' || $item[afa_kulcs] == 'AHX')
				$item[afa_kulcs] = 0;
			else
				$item[afa_kulcs] = $item[afa_kulcs];

			if($request[data][szla_xml][fej][arfolyam] <> '' && $request[data][szla_xml][fej][arfolyam] <> 1)
			{
				$item[netto_egysegar] = round($item[netto_egysegar]*$request[data][szla_xml][fej][arfolyam]);
				$item[brutto_egysegar] = round($item[brutto_egysegar]*$request[data][szla_xml][fej][arfolyam]);
			}	
				
			$data[record] = 21;
			
			if($item[brutto_egysegar] == '')
				$data[brutto_egysegar] = $item[netto_egysegar]*(1+($item[afa_kulcs]/100))*$item[mennyiseg];
			else
				$data[brutto_egysegar] = $item[brutto_egysegar]*$item[mennyiseg];
			
			$bt = $data[brutto_egysegar];
			
			$data[brutto_egysegar] = $data[brutto_egysegar]- round($data[brutto_egysegar]-($data[brutto_egysegar]/(1+($item[afa_kulcs]/100))));
					
			$data[afakulcs] = $item[afa_kulcs];
			$data[afaosszeg] = round($bt-($bt/(1+($item[afa_kulcs]/100))));
		
			$data[megjegyz] = '';
			
			//echo $request[data][szla_xml][fej][arfolyam] ."<hr/>";
			if($arr[company] == 'mkmedia')
				$data[fokonyvi] = 91;
			elseif($request[data][szla_xml][fej][arfolyam] <> '' && $request[data][szla_xml][fej][arfolyam] <> 1)
				$data[fokonyvi] = 915;
			elseif($data[afakulcs] == 18)
				$data[fokonyvi] = 913;
			elseif($data[afakulcs] == 0)
				$data[fokonyvi] = 912; //utalvany
			else
				$data[fokonyvi] = 911; //afas
				
			
				
			$data[munkaszam] = '';
			$data[munkaszam_nev] = '';
			$data[koltseghely] = '';
			$data[koltseghely_ev] = '';
			
			$row = implode("\t",$data);
			$content.="$row\r\n";
			
			$afa[$data[afakulcs]] = $afa[$data[afakulcs]]+$data[brutto_egysegar];
			
			$pmethods[$fmod] = $pmethods[$fmod] + $data[brutto_egysegar];
		}
		
		

	}
	
	
}
	$from = array('à',"è","ő","ű","õ");
	$to = array("á","é","ő","ű","ö");
	
	$content = str_replace($from,$to,$content);
	echo iconv("UTF-8","ISO-8859-2",$content);
	
	if($data[kelt] == '' && $data[bizonylat] <> '')
		die("hiba!");
	//echo $content;
}
elseif($_GET[type] == 'check')
{
	$query = $mysql->query("SELECT * FROM customers WHERE payment = 5 AND paid = 1 AND company_invoice = '$_GET[company]' AND year(user_invoice_date) = 2012 $extraselect ORDER BY user_invoice_date ASC");

?>

<input type='button' value='Nyomtatás' id='print' />
<hr/>
<?
	echo "<div id='printable'>";
	?>
	<script>

$(document).ready(function() {
		
	$('#print').click(function(){
		w=window.open();
		w.document.write($('#printable').html());
		w.print();
		w.close();
	});

});
</script>
<style>
.header {
	background-color:#c4c4c4;
	text-align:center;
	font-weight:bold;
}
.red {
	background-color:#E56E94;
}

table
{
    border-width: 0 0 1px 1px;
    border-spacing: 0;
    border-radius:3px;
    border-collapse: collapse;
}
table, td
{
    border-color: #c4c4c4;
    border-width:1px;
    border-style: solid;
	font-size:12px;
	font-family:arial;
}
</style>
	<?
	echo "<table>";
	
	echo "<tr class='header'>";
		echo "<td>Dátum</td>";
		echo "<td>Szla. szám.</td>";
		echo "<td>Vásárló neve</td>";
		echo "<td>Sorszám</td>";
		echo "<td>Összeg</td>";
		echo "<td>ÜCS. lista</td>";
		echo "<td>P. biz.</td>";
		echo "<td>P. összeg</td>";
	echo "</tr>";
	
	while($arr = mysql_fetch_assoc($query))
	{
	
		$payment = mysql_fetch_assoc($mysql->query("SELECT * FROM checkout_vtl WHERE voucher_id = '$arr[offer_id]'"));
		
		if($arr[checkpaper_id] == 0)
			$class = 'background-color:#E56E94';
		else
			$class = '';
		echo "<tr style='$class'>";
		
			echo "<td width='70'>$arr[user_invoice_date]</td>";
			echo "<td width='120'><a href='/invoices/vatera/".str_replace("/",'_',$arr[user_invoice_number]).".pdf' target='_blank'>$arr[user_invoice_number]</a></td>";
			
			if($arr[invoice_name] <> '')
				echo "<td>$arr[invoice_name]</td>";
			else
				echo "<td>$arr[name] </td>";
				echo "<td width='85'>$arr[offer_id] </td>";

			echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
			echo "<td align='right'>$arr[checkpaper_id]</td>";
			
			if($payment[warrant_id] <> 0)
			{
				echo "<td width='80' align='right'>$payment[warrant_year]/".str_pad($payment[warrant_id],5, "0", STR_PAD_LEFT)."</td>";
				echo "<td width='80' align='right'>".formatPrice($payment[value])."</td>";
			}
			else
				echo "<td></td><td></td>";
				
			
					
		echo "</tr>";
	}
	echo "</table></div>"; 
}

elseif($_GET[type] == 'credit')
{
	$query = $mysql->query("SELECT * FROM customers WHERE payment = 9 AND paid = 1 AND company_invoice = '$_GET[company]' AND year(user_invoice_date) = 2012 $extraselect ORDER BY user_invoice_date ASC");

?>

<input type='button' value='Nyomtatás' id='print' />
<hr/>
<?
	echo "<div id='printable'>";
	?>
	<script>

$(document).ready(function() {
		
	$('#print').click(function(){
		w=window.open();
		w.document.write($('#printable').html());
		w.print();
		w.close();
	});

});
</script>
<style>
.header {
	background-color:#c4c4c4;
	text-align:center;
	font-weight:bold;
}
.red {
	background-color:#E56E94;
}

table
{
    border-width: 0 0 1px 1px;
    border-spacing: 0;
    border-radius:3px;
    border-collapse: collapse;
}
table, td
{
    border-color: #c4c4c4;
    border-width:1px;
    border-style: solid;
	font-size:12px;
	font-family:arial;
}
</style>
	<?
	echo "<table>";
	
	echo "<tr class='header'>";
		echo "<td>Dátum</td>";
		echo "<td>Szla. szám.</td>";
		echo "<td>Vásárló neve</td>";
		echo "<td>Sorszám</td>";
		echo "<td>Összeg</td>";
		echo "<td>Kód</td>";
	echo "</tr>";
	
	while($arr = mysql_fetch_assoc($query))
	{
	
		
	
		echo "<tr style='$class'>";
		
			echo "<td width='70'>$arr[user_invoice_date]</td>";
			echo "<td width='120'><a href='/invoices/vatera/".str_replace("/",'_',$arr[user_invoice_number]).".pdf' target='_blank'>$arr[user_invoice_number]</a></td>";
			
			if($arr[invoice_name] <> '')
				echo "<td>$arr[invoice_name]</td>";
			else
				echo "<td>$arr[name] </td>";
				echo "<td width='85'>$arr[offer_id] </td>";

			echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
			echo "<td align='right'>$arr[creditcard]</td>";
					
			
					
		echo "</tr>";
	}
	echo "</table></div>"; 
}


if(strtoupper($_GET[download]) <> 'ON')
{
	echo "</div></div>";
	
	echo "<table style='width:200px;margin:0 auto;'>
	
		<tr class='header'>
				<td>Áfa</td>
				<td>Netto</td>
			</tr>
	";
	
	
	foreach($afa as $key => $value)
	{
		
		echo "<tr>
				<td>$key</td>
				<td align='right'>".formatprice($value)."</td>
			</tr>";
	}
	echo "</table><br/><br/>";



	echo "<table style='width:200px;margin:0 auto;'>
	
		<tr class='header'>
				<td>Fizetési mód</td>
				<td>Összeg</td>
			</tr>
	";
	
	//1 - Készpénz, 2 - Átutalás, 3 - Bankkártya, 4 - Hitel, 5 - Utánvét, 6 - Paypal,
	
	$methods = array(
	
	1 => "Készpénz",
	2 => "Átutalás",
	3 => "Bankkártya",
	5 => "Utánvét",
	
	);
	foreach($pmethods as $key => $value)
	{
		
		echo "<tr>
				<td>".$methods[$key]."</td>
				<td align='right'>".formatprice($value)."</td>
			</tr>";
	}
	echo "</table><br/><br/>";



//	debug($tomb);
	echo "<table style='width:200px;margin:0 auto;'>";
	foreach($tomb as $tb => $numbers)
	{
	
		$last = end($numbers);
		$first = $numbers[0];
		
		if($tb <> '')
		{
		echo "<tr>
			<td>$tb</td>
			<td align='right'>$first</td>
			<td align='right'>$last</td>
		</tr>";
		}
		
	}
	echo "</table>";
	
	foot();
}
?>