<?
/*
 * invoice_list.php 
 *
 * invoice_list page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER[userclass] < 50)
	header("location:index.php");
	
	
	if(class_exists('TSzlaTetelek') != true)
	{
	
	class TSzlaTetelek {
		public $tetel;
	}
	
	}
	
if(strtoupper($_GET[download]) <> 'ON')
{
head("Számla listák");
?>

<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?all=1" class='current'>Számlák</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>

<form method='get'>
	<input type='text' name='from_date' class='maskeddate'/> 
	<input type='text' name='to_date' class='maskeddate'/> 

	<select name='company'>
		<option value='hoteloutlet'>Hotel Outlet Kft.</option>
		<option value='indulhatunk'>Indulhatunk.hu Kft.</option>
		
	</select>
	<select name='type'>
		<option value='all'>számla lista</option>
		<option value='check'>üdülésicsekk lista</option>
		<option value='credit'>bankkártya lista</option>
	</select>
	<input type='checkbox' name='download' />Letöltés
	<input type='submit' value='Letöltés'/>
	
</form>
<div class='cleaner'></div>
<hr/>
<div style='width:880px;overflow:auto'>

<?
//foot();

}
else
{
//	header('Content-type: text/html; charset=utf-8');

	header("Content-type: text/plain; charset=utf-8");
//	header("Content-Disposition: attachment;filename=MX12SZLA-$company-".date("Y-m-d_h-i-s").".imp");
//	header('Pragma: no-cache');
//	header('Expires: 0');
		
}
//echo "<pre>";

if($_GET[type] == 'all')
{


$from_date = $_GET[from_date];
$to_date = $_GET[to_date];

if($from_date <> '' && $_GET[type] == 'all')
{
	$extraselect = "AND added >= '$from_date' AND added <= '$to_date'";
}

elseif($from_date <> '' && $_GET[type] == 'check')
{
	$extraselect = "AND user_invoice_date >= '$from_date' AND user_invoice_date <= '$to_date'";
}

$query = $mysql->query("SELECT * FROM log_invoice WHERE company = '$_GET[company]' AND year(added) = 2012 $extraselect ORDER BY invoice_number ASC");


function formatdate2($date)
{
	$date = str_replace("-",'',$date);
	
	return $date;
}

if($_GET[company] <> 'indulhatunk')
	$company = "HOTEL OUTLET KFT.";
else
	$company = "INDULHATUNK.HU KFT.";


$invoice_list_header = array(
	'A0',
	$company,
	'Z1X6',
	date("Y-m-d H:i:s")
);

$header = implode("\t",$invoice_list_header);

echo "$header\r\n";
while($arr = mysql_fetch_assoc($query))
{	
	$request = unserialize($arr[request]);
	
	$data = array();
	
	if($_GET[items] == 'on')
	{
		
		
	
			
	
	}
	else
	{
	
	$data[record] = 11;
	
	//echo $request[data][szla_xml][fej][fiz_mod]."<hr/>";
	
	if($request[data][szla_xml][fej][fiz_mod] == 2 || $request[data][szla_xml][fej][fiz_mod] == 3)
	{
		$data[megjeloles] = 'K'; //atutalas
		$fiz_hat = formatdate2($request[data][szla_xml][fej][fiz_hat]);
		$kelt = formatdate2($request[data][szla_xml][fej][kelt]);
	}
	else
	{
		$data[megjeloles] = 'P'; //p penztar
		$fiz_hat = formatdate2($request[data][szla_xml][fej][telj]);
		$kelt = formatdate2($request[data][szla_xml][fej][telj]);
	}
	
	
	$data[bizonylat] =$arr[invoice_number];
	$data[kelt] = $kelt;
	$data[telj] = formatdate2($request[data][szla_xml][fej][telj]);
	$data[fiz_hat] = $fiz_hat;
	$data[partner_id] = '';
	$data[partner_name] =  str_replace("\t",' ',$request[data][szla_xml][fej][vevo][nev]);
	$data[partner_tax] = '';//$request[data][szla_xml][fej][vevo][adoszam]
	$data[partner_eu] = '';
	$data[partner_other_id] = '';
	$data[szla_irsz] = str_replace("\t",' ',$request[data][szla_xml][fej][vevo][cim][irsz]);
	$data[szla_orszag] = 'Magyarország';
	$data[szla_varos] = str_replace("\t",' ',$request[data][szla_xml][fej][vevo][cim][varos]);
	$data[szla_kozterulet] = str_replace("\t",' ',$request[data][szla_xml][fej][vevo][cim][utca]);
	$data[szla_hazszam] = '';
	$data[szla_epulet] = '';
	$data[szla_lepcsohaz] = '';
	$data[szla_emelet] = '';
	$data[szla_ajto] = '';
	$data[szla_megjegyzes] = '';
	
	$row = implode("\t",$data);
	echo "$row\r\n";
	
	$data = '';
	
		foreach($request[data][szla_xml][tetelek]->tetel as $item)
		{
				
	
		
			
				
			if($request[data][szla_xml][fej][tipus] == 'S')
			{
				$isstorno = 1;
				$item[mennyiseg] = -1*$item[mennyiseg];
			}
			else
				$isstorno = 0;

			if($item[afa_kulcs] == 'AHK')
				$item[afa_kulcs] = 0;
			else
				$item[afa_kulcs] = $item[afa_kulcs];

				
				
			$data[record] = 21;
			
			if($item[brutto_egysegar] == '')
				$data[brutto_egysegar] = $item[netto_egysegar]*(1+($item[afa_kulcs]/100))*$item[mennyiseg];
			else
				$data[brutto_egysegar] = $item[brutto_egysegar]*$item[mennyiseg];
			
			$bt = $data[brutto_egysegar];
			
			$data[brutto_egysegar] = $data[brutto_egysegar]- round($data[brutto_egysegar]-($data[brutto_egysegar]/(1+($item[afa_kulcs]/100))));
					
			$data[afakulcs] = $item[afa_kulcs];
			$data[afaosszeg] = round($bt-($bt/(1+($item[afa_kulcs]/100))));
		
			$data[megjegyz] = '';
			
			if($data[afakulcs] == 0)
				$data[fokonyvi] = 912; //utalvany
			else
				$data[fokonyvi] = 911; //afas
				
			$data[munkaszam] = '';
			$data[munkaszam_nev] = '';
			$data[koltseghely] = '';
			$data[koltseghely_ev] = '';
			
			$row = implode("\t",$data);
			
			
			
			
			$totaltax = $totaltax + $data[afaosszeg];
			
			if($item[afa_kulcs] == 0)
			{
				$total0 = $total0 + $data[brutto_egysegar];
			}		
			elseif($item[afa_kulcs] == 18)
			{
				$total18 = $total18 + $data[brutto_egysegar];
			}
			elseif($item[afa_kulcs] == 27)
			{
				$total27 = $total27 + $data[brutto_egysegar];
				
					echo "<font color='red'><b>$row</b></font>\r\n";	
			}
			else
			{
				$totale = $total27 + $data[brutto_egysegar];
			}
			$total = $total + $data[brutto_egysegar];
		

		}
		
		

	}

	

}

echo "\n\n 0% ".formatPrice($total0)." | \n";
echo "\n\n 18% ".formatPrice($total18)." | \n";
echo "\n\n 27% ".formatPrice($total27)." | \n";

echo "\n\n ossz ".formatPrice($total)." | $totaltax\n";

}
elseif($_GET[type] == 'check')
{
	$query = $mysql->query("SELECT * FROM customers WHERE payment = 5 AND paid = 1 AND company_invoice = '$_GET[company]' AND year(user_invoice_date) = 2012 $extraselect ORDER BY user_invoice_date ASC");

?>

<input type='button' value='Nyomtatás' id='print' />
<hr/>
<?
	echo "<div id='printable'>";
	?>
	<script>

$(document).ready(function() {
		
	$('#print').click(function(){
		w=window.open();
		w.document.write($('#printable').html());
		w.print();
		w.close();
	});

});
</script>
<style>
.header {
	background-color:#c4c4c4;
	text-align:center;
	font-weight:bold;
}
.red {
	background-color:#E56E94;
}

table
{
    border-width: 0 0 1px 1px;
    border-spacing: 0;
    border-radius:3px;
    border-collapse: collapse;
}
table, td
{
    border-color: #c4c4c4;
    border-width:1px;
    border-style: solid;
	font-size:12px;
	font-family:arial;
}
</style>
	<?
	echo "<table>";
	
	echo "<tr class='header'>";
		echo "<td>Dátum</td>";
		echo "<td>Szla. szám.</td>";
		echo "<td>Vásárló neve</td>";
		echo "<td>Sorszám</td>";
		echo "<td>Összeg</td>";
		echo "<td>ÜCS. lista</td>";
		echo "<td>P. biz.</td>";
		echo "<td>P. összeg</td>";
	echo "</tr>";
	
	while($arr = mysql_fetch_assoc($query))
	{
	
		$payment = mysql_fetch_assoc($mysql->query("SELECT * FROM checkout_vtl WHERE voucher_id = '$arr[offer_id]'"));
		
		if($arr[checkpaper_id] == 0)
			$class = 'background-color:#E56E94';
		else
			$class = '';
		echo "<tr style='$class'>";
		
			echo "<td width='70'>$arr[user_invoice_date]</td>";
			echo "<td width='120'><a href='/invoices/vatera/".str_replace("/",'_',$arr[user_invoice_number]).".pdf' target='_blank'>$arr[user_invoice_number]</a></td>";
			
			if($arr[invoice_name] <> '')
				echo "<td>$arr[invoice_name]</td>";
			else
				echo "<td>$arr[name] </td>";
				echo "<td width='85'>$arr[offer_id] </td>";

			echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
			echo "<td align='right'>$arr[checkpaper_id]</td>";
			
			if($payment[warrant_id] <> 0)
			{
				echo "<td width='80' align='right'>$payment[warrant_year]/".str_pad($payment[warrant_id],5, "0", STR_PAD_LEFT)."</td>";
				echo "<td width='80' align='right'>".formatPrice($payment[value])."</td>";
			}
			else
				echo "<td></td><td></td>";
				
			
					
		echo "</tr>";
	}
	echo "</table></div>"; 
}

elseif($_GET[type] == 'credit')
{
	$query = $mysql->query("SELECT * FROM customers WHERE payment = 9 AND paid = 1 AND company_invoice = '$_GET[company]' AND year(user_invoice_date) = 2012 $extraselect ORDER BY user_invoice_date ASC");

?>

<input type='button' value='Nyomtatás' id='print' />
<hr/>
<?
	echo "<div id='printable'>";
	?>
	<script>

$(document).ready(function() {
		
	$('#print').click(function(){
		w=window.open();
		w.document.write($('#printable').html());
		w.print();
		w.close();
	});

});
</script>
<style>
.header {
	background-color:#c4c4c4;
	text-align:center;
	font-weight:bold;
}
.red {
	background-color:#E56E94;
}

table
{
    border-width: 0 0 1px 1px;
    border-spacing: 0;
    border-radius:3px;
    border-collapse: collapse;
}
table, td
{
    border-color: #c4c4c4;
    border-width:1px;
    border-style: solid;
	font-size:12px;
	font-family:arial;
}
</style>
	<?
	echo "<table>";
	
	echo "<tr class='header'>";
		echo "<td>Dátum</td>";
		echo "<td>Szla. szám.</td>";
		echo "<td>Vásárló neve</td>";
		echo "<td>Sorszám</td>";
		echo "<td>Összeg</td>";
		echo "<td>Kód</td>";
	echo "</tr>";
	
	while($arr = mysql_fetch_assoc($query))
	{
	
		
	
		echo "<tr style='$class'>";
		
			echo "<td width='70'>$arr[user_invoice_date]</td>";
			echo "<td width='120'><a href='/invoices/vatera/".str_replace("/",'_',$arr[user_invoice_number]).".pdf' target='_blank'>$arr[user_invoice_number]</a></td>";
			
			if($arr[invoice_name] <> '')
				echo "<td>$arr[invoice_name]</td>";
			else
				echo "<td>$arr[name] </td>";
				echo "<td width='85'>$arr[offer_id] </td>";

			echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
			echo "<td align='right'>$arr[creditcard]</td>";
					
			
					
		echo "</tr>";
	}
	echo "</table></div>"; 
}


if(strtoupper($_GET[download]) <> 'ON')
{
	echo "</div></div>";
	foot();
}
?>