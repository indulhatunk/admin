<?
/*
 * invoice_list.php 
 *
 * invoice_list page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
 
$qr= $mysql->query("SELECT * FROM log_invoice WHERE company = 'hoteloutlet' and year(added) = 2012 and (month(added) = 3 OR month(added) = 4) ORDER BY added ASC");

echo "<style>
    table { border-collapse: collapse; }
    td, th { border: 1px solid #000000; vertical-align: baseline; padding:5px; }
    
    .storno { background-color:#e7e7e7; }
</style>";

echo "<table>";
class TSzlaTetelek { } 

while($arr = mysql_fetch_assoc($qr)){
	$invoicetotal = 0;
	$request = unserialize($arr[request]);

$kelt = $request[data][szla_xml][fej][kelt];
	$telj = $request[data][szla_xml][fej][telj];
		
	$nev = $request[data][szla_xml][fej][vevo][nev];
	
	$fiz_mod = $request[data][szla_xml][fej][fiz_mod];
	
	
	$storno = $request[params][biz_tip];
	
	
	if($storno == 'ESS')
	{
		$storno = "SZTORNO";
		$class = 'storno';
	}
	else
	{
		$storno = '';
		$class = '';
	}
	//ESS
	
		
		$items = $request[data][szla_xml][tetelek]->tetel;
		
		//debug($items);

		foreach($items as $item)
		{
			if($item[netto_egysegar] <> '')
				$itemprice =  $item[netto_egysegar]*(1+($item[afa_kulcs]/100));
			else
				$itemprice =  $item[brutto_egysegar];
				
			$itemprice = $itemprice*$item[mennyiseg];
			
			$invoicetotal = $invoicetotal + $itemprice;
			
			//echo ">>> $item[megnev] | $itemprice $item[afa_kulcs]<hr/>";

				
		}
		
		 
		if($fiz_mod == 1)
			$method = "Készpénz";
		elseif($fiz_mod == 2)
			$method = "Átutalás";
		elseif($fiz_mod == 3)
			$method = "Bankkártya";
		elseif($fiz_mod == 5)
			$method = "Utánvét";
		else
			$method = '';	
			
		if(substr($arr[invoice_number],0,3) == 'LMB')
			$folder = 'lmb';
		else
			$folder = 'vatera';
			
		$url = "/invoices/$folder/HO-".str_replace("/",'_',$arr[invoice_number]).".pdf";
		echo "<tr class='$class'>
					<td><a href='$url' target='_blank'>$arr[invoice_number]</a></td>
					<td>$kelt</td>
					<td>$telj</td> 
					<td>$nev</td>
					<td align='center'>$method</td>
					<td align='right'>".formatPrice($invoicetotal)."</td>
					<td align='center'>$storno</td></tr>";

}

echo "</table>";

die;
if($CURUSER[userclass] < 50)
	header("location:index.php");
	
	
	if(class_exists('TSzlaTetelek') != true)
	{
	
	class TSzlaTetelek {
		public $tetel;
	}
	
	}
	

head("Számla listák - teljesítés alapján");
?>

<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?all=1" class='current'>Számlák</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>

<form method='get'>
	<input type='text' name='from_date' class='maskeddate' value='<?=$_GET[from_date]?>'/> 
	<input type='text' name='to_date' class='maskeddate' value='<?=$_GET[to_date]?>'/> 

	<select name='company'>
		<option value='hoteloutlet' <? if($_GET[company] == 'hoteloutlet') echo "selected";?>>Hotel Outlet Kft.</option>
		<option value='indulhatunk' <? if($_GET[company] == 'indulhatunk') echo "selected";?>>Indulhatunk.hu Kft.</option>
	</select>
	<select name='type'>
		<option value='all'>számla lista</option>
	</select>
	<input type='submit' value='Letöltés'/>
	
</form>
<div class='cleaner'></div>
<hr/>
<div style='width:880px;overflow:auto'>

<?



if($_GET[type] == 'all')
{


$from_date = $_GET[from_date];
$to_date = $_GET[to_date];

if($from_date <> '' && $_GET[type] == 'all')
{
	$extraselect = "AND telj >= '$from_date' AND telj < '$to_date'";
}

$query = $mysql->query("SELECT * FROM log_invoice WHERE company = '$_GET[company]' AND year(telj) = 2012 $extraselect ORDER BY telj ASC");




//echo "$header\r\n";

while($arr = mysql_fetch_assoc($query))
{	
	$request = unserialize($arr[request]);
	
	$data = array();
	

		
	//echo $request[data][szla_xml][fej][fiz_mod]."<hr/>";
	
	$idata[bizonylat] =$arr[invoice_number];
	$idata[partner_name] =  str_replace("\t",' ',$request[data][szla_xml][fej][vevo][nev]);
	$idata[partner_tax] = '';//$request[data][szla_xml][fej][vevo][adoszam]

	$r = implode("\t",$data);
//	echo "$row\r\n";
	
	$data = '';
	
		foreach($request[data][szla_xml][tetelek]->tetel as $item)
		{
				
					
			
						
			if($item[afa_kulcs] == 'AHK')
				$item[afa_kulcs] = 0;
			else
				$item[afa_kulcs] = $item[afa_kulcs];


			
			//debug($request);
			
			if($request[data][szla_xml][fej][tipus] == 'S')
			{
				$isstorno = 1;
				$item[mennyiseg] = -1*$item[mennyiseg];
			}
			else
				$isstorno = 0;
				
			$data[record] = 21;
			
			if($item[brutto_egysegar] == '')
				$data[brutto_egysegar] = $item[netto_egysegar]*(1+($item[afa_kulcs]/100));
			else
				$data[brutto_egysegar] = $item[brutto_egysegar];
			
			$bt = $data[brutto_egysegar];
			
			$data[brutto_egysegar] = ($data[brutto_egysegar]- round($data[brutto_egysegar]-($data[brutto_egysegar]/(1+($item[afa_kulcs]/100)))))*$item[mennyiseg];
					
			$data[afakulcs] = $item[afa_kulcs];
			$data[afaosszeg] = round($bt-($bt/(1+($item[afa_kulcs]/100))));
		
			$data[megjegyz] = '';
			
			if($data[afakulcs] == 0)
				$data[fokonyvi] = 912; //utalvany
			else
				$data[fokonyvi] = 911; //afas
				
		
			$data[munkaszam] = '';
			$data[munkaszam_nev] = '';
			$data[koltseghely] = '';
			$data[koltseghely_ev] = '';
			
			$row = implode("\t",$data);
		//	echo "$row\r\n";
			
			if($item[afa_kulcs] == 0)
			{	
				$zero = $zero+$data[brutto_egysegar];
				
				if($isstorno == 1 || $data[brutto_egysegar] < 0)
				{
					$zerostorno = $zerostorno + $data[brutto_egysegar];
					$class = 'red';
				}
				else
				{
						$class = '';
				}
					
					if($_GET[showtax] == 'ahk')
					{
						
					//	if($idata[bizonylat] == 'DSZO-M2012/000234')
					//		debug($request);
						$ahkrows.= "<tr class='$class'>
						<td>
							<a href='/invoices/vatera/".str_replace("/","_",$idata[bizonylat]).".pdf' target='_blank'>$idata[bizonylat]</a>	
						</td>
						<td>
							$arr[telj]
						</td>
						<td>
							$idata[partner_name]
						</td>
						<td>
							".$request[data][szla_xml][fej][tipus]."
						</td>
						<td>
							$data[afakulcs]
						</td>
						<td align='right'>
							".formatPrice($data[brutto_egysegar])."
						</td>
						<td align='right'>
							".$item[mennyiseg]."
						</td>
						</tr>";
						
						
					}
				//echo "$r\n$row\n";
			}
			elseif($item[afa_kulcs] == 18)
			{
				$eighteen = $eighteen+$data[brutto_egysegar];
				
					
					if($isstorno == 1 || $data[brutto_egysegar] < 0)
				{
					$eighteenstorno = $eighteenstorno + $data[brutto_egysegar];
					$class = 'red';
				}
				else
				{
						$class = '';
				}
					
					if($_GET[showtax] == '18')
					{
						
					//	if($idata[bizonylat] == 'DSZO-M2012/000234')
					//		debug($request);
						$erows.= "<tr class='$class'>
						<td>
							<a href='/invoices/vatera/".str_replace("/","_",$idata[bizonylat]).".pdf' target='_blank'>$idata[bizonylat]</a>	
						</td>
						<td>
							$arr[telj]
						</td>
						<td>
							$idata[partner_name]
						</td>
						<td>
							".$request[data][szla_xml][fej][tipus]."
						</td>
						<td>
							$data[afakulcs]
						</td>
						<td align='right'>
							".formatPrice($data[brutto_egysegar])."
						</td>
						<td align='right'>
							".$item[mennyiseg]."
						</td>
						</tr>";
						
						
					}


				
			}
			elseif($item[afa_kulcs] == 27)
			{
				$twenty = $twenty+$data[brutto_egysegar];
			//	echo "$r\n$row\n";
				
					if($isstorno == 1 || $data[brutto_egysegar] < 0)
				{
					$twentystorno = $twentystorno + $data[brutto_egysegar];
					$class = 'red';
				}
				else
				{
						$class = '';
				}
					
					if($_GET[showtax] == '27')
					{
						
					//	if($idata[bizonylat] == 'DSZO-M2012/000234')
					//		debug($request);
						$twsrows.= "<tr class='$class'>
						<td>
							<a href='/invoices/vatera/".str_replace("/","_",$idata[bizonylat]).".pdf' target='_blank'>$idata[bizonylat]</a>	
						</td>
						<td>
							$arr[telj]
						</td>
						<td>
							$idata[partner_name]
						</td>
						<td>
							".$request[data][szla_xml][fej][tipus]."
						</td>
						<td>
							$data[afakulcs]
						</td>
						<td align='right'>
							".formatPrice($data[brutto_egysegar])."
						</td>
						<td align='right'>
							".$item[mennyiseg]."
						</td>
						</tr>";
						
						
					}


			}
			elseif($item[afa_kulcs] == 25)
			{
				//echo "$row \n $twentyfive+$data[brutto_egysegar]<hr/>";
					$twentyfive = $twentyfive+$data[brutto_egysegar];
				
					if($isstorno == 1 || $data[brutto_egysegar] < 0)
				{
					$twentyfivestorno = $twentyfivestorno + $data[brutto_egysegar];
					$class = 'red';
				}
				else
				{
						$class = '';
				}
					
					if($_GET[showtax] == '25')
					{
						
					//	if($idata[bizonylat] == 'DSZO-M2012/000234')
					//		debug($request);
						$twrows.= "<tr class='$class'>
						<td>
							<a href='/invoices/vatera/".str_replace("/","_",$idata[bizonylat]).".pdf' target='_blank'>$idata[bizonylat]</a>	
						</td>
						<td>
							$arr[telj]
						</td>
						<td>
							$idata[partner_name]
						</td>
						<td>
							".$request[data][szla_xml][fej][tipus]."
						</td>
						<td>
							$data[afakulcs]
						</td>
						<td align='right'>
							".formatPrice($data[brutto_egysegar])."
						</td>
						<td align='right'>
							".$item[mennyiseg]."
						</td>
						</tr>";
						
						
					}



			}
			else
			{
			 echo "<font color='red'>$data[partner_name]</font><hr/><hr/><hr/>";	
				
			}
			
			$tt = $tt + $item[brutto_egysegar];
		}
	
	
	
	

}

?>
<table style='width:100%'>
	<tr class='header'>
		<td>ÁFA</td>
		<td>Nettó összeg</td>
		<td>Bruttó összeg</td>
		<td>Ebből bruttó storno</td>
		<td></td>
	</tr> 
	<tr>
		<td class='lablerow'>0% nettó</td>
		<td align='right'><?=formatPrice($zero)?></td>
		<td align='right'><?=formatPrice($zero)?></td>
		<td align='right'><?=formatPrice($zerostorno)?></td>
		<td align='center'><a href='?from_date=<?=$_GET[from_date]?>&to_date=<?=$_GET[to_date]?>&company=<?=$_GET[company]?>&type=<?=$_GET[type]?>&showtax=ahk'>[lista]</a></td>
	</tr>

<tr><td colspan='4'><table><?=$ahkrows?></table></td></tr>
	<tr>
		<td class='lablerow'>18% nettó</td>
		<td align='right'><?=formatPrice($eighteen)?></td>
		<td align='right'><?=formatPrice($eighteen*1.18)?></td>
		<td align='right'><?=formatPrice($eighteenstorno*1.18)?></td>
		<td align='center'><a href='?from_date=<?=$_GET[from_date]?>&to_date=<?=$_GET[to_date]?>&company=<?=$_GET[company]?>&type=<?=$_GET[type]?>&showtax=18'>[lista]</a></td>

	</tr>
	<tr><td colspan='4'><table><?=$erows?></table></td></tr>

	<tr>
		<td class='lablerow'>25% nettó</td>
		<td align='right'><?=formatPrice($twentyfive)?></td>
			<td align='right'><?=formatPrice($twentyfive*1.25)?></td>
		<td align='right'><?=formatPrice($twentyfivestorno*1.25)?></td>
		<td align='center'><a href='?from_date=<?=$_GET[from_date]?>&to_date=<?=$_GET[to_date]?>&company=<?=$_GET[company]?>&type=<?=$_GET[type]?>&showtax=25'>[lista]</a></td>

	</tr>
	<tr><td colspan='4'><table><?=$twrows?></table></td></tr>

	<tr>
		<td class='lablerow'>27% nettó</td>
		<td align='right'><?=formatPrice($twenty)?></td>
		<td align='right'><?=formatPrice($twenty*1.27)?></td>

		<td align='right'><?=formatPrice($twentystorno*1.27)?></td>
		<td align='center'><a href='?from_date=<?=$_GET[from_date]?>&to_date=<?=$_GET[to_date]?>&company=<?=$_GET[company]?>&type=<?=$_GET[type]?>&showtax=27'>[lista]</a></td>
	</tr>
	<tr><td colspan='4'><table><?=$twsrows?></table></td></tr>
	<!--<tr>
		<td class='lablerow'>Nettó összesen</td>
		<td align='right'><?=formatPrice($tt)?></td>
		<td align='right'><?=formatPrice($ttstorno)?></td>
		<td></td>
	</tr>-->
</table>
<?

}
?>