<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

	userlogin();
	
	if($CURUSER[userclass] < 50)
		header("location: index.php");
	
head("Szállás ajánlat kimutatás");

$pid = (int)$_GET[pid];
$category = (int)$_GET[category];
$country_id = (int)$_GET[country_id];
$name = $_GET[name];

$active = $_GET[aleph_ok];


if($_POST[id] > 0)
{
	$mysql_tour->query_update("partner",$_POST,"id=$_POST[id]");
	$msg = "Sikeresen szerkesztette az ajánlatot!";
}
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='?type=2' class="<? if($_GET[type] == 2 || $_GET[type] == '') echo "current"?>">Szálláshely kimutatás</a></li>
						<li><a href='?type=1' class="<? if($_GET[type] == 1) echo "current"?>">Szálláshelyek</a></li>
					</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<div style='text-align:center'>
<form method='get'>
<select onchange='submit()' name='id'>
	<option value=''>Válassz</option>
	<?
		$pages = $mysql_tour->query("SELECT * FROM domain ORDER BY name ASC");
		
		while($row = mysql_fetch_assoc($pages))
		{
			if($row[id] == $_GET[id])
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='$row[id]' $selected>$row[name]</option>";
		}
	?>
	
</select>
</form>
</div>


<hr/>

<table>

	<tr class='header'>
		<td colspan='2'>-</td>
		<td>Partner</td>
		<td>Kapcsolat</td>
		<td>Email</td>
		<td>Fő ajánlat</td>
		<td>Város</td>
		<td>Rack rate</td>
		<td>Aktív csomag</td>
		<td>-</td>
	</tr>
<?



		if($_GET[id] > 0)
			$offers = $mysql_tour->query("SELECT partner.* FROM partner INNER JOIN partner_domain ON partner_domain.partner_id = partner.id WHERE partnertype_id = 2 AND partner_domain.domain_id = '$_GET[id]' ORDER BY partner.name ASC");
		else
			$offers = $mysql_tour->query("SELECT * FROM partner WHERE partnertype_id = 2  ORDER BY name ASC");
			
	
		
		$i = 1;
		while($arr = mysql_fetch_assoc($offers))
		{
			$package = 0;
			$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $arr[id]"));
			
			$partner2 = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE coredb_id = $arr[id]"));
			
			$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = $arr[city_id]"));
			
			$package = mysql_num_rows($mysql_tour->query("SELECT accomodation.id,accomodation.name FROM accomodation LEFT JOIN prices ON prices.offer_id = accomodation.id WHERE accomodation.partner_id = '$arr[id]' AND prices.from_date <= NOW() AND prices.to_date >= NOW() AND accomodation.package = 1 GROUP BY accomodation.id"));
			
			$origoffer = mysql_num_rows($mysql_tour->query("SELECT accomodation.id,accomodation.name FROM accomodation LEFT JOIN prices ON prices.offer_id = accomodation.id WHERE accomodation.partner_id = '$arr[id]' AND prices.from_date <= NOW() AND prices.to_date >= NOW() AND accomodation.package = 0 GROUP BY accomodation.id"));

			
			
			if($package == 0 && $origoffer == 0)
				$class = 'red';
			elseif($origoffer > 0 && $package == 0)
				$class = 'blue';
			elseif($package > 0 && $origoffer == 0)
				$class = 'orange';
			else
				$class = '';
				
			echo "<tr class='$class'>
				<td align='right' width='10' align='center'>$i.</td>
					<td width='10'>
						<a href='/accomodation.php?id=$arr[id]&add=1'><img src='/images/edit.png' width='20'/></a>
						</td>
					<td><a href='#' target='_blank'><b>$partner[name]</b></a></td>
					<td>$partner2[contact]</td>
					<td>$partner2[email]</td>
					<td><a href='$arr[page_url]' target='_blank'><b>$arr[name]</b></a></td>
					<td>$city[name]</td>
					
					<td align='right'>$origoffer</td>
					<td align='right'>$package</td>
									<td width='50'>
						<a href='editoffers.php?pid=$arr[id]'>[csomagok]</a>
						<a href='rooms.php?pid=$arr[id]'>[szobatípusok]</a>
						<a href='partner-domain.php?pid=$arr[id]'>[domainek]</a>
						<a href='editoffers.php?pid=$arr[id]'><strike>[galéria]</strike></a>
						<a href='reviews.php?pid=$arr[id]'><strike>[értékelések]</strike></a>
						
						<a href='info/sendrequest.php?pid=$arr[id]' rel='facebox' target='_blank'>[értesítés]</a>
						</td>
				</tr>";
	
			$i++;
		}

?>

</table>
</div></div>
<?
foot();
?>