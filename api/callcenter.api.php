<?
	userlogin();
	
	

	if($CURUSER[userclass] < 50)	
		die;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Admin kereső</title>
	  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<style>
body {
  padding-top: 80px;
}
.starter-template {
  padding: 40px 15px;
  text-align: center;
}

	
</style>
<script src="/inc/js/jquery.js" type="text/javascript"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1346485-1', 'auto');
  ga('send', 'pageview');
</script>
<script>
	$(document).ready(function() {
		ga('send', 'event', 'Callcenter hívás', '<?=$CURUSER[username]?>', '<?=$_GET[phone]?>');
	});
	</script>

</head>
<body>
	
	   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Teigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Indulhatunk.hu</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="http://admin.indulhatunk.hu" target='_blank'>Admin</a></li>
            <li><a href="http://admin.indulhatunk.hu/customers.php" target='_blank'>SzállásOutlet vásárlók</a></li>
            <li><a href="http://admin.indulhatunk.hu/passengers.php" target='_blank'>Indulhatunk utasok</a></li>
            <li><a href="http://admin.indulhatunk.hu/passengers.php?add=1" target='_blank'>Új utas felvitele</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
	    
	    
<?
	
	if($_GET[phone] == '')
		$phone = '434343434343';
	else
	{
		$phone = substr($_GET[phone],2);
		$phone = "%".implode('%',str_split($phone))."%"; 	
	}
	$customers = array();
	//indulhatunk
	//sajat eladas
	//outlet
	//licittravel
	//mostutazz
	
	$query = $mysql->query("SELECT cid, name, phone,added, offer_id, pid, sub_pid FROM customers WHERE phone like '$phone' ORDER BY cid DESC");

	while($arr = mysql_fetch_assoc($query))
	{
		
		$customer = array();
		
		$time = strtotime($arr[added]);
		
		if($arr[sub_pid] > 0)
			$arr[pid] = $arr[sub_pid];
			
		$partner = mysql_fetch_assoc($mysql->query("SELECT hotel_name FROM partners WHERE pid = $arr[pid]"));
		$customer[source] = 'Outlet';
		$customer[name] = $arr[name];
		$customer[added] = $arr[added];
		$customer[offer] = $partner[hotel_name];
		$customer[offer_id] = $arr[offer_id];
		$customer[url] = "http://admin.indulhatunk.hu/customers.php?add=1&editid=".$arr[cid];

		$customers[$time] = $customer; 
	}
	
	$query = $mysql->query("SELECT id, name, phone,added, offer_id, offer_name FROM customers_tour WHERE phone like '$phone' ORDER BY id DESC");

	while($arr = mysql_fetch_assoc($query))
	{
		
		$customer = array();
		
		$time = strtotime($arr[added]);
		
		if($arr[sub_pid] > 0)
			$arr[pid] = $arr[sub_pid];
			
		$customer[source] = 'Indulhatunk';
		$customer[name] = $arr[name];
		$customer[added] = $arr[added];
		$customer[offer] = $arr[offer_name];
		$customer[offer_id] = $arr[offer_id];
		$customer[url] = "http://admin.indulhatunk.hu/passengers.php?add=1&editid=".$arr[id];

		$customers[$time] = $customer; 
	}
	
	
	$query = $mysql->query("SELECT id, name, phone,date, bid_name, contract_id FROM own_vouchers WHERE phone like '$phone' ORDER BY id DESC");

	while($arr = mysql_fetch_assoc($query))
	{
		
		$customer = array();
		
		$time = strtotime($arr[date]);
		
		if($arr[sub_pid] > 0)
			$arr[pid] = $arr[sub_pid];
			
		$customer[source] = 'Saját eladás';
		$customer[name] = $arr[name];
		$customer[added] = $arr[date];
		$customer[offer] = $arr[bid_name];
		$customer[offer_id] = "-";
		$customer[url] = "http://admin.indulhatunk.hu/own_vouchers.php?id=$arr[id]&add=1&cid=$arr[contract_id]";

		$customers[$time] = $customer; 
	}
	
	$query = $mysql->query("SELECT id, name, phone, added FROM licittravel_customers WHERE phone like '$phone' ORDER BY id DESC");

	while($arr = mysql_fetch_assoc($query))
	{
		
		$customer = array();
		
		$time = strtotime($arr[added]);
		
		if($arr[sub_pid] > 0)
			$arr[pid] = $arr[sub_pid];
			
		$customer[source] = 'Licittravel';
		$customer[name] = $arr[name];
		$customer[added] = $arr[added];
		$customer[offer] = $arr[bid_name];
		$customer[offer_id] = "-";
		$customer[url] = "#";

		$customers[$time] = $customer; 
	}
	
		$query = $mysql->query("SELECT id, name, phone, date FROM lmb WHERE phone like '$phone' ORDER BY id DESC");

	while($arr = mysql_fetch_assoc($query))
	{
		
		$customer = array();
		
		$time = strtotime($arr[date]);
		
		if($arr[sub_pid] > 0)
			$arr[pid] = $arr[sub_pid];
			
		$customer[source] = 'MostUtazz';
		$customer[name] = $arr[name];
		$customer[added] = $arr[date];
		$customer[offer] = $arr[bid_name];
		$customer[offer_id] = "-";
		$customer[url] = "#";

		$customers[$time] = $customer; 
	}
	
	krsort($customers);
	
	if(empty($customers))
		echo "<div class='alert alert-danger'>Nincs találat</div>";
	echo "<table class='table'>";
	
	$i = 1;
	foreach($customers as $cust)
	{
				$added = explode(" ",$cust[added]);

		echo "<tr>";
			echo "<td width='10' align='center'>".$i.".</td>";
			echo "<td>".$added[0]."</td>";
			echo "<td>$cust[source]</td>";
			echo "<td>$cust[name]</td>";
			echo "<td>$cust[offer]</td>";
			echo "<td>$cust[offer_id]</td>";
			echo "<td><a href='$cust[url]' target='_blank'>[megtekint]</a></td>";
		echo "</tr>";
		
		$i++;
		
	}
	echo "</table>";
?>


</div>
</body>
</html>
