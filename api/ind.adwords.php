<?
header('Content-Type: text/csv; charset=utf-8');


$query = $mysql_tour->query("SELECT * FROM country WHERE adwords = 1 ORDER BY name ASC");


$parameters = array(
"{orszag}",
"{orszag_what}", //magyarorszagi
"{orszag_where}", //magyarorszagban
"{orszag_whereto}", //magyarorszagba
"{regio}",
"{regio_what}",
"{regio_where}",
"{regio_whereto}",
"{varos}",
"{varos_what}",
"{varos_where}",
"{varos_whereto}",
//"{ellatas}",
//"{utazas_modja}",
);

$parameters2 = array(
	"{utazas_modja}",
	"{utazas_modja_os}",
	"{utazas_modja_vel}",
);

$parameters3 = array(
	"{ellatas}",
	"{ellatas_vel}",
);


$countrykeywords = "nyaralás {orszag}
{orszag} nyaralás
{orszag_what} nyaralás
nyaralás {orszag_where}
kedvezményes nyaralás {orszag}
{orszag} kedvezményes nyaralás
{orszag_what} kedvezményes nyaralás
kedvezményes nyaralás {orszag_where}
olcsó nyaralás {orszag}
{orszag} olcsó nyaralás
{orszag_what} olcsó nyaralás
olcsó nyaralás {orszag_where}
lastminute nyaralás {orszag}
{orszag} lastminute nyaralás
{orszag_what} lastminute nyaralás
lastminute nyaralás {orszag_where}
akciós nyaralás {orszag}
{orszag} akciós nyaralás
{orszag_what} akciós nyaralás
akciós nyaralás {orszag_where}
last minute nyaralás {orszag}
{orszag} last minute nyaralás
{orszag_what} last minute nyaralás
last minute nyaralás {orszag_where}
{utazas_modja_os} nyaralás {orszag}
{orszag} {utazas_modja_os} nyaralás
{orszag_what} {utazas_modja_os} nyaralás
{utazas_modja_os} nyaralás {orszag_where}
{ellatas_os} nyaralás {orszag}
{orszag} {ellatas_os} nyaralás
{orszag_what} {ellatas_os} nyaralás
{ellatas_os} nyaralás {orszag_where}
tengerparti nyaralás {orszag}
{orszag} tengerparti nyaralás
{orszag_what} tengerparti nyaralás
tengerparti nyaralás {orszag_where}
utazás {orszag}
{orszag} utazás
{orszag_what} utazás
utazás {orszag_where}
kedvezményes utazás {orszag}
{orszag} kedvezményes utazás
{orszag_what} kedvezményes utazás
kedvezményes utazás {orszag_where}
olcsó utazás {orszag}
{orszag} olcsó utazás
{orszag_what} olcsó utazás
olcsó utazás {orszag_where}
lastminute utazás {orszag}
{orszag} lastminute utazás
{orszag_what} lastminute utazás
lastminute utazás {orszag_where}
akciós utazás {orszag}
{orszag} akciós utazás
{orszag_what} akciós utazás
akciós utazás {orszag_where}
last minute utazás {orszag}
{orszag} last minute utazás
{orszag_what} last minute utazás
last minute utazás {orszag_where}
{utazas_modja_os} utazás {orszag}
{orszag} {utazas_modja_os} utazás
{orszag_what} {utazas_modja_os} utazás
{utazas_modja_os} utazás {orszag_where}
{ellatas_os} utazás {orszag}
{orszag} {ellatas_os} utazás
{orszag_what} {ellatas_os} utazás
{ellatas_os} utazás {orszag_where}
tengerparti utazás {orszag}
{orszag} tengerparti utazás
{orszag_what} tengerparti utazás
tengerparti utazás {orszag_where}
üdülés {orszag}
{orszag} üdülés
{orszag_what} üdülés
üdülés {orszag_where}
kedvezményes üdülés {orszag}
{orszag} kedvezményes üdülés
{orszag_what} kedvezményes üdülés
kedvezményes üdülés {orszag_where}
olcsó üdülés {orszag}
{orszag} olcsó üdülés
{orszag_what} olcsó üdülés
olcsó üdülés {orszag_where}
lastminute üdülés {orszag}
{orszag} lastminute üdülés
{orszag_what} lastminute üdülés
lastminute üdülés {orszag_where}
akciós üdülés {orszag}
{orszag} akciós üdülés
{orszag_what} akciós üdülés
akciós üdülés {orszag_where}
last minute üdülés {orszag}
{orszag} last minute üdülés
{orszag_what} last minute üdülés
last minute üdülés {orszag_where}
{utazas_modja_os} üdülés {orszag}
{orszag} {utazas_modja_os} üdülés
{orszag_what} {utazas_modja_os} üdülés
{utazas_modja_os} üdülés {orszag_where}
{ellatas_os} üdülés {orszag}
{orszag} {ellatas_os} üdülés
{orszag_what} {ellatas_os} üdülés
{ellatas_os} üdülés {orszag_where}
tengerparti üdülés {orszag}
{orszag} tengerparti üdülés
{orszag_what} tengerparti üdülés
tengerparti üdülés {orszag_where}";
$countrykeywords = explode("\n",$countrykeywords);


$citykeywords = "
nyaralás {varos}
nyaralás {varos} {orszag}
nyaralás {orszag} {varos}
{varos} nyaralás
{varos} {orszag} nyaralás
{orszag} {varos} nyaralás
{orszag_what} nyaralás {varos}
{varos_what} nyaralás
nyaralás {varos_where}
kedvezményes nyaralás {varos}
kedvezményes nyaralás {varos} {orszag}
kedvezményes nyaralás {orszag} {varos}
{varos} kedvezményes nyaralás
{varos} {orszag} kedvezményes nyaralás
{orszag} {varos} kedvezményes nyaralás
{orszag_what} kedvezményes nyaralás {varos}
{varos_what} kedvezményes nyaralás
kedvezményes nyaralás {varos_where}
olcsó nyaralás {varos}
olcsó nyaralás {varos} {orszag}
olcsó nyaralás {orszag} {varos}
{varos} olcsó nyaralás
{varos} {orszag} olcsó nyaralás
{orszag} {varos} olcsó nyaralás
{orszag_what} olcsó nyaralás {varos}
{varos_what} olcsó nyaralás
olcsó nyaralás {varos_where}
lastminute nyaralás {varos}
lastminute nyaralás {varos} {orszag}
lastminute nyaralás {orszag} {varos}
{varos} lastminute nyaralás
{varos} {orszag} lastminute nyaralás
{orszag} {varos} lastminute nyaralás
{orszag_what} lastminute nyaralás {varos}
{varos_what} lastminute nyaralás
lastminute nyaralás {varos_where}
akciós nyaralás {varos}
akciós nyaralás {varos} {orszag}
akciós nyaralás {orszag} {varos}
{varos} akciós nyaralás
{varos} {orszag} akciós nyaralás
{orszag} {varos} akciós nyaralás
{orszag_what} akciós nyaralás {varos}
{varos_what} akciós nyaralás
akciós nyaralás {varos_where}
last minute nyaralás {varos}
last minute nyaralás {varos} {orszag}
last minute nyaralás {orszag} {varos}
{varos} last minute nyaralás
{varos} {orszag} last minute nyaralás
{orszag} {varos} last minute nyaralás
{orszag_what} last minute nyaralás {varos}
{varos_what} last minute nyaralás
last minute nyaralás {varos_where}
{utazas_modja_os} nyaralás {varos}
{utazas_modja_os} nyaralás {varos} {orszag}
{utazas_modja_os} nyaralás {orszag} {varos}
{varos} {utazas_modja_os} nyaralás
{varos} {orszag} {utazas_modja_os} nyaralás
{orszag} {varos} {utazas_modja_os} nyaralás
{orszag_what} {utazas_modja_os} nyaralás {varos}
{varos_what} {utazas_modja_os} nyaralás
{utazas_modja_os} nyaralás {varos_where}
{ellatas_os} nyaralás {varos}
{ellatas_os} nyaralás {varos} {orszag}
{ellatas_os} nyaralás {orszag} {varos}
{varos} {ellatas_os} nyaralás
{varos} {orszag} {ellatas_os} nyaralás
{orszag} {varos} {ellatas_os} nyaralás
{orszag_what} {ellatas_os} nyaralás {varos}
{varos_what} {ellatas_os} nyaralás
{ellatas_os} nyaralás {varos_where}
tengerparti nyaralás {varos}
tengerparti nyaralás {varos} {orszag}
tengerparti nyaralás {orszag} {varos}
{varos} tengerparti nyaralás
{varos} {orszag} tengerparti nyaralás
{orszag} {varos} tengerparti nyaralás
{orszag_what} tengerparti nyaralás {varos}
{varos_what} tengerparti nyaralás
tengerparti nyaralás {varos_where}
utazás {varos}
utazás {varos} {orszag}
utazás {orszag} {varos}
{varos} utazás
{varos} {orszag} utazás
{orszag} {varos} utazás
{orszag_what} utazás {varos}
{varos_what} utazás
utazás {varos_where}
kedvezményes utazás {varos}
kedvezményes utazás {varos} {orszag}
kedvezményes utazás {orszag} {varos}
{varos} kedvezményes utazás
{varos} {orszag} kedvezményes utazás
{orszag} {varos} kedvezményes utazás
{orszag_what} kedvezményes utazás {varos}
{varos_what} kedvezményes utazás
kedvezményes utazás {varos_where}
olcsó utazás {varos}
olcsó utazás {varos} {orszag}
olcsó utazás {orszag} {varos}
{varos} olcsó utazás
{varos} {orszag} olcsó utazás
{orszag} {varos} olcsó utazás
{orszag_what} olcsó utazás {varos}
{varos_what} olcsó utazás
olcsó utazás {varos_where}
lastminute utazás {varos}
lastminute utazás {varos} {orszag}
lastminute utazás {orszag} {varos}
{varos} lastminute utazás
{varos} {orszag} lastminute utazás
{orszag} {varos} lastminute utazás
{orszag_what} lastminute utazás {varos}
{varos_what} lastminute utazás
lastminute utazás {varos_where}
akciós utazás {varos}
akciós utazás {varos} {orszag}
akciós utazás {orszag} {varos}
{varos} akciós utazás
{varos} {orszag} akciós utazás
{orszag} {varos} akciós utazás
{orszag_what} akciós utazás {varos}
{varos_what} akciós utazás
akciós utazás {varos_where}
last minute utazás {varos}
last minute utazás {varos} {orszag}
last minute utazás {orszag} {varos}
{varos} last minute utazás
{varos} {orszag} last minute utazás
{orszag} {varos} last minute utazás
{orszag_what} last minute utazás {varos}
{varos_what} last minute utazás
last minute utazás {varos_where}
{utazas_modja_os} utazás {varos}
{utazas_modja_os} utazás {varos} {orszag}
{utazas_modja_os} utazás {orszag} {varos}
{varos} {utazas_modja_os} utazás
{varos} {orszag} {utazas_modja_os} utazás
{orszag} {varos} {utazas_modja_os} utazás
{orszag_what} {utazas_modja_os} utazás {varos}
{varos_what} {utazas_modja_os} utazás
{utazas_modja_os} utazás {varos_where}
{ellatas_os} utazás {varos}
{ellatas_os} utazás {varos} {orszag}
{ellatas_os} utazás {orszag} {varos}
{varos} {ellatas_os} utazás
{varos} {orszag} {ellatas_os} utazás
{orszag} {varos} {ellatas_os} utazás
{orszag_what} {ellatas_os} utazás {varos}
{varos_what} {ellatas_os} utazás
{ellatas_os} utazás {varos_where}
tengerparti utazás {varos}
tengerparti utazás {varos} {orszag}
tengerparti utazás {orszag} {varos}
{varos} tengerparti utazás
{varos} {orszag} tengerparti utazás
{orszag} {varos} tengerparti utazás
{orszag_what} tengerparti utazás {varos}
{varos_what} tengerparti utazás
tengerparti utazás {varos_where}
üdülés {varos}
üdülés {varos} {orszag}
üdülés {orszag} {varos}
{varos} üdülés
{varos} {orszag} üdülés
{orszag} {varos} üdülés
{orszag_what} üdülés {varos}
{varos_what} üdülés
üdülés {varos_where}
kedvezményes üdülés {varos}
kedvezményes üdülés {varos} {orszag}
kedvezményes üdülés {orszag} {varos}
{varos} kedvezményes üdülés
{varos} {orszag} kedvezményes üdülés
{orszag} {varos} kedvezményes üdülés
{orszag_what} kedvezményes üdülés {varos}
{varos_what} kedvezményes üdülés
kedvezményes üdülés {varos_where}
olcsó üdülés {varos}
olcsó üdülés {varos} {orszag}
olcsó üdülés {orszag} {varos}
{varos} olcsó üdülés
{varos} {orszag} olcsó üdülés
{orszag} {varos} olcsó üdülés
{orszag_what} olcsó üdülés {varos}
{varos_what} olcsó üdülés
olcsó üdülés {varos_where}
lastminute üdülés {varos}
lastminute üdülés {varos} {orszag}
lastminute üdülés {orszag} {varos}
{varos} lastminute üdülés
{varos} {orszag} lastminute üdülés
{orszag} {varos} lastminute üdülés
{orszag_what} lastminute üdülés {varos}
{varos_what} lastminute üdülés
lastminute üdülés {varos_where}
akciós üdülés {varos}
akciós üdülés {varos} {orszag}
akciós üdülés {orszag} {varos}
{varos} akciós üdülés
{varos} {orszag} akciós üdülés
{orszag} {varos} akciós üdülés
{orszag_what} akciós üdülés {varos}
{varos_what} akciós üdülés
akciós üdülés {varos_where}
last minute üdülés {varos}
last minute üdülés {varos} {orszag}
last minute üdülés {orszag} {varos}
{varos} last minute üdülés
{varos} {orszag} last minute üdülés
{orszag} {varos} last minute üdülés
{orszag_what} last minute üdülés {varos}
{varos_what} last minute üdülés
last minute üdülés {varos_where}
{utazas_modja_os} üdülés {varos}
{utazas_modja_os} üdülés {varos} {orszag}
{utazas_modja_os} üdülés {orszag} {varos}
{varos} {utazas_modja_os} üdülés
{varos} {orszag} {utazas_modja_os} üdülés
{orszag} {varos} {utazas_modja_os} üdülés
{orszag_what} {utazas_modja_os} üdülés {varos}
{varos_what} {utazas_modja_os} üdülés
{utazas_modja_os} üdülés {varos_where}
{ellatas_os} üdülés {varos}
{ellatas_os} üdülés {varos} {orszag}
{ellatas_os} üdülés {orszag} {varos}
{varos} {ellatas_os} üdülés
{varos} {orszag} {ellatas_os} üdülés
{orszag} {varos} {ellatas_os} üdülés
{orszag_what} {ellatas_os} üdülés {varos}
{varos_what} {ellatas_os} üdülés
{ellatas_os} üdülés {varos_where}
tengerparti üdülés {varos}
tengerparti üdülés {varos} {orszag}
tengerparti üdülés {orszag} {varos}
{varos} tengerparti üdülés
{varos} {orszag} tengerparti üdülés
{orszag} {varos} tengerparti üdülés
{orszag_what} tengerparti üdülés {varos}
{varos_what} tengerparti üdülés
tengerparti üdülés {varos_where}";
$citykeywords = explode("\n",$citykeywords);

$regionkeywords = "
nyaralás {regio}
nyaralás {regio} {orszag}
nyaralás {orszag} {regio}
{regio} nyaralás
{regio} {orszag} nyaralás
{orszag} {regio} nyaralás
{orszag_what} nyaralás {regio}
{regio_what} nyaralás
nyaralás {regio_where}
kedvezményes nyaralás {regio}
kedvezményes nyaralás {regio} {orszag}
kedvezményes nyaralás {orszag} {regio}
{regio} kedvezményes nyaralás
{regio} {orszag} kedvezményes nyaralás
{orszag} {regio} kedvezményes nyaralás
{orszag_what} kedvezményes nyaralás {regio}
{regio_what} kedvezményes nyaralás
kedvezményes nyaralás {regio_where}
olcsó nyaralás {regio}
olcsó nyaralás {regio} {orszag}
olcsó nyaralás {orszag} {regio}
{regio} olcsó nyaralás
{regio} {orszag} olcsó nyaralás
{orszag} {regio} olcsó nyaralás
{orszag_what} olcsó nyaralás {regio}
{regio_what} olcsó nyaralás
olcsó nyaralás {regio_where}
lastminute nyaralás {regio}
lastminute nyaralás {regio} {orszag}
lastminute nyaralás {orszag} {regio}
{regio} lastminute nyaralás
{regio} {orszag} lastminute nyaralás
{orszag} {regio} lastminute nyaralás
{orszag_what} lastminute nyaralás {regio}
{regio_what} lastminute nyaralás
lastminute nyaralás {regio_where}
akciós nyaralás {regio}
akciós nyaralás {regio} {orszag}
akciós nyaralás {orszag} {regio}
{regio} akciós nyaralás
{regio} {orszag} akciós nyaralás
{orszag} {regio} akciós nyaralás
{orszag_what} akciós nyaralás {regio}
{regio_what} akciós nyaralás
akciós nyaralás {regio_where}
last minute nyaralás {regio}
last minute nyaralás {regio} {orszag}
last minute nyaralás {orszag} {regio}
{regio} last minute nyaralás
{regio} {orszag} last minute nyaralás
{orszag} {regio} last minute nyaralás
{orszag_what} last minute nyaralás {regio}
{regio_what} last minute nyaralás
last minute nyaralás {regio_where}
{utazas_modja_os} nyaralás {regio}
{utazas_modja_os} nyaralás {regio} {orszag}
{utazas_modja_os} nyaralás {orszag} {regio}
{regio} {utazas_modja_os} nyaralás
{regio} {orszag} {utazas_modja_os} nyaralás
{orszag} {regio} {utazas_modja_os} nyaralás
{orszag_what} {utazas_modja_os} nyaralás {regio}
{regio_what} {utazas_modja_os} nyaralás
{utazas_modja_os} nyaralás {regio_where}
{ellatas_os} nyaralás {regio}
{ellatas_os} nyaralás {regio} {orszag}
{ellatas_os} nyaralás {orszag} {regio}
{regio} {ellatas_os} nyaralás
{regio} {orszag} {ellatas_os} nyaralás
{orszag} {regio} {ellatas_os} nyaralás
{orszag_what} {ellatas_os} nyaralás {regio}
{regio_what} {ellatas_os} nyaralás
{ellatas_os} nyaralás {regio_where}
tengerparti nyaralás {regio}
tengerparti nyaralás {regio} {orszag}
tengerparti nyaralás {orszag} {regio}
{regio} tengerparti nyaralás
{regio} {orszag} tengerparti nyaralás
{orszag} {regio} tengerparti nyaralás
{orszag_what} tengerparti nyaralás {regio}
{regio_what} tengerparti nyaralás
tengerparti nyaralás {regio_where}
utazás {regio}
utazás {regio} {orszag}
utazás {orszag} {regio}
{regio} utazás
{regio} {orszag} utazás
{orszag} {regio} utazás
{orszag_what} utazás {regio}
{regio_what} utazás
utazás {regio_where}
kedvezményes utazás {regio}
kedvezményes utazás {regio} {orszag}
kedvezményes utazás {orszag} {regio}
{regio} kedvezményes utazás
{regio} {orszag} kedvezményes utazás
{orszag} {regio} kedvezményes utazás
{orszag_what} kedvezményes utazás {regio}
{regio_what} kedvezményes utazás
kedvezményes utazás {regio_where}
olcsó utazás {regio}
olcsó utazás {regio} {orszag}
olcsó utazás {orszag} {regio}
{regio} olcsó utazás
{regio} {orszag} olcsó utazás
{orszag} {regio} olcsó utazás
{orszag_what} olcsó utazás {regio}
{regio_what} olcsó utazás
olcsó utazás {regio_where}
lastminute utazás {regio}
lastminute utazás {regio} {orszag}
lastminute utazás {orszag} {regio}
{regio} lastminute utazás
{regio} {orszag} lastminute utazás
{orszag} {regio} lastminute utazás
{orszag_what} lastminute utazás {regio}
{regio_what} lastminute utazás
lastminute utazás {regio_where}
akciós utazás {regio}
akciós utazás {regio} {orszag}
akciós utazás {orszag} {regio}
{regio} akciós utazás
{regio} {orszag} akciós utazás
{orszag} {regio} akciós utazás
{orszag_what} akciós utazás {regio}
{regio_what} akciós utazás
akciós utazás {regio_where}
last minute utazás {regio}
last minute utazás {regio} {orszag}
last minute utazás {orszag} {regio}
{regio} last minute utazás
{regio} {orszag} last minute utazás
{orszag} {regio} last minute utazás
{orszag_what} last minute utazás {regio}
{regio_what} last minute utazás
last minute utazás {regio_where}
{utazas_modja_os} utazás {regio}
{utazas_modja_os} utazás {regio} {orszag}
{utazas_modja_os} utazás {orszag} {regio}
{regio} {utazas_modja_os} utazás
{regio} {orszag} {utazas_modja_os} utazás
{orszag} {regio} {utazas_modja_os} utazás
{orszag_what} {utazas_modja_os} utazás {regio}
{regio_what} {utazas_modja_os} utazás
{utazas_modja_os} utazás {regio_where}
{ellatas_os} utazás {regio}
{ellatas_os} utazás {regio} {orszag}
{ellatas_os} utazás {orszag} {regio}
{regio} {ellatas_os} utazás
{regio} {orszag} {ellatas_os} utazás
{orszag} {regio} {ellatas_os} utazás
{orszag_what} {ellatas_os} utazás {regio}
{regio_what} {ellatas_os} utazás
{ellatas_os} utazás {regio_where}
tengerparti utazás {regio}
tengerparti utazás {regio} {orszag}
tengerparti utazás {orszag} {regio}
{regio} tengerparti utazás
{regio} {orszag} tengerparti utazás
{orszag} {regio} tengerparti utazás
{orszag_what} tengerparti utazás {regio}
{regio_what} tengerparti utazás
tengerparti utazás {regio_where}
üdülés {orszag}
{orszag} üdülés
{orszag_what} üdülés
üdülés {orszag_where}
kedvezményes üdülés {orszag}
{orszag} kedvezményes üdülés
{orszag_what} kedvezményes üdülés
kedvezményes üdülés {orszag_where}
olcsó üdülés {orszag}
{orszag} olcsó üdülés
{orszag_what} olcsó üdülés
olcsó üdülés {orszag_where}
lastminute üdülés {orszag}
{orszag} lastminute üdülés
{orszag_what} lastminute üdülés
lastminute üdülés {orszag_where}
akciós üdülés {orszag}
{orszag} akciós üdülés
{orszag_what} akciós üdülés
akciós üdülés {orszag_where}
last minute üdülés {orszag}
{orszag} last minute üdülés
{orszag_what} last minute üdülés
last minute üdülés {orszag_where}
{utazas_modja_os} üdülés {orszag}
{orszag} {utazas_modja_os} üdülés
{orszag_what} {utazas_modja_os} üdülés
{utazas_modja_os} üdülés {orszag_where}
{ellatas_os} üdülés {orszag}
{orszag} {ellatas_os} üdülés
{orszag_what} {ellatas_os} üdülés
{ellatas_os} üdülés {orszag_where}
tengerparti üdülés {orszag}
{orszag} tengerparti üdülés
{orszag_what} tengerparti üdülés
tengerparti üdülés {orszag_where}";

$regionkeywords = explode("\n",$regionkeywords);

$param = implode(', ',$parameters);
$keywords = array(
	"country_id" => $countrykeywords,
	"region_id" => $regionkeywords,
	"city_id" => $citykeywords

);



function getmodes($type = "country_id",$id = 1)
{
	global $mysql_tour;
	$rp = array();
	
	$modes = array(
		'repulos-utazasok' => array('category'=> 'mode', 'tiny' => array('mode'=> 'repülő','mode_vel'=> 'repülővel','mode_os'=> 'repülős'), 'query' =>" offer_property.property_id  = 780 "),
		'buszos-utazasok' => array('category'=> 'mode',  'tiny' => array('mode'=> 'busz','mode_vel'=> 'busszal','mode_os'=> 'buszos'), 'query' =>" offer_property.property_id  = 1 "),
		'egyeni-utazasok' => array('category'=> 'mode',  'tiny' => array('mode'=> 'egyéni','mode_vel'=> 'egyénileg','mode_os'=> 'egyéni'), 'query' =>" offer_property.property_id  = 69884550 "),
		'onellatas' => array('category'=> 'board',  'tiny' => array('board'=> 'önellátás', 'board_vel' => 'önellátással', 'board_os' => 'önellátásos'), 'query' =>" upper(tour.board) ='ÖNELLÁTÁS' "),
		'felpanzio' => array('category'=> 'board',  'tiny' => array('board'=> 'félpanzió', 'board_vel' => 'félpanzióval', 'board_os' => 'félpanziós'), 'query' =>"  upper(tour.board) ='FÉLPANZIÓ' "),
		'teljes-ellatas' => array('category'=> 'board',  'tiny' => array('board'=> 'teljes ellátás', 'board_vel' => 'teljes ellátással', 'board_os' => 'teljes ellátásos'), 'query' =>"  upper(tour.board) ='TELJES ELLÁTÁS' "),
		'all-inclusive' => array('category'=> 'board', 'tiny' => array('board'=> 'all inclusive', 'board_vel' => 'all inclusive', 'board_os' => 'all inclusive'),  'query' =>"  upper(tour.board) ='ALL INCLUSIVE' "),
		//'ultra-all-inclusive' => array('category'=> 'board', 'tiny' => 'repülős utazások',  'query' =>"  upper(tour.board) ='ULTRA ALL INCLUSIVE' "),
		'reggeli' => array('category'=> 'board',  'tiny' => array('reggelivel'),  'query' =>" upper(tour.board)='REGGELI' ")
	
	);

	foreach($modes as $category => $extra)
	{
	
		$ck = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour  LEFT join offer_property ON offer_property.offer_id = tour.id WHERE tour.$type = $id  AND tour.aleph_ok = 1 AND $extra[query] LIMIT 1"));
	
		if($ck[name] <> '')
		{
			
			//foreach($extra[tiny] as $cat)
				$rp[$extra[category]][] = $extra[tiny];
		}
		

	}
	
	return $rp;
}

function getkeywords($type = "country_id",$country = '',$region = '',$city = '')
{
	global $keywords;
	global $parameters;
	
//$modes[mode] = array();
	
	if($type == 'country_id')
		$modes = getmodes($type,$country[id]);
	elseif($type == 'region_id')
		$modes = getmodes($type,$region[id]);
	elseif($type == 'city_id')
		$modes = getmodes($type,$city[id]);


	$kw = array();
	
	//debug($modes);
	
	
	foreach($keywords[$type] as $keyword)
	{
		
		
		/*foreach($parameters as $param)
		{
			$return.=  "$param<hr/>";
			
		}*/
		$from = $parameters;
		$to = array($country[name],
					$country[what], //magyarorszagi
					$country[where_], //magyarorszagban
					$country[whereto], //magyarorszagba
					$region[name],
					$region[what], //magyarorszagi
					$region[where_], //magyarorszagban
					$region[whereto], //magyarorszagba
					$city[name],
					$city[what], //magyarorszagi
					$city[where_], //magyarorszagban
					$city[whereto], //
				);
				
		$k = strtolower(str_replace($from,$to,$keyword));

		if(strpos(trim($k), ' ') !== false)
		{
			if (strpos($k,'{utazas_') !== false) {
				
				$spkey = '';
				
				if(empty($modes[mode] ))
					$modes[mode]  = array();
				foreach($modes[mode] as $md)
				{
					$s =  str_replace(array("{utazas_modja}","{utazas_modja_os}","{utazas_modja_vel}"), array($md[mode],$md[mode_os],$md[mode_vel]),$k);
					$spkey[$s] = $s;	
					//debug($md);
				}
			}
			else
			{								
				$kw[$k]= $k;
			}
		}
		
		if(empty($spkey))
			$spkey = array();
			
		$kw = array_merge($kw,$spkey);
		
		$r = array();
		foreach($kw as $key => $val)
		{
			if (strpos($val,'{ellatas') !== false) {
				if(empty($modes[board]))
					$modes[board]  = array();
				
				$spkey = '';	
				
				//debug($modes[board]);
				
				foreach($modes[board] as $md)
				{
					$s =  str_replace(array("{ellatas}","{ellatas_os}","{ellatas_val}"), array($md[board],$md[board_os],$md[board_vel]),$val);
					$spkey[$s] = $s;	
				}
			}
			else
			{
				$r[$key] = $val;
			}
			
		}
		
		if(empty($spkey))
			$spkey = array();
		$r = array_merge($r,$spkey);

		
		
	}
	
	foreach($r as $kv => $vl)
		$z[] = trim($vl);
		
	return $z;

}

//$return.= "<table>";
while($arr = mysql_fetch_assoc($query))
{

	$rp = array();
	
	$kw = getkeywords("country_id",$arr);
	//debug($kw);
	
	
	//$kw = '';

		/*if (strpos($keyword,'{utazas_modja}') !== false && strpos($keyword,'{ellatas}') !== false) {
			
			if(!empty($rp[$arr[id]][mode]))
			{
				foreach($rp[$arr[id]][mode] as $m => $md)
				{
					$from = array('{desztinacio}','{ellatas}','{utazas_modja}');
				
					foreach($rp[$arr[id]][board] as $b => $brd)
					{
						$to = array($arr[name],$brd,$md);
						$kw[]= str_replace($from,$to,$keyword);
					}

				}
				
			}
		
		}	*/
		/*if (strpos($keyword,'{utazas_modja}') !== false && strpos($keyword,'{ellatas}') !== true) {
			
				foreach($rp[$arr[id]][mode] as $m => $md)
				{
					$from = array('{desztinacio}','{ellatas}','{utazas_modja}');
			
						$to = array($arr[name],$brd,$md);
						$kw[]= str_replace($from,$to,$keyword);
					}

				}
				
			}
		
		}	*/
	//	else
	//	{
			//$from = array("{desztinacio}","{desztinacio_what}");
			//$to = array($arr[name],$arr[what]);
			//$kw[]= str_replace($from,$to,$keyword);
	//	}
	//}
	
	/*foreach($categories as $category => $extra)
	{
		
		//$mysql_tour->query("SELECT * FROM tour  LEFT join offer_property ON offer_property.offer_id = tour.id WHERE tour.country_id = $arr[id]  AND tour.aleph_ok = 1 $extra[query] LIMIT 1");
		
	$ck = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour  LEFT join offer_property ON offer_property.offer_id = tour.id WHERE tour.country_id = $arr[id]  AND tour.aleph_ok = 1 AND $extra[query] LIMIT 1");
	
		if($ck[name] <> '')
		{
			
			//$kw[]
		}
	}*/
	
		if($arr[offercount] == 0)
				$status = 'paused';
			else
				$status = 'active';
				
	//$return.=  "<tr>";
		$return.=  "<td colspan='3'>Search - $arr[name]</td>";
		$return.=  "<td colspan='3'>$arr[name]</td>";
		$return.=  "<td>$status</td>";
		$return.=  "<td><a href='http://www.indulhatunk.hu/utazas/$arr[url_name]' target='_blank'>http://www.indulhatunk.hu/utazas/$arr[url_name]</a></td>";
		$return.=  "<td>".implode(", ",$kw)."</td>";
		
		$line = array();
		$line[] = "Search - $arr[name]";
		$line[] = "$arr[name]";
		$line[] = "";
		$line[] = "";
		$line[] = "$status";
		$line[] = "http://www.indulhatunk.hu/utazas/$arr[url_name]";
		$line[] = implode(", ",$kw);
	
		$l = '';
		$l = implode(";",$line)."\n";
		
		echo $l;
	//$return.=  "</tr>";

		
	$q = $mysql_tour->query("SELECT * FROM region WHERE country_id = $arr[id] AND adwords = 1 ORDER BY name ASC");
	while($a = mysql_fetch_assoc($q))
	{
		$kw = array();
		
		$kw = getkeywords("region_id",$arr,$a);
		
		if($a[offercount] == 0)
			$status = 'paused';
		else
			$status = 'active';
		$return.=  "<tr>";
			$return.=  "<td colspan='3'>Search - $arr[name] - $a[name]</td>";
			$return.=  "<td style='background-color:#e7e7e7;width:20px;' colspan='2'></td>";
			$return.=  "<td>$arr[name] - $a[name]</td>";
			$return.=  "<td>$status</td>";
			$return.=  "<td><a href='http://www.indulhatunk.hu/utazas/$arr[url_name]/$a[url_name]' target='_blank'>http://www.indulhatunk.hu/utazas/$arr[url_name]/$a[url_name]</a></td>";
				$return.=  "<td>".implode(", ",$kw)."</td>";
		$return.=  "</tr>";
		
		
		$line = array();
		$line[] = "Search - $arr[name] - $a[name]";
		$line[] = "$arr[name]";
		$line[] = "$a[name]";
		$line[] = "";
		$line[] = "$status";
		$line[] = "http://www.indulhatunk.hu/utazas/$arr[url_name]/$a[url_name]";
		$line[] = implode(", ",$kw);
	
		$l = '';
		$l = implode(";",$line)."\n";
		
		echo $l;
		
		
		$q2= $mysql_tour->query("SELECT * FROM city WHERE region_id = $a[id] AND adwords = 1 ORDER BY name ASC");
		while($c = mysql_fetch_assoc($q2))
		{
			if($c[offercount] == 0)
				$status = 'paused';
			else
				$status = 'active';
			
			$kw = array();
			$kw = getkeywords("city_id",$arr,$a,$c);
			$return.=  "<tr>";
			$return.=  "<td colspan='3'>Search - $arr[name] - $a[name] - $c[name]</td>";
			$return.=  "<td style='background-color:#e7e7e7;width:20px;'></td>";
			$return.=  "<td style='background-color:orange;width:20px;'></td>";

			$return.=  "<td>$arr[name] - $a[name] - $c[name]</td>";
			$return.=  "<td>$status</td>";
			$return.=  "<td><a href='http://www.indulhatunk.hu/utazas/$arr[url_name]/$a[url_name]/$c[url_name]' target='_blank'>http://www.indulhatunk.hu/utazas/$arr[url_name]/$a[url_name]/$c[url_name]</a></td>";
			$return.=  "<td>".implode(", ",$kw)."</td>";
		$return.=  "</tr>";
		
			$line = array();
		$line[] = "Search - $arr[name] - $a[name] -  $c[name]";
		$line[] = "$arr[name]";
		$line[] = "$a[name]";
		$line[] = "$c[name]";
		$line[] = "$status";
		$line[] = "http://www.indulhatunk.hu/utazas/$arr[url_name]/$a[url_name]/$c[url_name]";
		$line[] = implode(", ",$kw);
	
		$l = '';
		$l = implode(";",$line)."\n";
		
		echo $l;



		
		}

		}
	}
//$return.=  "</table>";
?>