<?

include("../inc/tour.init.inc.php");


header("Content-type: text/xml; charset=utf-8");

echo '<?xml version="1.0" encoding="UTF-8"?>';


$lang["#COUNTRY-hu#"] = "Ungaria";
$lang["#COUNTRY-bg#"] = "Bulgaria";
$lang["#COUNTRY-it#"] = "Italia";
$lang["#COUNTRY-cz#"] = "Boemia/Cehia";
$lang["#COUNTRY-at#"] = "Austria";
$lang["#COUNTRY-me#"] = "Serbia";
$lang["#COUNTRY-de#"] = "Germania";
$lang["#COUNTRY-pl#"] = "Polonia";
$lang["#COUNTRY-si#"] = "Slovenia";
$lang["#COUNTRY-ba#"] = "Bosnia și Herțegovina";
$lang["#COUNTRY-tr#"] = "Turcia";
$lang["#COUNTRY-hr#"] = "Croația";


$mysql->query("SET NAMES 'utf8'");
?>

<OKAZII>
	<SETTINGS>
		<STATE>1</STATE>
		<INVOICE>1</INVOICE>
		<WARRANTY>2</WARRANTY>
		<FORUM>1</FORUM>
		<REPOST>1</REPOST>
		<PAYMENT>
			<PERSONAL>0</PERSONAL>
			<RAMBURS>1</RAMBURS>
			<AVANS>1</AVANS>
			<DETAILS>Detalii despre plata</DETAILS>
		</PAYMENT>
		<DELIVERY>
			<PERSONAL>0</PERSONAL>
			<DELIVERY_TIME>5</DELIVERY_TIME>
			<DETAILS>Detalii de livrare</DETAILS>
			<COURIERS>
				<NAME>Poșta Română - Coletarie</NAME>
				<AREA>in romania</AREA>
				<PRICE>4.1</PRICE>
			</COURIERS>
		</DELIVERY>
		<RETURN>
			<ACCEPT>1</ACCEPT>
			<DAYS>0</DAYS>
			<METHOD>1</METHOD>
			<COST>0</COST>
			<DETAILS></DETAILS>
		</RETURN>
	</SETTINGS>
	

<? 
	$offers = $mysql->query("SELECT * FROM offers WHERE ro_main_description <> '' AND okazii_from <= NOW() AND okazii_to >= NOW() AND active = 1 ORDER BY id ASC");
	
	while($arr = mysql_fetch_assoc($offers))
	{
		$exrate = 70;
		
		$images = '';

		if($arr[sub_partner_id] > 0)
			$arr[partner_id] = $arr[sub_partner_id];
		else
			$arr[partner_id] = $arr[partner_id];
			
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[partner_id] LIMIT 1"));
		$data[actual_price] = round($arr[actual_price]/$exrate);
		$data[normal_price] = round($arr[normal_price]/$exrate);
		
		if($partner[image_num] > 10)
			$partner[image_num] = 10;
			
		for($i=1;$i<$partner[image_num];$i++)
		{
			if($i < 10)
				$z = "0$i";
			else
				$z = $i;
				
			$images.="<URL>http://static.indulhatunk.hu/uploaded_images/accomodation/$partner[coredb_id]/$partner[coredb_package]/orig/$z.jpg</URL>";
			
		}
		
		//$arr[ro_main_description] = str_replace("#PRICE#",formatPrice($arr[actual_price]),$arr[ro_main_description]." RON");

		$body = $arr[ro_main_description];
if(file_exists("/var/www/hosts/pihenni.hu/htdocs/static.pihenni/fullhouse/".$arr[partner_id]."-ro.jpg") && $_GET[disablebooking] <> 1)
	{
		$body = str_replace('/more.jpg) no-repeat; width: 715px; height: 120px;">&nbsp;</div>','/more.jpg) no-repeat; width: 715px; height: 120px;"></div>
<img src="http://admin.indulhatunk.hu/fullhouse/'.$arr[partner_id].'-ro.jpg" style="margin:0 0px 0 10px"/>

',$body);

	}
	
	
	$body = str_replace("/background.jpg) repeat-y;","/background.jpg) repeat-y;line-height:normal !important;",$body);
	
	$body = str_replace('<div style="text-align: center; font-size: 10px; color: #aca89c; font-family: tahoma; padding: 10px 0 10px 0;">Az aj&aacute;nlat a Sz&aacute;ll&aacute;shely &eacute;s a Sz&aacute;ll&aacute;s Outlet k&ouml;z&ouml;tti egy&eacute;b marketingmeg&aacute;llapod&aacute;s eredm&eacute;nyek&eacute;nt jelenik meg limit&aacute;lt darabsz&aacute;mban &eacute;s a Sz&aacute;ll&aacute;s Outlet saj&aacute;t <br />tulajdon&aacute;t k&eacute;pezi. Az aj&aacute;nlat, a sz&aacute;ll&aacute;shellyel egy&uuml;ttműk&ouml;dve ker&uuml;lt kidolgoz&aacute;sra.</div>','',$body);


		$body = str_replace(formatPrice($arr[actual_price]),formatPrice($arr[actual_price],0,1,'RON'),$body);
		$body = str_replace(formatPrice($arr[actual_price],2),formatPrice($arr[actual_price],0,1,'RON'),$body);
		$body = str_replace(formatPrice($arr[normal_price],3),formatPrice($arr[normal_price],0,1,'RON'),$body);

		$body = str_replace("#PRICE#",formatPrice($arr[actual_price],0,1,'RON'),$body);

		$body = str_replace("header","headerro",$body);
		$body = str_replace("more","morero",$body);
		$body = str_replace("christmasblue","dblue",$body);

		$arr[ro_main_description] = $body;
		
		
		//die;
		
		if (strpos($partner[hotel_name],$partner[city]) !== false) {
		
		$from = array('ö','Ö','ú','Ú','ü','Ü','ű','Ű','ő','Ő','é','É','í','Í');
		
		$to = array('o','O','u','U','U','U','U','U','o','O','e','E','i','I');
			$pname = str_replace($from,$to,$partner[hotel_name]);
					
		}
		else
			$pname = "$partner[hotel_name] $partner[city]";

			
		if($partner[country] <> '')
			$pname = "$pname, ".$lang["#COUNTRY-".$partner[country]."#"];
		elseif($arr[country] == '')
			$pname = "$pname, Ungaria";		
		else
			$pname = "$pname, $arr[country]";
			
		echo "<AUCTION>
		<UNIQUEID>$arr[id]</UNIQUEID>
		<TITLE>".str_replace("&","&amp;","$pname - $arr[ro_name]")."</TITLE>
		<CATEGORY>".str_replace("&","&amp;","$partner[city], $partner[hotel_name]")."</CATEGORY>
		<DESCRIPTION><![CDATA[$arr[ro_main_description]]]></DESCRIPTION>
		<PRICE>$data[normal_price]</PRICE>
		<DISCOUNT_PRICE>$data[actual_price]</DISCOUNT_PRICE>
		<CURRENCY>RON</CURRENCY>
		<AMOUNT>100</AMOUNT>
		<PHOTOS>
			$images
		</PHOTOS>
		<STATE>1</STATE>
		<INVOICE>1</INVOICE>
		<WARRANTY>0</WARRANTY>
		<FORUM>1</FORUM>
		<REPOST>2</REPOST>
		<PAYMENT>
			<PERSONAL>0</PERSONAL>
			<RAMBURS>1</RAMBURS>
			<AVANS>1</AVANS>
			<DETAILS>Detalii despre plata custom</DETAILS>
		</PAYMENT>
		<DELIVERY>
			<PERSONAL>0</PERSONAL>
			<DELIVERY_TIME>10</DELIVERY_TIME>
			<COURIERS>
				<NAME>Poșta Română - Coletarie</NAME>
				<AREA>in romania</AREA>
				<PRICE>4.1</PRICE>
				<CURRENCY>RON</CURRENCY>
			</COURIERS>
			<DETAILS>Detalii de livrare custom</DETAILS>
		</DELIVERY>
		<RETURN>
			<ACCEPT>1</ACCEPT>
			<DAYS>0</DAYS>
			<METHOD>1</METHOD>
			<COST>0</COST>
			<DETAILS>Detalii de retur custom</DETAILS>
		</RETURN>
	</AUCTION>
";
		
	}
/* 	<!-- -->
	<!--
	<AUCTION>
		<UNIQUEID>1</UNIQUEID>
		<TITLE>Titlu de test</TITLE>
		<CATEGORY>Categorie din magazin</CATEGORY>
		<DESCRIPTION><![CDATA[Descriere de test]]></DESCRIPTION>
		<PRICE>10</PRICE>
		<CURRENCY>RON</CURRENCY>
		<AMOUNT>1</AMOUNT>
		<BRAND>nume brand</BRAND>
		<PHOTOS>
			<URL>http://static1.okr.ro/images/magazine/logo.gif</URL>
			<URL>http://www.okazii.ro/img/other/final/logo.gif</URL>
		</PHOTOS>
		<ATTRIBUTES>
			<CULOARE>Alb</CULOARE>
		</ATTRIBUTES>
	</AUCTION>
	<AUCTION>
		<UNIQUEID>2</UNIQUEID>
		<TITLE>titlu de test2</TITLE>
		<CATEGORY>Alta categorie din magazin</CATEGORY>
		<DESCRIPTION><![CDATA[descriere de test2]]></DESCRIPTION>
		<PRICE>20</PRICE>
		<CURRENCY>RON</CURRENCY>
		<AMOUNT>5</AMOUNT>
		<BRAND>Nume brand</BRAND>
		<PHOTOS>
			<URL>http://static1.okr.ro/images/magazine/logo.gif</URL>
			<URL>http://www.okazii.ro/img/other/final/logo.gif</URL>
		</PHOTOS>
		<STATE>2</STATE>
		<INVOICE>2</INVOICE>
		<WARRANTY>2</WARRANTY>
		<FORUM>1</FORUM>
		<REPOST>2</REPOST>
		<PAYMENT>
			<PERSONAL>0</PERSONAL>
			<RAMBURS>1</RAMBURS>
			<AVANS>1</AVANS>
			<DETAILS>Detalii despre plata custom</DETAILS>
		</PAYMENT>
		<DELIVERY>
			<PERSONAL>0</PERSONAL>
			<DELIVERY_TIME>10</DELIVERY_TIME>
			<COURIERS>
				<NAME>FanCourier</NAME>
				<AREA>in romania</AREA>
				<PRICE>12</PRICE>
				<CURRENCY>RON</CURRENCY>
			</COURIERS>
			<DETAILS>Detalii de livrare custom</DETAILS>
		</DELIVERY>
		<RETURN>
			<ACCEPT>1</ACCEPT>
			<DAYS>3</DAYS>
			<METHOD>2</METHOD>
			<COST>1</COST>
			<DETAILS>Detalii de retur custom</DETAILS>
		</RETURN>
		<ATTRIBUTES>
			<CULOARE>Albastru</CULOARE>
		</ATTRIBUTES>
	</AUCTION>
	-->
	<!-- -->
*/?>
	
</OKAZII>