<?
header('Content-Type: text/xml; charset=utf-8');


$mysql->query("SET names 'utf8'");


$query = $mysql->query("SELECT count(cid) as cnt, sum(orig_price), partners.hotel_name,partners.pid FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE  paid = 1 and inactive = 0 AND customers.added < NOW() - INTERVAL 3 MONTH  GROUP BY customers.pid  ORDER by customers.added DESC");

$i = 1;
while($arr = mysql_fetch_assoc($query))
{
	$priority[$arr[pid]] = $i;
	
	$i++;
}


$lang["#COUNTRY-hu#"] = "Maďarsko";
$lang["#COUNTRY-bg#"] = "Bulharsko";
$lang["#COUNTRY-it#"] = "Taliansko";
$lang["#COUNTRY-cz#"] = "Česko";
$lang["#COUNTRY-at#"] = "Rakúsko";
$lang["#COUNTRY-me#"] = "Srbsko";
$lang["#COUNTRY-de#"] = "Nemecko";
$lang["#COUNTRY-pl#"] = "Poľsko";
$lang["#COUNTRY-si#"] = "Slovinsko";
$lang["#COUNTRY-ba#"] = "Bosna a Hercegovina";
$lang["#COUNTRY-tr#"] = "Turecko";
$lang["#COUNTRY-hr#"] = "Croatia";
$lang["#COUNTRY-hu#"] = "Maďarsko";
$lang["#COUNTRY-bg#"] = "Bulharsko";
$lang["#COUNTRY-ro#"] = "Rumunsko";
$lang["#COUNTRY-it#"] = "Taliansko";
$lang["#COUNTRY-cz#"] = "Česko";
$lang["#COUNTRY-at#"] = "Rakúsko";
$lang["#COUNTRY-me#"] = "Srbsko";
$lang["#COUNTRY-de#"] = "Nemecko";
$lang["#COUNTRY-pl#"] = "Poľsko";
$lang["#COUNTRY-si#"] = "Slovinsko";
$lang["#COUNTRY-ba#"] = "Bosna a Hercegovina";
$lang["#COUNTRY-tr#"] = "Turecko";
$lang["#COUNTRY-hr#"] = "Chorvátsko";
$lang["#COUNTRY-gr#"] = "Grécko";
$lang["#COUNTRY-af#"] = "Afganistan";
$lang["#COUNTRY-al#"] = "Albansko";
$lang["#COUNTRY-dz#"] = "Alžírsko";
$lang["#COUNTRY-ad#"] = "Andorra";
$lang["#COUNTRY-ao#"] = "Angola";
$lang["#COUNTRY-aq#"] = "Antarktída";
$lang["#COUNTRY-ar#"] = "Argentína";
$lang["#COUNTRY-am#"] = "Arménsko";
$lang["#COUNTRY-au#"] = "Austrália";
$lang["#COUNTRY-az#"] = "Azerbajdžan";
$lang["#COUNTRY-bh#"] = "Bahrajn";
$lang["#COUNTRY-bd#"] = "Bangladéš";
$lang["#COUNTRY-bb#"] = "Barbados";
$lang["#COUNTRY-by#"] = "Bielorusko";
$lang["#COUNTRY-be#"] = "Belgicko";
$lang["#COUNTRY-bj#"] = "Benin";
$lang["#COUNTRY-bm#"] = "Bermudy";
$lang["#COUNTRY-bt#"] = "Bhután";
$lang["#COUNTRY-bo#"] = "Bolívia";
$lang["#COUNTRY-ba#"] = "Bosna a Hercegovina";
$lang["#COUNTRY-bw#"] = "Botswana";
$lang["#COUNTRY-br#"] = "Brazília";
$lang["#COUNTRY-io#"] = "Britské indickooceánske územie";
$lang["#COUNTRY-bn#"] = "Brunej";
$lang["#COUNTRY-bf#"] = "Burkina Faso";
$lang["#COUNTRY-bi#"] = "Burundi";
$lang["#COUNTRY-kh#"] = "Kambodža";
$lang["#COUNTRY-cm#"] = "Kamerun";
$lang["#COUNTRY-ca#"] = "Kanada";
$lang["#COUNTRY-cv#"] = "Kapverdy";
$lang["#COUNTRY-ky#"] = "Kajmanské ostrovy";
$lang["#COUNTRY-cf#"] = "Stredoafrická republika";
$lang["#COUNTRY-td#"] = "Čad";
$lang["#COUNTRY-cl#"] = "Čile";
$lang["#COUNTRY-cn#"] = "Čína";
$lang["#COUNTRY-cx#"] = "Vianočný ostrov";
$lang["#COUNTRY-cc#"] = "Kokosové ostrovy (Keeling)";
$lang["#COUNTRY-co#"] = "Kolumbia";
$lang["#COUNTRY-km#"] = "Komory";
$lang["#COUNTRY-cg#"] = "Kongo";
$lang["#COUNTRY-ck#"] = "Cookove ostrovy";
$lang["#COUNTRY-cr#"] = "Kostarika";
$lang["#COUNTRY-cu#"] = "Kuba";
$lang["#COUNTRY-cy#"] = "Cyprus";
$lang["#COUNTRY-cd#"] = "Demokratická republika Kongo";
$lang["#COUNTRY-dk#"] = "Dánsko";
$lang["#COUNTRY-dj#"] = "Džibuti";
$lang["#COUNTRY-dm#"] = "Dominika";
$lang["#COUNTRY-do#"] = "Dominikánska republika";
$lang["#COUNTRY-tp#"] = "Východný Timor";
$lang["#COUNTRY-ec#"] = "Ekvádor";
$lang["#COUNTRY-eg#"] = "Egypt";
$lang["#COUNTRY-sv#"] = "Salvádor";
$lang["#COUNTRY-gq#"] = "Rovníková Guinea";
$lang["#COUNTRY-er#"] = "Eritrea";
$lang["#COUNTRY-ee#"] = "Estónsko";
$lang["#COUNTRY-et#"] = "Etiópia";
$lang["#COUNTRY-fk#"] = "Falklandské ostrovy (Malvíny)";
$lang["#COUNTRY-fo#"] = "Faerské ostrovy";
$lang["#COUNTRY-fj#"] = "Fidži";
$lang["#COUNTRY-fi#"] = "Fínsko";
$lang["#COUNTRY-fr#"] = "Francúzsko";
$lang["#COUNTRY-fx#"] = "Metropolitné Francúzsko";
$lang["#COUNTRY-gf#"] = "Francúzska Guinea";
$lang["#COUNTRY-pf#"] = "Francúzska Polynesia";
$lang["#COUNTRY-tf#"] = "Francúzske južné územia";
$lang["#COUNTRY-ga#"] = "Gabon";
$lang["#COUNTRY-gm#"] = "Gambia";
$lang["#COUNTRY-ge#"] = "Gruzínsko";
$lang["#COUNTRY-gh#"] = "Ghana";
$lang["#COUNTRY-gi#"] = "Gibraltár";
$lang["#COUNTRY-gl#"] = "Grónsko";
$lang["#COUNTRY-gd#"] = "Grenada";
$lang["#COUNTRY-gp#"] = "Guadeloupe";
$lang["#COUNTRY-gu#"] = "Guam";
$lang["#COUNTRY-gt#"] = "Guatemala";
$lang["#COUNTRY-gn#"] = "Guinea";
$lang["#COUNTRY-gw#"] = "Guinea-Bissau";
$lang["#COUNTRY-gy#"] = "Guyana";
$lang["#COUNTRY-ht#"] = "Haiti";
$lang["#COUNTRY-hm#"] = "Heardov ostrov a McDonaldove ostrovy";
$lang["#COUNTRY-hn#"] = "Honduras";
$lang["#COUNTRY-hk#"] = "Hongkong";
$lang["#COUNTRY-is#"] = "Island";
$lang["#COUNTRY-in#"] = "India";
$lang["#COUNTRY-id#"] = "Indonézia";
$lang["#COUNTRY-ir#"] = "Irán";
$lang["#COUNTRY-iq#"] = "Irak";
$lang["#COUNTRY-ie#"] = "Írsko";
$lang["#COUNTRY-il#"] = "Izrael";
$lang["#COUNTRY-jm#"] = "Jamajka";
$lang["#COUNTRY-jp#"] = "Japonso";
$lang["#COUNTRY-jo#"] = "Jordánsko";
$lang["#COUNTRY-kz#"] = "Kazachstan";
$lang["#COUNTRY-ke#"] = "Keňa";
$lang["#COUNTRY-ki#"] = "Kiribati";
$lang["#COUNTRY-kw#"] = "Kuvajt";
$lang["#COUNTRY-kg#"] = "Kirgizsko";
$lang["#COUNTRY-la#"] = "Laos";
$lang["#COUNTRY-lv#"] = "Estónsko";
$lang["#COUNTRY-lb#"] = "Libanon";
$lang["#COUNTRY-ls#"] = "Lesotho";
$lang["#COUNTRY-lr#"] = "Libéria";
$lang["#COUNTRY-ly#"] = "Líbya";
$lang["#COUNTRY-li#"] = "Lichtenštajnsko";
$lang["#COUNTRY-lt#"] = "Litva";
$lang["#COUNTRY-lu#"] = "Luxembursko";
$lang["#COUNTRY-mo#"] = "Macao";
$lang["#COUNTRY-mk#"] = "Macedónsko";
$lang["#COUNTRY-mg#"] = "Madagaskar";
$lang["#COUNTRY-mw#"] = "Malawi";
$lang["#COUNTRY-my#"] = "Malajzia";
$lang["#COUNTRY-mv#"] = "Maldivy";
$lang["#COUNTRY-ml#"] = "Mali";
$lang["#COUNTRY-mt#"] = "Malta";
$lang["#COUNTRY-mh#"] = "Marshallove ostrovy";
$lang["#COUNTRY-mq#"] = "Martinik";
$lang["#COUNTRY-mr#"] = "Mauritánia";
$lang["#COUNTRY-mu#"] = "Maurícius";
$lang["#COUNTRY-yt#"] = "Mayotte";
$lang["#COUNTRY-me#"] = "Srbsko";
$lang["#COUNTRY-mx#"] = "Mexiko";
$lang["#COUNTRY-fm#"] = "Mikronézia";
$lang["#COUNTRY-md#"] = "Moldavsko";
$lang["#COUNTRY-mc#"] = "Monako";
$lang["#COUNTRY-mn#"] = "Mongolsko";
$lang["#COUNTRY-ms#"] = "Montserrat";
$lang["#COUNTRY-ma#"] = "Maroko";
$lang["#COUNTRY-mz#"] = "Mozambik";
$lang["#COUNTRY-mm#"] = "Mjanmarsko (Barma)";
$lang["#COUNTRY-na#"] = "Namíbia";
$lang["#COUNTRY-nr#"] = "Nauru";
$lang["#COUNTRY-np#"] = "Nepál";
$lang["#COUNTRY-nl#"] = "Holandsko";
$lang["#COUNTRY-an#"] = "Holandské Antily";
$lang["#COUNTRY-nc#"] = "Nová Kaledónia";
$lang["#COUNTRY-nz#"] = "Nový Zéland";
$lang["#COUNTRY-nu#"] = "Niue";
$lang["#COUNTRY-nf#"] = "Norfolk";
$lang["#COUNTRY-kp#"] = "Severná Kórea";
$lang["#COUNTRY-mp#"] = "Severné Mariány";
$lang["#COUNTRY-no#"] = "Nórsko";
$lang["#COUNTRY-om#"] = "Omán";
$lang["#COUNTRY-pk#"] = "Pakistan";
$lang["#COUNTRY-pw#"] = "Palau";
$lang["#COUNTRY-pa#"] = "Panama";
$lang["#COUNTRY-pg#"] = "Papua-Nová Guinea";
$lang["#COUNTRY-py#"] = "Paraguaj";
$lang["#COUNTRY-pe#"] = "Peru";
$lang["#COUNTRY-ph#"] = "Filipíny";
$lang["#COUNTRY-pt#"] = "Portugalsko";
$lang["#COUNTRY-pr#"] = "Portoriko";
$lang["#COUNTRY-ru#"] = "Rusko";
$lang["#COUNTRY-rw#"] = "Rwanda";
$lang["#COUNTRY-sh#"] = "Svätá Helena";
$lang["#COUNTRY-kn#"] = "Svätý Krištof a Nevis";
$lang["#COUNTRY-lc#"] = "Svätá Lucia";
$lang["#COUNTRY-pm#"] = "Svätý Pierre";
$lang["#COUNTRY-vc#"] = "Svätý Vincent";
$lang["#COUNTRY-sm#"] = "San Maríno";
$lang["#COUNTRY-st#"] = "Svätý Tomáš a Princov ostrov";
$lang["#COUNTRY-sa#"] = "Saudská Arábia";
$lang["#COUNTRY-sn#"] = "Senegal";
$lang["#COUNTRY-sc#"] = "Seychely";
$lang["#COUNTRY-sl#"] = "Sierra Leone";
$lang["#COUNTRY-sg#"] = "Singapur";
$lang["#COUNTRY-es#"] = "Španielsko";
$lang["#COUNTRY-lk#"] = "Srí Lanka";
$lang["#COUNTRY-se#"] = "Švédsko";
$lang["#COUNTRY-ch#"] = "Švajčiarsko";
$lang["#COUNTRY-th#"] = "Thajsko";
$lang["#COUNTRY-tn#"] = "Tunisko";
$lang["#COUNTRY-ua#"] = "Ukrajina";
$lang["#COUNTRY-ae#"] = "Spojené arabské emiráty";
$lang["#COUNTRY-uk#"] = "Spojené kráľovstvo";
$lang["#COUNTRY-us#"] = "Spojené štáty americké";
$lang["#COUNTRY-um#"] = "Menšie odľahlé ostrovy Spojených štátov";
$lang["#COUNTRY-uy#"] = "Uruguaj";
$lang["#COUNTRY-uz#"] = "Uzbekistan";
$lang["#COUNTRY-vu#"] = "Vanuatu";
$lang["#COUNTRY-va#"] = "Vatikán";
$lang["#COUNTRY-ve#"] = "Venezuela";
$lang["#COUNTRY-vn#"] = "Vietnam";
$lang["#COUNTRY-yu#"] = "Jugoszlávia";
$lang["#COUNTRY-zm#"] = "Zambia";
$lang["#COUNTRY-zw#"] = "Zimbabwe";
$lang["#COUNTRY-sk#"] = "Slovensko";

$lang["Budapest"] = "Budapešť";
$lang["Visegrád"] = "Vyšehrad";


$lang["Little Bucharest Old Town Hostel"] = "Little Bucharest Old Town Hostel";
$lang["Absolut Apartman Hajdúszoboszló"] = "Apartmán Absolut";
$lang["Anna Grand Hotel**** Wine & Vital"] = "Hotel Anna Grand**** Vin & Vital";
$lang["Aparthotel Castrum Novum Novigrad"] = "Aparthotel Castrum Novum Novigrad";
$lang["Apartman Hotel Sárvár /nmb"] = "Apartman Hotel";
$lang["Aplend Mountain Resort*** Felsőtátrafüred (Horný Smokovec) Magas-Tátra"] = "Aplend Mountain Resort***";
$lang["Aqua-Lux*** Wellness Hotel"] = "Wellness Hotel Aqua-Lux";
$lang["Aqua-Spa**** Wellness Hotel"] = "Aqua-Spa**** Wellness Hotel";
$lang["Arany Prága - Prága, Karlovy Vary, Telc"] = "Arany Praha - Praha, Karlovy Vary, Telc";
$lang["Aranykereszt Hotel Gyula"] = "Hotel Aranykereszt";
$lang["Arena Savaria Szombathely"] = "Hosťovské izby Arena Savaria";
$lang["Astoria Hévíz Panzió"] = "Penzión Astoria";
$lang["Auschwitz - Krakkó autóbuszos utazás"] = "Auschwitz - Krakov s autobusom";
$lang["Bagolyvár Étterem és Vendégház Inárcs"] = "Reštaurácia a hostinec Bagolyvár";
$lang["Balaton Panzió és Apartman Siófok "] = "Penzión a Apartmán Balaton";
$lang["Bambara Hotel****premium"] = "Hotel Bambara****";
$lang["Barcelo Praha Five"] = "Barcelo Praha Five";
$lang["Bástya Wellness Hotel Miskolctapolca"] = "Wellness Hotel Bástya";
$lang["Batthyány Kastélyszálló Zalacsány"] = "Hotel Kaštieľ Batthány";
$lang["CE Plaza Hotel**** Siófok"] = "Hotel CE Plaza****";
$lang["Bellevue Wellness Hotel"] = "Hotel Bellevue Wellness";
$lang["Bodrogi Kúria**** Wellness Hotel"] = "Wellness Hotel Bodrogi Kúria****";
$lang["Bonne Chance Hotel Bázakerettye"] = "Bonne Chance Hotel";
$lang["Calimbra Wellness Hotel****"] = "Hotel Wellness Calimbra****";
$lang["Carpe Diem Apartman Hotel"] = "Carpe Diem Apartman Hotel";
$lang["Cizici Bayview Residence"] = "Cizici Bayview Residence";
$lang["Club 218 Premium Wellness Apartman"] = "Club 218 Premium Wellness Apartmán";
$lang["Club Kehida Aparthotel***"] = "Club Kehida Aparthotel***";
$lang["Corvin Gyulai Wellness Apartmanok"] = "Wellness apartmány Corvin";
$lang["D-Hotel*** Gyula"] = "D-Hotel***";
$lang["Diamant Hotel Szigetköz"] = "Hotel Diamant";
$lang["Dóm Hotel**** Szeged"] = "Hotel Dóm****";
$lang["Domínium Vendégház Sümeg"] = "Hostinec Domínium";
$lang["Dream Hill Business Deluxe Hotel Isztambul"] = "Hotel Dream Hill Business Deluxe Istambul";
$lang["Duna Parti Vendégház"] = "Hostinec na brehu Dunaja";
$lang["Echo Residence All Suite Hotel Tihany"] = "Echo Residence All Suite Hotel";
$lang["Egri Korona Borház"] = "Vinársky dom Egri Korona";
$lang["Erdei Wellness Apartmanpark"] = "Apartmán Park Erdei Wellness";
$lang["Éva Panzió Tiszafüred"] = "Penzión Éva";
$lang["Felfedező úton a Délvidéken"] = "Objavná cesta na južnom vidieku";
$lang["Főnix Club Hotel**** Hévíz"] = "Club Hotel Főnix****";
$lang["Gästehaus Jager Ausztria Mariapfarr"] = "Gästehaus Jager";
$lang["Gasthof Kreischberg"] = "Gasthof Kreischberg";
$lang["Gerendás Vendégház Egerszalók"] = "Hostinec Gerendás";
$lang["Gondűző Szálloda és Étterem"] = "Hotel a Reštaurácia Gondűző";
$lang["Gosztola Gyöngye Wellness Hotel"] = "Wellness Hotel Gosztola Gyöngye";
$lang["Grand Hotel Aranybika Debrecen"] = "Grand Hotel Aranybika";
$lang["Grandhotel Evropa Prága"] = "Grandhotel Evropa";
$lang["Gunaras Resort Spa Hotel****superior"] = "Gunaras Resort Spa Hotel****superior";
$lang["Gyopár Hotel Orosháza-Gyopárosfürdő"] = "Hotel Gyopár";
$lang["Happy Star Club Hotel"] = "Happy Star Club Hotel";
$lang["Harmónia Wellness Villa Szilvásvárad"] = "Wellness Vila Harmónia";
$lang["Hollókői Palóc Üdülőházak"] = "Palócske Rekreačné domy v Hollókő";
$lang["Hotel Achillion Athén"] = "Hotel Achillion";
$lang["Hotel Aida**** Prága"] = "Hotel Aida****";
$lang["Hotel Aqua*** Eger"] = "Hotel Aqua***";
$lang["Hotel Aquarell**** Cegléd"] = "Hotel Aquarell****";
$lang["Hotel Arkadia*** Pécs"] = "Hotel Arkadia***";
$lang["Hotel Aurora**** Miskolctapolca"] = "Hotel Aurora****";
$lang["Hotel Barbizon Nyíregyháza-Sóstófürdő"] = "Hotel Barbizon";
$lang["Hotel Brixen Praga"] = "Hotel Brixen Praha";
$lang["Hotel Budapest Hilton"] = "Hotel Budapest Hilton";
$lang["Hotel Budapest Szófia"] = "Hotel Budapest";
$lang["Hotel Bungallo Hajdúszoboszló"] = "Hotel Bungallo";
$lang["Hotel Carmen Balatongyörök"] = "Hotel Carmen";
$lang["Hotel Charles Central*** Prága"] = "Hotel Charles Central***";
$lang["Hotel Continent Nyíregyháza"] = "Hotel Continent";
$lang["Hotel Daisy Superior"] = "Hotel Daisy Superior";
$lang["Hotel Divinus***** Debrecen"] = "Hotel Divinus*****";
$lang["Hotel Eger & Park"] = "Hotel Eger & Park";
$lang["Hotel Europa*** Gunaras"] = "Hotel Europa*** Gunaras";
$lang["Hotel Familia Balatonboglár"] = "Hotel Familia";
$lang["Hotel Fortuna Dávod"] = "Hotel Fortuna Dávod";
$lang["Hotel Galerie Royale****"] = "Hotel Galerie Royale****";
$lang["Hotel Gasthof-Sonnenbichl"] = "Hotel Gasthof-Sonnenbichl";
$lang["Hotel Gyöngyvirág Mátrafüred"] = "Hotel Gyöngyvirág";
$lang["Hotel Holiday Csopak"] = "Hotel Holiday";
$lang["Hotel Hollywood Sarajevo"] = "Hotel Hollywood";
$lang["Hotel Jasmin Podcetrtek Szlovénia"] = "Hotel Jasmin";
$lang["Hotel Ködmön**** Eger"] = "Hotel Ködmön****";
$lang["Hotel Kumánia*** Kisújszállás"] = "Hotel Kumánia***";
$lang["Hotel Lajta Park"] = "Lajta Park Hotel";
$lang["Hotel Laroba****"] = "Hotel Laroba****";
$lang["Hotel Lelle és Park Hotel Balatonlelle"] = "Hotel Lelle a Park Hotel";
$lang["Hotel Louis Leger"] = "Hotel Louis Leger";
$lang["Hotel Mária*** Balatonmáriafürdő"] = "Hotel Mária***";
$lang["Hotel Mediterrán Pécs***"] = "Hotel Mediterrán Pécs***";
$lang["Hotel Napfény Balatonlelle"] = "Hotel Napfény";
$lang["Hotel Narád & Park**** Mátraszentimre"] = "Hotel Narád & Park****";
$lang["Hotel Négy Évszak Hajdúszoboszló"] = "Hotel Négy Évszak";
$lang["Hotel Négy Évszak***superior"] = "Hotel Négy Évszak***superior";
$lang["Hotel Nevis Wellness & Spa"] = "Hotel Nevis Wellness & Spa";
$lang["Hotel Olympik**"] = "Hotel Olympik**";
$lang["Hotel Olympik*** Tristar"] = "Hotel Olympik*** Tristar";
$lang["Hotel Opál*** Gyöngyös"] = "Hotel Opál***";
$lang["Hotel Orchidea*** Felsőtengelic"] = "Hotel Orchidea***";
$lang["Hotel Platán Székesfehérvár"] = "Hotel Platán";
$lang["Hotel Reál*** Balatonföldvár"] = "Hotel Reál***";
$lang["Hotel Rubinia Eger"] = "Hotel Rubinia";
$lang["Hotel Silvanus**** Visegrád"] = "Hotel Silvanus****";
$lang["Hotel Sonnhof Rauris*** Ausztria"] = "Hotel Sonnhof Rauris***";
$lang["Hotel Start Krakkó"] = "Hotel Start";
$lang["Hotel Theatrino**"] = "Hotel Theatrino**";
$lang["Hotel U Divadla**"] = "Hotel U Divadla**";
$lang["Hotel Villa Gasparini Dolo"] = "Hotel Villa Gasparini Dolo";
$lang["Hotel Villa Natura*** Zalakaros"] = "Hotel Villa Natura***";
$lang["Hotel Vital Zalakaros"] = "Hotel Vital";
$lang["Hotel Yacht Club***superior"] = "Hotel Yacht Club***superior";
$lang["Hotel-Gasthof Sonnenbichl"] = "Hotel-Gasthof Sonnenbichl";
$lang["Hotel*** Bobbio Budapest"] = "Hotel Bobbio***";
$lang["Hunguest Hotel Mirage Hévíz"] = "Hunguest Hotel Mirage";
$lang["Hunguest Hotel Panoráma*** Superior"] = "Hunguest Hotel Panoráma*** Superior";
$lang["Jázmin Panzió Gyula"] = "Penzión Jázmin";
$lang["Juno Hotel & Camping"] = "Juno Hotel & Camping";
$lang["Kastély Fogadó Gyékényes"] = "Hostinec Kaštieľ Gyékényes";
$lang["Kastély Hotel Sasvár Resort"] = "Zámocký Hotel Sasvár Resort";
$lang["Katschberg Üdülőház"] = "Rekreačny dom Katschberg";
$lang["Kék Duna Wellness Hotel****"] = "Wellness Hotel Kék Duna****";
$lang["Kék Lagúna Apartmanok"] = "Apartmány Kék Lagúna";
$lang["Kék Villa Apartmanház Hévíz"] = "Apatmán Kék Villa";
$lang["Kerekhegy Vendégház"] = "Hostinec Kerekhegy";
$lang["Kikelet Club Hotel"] = "Club Hotel Kikelet";
$lang["Kincsem Wellness Hotel"] = "Wellness Hotel Kincsem";
$lang["King's Hotel Budapest"] = "King's Hotel Budapest";
$lang["Királyrét Hotel Szokolya"] = "Hotel Királyrét Szokolya";
$lang["Komfort Hotel Platán"] = "Komfort Hotel Platán";
$lang["Kőkapu Vadászkastély és Hotel"] = "Kőkapu Poľovnícky Kaštieľ a Hotel";
$lang["Kustur Club***** Üdülőfalu Kusadasi"] = "Rekreačná osada Kustur Club*****";
$lang["Laroba Hotel** Alsóörs"] = "Hotel Laroba** ";
$lang["Lindner Hotel Prague Castle"] = "Hotel Lindner Prague Castle";
$lang["Little Bucharest Old Town Hostel"] = "Little Bucharest Old Town Hostel";
$lang["Magnific Hotel**** Bodrum"] = "Hotel Magnific**** Bodrum";
$lang["Majerik Hotel*** Hévíz"] = "Hotel Majerik***";
$lang["Martfű Termál Spa"] = "Thermal Spa Martfű";
$lang["Mátyus Udvarház Eger"] = "Mátyus Udvarház";
$lang["Melis Panzió és Étterem"] = "Penzión a Reštaurácia Melis";
$lang["Mesés Shiraz Hotel****superior Egerszalók"] = "Hotel Rozprávkový Shiraz****superior";
$lang["MTA Üdülője Visegrád"] = "Vila MTA";
$lang["Nereus Park Hotel Balatonalmádi"] = "Hotel Park Nereus";
$lang["Nomád Baradla Turistaszálló"] = "Hotel Nomád";
$lang["Old Lake Golf Hotel Tata"] = "Old Lake Golf Hotel";
$lang["Oxigén Hotel****& ZEN Spa Noszvaj"] = "Hotel Oxigén****& ZEN Spa";
$lang["Palace Hotel**** Hévíz"] = "Hotel Palace****";
$lang["Palm Wings Beach Resort Hotel***** Kusadasi"] = "Palm Wings Beach Resort Hotel*****";
$lang["Pálma Panzió Kaposvár"] = "Penzión Pálma";
$lang["Panoráma Wellness Apartman Hotel****"] = "Hotel Panoráma Wellness Apartman****";
$lang["Park Hotel Minaret Eger"] = "Hotel Park Minaret";
$lang["Park Hotel Pelikán**** Szombathely"] = "Hotel Park Pelikán****";
$lang["Patak Park Hotel**** Visegrád"] = "Hotel Patak Park****";
$lang["Platán Panzió Dobogókő"] = "Penzión Platán";
$lang["Plaza Alta Hotel"] = "Hotel Plaza Alta";
$lang["Ramada Hotel & Resort Lake Balaton"] = "Ramada Hotel & Resort Lake Balaton";
$lang["Relais Alcova del Doge Mira - Velence"] = "Relais Alcova del Doge Mira";
$lang["Relax Apartman Szálloda"] = "Hotel Relax Apartman";
$lang["Renegade Hotel Siófok"] = "Hotel Renegade";
$lang["Rider Beach Balatonszemes"] = "Rider Beach";
$lang["Rózsa Panzió Siófok"] = "Penzión Rózsa";
$lang["Sárkány Apartman és Kemping"] = "Apartmán a Kemping Sárkány";
$lang["Sárkány Apartman Wellness és Gyógyfürdő Nyírbátor"] = "Apartmán Sárkány Wellness a Liečivé kúpele";
$lang["Soline Seashore Residence"] = "Soline Seashore Residence";
$lang["Start Hotel Krakkó"] = "Hotel Start";
$lang["Stúdió Tatry Holiday"] = "Štúdio Tatry Holiday";
$lang["Stuhleck Café & Pension Spital Am Semmering Ausztria"] = "Stuhleck Café & Pension Spital Am Semmering Austria";
$lang["Szalajka Fogadó Szilvásvárad"] = "Hostinec Szalajka";
$lang["Székelyföldi látogatás - Ízelítő Erdélyből"] = "Návšteva Sikulskej oblasti - Prehľad Sedmohradska";
$lang["Szent Orbán Erdei Wellness Hotel****"] = "Hotel Szent Orbán Erdei Wellness****";
$lang["Szépia Bio & Art Hotel****"] = "Bio & Art Hotel Szépia****";
$lang["Szindbád Wellness Hotel Balatonszemes"] = "Wellness Hotel Szindbád";
$lang["Tatry Holiday Apartmanok*** Nagyszalók Magas-Tátra"] = "Apartmány Tatry Holiday***";
$lang["Tatry Holiday Faházak Nagyszalók Magas-Tátra"] = "Drevenice Tatry Holiday";
$lang["Tatry Holiday Kisházak Nagyszalók Magas-Tátra"] = "Chaty Tatry Holiday";
$lang["Tatry Holiday Kisházak Nagyszalók Magas-Tátra"] = "Chaty Tatry Holiday";
$lang["Termál Hotel Liget*** Érd"] = "Hotel Termál Liget***";
$lang["Thermal Hotel Balmaz***"] = "Thermal Hotel Balmaz***";
$lang["Thermal Hotel Victoria***"] = "Thermal Hotel Victoria***";
$lang["Thermal Park Egerszalók"] = "Thermal Park Egerszalók /emu";
$lang["Tisza Corner Hotel Szeged"] = "Corner Hotel Tisza";
$lang["Tisza Hotel*** Szeged"] = "Hotel Tisza***";
$lang["Tópart Kemping Szombathely"] = "Kemping Tópart";
$lang["Vándorbab Apartmanok"] = "Apartmány Vándorbab";
$lang["Vár Hotel Wellness és Kastélyszálló Visegrád"] = "Hotel Wellness a Kaštieľ Vár";
$lang["Várszálló Apartman Gyula"] = "Apartmán Várszálló";
$lang["Viktor Bor- és Vendégház"] = "Vinársky dom a hostinec Viktor";
$lang["Villa City Center Hévíz"] = "Vila City Center Hévíz";
$lang["Villa Dora Apartmanok Kraljevica"] = "Apartmány Villa Dora";
$lang["Walden Hotel Dobogókő"] = "Hotel Walden";
$lang["Wellness Hotel Venus Zalakaros"] = "Wellness Hotel Venus";


$offers = $mysql->query("SELECT * FROM offers WHERE offers.rival_start_date <= NOW() AND offers.rival_end_date > NOW() AND active = 1  AND affiliate_lira = 0  GROUP BY partner_id ORDER BY id ASC");
//
/*
	$offers = $mysql->fetch_assoc("SELECT * FROM offers WHERE offers.".$lang['#PREFIX#']."start_date <= NOW() AND offers.".$lang['#PREFIX#']."end_date > NOW() AND active = 1  $extrafilter AND affiliate_lira = 0  GROUP BY partner_id ORDER BY RAND() LIMIT $limit");
	
*/
//
echo '<?xml version="1.0" encoding="utf-8"?>';
echo "<SERVER>";

while($arr = mysql_fetch_assoc($offers))
{

			
	if($arr[sub_partner_id] > 0)
		$arr[partner_id] = $arr[sub_partner_id];
		
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[partner_id] LIMIT 1"));
	
	$origpartner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM accomodation WHERE partner_id = $partner[coredb_id] AND package = 0  ORDER BY id ASC LIMIT 1"));
	
	
	
	$data[id] = $arr[id];
	$data[partner_id] = $partner[pid];
	$data[partner_name] = $partner[hotel_name];
	
	
	
	$data[partner_email] = $partner[reception_email];
	$data[partner_phone] = $partner[reception_phone];
	$data[partner_zip] = $partner[zip];
	$data[partner_city] = $partner[city];
	$data[partner_address] = $partner[address];
	$data[partner_description] = $origpartner[description];
	$data[partner_image_count] = $partner[image_num];
	$data[partner_image_base] = "http://static.indulhatunk.hu/uploaded_images/accomodation/$partner[coredb_id]/$partner[coredb_package]/orig/";

	$data[offer_title] = $arr[name];
	$data[offer_subtitle] = $arr[shortname];
	$data[offer_original_price] = $arr[normal_price];
	
//	if($affiliate = 9999)
		$data[offer_price] = $arr[actual_price];
//	else
//		$data[offer_price] = $arr[lajos_price];
	$data[offer_description] = $arr[main_description];
	$data[offer_days] = $arr[days];
	
	$data[offer_child1_name] = $arr[plus_child1_name];
	$data[offer_child2_name] = $arr[plus_child2_name];
	$data[offer_child3_name] = $arr[plus_child3_name];
	
	$data[offer_child1_value] = $arr[plus_child1_value];
	$data[offer_child2_value] = $arr[plus_child2_value];
	$data[offer_child3_value] = $arr[plus_child3_value];
	
	$data[offer_not_include] = $arr[not_include];
	$data[offer_validity] = $arr[offer_validity];

	$data[offer_voucher_expiration] = $arr[expiration_date];
	
	
	$offerlist[] = $data;
	
	$imglist = '';
	for($z=1;$z<=$partner[image_num];$z++)
	{
		if($z < 10)
			$imgname = '0'.$z;
		else
			$imgname = $z;
			
		$imglist.= "<IMAGE>http://static.indulhatunk.hu/uploaded_images/accomodation/$partner[coredb_id]/$partner[coredb_package]/orig/$imgname.jpg</IMAGE>";
				
	}
	
		$hotel = $lang[$partner[hotel_name]];
	
	if($hotel == '')
		$hotel = $partner[hotel_name];
		
		if($partner[country] <> '')
			$cname = $lang["#COUNTRY-".$partner[country]."#"];
		elseif($arr[country] == '')
			$cname = "Maďarsko";		
		else
			$cname = "$pname, $arr[country]";


$newid = $data[id]*2;

	echo "<DEAL>";
	echo " <ID>$newid</ID>";
	echo "<MARGIN>10</MARGIN>";
    echo "<CITIES>";
    echo "<CITY>$cname</CITY>";
    echo "  <CITY>$partner[city]</CITY>";
	echo "</CITIES>";
echo "<TITLE_SHORT> ".str_replace("&","&amp;","$partner[hotel_name]")."</TITLE_SHORT>";
echo "<TITLE> ".str_replace("&","&amp;","$arr[sk_shortname]: $arr[sk_name], $hotel, $partner[city] $cname")."</TITLE>";
echo "<URL>http://www.hoteloutlet.sk/".clean_url2($partner[city])."-ubytovanie/".clean_url2($partner[hotel_name])."-$arr[id]?utm_source=rival.cz&amp;utm_campaign=affiliate&amp;utm_medium=affiliate</URL>";
echo "<IMAGES>$imglist</IMAGES>";
echo "<FINAL_PRICE>".formatPrice($arr[actual_price],1,0,'EUR',0,1)."</FINAL_PRICE>";
echo "<ORIGINAL_PRICE>".formatPrice($arr[normal_price],1,0,'EUR',0,1)."</ORIGINAL_PRICE>";
echo "<CURRENCY>EUR</CURRENCY>";
echo "<CUSTOMERS>23</CUSTOMERS>";
echo "<DEAL_START>$arr[rival_start_date]</DEAL_START>";

	//$end = date('Y-m-d', strtotime("now -30 days") );
	//$end = date('Y-m-d',strtotime("-30 days",strtotime($arr[expiration_date])));
	
echo "<DEAL_END>$arr[rival_end_date]</DEAL_END>";
echo "<CATEGORY>pobyty</CATEGORY>";
echo "<TAGS>";
   /*   echo "<TAG>$cname</TAG>";
      echo "<TAG>$partner[city]</TAG>";
	  echo " <TAG>vikend</TAG>";
      echo "<TAG>relaxace</TAG>";
      echo "<TAG>Dovolenka</TAG>";*/
echo "</TAGS>";
echo "<PRIORITY>".$priority[$arr[partner_id]]."</PRIORITY>";
echo "<ADDRESS>$cname, $partner[zip] $partner[city] $partner[address]</ADDRESS>";
echo "<LANGUAGE>SK</LANGUAGE>";
echo "<ADULT>0</ADULT>";
	/*
		echo "<id>$data[id]</id>";
		echo "<rank>".$priority[$arr[partner_id]]."</rank>";
		echo "<language>hu</language>";
		echo "<url>http://www.szallasoutlet.hu/".clean_url2($partner[city])."-szallas/".clean_url2($partner[hotel_name])."-$arr[id]?utm_source=$affiliate&amp;utm_campaign=affiliate&amp;utm_medium=affiliate</url>";
		echo "<title>$arr[shortname]: ".str_replace("&","&amp;","$partner[hotel_name]").", $partner[city] - $arr[name], ".formatPrice($arr[actual_price])."-ért</title>";
		echo "<description>";
			
			echo "<short>$arr[name]</short>";
			
		if($_GET[ios] == 1)
		{
			echo "<buy><![CDATA[<a href='http://www.szallasoutlet.hu/getoffer/$data[id]/mobile' style='display:block;text-align:center; color:white;text-decoration:none;background-color:#27d71a;font-size:58px;font-family:Helvetica Neue; padding:25px 25px 25px 35px;margin:-8px 0 35px -10px;width:100%'>MEGVESZEM!</a>]]></buy>";
			$separator = 'Az aj&aacute;nlat tartalma';
			$separator2= 'a szállodáról';
			$body = explode("<strong>$separator</strong>",$arr[main_description]);
		
		$body = $body[1];
		

	
		if($body[1] == '')
			{
				$body = explode("<strong>$separator<br /></strong>", $detailArr[$prefix."main_description"]);
				$body = $body[1];

			}
		$body = str_replace("&nbsp;",'',$body);
		$body = preg_replace("/<div style=\"background: url(.*?)<\/div>/im", "", $body);
		$body = preg_replace("/<div style=\"background: url(.*?)<\/div>/im", "", $body);
		

	
	//	$detail->set("description","<table><div>".$body."</table>");
	$body = "<div>".str_replace("<span></span><br />",'',"<span>".$body);
	
	$body = removechars($body);


	$main = explode($separator2,$body);
	$origmain = explode("<div style=",$main[0]);
	
	$main = str_replace("#f5bf3c",'#db3600',$origmain[0]);
	
	$main = str_replace('<img','<img style="width:100% !important"',$main);


	$body = str_replace($origmain[0],'',$body);
	$body = str_replace('694','680',$body);
	


	$body = preg_replace('/(<(?!img)\w+[^>]+)(style="[^"]+")([^>]*)(>)/', '${1}${3}${4}', $body);
	$body = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\>/i', '<$1$4$7>', $body);

	$body = str_replace('<img','<img style="width:100% !important"',$body);

// $body = preg_replace("/<img[^>]+\>/i", "(image) ", $body); 
 
			//echo $body;
			echo "<main><![CDATA[<style>body, html { 
			    font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
    color: #919191;
    font-size:47px;
    text-align: justify; } 
    .iostext img { width:300px !important; padding:20px 0 20px 0;} </style><div class='iostext'>".str_replace("#PRICE#",formatPrice($arr[actual_price]),$main)."</div>]]></main>";
			echo "<hotel><![CDATA[<div class='iostext'><img src='http://admin.indulhatunk.hu/fullhouse/$arr[partner_id].jpg' style='width:100% !important'/>".str_replace("#PRICE#",formatPrice($arr[actual_price]),$body)."</div>]]></hotel>";
			echo "<calendar>http://admin.indulhatunk.hu/fullhouse/$arr[partner_id].jpg</calendar>";

		}
		else
		{
			echo "<full><![CDATA[".$arr[main_description]."]]></full>";
		}
		echo "</description>";
		
		
		if(file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$arr[id]/8.jpg"))
		{
			$firstimage = "<image original='http://admin.indulhatunk.hu/images/offers/$arr[id]/8.jpg' thumbnail='http://admin.indulhatunk.hu/images/offers/$arr[id]/9.jpg'/>";
			
		}
		else
			$firstimage = '';
		
		$start = explode(" ",$arr[start_date]);
		$end = explode(" ",$arr[end_date]);
		$discount = 100-round(($arr[actual_price]/$arr[normal_price])*100);
		echo "<start_date>$start[0]</start_date>";
		echo "<end_date>$end[0]</end_date>";
		echo "<images>";
			echo $irstimage;
			echo "$imglist";
		echo "</images>";
		echo "<original_price>$arr[normal_price]</original_price>";
		echo "<price>$arr[actual_price]</price>";
		echo "<currency name='Ft' iso='HUF' />";
		echo "<discount_percent>$discount</discount_percent>";
		
		//DEV
		if($partner[country] == 'sk')
			$partner[country] = 'hu';
		echo "<accomodation name='".str_replace("&","&amp;","$partner[hotel_name]")."' pid='$partner[pid]' zip='".trim($partner[zip])."' country='".trim($partner[country])."' city='".trim($partner[city])."' address='".trim($partner[address])."' lat='$partner[latitude]' lng='$partner[longitude]' is_wellness='$partner[property_wellness]' is_children='$partner[property_child]' is_mountain='$partner[property_mountain]' is_castle='$partner[property_castle]' is_lake='$partner[property_water]'/>";
		
		
		if($_GET[ios] == 1)
		{
			$rquery = $mysql->query("SELECT * FROM reviews WHERE partner_id = $partner[pid] AND comment <> '' and type='out' ORDER BY id DESC LIMIT 20");
			echo "<reviews>";
				while($r = mysql_fetch_assoc($rquery))
				{	
					$added = explode(" ",$r[added]);
					$added = $added[0];
					
					echo "<review stars='".rand(0,5)."' name='$r[name]' added='".formatDate($added)."'><![CDATA[$r[comment]]]></review>";
					
				}
			echo "</reviews>";
		
		}
		
		*/
	echo "</DEAL>";
	
	
	//}

}
echo "</SERVER>";
?>