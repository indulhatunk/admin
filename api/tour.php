<?
include("../inc/tour.init.inc.php");
header("Content-type: text/xml; charset=utf-8");


echo '<?xml version="1.0" encoding="UTF-8"?>
<travels>';

$mysql_tour->query("SET NAMES 'utf8'");

$query = $mysql_tour->query("SELECT tour.*, offer_property.property_id FROM `offer_property` INNER JOIN tour ON tour.id = offer_property.offer_id WHERE (property_id = 780 or property_id = 1 or property_id = 2) AND aleph_ok = 1 AND image_num > 10  AND disabled = 0  GROUP BY offer_id LIMIT 1500");

while($arr = mysql_fetch_assoc($query)) { 

$arr[name] = str_replace("&","&amp;",$arr[name]);

if($arr[property_id] == 1)
	$property = 'Busz';
elseif($arr[property_id] == 2)
	$property = 'Hajóút';
else
	$property = 'Repülő';
?>
    <travel name="<?=$arr[name]?>">
        <country><?=$arr[country_id]?></country>
        <county><?=$arr[region_id]?></county>
        <city><?=$arr[city_id]?></city>
        <transfer><?=$property?></transfer>
        <accomodation_type>szálloda</accomodation_type>
        <accomodation_name><?=$arr[name]?></accomodation_name>
        <category><?=$arr[category]?></category>
        <description/>
        <hyperlink><?echo "http://www.pihenni.hu/".clean_url3($arr[name])."+$arr[id]" ?></hyperlink>
        <prices>
        
        <? $price = $mysql_tour->query("SELECT * FROM prices WHERE from_date > NOW() AND offer_id = '$arr[id]' ORDER BY from_date, PRICE ASC");
	        while($prc = mysql_fetch_assoc($price)) {         ?>
            <price date="<?=$prc[from_date]?>">
                <caftering><?=$prc[board]?></caftering>
                <days><?=$prc[day_num]?></days>
                <nights><?=$prc[nights]?></nights>
                <persons>1</persons>
                <price><?=$prc[price]?></price>
                <currency>HUF</currency>
                <travelling_type><?=$property?></travelling_type>
                <extra>
                </extra>
            </price>
            
           <? }?>
        </prices>
        <images>
          <? for($i = 1; $i<=$arr[image_num]; $i++) { 
	          
	          if($i < 10)
	          	$z = "0$i";
	          else
	          	$z = $i;
          ?>
            <image>http://static.indulhatunk.hu/uploaded_images/tour/<?=$arr[partner_id]?>/<?=$arr[id]?>/orig/<?=$z?>.jpg</image>
           <? } ?>
        </images>
        <description>
            <![CDATA[<?=$arr[description]?>]]>
        </description>
    </travel>
<?
}
?>

</travels>