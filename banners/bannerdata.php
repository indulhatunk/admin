<? 

if($_GET['file'] == 'banner-portrait')
{
	$size = '127x66';
}
else
{
	$size = '152x86';
}
$affil = (int)end(explode('=',$_GET[args]));

$xmldata = "
<banner>
<!-- 1. ajánlat -->
<ajanlat 
kep_url ='http://static.indulhatunk.hu/main_page/banner/mintakep1_$size.jpg' 
cim ='Visegrád'
alcim ='Hotel Silvanus **** Visegrád '
leiras ='2 éj 2 fő, félpanzióval'
ar ='83 200 Ft'
akcios_ar ='54 990 Ft'
kedvezmeny ='-34%'
link ='http://xn--akcis-szlls-r7ac8v.hu/?affil=$affil'
/>
<ajanlat 
kep_url ='http://static.indulhatunk.hu/main_page/banner/mintakep2_$size.jpg' 
cim ='Prága'
alcim ='Hotel Euro*** Prága'
leiras ='2 éj 2 fő, reggelivel'
ar ='32 900 Ft'
akcios_ar ='24 990 Ft'
kedvezmeny ='-24%'
link ='http://xn--akcis-szlls-r7ac8v.hu/?affil=$affil'
/>
<ajanlat 
kep_url ='http://static.indulhatunk.hu/main_page/banner/mintakep3_$size.jpg' 
cim ='Eger'
alcim ='Hotel Aqua*** Eger'
leiras ='2 éj 2 fő, félpanzióval'
ar ='47 800 Ft'
akcios_ar ='29 990 Ft'
kedvezmeny ='-37%'
link ='http://xn--akcis-szlls-r7ac8v.hu/?affil=$affil'
/>
<ajanlat 
kep_url ='http://static.indulhatunk.hu/main_page/banner/mintakep4_$size.jpg' 
cim ='Pécs'
alcim ='Hotel Mediterrán*** Pécs'
leiras ='2 éj 2 fő, félpanzióval'
ar ='33 200 Ft'
akcios_ar ='18 990 Ft'
kedvezmeny ='-43%'
link ='http://xn--akcis-szlls-r7ac8v.hu/?affil=$affil'
/>
<ajanlat 
kep_url ='http://static.indulhatunk.hu/main_page/banner/mintakep5_$size.jpg' 
cim ='Siófok'
alcim ='Hotel Radio Inn'
leiras ='2 éj 2 fő részére'
ar ='23 600 Ft'
akcios_ar ='14 990 Ft'
kedvezmeny ='-36%'
link ='http://xn--akcis-szlls-r7ac8v.hu/?affil=$affil'
/>
<ajanlat 
kep_url ='http://static.indulhatunk.hu/main_page/banner/mintakep6_$size.jpg' 
cim ='Gunaras'
alcim ='Hotel Europa*** Gunaras'
leiras ='2 éj 2 fő félpanzióval'
ar ='43 800 Ft'
akcios_ar ='25 990 Ft'
kedvezmeny ='-41%'
link ='http://xn--akcis-szlls-r7ac8v.hu/?affil=$affil'
/>
</banner>";

header ('Content-Type:text/xml'); 
header('Cache-Control: no-cache, must-revalidate'); // HTTP/1.1
header('Expires: Sat, 26 Jul 2008 05:00:00 GMT'); // Date in the past

echo $xmldata;
?>
