<?php
/*
 * torokexport.php
 * Author: gergely.solymosi@gmail.com
 *
 */

/* bootstrap file */
$noheader = 'no';
include("inc/init.inc.php");

userlogin();
if($CURUSER[userclass] < 50)
    header("location: index.php");

$header = array(
		  'offer_id' 	    => "Utalvány azonosító",
          'shortname' 	    => "Szálloda",
          'name' 	        => "Név",
          'phone' 	        => "Telefon",
          'email' 	        => "Email",
          'comment'         => "Info",
          'orig_price'      => "Ár",
          'actual_price'    => "Eredeti ár",
          'added' 	        => "Rögzítés időpontja",
       );
$search  = array(',','!','*','.','/','%!%',";","(",")", " ", "&", "á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ö", "Ö", "ő", "Ő", "ú", "Ú", "ü", "Ü", "ű", "Ű");
$replace = array("","","","","","","","","-", "-", "a", "A", "e", "E", "i", "I", "o", "O", "o", "O", "o", "O", "u", "U", "u", "U", "u", "u");

$sql = "SELECT *,customers_tmp.name as cname,customers_tmp.added as cadded FROM customers_tmp LEFT JOIN offers ON customers_tmp.offers_id = offers.id where customers_tmp.added > '2017-04-22 23:59:59' and customers_tmp.name != '';";

$query = $mysql->query($sql);
$result = array();
while($row = mysql_fetch_assoc($query)){
        $result[] = array(
		  'offer_id' 	    => $row['offer_id'],
          'shortname'       => $row['shortname'],
          'name'            => $row['cname'],
          'phone'           => $row['phone'],
          'email'           => $row['email'],
          'comment'         => $row['comment'],
          'orig_price'      => $row['orig_price'],
          'actual_price'    => $row['actual_price'],
          'added' 	        => $row['cadded'],
        );
	
}



$path = realpath('lib/PHPExcel') . '/PHPExcel.php';
include_once($path);
$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);

$doc->getActiveSheet()  ->setTitle("list")
->fromArray($header, null, 'A1')
->fromArray($result, null, 'A2');

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="export-'.date('Ymdhis').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
$objWriter->save('php://output');
exit;

?>