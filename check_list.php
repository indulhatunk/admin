<?
/*
 * checkout_list.php 
 *
 * the checklist page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

require_once('vouchers/lib/config.inc.php');
require_once(HTML2PS_DIR.'pipeline.factory.class.php');
parse_config_file(HTML2PS_DIR.'html2ps.config');


$company = $_GET[company];
if($company == '')
	$company = 'indulhatunk';
	
if($_GET[invoice] == 1 && $_GET[check] > 0)
{

$file = "invoices/cache/$_GET[check].pdf";

if (file_exists($file) == true) {
	if ($fd = fopen ($file, "r")) {
    $fsize = filesize($file);
    $path_parts = pathinfo($file);
    $ext = strtolower($path_parts["extension"]);
     header("Content-type: application/pdf"); // add here more headers for diff. extensions
     header("Content-Disposition: attachment; filename=\"udulesicsekk-".$path_parts["basename"]."\""); // use 'attachment' to force a download
     header("Content-length: $fsize");
   	 header("Cache-control: private"); //use this to open files directly
    while(!feof($fd)) {
        $buffer = fread($fd, 2048);
        echo $buffer;
    }
	}
	fclose ($fd);
	exit;
}
else
{
	$query = $mysql->query("SELECT * FROM checkout_check WHERE checkpaper_id = '$_GET[check]' AND company = '$company' ORDER BY id ASC LIMIT 30");
	
	while($arr = mysql_fetch_assoc($query))
	{
		$customerdata = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE offer_id = '$arr[voucher_id]' AND company_invoice = '$company' LIMIT 1"));
		$invoicenumber.= "invoices/vatera/".str_replace("/","_",$customerdata[user_invoice_number]).".pdf ";
	}
	
	$command = "pdftk $invoicenumber cat output invoices/cache/$_GET[check].pdf";
	
	
	//echo $command;
	//die;
	passthru("$command");
	
	
	if ($fd = fopen ($file, "r")) {
    $fsize = filesize($file);
    $path_parts = pathinfo($file);
    $ext = strtolower($path_parts["extension"]);
     header("Content-type: application/pdf"); // add here more headers for diff. extensions
     header("Content-Disposition: attachment; filename=\"udulesicsekk-".$path_parts["basename"]."\""); // use 'attachment' to force a download
     header("Content-length: $fsize");
   	 header("Cache-control: private"); //use this to open files directly
    while(!feof($fd)) {
        $buffer = fread($fd, 2048);
        echo $buffer;
    }
	}
	fclose ($fd);
	exit;
	

}
}
if($_GET[generate] == 1)
{
global $g_config;
$g_config = array(
                  'cssmedia'     => 'screen',
                  'renderimages' => true,
                  'renderforms'  => false,
                  'renderlinks'  => false,
                  'mode'         => 'html',
                  'debugbox'     => false,
                  'draw_page_border' => false
                  );

$media = Media::predefined('A4');
$media->set_landscape(false);
$media->set_margins(array('left'   => 0,
                          'right'  => 0,
                          'top'    => 0,
                          'bottom' => 0));
$media->set_pixels(870);

global $g_px_scale;
$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;

global $g_pt_scale;
$g_pt_scale = $g_px_scale * 1.43; 

$pipeline = PipelineFactory::create_default_pipeline("","");
$pipeline->configure($g_config);

$pipeline->process(getProtocol() ."admin.indulhatunk.hu/check_list.php?check=$_GET[check]&print=1&company=$_GET[company]&username=$CURUSER[username]", $media); 

}
if($_GET["print"] == 1)
{

if($_GET[check] > 0)
{
	$max = $_GET[check];
	$checkid = $max;
}
else
{
$max = mysql_fetch_assoc($mysql->query("SELECT max(checkpaper_id) as maximum FROM checkout_check WHERE company = '$company'"));
$max = $max[maximum]+1;
}

?>
<style>
	body { margin:0; padding:0; font-size:14px; font-family:Arial; }
	#container { margin:35px 0 0 35px;width:800px;height:1161px; }
	.cleaner { clear:both; }
	.grey { background-color:#c4c4c4; font-weight:bold;}
table, td
{
   	border:1px solid black;
    font-size:12px;
	font-family:arial;
}
table{
	margin:10px 0 0 10px;
	width:780px;
    border-width: 0 0 1px 1px;
    border-top:none;
    border-right:none;
    border-spacing: 0;
    border-collapse: collapse;
}
td
{
    margin: 0;
    padding: 4px;
    border-width: 1px 1px 0 0;
}
.header {
	background-color:#c4c4c4;
	text-align:center;
	font-weight:bold;
}
.footer,.topblock { font-weight:bold; margin:10px;}
.footer { margin:30px 0 0 10px;}
.leftfooter { float:left;}
.thirdtitle { margin:10px 0 10px 0; text-decoration:underline; font-size:16px;}
.rightfooter { float:right; }
.date { margin:10px 0 10px 0;}
.note { font-weight:bold;}
.toptitle { margin:30px 0 0 0;}
.toptitle,.secondtitle {text-align:center; font-weight:bold; font-size:18px;}
.secondtitle { margin:0 0 30px 0;}
.code { font-size:18px;}
.companydata { margin:0 0 20px 0;}
</style>
<body>
<div id="container">

<div class='topblock'>
	<div class='note'>1. sz. segédlet</div>
	<div class='toptitle'>Üdülési Csekk összesítő</div>
	<div class='secondtitle'>Kötelező formátum</div>
	
	<div class='block'>
		<? if($company =='indulhatunk') { ?>
		<div class='leftfooter'>Elfogadóhely kódja: <span class='code'>800157661</span></div>
		<? } else { ?>
		<div class='leftfooter'>Elfogadóhely kódja: <span class='code'>800165904</span></div>
		<? 
			$prefix = 'HO-';
		} ?>
		<div class='rightfooter'>Összesítő sorszáma: <span class='code'><?=$prefix?><?=str_pad($max, 4, "0", STR_PAD_LEFT)?></span></div>
	<div class='cleaner'></div>
	</div>

	<div class='thirdtitle'>Elfogadóhely adatai:</div>
	<div class='companydata'>
	<? if($company =='indulhatunk') { ?>
	Neve: Indulhatunk.hu Kft<br/>
	<? } else { ?>
	Neve: Hotel Outlet Kft<br/>
	<? } ?>
	Címe: 1056 Budapest, Váci utca 9.<br/>
	Telefon: +36-70-930-78-11
	</div>
</div>

<?
	echo "<table>";
	
	echo "<tr class='header'>";
			echo "<td>Sorsz.</td>";
			echo "<td>Számla száma</td>";
			echo "<td>Dátum</td>";
			echo "<td>Számla értéke</td>";
			echo "<td>SZJ szám</td>";
			echo "<td>Üdülés<br/> csekk (db)</td>";
			echo "<td>Csekk értéke</td>";
	echo "</tr>";

		
	$query = $mysql->query("SELECT * FROM checkout_check WHERE checkpaper_id = '$checkid' AND added > '2011-04-01 00:00:00' AND company = '$company' ORDER BY id ASC LIMIT 30");
	$i=1;
	while($arr = mysql_fetch_assoc($query))
	{
		$customerdata = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE offer_id = '$arr[voucher_id]' and company_invoice = '$company' LIMIT 1"));
		
		echo "<tr>";
			echo "<td align='center'>$i/".str_replace("SZO-",'',trim(str_replace("VTL-","",$customerdata[offer_id])))."</td>";
			echo "<td align='center'>$customerdata[user_invoice_number]</td>";
			echo "<td align='center'>$customerdata[user_invoice_date]</td>";
			echo "<td align='right'>".formatPrice($customerdata[orig_price])."</td>";
			echo "<td align='center'>63.30</td>";
			echo "<td align='right'>$customerdata[check_count]</td>";
			echo "<td align='right'>".formatPrice($customerdata[check_value])."</td>";
		echo "</tr>";
		$checkcount = $checkcount + $customerdata[check_count];
		$checkvalue = $checkvalue + $customerdata[check_value];
		$i++;
		
		$data[checkpaper_id] = $max;
		$data[checkpaper_date] = 'NOW()';
		$mysql->query_update("checkout_check",$data,"id=$arr[id]");
		//$mysql->query("UPDATE checkout_check SET checkpaper_id = '$max', checkpaper_date = 'NOW()' WHERE id = $arr[id]");
	}
		writelog("$_GET[username] printed CHECKOUT_LIST: $company / $max");

	echo "<tr>";
			echo "<td align='right' colspan='5'><b>Összesen</b></td>";
			echo "<td align='right'>$checkcount</td>";
			echo "<td align='right'>".formatPrice($checkvalue)."</td>";
		echo "</tr>";
		
	echo "</table>";
?>
<div class='footer'>
<div class='date'>Dátum: <? echo date("Y.m.d.")?></div>
<div class='block'>
	<div class='leftfooter'>Aláírás:</div>
	<div class='rightfooter'>Készítsen másolatot a kész összesítőről</div>
	<div class='cleaner'></div>
</div>
</div>
</div>


</body>
<?
	die;
}
if($_GET[delete] > 0 && $_GET[sure] <> 1)
{
	echo "Biztosan törölni szeretné a tételt? <a href='?delete=$_GET[delete]&sure=1&offer_id=$_GET[offer_id]'>Igen</a> - <a href='#'>Nem</a>";
	die;
}
if($_GET[delete] > 0 && $_GET[sure] == 1)
{
	userlogin();
	$mysql->query("DELETE FROM checkout_check WHERE id = $_GET[delete]");
	writelog("$CURUSER[username] deleted $_GET[offer_id] from checkout_check");
	$msg = "Sikeresen törölte a tételt!";
}
if($_GET[showitems] == 1 && $_GET[check] > 0)
{
	userlogin();
	head();
	echo "<h1><a href='check_list.php'>Üdülésicsekk lista</a> &raquo; ".str_pad($_GET[check], 4, "0", STR_PAD_LEFT).". sz. üdülésicsekk lista tételei:</h1>";	
	
	echo "<table style='margin:0 auto;'>";


	echo "<tr class=\"header\">";
		echo "<td width='0' colspan='2' width='30'>-</td>";
		echo "<td width='0'>Nr.</td>";
		echo "<td width='0'>Tétel neve</td>";
	echo "</tr>";
	
	$query = $mysql->query("SELECT * FROM checkout_check WHERE checkpaper_id = $_GET[check] and company = '$company' ORDER BY id ASC");
	$z = 1;
	while($arr = mysql_fetch_assoc($query))
	{	
		if($arr[cleared] == 1)
		{
			$class= 'green';
			$deletelink = '-';
		}
		else
		{
			$class = '';
			$deletelink = "<a href=\"?delete=$arr[id]&offer_id=$arr[voucher_id]\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/trash.png' alt='töröl' title='töröl' width='20'/></b></a>";

		}
		echo "<tr class=\"$class\">";
			
		if($arr[company] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		else
			$logo = 'ologo_small.png';
			
			echo "<td width='20'><img src='/images/$logo'/></td>";
			
			echo "<td width='20'>$deletelink</td>";
			echo "<td width='0'>".str_pad($_GET[check], 4, "0", STR_PAD_LEFT)." / $z.</td>";
			echo "<td width='0'>$arr[comment]</td>";
		echo "</tr>";
		
		$z++;
	}
	echo "</table>";





	foot();
	die;
}
userlogin();



head("Üdülésicsekk alapítvány lista");

if($_POST[checkpaper_id] > 0)
{


	
	$qr = $mysql->query("SELECT * FROM checkout_check WHERE checkpaper_id = $_POST[checkpaper_id] AND company = '$_POST[company]'");
	
	
	
	//echo "SELECT * FROM checkout_check WHERE checkpaper_id = $_POST[checkpaper_id] AND company = '$_POST[company]'";
	

	while($arr = mysql_fetch_assoc($qr))
	{
	

		$item = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE offer_id = '$arr[voucher_id]' AND company_invoice = '$_POST[company]'"));
		
		
		if($item[cid] > 0)
		{
			//echo "$item[cid] / $item[offer_id] <hr/>";
			$data[checkpaper_id] = $_POST[checkpaper_id];
			$data[check_arrival] = 'NOW()';
			$mysql->query_update("customers",$data,"cid=$item[cid]");
		}
		$mysql->query("UPDATE checkout_check SET cleared = 1 WHERE checkpaper_id = $_POST[checkpaper_id] AND company = '$_POST[company]'");
		writelog("$CURUSER[username] cleared CHECKOUT_INVOICE: $_POST[checkpaper_id] / $_POST[company]");



	}
/*
	$mysql->query("UPDATE checkout_check SET cleared = 1 WHERE checkpaper_id = $_POST[checkpaper_id]");
	writelog("$CURUSER[username] cleared CHECKOUT_INVOICE: $_POST[checkpaper_id]");
*/
}


echo "<h1>Üdülésicsekk alapítvány listák</h1>";
  
 $qr = mysql_fetch_assoc($mysql->query("SELECT count(*) as count FROM checkout_check WHERE checkout_check.checkpaper_id = '' AND added > '2011-04-01 00:00:00' AND company = 'indulhatunk' ORDER BY id ASC"));
 
 $count = ceil($qr[count]/30);
 
$qr2 = mysql_fetch_assoc($mysql->query("SELECT count(*) as count FROM checkout_check WHERE checkout_check.checkpaper_id = '' AND added > '2011-04-01 00:00:00' AND company = 'hoteloutlet' ORDER BY id ASC")); //month(added) >= 4 and year(added) >= 2011 AND
$count2 = ceil($qr2[count]/30);
 echo message($msg);

if($qr[count] > 30) {
 $style = 'display:none;';
 
} ?>

<div style='margin:0 auto;width:330px; padding:0 0 10px 0;'>
<a class='button red' href="?generate=1&company=indulhatunk"> <img src='/images/print.png' width='30' style='float:left;'/> <div>Indulhatunk.hu<br/>(<?=$count?> oldal / <?=$qr[count]?> db)</div></a>
<a class='button red' href="?generate=1&company=hoteloutlet"> <img src='/images/print.png' width='30' style='float:left;'/> <div>Hotel Outlet<br/>(<?=$count2?> oldal / <?=$qr2[count]?> db)</div></a>
<div class='cleaner'></div>

</div>

<div style='margin:0 auto;width:330px; padding:0 0 10px 0;'>
<a class='button red' href="/imap/billing.php?company=indulhatunk"> <img src='/images/print.png' width='30' style='float:left;'/> <div>Indulhatunk.hu<br/>számlázás</div></a>
<a class='button red' href="/imap/billing.php"> <img src='/images/print.png' width='30' style='float:left;'/> <div>Hotel Outlet<br/>számlázás</div></a>

<a class='button red' href="/imap/billing.php?company=szallasoutlet"> <img src='/images/print.png' width='30' style='float:left;'/> <div>SzállásOutlet<br/>számlázás</div></a>

<div class='cleaner'></div>

</div>


<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?cleared=0" class="<? if($_GET[cleared] == 0 || $_GET[cleared] == '') echo "current";?>">Kiegyenlítetlenek</a></li>
		<li><a href="?cleared=1"  class="<? if($_GET[cleared] == 1) echo "current";?>">Kiegyenlítettek</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>

<?// } ?>

<?


$companies = array('hoteloutlet' => "Hotel Outlet Kft.",'indulhatunk' => "Indulhatunk.hu Kft.");


foreach($companies as $cmp => $cname)
{

if($_GET[cleared] == 1)
	$clr = "AND cleared = 1";
else
	$clr = "AND cleared = 0";
$query = $mysql->query("SELECT * FROM checkout_check WHERE checkpaper_id > 0 $clr AND company = '$cmp' GROUP BY company,checkpaper_id ORDER BY checkpaper_id DESC");


echo "<h3>$cname</h3>";

echo "<table style='margin:0 auto;'>";


echo "<tr class=\"header\">";
	echo "<td width='0'>-</td>";
	echo "<td width='0'>Dátum</td>";
	echo "<td width='0'>Sorszám</td>";
	echo "<td width='0' align='center'>Érték</td>";
	echo "<td width='0' align='center'>-</td>";
	echo "<td width='0' align='center'>-</td>";
	echo "<td width='0' align='center'>-</td>";
	echo "<td width='0' align='center'>Kiegyenlítve</td>";
echo "</tr>";



while($dat = mysql_fetch_assoc($query))
{	

	if($dat[cleared] == 1)
	{
		$selected = 'selected';
		$class= 'green';
		$disabled = 'disabled';
	}
	else
	{
		$selected = '';
		$class = '';
		$disabled = '';
	} 
	
	$total = mysql_fetch_assoc($mysql->query("SELECT sum(check_value) as sum FROM checkout_check INNER JOIN customers ON customers.offer_id = checkout_check.voucher_id WHERE checkout_check.checkpaper_id = $dat[checkpaper_id] AND company = '$cmp'"));

		if($cmp == 'indulhatunk')
			$logo = 'ilogo_small.png';
		else
			$logo = 'ologo_small.png';
			
		$logorow = "<td align='center'><img src='/images/$logo'/></td>";
		
		
	$year = explode("-",$dat[checkpaper_date]);
	$year = $year[0];
	
	if($year < 2012)
		$yield = 0.925;
	else
		$yield = 0.9238;
		
	echo "<form method='post'><input type='hidden' name='checkpaper_id' value='$dat[checkpaper_id]'>
		<input type='hidden' name='company' value='$cmp'/>
		<tr class='$class'>
		$logorow
		<td width='140' align='center'>$dat[checkpaper_date]</td>
		<td align='center'>".str_pad($dat[checkpaper_id], 4, "0", STR_PAD_LEFT)."</td>
		<td align='center'>".formatPrice($total[sum]*$yield)."</td>
		<td align='center'><a href='?generate=1&check=$dat[checkpaper_id]&company=$dat[company]'>Nyomtat</a></td>
		<td align='center'><a href='?invoice=1&check=$dat[checkpaper_id]&company=$dat[company]'>Számlák</a></td>
		<td align='center'><a href='?showitems=1&check=$dat[checkpaper_id]&company=$dat[company]'>Tételek</a></td>
		<td align='center'>
			
		<select name='cleared' onchange='submit()' $disabled>
			<option value='0'>Nem</option>
			<option value='1' $selected>Igen</option>
		</select>
		</td>
	</tr></form>";
	
	
}


echo "</table>";

}
echo "</div></div>";
foot();
?>