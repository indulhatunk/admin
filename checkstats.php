<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

head("Ücs Statisztika");

?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
			<li><a href='#' class='current'>Ücs statisztika</a></li>			
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<?
$query = $mysql->query("SELECT pid, count(cid) as cnt, sum(orig_price) as total, sum(check_value) as cvalue FROM customers WHERE payment = 5 AND paid = 1 AND inactive = 0 GROUP BY pid ORDER BY cnt DESC");

echo "<table>";
echo "<tr class='header'>
			<td>Partner</td>
			<td>Eladott</td>
			<td>Fizetett Ft</td>
			<td>Ebből ÜCS</td>
			<td>Átlagár</td>
		</tr>";
		
while($arr = mysql_fetch_assoc($query))
{	
	$partner = mysql_fetch_assoc($mysql->query("SELECT hotel_name FROM partners WHERE pid = $arr[pid] LIMIT 1"));
	
	echo "<tr>
			<td>$partner[hotel_name]</td>
			<td align='right'>$arr[cnt] db</td>
			<td align='right'>".formatPrice($arr[total])."</td>
			<td align='right'>".formatPrice($arr[cvalue])."</td>
			<td align='right'>".formatPrice($arr[total]/$arr[cnt])."</td>
		</tr>";

	$totalcnt = $totalcnt + $arr[cnt];
	$totalprice = $totalprice + $arr[total];
	$totalvalue = $totalvalue + $arr[cvalue];
	
}

echo "<tr class='header'>
			<td>Összesen</td>
			<td align='right'>$totalcnt db</td>
			<td align='right'>".formatPrice($totalprice)."</td>
			<td align='right'>".formatPrice($totalvalue)."</td>
			<td align='right'>".formatPrice($totalprice/$totalcnt)."</td>
		</tr>";


echo "</table></div></div>";
foot();
//foot();
?>