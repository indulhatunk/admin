<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

userlogin();


if($CURUSER[userclass] < 40)
	header("location:index.php");
	

head('Szerződések');
mysql_query("SET NAMES 'utf8'"); 

if($_POST[body] <> '' && $_POST[editid] == '')
{
	
	$mysql->query_insert("contract_templates",$_POST);
	
	message("A szerződésminta sikeresen letöltve");
}

if($_GET[edit] > 0)
	$editarr = mysql_fetch_assoc($mysql->query("SELECT * FROM contract_templates WHERE id = $_GET[edit] LIMIT 1"));

if($_POST[editid] > 0)
{
	$id = $_POST[editid];
	unset($_POST[editid]);
	$mysql->query_update('contract_templates',$_POST,"id=$id");
}
?>

<div class='content-box'>
<div class='content-box-header'>
			<ul class="content-box-tabs">
				<li><a href="partners.php">Partnerek</a></li>
				<li><a href="?add=0" class='<?if($_GET[add] <> 1) echo "current"?>'>Szerződés template-ek</a></li>
				<li><a href="?add=1" class='<?if($_GET[add] == 1) echo "current"?>'>Új template készítése</a></li>

			</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<? if($_GET[add] == 1) { ?>
<div class="partnerForm">
	<form method="POST" action="contract_templates.php">
	
	<? if($editarr[id] > 0) {
		echo "<input type='hidden' name='editid' value='$editarr[id]'/>";
	}
	?>
	<fieldset>
		<legend>Szerződések kezelése</legend>
	<ul>
		<? if($editarr[id] > 0) { ?>
		<? } ?>
		
		
		<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,source,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,|,bullist,numlist,|,outdent,indent|,undo,redo,|,link,unlink,anchor,image,cleanup,|,forecolor,backcolor,fullscreen,preview",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|media,advhr,||,ltr,rtl,",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,
			entity_encoding : "raw",
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>

<table>
	<tr>
		<td class='lablerow'>Template neve:</td>
		<td><input type="text" name="title" value="<?=$editarr[title]; ?>"/></td>
	</tr>
	<tr>
		<td class='lablerow'>Típus:</td>
		<td><select name="type">
				<option value=''>Válasszon</option>
				<option value='facebook' <?if($editarr[type] == 'facebook') echo 'selected'?> >facebook</option>
				<option value='mostutazz'  <?if($editarr[type] == 'mostutazz') echo 'selected'?>>Mostutazz szerződés</option>
				<option value='lmb'  <?if($editarr[type] == 'lmb') echo 'selected'?>>Last Minute Belföld</option>
				<option value='gyermekbarát'  <?if($editarr[type] == 'gyermekbarát') echo 'selected'?>>gyermekbarát</option>
				<option value='indulhatunk'  <?if($editarr[type] == 'indulhatunk') echo 'selected'?>>indulhatunk</option>
				<option value='barterhotel'  <?if($editarr[type] == 'barterhotel') echo 'selected'?>>barterhotel</option>
				<option value='nászutak'  <?if($editarr[type] == 'nászutak') echo 'selected'?>>nászutak</option>
				<option value='outlet'  <?if($editarr[type] == 'outlet') echo 'selected'?>>outlet</option>
				<option value='facebook-szállás'  <?if($editarr[type] == 'facebook-szállás') echo 'selected'?>>FB Szállás</option>

				<option value='rajongok'  <?if($editarr[type] == 'rajongok') echo 'selected'?>>Rajongók</option>

				<option value='pihenni.hu'  <?if($editarr[type] == 'pihenni.hu') echo 'selected'?>>pihenni.hu</option>
				<option value='szilveszter' <?if($editarr[type] == 'szilveszter') echo 'selected'?>>szilveszter</option>
				<option value='üdülési csekk' <?if($editarr[type] == 'üdülési csekk') echo 'selected'?>>üdülési csekk</option>
				<option value='wellness ajánlatok' <?if($editarr[type] == 'wellness ajánlatok') echo 'selected'?>>wellness ajánlatok</option>
			
				<option value='WillHol' <?if($editarr[type] == 'WillHol') echo 'selected'?>>WillHol</option>
				<option value='SunDay' <?if($editarr[type] == 'SunDay') echo 'selected'?>>Sunday</option>
				<option value='Cancelled' <?if($editarr[type] == 'Cancelled') echo 'selected'?>>Megszűnt</option>
					<option value='travel' <?if($editarr[type] == 'travel') echo 'selected'?>>Utazási iroda</option>
				<option value='viszonteladó' <?if($editarr[type] == 'viszonteladó') echo 'selected'?>>Viszonteladó</option>
				<option value='szepkartya' <?if($editarr[type] == 'szepkartya') echo 'selected'?>>Szépkártya felhasználás.hu</option>
			
			
			</select>
</td>
	</tr>
	<tr>
		<td class='lablerow'>Cég:</td>
		<td><select name="company">
					<option value='hoteloutlet' <?if($editarr[company] == 'hoteloutlet') echo 'selected'?>>Hotel Outlet Kft</option>
					<option value='szallasoutlet' <?if($editarr[company] == 'szallasoutlet') echo 'selected'?>>SzállásOutlet Kft</option>
					<option value='indulhatunk' <?if($editarr[company] == 'indulhatunk') echo 'selected'?>>Indulhatunk.hu Kft</option>
			</select></td>
	</tr>
	<tr>
		<td class='lablerow'>Lábléc aláírás:</td>
		<td><select name="footer">
					<option value='1' <?if($editarr[footer] == '1') echo 'selected'?>>Igen</option>
					<option value='0' <?if($editarr[footer] == '0') echo 'selected'?>>Nem</option>
			</select></td>
	</tr>
	<tr>
		<td class='lablerow'>Tartalom:</td>
		<td><textarea name="body" class='tinymce' style='width:670px;height:1000px;'><?=$editarr[body]?></textarea></td>
	</tr>
	<tr>
		<td class='lablerow' colspan='2' align='center'><input type="submit" value="Mentés" /></td>
	</tr>

</table>

	</fieldset>
	</form>
</div>
<? } else {  
 
	echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td>-</td>";
	echo "<td>Cég</td>";
	echo "<td>Típus</td>";
	echo "<td>Név</td>";
	echo "<td>Minta</td>";

echo "</tr>";

$query = $mysql->query("SELECT * FROM contract_templates ORDER by  company, title ASC");
while($arr = mysql_fetch_assoc($query)) {

echo "<tr>";

	echo "<td width='20'><a href='?edit=$arr[id]&add=1'><img src='/images/edit.png' width='20'/></a></td>";
	
	if($arr[company] == 'szallasoutlet')
		echo "<td width='20'><img src='/images/szo_logo.png' width='20'/></td>";
	elseif($arr[company] == 'hoteloutlet')
		echo "<td><img src='/images/ologo_small.png' width='20'/></td>";

	echo "<td width='20'>$arr[type]</td>";
	echo "<td>$arr[title]</td>";
	
	echo "<td width='50'><a href='vouchers/print_contract.php?id=$arr[id]' target='_blank'>minta &raquo;</a></td>";
	
	
echo "</tr>";

}

echo "</table>";	

 }
		
	echo "</div></div>";
foot();
?>