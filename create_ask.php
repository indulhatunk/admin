<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */

include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

	

if( $CURUSER[userclass] < 35)
	header("location: index.php");

head("Számlák kezelése");
?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
		<li><a href="?create=0"  class="<? if($_GET[create] == 0 || $_GET[create] == '') echo "current"?>">Minden díjbekérő</a></li>
		<li><a href="?create=1"  class="<? if($_GET[create] == 1) echo "current"?>">Új díjbekérő készítése</a></li>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


	<?
if($_GET[create] == 1)
{
 	
		
	?>
	<form method='POST' action='/invoice/create_invoice_ask.php' id='invoicesubmit'>
	<fieldset id='invoiceform' style='width:900px;'>
		<legend>Díjbekérő készítése</legend>
	<ul>
		<li><label><b>Cég:</b></label><select name='company'>

		<? echo getCompany($editarr[company], 'select', 0); ?>
		<!--	<option value='hoteloutlet' <? if($query[company] == "hoteloutlet") echo 'selected'; ?>>Hotel Outlet Kft.</option>
			<option value='szallasoutlet' <? if($query[company] == "szallasoutlet") echo 'selected'; ?>>SzállásOutlet Kft.</option>
			<option value='indulhatunk'  <? if($query[company] == "indulhatunk") echo 'selected'; ?>>Indulhatunk média Kft.</option>
			<option value='indusz'  <? if($query[company] == "indusz") echo 'selected'; ?>>Indulhatunk utazásszervező Kft.</option>
			<option value='horizonttravel'  <? if($query[company] == "horizonttravel") echo 'selected'; ?>>Horizonttravel Kft</option>
			<option value='mkmedia'  <? if($query[company] == "mkmedia") echo 'selected'; ?>>Mindenkupon kft.</option> -->


		</select></li>
	
		<li><label><b>Partner:</b></label>
			<select id="invoice_partner">
				<option value='0'>Cég neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND  (country = '' OR country = 'hu') AND trim(company_name) <> '' order by company_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[invoice_name] <> '')
			{
				$arr[company_name] = $arr[invoice_name];
				$arr[zip] = $arr[invoice_zip];
				$arr[city] = $arr[invoice_city];
				$arr[address] = $arr[invoice_address];
				
			}
			echo "<option value='$arr[pid]' zip='$arr[zip]' city='$arr[city]' address='$arr[address]' email='$arr[email]' company_name='$arr[company_name]|$arr[hotel_name]' tax_no='$arr[tax_no]' $selected>$arr[company_name] / $arr[hotel_name]</option>";
		}
	?>
		</select>
		</li>

		<li><label>Vevő neve:</label><input type="text" name="nev" id='invoice_name' value="<?=$partner[vevo][nev]?>"/></li>
		<li><label>Cím:</label><input type="text" name="irsz" id='invoice_zip' value="<?=$partner[vevo][cim][irsz]?>" style='width:30px;' class='numeric'/><input type="text" name="varos" id='invoice_city' style='width:140px;margin:0 1px 0 1px' value="<?=$partner[vevo][cim][varos]?>" /><input type="text" name="cim" class="small" style='width:320px;' id='invoice_address' value="<?=$partner[vevo][cim][utca]?>" /></li>
		<li><label>E-mail cím:</label><input type="text" name="email" id='invoice_email'  value="<?=$partner[vevo][email]?>"/></li>
		<li><label>Adószám:</label><input type="text" name="adoszam" id='invoice_tax' value="<?=$partner[vevo][adoszam]?>"/></li>
		<li><label>Megjegyzés:</label><input type="text" name="megjegy" value="<?=$partner[vevo][megjegy]?>"/></li>
		<li><label>Lábléc:</label><input type="text" name="lablec" value="<?=$partner[info][lablec_info]?>"/></li>
		<li><label>Fizetés módja:</label>
			<select name='fiz_mod'>
					<option value=''>Kérem válasszon</option>
				   <option value="1" <? if($partner[fiz_mod] == 1) echo 'selected'; ?>>Készpénz</option>
                   <option value="2"  <? if($partner[fiz_mod] == 2) echo 'selected'; ?>>Átutalás</option>
					<option value="3" <? if($partner[fiz_mod] == 3) echo 'selected'; ?>>Bankkártya</option>
                    <option value="5" <? if($partner[fiz_mod] == 4) echo 'selected'; ?>>Utánvét</option>
			</select>
		</li>
		<li><label>Kelt:</label><input type="text" name="kelt" class='maskeddate' style='width:90px' value="<?=date("Y-m-d")?>"/></li>
		<li><label>Teljesítés:</label><input type="text" name="telj" class='maskeddate' style='width:90px' value="<?=date("Y-m-d")?>"/></li>
		<li><label>Fizetési határidő:</label><input type="text" name="fiz_hat" class='maskeddate' style='width:90px' value="<?=date('Y-m-d', mktime( 0, 0, 0, /* hó */ substr(date('Y-m-d'), 5, 2), /* nap */ substr(date('Y-m-d'), 8, 2), /* év */ substr(date('Y-m-d'), 0, 4)) +11*24*60*60)?>"/></li>

		<div class='cleaner'></div>
			<hr/>
		<li>
			<table>
				<tr class='header'>
					<td colspan='2'>Megnevezés</td>
					<td>Besorolás</td>
					<td>N. egységár</td>
					<td>B. egységár</td>
					<td>Menny.</td>
					<td>M. egys.</td>
					<td>ÁFA</td>
				</tr>
				
				
				<?
				$i = 1;
				
				if(empty($items))
					$items = array('','','','','');
					
				
				foreach($items as $item)
				{
				//debug($item);
				?>
				<tr>
					<td align='center'><?=$i?>.</td>
					<td><input type='text'  name='megnev[]' style='width:250px;' class='invoiceitem' id="invoiceitem-<?=$i?>" value='<?=$item[megnev]?>'/></td>
					<td>
					<select name='besorolas_tip[]'>
						<option value=''>-</option>
						<option value='VTSZ' <? if($item[besorolas_tip] == "VTSZ") echo 'selected'; ?>>VTSZ</option>
						<option value='SZJ' <? if($item[besorolas_tip] == "SZJ") echo 'selected'; ?>>SZJ</option>
					</select>
					<input type='text'  name='besorolas[]' style='width:38px;' value='<?=$item[besorolas]?>'/></td>
					<td><input type='text' name='netto_egysegar[]' style='width:50px;' class='net numeric' id="net-<?=$i?>"  value='<?
					if($item[brutto_egysegar] > 0)
						echo round($item[brutto_egysegar]/(1+$item[afa_kulcs]/100));
					elseif($item[netto_egysegar] > 0)
						echo round($item[netto_egysegar]);
					?>'/> Ft</td>
					<td><input type='text' name='brutto_egysegar[]' style='width:50px;' class='gross numeric' id="gross-<?=$i?>"  value='<? 
						if($item[netto_egysegar] > 0 )
							 echo round($item[netto_egysegar]*(1+$item[afa_kulcs]/100));
						elseif($item[brutto_egysegar] > 0)
							echo round($item[brutto_egysegar]);?>'/> Ft</td>
					<td>
						<input type='text' name='mennyiseg[]' style='width:50px;' class='' id="quantity-<?=$i?>"  value='<?=$item[mennyiseg]?>'>	
					</td>
					<td><input type='text' name='mennyisegi_egy[]' style='width:30px;' id="quantity2-<?=$i?>"   value='<?=$item[mennyisegi_egy] ?>'/></td>
					<td>
					<select name='afa_kulcs[]' id='tax-<?=$i?>' class='tax'>
						<option value='18' <? if($item[afa_kulcs] == 18) echo 'selected'; ?>>18%</option>
						<option value='25' <? if($item[afa_kulcs] == 25) echo 'selected'; ?>>25%</option>
						<option value='27' <? if($item[afa_kulcs] == 27) echo 'selected'; ?>>27%</option>
						<option value='AHK' <? if($item[afa_kulcs] == 'AHK') echo 'selected'; ?>>0%</option>
					</select></td>
				</tr>
				<? 
				$i++;
				} ?>
			</table>
		</li>
	<li><input type='button' id='create_invoice' value='Díjbekérő elkészítése &raquo;'/></li>

	</ul>
	</fieldset>
	</form>
	
	<? foot();
		die;
	}?>
<div class='cleaner'></div>
<hr/>
<fieldset style="border:1px solid black;-moz-opacity:0.8;opacity:0.80;width:215px;padding:5px;position:fixed;top:10px;right:10px;background-color:white;">
<form method="get" id="sform" >
<input type="text" name="search"  style='height:20px;width:150px;margin:1px 5px 0 0;padding:0;float:left;' /><a href="#"; style='display:block; background:none; color:black; border:1px solid black; height:10px;padding:5px; margin:1px 0 0 0;float:left;width:30px;float:left;' id='submit'>Ok</a>
</form>
</fieldset>

<?
	$dh = opendir('invoices/dijbekero');
		while (($file = readdir($dh)) !== false) {
		
		if($file <> '.' && $file <> '..')
        	$files[] = $file;
	}
	closedir($dh);
	
	sort($files);
	$files = array_reverse($files,true);
		foreach($files as $f)
			echo "<a href='invoices/dijbekero/$f' target='_blank'>$f</a><hr/>";



echo "</div></div>";

foot();
?>