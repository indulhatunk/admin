<?
include("inc/tour.init.inc.php");

class fngccvalidator{

	/**
	 * Validate credit card number and return card type.
	 * Optionally you can validate if it is a specific type.
	 *
	 * @param string $ccnumber
	 * @param string $cardtype
	 * @param string $allowTest
	 * @return mixed
	 */
	public function CreditCard($ccnumber, $cardtype = '', $allowTest = false){
		// Check for test cc number
		if($allowTest == false && $ccnumber == '4111111111111111'){
			return false;
		}

		$ccnumber = preg_replace('/[^0-9]/','',$ccnumber); // Strip non-numeric characters

		$creditcard = array(
			'visa'			=>	"/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/",
			'mastercard'	=>	"/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/",
			'discover'		=>	"/^6011-?\d{4}-?\d{4}-?\d{4}$/",
			'amex'			=>	"/^3[4,7]\d{13}$/",
			'diners'		=>	"/^3[0,6,8]\d{12}$/",
			'bankcard'		=>	"/^5610-?\d{4}-?\d{4}-?\d{4}$/",
			'jcb'			=>	"/^[3088|3096|3112|3158|3337|3528]\d{12}$/",
			'enroute'		=>	"/^[2014|2149]\d{11}$/",
			'switch'		=>	"/^[4903|4911|4936|5641|6333|6759|6334|6767]\d{12}$/"
		);

		if(empty($cardtype)){
			$match=false;
			foreach($creditcard as $cardtype=>$pattern){
				if(preg_match($pattern,$ccnumber)==1){
					$match=true;
					break;
				}
			}
			if(!$match){
				return false;
			}
		}elseif(@preg_match($creditcard[strtolower(trim($cardtype))],$ccnumber)==0){
			return false;
		}		

		$return['valid']	=	$this->LuhnCheck($ccnumber);
		$return['ccnum']	=	$ccnumber;
		$return['type']		=	$cardtype;
		return $return;
	}

	/**
	 * Do a modulus 10 (Luhn algorithm) check
	 *
	 * @param string $ccnum
	 * @return boolean
	 */
	public function LuhnCheck($ccnum){
		$checksum = 0;
		for ($i=(2-(strlen($ccnum) % 2)); $i<=strlen($ccnum); $i+=2){
			$checksum += (int)($ccnum{$i-1});
		}

		// Analyze odd digits in even length strings or even digits in odd length strings.
		for ($i=(strlen($ccnum)% 2) + 1; $i<strlen($ccnum); $i+=2){
			$digit = (int)($ccnum{$i-1}) * 2;
			if ($digit < 10){
				$checksum += $digit;
			}else{
				$checksum += ($digit-9);
			}
		}

		if(($checksum % 10) == 0){
			return true; 
		}else{
			return false;
		}
	}

}

/* Example usage: */


// Validate a credit card

$fngccvalidator = new fngccvalidator();

//print_r($fngccvalidator->CreditCard('4539365944838357'));




$visaPrefixList[] =  "4539";


/*
'prefix' is the start of the CC number as a string, any number of digits.
'length' is the length of the CC number to generate. Typically 13 or 16
*/
function completed_number($prefix, $length) {

    $ccnumber = $prefix;

    # generate digits

    while ( strlen($ccnumber) < ($length - 1) ) {
        $ccnumber .= rand(0,9);
    }

    # Calculate sum

    $sum = 0;
    $pos = 0;

    $reversedCCnumber = strrev( $ccnumber );

    while ( $pos < $length - 1 ) {

        $odd = $reversedCCnumber[ $pos ] * 2;
        if ( $odd > 9 ) {
            $odd -= 9;
        }

        $sum += $odd;

        if ( $pos != ($length - 2) ) {

            $sum += $reversedCCnumber[ $pos +1 ];
        }
        $pos += 2;
    }

    # Calculate check digit

    $checkdigit = (( floor($sum/10) + 1) * 10 - $sum) % 10;
    $ccnumber .= $checkdigit;

    return $ccnumber;
}

function credit_card_number($prefixList, $length, $howMany) {

    for ($i = 0; $i < $howMany; $i++) {

        $ccnumber = $prefixList[ array_rand($prefixList) ];
        $ccnumber = completed_number($ccnumber, $length);
        $result[$ccnumber] = 1;
    }

    return $result;
}


function formatcc($cc)
{
	$cc = str_replace(array('-',' '),'',$cc);
	$cc_length = strlen($cc);
	$newCreditCard = substr($cc,-4);
	for($i=$cc_length-5;$i>=0;$i--){
		if((($i+1)-$cc_length)%4 == 0){
			$newCreditCard = '-'.$newCreditCard;
		}
		$newCreditCard = $cc[$i].$newCreditCard;
	}
	return $newCreditCard;
}

#
# Main
#

$visa16 = credit_card_number($visaPrefixList, 16, 3000);

$i = 1;
foreach($visa16 as $card => $value)
{
	$card = formatcc($card);
	
	
	$card = substr($card, 5);


	echo "$i. $card<br/>";

	$data = array();
	
	$data[code] = $card;
	$data[value] = 1;
	$data[expiration_date] = '2015-04-31';
	$data[source] = 'taxfree';
	
	$mysql->query_insert_ignore("tour_vouchers",$data);
	$i++;

}

echo "done";




?>