<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();

if($CURUSER[userclass] > 255)
	header("location: index.php");
	
head('Eladási statisztikák');


?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?type=0" class="<? if($_GET[type] == 0) echo "current";?>">Fizetési statisztika lealkudtuk</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>
<form method='get'>
<select name='company' onchange='submit()' style='width:430px;'>
	<option value='0'>Cég neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(company_name) <> '' order by company_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[company_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[company_name]</option>";
		}
	?>
</select>
<select name='hotel' onchange='submit()'  style='width:430px;'>
	<option value='0'>Hotel neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(hotel_name) <> '' order by hotel_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[hotel_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[hotel_name]</option>";
		}
	?>
</select>
</form>
<hr/>


<?




//brand new stats end 
$paidtypes = array(
	
	'Teljes' => '',
	'Üdülési csekk' => 5,
	'Helyszínen' => 6,
	'Átutalás' => 1,
	'Bankkártya' => 9,
	'Készpénz' => 2,
	'OTP Szép kártya' => 10,
	'K&H Szép kártya' => 11,
	'MKB Szép kártya' => 12,
	//'Bankkártya online' => 13	
	);
	
$soldtypes = array(
	'Összesen' => '',
	'Lealkudtuk' => 6,
	'Grando' => 7,
	'Vatera' => 1,
	'Teszvesz' => 2,
	'Licittravel' => 3,
	'Outlet' => 4,
	'Viszonteladó' => 8,
);

$today = date("Y-m-d");

//$today =  date("Y-m-d",strtotime(" +1 days"));
$week = date("W");
$month = date("n");
$year = date("Y");

$days = array();
$days[] = $today;
$days[] = date("Y-m-d",strtotime(" -1 days"));
$days[] = date("Y-m-d",strtotime(" -7 days"));
$days[] = date("Y-m-d",strtotime(" -49 days"));
$days[] = date("Y-m-d",strtotime(" -364 days"));


$weeks = array();
$weeks[] = $week;
$weeks[] = $week-1;
$weeks[] = $week-4;
$weeks[] =	$week;


$months = array();
$months[] = $month;
$months[] = $month-1;
$months[] = $month;
$months[] = $month+1;
$months[] = $month+2;


$prevmonths = array();
$prevweeks = array();


?>
<table>

<?

if($_GET[company] > 0)
{
	$clr = "AND customers.pid = $_GET[company]";
	$pid = $_GET[company];
}
elseif($_GET[hotel] > 0)
{
	$clr = "AND customers.pid = $_GET[hotel]";
	$pid = $_GET[hotel];
}
else
{
	$clr = '';
	$pid = '';
}
	
	

if($pid > 0)
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT company_name, hotel_name FROM partners WHERE pid = $pid"));
	
	echo "<tr class='header'><td colspan='30'>$partner[company_name], $partner[hotel_name]</td></tr>";
}
else
{
	echo "<tr class='header'><td colspan='30'>Teljes statisztika</td></tr>";
}
	
	
?>
<tr class='header'>
	<td rowspan='2'>-</td>
	<td colspan='2' class='rightborder'>Ma</td>
	<td colspan='2' class='rightborder'>Tegnap</td>
	<td colspan='2' class='rightborder'>1 hete</td>
	<td colspan='2' class='rightborder'>4 hete</td>
	<td colspan='2' class='rightborder'>52 hete</td>
	<td colspan='2' class='rightborder'>Ehéten</td>
	<td colspan='2' class='rightborder'>1 hete</td>
	<td colspan='2' class='rightborder'>4 hete</td>
	<td colspan='2' class='rightborder'>52 hete</td>
	<td colspan='2' class='rightborder'>Ehónap</td>
	<td colspan='2' class='rightborder'>Előző hónap</td>
	<td colspan='2' class='rightborder'>12 hónapja</td>
</tr>

<!-- -->

<tr class='centered bold'>
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	<td>db</td>
	<td class='rightborder'>Ft</td>
	
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>

	<td class='lightgrey'>db</td>
	<td class='lightgrey rightborder'>Ft</td>
	
	
 
</tr>


<!-- -->
<!-- -->
<?

if($_GET[type] == 1)
{
	$basearray = $soldtypes;
	$criteriafield = "type";
	$orderbyfield = "added";
	$einactive = "";
}
else
{
	$basearray = $paidtypes;
	$criteriafield = "payment";
	$orderbyfield = "paid_date";
	$einactive = "AND inactive = 0";

}



	

foreach($basearray as $title => $criteria) { 
	
$prevmonths = array();
$prevweeks = array();	

if($criteria <> '')	
	$extraselect = "AND $criteriafield = $criteria  AND customers.type = 6";
else
	$extraselect = " AND customers.type = 6";
		
	
?>
<tr class='right'>
	<td class='header'><?=$title?></td>

	<? foreach($days as $day) {
	



	$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE $orderbyfield >= '$day 00:00:00' AND $orderbyfield <= '$day 23:59:59' $extraselect $clr $einactive"));
	
	$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE $orderbyfield >= '$day 00:00:00' AND $orderbyfield <= '$day 23:59:59' $extraselect $clr $einactive"));


	 ?>
	
	<td class='lightgrey'><?=$curdate[count]?></td>
	<td class='lightgrey rightborder'><?=formatPrice($totals[count],0,1)?></td>
	
	<? } ?>
	
	<? foreach($weeks as $week) {
	
	
	if(in_array($week,$prevweeks))
		$yearselect = $year-1;
	else
		$yearselect = $year;
		
//	if($criteria <> '')
		$prevweeks[] = $week;
	
	
	$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE week($orderbyfield,3) = '$week' AND year($orderbyfield) = '$yearselect' $extraselect $clr $einactive "));
		
		
	$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE week($orderbyfield,3) = '$week' AND year($orderbyfield) = '$yearselect' $extraselect $clr $einactive"));

	 ?>
		<td><?=$curdate[count]?></td>
		<td class='rightborder'><?=formatPrice($totals[count],0,1)?></td>
	<? } ?>	

	<? foreach($months as $month) {
	
	
	if(in_array($month,$prevmonths) || $month > date("m"))
		$yearselect = $year-1;
	else
		$yearselect = $year;
		
	//if($criteria <> '')
		$prevmonths[] = $month;
	
	
//	echo "SELECT count(cid) AS count FROM customers WHERE month($orderbyfield) = '$month' AND year($orderbyfield) = '$yearselect' $extraselect AND inactive = 0<hr/>";
	$curdate = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS count FROM customers WHERE month($orderbyfield) = '$month' AND year($orderbyfield) = '$yearselect' $extraselect $einactive"));
	
	$totals = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS count FROM customers WHERE month($orderbyfield) = '$month' AND year($orderbyfield) = '$yearselect' $extraselect $einactive"));

	 ?>
		<td class='lightgrey'><?=$curdate[count]?></td>
		<td class='lightgrey rightborder'><?=formatPrice($totals[count],0,1)?></td>
	<? } ?>	

</tr>
<!-- -->
<? } 
?>
</table>


</div>

</div>
<?


foot();
?>