<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */

include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

	

if( $CURUSER[userclass] < 100 && $CURUSER[userclass] <> 51)
	header("location: index.php");

head("Számlák kezelése");
?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
		<li><a href="?create=0"  class="<? if($_GET[create] == 0 || $_GET[create] == '') echo "current"?>">Minden számla</a></li>
		<li><a href="?create=1"  class="<? if($_GET[create] == 1) echo "current"?>">Új számla készítése</a></li>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<div class='cleaner'></div>

<?


$search = $_GET[search];

//required because of php
class TSzlaTetelek { } 

if($search <> '')
{
	$query = $mysql->query("SELECT * FROM log_invoice WHERE request LIKE '%$search%' or invoice_number like '%$search%'ORDER BY added DESC LIMIT 100");
}
else
{
	$query = $mysql->query("SELECT * FROM log_invoice WHERE year(added) = 2012 AND company = 'indulhatunk' ORDER BY added DESC LIMIT 400");
}

echo "<table>";
echo "<tr class='header'>";
	echo "<td>-</td>";
	echo "<td>Hét</td>";

	echo "<td>Számlaszám</td>";
	echo "<td>Hivatkozas</td>";
	echo "<td>Dátum</td>";
	echo "<td>Voucher</td>";
	echo "<td>Partner</td>";
	echo "<td>Bruttó összesen</td>";
	echo "<td>Letöltés</td>";
	echo "<td>Műveletek</td>";
echo "</tr>";

while($arr=mysql_fetch_assoc($query))
{
	$total = 0;
	
				$invoice = unserialize($arr[request]);


	if($invoice[params][biz_tip] == 'ESS')
	{
		$class = 'red';
		$ref_no = $invoice[data][szla_xml][fej][hivatkozas];
	}
	else
	{
		$class = '';
		$ref_no = '';
	}
	
	echo "<tr class='$class'>";	
	
	
		if($arr[company] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		else
			$logo = 'ologo_small.png';
			
		echo "<td><img src='/images/$logo'/></td>";



		$week = date("W",strtotime($invoice[data][szla_xml][fej][kelt]))-1;
		if($week == 0)
			$week = 52;
		
		
		echo "<td>$week. hét</td>";
		echo "<td>$arr[invoice_number]</td>";
		echo "<td>$ref_no</td>";
		
		
		echo "<td>$arr[added]</td>";
		
	
		foreach( $invoice[data][szla_xml][tetelek]->tetel as $item)
		{
			//print_r($item);
			
			if($item[brutto_egysegar] <> '')
			{
				$total = $total + $item[brutto_egysegar];
			}
			else
			{
				$total = $total + $item[netto_egysegar]*((100+$item[afa_kulcs])/100);
			}
		}
		
		//debug($invoice);
		
		//die;
		
		if($total == 0)
		{
			$vid = explode(" ",$invoice[data][szla_xml][tetelek]->tetel[0][megnev]);
			$voucher_id = $vid[6];
		}
		else
		{
			$voucher_id = end(explode(" ",$invoice[data][szla_xml][tetelek]->tetel[0][megnev]));
		}
		if(strlen($voucher_id) <> 10)
			$voucher_id = '-';
		echo "<td align='center'>$voucher_id</td>";
		echo "<td>".$invoice[data][szla_xml][fej][vevo][nev]."</td>";
		echo "<td align='right'>".formatPrice($total)."</td>";
		
		
		if(strpos($arr[invoice_number], 'LMB-') !== false)
			$folder = 'lmb';
		else
			$folder = 'vatera';
			
		echo "<td align='right'><a href='http://admin.indulhatunk.info/invoices/$folder/".str_replace("/","_",$arr[invoice_number]).".pdf' target='_blank'>Letöltés</a></td>";
		echo "<td align='center'><a href='/invoice/invoice_storno.php?storno=$arr[invoice_number]'>[sztornó]</a><br/><a href='?invoice_number=$arr[invoice_number]&create=1'>[másolat]</a></td>";
	echo "</tr>";
}
echo "</table></div></div>";

foot();
?>