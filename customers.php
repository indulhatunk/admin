<?
/*
 * customers.php 
 *
 * customers page
*/
error_reporting(E_ERROR | E_PARSE);

/* bootstrap file */
include("inc/tour.init.inc.php");
userlogin();

//update hotel's own comment
if($_POST[hotel_comment] <> '' && $_POST[cid] > 0)
{
	if(validString($_POST[hotel_comment]) == true && validString($_POST[cid]) == true)
	{
	$msg = "A megjegyzést elmentettük!";
	
		if($CURUSER[userclass] < 10)
			$limit = "AND pid = $CURUSER[pid]";
			
		$mysql->query("UPDATE customers SET hotel_comment = '$_POST[hotel_comment]' WHERE cid = '$_POST[cid]' $limit");
		
		writelog("$CURUSER[username] added hotel comment for $_POST[cid]");
	}
}

if($CURUSER[userclass] < 4 && $_GET[type] == '' && $CURUSER[is_qr] == 1 && empty($_POST))
{
	header("location: /customers.php?type=grey");
	die;
}
if($CURUSER[userclass] < 4 && $_GET[type] == '' && $CURUSER[is_qr] == 0 && empty($_POST))
{
	header("location: /customers.php?type=green");
	die;
}
//search 
$s = trim($_POST[search]);
if($s <> '')
	setcookie ("search", $s, time() + 3600);

$cookie = $_COOKIE[search];

//set limits depending on the userclass
if($CURUSER[userclass] == 5)
	$extraSelect = "AND reseller_id = '$CURUSER[pid]'";
elseif($CURUSER[userclass] < 80)
	$extraSelect = "AND (pid = '$CURUSER[pid]' or sub_pid = '$CURUSER[pid]')";
else
	$extraSelect = '';


if($CURUSER[reseller_office] > 0)
	$extraSelect = "$extraSelect AND reseller_office = '$CURUSER[reseller_office]'";


$month 	= (int)$_GET[month];
$year 	= (int)$_GET[year];
$filter = $_GET[type];

if($month == "") {
	$month = date("n");
}
if($year == "") {
	$year = date("Y");
}


$add = $_GET[add];
$editid = (int)$_GET[editid];
$editid = (int)$_GET[editid];
$delete = (int)$_GET[delete];
$sure 	= $_GET[sure];

$editIDPost = (int)$_POST[editid];


if($_POST[discount_code] <> '')
{
	$promocode = strtoupper($_POST[discount_code]);
	
	$checkpromo = mysql_fetch_assoc($mysql->query("SELECT * FROM promotion WHERE code = '$promocode' AND start_date <= NOW() AND end_date >= NOW() AND active = 1"));
	
	if($checkpromo[id] > 0)
	{
	
		if($checkpromo[wellness_only] == 1)
		{
		
			
			$o = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = '$_POST[offers_id]' LIMIT 1 "));
			if($o[sub_partner_id] > 0)
				$o[partner_id] = $o[sub_partner_id];
				
			$p = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$o[partner_id]' LIMIT 1 "));
			
			if($p[property_wellness] == 0)
				die("Érvénytelen promóciós kód!");
	
		}
	
		$data[discount_percent] = trim($checkpromo[discount_percent]);
		$promodiscount = $checkpromo[discount_percent];
	}
	else
	{
		die("Érvénytelen promóciós kód!");
		
	}
}

$data[discount_code] = trim($_POST[discount_code]);
$data[offer_id] = trim($_POST[offer_id]);
$data[offers_id] = trim($_POST[offers_id]);
$data[name] = trim($_POST[name]);
$data[phone] = trim($_POST[phone]);
$data[address] = trim($_POST[address]);
$data[zip] = trim($_POST[zip]);
$data[city] = trim($_POST[city]);
$data[email] = trim($_POST[email]);
$data[bid_id] = trim($_POST[bid_id]);
$data[invoice_name] = trim($_POST[invoice_name]);
$data[invoice_address] = trim($_POST[invoice_address]);
$data[invoice_zip] = trim($_POST[invoice_zip]);
$data[invoice_city] = trim($_POST[invoice_city]);
$data[tax_no] = trim($_POST[tax_no]);
$data[sub_pid] = (int)$_POST[sub_pid];
$data[bid_name] = trim($_POST[bid_name]);
$data[validity] = trim($_POST[validity]);
$data[not_include] = trim($_POST[not_include]);
$data[payment] = trim($_POST[payment]);
$data[comment] = trim($_POST[comment]);
$data[hotel_comment] = trim($_POST[hotel_comment]);



$data[orig_price] = trim($_POST[orig_price]);
$data[type] = trim($_POST[type]);
$data[post_fee] = trim($_POST[post_fee]);
$data[check_count] = trim($_POST[check_count]);
$data[check_value] = trim($_POST[check_value]);
$data[email_id] = trim($_POST[email_id]);
$data[promotion_code] = trim($_POST[promotion_code]);
$data[gift] = trim($_POST[gift]);
$data[reseller_id] = trim($_POST[reseller_id]);

if($CURUSER[pid] == 2999)
{
	$data[reseller_office] = trim($_POST[reseller_office]);
	$data[reseller_agent] = trim($_POST[reseller_agent]);

}

if($_POST[delreason] == 1 && $_POST[cid] > 0)
{
		
	$mysql->query("UPDATE customers SET inactive_visible = '$_POST[inactive_visible]', comment = CONCAT(comment,'\n\n Törlés indoka: $_POST[delcomment]') WHERE cid = $_POST[cid]");
	
	
	$pid = mysql_fetch_assoc($mysql->query("SELECT name,offer_id, pid, paid, invoice_created FROM customers WHERE cid = $_POST[cid]"));
	$ml = mysql_fetch_assoc($mysql->query("SELECT email FROM partners WHERE pid = $pid[pid]"));
	
	if($pid[paid] == 1)
	{
//echo $ml[email]."<hr/>";
		
			$message = "


A <b>$pid[offer_id] ($pid[name])</b> utalványszámú megrendelés rendszerünkben törlésre került.<br/><br/>
Amennyiben a törlés kapcsán kérdése merülne fel, kérjük, szíveskedjen azt írásban jelezni felénk 24 órán belül a <b>vtl@indulhatunk.hu</b> címen. 
<br/><br/>A törölt foglalások kapcsán kizárólag írásbeli visszajelzéseket tudunk elfogadni.";

		sendEmail("Törölt utalvány: $pid[offer_id] $pid[name]",$message,$ml[email],"Partnerünk",'lemondas@indulhatunk.hu',"Szállás Outlet");
		
		
	}
	header("location: customers.php");
}
//delete, copy
if($delete > 0 && $sure <> 1) {
	die("
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>$lang[delete_item]</h2> <br/> <a href=\"?delete=$delete&sure=1\" class='button green'>$lang[yes]!</a>  <a href=\"customers.php\" class='button red closefacebox'>$lang[no]!</a><div class='cleaner'></div>");
}
if($_GET[copy] > 0 && $_GET[sure] <> 1)
{
	die("
	
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan másolni szeretné a <b><font color='red'>$_GET[offer_id]</font></b> sorszámú utalványt?</h1><br/>  <a href=\"?copy=$_GET[copy]&offer_id=$_GET[offer_id]&sure=1\" class='button green'>$lang[yes]</a> <a href=\"customers.php\" class='button red closefacebox'>$lang[no]</a><div class='cleaner'></div>");
}
if($_GET[copy] > 0 && $_GET[sure] == 1)
{
	$copydata = $mysql->query("SELECT * FROM customers WHERE cid = $_GET[copy]");
	$copydata = mysql_fetch_assoc($copydata);
	
	if($copydata[company_invoice] == 'hoteloutlet')
		$companyname = 'hoteloutlet';
	elseif($copydata[company_invoice] == 'szallasoutlet')
		$companyname = 'szallasoutlet';	
	elseif($copydata[company_invoice] == 'professional')
		$companyname = 'professional';	

	elseif($copydata[company_invoice] == 'indusz')
		$companyname = 'indusz';	
	else
		$companyname = '';
	
	
	//reset default items on copy
	$copydata[offer_id] = generateNumber($companyname);
	
	if($copydata[type] == 10 || $copydata[type] == 11)
		$copydata[offer_id] = str_replace("SZO-","CZO-",$copydata[offer_id]);


	$copydata[added] = 'NOW()';
	$copydata[paid] = 0;
	$copydata[cid] = '';
	$copydata[paid_date] = '';
	$copydata[due_date] = '';
	$copydata[invoice_created] = '';
	$copydata[user_invoice_ask] = '';
	$copydata[invoice_number] = '';
	$copydata[invoice_date] = '';
	$copydata[voucher] = '';
	$copydata[inactive] = 0;
	$copydata[checked] = 0;
	$copydata[checked_date] = '';
	$copydata[checked_ip] = '';
	$copydata[checks] = 0;
	$copydata[checkout] = 0;
	$copydata[promotion_code] = '';
	$copydata[post_invoice_created] = '';
	$copydata[post_invoice_number] = '';
	$copydata[notification1] = '';
	$copydata[notification2] = '';
	$copydata[notification3] = '';
	$copydata[notification4] = '';
	$copydata[customer_left] = '';
	$copydata[reviewed] = '';
	$copydata[print_date] = '';
	$copydata[print_id] = '';
	$copydata[creditcard] = '';
	$copydata[check_count] = '';
	$copydata[check_value] = '';
	$copydata[user_invoice_number] = '';
	$copydata[user_invoice_date] = '';
	$copydata[user_invoice_started] = '';
	$copydata[check_arrival] = '';
	$copydata[checkpaper_id] = '';
	$copydata[invoice_cleared] = '';
	$copydata[user_storno_invoice_number] = '';
	$copydata[user_storno_invoice_date] = '';
	
	$mysql->query_insert("customers",$copydata);
	writelog("$CURUSER[username] copied VTL_ID: $_GET[offer_id] to $copydata[offer_id]");

	$copy_id = mysql_insert_id();
	$msg = "Sikeres másolás $copy_id";
	
	//header("location: customers.php?add=1&editid=23158");
}
if($delete > 0 && $sure == 1 && $CURUSER[userclass] >= 5) {
	writelog("$CURUSER[username] deleted VTL_ID: $delete");
	$mysql->query("update customers SET inactive = 1, inactive_date = NOW() where cid = $delete $extraSelect") or die(mysql_error());
	head($lang[manage_customers]);
	echo message(" 
	<form method='POST'>
	Törlés indoka:<br/>
		<input type='hidden' name='cid' value='$delete'/>
		<input type='hidden' name='delreason' value='1'/>
		<input type='text' name='delcomment'/>
		<select name='inactive_visible'>
			<option value='1'>látható a szálloda számára</option>
			<option value='0'>nem látható a szálloda számára</option>
			
		</select><br/>
		<input type='submit' value='Törlés véglegesítése'>
	</form>
	");
	
	$c = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE cid = '$delete' LIMIT 1"));
	
	/*** send message ****/
	
	$message = "

Sajnálattal tapasztaltaltuk, hogy Önök korábban utalványt vásároltak tőlünk, azonban azt nem vették át.<br/><br/>

Az alábbi űrlap kitöltésével kérjük segítse munkánkat annak érdekében, hogy kiszolgálásunkat még jobban az Önök igényeihez igazítsuk!<br/><br/>

Kitöltés: <a href='http://vasarlas.szallasoutlet.hu/customers/delete/$c[hash]/$c[cid]' target='_blank'>http://vasarlas.szallasoutlet.hu/customers/delete/$c[hash]/$c[cid]</a>
<br/><br/>
Az Ön véleménye fontos számunkra!<br/><br/>

<a href='https://www.facebook.com/groups/szallasoutlet/' target='_blank'>Nem ez az ajánlat volt az igazi? Inkább másikat választana? Ha nem szeretne lemaradni a legjobb ajánlatokról, lépjen be Ön is a zártkörű SzállásOutlet Klubba, ahol korlátozott számban elérhető, exkluzív ajánlatokból is válogathat.  »</a><br/><br/>


Segítségét előre is köszönjük!";
	
	$message ="
<p>
Sajnálattal tapasztaltuk, hogy az Ön által lefoglalt utalványt nem vette  igénybe.</p>
<p>
Értesítjük, hogy utalványát a mai napon töröltük.</p>
<p>
Amennyiben a helyszínen rendezte az utalvány ellenértékét, a kapott bizonylatot kérem küldje el részünkre.</p>
<p>  Kérem segítse munkánkat az  alábbi űrlap kitöltésével.</p><p>
  Ezzel hozzájárul szolgáltatásunk színvonalának emeléséhez és kiszolgálásunkat  még jobban az Ön igényeihez igazíthatjuk!</p>
<p>
  Kitöltés: <a href='http://vasarlas.szallasoutlet.hu/customers/delete/$c[hash]/$c[cid]' target='_blank'>http://vasarlas.szallasoutlet.hu/customers/delete/$c[hash]/$c[cid]</a>
  <br/><br/>
  Az Ön véleménye fontos számunkra!<br/><br/>
  <strong><a href='http://www.szallasoutlet.hu'>szallasoutlet.hu</a><br>
E-mail: <a href='mailto:info@szallasoutlet.hu'>info@szallasoutlet.hu</a><br>
Tel.: </strong>+36 1 510 0505<strong><br>
Nyitva tartás: </strong>H-P: 8-18 óráig <strong><br>
</strong>Online ügyfél-szolgálatunk hétvégén is, általában 24 h órán belül, e-mailben  válaszol Önnek.</p>";

	if(substr($c[offer_id],0,3) <> 'CZO' && substr($c[offer_id],0,3) <> 'TVO')
	{
	
		if($c[type] <> 12 && $c[sub_pid] <> 3352)
			sendEmail("Szállás Outlet - véleménye fontos számunkra!",$message,$c[email],$c[name]);
	}	
	/**** ***/
	//<a href='customers.php' class='button grey' style='margin:10px 0 0 115px;'>Vissza!</a><div class='cleaner'></div>
	foot();
	die();
}
if($editid >0) {
	$customerQuery = $mysql->query("SELECT * FROM customers WHERE cid = $editid $extraSelect");
	$editarr = mysql_fetch_assoc($customerQuery);
}
if($editIDPost == "" && $data[name] <> "") {

	$data[added] = 'NOW()';

	$offerQr = $mysql->query("SELECT * FROM offers WHERE id = '$data[offers_id]'");
	$offerArr = mysql_fetch_assoc($offerQr);
	
	if($offerArr[company_invoice] == 'hoteloutlet')
	{
		$data[offer_id] = str_replace("VTL-","SZO-",trim($_POST[offer_id]));
		$data[company_invoice] = $offerArr[company_invoice];
	}
	else
	{
		$data[offer_id] = trim($_POST[offer_id]);
		$data[company_invoice] = $offerArr[company_invoice];
	}
 
	if($_POST[type] == 10 || $_POST[type] == 11)
		$data[offer_id] = str_replace("SZO-","CZO-",trim($_POST[offer_id]));

	
	$data[is_qr] = trim($offerArr[is_qr]);
	
	$data[validity] = trim($offerArr[validity]);
	$data[not_include] = trim($offerArr[not_include]);
	$data[bid_name] = trim($offerArr[name]);

	

		$data[gift_type] = $_POST[gift_type];
		$data[gift_body] = $_POST[gift_body];
		$data[gift_title] = $_POST[gift_title];
		
		
	$data[pid] = trim($offerArr[partner_id]);
	$data[sub_pid] = trim($offerArr[sub_partner_id]);
	
	
	if(trim($_POST[promotion_code]) <> '')
		$data[orig_price] = trim($_POST[orig_price]);
	else
	{
		if($CURUSER[userclass] == 5)
			$data[orig_price] = trim($offerArr[actual_price]);
		else
			$data[orig_price] = trim($offerArr[actual_price]);

	}
	//deduct the promotion value
	if($promodiscount > 0)
	{
		
			$data[discount_percent] = trim($checkpromo[discount_percent]);
			$promodiscount = $checkpromo[discount_percent];
			
			if($checkpromo[base_price] == 1)
			    $orig_field = $offerArr[actual_price];
			else
			    $orig_field = $data[orig_price];


			if($checkpromo[discount_type] == 'percentage')
			    $dc = round($orig_field*($promodiscount/100));
			else
			    $dc = $promodiscount;
			    
				//$dc = round($data[orig_price]*($promodiscount/100));
		$data[discount_value] = $dc;
	}
	else
		$data[discount_value] = '';
	
	$data[hash] = md5($data[name].$data[offer_id].'4fAFVE4f_aranymetszes_994');


		$data[hash] = md5($data[name].$data[offer_id].'4fAFVE4f_aranymetszes_994');
		
		$data[added_by] = $CURUSER[username];
		$insertID = $mysql->query_insert("customers",$data);
		//$insertID = mysql_insert_id();
		
		/****/
		$message = "Köszönjük, hogy nálunk vásárolt!<br/><br/>
<b>Az alábbi linken kérjük, állítsa be megrendelése részleteit!</b><br/>
Kérjük, ügyeljen a kötelező mezők helyes kitöltésére!<br/>
Megrendelése az űrlap helyes kitöltése után lesz végleges.<br/><br/>

<a href=\"http://vasarlas.szallasoutlet.hu/customers/$data[hash]/$insertID\">http://vasarlas.szallasoutlet.hu/customers/$data[hash]/$insertID</a> <br/><br/>
Az utalvány sorszáma: $data[offer_id]

<br/><br/><a href='https://www.facebook.com/groups/szallasoutlet/' target='_blank' style='text-decoration:none'>Legyen tagja a zártkörű SzállásOutlet Klubnak, ahol korlátozott számban foglalható titkos ajánlatainkat érheti el! Különleges akciók, meghosszabbított foglalási időszak, csak itt elérhető ajánlatok. Érdemes csatlakozni! »</a><br/><br/>
";
		
		if($CURUSER[userclass] <> 5 && $data[type] <> 9 && $data[type] <> 10 && $data[type] <> 11)
			sendEmail("Szállás Outlet: Állítsa be megrendelése részleteit!",$message,$data[email],"$data[name]",'vtl@indulhatunk.hu','Szállás Outlet','',1);
		
		if($CURUSER[userclass] == 5)
		{
			$offdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $data[pid] LIMIT 1"));
			$letterBody = "Viszonteladói vásárlás, az adminon keresztül<br/><br/>
			$offdata[hotel_name] <br/><br/>
			
			Az utalvány sorszáma: $data[offer_id]  ";
			sendEmail("Akciós szállás vásárlás -  $CURUSER[hotel_name] / $CURUSER[company_name] - adminon keresztül",$letterBody,"vtl@indulhatunk.hu","$data[name]");
		}
		$msg = $lang[customer_added];
		writelog("$CURUSER[username] inserted VTL item $data[offer_id]");

		
	setcookie("customerForm", serialize($data), time()+3600*24*10);
	
		header("location: customers.php?add=1&editid=$insertID");
		
	}
elseif($editIDPost > 0 && $data[name] <> "") {


	$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $data[offers_id]");
	$offerArr = mysql_fetch_assoc($offerQr);
	
	$item = mysql_fetch_assoc($mysql->query("SELECT orig_price, offer_id, foreign_orig_price, foreign_currency FROM customers WHERE cid = $editIDPost LIMIT 1"));

	$prevorigprice = $item[orig_price];
/* update offer data */
	$data[shipment]  = (int)$_REQUEST[shipment];
	
	$data[plus_half_board]  = (int)$_REQUEST[plus_half_board];
	$data[plus_weekend_plus]  = (int)$_REQUEST[plus_weekend_plus];
	$data[plus_bed]  = (int)$_REQUEST[plus_bed];
	$data[plus_bed_plus_food]  = (int)$_REQUEST[plus_bed_plus_food];
	$data[plus_child1_value]  = (int)$_REQUEST[child1_value];
	
	$data[inactive] = $_POST[inactive];
	
		if($data[plus_child1_value] == 999)
			$data[plus_child1_value] = 0;
		elseif($data[plus_child1_value] == 0)
			$data[plus_child1_value] = 1;
		else
			$data[plus_child1_value] = (int)$_REQUEST[child1_value];
	
	$data[plus_child2_value]  = (int)$_REQUEST[child2_value];
		if($data[plus_child2_value] == 999)
			$data[plus_child2_value] = 0;
		elseif($data[plus_child2_value] == 0)
			$data[plus_child2_value] = 1;
		else
			$data[plus_child2_value] = (int)$_REQUEST[child2_value];
			
			
			
	$data[plus_child3_value]  = (int)$_REQUEST[child3_value];
		if($data[plus_child3_value] == 999)
			$data[plus_child3_value] = 0;
		elseif($data[plus_child3_value] == 0)
			$data[plus_child3_value] = 1;
		else
			$data[plus_child3_value] = (int)$_REQUEST[child3_value];
			
	$data[plus_room1_value]  = (int)$_REQUEST[room1_value];
	$data[plus_room2_value]  = (int)$_REQUEST[room2_value];
	$data[plus_room3_value]  = (int)$_REQUEST[room3_value];
	$data[plus_other1_value]  = (int)$_REQUEST[other1_value];
	$data[plus_other2_value]  = (int)$_REQUEST[other2_value];
	$data[plus_other3_value]  = (int)$_REQUEST[other3_value];
	$data[plus_other4_value]  = (int)$_REQUEST[other4_value];
	$data[plus_other5_value]  = (int)$_REQUEST[other5_value];
	$data[plus_other6_value]  = (int)$_REQUEST[other6_value];
	
	
	$data[plus_single1_value]  = (int)$_REQUEST[plus_single1_value];
	$data[plus_single2_value]  = (int)$_REQUEST[plus_single2_value];
	$data[plus_single3_value]  = (int)$_REQUEST[plus_single3_value];
	
	$data[plus_days]  = (int)$_REQUEST[plus_days];
	
	$data[actual_price]  = (int)$offerArr[actual_price];
	
	
	if(trim($_POST[promotion_code]) <> '')
		$data[actual_price] = trim($_POST[orig_price]);
	else
	{
		if($CURUSER[userclass] == 5 || $data[reseller_id] > 0)
		
			$data[actual_price] = trim($offerArr[actual_price]);
		else
			$data[actual_price] = trim($offerArr[actual_price]);
	}


	$dayNum = (int)$_REQUEST[days];
	
	if($data[plus_days] > 0)
	{
	$data[plus_days] = 
		 	round($data[actual_price]/$dayNum) + 
			 $data[plus_half_board]*1 +
			 $data[plus_bed]*1 +
			 $data[plus_bed_plus_food]*1 +
			 $data[plus_child1_value] *1 +
			 $data[plus_child2_value] *1 +
			 $data[plus_child3_value] *1 +
			 $data[plus_room1_value]*1 +
			 $data[plus_room2_value]*1 +
			 $data[plus_room3_value]*1 +
			 $data[plus_other1_value]*1 +
			 $data[plus_other2_value]*1 +
			 $data[plus_other3_value]*1 +
			 $data[plus_other4_value]*1 +
			 $data[plus_other5_value]*1 +
			 $data[plus_other6_value]*1;
	}
	
	$totalPrice =
			 $data[actual_price] + 
			 $data[plus_half_board]*$dayNum +
			 $data[plus_weekend_plus] *2 +
			 $data[plus_bed]*$dayNum +
			 $data[plus_bed_plus_food]*$dayNum +
			 $data[plus_child1_value] *$dayNum +
			 $data[plus_child2_value] *$dayNum +
			 $data[plus_child3_value] *$dayNum +
			 $data[plus_room1_value]*$dayNum +
			 $data[plus_room2_value]*$dayNum +
			 $data[plus_room3_value]*$dayNum +
			 $data[plus_other1_value]*$dayNum +
			 $data[plus_other2_value]*$dayNum +
			 $data[plus_other3_value]*$dayNum +
			 $data[plus_other4_value]*$dayNum +
			 $data[plus_other5_value]*$dayNum +
			 $data[plus_other6_value]*$dayNum +
		  	 $data[plus_single1_value] +
		  	 $data[plus_single2_value] +
		  	 $data[plus_single3_value] +
		  	 
			 $data[plus_days];

	$data[orig_price] = round($totalPrice,-1);
/*update offer data */

		$data[gift_type] = $_POST[gift_type];
		$data[gift_body] = $_POST[gift_body];
		$data[gift_title] = $_POST[gift_title];
		
		
	if($prevorigprice <> $data[orig_price])
	{
		$data[extra_price] = abs($prevorigprice-$data[orig_price]);
		$data[extra_price_generated] = 1;
		writelog("$CURUSER[username] added extra service VTL_ID: $voucherid[offer_id]");

	}
	
	if($CURUSER[userclass] > 50) {
	//remove
		$data[user_invoice_number] = trim($_POST[user_invoice_number]);
		$data[user_invoice_date] = trim($_POST[user_invoice_date]);
		$data[user_invoice_started] = trim($_POST[user_invoice_started]);
		$data[company_invoice] = $_POST[company_invoice];
		$data[reseller_id] = $_POST[reseller_id];
		$data[customer_left] = $_POST[customer_left];
		$data[customer_booked] = $_POST[customer_booked];
		
		$data[call_comment] = trim($_POST[call_comment]);
		//$data[customer_booked] = trim($_POST[customer_booked]);


	}
	
	
		$data[validity] = trim($offerArr[validity]);
		$data[not_include] = trim($offerArr[not_include]);
		$data[bid_name] = trim($offerArr[name]);


	$data[pid] = trim($offerArr[partner_id]);
	
	
	if($CURUSER[passengers_admin] == 1)
	{
		$data[paid] = $_REQUEST[paid];
		writelog("$CURUSER[username] reset paid = $data[paid] at VTL_ID: $item[offer_id]");
		$data[voucher] = $_REQUEST[voucher];	
	}
	if($CURUSER[userclass] > 50)
	{
		$data[foreign_paid_date] = trim($_POST[foreign_paid_date]);
		$data[foreign_yield] = trim($_POST[foreign_yield]);
		
		$data[foreign_status] = trim($_POST[foreign_status]);
		$data[customer_booked] = trim($_POST[customer_booked]);
		
		
	

		$data[joker] = trim($_POST[joker]);
		
		if($data[joker] > 0)
		{
			$jokerdiscount = $data[actual_price]*$_POST[joker]/100;
			$data[orig_price] = $data[orig_price]  - $jokerdiscount;
		}
		
		
		//print_r($data);
		
	}
	
	//deduct promotion value
	if($promodiscount > 0)
	{
		$data[discount_percent] = trim($checkpromo[discount_percent]);
		$promodiscount = $checkpromo[discount_percent];
		
		
			if($checkpromo[base_price] == 1)
			    $orig_field = $offerArr[actual_price];
			else
			    $orig_field = $data[orig_price];


		if($checkpromo[discount_type] == 'percentage')
		    $dc = round($orig_field*($promodiscount/100));
		else
		    $dc = $promodiscount;

		//$dc = round($data[orig_price]*($promodiscount/100));
		$data[discount_value] = $dc;
	}
	else
		$data[discount_value] = '';


//	$data[orig_price] = trim($offerArr[actual_price]);
	
	if($item[foreign_orig_price] > 0)
	{	
		$data[foreign_orig_price] = formatPrice($data[orig_price]-$data[discount_value],1,0,$item[foreign_currency],0,1);
		//echo $data[foreign_orig_price];
		//die;
	}		
		
	if($CURUSER[username] == 'orsolya' || $CURUSER[username] == 'vargapeter' || $CURUSER[username] == 'iSuperG')
		$data[is_qr] = $_POST[is_qr];
		
	$mysql->query_update("customers",$data,"cid=$editIDPost $extraSelect");
	
	$voucherid = mysql_fetch_assoc($mysql->query("SELECT offer_id FROM customers WHERE cid = $editIDPost"));
	writelog("$CURUSER[username] edited VTL_ID: $voucherid[offer_id]");
	
	header("location: /customers.php?add=1&editid=$editIDPost");
	die;
	$msg = $lang[customer_edited];
}

head($lang[vatera_customers]);
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					<? 
		
	if($CURUSER[userclass] < 90 && $CURUSER[pid] <> '3557')
		$export = "<li><a href=\"/export.php\">$lang[export]</a></li>";
	else
		$export = '';
		
	if($CURUSER[userclass] < 5)
		$bookings = "<li><a href=\"/bookings.php\">$lang[bookings]</a></li>";
	else
		$bookings = '';
		
	if($CURUSER[userclass] == 5)
	{
		if($_GET[add] == 1)	
			$aclass = 'current';
		else
			$aclass = '';	
			
		if($_GET[editid] > 0)
			$addlink = "<li><a href=\"?add=1\" class='$aclass'>Vásárló szerkesztése</a></li>";
		else
			$addlink = "<li><a href=\"?add=1\" class='$aclass'>Új vásárló</a></li>";
	}
		
		
		if(($_GET[type] == 'white' || !isset($_GET[type]) ||  $_GET[type] == '') && $_GET[add] <> 1)
			$class1 = 'current';
		if($_GET[type] == 'green')
			$class2 = 'current';
		if($_GET[type] == 'red')
			$class3 = 'current';
		if($_GET[type] == 'blue')
			$class4 = 'current';
		if($_GET[type] == 'grey')
			$class5 = 'current';

		if($CURUSER[userclass] > 4 || $CURUSER[is_reliable] == 1)
		{
			$menulinks = "<li><a href=\"?year=$year&month=$month&type=white\" class=\"$class1\">$lang[active]</a></li>
		<li><a href=\"?year=$year&month=$month&type=green\"  class=\"$class2\">$lang[paid_printed]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=red\"  class=\"$class3\">$lang[notpaid_printed]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=blue\"  class=\"$class4\">$lang[paid_notprinted]</a></li>
		 <li><a href=\"?year=$year&month=$month&type=grey\"  class=\"$class5\">$lang[billed]</a></li>";
		}
		elseif($CURUSER[is_qr] == 1)
		{
			$menulinks = "<li><a href=\"?year=$year&month=$month&type=grey\"  class=\"$class5\">$lang[billed]</a></li>";
		}
		else
		{
			$menulinks = "<li><a href=\"?year=$year&month=$month&type=green\"  class=\"$class2\">$lang[paid_printed]</a></li><li><a href=\"?year=$year&month=$month&type=grey\"  class=\"$class5\">$lang[billed]</a></li>";
			
		}
		echo "$addlink
		$menulinks
		$export $bookings";
		 
		 if($CURUSER[userclass] > 50)
		 {
		 	echo "<li><a href=\"deleted.php\" class=\"\">Törölt elemek</a></li>";
		 	echo "<li><a href=\"inactive.php\" class=\"\">Inaktív vásárlók</a></li>";
			echo "<li><a href=\"emails.php\" class=\"\">Feldolgozatlan levelek</a></li>";
			echo "<li><a href=\"szep-paid.php\" class=\"\">SZÉP</a></li>";

		 }
					?>
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>



<fieldset class='searchform'>




<form method="post" id="sform" action="customers.php">
<input type="text" name="search" placeholder='Vendég keresése' <? if($cookie <> '') echo "value='$cookie'";?> autofocus/><a href="#" class='searchbutton' id='submit'>Ok</a>
<? if($CURUSER[userclass] > 50) {

if($_POST[showinactive] == 'on')
{
	$showchecked = "checked";
}
 ?>
<br/>
<div style='margin:0px 0 0 0'>
	<label for="showinactive"><input type='checkbox' name='showinactive' id="showinactive" <?=$showchecked?>/> Töröltek között </label>
</div>
<? } ?>
</form>
</fieldset>
<?
//submenu
if($CURUSER["userclass"] > 50)
{	
	if($CURUSER["userclass"] == 255)
	{
		$stat = "<a href=\"?statistics=1\" class=\"show\">$lang[statistics]</a>";
	}
	else 
		$stat = "<a href=\"?type=orange\" class=\"orange\">$lang[postpaid]</a>";
	echo "<div class='subMenu'><a href=\"customers.php?add=1\">$lang[new_customer]</a> <a href=\"vouchers/print.php\" class='grey'>$lang[print_vouchers]</a> <a href=\"vouchers/print_envelope.php\" class='grey'>$lang[print_envelopes]</a> <a href=\"vouchers/print_post.php\" class='grey'>$lang[print_post]</a>  $stat <div class='cleaner'></div></div>";
}

//show statistics
if($CURUSER["userclass"] == 255 && $_GET[statistics] == 1)
{

$dateNow = date("n");
$dateMinus = $dateNow-1;
$yearMinus = date("Y");

if($dateMinus == 0)
{
	$dateMinus = 12;
	$yearMinus = date("Y")-1;
}	
	//echo "<h3>Indulhatunk.hu Kft.</h3>";
	getLongStats();
	getStatistics($dateMinus,$yearMinus);
	getStatistics($dateNow,date("Y"));
	getShortStats();

	foot();
	die;
}

if($msg <> '')
	echo message("$msg");

	$string = generateNumber();
	
if($add == 1 && $CURUSER["userclass"] >= 5) {

	if($editid > 0) 
		$formData = '';
	else
		$formData = unserialize(stripslashes($_COOKIE[customerForm]));
	
?>
<div class="partnerForm">
	<form method="POST" id="cform" action="customers.php?month=<?=$month?>&year=<?=$year?>&type=<?=$type?>">
	
		<?if($editarr[offer_id] =="") {?>
			<input type="hidden" name="offer_id" value="<?=$string?>">
		<?} else {?>
			<input type="hidden" name="editid" value="<?=$editarr[cid]?>">
			<input type="hidden" name="offer_id" value="<?=$editarr[offer_id]?>">
		<?}?>
		
		<? if($CURUSER[userclass] == 5) { ?>
			<input type="hidden" name='reseller_id' value='<?=$CURUSER[pid]?>'/> 
			
			
			<? if($CURUSER[pid] == 2999) { ?>
				<input type="hidden" name='type' value='12'/> 
			<? } else { ?>
				<input type="hidden" name='type' value='8'/> 
			<? } ?>
			<input type="hidden" name='orig_price' value='<?=$editarr[orig_price]?>'/> 
			<input type="hidden" name='payment' value='<? if($editarr[payment] > 0) echo $editarr[payment]; else echo "2";?>'/> 
			<input type="hidden" name='shipment' value='<?=$editarr[shipment]?>'/> 

			<input type="hidden" name='check_count' value='<?=$editarr[check_count]?>'/> 
			<input type="hidden" name='check_value' value='<?=$editarr[check_value]?>'/> 
			<input type="hidden" name='hotel_comment' value='<?=$editarr[hotel_comment]?>'/> 

			<input type="hidden" name='email_id' value='<?=$editarr[email_id]?>'/> 

			
		<? } else { ?>
			<!-- <input type="hidden" name='reseller_id' value='<?=$editarr[reseller_id]?>'/>-->
		<? } ?>
		
		
	<fieldset id='customerForm'>
	<legend>Vásárlók kezelése</legend>
	
	<table width='100%'>
	
		
		<?if($editarr[offer_id] =="") {?>
			<tr>
				<td class='lablerow'>Azonosító:</td>
				<td><?=$string?></td>
			</tr>
		<?}	else {?>
			<tr>
				<td class='lablerow'>Azonosító:</td>
				<td><?=$editarr[offer_id]?></td>
			</tr>
		<?}?>
		
		
		<? if($CURUSER[userclass] > 50) { ?>
			<tr>
				<td class='lablerow'>Cég:</td>
				<td>
					<select name='company_invoice'>
						<? echo getCompany($editarr[company_invoice], 'select'); ?>

						<!-- 
						<option value='hoteloutlet' <? if($editarr[company_invoice] == 'hoteloutlet' || $editarr[company_invoice] == '') echo "selected";?>>Hotel Outlet Kft.</option>
							<option value='szallasoutlet' <? if($editarr[company_invoice] == 'szallasoutlet') echo "selected";?>>Szállás Outlet Kft.</option>
							<option value='indusz' <? if($editarr[company_invoice] == 'indusz') echo "selected";?>>Indulhatunk utazásszervező Kft.</option>

						<option value='indulhatunk' <? if($editarr[company_invoice] == 'indulhatunk') echo "selected";?>>Indulhatunk.hu Kft.</option> -->
					</select>
				</td>
			</tr>
			<? 	if($editarr[cid] <> '' && ($CURUSER[username] == 'orsolya' || $CURUSER[username] == 'vargapeter' || $CURUSER[username] == 'iSuperG'))
				{ 
			
				?>
			<tr>
				<td class='lablerow'>QR?:</td>
				<td>
					<select name='is_qr'>
						<option value='1' <? if($editarr[is_qr] == '1') echo "selected";?>>qr utalvány</option>
						<option value='0' <? if($editarr[is_qr] == '0') echo "selected";?>>nem qr utalvány</option>
					</select>
				</td>
			</tr>
			<? } ?>
		<? } ?>
		
		
		<? if($CURUSER[userclass] > 50) { ?>
		<tr><td colspan='2'>
		<select name="offers_id"  id='offers_id' >
				<?
				
				if($editarr[cid] == '')
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW() AND offers.affiliate_lira = 0  ORDER BY partners.hotel_name ASC");
				}
				else
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id ORDER BY partners.hotel_name ASC");
				}
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
							
						$shortid = substr($partnerArr[outer_name], 0,3);
						
						$shortname = mb_substr($partnerArr[shortname], 0,30,'UTF-8');
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $shortname - $shortid - ".formatPrice($partnerArr[actual_price])."</option>";
					}	
				?>
			</select>
		</td>
			</tr>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] > 0) { 

			$partnerArr = mysql_fetch_assoc($mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.id = $editarr[offers_id] LIMIT 1"));

		?>	
			<tr>
				<td class='lablerow'>Ajánlat:</td>
				<td><? echo "$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name]" ?></td>
			</tr>
			<input type="hidden" name="offers_id" id='offers_id' value="<?=$editarr[offers_id]?>"/>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] == 0) { ?>
		<tr><td colspan='2'>
		
		
		<select name="offers_id" style='width:680px;'  id='offers_id' >
				<?
				
					if($CURUSER[extra_condition] <> '')
						$eselect = $CURUSER[extra_condition];
					else
						$eselect = 'AND offers.start_date <= NOW() AND offers.end_date > NOW() AND (vatera_to < NOW()) AND affiliate_lira <> 1  AND affiliate_lajos <> 1 AND pid <> 3121';
					$partnerQuery = $mysql->query("SELECT partners.property_wellness,offers.reseller_price, offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.id > 0 $eselect ORDER BY partners.hotel_name ASC");
			
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
							
					
						$shortname = mb_substr($partnerArr[shortname],0, 20, "UTF-8");
		
						$shortid = substr($partnerArr[outer_name], 0,3);
						

						if($partnerArr[property_wellness] == 1)
							$extracode = "(✓)";
						else
							$extracode = "";
							
						
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $shortname - $shortid - ".formatPrice($partnerArr[actual_price])." $extracode</option>";
					}	
				?>
			</select>
		</td>
			</tr>
		
		<? } ?>
		<? 	if($editarr[cid] <> '' && $CURUSER[userclass] > 50)
			{ ?>
		<tr><td colspan='2'>
		<select name="sub_pid">
			<option value=''>Válasszon</option>
				<?
				
				$partnerQuery = $mysql->query("SELECT * FROM partners WHERE hotel_name <> '' ORDER BY partners.hotel_name ASC");
				
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[pid] == $editarr[sub_pid] || $partnerArr[pid] == $formData[sub_pid])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name]</option>";
					}
				
				?>
			</select>
		</td>
			</tr>
			
			<tr>
				<td class='lablerow'>Viszonteladó:<br/>
					<span style='font-size:11px'></span>
				</td>
				<td>
				<select name='reseller_id'>
			<option value=''>Válasszon</option>
				<?
				
				$partnerQuery = $mysql->query("SELECT * FROM partners WHERE (userclass = 5  AND hotel_name <> '') or pid = 3 ORDER BY partners.hotel_name ASC");
				
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[pid] == $editarr[reseller_id])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name]</option>";
					}
				
				?>
			</select>
				<!--<input type="text" name="reseller_id" value="<?=$editarr[reseller_id]?>" id=''/></td>-->
			</tr>
			<tr>
				<td class='lablerow'>Távozott:</td>
				<td><input type="text" name="customer_left" value="<?=$editarr[customer_left]?>" id=''/></td>
			</tr>
		
		<? } else { ?>
			<input type="hidden" name="sub_pid" value="<?=$editarr[sub_pid]?>"/>
		<? } ?>
		
		<tr>
				<td class='lablerow'>Vevő neve:</td>
				<td><input type="text" name="name" value="<?=$editarr[name]?>" id='customername'/></td>
			</tr>
		<tr>
				<td class='lablerow'>Cím:</td>
				<td><input type="text" name="zip" value="<?=$editarr[zip]?>" style='width:35px;' class='numeric'/><input type="text" name="city" style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" name="address" class="small" style='width:295px;' value="<?=$editarr[address]?>" /></td>
			</tr>
		
		
		<tr id='invoicebutton' ><td colspan='2' <? if($editarr[invoice_name] <> '') echo "style='display:none'"?>><div style='text-align:center; padding:5px; '><a href='#' id='showinvdata'><b>Eltérő számlázási adatok</b></a></div></td></tr>
		
		<tr class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><td class='lablerow'>Számlázási név:</td>
				<td><input type="text" name="invoice_name" value="<?=$editarr[invoice_name]?>"/></td>
			</tr>
		<tr class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><td class='lablerow'>Adószám:</td>
				<td><input type="text" name="tax_no" value="<?=$editarr[tax_no]?>"/></td>
		</tr>
		<tr class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><td class='lablerow'>Számlázási Cím:</td>
				<td><input type="text" name="invoice_zip"value="<?=$editarr[invoice_zip]?>" style='width:35px;'/><input type="text" style='width:140px;margin:0 1px 0 1px' class="small1" value="<?=$editarr[invoice_city]?>" name="invoice_city"/><input type="text" name="invoice_address" class="small" style='width:295px;' value="<?=$editarr[invoice_address]?>" /></td>
			</tr>
		
		
		<tr>
				<td class='lablerow'>Telefonszám*:</td>
				<td><input type="text" name="phone" value="<?=$editarr[phone]?>" id='customerphone'/></td>
			</tr>
		<tr>
				<td class='lablerow'>E-mail cím*:</td>
				<td><input type="text" name="email" value="<?=$editarr[email]?>" id='customeremail'/></td>
			</tr>

		<? if($CURUSER[userclass] > 50) { ?>
		<tr>
				<td class='lablerow'>Licit:</td>
				<td><input type="text" name="bid_name" value="<?=$editarr[bid_name]?><?=$formData[bid_name]?>"/></td>
			</tr>
		<tr>
				<td class='lablerow'>Licit típus:</td>
				<td>
			<select name="type">
				<option value="4" <?if($editarr[type]==4)echo"selected";?>>Outlet</option>
				<option value="1" <?if($editarr[type]==1)echo"selected";?>>Vatera</option>
				<option value="2" <?if($editarr[type]==2)echo"selected";?>>Teszvesz</option>
				<option value="6" <?if($editarr[type]==6)echo"selected";?>>Lealkudtuk</option>
				<option value="7" <?if($editarr[type]==7)echo"selected";?>>Grando</option>
				<option value="3" <?if($editarr[type]==3)echo"selected";?>>Licittravel</option>
				<option value="5" <?if($editarr[type]==5)echo"selected";?>>Pult</option>
				<option value="8" <?if($editarr[type]==8)echo"selected";?>>Viszonteladó</option>
				<option value="9" <?if($editarr[type]==9)echo"selected";?>>Cseh értékesítés</option>
				<option value="10" <?if($editarr[type]==10)echo"selected";?>>Cazare Outlet</option>
				<option value="11" <?if($editarr[type]==11)echo"selected";?>>Okazii</option>
				<option value="12" <?if($editarr[type]==12)echo"selected";?>>Quaestor</option>

			</select>
		</td>
			</tr>
		<tr>
				<td class='lablerow'>Eladási ár:</td>
				<td><input type="text" name="orig_price" value="<?=$editarr[orig_price]?><?=$formData[orig_price]?>" style='width:50px;' class='numeric'/> Ft
				<? 
					if($editarr[discount_percent] > 50)
						$dcv = ' Ft';
					else
						$dcv = ' %';
					if($editarr[discount_value] > 0) { echo "<b><font color='red'>-".formatPrice($editarr[discount_value])." kedvezmény (-$editarr[discount_percent]$dcv)</font></b>"; } ?>
				</td>
			</tr>
		<!--<tr>
				<td class='lablerow'>Promóciós kód:</td>
				<td>
			<select name="promotion_code">
				<option value=''>Válasszon</option>
				<?
					$partnerQuery = $mysql->query("SELECT * FROM promotion ORDER BY name ASC");
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[code] == $editarr[promotion_code])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[code]\" $selected>$partnerArr[name]</option>";
					}
				
				?>
			</select>
		</td>
			</tr>-->
		<tr>
				<td class='lablerow'>Fizetés módja:</td>
				<td>
			<select name="payment">
				<option value="2" <?if($editarr[payment]==2)echo"selected";?>>Készpénz</option>
				<option value="1" <?if($editarr[payment]==1)echo"selected";?>>Átutalás</option>
				<option value="3" <?if($editarr[payment]==3)echo"selected";?>>Utánvét</option>
				<option value="9" <?if($editarr[payment]==9)echo"selected";?>>Bankkártya</option>
				<option value="4" <?if($editarr[payment]==4)echo"selected";?>>Futár</option>
				<option value="5" <?if($editarr[payment]==5)echo"selected";?>>Üdülési csekk</option>
				<option value="6" <?if($editarr[payment]==6)echo"selected";?>>Helyszinen</option>
				<option value="7" <?if($editarr[payment]==7)echo"selected";?>>Online</option>
				<option value="8" <?if($editarr[payment]==8)echo"selected";?>>Facebook</option>
				<option value="10" <?if($editarr[payment]==10)echo"selected";?>>OTP SZÉP kártya</option>
				<option value="11" <?if($editarr[payment]==11)echo"selected";?>>MKB SZÉP kártya</option>
				<option value="12" <?if($editarr[payment]==12)echo"selected";?>>K&H SZÉP kártya</option>


			</select>
		</td>
			</tr>
		<tr>
				<td class='lablerow'>Szállítás módja:</td>
				<td>
			<select name="shipment">
				<option value="0">Válassza ki az átvételi módot</option>
				<option value="2" <?if($editarr[shipment]==2)echo"selected";?>>Online kérem</option>
				<option value="3" <?if($editarr[shipment]==3)echo"selected";?>>Személyes átvétel</option>
				<option value="4" <?if($editarr[shipment]==4)echo"selected";?>>Ajánlott levélként: +395 Ft</option>
				<option value="5" <?if($editarr[shipment]==5)echo"selected";?>>Postai utánvéttel: +1 450 Ft</option>
			</select>

		</td>
			</tr>
		<tr>
				<td class='lablerow'>Postaköltség:</td>
				<td><input type="text" name="post_fee" value="<?=$editarr[post_fee]?><?=$formData[post_fee]?>" style='width:50px;' class='numeric'/> Ft</td>
			</tr>
		
		<!--<tr>
				<td class='lablerow'>Üdülésicsekk info:</td>
				<td><textarea name="checks"><?=$editarr[checks]?></textarea></td>
			</tr>-->
		<tr>
				<td class='lablerow'>Üdülésicsekkek értéke:</td>
				<td><input type="text" name="check_value" value="<?=$editarr[check_value]?>" style='width:50px' class='numeric'/> Ft</td>
			</tr>
		<tr>
				<td class='lablerow'>Üdülésicsekkek száma:</td>
				<td><input type="text" name="check_count" value="<?=$editarr[check_count]?>"  style='width:50px' class='numeric'/> darab</td>
			</tr>
			<tr>
				<td class='lablerow'>Külföldi státusz</td>
				<td>
					<select name='foreign_status'>
						<option value='0'>válasszon</option>
						<option value='1' <? if($editarr[foreign_status]== 1)echo"selected";?>>van hely</option>
						<option value='2' <? if($editarr[foreign_status]== 2)echo"selected";?>>lekérve</option>
						<option value='3' <? if($editarr[foreign_status]== 3)echo"selected";?>>visszaigazolva</option>
						<option value='4' <? if($editarr[foreign_status]== 4)echo"selected";?>>értesítve</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Érkezés dátuma:</td>
				<td><input type="text" name="customer_booked" value="<?=$editarr[customer_booked]?>" class="maskeddate" style='width:90px'/></td>
			</tr>
			<tr>
				<td class='lablerow'>Külföldi fizetés dátuma:</td>
				<td><input type="text" name="foreign_paid_date" value="<?=$editarr[foreign_paid_date]?>" class="maskeddate" style='width:90px'/></td>
			</tr>
			
			<tr>
				<td class='lablerow'>Külföldi elszámolás:</td>
				<td><?=$editarr[foreign_invoice_number]?></td>
			</tr>
			<tr>
				<td class='lablerow'>Külföldi jutalék<br/>vig alapján:</td>
				<td><input type="text" name="foreign_yield" value="<?=$editarr[foreign_yield]?>" class="numeric" style='width:90px'/> Ft</td>
			</tr>
			
<? } ?>
		<tr>
				<td class='lablerow'>Publikus megjegyzés:</td>
				<td><textarea name="comment"><?=$editarr[comment]?></textarea></td>
			</tr>
		
		
		<? if($editarr[offer_id] == '' && $CURUSER[pid] == 2999) { ?>
			<tr>
				<td class='lablerow'>Értékesítő neve:</td>
				<td>
				<select name='reseller_agent'>
				<option value='0'>Válasszon</option>

				<? $office = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE code = '$CURUSER[reseller_office]' LIMIT 1"));
					
					for($i = 1; $i<=10;$i++)
					{
						if($office["agent_".$i] <> '')
							echo "<option value='".trim($office["agent_".$i])."'>".$office["agent_".$i]."</option>";
						
					}	
					
				?>
				</select>
				<input type="hidden" name="reseller_office" value="<?=$CURUSER[reseller_office]?>"/></td>
			</tr>
		<? } elseif($editarr[reseller_agent] <> '') { 
			$office = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = '$editarr[reseller_office]' LIMIT 1"));
	?>
		<tr>
				<td class='lablerow'>Értékesítő neve:</td>
				<td><?=$editarr[reseller_agent]?><input type="hidden" name="reseller_office" value="<?=$editarr[reseller_office]?>"/><input type="hidden" name="reseller_agent" value="<?=$editarr[reseller_agent]?>"/> - <?=$office[name]?> (<?=$office[phone]?>)</td>
			</tr>
		<? } ?>
		<? if($CURUSER[userclass] > 50) { ?>
		<tr>
				<td class='lablerow'>Privát megjegyzés:
					<div style='font-size:10px;'>a hotel is látja</div>
				</td>
				<td><textarea name="hotel_comment"><?=$editarr[hotel_comment]?></textarea></td>
			</tr>
		<tr>
				<td class='lablerow'>Hívás megjegyzés:
					<div style='font-size:10px;'>a hotel SEM látja</div>
				</td>
				<td><textarea name="call_comment"><?=$editarr[call_comment]?></textarea></td>
			</tr>
		
		<tr>
				<td class='lablerow'>Érvényesség:</td>
				<td><textarea name="validity"><?=$editarr[validity]?><?=$formData[validity]?></textarea></td>
			</tr>
		<tr>
				<td class='lablerow'>Nem tartalmazza:</td>
				<td><input type="text" name="not_include" value="<?=$editarr[not_include]?><?=$formData[not_include]?>"/></td>
			</tr>
					
		<? } ?>
		
		<tr>
				<td class='lablerow'>Utalvány típusa:</td>
				<td><select name='gift_type' class="ipicker image-picker show-html">
				<option value='0'  data-img-src="/images/s0.jpg" >Válasszon</option>
				<option value='11' data-img-src="/images/s11.jpg" <? if($editarr[gift_type] == 11) echo "selected"?>>Arany karácsony</option>
				<option value='12' data-img-src="/images/s12.jpg" <? if($editarr[gift_type] == 12) echo "selected"?>>Kék karácsony</option>
				<option value='13' data-img-src="/images/s13.jpg" <? if($editarr[gift_type] == 13) echo "selected"?>>Rózsaszín karácsony</option>
				<option value='14' data-img-src="/images/s14.jpg" <? if($editarr[gift_type] == 14) echo "selected"?>>Ezüst karácsony</option>
				<option value='15' data-img-src="/images/s15.jpg" <? if($editarr[gift_type] == 15) echo "selected"?>>Esküvő</option>
				<option value='16' data-img-src="/images/s16.jpg" <? if($editarr[gift_type] == 16) echo "selected"?>>Barna karácsony</option>
				</select>
				<table style='border:none;width:490px;'>
					<tr style='border:none;'>
						<td width='55' style='border:none;'></td>
						<? for($i=11;$i<=16;$i++) { ?>
							<td  style='border:none;' width='55' align='center'><a href='http://admin.indulhatunk.hu/images/<?=$i?>.jpg' rel='fancy_gallery' class='fancy_gallery' style='text-decoration:none; font-weight:bold; color:#252525;font-size:11px;'>megtekint</a></td>
						<?}?>
					</tr>
				</table>
				</td>
			</tr>	
	
		<tr>
				<td class='lablerow'>Megszólítása:</td>
				<td><input type="text" name="gift_title" style ='width:200px;' placeholder='Kedves Erzsébet és András' value="<?=$editarr[gift_title]?>"/>!</td>
			</tr>	
		<tr>
				<td class='lablerow'>Személyes szöveg:
				</td>
				<td><textarea name="gift_body" id='limited' style='height:70px;'><?=$editarr[gift_body]?></textarea>
				<div style='text-align:right;'><span id="chars">300</span> / 300 karakter</div></td>
			</tr>


			<tr>
				<td class='lablerow'>Kuponkód:</td>
				<td><input type="text" name="discount_code" id="discount_code" style ='width:100px;' value="<?=$editarr[discount_code]?>"/> <span id="promo_status"></span></td>
			</tr>	
			




		<tr>
				<td class='lablerow'>Ár elrejtése?</td>
				<td>
		<select name='gift'>
			<option value='1' <?if($editarr[gift]==1)echo"selected";?>>Igen</option>
			<option value='0'>Nem</option>
			</select>
		</td>
			</tr>
		
		
		<? if($CURUSER[userclass] > 50) { ?>
			
			<tr>
				<td class='lablerow'>Email ID:</td>
				<td><input type="text" name="email_id" value="<?=$editarr[email_id]?>" class='numeric' style='width:50px;'/></td>
			</tr>
			<tr>
				<td class='lablerow'>Számlaszám:</td>
				<td><input type="text" name="user_invoice_number" value="<?=$editarr[user_invoice_number]?>"/></td>
			</tr>
			<tr>
				<td class='lablerow'>Számla dátuma:</td>
				<td><input type="text" name="user_invoice_date" value="<?=$editarr[user_invoice_date]?>"/></td>
			</tr>
			<tr>
				<td class='lablerow'>Számla elkezdve:</td>
				<td><input type="text" name="user_invoice_started" value="<?=$editarr[user_invoice_started]?>"/></td>
			</tr>
		<? } ?>

		<?if($editarr[inactive] == 1 && $CURUSER[userclass] > 50){?>
		<tr>
				<td class='lablerow'>Törölt:</td>
				<td>
			<select name="inactive">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[inactive]==1)echo"selected";?>>Igen</option>
			</select>
		</td>
			</tr>
		<?
		}
		//if($CURUSER["userclass"] == 255)
		if($CURUSER[passengers_admin] == 1)
		{
		?>		
		<tr>
				<td class='lablerow'>Fizetve:</td>
				<td>
			<select name="paid">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[paid]==1)echo"selected";?>>Igen</option>
			</select>
		</td>
			</tr>			
<tr>
				<td class='lablerow'>Nyomtatva?:</td>
				<td><input type='text' name='voucher' value='<?=$editarr[voucher]?>'></td>
			</tr>
		<? } ?>

	
	<?
	if($editarr[offer_id] <>"") {
		$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $editarr[offers_id]");
		$offerArr = mysql_fetch_assoc($offerQr);
		
		if($CURUSER[userclass] == 5 || $editarr[type] == 8)
			$offerArr[actual_price] = $offerArr[actual_price];
		else	
			$offerArr[actual_price] = $offerArr[actual_price];
	?>
	<input type="hidden" name="days" value="<?=$offerArr[days]?>"/>
	<input type="hidden" name="actual_price" value="<?=$offerArr[actual_price]?>"/>

	<tr>
		<td colspan='2' class='header'>Felár információk</td>
	</tr>
	<tr>
				<td class='lablerow'>Ajánlat alapára:</td>
				<td> <?=formatPrice($offerArr[actual_price]);?> </td>
			</tr>
	<tr>
				<td class='lablerow'>Éjszakák száma:</td>
				<td> <?=$offerArr[days]?> éj </td>
			</tr>


<?
	if($offerArr[joker] <> 0 && $CURUSER[userclass] > 50) { ?>
	<tr class='yellow'>
				<td class='lablerow'>Joker kártya?</td>
				<td><input type="checkbox" name="joker" class="joker" value="<?=$offerArr[joker]?>" <?if($editarr[joker]>0)echo"checked"?> <?=$disabled?>/><label>- <?=formatPrice($offerArr[actual_price]*$offerArr[joker]/100)?> kedvezmény</td>
				<td></td>
			</tr>
<?}?>


	<?
	if($offerArr[plus_half_board]<>0) { ?>
	<tr>
				<td class='lablerow'>Félpanzió</td>
				<td><input type="checkbox" name="plus_half_board" class="plus_half_board" value="<?=$offerArr[plus_half_board]?>" <?if($editarr[plus_half_board]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_half_board])?>/éj</td>
				<td></td>
			</tr>
<?}?>

<?if($offerArr[plus_weekend_plus]<>0) {?>
<tr>
				<td class='lablerow'>Hétvégi felár</td>
				<td><input type="checkbox" name="plus_weekend_plus" class="plus_weekend_plus" value="<?=$offerArr[plus_weekend_plus]?>" <?if($editarr[plus_weekend_plus]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_weekend_plus]*2)?></td>
			</tr>
<?}?>

<?if($offerArr[plus_bed]<>0) {?>
<tr>
				<td class='lablerow'>Pótágy felár</td>
				<td><input type="checkbox" name="plus_bed" class="plus_bed" value="<?=$offerArr[plus_bed]?>" <?if($editarr[plus_bed]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_bed_plus_food]<>0) {?>
<tr>
				<td class='lablerow'>Félpanzió + pótágy felár</td>
				<td><input type="checkbox" name="plus_bed_plus_food" class="plus_bed_plus_food" value="<?=$offerArr[plus_bed_plus_food]?>"  <?if($editarr[plus_bed_plus_food]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed_plus_food])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_child2_value]<>0) {?>
<tr>
				<td class='lablerow'>1. gyermek kora:</td>
				<td> <input type="hidden" name="prev_child1" value="0" class="prev_child1" <?=$disabled?>/>
	<select name="child1_value" class="child1_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>"  <? if($editarr[plus_child1_value]==1 || ($editarr[plus_child1_value] >1 && $editarr[plus_child1_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	</td>
			</tr><?}
else
{
?>
<input type='hidden' name='child1_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_child2_value]<>0) {?>
<tr>
				<td class='lablerow'>2. gyermek kora: </td>
				<td> <input type="hidden" name="prev_child2" value="0" class="prev_child2"/>
	<select name="child2_value" class="child2_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child2_value]==1 || ($editarr[plus_child2_value] >1 && $editarr[plus_child2_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	</td>
			</tr><?}
else
{
?>
<input type='hidden' name='child2_value' value='999'/>
<?
}
?>
<?if($offerArr[plus_child2_value]<>0) {?>
<tr>
				<td class='lablerow'>3. gyermek: </td>
				<td>	
	<select name="child3_value" class="child3_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child3_value]==1 || ($editarr[plus_child3_value] >1 && $editarr[plus_child3_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig </option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett</option>
	</select>
</td>
			</tr>
<?}
else
{
?>
<input type='hidden' name='child3_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_room1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room1_name]?> szoba felár</td>
				<td> <input type="checkbox" class="room1_value" name="room1_value" value="<?=$offerArr[plus_room1_value]?>" <?if($editarr[plus_room1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room1_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_room2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room2_name]?> szoba felár</td>
				<td> <input type="checkbox" class="room2_value" name="room2_value" value="<?=$offerArr[plus_room2_value]?>" <?if($editarr[plus_room2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room2_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_room3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room3_name]?> szoba felár</td>
				<td> <input type="checkbox" class="room3_value" name="room3_value" value="<?=$offerArr[plus_room3_value]?>" <?if($editarr[plus_room3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room3_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other1_name]?> felár</td>
				<td> <input type="checkbox" class="other1_value" name="other1_value" value="<?=$offerArr[plus_other1_value]?>" <?if($editarr[plus_other1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other1_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other2_name]?> felár</td>
				<td> <input type="checkbox"  class="other2_value" name="other2_value" value="<?=$offerArr[plus_other2_value]?>" <?if($editarr[plus_other2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other2_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other3_name]?> felár</td>
				<td> <input type="checkbox" class="other3_value" name="other3_value" value="<?=$offerArr[plus_other3_value]?>" <?if($editarr[plus_other3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other3_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other4_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other4_name]?> felár</td>
				<td> <input type="checkbox" class="other4_value" name="other4_value" value="<?=$offerArr[plus_other4_value]?>" <?if($editarr[plus_other4_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other4_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other5_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other5_name]?> felár</td>
				<td> <input type="checkbox" class="other5_value" name="other5_value" value="<?=$offerArr[plus_other5_value]?>" <?if($editarr[plus_other5_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other5_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_other6_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other6_name]?> felár</td>
				<td> <input type="checkbox" class="other6_value" name="other6_value" value="<?=$offerArr[plus_other6_value]?>" <?if($editarr[plus_other6_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other6_value])?>/éj</td>
			</tr>
<?}?>

<?if($offerArr[plus_single1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single1_name]?> felár</td>
				<td> <input type="checkbox" class="plus_single1_value" name="plus_single1_value" value="<?=$offerArr[plus_single1_value]?>" <?if($editarr[plus_single1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single1_value])?>/csomag</td>
			</tr>
<?}?>
<?if($offerArr[plus_single2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single2_name]?> felár</td>
				<td> <input type="checkbox" class="plus_single2_value" name="plus_single2_value" value="<?=$offerArr[plus_single2_value]?>" <?if($editarr[plus_single2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single2_value])?>/csomag</td>
			</tr>
<?}?>
<?if($offerArr[plus_single3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single3_name]?> felár</td>
				<td> <input type="checkbox" class="plus_single3_value" name="plus_single3_value" value="<?=$offerArr[plus_single3_value]?>" <?if($editarr[plus_single3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single3_value])?>/csomag</td>
			</tr>
<?}?>


<?if( $offerArr[days]>0 && $offerArr[no_plus_one_day] == 0) {?>

<tr>
				<td class='lablerow'>Maradjon még egy éjszakát</td>
				<td><input type="hidden" name="plus_day_temp" class="plus_day_temp" value="<?=$offerArr[actual_price]/$offerArr[days]?>"  /><input type="checkbox" name="plus_days" class="plus_days" value="<?=$offerArr[actual_price]/$offerArr[days]?>" <?if($editarr[plus_days]>0)echo"checked"?> <?=$disabled?>/><label><span id="plusDays"><? if($CURUSER[userclass] > 50) echo "+ ".formatPrice($offerArr[actual_price]/$offerArr[days])?></span></td>
			</tr>
<?
}

}

echo "<tr class='header'>
				<td align='left'>Végösszeg</td>
				<td align='right'>".formatPrice($editarr[orig_price])." <br/><span class='small'>(ha új felárat adsz hozzá, a mentés után látszik a jó ár)</span></td>
			</tr>";


if($editarr[invoice_created] <> 1) { 

if($CURUSER[userclass] < 50 && $editarr[paid] == 1)
{
?>
	<tr><td colspan='2'><center><b>Az vásárló fizetés után már nem módosítható!</b></center></td></tr>
<?
}
else {

	if($editarr[pid] > 0) { 
?>
	<tr><td colspan='2' align='center'><input type="submit" value="Mentés" id="customersubmit"/></td></tr>
<?
	} else { ?>

	<tr><td colspan='2' align='center'><input type="submit" value="Mentés és felárak hozzáadása &raquo;" id="customersubmit"/></td></tr>
	
	<?
	}
}
 }
 elseif($editarr[invoice_created] == 1 && $CURUSER[userclass] > 50) { ?>
	<tr><td colspan='2' align='center'><input type="submit" value="Mentés" id="customersubmit"/></td></tr>

 <? } ?>
	</table>




<? /* ?>
	<ul>
		<?if($editarr[offer_id] =="") {?>
			<li><label>Azonosító:</label><?=$string?></li>
		<?}	else {?>
			<li><label>Azonosító:</label><?=$editarr[offer_id]?></li>
		<?}?>
		
		<? if($CURUSER[userclass] > 50) { ?>
		<li>
		<select name="offers_id">
				<?
				
				if($editarr[cid] == '')
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW()  ORDER BY partners.hotel_name ASC");
				}
				else
				{
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id ORDER BY partners.hotel_name ASC");
				}
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name] - ".formatPrice($partnerArr[actual_price])."</option>";
					}	
				?>
			</select>
		</li>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] > 0) { 

			$partnerArr = mysql_fetch_assoc($mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.id = $editarr[offers_id] LIMIT 1"));

		?>	
			<li><label>Ajánlat:</label><? echo "$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name]" ?></li>
			<input type="hidden" name="offers_id" value="<?=$editarr[offers_id]?>"/>
		<? } elseif($CURUSER[userclass] == 5 && $editarr[cid] == 0) { ?>
			<select name="offers_id">
				<?
				
					$partnerQuery = $mysql->query("SELECT offers.outer_name,offers.id,offers.shortname,partners.hotel_name, offers.actual_price FROM offers INNER JOIN partners on partners.pid = offers.partner_id WHERE  offers.start_date <= NOW() AND offers.end_date > NOW() AND reseller_enabled = 1  ORDER BY partners.hotel_name ASC");
			
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[id] == $editarr[offers_id] || $partnerArr[id] == $formData[id])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[id]\" $selected>$partnerArr[hotel_name] - $partnerArr[shortname] - $partnerArr[outer_name] - ".formatPrice($partnerArr[actual_price])."</option>";
					}	
				?>
			</select>
		</li>
		
		<? } ?>
		<? 	if($editarr[cid] <> '' && $CURUSER[userclass] > 50)
			{?>
		<li>
		<select name="sub_pid">
			<option value=''>Válasszon</option>
				<?
				
				$partnerQuery = $mysql->query("SELECT * FROM partners WHERE (country = '' or country = 'hu') AND hotel_name <> '' ORDER BY partners.hotel_name ASC");
				
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[pid] == $editarr[sub_pid] || $partnerArr[pid] == $formData[sub_pid])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name]</option>";
					}
				
				?>
			</select>
		</li>
		<? } else { ?>
			<input type="hidden" name="sub_pid" value="<?=$editarr[sub_pid]?>"/>
		<? } ?>
		<li><label>Vevő neve*:</label><input type="text" name="name" value="<?=$editarr[name]?>"/></li>
		<li><label>Cím:</label><input type="text" name="zip" value="<?=$editarr[zip]?>" style='width:35px;' class='numeric'/><input type="text" name="city" style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" name="address" class="small" style='width:295px;' value="<?=$editarr[address]?>" /></li>
		
		
		<li id='invoicebutton'  <? if($editarr[invoice_name] <> '') echo "style='display:none'"?>><div style='text-align:center; margin:5px; padding:5px; border-top:1px solid #c4c4c4; border-bottom:1px solid #c4c4c4;'><a href='#' id='showinvdata'><b>Eltérő számlázási adatok</b></a></div></li>
		
		<li class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><label>Számlázási név:</label><input type="text" name="invoice_name" value="<?=$editarr[invoice_name]?>"/></li>
		<li class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><label>Számlázási Cím:</label><input type="text" name="invoice_zip"value="<?=$editarr[invoice_zip]?>" style='width:35px;'/><input type="text" style='width:140px;margin:0 1px 0 1px' class="small1" value="<?=$editarr[invoice_city]?>" name="invoice_city"/><input type="text" name="invoice_address" class="small" style='width:295px;' value="<?=$editarr[invoice_address]?>" /></li>
		
		
		<li><label>Telefonszám*:</label><input type="text" name="phone" value="<?=$editarr[phone]?>"/></li>
		<li><label>E-mail cím*:</label><input type="text" name="email" value="<?=$editarr[email]?>"/></li>

		<? if($CURUSER[userclass] > 50) { ?>
		<li><label>Licit:</label><input type="text" name="bid_name" value="<?=$editarr[bid_name]?><?=$formData[bid_name]?>"/></li>
		<li><label>Licit típus:</label>
			<select name="type">
				<option value="4" <?if($editarr[type]==4)echo"selected";?>>Outlet</option>
				<option value="1" <?if($editarr[type]==1)echo"selected";?>>Vatera</option>
				<option value="2" <?if($editarr[type]==2)echo"selected";?>>Teszvesz</option>
				<option value="6" <?if($editarr[type]==6)echo"selected";?>>Lealkudtuk</option>
				<option value="7" <?if($editarr[type]==7)echo"selected";?>>Grando</option>
				<option value="3" <?if($editarr[type]==3)echo"selected";?>>Licittravel</option>
				<option value="5" <?if($editarr[type]==5)echo"selected";?>>Pult</option>
				<option value="8" <?if($editarr[type]==8)echo"selected";?>>Viszonteladó</option>
			</select>
		</li>
		<li><label>Eladási ár:</label><input type="text" name="orig_price" value="<?=$editarr[orig_price]?><?=$formData[orig_price]?>" style='width:50px;' class='numeric'/> Ft</li>
		<!--<li><label>Promóciós kód:</label>
			<select name="promotion_code">
				<option value=''>Válasszon</option>
				<?
					$partnerQuery = $mysql->query("SELECT * FROM promotion ORDER BY name ASC");
					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($partnerArr[code] == $editarr[promotion_code])
							$selected = "selected";
						else
							$selected = "";
						echo "<option value=\"$partnerArr[code]\" $selected>$partnerArr[name]</option>";
					}
				
				?>
			</select>
		</li>-->
		<li><label>Fizetés módja:</label>
			<select name="payment">
				<option value="2" <?if($editarr[payment]==2)echo"selected";?>>Készpénz</option>
				<option value="1" <?if($editarr[payment]==1)echo"selected";?>>Átutalás</option>
				<option value="3" <?if($editarr[payment]==3)echo"selected";?>>Utánvét</option>
				<option value="9" <?if($editarr[payment]==9)echo"selected";?>>Bankkártya</option>
				<option value="4" <?if($editarr[payment]==4)echo"selected";?>>Futár</option>
				<option value="5" <?if($editarr[payment]==5)echo"selected";?>>Üdülési csekk</option>
				<option value="6" <?if($editarr[payment]==6)echo"selected";?>>Helyszinen</option>
				<option value="7" <?if($editarr[payment]==7)echo"selected";?>>Online</option>
				<option value="8" <?if($editarr[payment]==8)echo"selected";?>>Facebook</option>
				<option value="10" <?if($editarr[payment]==10)echo"selected";?>>OTP SZÉP kártya</option>
				<option value="11" <?if($editarr[payment]==11)echo"selected";?>>MKB SZÉP kártya</option>
				<option value="12" <?if($editarr[payment]==12)echo"selected";?>>K&H SZÉP kártya</option>


			</select>
		</li>
		<li><label>Szállítás módja:</label>
			<select name="shipment">
				<option value="0">Válassza ki az átvételi módot</option>
				<option value="2" <?if($editarr[shipment]==2)echo"selected";?>>Online kérem</option>
				<option value="3" <?if($editarr[shipment]==3)echo"selected";?>>Személyes átvétel</option>
				<option value="4" <?if($editarr[shipment]==4)echo"selected";?>>Ajánlott levélként: +395 Ft</option>
				<option value="5" <?if($editarr[shipment]==5)echo"selected";?>>Postai utánvéttel: +1 450 Ft</option>
			</select>

		</li>
		<li><label>Postaköltség:</label><input type="text" name="post_fee" value="<?=$editarr[post_fee]?><?=$formData[post_fee]?>" style='width:50px;' class='numeric'/> Ft</li>
		<? } ?>
		<!--<li><label>Üdülésicsekk info:</label><textarea name="checks"><?=$editarr[checks]?></textarea></li>-->
		<li><label>Üdülésicsekkek értéke:</label><input type="text" name="check_value" value="<?=$editarr[check_value]?>" style='width:50px' class='numeric'/> Ft</li>
		<li><label>Üdülésicsekkek száma:</label><input type="text" name="check_count" value="<?=$editarr[check_count]?>"  style='width:50px' class='numeric'/> darab</li>

		<li><label>Megjegyzés:</label><textarea name="comment"><?=$editarr[comment]?></textarea></li>
		<li><label>Hotel megjegyzés:</label><textarea name="hotel_comment"><?=$editarr[hotel_comment]?></textarea></li>
		
		<? if($CURUSER[userclass] > 50) { ?>
		<li><label>Érvényesség:</label><textarea name="validity"><?=$editarr[validity]?><?=$formData[validity]?></textarea></li>
		<li><label>Nem tartalmazza:</label><input type="text" name="not_include" value="<?=$editarr[not_include]?><?=$formData[not_include]?>"/></li>
		<? } ?>
		
		<li><label>Ár elrejtése?</label>
		<select name='gift'>
			<option value='0'>Nem</option>
			<option value='1' <?if($editarr[gift]==1)echo"selected";?>>Igen</option>
			</select>
		</li>
		
		
		<? if($CURUSER[userclass] > 50) { ?>
			
			<li><label>Email ID:</label><input type="text" name="email_id" value="<?=$editarr[email_id]?>" class='numeric' style='width:50px;'/></li>
			<li><label>Számlaszám:</label><input type="text" name="user_invoice_number" value="<?=$editarr[user_invoice_number]?>"/></li>
			<li><label>Számla dátuma:</label><input type="text" name="user_invoice_date" value="<?=$editarr[user_invoice_date]?>"/></li>
			<li><label>Számla elkezdve:</label><input type="text" name="user_invoice_started" value="<?=$editarr[user_invoice_started]?>"/></li>
		<? } ?>

		<?if($editarr[inactive] == 1 && $CURUSER[userclass] > 50){?>
		<li><label>Törölt:</label>
			<select name="inactive">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[inactive]==1)echo"selected";?>>Igen</option>
			</select>
		</li>
		<?
		}
		if($CURUSER["userclass"] == 255)
		{
		?>		
		<li><label>Fizetve:</label>
			<select name="paid">
				<option value="0">Nem</option>
				<option value="1" <?if($editarr[paid]==1)echo"selected";?>>Igen</option>
			</select>
		</li>			

		<? } ?>
		<hr/>
	
	<?
	if($editarr[offer_id] <>"") {
		$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $editarr[offers_id]");
		$offerArr = mysql_fetch_assoc($offerQr);
		
		if($CURUSER[userclass] == 5 || $editarr[type] == 8)
			$offerArr[actual_price] = $offerArr[reseller_price];
		else	
			$offerArr[actual_price] = $offerArr[actual_price];
	?>
	<input type="hidden" name="days" value="<?=$offerArr[days]?>"/>
	<input type="hidden" name="actual_price" value="<?=$offerArr[actual_price]?>"/>


	<li><label>Ajánlat alapára:</label> <?=formatPrice($offerArr[actual_price]);?> </li>
	<li><label>Éjszakák száma:</label> <?=$offerArr[days]?> éj </li>

	<?
		if($offerArr[plus_half_board]<>0) {?>
	<li><label>Félpanzió</label><input type="checkbox" name="plus_half_board" class="plus_half_board" value="<?=$offerArr[plus_half_board]?>" <?if($editarr[plus_half_board]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_half_board])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_weekend_plus]<>0) {?>
<li><label>Hétvégi felár</label><input type="checkbox" name="plus_weekend_plus" class="plus_weekend_plus" value="<?=$offerArr[plus_weekend_plus]?>" <?if($editarr[plus_weekend_plus]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_weekend_plus]*2)?></label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_bed]<>0) {?>
<li><label>Pótágy felár</label><input type="checkbox" name="plus_bed" class="plus_bed" value="<?=$offerArr[plus_bed]?>" <?if($editarr[plus_bed]>0)echo"checked"?> <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_bed_plus_food]<>0) {?>
<li><label>Félpanzió + pótágy felár</label><input type="checkbox" name="plus_bed_plus_food" class="plus_bed_plus_food" value="<?=$offerArr[plus_bed_plus_food]?>"  <?if($editarr[plus_bed_plus_food]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_bed_plus_food])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>1. gyermek kora:</label> <input type="hidden" name="prev_child1" value="0" class="prev_child1" <?=$disabled?>/>
	<select name="child1_value" class="child1_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>"  <? if($editarr[plus_child1_value]==1 || ($editarr[plus_child1_value] >1 && $editarr[plus_child1_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child1_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	<div class='cleaner'></div>
</li>
<?}
else
{
?>
<input type='hidden' name='child1_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>2. gyermek kora: </label> <input type="hidden" name="prev_child2" value="0" class="prev_child2"/>
	<select name="child2_value" class="child2_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child2_value]==1 || ($editarr[plus_child2_value] >1 && $editarr[plus_child2_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig + <?=formatPrice($offerArr[plus_child1_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között + <?=formatPrice($offerArr[plus_child2_value])?>/éj</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child2_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett + <?=formatPrice($offerArr[plus_child3_value])?>/éj</option>
	</select>
	<div class='cleaner'></div>
</li>
<?}
else
{
?>
<input type='hidden' name='child2_value' value='999'/>
<?
}
?>
<?if($offerArr[plus_child2_value]<>0) {?>
<li><label>3. gyermek: </label>	
	<select name="child3_value" class="child3_value" <?=$disabled?>>
		<option value="999">Kérjük válasszon</option>
		<option value="<?=$offerArr[plus_child1_value]?>" <? if($editarr[plus_child3_value]==1 || ($editarr[plus_child3_value] >1 && $editarr[plus_child3_value]==$offerArr[plus_child1_value])) echo "selected"?>>0-<?=$offerArr[plus_child1_name]?> éves korig </option>
		<option value="<?=$offerArr[plus_child2_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child2_value])echo"selected"?>><?=$offerArr[plus_child1_name]?>-<?=$offerArr[plus_child2_name]?> kor között</option>
		<option value="<?=$offerArr[plus_child3_value]?>" <?if($editarr[plus_child3_value]==$offerArr[plus_child3_value])echo"selected"?>><?=$offerArr[plus_child2_name]?> éves kor felett</option>
	</select>
<div class='cleaner'></div></li>
<?}
else
{
?>
<input type='hidden' name='child3_value' value='999'/>
<?
}
?>

<?if($offerArr[plus_room1_value]<>0) {?>
<li><label><?=$offerArr[plus_room1_name]?> szoba felár</label> <input type="checkbox" class="room1_value" name="room1_value" value="<?=$offerArr[plus_room1_value]?>" <?if($editarr[plus_room1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room1_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_room2_value]<>0) {?>
<li><label><?=$offerArr[plus_room2_name]?> szoba felár</label> <input type="checkbox" class="room2_value" name="room2_value" value="<?=$offerArr[plus_room2_value]?>" <?if($editarr[plus_room2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room2_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_room3_value]<>0) {?>
<li><label><?=$offerArr[plus_room3_name]?> szoba felár</label> <input type="checkbox" class="room3_value" name="room3_value" value="<?=$offerArr[plus_room3_value]?>" <?if($editarr[plus_room3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_room3_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other1_value]<>0) {?>
<li><label><?=$offerArr[plus_other1_name]?> felár</label> <input type="checkbox" class="other1_value" name="other1_value" value="<?=$offerArr[plus_other1_value]?>" <?if($editarr[plus_other1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other1_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other2_value]<>0) {?>
<li><label><?=$offerArr[plus_other2_name]?> felár</label> <input type="checkbox"  class="other2_value" name="other2_value" value="<?=$offerArr[plus_other2_value]?>" <?if($editarr[plus_other2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other2_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other3_value]<>0) {?>
<li><label><?=$offerArr[plus_other3_name]?> felár</label> <input type="checkbox" class="other3_value" name="other3_value" value="<?=$offerArr[plus_other3_value]?>" <?if($editarr[plus_other3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other3_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other4_value]<>0) {?>
<li><label><?=$offerArr[plus_other4_name]?> felár</label> <input type="checkbox" class="other4_value" name="other4_value" value="<?=$offerArr[plus_other4_value]?>" <?if($editarr[plus_other4_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other4_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other5_value]<>0) {?>
<li><label><?=$offerArr[plus_other5_name]?> felár</label> <input type="checkbox" class="other5_value" name="other5_value" value="<?=$offerArr[plus_other5_value]?>" <?if($editarr[plus_other5_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other5_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_other6_value]<>0) {?>
<li><label><?=$offerArr[plus_other6_name]?> felár</label> <input type="checkbox" class="other6_value" name="other6_value" value="<?=$offerArr[plus_other6_value]?>" <?if($editarr[plus_other6_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_other6_value])?>/éj</label><div class='cleaner'></div></li>
<?}?>

<?if($offerArr[plus_single1_value]<>0) {?>
<li><label><?=$offerArr[plus_single1_name]?> felár</label> <input type="checkbox" class="plus_single1_value" name="plus_single1_value" value="<?=$offerArr[plus_single1_value]?>" <?if($editarr[plus_single1_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single1_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>
<?if($offerArr[plus_single2_value]<>0) {?>
<li><label><?=$offerArr[plus_single2_name]?> felár</label> <input type="checkbox" class="plus_single2_value" name="plus_single2_value" value="<?=$offerArr[plus_single2_value]?>" <?if($editarr[plus_single2_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single2_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>
<?if($offerArr[plus_single3_value]<>0) {?>
<li><label><?=$offerArr[plus_single3_name]?> felár</label> <input type="checkbox" class="plus_single3_value" name="plus_single3_value" value="<?=$offerArr[plus_single3_value]?>" <?if($editarr[plus_single3_value]>0)echo"checked"?>  <?=$disabled?>/><label>+ <?=formatPrice($offerArr[plus_single3_value])?>/csomag</label><div class='cleaner'></div></li>
<?}?>


<?if( $offerArr[days]>0) {?>

<li><label>Maradjon még egy éjszakát</label><input type="hidden" name="plus_day_temp" class="plus_day_temp" value="<?=$offerArr[actual_price]/$offerArr[days]?>"  /><input type="checkbox" name="plus_days" class="plus_days" value="<?=$offerArr[actual_price]/$offerArr[days]?>" <?if($editarr[plus_days]>0)echo"checked"?> <?=$disabled?>/><label><span id="plusDays"><!--+ <?=formatPrice($offerArr[actual_price]/$offerArr[days])?>--></span></label><div class='cleaner'></div></li>
<?
}

}


if($editarr[invoice_created] <> 1) { 

if($CURUSER[userclass] < 50 && $editarr[paid] == 1)
{
?>
	<li><center><b>Az vásárló fizetés után már nem módosítható!</b></center></li>
<?
}
else {
?>
	<li><input type="submit" value="Mentés" /></li>
<?
}
 } ?>
<? */ ?>


	</fieldset>
	</form>
</div>

</div></div>
<?
foot();
die;
}



echo "<center><select id='monthChange'>";
$listerQuery = $mysql->query("SELECT MONTH(added) AS listerMonth, YEAR(added) AS listerYear FROM customers WHERE cid > 0 AND inactive = 0 $extraSelect GROUP BY YEAR(added),MONTH(added)");

	echo "<option value='0'>$lang[select]</option>";
while($listerArr = mysql_fetch_assoc($listerQuery)) {
	
	if($listerArr[listerMonth] == $month && $listerArr[listerYear] == $year) {
		$bold = "selected";
	}
	else
		$bold = "";
	echo "<option value='year=$listerArr[listerYear]&month=$listerArr[listerMonth]&type=$filter' $bold>$listerArr[listerYear]. ".$lang['months'][$listerArr[listerMonth]]."</option>";
}
echo "</select></center>";


echo "<hr/>";

if($filter <> 'white' &&  $filter <> 'green' && $filter <> 'red' && $filter <> 'blue' && $filter <> 'yellow' && $filter <> '' && $filter <> 'grey' && $filter <> 'orange')// || 
{
	die(":)");
}
if($filter == "white" || $filter == '')
	$filterSelect = "AND paid = 0 AND voucher = 0 AND name <> 'Pult'";
if($filter == "green" && $CURUSER[userclass] <> 5)
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created <> 1";
if($filter == "green" && $CURUSER[userclass] == 5)
	$filterSelect = "AND paid = 1 AND voucher = 1";
if($filter == "blue")
	$filterSelect = "AND paid = 1 AND voucher = 0";
if($filter == "red")
	$filterSelect = "AND paid = 0 AND voucher = 1 AND type <> 5";
if($filter == "yellow")
	$filterSelect = "AND paid = 0 AND type = 5";
if($filter == "grey" && $CURUSER[userclass] <> 5)
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created = 1";
if($filter == "grey" && $CURUSER[userclass] == 5)
	$filterSelect = "AND reseller_invoice <> ''";
if($filter == "orange")
	$filterSelect = "AND paid = 0 AND voucher = 0 AND payment = 3";
	
if($CURUSER[disable_group_customers] == 1 && $filter == '')
{
	$filterSelect = '';
}	
//pagination 
 $perpage = 25; 
$qrStart = 0;

if($CURUSER[userclass] < 10)
{

$pg = (int)$_GET[page];

 $pq = "SELECT cid FROM customers WHERE MONTH(added)=$month AND YEAR(added)=$year AND inactive = 0 $filterSelect $extraSelect  ORDER BY added DESC";
 $pq = $mysql->query($pq);
 $rows = mysql_num_rows($pq);
 $last = ceil($rows/$perpage);
 if($pg == '' && $pg == 0)
 {
 	$qrStart = 0;
 }
 elseif($pg == 1)
 {
 	$qrStart = 1;
 }
 else
 {
 	$qrStart = ($pg-1)*$perpage-1;
 	
  }
}
  //$qrStart = 0;
  //$perpage = 25; 
//pagination

//if searchquery is not empty

if($_GET[offer_id] <> '' && $CURUSER[userclass] > 10)
{
		$qr = "SELECT * FROM customers WHERE offer_id = '$_GET[offer_id]'  ORDER BY added DESC LIMIT $qrStart, $perpage";

}
else if($s <> '')
{

	if($CURUSER[userclass] > 50)
	{
		$ownqr = mysql_fetch_assoc($mysql->query("SELECT * FROM own_vouchers WHERE id > 0  AND (name LIKE '%$s%' OR email like '%$s%') AND paid = 0"));

		if($ownqr[name] <> '')
		{
			echo message("<a href='/own_vouchers.php'>Vásárló a saját eladások között ($ownqr[name]) &raquo;</a>");
		}
	}


	if($_POST[showinactive] == 'on')
	{
		$showdeleted = 'inactive = 1';
	}
	else
	{
		$showdeleted = 'inactive = 0';
	}
	
	if($s == 'tvo-' || $s == 'czo-')
		$lm = '';
	else
		$lm = "LIMIT $qrStart, $perpage";
	
	if($s == 'szo' || $s == 'szo-' || $s == 'isz-' || $s == 'szo')
		die;
			
	$qr = "SELECT * FROM customers WHERE cid > 0 AND $showdeleted  $extraSelect AND (offer_id like '%$s%' OR user_invoice_number like '%$s%' OR invoice_number LIKE '%$s%'  OR phone LIKE '%$s%'  OR name like '%$s%' OR invoice_name like '%$s%' OR comment like '%$s%' OR hotel_comment like '%$s%' OR bidder_name like '%$s%' OR email like '%$s%' ) ORDER BY added DESC $lm";
}
else
	$qr = "SELECT * FROM customers WHERE MONTH(added)=$month AND YEAR(added)=$year AND inactive = 0 $filterSelect $extraSelect  ORDER BY added DESC LIMIT $qrStart, $perpage";

	
//rint $qr;
$query = $mysql->query($qr);


if(mysql_num_rows($query) <= 0)
{
	echo "<div class='redmessage'>$lang[no_results]</div>";
	foot();
	die;
}	

?>
<? if($CURUSER[userclass] >= 5) { ?>

<div id='sumtotal'>
	<form>
	<table style='width:250px'>
		<tr>
			<td colspan='1'>Fizetendő:</td>
			<td colspan='1' id='sumtvalue' align='right'></td>
		</tr>
		<tr>
			<td>Készpénz:</td>
			<td align='right'><input type='text' id='ptotal' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td>ÜCS/SZÉP</td>
			<td align='right'><input type='text' id='paidtotal' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td>Visszajáró:</td>
			<td id='chtotal' align='right' style='font-size:16px; font-weight:bold;'></td>
		</tr>
		<tr>
			<td id='numbers' colspan='2' align='center'></td>
		</tr>

	</table>
	</form>
	
</div>


<? } ?>
<?
echo "<table class=\"general\" width='100%'>";
echo "<tr class=\"header\">";

	//if($CURUSER[userclass] <> 5) {
		echo "<td width='20'></td>";
	//}
	
	if($CURUSER[userclass] > 50)
		echo "<td width='20'></td>";
	
	echo "<td width='85'>$lang[id]</td>";
	
	if($CURUSER[userclass] >= 5) {
		echo "<td width='70'>$lang[date]</td>";
	}

	echo "<td width='200'>$lang[customer_name]</td>";
	echo "<td>$lang[partner]</td>";
	echo "<td width='60'>$lang[price]</td>";
	echo "<td>$lang[payment]</td>";
	
	if($CURUSER[userclass] < 5)
	{
		echo "<td>$lang[customer_left]</td>";
		echo "<td>$lang[customer_arrival]</td>";
	}
		
	if($CURUSER[userclass] >= 5) {
		echo "<td>$lang[paid]?</td>";
		echo "<td>$lang[tools]?</td>";
		
		if($CURUSER[userclass] > 50)
			echo "<td width='45'>$lang[post]</td>";
			
		echo "<td>$lang[bill]</td>";
		echo "<td colspan='2'>$lang[comment]</td>";
	}
echo "</tr>";
while($arr = mysql_fetch_assoc($query)) {

	$row = '';
	if($arr[type] == 1) 
		$type = "Vatera";
	elseif($arr[type] == 2) 
		$type = "Teszvesz";
	elseif($arr[type] == 3) 
		$type = "Licittravel";
	elseif($arr[type] == 4) 
		$type = "Outlet";
	elseif($arr[type] == 5) 
		$type = "Pult";
	elseif($arr[type] == 6) 
		$type = "Lealkudtuk";
	elseif($arr[type] == 7) 
		$type = "Grando";
	elseif($arr[type] == 8) 
		$type = "Viszonteladó";
	elseif($arr[type] == 12) 
		$type = "Quaestor";
	
	
	//do not display postponed because of offers
	$offerQuery = $mysql->query("SELECT expiration_date,abroad, special_voucher FROM offers WHERE id = '$arr[offers_id]' LIMIT 1");
	$offerArr = mysql_fetch_assoc($offerQuery);
		
	$ppyear = explode("-","$arr[paid_date]");
	$ppyear = $ppyear[0];
	
	if($arr[postpone] == 1 && $offerArr[abroad] <> 1 && $CURUSER[userclass] <> 5 && $ppyear == 2012)
		$payment = $lang[t_check];
	elseif($arr[postpone] == 1 && $offerArr[abroad] <> 1 && $CURUSER[userclass] <> 5 && $ppyear == 2013)
		$payment = "OTP SZÉP kártya";
	elseif($arr[payment] == 1  || $arr[checkpaper_id] > 0) 
		$payment = $lang[transfer];
	elseif($arr[payment] == 2) 
		$payment = $lang[cash];
	elseif($arr[payment] == 3) 
		$payment = $lang[postpaid];
	elseif($arr[payment] == 4) 
		$payment = $lang[delivery];
	elseif($arr[payment] == 5) 
		$payment = $lang[t_check];
	elseif($arr[payment] == 6) 
		$payment = $lang[place];
	elseif($arr[payment] == 7) 
		$payment = $lang[online];
	elseif($arr[payment] == 8) 
		$payment = $lang[facebook];
	elseif($arr[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($arr[payment] == 10) 
		$payment = "OTP SZÉP kártya";
	elseif($arr[payment] == 11) 
		$payment = "MKB SZÉP kártya";
	elseif($arr[payment] == 12) 
		$payment = "K&H SZÉP kártya";
		
	if($arr[paid] == 0 && $arr[voucher] <> 1 && $arr[name] <> 'Pult') {
		$class = "";
		$paid = $lang[no];
	} 

	elseif($arr[voucher] == 0 && $arr[paid] == 1) {
		$class = "blue";
	}	
	elseif($arr[voucher] == 1 && $arr[paid] == 0) {
		$class = "red";
	}	
	elseif($arr[paid] == 1 && $arr[voucher] == 1 && $arr[invoice_created] <> 1) {
		
		$weekday = date("N");
		
		if($weekday == 1) //monday
			$lightdays = 6;
		if($weekday == 2) //tuesday
			$lightdays = 6;
		if($weekday == 3) //wednesday
			$lightdays = 6;
		if($weekday == 4) //thursday
			$lightdays = 4;
		if($weekday == 5) //friday
			$lightdays = 4;
		if($weekday == 6) //saturday
			$lightdays = 5;
		if($weekday == 7) //sunday
			$lightdays = 6;
					
			
		if(strtotime($arr[paid_date]) > strtotime("-$lightdays days") && $arr[payment] <> 6)
		{
			$class = "lightgreen";
		}
		else
			$class = 'green';
		$paid = $lang[yes];	
	}
	elseif($arr[invoice_created] == 1 && $arr[paid] == 1 && $arr[voucher] == 1){
		$class = "grey";
	}
	elseif($arr[foreign_invoice_number] <> ''){
		$class = "grey";
	}
	elseif($arr[type] == 5 && $arr[address] == '')
	{
		$class = "yellow";
	}
	
	if($arr[inactive] == 1)
		$class = 'purple';

	$partnerQuery = $mysql->query("SELECT company_name,hotel_name FROM partners WHERE pid = '$arr[pid]' LIMIT 1");
	$partnerArr = mysql_fetch_assoc($partnerQuery);

	
	$realPartnerArr = '';
	if($arr[sub_pid] > 0)
	{
		$realPartnerQuery = $mysql->query("SELECT company_name,hotel_name FROM partners WHERE pid = '$arr[sub_pid]' LIMIT 1");
		$realPartnerArr = mysql_fetch_assoc($realPartnerQuery);
	}
	if($CURUSER[userclass] >= 5) {
		$row.= "<form method=\"post\" action=\"customers.php\">";
		$row.=  "<input type=\"hidden\" name=\"cid\" value=\"$arr[cid]\">";
		$row.=  "<input type=\"hidden\" name=\"paidBill\" value=\"1\">";
		$row.=  "<input type=\"hidden\" name=\"payment\" value=\"$arr[payment]\">";
		
		$row.=  "<input type=\"hidden\" name=\"comment\" value=\"$arr[offer_id] voucher fizetve $arr[name] által\">";
		$row.=  "<input type=\"hidden\" name=\"value\" value=\"$arr[orig_price]\">";
		$row.=  "<input type=\"hidden\" name=\"email\" value=\"$arr[email]\">";

		$row.=  "<input type=\"hidden\" name=\"postage\" value=\"$arr[post_fee]\">";

		$row.=  "<input type=\"hidden\" name=\"voucher_id\" value=\"$arr[offer_id]\">";
		
		$row.=  "<input type=\"hidden\" name=\"partner_id\" value=\"$arr[pid] voucher fizetve $arr[name] által\">";
		$row.=  "<input type=\"hidden\" name=\"check_comment\" value=\"$arr[checks]\">";
	}
	$row.=  "<tr class=\"$class\">";
	
	
	if($CURUSER["userclass"] >= 5) {
	

			if($arr[checked] == 1)
				$checked = "<a target='_blank' href=\"http://admin.indulhatunk.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/check1.png' alt='".$arr[checked_date]." @ ".$arr[checked_ip]."' title='".$arr[checked_date]." @ ".$arr[checked_ip]."' width='20'/></b></a>";
			else
				$checked = "<a target='_blank' href=\"http://admin.indulhatunk.hu/customers/".$arr[hash]."/$arr[cid]\" id=\"$arr[cid]\"><b><img src='images/cross1.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>";
				
			if($CURUSER[userclass] == 5)
				$checked = '';
				
			if($CURUSER[userclass] > 50)
				$createinvoice = "<a href=\"/info/manage_invoice.php?cid=$arr[cid]\" rel='facebox iframe'><b><img src='images/storno.png' alt='Számlázási funkciók' title='Számlázási funkciók' width='20'/></b></a>";
			else
				$creatinvoice = "";
				
			$editlink = "$checked
				<a href=\"?add=1&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>
				<a href=\"?delete=$arr[cid]&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/trash.png' alt='töröl' title='töröl' width='20'/></b></a>
				<a href=\"?copy=$arr[cid]&offer_id=$arr[offer_id]\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/copy.png' alt='másol' title='másol' width='20'/></b></a> 
				$createinvoice
				";

		$row.=  "<td>$editlink</td>";
		
	
				
	}
	
	
		if($arr[company_invoice] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		elseif($arr[company_invoice] == 'szallasoutlet')
			$logo = 'szo_logo.png';
		else
			$logo = 'ologo_small.png';
			
			
	if($arr[pid] == 3003 || $arr[pid] == 3121)
		$tvcampaign = "<br/><img src='/images/film.png' width='20' alt='TV kampány utalvány' title='TV kampány utalvány'/>";
	else
		$tvcampaign = '';
			
	if($arr[gift] == 1 && $CURUSER[userclass] >5)
		$gift = "<br/><img src='/images/heart.png' width='20' alt='Az utalvány ajándék' title='Az utalvány ajándék'/>";
	else
		$gift = '';
				
	if($arr[postpone] == 1 && $CURUSER[userclass] > 50 && $arr[postpone_cleared] <> 1)
		$delay = "<br/><img src='/images/delayed.png' width='20' alt='$arr[postpone_date]' title='$arr[postpone_date]'/>";
	else
		$delay = '';


		if($arr[is_qr] == 1)
			$qrlogo = '<img src="http://admin.indulhatunk.hu/images/qr2.jpg" width="15"/>';
		else
			$qrlogo = "";



		if($CURUSER[userclass] <> 5)
			$row.=  "<td><img src='/images/$logo'/> $qrlogo $tvcampaign $gift $delay</td>";
			
		$row.=  "<td>".str_replace("SZO-","",$arr[offer_id])."</td>";
	
		if($CURUSER[userclass] >= 5) {
			$row.=  "<td>".date('y.m.d. H:i',strtotime($arr[added]))."</td>";
		}

		if($arr[inactive] == 1)
			$deletetext = " [ TÖRÖLT ] ";
		else
			$deletetext = '';
			
		$row.=  "<td><a href='info/customer.php?cid=$arr[cid]' rel='facebox'><b>".$deletetext."$arr[name]</b></a></td>";

	$extrapartner ='';
	
	if($arr[sub_pid] > 0)
		$extrapartner = "<br/>".$realPartnerArr[hotel_name];
	
	$row.=  "<td><a href='/info/offer.php?id=$arr[offers_id]&type=offer' rel='facebox'><b>$partnerArr[hotel_name]$extrapartner</b></a></td>"; //preview
	
	
	if($arr[foreign_currency] <> '' && $arr[foreign_currency] <> 'Ft')
		$forval = "<br/>$arr[foreign_orig_price] $arr[foreign_currency] ";
	else
		$forval = '';
	$row.=  "<td align='right'>".str_replace(" ","&nbsp;",formatPrice($arr[orig_price]-$arr[discount_value]))."$forval</td>";
	
	
	if($CURUSER[userclass] == 5 && $arr[paid] == 0)
		$row.=  "<td align='center'>-</td>";
	else
		$row.=  "<td align='center'>$payment</td>";
	
	
	if($CURUSER[userclass] < 5)
	{
		$row.=  "<td align='center'>";
		
		
	
		
		if($arr[paid] == 0)
		{
			$row.=  "<font color='red'><b>$lang[not_paid]</b></font>";
		}
		elseif($arr[is_qr] == 1 && $arr[customer_left] == '0000-00-00 00:00:00')
		{
			$row.=  "<a href='reader.php'><b>QR beolvasás &raquo;</b></a>";
		}
		elseif($arr[paid] == 1 && $arr[customer_left] == '0000-00-00 00:00:00')
		{
			$row.=  "<input type='button' value='$lang[customer_left]' class='alertbox' id='$arr[cid]'/>";
		}
		$row.=  "</td>";
		
		$row.=  "<td align='center'>";
		if($arr[customer_booked] == '0000-00-00')
		{
			$row.=  "<div><input type='button' value='$lang[customer_booked]' class='bookbox' id='$arr[cid]' onclick=\"jQuery.facebox({ ajax: 'info/customer_booked.php?cid=$arr[cid]&returnto=".urlencode($_SERVER[REQUEST_URI])."' });\"/></div>";
		}
		else
			$row.=  "$arr[customer_booked]";
		$row.=  "</td>";
	}
	if($CURUSER["userclass"] >= 5) {
	
		if($arr[invoice_created] == 1) {
				$disabled = "disabled";
		}	
		elseif ($arr[paid] == 1) // && $arr[voucher] == 1
		{
			$disabled = "disabled";
		}
		else {
			$disabled = "";
		}
		
		
		
		$row.=  "<td align='center'>";
		
		if($CURUSER[reseller_online] <> 1)
		{
			$row.=  "<select  onchange=\"jQuery.facebox({ ajax: 'info/pay.php?cid=$arr[cid]&voucher=$arr[voucher]&company=$arr[company_invoice]&returnto=".urlencode($_SERVER[REQUEST_URI])."' });\" $disabled>";
			$row.=  "<option value=\"0\">$lang[no]</option>";
			$row.=  "<option value=\"1\"";
				if($arr[paid] == 1)
					$row.=  "selected";
			$row.=  ">$lang[yes]</option>";
			$row.=  "</select>";
		}
		else
		{
			$row.='-';			
		}	
			if($CURUSER[userclass] > 50 && $arr[szep_successful] == 1)
			{
				$row.=  "<a href='#' alt='".formatPrice($arr[szep_paid])."fizetve | $arr[szep_paid_date] online SZÉP' title='".formatPrice($arr[szep_paid])." fizetve |$arr[szep_paid_date] online SZÉP'> ".formatPrice($arr[szep_paid])."</a>";
				
			}
			$row.=  "</td>";
	
			$row.=  "<input type=\"hidden\" name='voucher' value=\"$arr[voucher]\"/>";
			if($arr[user_invoice_number] == ''  && $CURUSER[userclass] >= 90)
				$asklink = " [<a href=\"invoice/create_ask.php?cid=$arr[cid]&email=$arr[email]\" rel='facebox'>díjbekérő</a>]";
			else
				$asklink = '';
			
			if($CURUSER[userclass] < 50)
				$clearancelink = '';
			elseif($arr[paid] == 1 && ($arr[payment] == 2 || $arr[payment] == 5))
				$clearancelink = " [<a href=\"/vouchers/print_checkout.php?sure=1&type=&cid=$arr[offer_id]\">bizonylat</a>]";
			else
				$clearancelink = '';
				
			
		
			if($CURUSER[userclass] > 50)
			{
				if($arr[paid] <> 1)	
					$sletter = " [<a href=\"info/show_customer_letter.php?cid=$arr[cid]\" rel='facebox'>levél</a>]<br/>";
				else
					$sletter = "";
					
				$notify = " $sletter [<a href=\"vouchers/sendletter.php?cid=$arr[cid]\" rel='facebox'>$lang[notify]</a>]<br/>";
				
				if($arr[is_qr] == 1 && $arr[customer_left] == '0000-00-00 00:00:00')
					$arrivedlink = "[<a href='/reader.php?cid=$arr[cid]&offer_id=$arr[offer_id]' rel='facebox'>távozott</a>]";
				else
					$arrivedlink = '';
			}
			else
				$notify = "";
				
			if($arr[user_invoice_ask] <> '' && $CURUSER[userclass] >= 90)
				$asklink = " [<a href=\"invoices/vatera/".str_replace("/","_",$arr[user_invoice_ask]).".pdf\" target='_blank'><b>díjbekérő</b></a>]";
			
			if($arr[reseller_id] == 3404)
				$lira = "&lira=1";
			elseif($arr[reseller_id] == 3415)
				$lira = "&lira=2";
			else
				$lira = '';
				
			
			if($offerArr[special_voucher] == 1)
				$plink = "<a href=\"vouchers/print.php?cid=$arr[cid]&paid=$arr[paid]$lira\" rel='facebox'><b>[EGYEDI]</b></a>";
			else
				$plink = "[<a href=\"vouchers/print.php?cid=$arr[cid]&paid=$arr[paid]$lira\" rel='facebox'>$lang[print]</a>]<br/> [<a href=\"vouchers/online.php?cid=$arr[cid]&email=$arr[email]&realID=$arr[offer_id]&paid=$arr[paid]\" rel='facebox'>$lang[o_online]</a>]";
				
				$print = "$plink
						
						$notify
						 $asklink
						 $clearancelink
						 $arrivedlink
						 ";
			
				$row.=  "<td>$print</td>";
			
			$post_invoice ="";
			if($arr[post_invoice_number] <> "") {
				if($month <10) {
					$zeroMonth = "0".$month;
				}
				else
					$zeroMonth = $month;
				
				$post_invoice = "<a href=\"invoices/vatera/".str_replace("/","_",$arr[post_invoice_number]).".pdf\" target=\"_blank\">$arr[post_invoice_number]</a>";
				
			}
		
			if($CURUSER[userclass] > 50) 
				$row.=  "<td align='right'>$arr[post_fee] Ft $post_invoice</td>";
				
			if($arr[user_invoice_number] <> "") {
				$date = explode("-",$arr[user_invoice_date]);
				$year = $date[0];
				$month = $date[1];
				
				if($CURUSER[userclass] > 50)
					$companyinvoice = "<a href=\"invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf\" target=\"_blank\"  class='nobr'>".str_replace("M2012/",'',$arr[invoice_number])."</a>";
				else
					$companyinvoice = "";
					
					$row.=  "<td align='right'>
						<a href=\"invoices/vatera/".str_replace("/","_",$arr[user_invoice_number]).".pdf\" target=\"_blank\"  class='nobr'>".str_replace("M2012/",'',$arr[user_invoice_number])."</a> 
						<a href=\"invoices/vatera/".str_replace("/","_",$arr[user_final_invoice_number]).".pdf\" target=\"_blank\"  class='nobr'>".str_replace("M2012/",'',$arr[user_final_invoice_number])."</a> 
						$companyinvoice</td>";
			}
			elseif($arr[invoice_created] == 1)
			{
				$companyinvoice = "<a href=\"invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf\" target=\"_blank\"  class='nobr'>".str_replace("M2012/",'',$arr[invoice_number])."</a>";
				$row.=  "<td align='center'>$companyinvoice</td>";
				
			}
			else {
				$row.=  "<td align='center'>-</td>"; //<a href=\"invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf\" target=\"_blank\" style='color:#c4c4c4' class='nobr'>".str_replace("M2012/",'',$arr[invoice_number])."</a>
			}
			
			$row.=  "<td>$arr[comment]</td>";
			$row.=  "<td  class='sumbox'><input type='checkbox'  class='sumvalue' value='$arr[orig_price]' printid='$arr[cid]'/></td>";
	}
	$row.=  "</tr>";
	if($CURUSER["userclass"] >= 5) {
		$row.=  "</form>";
	}
	
	if($CURUSER[userclass] < 5 && strtotime($offerArr[expiration_date]) < time() && $arr[invoice_created] == 0 && $arr[is_qr] == 1)
	{
		
	}
	else {
		$rowdata.=$row;
	}
}

if($rowdata == '')
{
	echo "</table><div class='redmessage'>$lang[no_results]</div>";
	foot();
	die;
}
else{
echo $rowdata;
echo "</table>";
}
/* end of pagination */
if($s == '')
{
	echo "<hr/>";
	for($g=1;$g<=$last;$g++)
 	{
 	$e = $g*$perpage;
 	$st = $e-($perpage-1);
 	if($pg == $g)
 		$bold = "class='bold'";
 	elseif($pg == '' && $g==1)
 	{
 		$bold = "class='bold'";
 	}
	else
 		$bold = '';
 	echo "<a href=\"?year=$year&month=$month&type=$filter&page=$g\" $bold>$st - $e</a> &raquo; ";
}
/* end of pagination */
 }
?>
</div></div>
<?
foot();
?>