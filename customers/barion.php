<?
header('Content-type: text/html; charset=utf-8');

require_once '../inc/barion/BarionClient.php';


$cart = $_REQUEST[cart];
$paid = $_REQUEST[value];

$customer = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE md5(offer_id) = '$cart' AND paid <> 1 LIMIT 1"));

if($customer[cid] > 0)
{
	writelog("BARION NEW TRANSACTION: " . implode("|", $_REQUEST));

	$company = getCompany($customer[company_invoice], 'single');

	$myPosKey = $company[barion_key]; // <-- Replace this with your POSKey!
	//$myPosKey = "322b746e107f483db9aa8c1aba445e3c";
	$paymentId = $customer[barion_txid]; // <-- Replace this with the ID of the payment!

	// Barion Client that connects to the TEST environment
	$BC = new BarionClient($myPosKey, 2, BarionEnvironment::Prod, true);
	
	// send the request
	$paymentDetails = $BC->GetPaymentState($paymentId);
	
	
	$_REQUEST[result] = $paymentDetails->Status;

	$mysql->query("UPDATE customers SET barion_status = '$_REQUEST[result]' WHERE cid = '$customer[cid]'");


if($_REQUEST[result] == 'Failed')
{
		
	$status = 'Sikertelen fizetés!';
	$msg = "<div style='text-align:center;'>
	
	Sikertelen tranzakció! <br/><br/>
	
	<table border='0' cellpadding='0' cellspacing='0' style='text-align:left;margin:0 auto;width:280px'>
	<tr><td><b>Utalványszám:</b></td><td align='right'>$customer[offer_id]</td></tr>
	<tr><td><b>Terhelni kívánt összeg:</b></td><td align='right'>".formatPrice($customer[orig_price]-$customer[discount_value])."</td></tr>
	<tr><td><b>Tranzakció azonosító:</b></td><td align='right'>$customer[barion_txid]</td></tr>
	</table><br/><br/>
	Kérjük, ellenőrizze a tranzakció során megadott adatok helyességét.<br/>
Amennyiben minden adatot helyesen adott meg, a visszautasítás okának kivizsgálása kapcsán kérjük, szíveskedjen kapcsolatba lépni kártyakibocsátó bankjával.<br/><br/>


	<a href='http://admin.indulhatunk.info/customers/$customer[hash]/$customer[cid]'>Megpróbálom újra a tranzakciót! &raquo;</a>
	</div>
	";
	
	$letterBody = "A $_POST[cart_id] számon megadott, ".formatPrice($_POST[amount])." összegű vásárlását a bank visszautasította.<br/><br/>
		A bank válasza: <br/><br/>$_REQUEST[trantext]";

//	sendEmail("Indul6unk: PayU sikertelen fizetés $_POST[payrefno]","$letterBody","mail@isuperg.com","Vásárlónk");
	sendEmail("Szállás Outlet sikertelen fizetés $_POST[payrefno]","$letterBody",$customer[email],"Vásárlónk");

	//$customerpe
}
elseif($_REQUEST[result] == 'Canceled')
{
	$status = 'Sikertelen fizetés!';
	$msg = "Sikertelen tranzakció!</a>";
	
	$letterBody = "A $_POST[cart_id] számon megadott, ".formatPrice($_POST[amount])." összegű vásárlását a bank visszautasította.";
//	sendEmail("Indul6unk: PayU sikertelen fizetés","$letterBody","mail@isuperg.com","Vásárlónk");
	//sendEmail("Indul6unk: PayU sikertelen fizetés","$letterBody",$customer[email],"Vásárlónk");


}
elseif($_REQUEST[result] == 'Succeeded')
{
	$pdata = $customer[orig_price]-$customer[discount_value];
	$mysql->query("UPDATE customers SET szep_successful = '1', szep_paid_date = NOW(), szep_paid = '$pdata', paid_date = NOW(), paid = 1, inactive = 0, barion_status = '$_REQUEST[result]' WHERE offer_id = '$customer[offer_id]' AND paid <> 1");
	
	$status = "Sikeres fizetés!";
	
	if($customer[sub_pid] > 0)
		$partnerInfo = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $customer[sub_pid]"));
	else
		$partnerInfo = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $customer[pid]"));
		
		
	$paid = $_REQUEST[amount];
	$topay = $customer[orig_price];
	
	$left = $topay - $paid;
	
	$company_name = 'Hotel Outlet Kft.';
	$company_bank = 'Erste bank: 11600006 - 00000000 - 49170520<br/>Nemzetközi bankszámlaszámunk: HU63 11600006 - 00000000 - 49170520 SWIFT: GIBA HU HB';
	
	

		
		$extramsg = "$extrashipmentlong Foglalási szándékát kérjük az alábbi telefonon jelezze:<br/><br/>

<b>$partnerInfo[hotel_name]</b><br/>
$partnerInfo[zip] $partnerInfo[city] $partnerInfo[address]<br/>
$partnerInfo[reception_phone]<br/>
<a href='mailto:$partnerInfo[reception_email]'>$partnerInfo[reception_email]</a>
</b><br/><br/>

Foglalásnál kérjük hivatkozzon az alábbi utalványszámra: <b>$customer[offer_id]</b><br/><br/>";
			


	$msg = "<div class='offertitle' style='margin:0 0 10px 0'>Sikeres fizetés, köszönjük!</div>
	
	<table border='0' cellpadding='0' cellspacing='0' style='text-align:left;margin:0 auto;width:280px'>
	
	<tr><td><b>Utalványszám:</b></td><td align='right'>$customer[offer_id]</td></tr>

	<tr><td><b>Leemelt összeg:</b></td><td align='right'>".formatPrice($customer[orig_price]-$customer[discount_value])."</td></tr>
	
		<tr><td><b>Tranzakció dátuma: </b></td><td align='right'>$customer[paid_date]</td></tr>
		
		<tr><td><b>Tranzakció azonosító:</b></td><td align='right'>$customer[barion_txid]</td></tr>
	</table>
	
	<div class='offertitle' style='margin:10px 0 10px 0'>További teendők:</div>
	
	$extramsg
	
	
	";
	
			
	$letterBody = "A $customer[offer_id] számon megadott, ".formatPrice($customer[orig_price])." összegű vásárlását a bank visszaigazolta.<br/><br/>
		<center><b>A tranzakció adatai:</b></center><br/><br/>
		
	<table border='0' cellpadding='0' cellspacing='0' style='text-align:left;margin:0 auto;width:280px'>
	
	<tr><td><b>Utalványszám:</b></td><td align='right'>$customer[offer_id]</td></tr>

	<tr><td><b>Leemelt összeg:</b></td><td align='right'>".formatPrice($customer[orig_price]-$customer[discount_value])."</td></tr>
	
			<tr><td><b>Tranzakció dátuma: </b></td><td align='right'>$_REQUEST[date]</td></tr>

	<tr><td><b>Tranzakció azonosító: </b></td><td align='right'>$_REQUEST[payrefno]</td></tr>
		
	</table>

		
		";
	//sendEmail("Indul6unk: PayU sikeres fizetés: $customer[offer_id]!","$letterBody","mail@isuperg.com","Vásárlónk");
	//sendEmail("Indul6unk: PayU sikeres fizetés: $customer[offer_id]!","$letterBody","payu@indulhatunk.zendesk.com","Vásárlónk");

	//sendEmail("Indul6unk: PayU sikeres fizetés: $customer[offer_id]!","$letterBody","mail@isuperg.com","Vásárlónk");
	sendEmail("Szállás Outlet sikeres fizetés: $customer[offer_id]!","$letterBody",$customer[email],"Vásárlónk");


}
}
else
{
		writelog("BARION PAID TRANSACTION: " . implode("|", $_REQUEST));

}
die;


?>