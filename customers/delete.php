<?
header('Content-type: text/html; charset=utf-8');

/*
cart_id : ez azonosítja a megrendelést
- amount : a kért zsebre terhelt összeg, vagy 0, ha nem sikerült
- anum : a terhelés engedélyszáma	
- pos : a zseb POS válaszkódja	
- txid : kártya-tranzakció azonosító
- trantext : szöveges válaszkód
*/

$it = explode("/",$_GET[q]);


$editarr = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE inactive = 1 AND hash = '$it[1]' AND hash <> '' LIMIT 1"));



$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = '$editarr[offers_id]' LIMIT 1"));

if($offer[sub_partner_id] > 0)
	$offer[partner_id] = $offer[sub_partner_id];
	
$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$offer[partner_id]' LIMIT 1"));


$reason = array(
				"",
				"Véletlenül rendelte meg",
				"Nem kaptam meg a vásárlás véglegesítése linket",
				"Nem tudtam elérni a szállodát",
				"Dupla rendelés",
				"Nem volt hely a kért időpontban",
				"Betegség miatt",
				"Váratlan kiadás miatt",
				"Nem kaptam szabadságot",
				"Egyéb családi ok miatt",
				"Egyéb, nem kívánom részletezni");
$service = array("",
				"A kiszolgálás megfelelő volt!",
				"Az ügyintézők gyorsan és pontosan válaszoltak!",
				"Nem tudtam elérni az irodát telefonon",
				"Nem tudtam elérni az irodát e-mailben",
				"Nem kaptam meg a választ időben",
				"Túl kevés, vagy téves információt kaptam");
				




head_customers();
?>
<?
echo "<div id='leftSide'>";



if($editarr[cid] == 0)
{
	echo '<div class="note">
	<div class="welcome">Az utalvány nem található!</div>
	Kérjük jelezze kollégáinknak a hibát!
	</div>
';
}
elseif($_POST[hash] <> '')
{
    
    
	$customer = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE cid = '$_POST[id]' LIMIT 1"));
	$mysql->query("UPDATE customers SET delete_reason = '$_POST[delete_reason]', delete_comment = '$_POST[delete_comment]',  delete_service = '$_POST[delete_service]' WHERE hash = '$_POST[hash]' AND cid = '$_POST[id]' AND inactive = 1");
	
	writelog("$_POST[id] delete comment has been added");
	
	
	$message = "
	
		<b>Név:</b> $customer[name]<br/>
		<b>Utalvány:</b> $customer[offer_id]<br/>
		<b>Törlés oka:</b> ".$reason[$_POST[delete_reason]]."<br/>
		<b>Kiszolgálás:</b> ".$service[$_POST[delete_service]]."<br/>
		<b>Megjegyzés:</b> <br/><br/>
		$_POST[delete_comment]

";
			
	sendEmail("Szállásoutlet.hu törlési indok - $partner[hotel_name]",$message,'info@indulhatunk.hu',"Adminisztrátor");
	//sendEmail("Szállásoutlet.hu törlési indok - $partner[hotel_name]",$message,'mail@isuperg.com',"Adminisztrátor");



	echo '<div class="note">
	<div class="welcome">Köszönjük visszajelzését!</div>
	A véleménye fontos számunkra!
	</div>';
}
elseif($editarr[delete_comment] <> '')
{
	echo '<div class="note">
	<div class="welcome">Köszönjük!</div>
	Ön már visszajelzett számunkra!
	</div>';
}
else
{
?>

<div class="note">
	<div class="welcome">Kedves <?=$editarr[name]?>!</div>
	Kérjük segítse munkánkat az alábbi űrlap kitöltésével. Köszönjük!
</div>


<?
echo '<div class="leftTop">Kedves Vásárlónk!</div>
<div class="leftItem alert">
<form method="POST" id="serviceform">
	<input type="hidden" name="hash" value="'.$editarr[hash].'"/>
	<input type="hidden" name="id" value="'.$editarr[cid].'"/>

		<ul>
			<li><label>Utalványszám:</label> '.$editarr[offer_id].' <div class="cleaner"></div></li>
			<li><label>Ajánlat:</label> <b>'.$partner[hotel_name].'</b><br/>'.$offer[name].' <br/><br/> <div class="cleaner"></div></li>
			<li  style="height:35px"><label>Visszalépés indoka:</label>
			
			<select name="delete_reason" style="width:350px" id="reason">
				<option value="0">Kérjük válasszon!</option>
				<option value="1">Véletlenül rendelte meg</option>
				<option value="2">Nem kaptam meg a vásárlás véglegesítése linket</option>
				<option value="3">Nem tudtam elérni a szállodát</option>
				<option value="4">Dupla rendelés</option>
				<option value="5">Nem volt hely a kért időpontban</option>
				<option value="6">Betegség miatt</option>
				<option value="7">Váratlan kiadás miatt</option>
				<option value="8">Nem kaptam szabadságot</option>
				<option value="9">Egyéb családi ok miatt</option>
				<option value="10">Egyéb, nem kívánom részletezni</option>
			</select>
			</li>
			<li style="height:35px"><label>Kiszolgálás minősége?</label>
			
			<select name="delete_service" style="width:350px" id="service">
				<option value="0">Kérjük válasszon!</option>
				<option value="1">A kiszolgálás megfelelő volt!</option>
				<option value="2">Az ügyintézők gyorsan és pontosan válaszoltak!</option>
				<option value="3">Nem tudtam elérni az irodát telefonon</option>
				<option value="4">Nem tudtam elérni az irodát e-mailben</option>
				<option value="5">Nem kaptam meg a választ időben</option>
				<option value="6">Túl kevés, vagy téves információt kaptam</option>
			</select>
			</li>
		<li><label>Kérjük írja le véleményét:</label> <textarea name="delete_comment" style="width:370px;height:60px;border:1px solid #cfcfcf; font-size:14px; border-radius:5px; background-color:#fafafa; width:570px; margin:10px 0 0 0;padding:5px;"></textarea></li>
		<li><center><input type="submit" id="delsubmit" value="Mentés"/></center></li>
		</ul>
</form>';


echo'<div class="cleaner"></div>
</div>
<div class="leftBottom"></div>';
}
	
	
echo "</div><div id='rightside'>";

foot_customers(0,1,$offer['company_invoice']);
?>