<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER[userclass] < 40)
	header("location: index.php");
	
if($CURUSER[userclass] >= 40)
{
	$extrapid = "AND partner_id = '$_GET[pid]'";
	$pid = $_GET[pid];
}
else
{
	$extrapid = "AND partner_id = '$CURUSER[coredb_id]'";
	$cpid = $extrapid;
	$pid = $CURUSER[coredb_id];
}


if($_GET[deleteprice] > 0 && $_GET[sure] <> 1)
{

	die("
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan törölni szeretné az árat?</h2> <br/> <a href=\"?editid=$_GET[editid]&pid=$_GET[pid]&add=1&deleteprice=$_GET[deleteprice]&sure=1\" class='button green'>$lang[yes]!</a>  <a href=\"customers.php\" class='button red closefacebox'>$lang[no]!</a><div class='cleaner'></div>");
	
}
if($_GET[deleteprice] > 0 || $_GET[sure] == 1)
{
	$oid = (int)$_GET[editid];
	$partid = (int)$_GET[pid];
	
	$partner = mysql_fetch_assoc($mysql_tour->query("SELECT partner_id FROM accomodation WHERE id = '$oid' AND partner_id = '$partid' LIMIT 1"));
	
	if($CURUSER[userclass] < 90 && $partner[partner_id] <> $CURUSER[coredb_id])
	{
		writelog("$CURUSER[username] INVALID PRICE DELETE ATTEMPT!");
		die("Hiba");
	}
	else
	{
		$dp = (int)$_GET[deleteprice];
		$mysql_tour->query("DELETE FROM prices WHERE accomodation = 1 AND id = $dp");
		$msg = 'Sikeresen törölte az árat';
		
		writelog("$CURUSER[username] deleted price $_GET[deleteprice]");
	}
}

if($_GET[prices] == 1 )
{	
	$dt[from_date] = $_POST[from_date];
	$dt[to_date] = $_POST[to_date];
	$dt[description] = $_POST[description];
	$dt[offer_id] = $_POST[offer_id];
	$dt[price] = $_POST[price];
	$dt[day_num] = $_POST[day_num];
	$dt[accomodation] =1;
	$dt[added] = "NOW()";
	
	$_POST[editprice] = (int)$_POST[editprice] ;
	if($_POST[editprice] > 0)
	{
		$msg = 'Az ár sikeresen módosítva!';
		$mysql_tour->query_update("prices",$dt,"id=$_POST[editprice]");
		writelog("$CURUSER[username] EDITED price $dt[offer_id]");

	}
	else
	{
		
		$mysql_tour->query_insert("prices",$dt);
		writelog("$CURUSER[username] ADDED price $dt[offer_id]");
		
		$msg = 'Az ár sikeresen rögzítve!';
	}
}
if(!empty($_POST) && $_POST[editform] == 1)
{
	//$_POST[DESCRIPTION] =("'","",$_POST[DESCRIPTION]);
	//print_r($_POST[DESCRIPTION]);
	//validatePost($_POST);
		
	if($_POST[id] > 0)
	{
	
//	$_POST[DESCRIPTION] = stripslashes(($_POST[DESCRIPTION]));
	$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = '$_POST[partner_id]'"));
	
	unset($_POST[editform]);
 		
 		$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = '$partner[city_id]'"));
		$page = clean_url_indulhatunk($_POST[name]);
		$subdomain = clean_url_indulhatunk($city[name]);
		$_POST[page_url] = "http://szallas-$subdomain.indulhatunk.hu/".$page."-$_POST[id]";
		$_POST[special_url] = clean_url_specialchar($_POST[name]);
 		$stid = $mysql_tour->query_update("accomodation",$_POST, "id=$_POST[id] $cpid");

	
		writelog("$CURUSER[username] edited an OCI offer #$_POST[id]");

	$msg =  message("Sikeresen szerkesztette az ajánlatot!");
	}
	else
	{
	
		$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM accomodation WHERE partner_id = '$_POST[partner_id]' AND package = 0 LIMIT 1"));
		
	
 		
 			$ins[partner_id] = $_POST[partner_id];
 			$ins[name] = $_POST[name];
 			$ins[aktiv] = 1;
 			$ins[country_id] = 125; 
 			$ins[region_id] = $partner[region_id]; 
 			$ins[city_id] = $partner[city_id];  
 			$ins[description] = $_POST[description];  
 			$ins[category] = $_POST[category];  
 			$ins[address] = $_POST[address];  
 			$ins[email] = $_POST[email];  
 			$ins[from_date] = $_POST[from_date]; 
  			$ins[expiration_date] = $_POST[expiration_date]; 
 			$ins[package] = 1;  
 			$ins[catering] = $_POST[catering]; 
 			$ins[enabled_days] = $_POST[enabled_days]; 
 			$ins[package_orig_offer_id] = $partner[id];  
 			$ins[day_num] = $_POST[day_num]; 
 			$ins[special_day] = $_POST[special_day]; 
 			$ins[plus_day] = $_POST[plus_day]; 
 			$ins[create_date] = 'NOW()';

		$id = $mysql_tour->query_insert("accomodation",$ins);
 		
		//$id = mysql_insert_id();
		
		
		$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = '$partner[city_id]'"));
		$page = clean_url_indulhatunk($_POST[name]);
		$subdomain = clean_url_indulhatunk($city[name]);
		$url = "http://szallas-$subdomain.indulhatunk.hu/".$page."-$id";
		$special_url = clean_url_specialchar($_POST[name]);
		$mysql_tour->query("UPDATE accomodation SET  page_url = '$url', special_url = '$special_url' WHERE id = $id");


 		 	
 		writelog("$CURUSER[username] added an accomodation offer $id");
  				
		
	//	print_r($editdata);
		$editdata = $editdata[max];
		header("location: ../editoffers.php?editid=$id&pid=$partner[partner_id]&add=1&edited=1");
		die;	
		
	}
}

head("Ajánatok szerkesztése");

?>



<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
	
	<? if($CURUSER[userclass] > 90) { ?>
			<li><a href='accomodation.php'>Szálláshelyek</a></li>
	<? } ?>
					
		<li><a href='editoffers.php?pid=<?=$pid?>' class='<? if($_GET[add] <> 1 && $_GET[active] == 0) echo "current";?>'>Aktív ajánlatok</a></li>
		<li><a href='?active=1&pid=<?=$pid?>' class='<? if($_GET[active] == 1) echo "current";?>'>Lejárt ajánlatok</a></li>
	</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>
<?

if($_GET[edited] == 1)
	$msg = "Sikeresen létrehozta az ajánlatot!";
	
$id = (int)$_GET[editid];

$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = '$_GET[pid]' LIMIT 1"));


if($id > 0)
{
	$editdata = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM accomodation WHERE id = '$id' $extrapid LIMIT 1"));
	

}
else
{
	$editdata[expiration_date] = date("Y")+1 ."-".date("m-d");
}

if($editdata[day_num] == '' && $editdata[id] == '')
	$editdata[day_num] = 2;

echo message($msg);

//page url
//special url
if($_GET[add] == 1)
{

?>

<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
$().ready(function() {
	$("#day_num").change(function () {
		var daynum = $(this).val()*1+1;
		
		if(daynum < 0)
			daynum = 0;
			
		$("#realnight").html(daynum);
		
		return false;
    });
    
    $(".changepackage").keyup(function () {
    	var daynum = <?=$editdata[day_num]?>*1;
		var thisid = $(this).attr('id');
		var thisval = Math.round($(this).val()*1/daynum/2);
		$('#pernight-'+thisid).html(thisval);
		//alert(thisid);
	
    });
});

	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			height:500,
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect,outdent,indent,blockquote,|,undo,redo,|,bullist,numlist,|",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
			  width : "745",
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>
<? if($_GET[hidethis] <> 1) { ?>

<script>

$(document).ready(function() {

$("#checkoffer").click(function() {

	var error = 0;
	if($("#name").val() == '')
	{
		$("#name").addClass('invalid');
		error = 1;
	}
	if($("#templateBody").val() == '')
	{
		$("#templateBody").addClass('invalid');
		error = 1;
	}
	if($("#day_num").val() == '0' || $("#day_num").val() == '')
	{
		$("#day_num").addClass('invalid');
		error = 1;
	}
		
	if(error == 1)	
		return false;
	else
		return true;
});

$("#checkprice").click(function() {

	var error = 0;
	if($("#p_desc").val() == '')
	{
		$("#p_desc").addClass('invalid');
		error = 1;
	}
	if($("#p_from").val() == '')
	{
		$("#p_from").addClass('invalid');
		error = 1;
	}
	if($("#p_to").val() == '')
	{
		$("#p_to").addClass('invalid');
		error = 1;
	}
	if($("#p_day").val() == '')
	{
		$("#p_day").addClass('invalid');
		error = 1;
	}
	if($("#p_price").val() == '')
	{
		$("#p_price").addClass('invalid');
		error = 1;
	}
		
	if(error == 1)	
		return false;
	else
		return true;
});

$(".editprice").click(function() {
	var priceid = $(this).attr('priceid');
	
	$(".allprice").show();
	$("#pricerow-"+priceid).hide();
	
	$("#p_desc").val($("#description-"+priceid).val());
	$("#p_from").val($("#from_date-"+priceid).val());
	$("#p_to").val($("#to_date-"+priceid).val());
	$("#p_day").val($("#day_num-"+priceid).val());
	$("#p_price").val($("#price-"+priceid).val());
	$("#editprice").val(priceid);
		
		
	
	return false;

});





});

</script>



	<form method='POST' action='editoffers.php?add=1&editid=<?=$id?>&pid=<?=$partner[id]?>'>
	<input type='hidden' name='editform' value='1'/>
	<fieldset id='editform'>
		<legend>Ajánlat szerkesztése</legend>
		<input type='hidden' name='id' value='<?=$id?>'>
		<input type='hidden' name='partner_id' value='<?=$partner[id]?>'>

		<ul>		
		<li><label>Partner neve:</label> <?=$partner[name]?> </li>
		<li><label>Csomag neve:</label>  <input type='text' name='name' value='<?=$editdata[name]?>' class='longinput' id='name'/> </li>
		<? if($CURUSER[userclass] > 50) { ?>
		<li><label>Csomagajánlat?</label>
			<select name='package' >
				<option value='1' <? if($editdata[package] == 1) echo "selected"?>>csomag</option>
				<? if($_GET[editid] <> '') { ?>
				<option value='0' <? if($editdata[package] == 0) echo "selected"?>>főajánlat</option>
				<? } ?>
			</select>
		</li>
		<?/* for($i=1;$i<=21;$i++) { ?>
			<li><label>LMB <?=$i?>. nap:</label>  <input type='text' name='lmb_day<?=$i?>' value='<?=$editdata["lmb_day".$i]?>' class='numeric'/>% </li>

		<? }*/ ?>
		<? } ?>
		<li><label>Aktív:</label>
			<select name='aktiv' id='aktiv'>
			<option value='0' <? if($editdata[aktiv] == 0) echo "selected"?>>Nem</option>
			<option value='1' <? if($editdata[aktiv] == 1) echo "selected"?>>Igen</option>
			</select>
		</li>

		<li><label>Ajánlat típusa:</label>
			<select name='enabled_days' id='enabled_days'>
			<option value='0' <? if($editdata[enabled_days] == 0) echo "selected"?>>Válasszon</option>
			<option value='2' <? if($editdata[enabled_days] == 2) echo "selected"?>>hétvégi ajánlat</option>
			<option value='3' <? if($editdata[enabled_days] == 3) echo "selected"?>>hétköznapi ajánlat</option>
			<option value='1' <? if($editdata[enabled_days] == 1) echo "selected"?>>ünnepi ajánlat</option>
			<option value='4' <? if($editdata[enabled_days] == 4) echo "selected"?>>általános ajánlat</option>
			</select> (Mostutazz letiltáshoz: Válasszon)
		</li>
		<li><label>Ellátás típusa:</label>
			<select name='catering'>
			<option value='0' <? if($editdata[catering] == 0) echo "selected"?>>Válasszon</option>
			<option value='1' <? if($editdata[catering] == 1) echo "selected"?>>reggeli</option>
			<option value='2' <? if($editdata[catering] == 2) echo "selected"?>>félpanzió</option>
			<option value='3' <? if($editdata[catering] == 3) echo "selected"?>>teljes ellátás</option>
			<option value='4' <? if($editdata[catering] == 4) echo "selected"?>>nincs ellátás</option>
		</select>
		</li>
		<li><label>Érvényesség kezdete:</label> <input type='text' name='from_date' value='<? if($editdata[from_date] == '') echo date("Y-m-d"); else echo $editdata[from_date];?>'  class='maskeddate' style='width:80px;'/></li>
		<li><label>Érvényesség vége:</label> <input type='text' name='expiration_date' value='<?=$editdata[expiration_date]?>'  class='maskeddate' style='width:80px;'/></li>
		<li><label>Csomag időtartama:</label><input type='text' name='day_num' id="day_num" value="<?=$editdata[day_num]?>" class='numeric'/> éj / <span id='realnight'><? if($editdata[day_num] >= 1) echo $editdata[day_num]+1; else echo "0";?></span> nap</li>
		<li><label>Kiemelt időszak? </label> <select name='special_day'>
			<option value='0' <? if($editdata[special_day] == 0) echo "selected"?>>A csomag kiemelt időszakban NEM érvényes</option>
			<option value='1' <? if($editdata[special_day] == 1) echo "selected"?>>A csomag kiemelt időszakban érvényes</option></select></li>
		

<li><label>Plusz éjszakák </label> <select name='plus_day'>
	<option value='0' <? if($editdata[plus_day] == 0) echo "selected"?>>A csomagot plusz éjszakákkal NEM lehet foglalni</option>
		<option value='1' <? if($editdata[plus_day] == 1) echo "selected"?>>A csomagot 1 plusz éjszakával lehet foglalni</option>
		<option value='2' <? if($editdata[plus_day] == 2) echo "selected"?>>A csomagot 2 plusz éjszakával lehet foglalni</option>
		<option value='3' <? if($editdata[plus_day] == 3) echo "selected"?>>A csomagot 3 plusz éjszakával lehet foglalni</option>
		<option value='4' <? if($editdata[plus_day] == 4) echo "selected"?>>A csomagot 4 plusz éjszakával lehet foglalni</option>
		<option value='5' <? if($editdata[plus_day] == 5) echo "selected"?>>A csomagot 5 plusz éjszakával lehet foglalni</option>
		<option value='99' <? if($editdata[plus_day] == 99) echo "selected"?>>A csomagot végtelen számú plusz éjszakával lehet foglalni</option>

	</select>
</li>




			<li><label>Ajánlat leírása:</label> <br/><br/>
			
			<textarea name="description" rows="10"  style="width: 500px" class="tinymce" id="templateBody"><? if($editdata[description] <> '') { echo $editdata[description]; }?></textarea></li>
			
		<li><label>URL:</label>  <input type='text' name='page_url' value='<?=$editdata[page_url]?>' class='longinput'/> </li>
		<li><label>Speciális URL:</label>  <input type='text' name='special_url' value='<?=$editdata[special_url]?>' class='longinput'/> </li>

		<li><center><input type="submit" id='checkoffer' value="<? if($_GET[editid] == 0) echo "Tovább az árak rögzítéséhez"; else echo "Mentés";?> &raquo;" /></center></li>
		
		</ul>	
		
	</fieldset>
	</form>
	<? } ?>
	<? if($id > 0) { ?>
		<form method='post' action="editoffers.php?prices=1&editid=<?=$id?>&pid=<?=$partner[id]?>&add=1">
		<input type='hidden' name='offer_id' value='<?=$id?>'>

	<fieldset id='editform'>
		<legend>Árak szerkesztése</legend>
		<table width="740">
			<tr class='header'>
				<td>Megnevezés</td>
				<td>Érv. kezdete</td>
				<td>Érv. vége</td>
				<td>Éj</td>
				<td>Megjegyzés</td>
				<td>#</td>
			</tr>
			<?			
				$price = $mysql_tour->query("SELECT * FROM prices WHERE offer_id = '$id' AND accomodation = 1");
		
			while($list = mysql_fetch_assoc($price))
			{
			
					echo "<tr id='pricerow-$list[id]' class='allprice'>";
					echo "<td width='150'>$list[description]</td>";
					echo "<td align='center' width='100'>".format_date($list[from_date])."</td>";
					echo "<td align='center' width='100'>".format_date($list[to_date])."</td>";
					echo "<td align='right'>$list[day_num] éj</td>";
					echo "<td align='right'>".formatPrice($list[price])." / fő</td>";
					echo "<td align='center' width='30'>
					
					<input type='hidden' id='description-$list[id]' value='$list[description]'/>
					<input type='hidden' id='from_date-$list[id]' value='$list[from_date]'/>
					<input type='hidden' id='to_date-$list[id]' value='$list[to_date]'/>
					<input type='hidden' id='price-$list[id]' value='$list[price]'/>
					<input type='hidden' id='day_num-$list[id]' value='$list[day_num]'/>
					
					<a href='#' class='editprice' priceid='$list[id]'><img src='/images/edit.png' width='15'/></a>
					<a href='?editid=$id&pid=$partner[id]&add=1&deleteprice=$list[id]' rel='facebox'><img src='/images/trash.png' width='15'/></a></td>";
					
					echo "</tr>";
			}
			
			$dn =  $editdata[day_num];
			echo "<tr>";
				echo "<input type='hidden' name='editprice' id='editprice' value=''/>";
					echo "<td width='150'><input type='text' name='description' id='p_desc' placeholder='pl. 2 éj/fő félpanzióval'></td>";
					echo "<td align='center' width='150'><input type='text' id='p_from' name='from_date' class='maskeddate' placeholder='érv. kezdete'></td>";
					echo "<td align='center' width='150'><input type='text'  id='p_to' name='to_date' class='maskeddate' placeholder='érv. vége'></td>";
					echo "<td align='right'><input type='text' name='day_num'  id='p_day' class='numeric' style='width:10px;'  placeholder='' value='$dn'> éj</td>";
					echo "<td align='right'><input type='text' name='price'  id='p_price' class='numeric'  placeholder='ára' style='width:50px'> Ft / fő</td>";
					echo "<td align='center' width='20'><input type='submit' id='checkprice' value='Ok'/></td>";
					echo "</tr>";
					
			?>
		</table>
		</form>
	</fieldset>
	</form>
	<? } ?>


</div></div>


<?

foot();
die;
} ?>


<?

$origPartnerData = $mysql_tour->query("SELECT * FROM accomodation WHERE id > 0 $extrapid AND (package IS NULL OR package = 0)");

$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM accomodation WHERE id > 0 $extrapid AND package = 0 LIMIT 1"));
?>
<? if($partner[id] > 0) { ?>
	<fieldset>
		<legend>Eszköztár</legend>
		<table style='text-align:center;'>
			<tr>
				<td colspan='5' align='center' style='font-weight:bold;'><?=$partner[name]?> csomagajánlatai</td>
			</tr>
			<tr>
				<td><a href='accomodation.php?id=<?=$partner[partner_id]?>&add=1'>[partner szerkesztése]</a></td>
				<td class='header'><a href='editoffers.php?pid=<?=$partner[partner_id]?>'>[csomagok]</a></td>
				<td><a href='rooms.php?pid=<?=$partner[partner_id]?>'>[szobatípusok]</a></td>
				<td><a href='partner-domain.php?pid=<?=$partner[partner_id]?>'>[domainek]</a></td>
				<td><a href='reviews.php?pid=<?=$partner[partner_id]?>'>[értékelések]</a></td>
			</tr>
		</table>
	</fieldset>
	<hr/>
<? } ?>

<?
echo "<table style='margin:0 auto;'>";

echo "<tr class='header'><td colspan='6'><a href='?add=1&pid=$pid'>Új csomagajánlat</a></td></tr>";



if($_GET[active] == 0)
{

echo "<tr class='header'>";
	echo "<td>Ajánlat neve</td>";
	echo "<td>Lejárati dátum</td>";
	echo "<td>Legalacsonyabb ár</td>";
	echo "<td width='50'>Szerkesztés</td>";
//	echo "<td>Árak</td>";
	echo "<td width='50'>Előnézet</td>";
	echo "<td width='20'>Aktív</td>";
echo "</tr>";
echo "<tr class='header'><td colspan='7'>Alap megjelenés</td>";


while($data = mysql_fetch_assoc($origPartnerData))
{
	echo "<tr>";
	echo "<td>Alap megjelenés</td>";
	echo "<td align='center'>-</td>";// ".date("Y.m.d.",strtotime($data[expiration_date]))."
	
	$prices = mysql_fetch_assoc($mysql_tour->query("SELECT price FROM prices WHERE offer_id = '$data[id]' and to_date >= NOW() ORDER BY price ASC"));

	echo "<td align='center'>-</td>"; //".formatPrice($prices[0][PRICE])."

	echo "<td><a href='?editid=$data[id]&add=1&pid=$data[partner_id]'>[szerkesztés]</a></td>";
	//echo "<td>[árak]</td>";
	
	$id = $data[id]*2;
	echo "<td><a href='$data[page_url]' target='_blank'>[előnézet]</a></td>";
		
	if($data[aktiv] == 1)
		$active = '<img src="/images/check1.png" width="25"/>';
	else
		$active = '<img src="/images/cross1.png" width="25"/>';
	echo "<td>$active</td>";
	echo "</tr>";
}
}

if($_GET[active] == 1)
{
	$act = 'AND aktiv = 0';
}
else
{
	$act = 'AND aktiv = 1'; 
}

echo "<tr class='header'><td colspan='7'>Csomagajánlatok</td>";
$partnerdata = $mysql_tour->query("SELECT * FROM accomodation WHERE id > 0 $extrapid AND package = 1 $act ORDER BY expiration_date ASC");


while($data = mysql_fetch_assoc($partnerdata))
{
	if($data[aktiv] == 1)
	{
		$active = '<img src="/images/check1.png" width="25"/>';
		$class = '';
	}
	else
	{
		$active = '<img src="/images/cross1.png" width="25"/>';
		$class = 'red';
	}
		
	echo "<tr class='$class'>";
	echo "<td>$data[name]</td>";
	
	if($data[expiration_date] <> '0000-00-00')
		echo "<td align='center'>".date("Y.m.d.",strtotime($data[expiration_date]))."</td>";
	else	
		echo "<td align='center'>-</td>";

	
	$prices = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE offer_id = '$data[id]' AND accomodation = 1 AND to_date >= NOW() ORDER BY from_date ASC LIMIT 1"));

	if($prices[price] > 0)
		echo "<td align='right'>".formatPrice($prices[price])."</td>";	
	else
		echo "<td align='center'>-</td>";	

	echo "<td><a href='?editid=$data[id]&add=1&pid=$data[partner_id]'>[szerkesztés]</a></td>";
	//echo "<td>[árak]</td>";
	$id = $data[id]*2;
	echo "<td><a href='$data[page_url]' target='_blank'>[előnézet]</a></td>";
	
	
	echo "<td>$active</td>";
	
	echo "</tr>";
}
echo "</table>";
//debug($partnerdata);

echo "</div></div>";
foot();



//foot();
?>