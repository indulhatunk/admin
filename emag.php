<?php
/*
 * customers.php
 * Author: gergely.solymosi@gmail.com
 *
 */

/* bootstrap file */
$noheader = 'no';
include("inc/init.inc.php");

userlogin();
if($CURUSER[userclass] < 50)
    header("location: index.php");

$header = array(
    'vendor_name' => 'vendor_name',
    'doc_id' => 'doc_id / part_number_key',
    'vendor_ext_id' => 'vendor_ext_id',
    'part_number' => 'part_number',
    'validation_name' => 'validation_name',
    'sale_price' => 'sale_price',
    'original_sale_price' => 'original_sale_price',
    'vat_rate' => 'vat_rate',
    'currency' => 'currency',
    'start_date' => 'start_date',
    'warranty' => 'warranty',
    'availability_id' => 'availability_id',
    'stock' => 'stock',
    'handling_time' => 'handling_time',
    'status' => 'status',
    'name' => 'name',
    'brand' => 'brand',
    'short_description' => 'short_description',
    'description' => 'description',
    'url' => 'url',
    'weight' => 'weight',
    'barcode' => 'barcode',
    'main_image_url' => 'main_image_url',
    'other_image_url1' => 'other_image_url1',
    'other_image_url2' => 'other_image_url2',
    'other_image_url3' => 'other_image_url3',
    'other_image_url4' => 'other_image_url4',
    'other_image_url5' => 'other_image_url5',
    'attachment_url1' => 'attachment_url1',
    'attachment_url2' => 'attachment_url2',
    'force_download' => 'force_download',
    'tipus' => 'típus: [5637]',
    'cel' => 'cél: [5414]',
    'indulas_varosa' => 'indulás városa: [8179]',
    'indulas_datuma' => 'indulás dátuma: [8180]',
    'visszateres_datuma' => 'visszatérés dátuma: [8181]',
    'vendegejszakak_szama' => 'vendégéjszakák száma: [8182]',
    'elszallasolas_tipusa' => 'elszállásolás típusa: [8183]',
    'ingatlan_megnevezese' => 'ingatlan megnevezése: [8185]',
    'csillagok_szama' => 'csillagok száma: [8187]',
    'kamera_tipusa' => 'kamera típusa: [5290]',
    'kozlekedes' => 'közlekedés: [8189]',
    'legitarsasag' => 'légitársaság: [8192]',
    'osztaly' => 'osztály: [8194]',
    'ellatas' => 'ellátás: [8197]',
    'kapcsolodo_szolg' => 'kapcsolódó szolgáltatások: [8198]',
    'szolgaltatasok' => 'szolgáltatások: [2174]',
    'rejected' => 'rejected',
);
$search  = array(',','!','*','.','/','%!%',";","(",")", " ", "&", "á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ö", "Ö", "ő", "Ő", "ú", "Ú", "ü", "Ü", "ű", "Ű");
$replace = array("","","","","","","","","-", "-", "a", "A", "e", "E", "i", "I", "o", "O", "o", "O", "o", "O", "u", "U", "u", "U", "u", "u");

$id = "6833, 6835, 6836, 6837, 6822, 6838, 6852, 6853, 6854, 6855, 6856, 6857, 6858, 6859, 6861, 6862, 6863, 6852, 6853, 6854, 6855, 6856, 6857, 6833, 6835, 6836, 6837, 6838, 6881, 6882,6883,6884,6885,6886,6887,6888";
$id = "7185,7186,7184,7187,7188,7189,7209,7190,7191,7192,7193,7229,7205,7203,7204,7194,7195,7196,7200,7228";

$sql = "SELECT sub_partner.city,sub_partner.country,sub_partner.coredb_id,sub_partner.coredb_package, offers.id, offers.special_comment, offers.shortname, offers.name, offers.partner_id, offers.days, offers.main_description AS body, offers.actual_price, offers.normal_price, offers.plus_child1_value, offers.plus_child2_value, offers.plus_child3_value, offers.plus_room1_name, offers.plus_other1_name, offers.sub_partner_id FROM offers LEFT JOIN partners AS sub_partner ON offers.sub_partner_id = sub_partner.pid WHERE id IN ( ".$id." )";

if($_GET['s'] == "belfold") {
   $id = '6748,6778,6841,6623,6832,6894,6793,6810,6914,6890,6876,6716,6915,6909,6912,6908,6879,6877,6878,6876,6875,6872,6873,6874,6865,';   
   $id = "6890,6793,6920,6748,6915,6841,6778,6919,6832,6920,6914,6918,6894,6810,6808,6892,6790,6791,6895,6879,6877,6749,6879,6929,6720,6628,6698";
   $id .= ",6831,6776,6843,6651,6934,6819,6928,6870,6925,6917,6942,6911,6875,6872,6874,6904,6902";
   $id = "7251,7180,7206,7243,7245,7248,7233,7201,7240,7241,7183,7237,7232,7219,7236,7249,7238,7124,7235,7199,7244,7211,7247,7202,7242,7207,7182,7213,7246,7250,7210,7239";
   $sql = "SELECT sub_partner.city,sub_partner.country,sub_partner.coredb_id,sub_partner.coredb_package, offers.url, offers.id, offers.special_comment, offers.shortname, offers.name, offers.partner_id, offers.days, offers.main_description AS body, offers.actual_price, offers.normal_price, offers.plus_child1_value, offers.plus_child2_value, offers.plus_child3_value, offers.plus_room1_name, offers.plus_other1_name, offers.sub_partner_id FROM offers LEFT JOIN partners AS sub_partner ON offers.partner_id = sub_partner.pid WHERE id IN ( ".$id." )";

}
//print $sql; die(); 

$query = $mysql->query($sql);
$result = array();
while($row = mysql_fetch_assoc($query)){
    $country = explode(' - ',$row['city']);
    $params = explode(';',$row['special_comment']);
    $separator = "Az aj&aacute;nlat tartalma";
    $separator2 =  "a szállodáról";
    
    $body = explode("<strong>$separator</strong>", $row['body']);
    $body = $body[1];
    $body = preg_replace('/(<(?!img)\w+[^>]+)(style="[^"]+")([^>]*)(>)/', '${1}${3}${4}', $body);
    $body = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\>/i', '<$1$4$7>', $body);
    $body = str_replace('#PRICE#',$row['actual_price']." Ft",$body);
    $main = explode($separator2,$body);
    
    $start_date = array_map('trim', explode(',', $params[10]));
    
    $end_date = array();
    $days = 8;
    foreach($start_date as $date) {
        $date_ = strtotime("+".$days." days", strtotime($date));
        $end_date[] = date("Y-m-d", $date_);
    }
    $url = 'http://www.indulhatunk.hu/valentin-nap/'.mb_strtolower(str_replace($search, $replace, $row['shortname'])).'-'.$row['id'];
    if($_GET['s'] == "belfold") {
	$url = 'http://www.szallasoutlet.hu/'.mb_strtolower(str_replace($search, $replace,$row['city'])).'/'.mb_strtolower(str_replace($search, $replace, $row['shortname'])).'-'.$row['id'];

	}
    $result[] = array(
        'vendor_name' => "Szállás Outlet",
        'doc_id' => null, //'doc_id / part_number_key',
        'vendor_ext_id' => $row['id'], //ajánlat_id 'vendor_ext_id',
        'part_number' => '', //'part_number',
        'validation_name' => null, //'validation_name',
        'sale_price' => $row['actual_price'],//'sale_price',
        'original_sale_price' => $row['normal_price'], //'original_sale_price',
        'vat_rate' => '0',
        'currency' => 'HUF',
        'start_date' => '2016-11-25',
        'warranty' => '0',
        'availability_id' => '3',
        'stock' => null,
        'handling_time' => null,
        'status' => '1',
        'name' => $row['city'] . ' ' . $row['shortname'],
        'brand' => '',
        'short_description' => $params[0],
        'description' => $main[0],
        'url' => $url,
        'weight' => '',
        'barcode' => '',
        'main_image_url' => 'http://static.szallasoutlet.hu/uploaded_images/accomodation/'.$row['coredb_id'].'/'.$row['coredb_package'].'/orig/01.jpg',
        'other_image_url1' => 'http://static.szallasoutlet.hu/uploaded_images/accomodation/'.$row['coredb_id'].'/'.$row['coredb_package'].'/orig/01.jpg',
        'other_image_url2' => 'http://static.szallasoutlet.hu/uploaded_images/accomodation/'.$row['coredb_id'].'/'.$row['coredb_package'].'/orig/02.jpg',
        'other_image_url3' => 'http://static.szallasoutlet.hu/uploaded_images/accomodation/'.$row['coredb_id'].'/'.$row['coredb_package'].'/orig/03.jpg',
        'other_image_url4' => 'http://static.szallasoutlet.hu/uploaded_images/accomodation/'.$row['coredb_id'].'/'.$row['coredb_package'].'/orig/04.jpg',
        'other_image_url5' => 'http://static.szallasoutlet.hu/uploaded_images/accomodation/'.$row['coredb_id'].'/'.$row['coredb_package'].'/orig/05.jpg',
        'attachment_url1' => '',
        'attachment_url2' => '',
        'force_download' => '',
        'tipus' =>  ($_GET['s'] == "belfold") ? '' : 'Tengerpart',
        'cel' => $country[0],
        'indulas_varosa' =>  ($_GET['s'] == "belfold") ? '' : 'Budapest',
        'indulas_datuma' => ($_GET['s'] == "belfold") ? '' : implode("\r ",$start_date),
        'visszateres_datuma' => ($_GET['s'] == "belfold") ? '' : implode("\r ",$end_date),
        'vendegejszakak_szama' => $row['days'],
        'elszallasolas_tipusa' => 'Szálloda',
        'ingatlan_megnevezese' => $row['shortname'],
        'csillagok_szama' => '',
        'kamera_tipusa' => ($_GET['s'] == "belfold") ? '' : 'Dupla',
        'kozlekedes' => ($_GET['s'] == "belfold") ? '' : 'Repülővel',
        'legitarsasag' => '',
        'osztaly' => '',
        'ellatas' => '',
        'kapcsolodo_szolg' => '',
        'szolgaltatasok' => '',
        'rejected' => '',
    );
    
}



$path = realpath('lib/PHPExcel') . '/PHPExcel.php';
include_once($path);
$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);

$doc->getActiveSheet()  ->setTitle("checklist")
->fromArray($header, null, 'A1')
->fromArray($result, null, 'A2');

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="checklist-'.date('Ymdhis').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
$objWriter->save('php://output');
exit;

?>