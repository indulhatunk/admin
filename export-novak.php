<?
header('Content-type: text/html; charset=utf-8');
include("inc/config.inc.php");
include("inc/mysql.class.php");
include("inc/functions.inc.php");
include("inc/PHPExcel.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

$date = date("Ymd_His");

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();



// Set properties
$objPHPExcel->getProperties()->setCreator("Indulhatunk.info")
							 ->setLastModifiedBy("indulhatunk.info")
							 ->setTitle("Indulhatunk.info export")
							 ->setSubject("Indulhatunk.info export")
							 ->setDescription("Indulhatunk.info export")
							 ->setKeywords("")
							 ->setCategory("");





	

// Add some data

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Szerződés száma')
            ->setCellValue('B1', 'Kapcsolattartó neve')
            ->setCellValue('C1', 'Kapcsolattarto e-mail címe')
            ->setCellValue('D1', 'Kapcsolattartó telefonszáma')
            ->setCellValue('E1', 'Partner neve')
 			->setCellValue('F1', 'Partner cégneve')
 		    ->setCellValue('G1', 'Érték')
 		    ->setCellValue('H1', 'Mennyiség')
 		    ->setCellValue('I1', 'Voucher ID')
 		    ->setCellValue('J1', 'Lejárat');


//
$whiteQr = $mysql->query("SELECT * FROM own_vouchers WHERE licittravel_date <> '' AND name = '' ORDER BY licittravel_date ASC");

$whiteCount = 2;
while($whiteArr = mysql_fetch_assoc($whiteQr))
{
	$contractArr = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE id = $whiteArr[contract_id]"));
	$partnerArr = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partner_id = $whiteArr[partner_id]"));

			
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$whiteCount", "$whiteArr[contract_id]")
            ->setCellValue("B$whiteCount", trim($contractArr[contact]))
            ->setCellValue("C$whiteCount", trim($contractArr[email]))
            ->setCellValue("D$whiteCount", trim($contractArr[phone]))
 			->setCellValue("E$whiteCount", trim($partnerArr[hotel_name]))
 		    ->setCellValue("F$whiteCount", trim($partnerArr[company_name]))
 		    ->setCellValue("G$whiteCount", "$whiteArr[orig_price]")
 		    ->setCellValue("H$whiteCount", "$whiteArr[quantity2] $whiteArr[quantity] ")
 		    ->setCellValue("I$whiteCount", "$whiteArr[voucher_id]")
 		    ->setCellValue("J$whiteCount", "$whiteArr[expiration_date]");

	$whiteCount++;
}
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Eladasok');

	$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setAutoSize(true);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=\"indulhatunk_info_elszamolas_$date.xls\"");
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

exit;

?>