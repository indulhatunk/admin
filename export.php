<?
header('Content-type: text/html; charset=utf-8');
include("inc/config.inc.php");
include("inc/mysql.class.php");
include("inc/functions.inc.php");
include("inc/PHPExcel.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

$date = date("Ymd_His");

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

if($CURUSER[userclass] == 5)
	$limit = "reseller_id = $CURUSER[pid]";
else
	$limit = "pid = $CURUSER[pid]";


if($CURUSER[reseller_office] > 0)
	$limit = "$limit AND reseller_office = '$CURUSER[reseller_office]'";
	

if($CURUSER[userclass] < 5)
{
	$pdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $CURUSER[pid] LIMIT 1"));
	$startid = 0;
}
else
	$startid = 2;
	
// Set properties
$objPHPExcel->getProperties()->setCreator("Indulhatunk.info")
							 ->setLastModifiedBy("indulhatunk.info")
							 ->setTitle("Indulhatunk.info elszámolás")
							 ->setSubject("Indulhatunk.info elszámolás")
							 ->setDescription("Indulhatunk.info elszámolás")
							 ->setKeywords("")
							 ->setCategory("");





if($pdata[is_qr] <> 1)
{



// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Voucher Sorszáma')
            ->setCellValue('B1', 'Vevő neve')
            ->setCellValue('C1', 'Vevő címe')
            ->setCellValue('D1', 'Vevő telefonszáma')
            ->setCellValue('E1', 'Vevő email címe')
 			->setCellValue('F1', 'Licit')
 		    ->setCellValue('G1', 'Eladási ár')
 		    ->setCellValue('H1', 'Rögzítés dátuma')
 		    ->setCellValue('I1', 'Hol?')
 		    ->setCellValue('J1', 'Fizetési mód')
 		     ->setCellValue('K1', 'Távozott');

if($CURUSER[pid] == 3003)
{
			 $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', 'Szálloda neve');
}


$whiteQr = $mysql->query("SELECT customers.* FROM customers INNER JOIN offers ON offers.id = customers.offers_id WHERE $limit AND paid = 0 AND type <> 5 AND inactive = 0 AND offers.expiration_date > NOW()");

$whiteCount = 2;
while($whiteArr = mysql_fetch_assoc($whiteQr))
{
	
	


	if($whiteArr[type] == 1) 
		$type = "Vatera";
	elseif($whiteArr[type] == 2) 
		$type = "Teszvesz";
	elseif($whiteArr[type] == 3) 
		$type = "Licittravel";
	elseif($whiteArr[type] == 4) 
		$type = "Outlet";
	elseif($whiteArr[type] == 6) 
		$type = "Lealkudtuk";
	
	$ppyear = explode("-","$whiteArr[paid_date]");
	$ppyear = $ppyear[0];


	if($whiteArr[postpone] == 1 && $ppyear == 2012)
		$payment = 'Üdülési csekk';
	elseif($whiteArr[postpone] == 1 && $ppyear == 2013)
		$payment = 'SZÉP kártya';
	elseif($whiteArr[payment] == 1 || $whiteArr[checkpaper_id] > 0) 
		$payment = "Átutalás";
	elseif($whiteArr[payment] == 2) 
		$payment = "Készpénz";
	elseif($whiteArr[payment] == 3) 
		$payment = "Utánvét";
	elseif($whiteArr[payment] == 4) 
		$payment = "Futár";
	elseif($whiteArr[payment] == 5) 
		$payment = "Üdülési csekk";
	elseif($whiteArr[payment] == 6) 
		$payment = "Helyszinen";
	elseif($whiteArr[payment] == 7) 
		$payment = "Online";
	elseif($whiteArr[facebook] == 8) 
		$payment = "Facebook";
	elseif($whiteArr[payment] == 10 || $whiteArr[payment] == 11 || $whiteArr[payment] == 12)
		$payment = "SZÉP kártya";
			
	if($whiteArr[check_arrival] <> '0000-00-00 00:00:00')
		$payment = "Átutalás";
		
	if($whiteArr[customer_left] == '0000-00-00 00:00:00')
		$left = "-";
	else
		$left = $whiteArr[customer_left];
			
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$whiteCount", "$whiteArr[offer_id]")
            ->setCellValue("B$whiteCount", trim($whiteArr[name]))
            ->setCellValue("C$whiteCount", trim($whiteArr[zip])." ".trim($whiteArr[city])." ".trim($whiteArr[address]))
            ->setCellValue("D$whiteCount", trim($whiteArr[phone]))
 			->setCellValue("E$whiteCount", trim($whiteArr[email]))
 		    ->setCellValue("F$whiteCount", trim($whiteArr[bid_name]))
 		    ->setCellValue("G$whiteCount", "$whiteArr[orig_price]")
 		    ->setCellValue("H$whiteCount", "$whiteArr[added]")
 		    ->setCellValue("I$whiteCount", "$type")
 		    ->setCellValue("J$whiteCount", "$payment")
 		    ->setCellValue("K$whiteCount", "$left");		
 		    
	if($CURUSER[pid] == 3003)
	{
		$partner = mysql_fetch_assoc($mysql->query("SELECT  * FROM partners WHERE pid = $whiteArr[sub_pid]"));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K$whiteCount", $partner[hotel_name]);
	}


	$whiteCount++;
}
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Elintézendő');

	$objPHPExcel->getActiveSheet(0)->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension('L')->setAutoSize(true);


$objPHPExcel->createSheet();

// Add some data
$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue('A1', 'Voucher Sorszáma')
            ->setCellValue('B1', 'Vevő neve')
            ->setCellValue('C1', 'Vevő címe')
            ->setCellValue('D1', 'Vevő telefonszáma')
            ->setCellValue('E1', 'Vevő email címe')
 			->setCellValue('F1', 'Licit')
 		    ->setCellValue('G1', 'Eladási ár')
 		    ->setCellValue('H1', 'Fizetés dátuma')
 		    ->setCellValue('I1', 'Hol?')
 		    ->setCellValue('J1', 'Fizetési mód')
 		    ->setCellValue('K1', 'Távozott');
 		    
 		    
 		    if($CURUSER[pid] == 3003)
{
			 $objPHPExcel->setActiveSheetIndex(1)->setCellValue('K1', 'Szálloda neve');
}


//$whiteQr = $mysql->query("SELECT * FROM customers WHERE $limit AND paid = 1 and voucher = 1 AND invoice_created = 0  AND inactive = 0");

$whiteQr = $mysql->query("SELECT customers.* FROM customers INNER JOIN offers ON offers.id = customers.offers_id WHERE $limit AND paid = 1 and voucher = 1 AND invoice_created = 0  AND inactive = 0 AND expiration_date > NOW()");


$whiteCount = 2;
while($whiteArr = mysql_fetch_assoc($whiteQr))
{
	
	if($whiteArr[type] == 1) 
		$type = "Vatera";
	elseif($whiteArr[type] == 2) 
		$type = "Teszvesz";
	elseif($whiteArr[type] == 3) 
		$type = "Licittravel";
	elseif($whiteArr[type] == 4) 
		$type = "Outlet";
	elseif($whiteArr[type] == 6) 
		$type = "Lealkudtuk";
		
	
	$ppyear = explode("-","$whiteArr[paid_date]");
	$ppyear = $ppyear[0];


	if($whiteArr[postpone] == 1 && $ppyear == 2012)
		$payment = 'Üdülési csekk';
	elseif($whiteArr[postpone] == 1 && $ppyear == 2013)
		$payment = 'SZÉP kártya';
	elseif($whiteArr[payment] == 1 || $whiteArr[checkpaper_id] > 0) 
		$payment = "Átutalás";
	elseif($whiteArr[payment] == 2) 
		$payment = "Készpénz";
	elseif($whiteArr[payment] == 3) 
		$payment = "Utánvét";
	elseif($whiteArr[payment] == 4) 
		$payment = "Futár";
	elseif($whiteArr[payment] == 5) 
		$payment = "Üdülési csekk";
	elseif($whiteArr[payment] == 6) 
		$payment = "Helyszinen";
	elseif($whiteArr[payment] == 7) 
		$payment = "Online";
	elseif($whiteArr[facebook] == 8) 
		$payment = "Facebook";
	elseif($whiteArr[payment] == 10 || $whiteArr[payment] == 11 || $whiteArr[payment] == 12)
		$payment = "SZÉP kártya";
		
	if($whiteArr[check_arrival] <> '0000-00-00 00:00:00')
		$payment = "Átutalás";
		
	if($whiteArr[customer_left] == '0000-00-00 00:00:00')
		$left = "-";
	else
		$left = $whiteArr[customer_left];
		
	$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue("A$whiteCount", "$whiteArr[offer_id]")
            ->setCellValue("B$whiteCount", trim($whiteArr[name]))
            ->setCellValue("C$whiteCount", trim($whiteArr[zip])." ".trim($whiteArr[city])." ".trim($whiteArr[address]))
            ->setCellValue("D$whiteCount", trim($whiteArr[phone]))
 			->setCellValue("E$whiteCount", trim($whiteArr[email]))
 		    ->setCellValue("F$whiteCount", trim($whiteArr[bid_name]))
 		    ->setCellValue("G$whiteCount", "$whiteArr[orig_price]")
 		    ->setCellValue("H$whiteCount", "$whiteArr[paid_date]")
 		    ->setCellValue("I$whiteCount", "$type")
 		    ->setCellValue("J$whiteCount", "$payment")
 		     ->setCellValue("K$whiteCount", "$left");	
 		    
 	if($CURUSER[pid] == 3003)
	{
		$partner = mysql_fetch_assoc($mysql->query("SELECT  * FROM partners WHERE pid = $whiteArr[sub_pid]"));
		$objPHPExcel->setActiveSheetIndex(1)->setCellValue("K$whiteCount", $partner[hotel_name]);
	}
 		  		
	$whiteCount++;
}
$objPHPExcel->getActiveSheet()->setTitle('Fizetett');

	$objPHPExcel->getActiveSheet(1)->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet(1)->getColumnDimension('L')->setAutoSize(true);

	
$objPHPExcel->createSheet();

}

$whiteQr = $mysql->query("SELECT * FROM customers WHERE $limit AND paid = 1 and voucher = 1 AND invoice_created = 1   AND inactive = 0 ORDER BY invoice_number ASC");

if(mysql_num_rows($whiteQr) > 0)
{


$objPHPExcel->setActiveSheetIndex($startid);
// Add some data
$objPHPExcel->setActiveSheetIndex($startid)
            ->setCellValue('A1', 'Voucher Sorszáma')
            ->setCellValue('B1', 'Vevő neve')
            ->setCellValue('C1', 'Vevő címe')
            ->setCellValue('D1', 'Vevő telefonszáma')
            ->setCellValue('E1', 'Vevő email címe')
 			->setCellValue('F1', 'Licit')
 		    ->setCellValue('G1', 'Eladási ár')
 		    ->setCellValue('H1', 'Fizetés dátuma')
 		    ->setCellValue('I1', 'Hol?')
			->setCellValue('J1', 'Számlaszám')
			->setCellValue('K1', 'Fizetési mód')
			->setCellValue('L1', 'Távozott');
			
			
if($CURUSER[pid] == 3003)
{
			 $objPHPExcel->setActiveSheetIndex($startid)->setCellValue('L1', 'Szálloda neve');
}


$whiteCount = 2;
while($whiteArr = mysql_fetch_assoc($whiteQr))
{
	
	
	if($whiteArr[type] == 1) 
		$type = "Vatera";
	elseif($whiteArr[type] == 2) 
		$type = "Teszvesz";
	elseif($whiteArr[type] == 3) 
		$type = "Licittravel";
	elseif($whiteArr[type] == 4) 
		$type = "Outlet";
	elseif($whiteArr[type] == 6) 
		$type = "Lealkudtuk";
		
	$ppyear = explode("-","$whiteArr[paid_date]");
	$ppyear = $ppyear[0];

	if($whiteArr[postpone] == 1 && $ppyear == 2012)
		$payment = 'Üdülési csekk';
	elseif($whiteArr[postpone] == 1 && $ppyear == 2013)
		$payment = 'SZÉP kártya';
	elseif($whiteArr[payment] == 1 || $whiteArr[checkpaper_id] > 0) 
		$payment = "Átutalás";
	elseif($whiteArr[payment] == 2) 
		$payment = "Készpénz";
	elseif($whiteArr[payment] == 3) 
		$payment = "Utánvét";
	elseif($whiteArr[payment] == 4) 
		$payment = "Futár";
	elseif($whiteArr[payment] == 5) 
		$payment = "Üdülési csekk";
	elseif($whiteArr[payment] == 6) 
		$payment = "Helyszinen";
	elseif($whiteArr[payment] == 7) 
		$payment = "Online";
	elseif($whiteArr[facebook] == 8) 
		$payment = "Facebook";
	elseif($whiteArr[payment] == 10 || $whiteArr[payment] == 11 || $whiteArr[payment] == 12)
		$payment = "SZÉP kártya";	
		
	if($whiteArr[check_arrival] <> '0000-00-00 00:00:00')
		$payment = "Átutalás";
		
	if($whiteArr[customer_left] == '0000-00-00 00:00:00')
		$left = "-";
	else
		$left = $whiteArr[customer_left];
		
	$objPHPExcel->setActiveSheetIndex($startid)
          ->setCellValue("A$whiteCount", "$whiteArr[offer_id]")
            ->setCellValue("B$whiteCount", trim($whiteArr[name]))
            ->setCellValue("C$whiteCount", trim($whiteArr[zip])." ".trim($whiteArr[city])." ".trim($whiteArr[address]))
            ->setCellValue("D$whiteCount", trim($whiteArr[phone]))
 			->setCellValue("E$whiteCount", trim($whiteArr[email]))
 		    ->setCellValue("F$whiteCount", trim($whiteArr[bid_name]))
 		    ->setCellValue("G$whiteCount", "$whiteArr[orig_price]")
 		    ->setCellValue("H$whiteCount", "$whiteArr[paid_date]")
 		    ->setCellValue("I$whiteCount", "$type")
 		    ->setCellValue("J$whiteCount", "$whiteArr[invoice_number]")
 		    ->setCellValue("K$whiteCount", "$payment")
 		    ->setCellValue("L$whiteCount", "$left");		
		
		if($CURUSER[pid] == 3003)
		{
			$partner = mysql_fetch_assoc($mysql->query("SELECT  * FROM partners WHERE pid = $whiteArr[sub_pid]"));
			$objPHPExcel->setActiveSheetIndex(2)->setCellValue("L$whiteCount", $partner[hotel_name]);
		}
	
	$whiteCount++;
}
$objPHPExcel->getActiveSheet()->setTitle('Számlázott');
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet($startid)->getColumnDimension('L')->setAutoSize(true);
}

$objPHPExcel->setActiveSheetIndex($startid);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=\"indulhatunk_info_elszamolas_$date.xls\"");
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

exit;

?>