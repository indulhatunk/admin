<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

userlogin();

if($_GET[delete] > 0 && $_GET[sure] <> 1)
{
	echo ("<h2>Biztosan törölni szeretné az elemet?</h1><br/>  <a href=\"?delete=$_GET[delete]&sure=1\" class='button green'>$lang[yes]</a> <a href=\"?donotdel=1\" class='button red'>$lang[no]</a><div class='cleaner'></div>");
	die;
}

if($_GET[delete] > 0 && $_GET[sure] == 1)
{
	$del = (int)$_GET[delete];
	$pid = (int)$CURUSER[pid];
	$mysql->query("DELETE FROM facebook_offers WHERE id = $del AND pid = $pid");
	$msg = 'Sikeresen törölte a tételt!';
}
$eid = (int)$_GET[id];

$editid = $_POST[id];
if($_POST[title] <> '' && $editid == '')
{
	$mysql->query_insert("facebook_offers",$_POST);
}

if($_POST[title] <> '' && $editid > 0)
{

	$mysql->query_update("facebook_offers",$_POST,"id=$_POST[id]");
}

head('Facebook ajánlatok');



mysql_query("SET NAMES 'utf8'"); 
echo "<h1><a href=\"#\">Vatera adminisztrációs felület</a> &raquo; Facebook ajánlatok kezelése</h1>";

echo message($msg);

if($_GET[addimage] == 1 && $_GET[offer] > 0)
{



//image upload part
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/pjpeg")
|| ($_FILES["file"]["type"] == "image/png"))

&& ($_FILES["file"]["size"] < 200000000))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    }
  else
    {
    
     $filename = "images/facebook/orig/$_GET[offer].jpg";
     $smallfilename = "images/facebook/small/$_GET[offer].jpg";

 
      move_uploaded_file($_FILES["file"]["tmp_name"],$filename);
      
      Image($filename, '1.26:1', '120x',$smallfilename); //95
		$mysql->query("UPDATE facebook_offers SET image = 1 WHERE id = $_GET[offer] AND pid = $CURUSER[pid]");
		
		echo message("Sikeresen feltöltötte a képet! <a href='facebookoffers.php'>Vissza!</a>");
		foot();
		die;
    }
  }
//image upload part end
?>

<form action="facebookoffers.php?addimage=1&offer=<?=$_GET[offer]?>" method="post" enctype="multipart/form-data">
<label for="file">Kép:</label>
<input type="file" name="file" id="file" /> <input type="submit" name="submit" value="Feltöltés" />
</form>
<?
	foot();
	die;
}

if($_GET[add] == 1)
{
$editarr = mysql_fetch_assoc($mysql->query("SELECT * FROM facebook_offers WHERE id = $eid"));
?>

<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>


<div class='cleaner'></div>
<form method="post" action="facebookoffers.php?cid=<?=$id?>">
<fieldset style='width:674px;' class='facebookoffers'>
<ul>
	<?
		echo "<input type=\"hidden\" name=\"id\" value=\"$editarr[id]\"/>";

	?>
	<input type='hidden' name='pid' value='<?=$CURUSER[pid]?>'/>
<li>
	<label>Sorrend</label><input type='text' name='priority' value='<?=$editarr[priority]?>' class='numeric' style='width:30px;'/>
</li>
<li>
	<label>Cím</label><input type='text' name='title' value='<?=$editarr[title]?>'/>
</li>
<li>
	<label>Rövid cím</label><input type='text' name='subtitle' value='<?=$editarr[subtitle]?>'/>
</li>
<li>
	<label>Tartalom címe</label><input type='text' name='contenttitle' value='<?=$editarr[contenttitle]?>'/>
</li>
<li>
	<label>Link</label><input type='text' name='url' value='<?=$editarr[url]?>'/>
</li>
<li>
	<label>Leírás</label><br/><textarea name="description" rows="10"  style="width: 480px" class="tinymce"><?=$editarr[description]?></textarea>
</li>




	<li style='text-align:center;'><input type="submit" value="Mentés" /></li>
</ul>	
</fieldset>
</form>

<?
foot();
die;
}
?>


<fieldset style='width:410px;margin:0 0 20px 0;float:left;'>
	<legend>Miért érdemes a Szállás ajánlatok alkalmazást használnia?</legend>
	<ol>
		<li>Napjaink legnépszerűbb közösségi oldala a Facebook, melynek segítségével már több mint 3 millió magyar felhasználót érhet el!</li>
		<li>Rajongói oldala segítségével könnyedén tájékoztathatja látogatóit a legfontosabb eseményekről, hírekről, információkról!</li>
		<li><b>INGYENES</b> alkalmazásunk lehetővé teszi, hogy felhasználói egy teljesen egyedi felületen tekinthessék meg legújabb ajánlatait.</li>
		<li>Szolgáltatásunk ingyenes, s az is marad!</li>
	</ol>
	
	<a href='http://www.facebook.com/pages/Próba-ajánlatok/186920414718015?sk=app_299746896706018' target="_blank" style='display block; border:1px solid black; padding:5px; margin:5px 5px 5px 180px'>Így fog kinézni!</a>
	<br/><br/>
</fieldset>


<fieldset style='width:410px;margin:0 0 20px 20px;float:left;'>
	<legend>Hogyan telepíthetem a Facebook oldalamra?</legend>
	<ol>
		<li>Az alkalmazás telepítéséhez kattintson az alábbi URL-re: <br/>
			<a style='text-decoration:underline;font-weight:bold;' href='https://www.facebook.com/apps/application.php?id=299746896706018' target='_blank'>Facebook szállás alkalmazás</a> </li>
		<li>A képernyő bal oldalán kattintson rá az "Add to my Page (Hozzáadás az oldalamhoz)" gombra, majd a felugró ablakban kattitnson a "Add to page" opcióra.</li>
		<li>Ezután nem kell mást tennie, mint meglátogatnia a <b>saját</b> Facebook rajongói oldalát, s aktiválni az alkalmazást.</li>
		<li>A szállás ajánlatok fülre kattintva írja be az alábbi aktivációs kulcsot: <b><?=md5($CURUSER[username])?></b> <br/>
		 Ezt követően pedig kattintson az aktivál gombra.</b></li>	
		<li>Gratulálunk! Ön sikeresen feltelepítette az egyedi Facebook szállás alkalmazását.</li>
	</ol>
</fieldset>
<div class='cleaner'></div>
<?
echo "<table class=\"general\" style='width:100%'>";


$qr = "SELECT * FROM facebook_offers WHERE pid = '$CURUSER[pid]' ORDER BY priority ASC";
$query = mysql_query($qr); 



	echo "<tr class=\"header\">";
		echo "<td width='50'>Kép</td>";
		echo "<td width='10'>Pr.</td>";
		echo "<td>Cím</td>";
		echo "<td>Rövid cím</td>";
		echo "<td width='50'>Link</td>";
		
		echo "<td width='50'>-</td>";
		echo "<td width='50'>Törlés</td>";
	echo "</tr>";
	

if(mysql_num_rows($query) == 0)
{
	echo "<tr class='blue'>";
		echo "<td width='50'><img src='http://admin.indulhatunk.info/images/facebook/offerimage.png'/></td>";
		echo "<td width='10'>1</td>";
		echo "<td>Minta ajánlat</td>";
		echo "<td>3 éj / fő / félpanzió 32 700 Ft-ért</td>";
		echo "<td width='50'><a href='http://www.belenushotel.hu/' target='_blank'>[link]</a></td>";
		
		echo "<td width='50'><a href='facebook.php?pid=916' target='_blank'>[előnézet]</a></td>";
		echo "<td width='50'>-</td>";
	echo "</tr>";
}	
while($arr = mysql_fetch_assoc($query)) {
	echo "<tr>";
	if($arr[image] == 0)
	{
		echo "<td>[<a href='?addimage=1&offer=$arr[id]'>kép&nbsp;feltöltése</a>]</td>";
	}
	else
	{
		echo "<td align='center'><img src='/images/facebook/small/$arr[id].jpg'/><br/>[<a href='?addimage=1&offer=$arr[id]'>kép&nbsp;feltöltése</a>]</td>";
	}
		echo "<td align='center'>$arr[priority]</td>";
		echo "<td>$arr[title]</td>";
		echo "<td>$arr[subtitle]</td>";
		echo "<td align='center'><a href='$arr[url]' target='_blank'>[link]</a></td>";	
		
		echo "<td align='center'><a href='?id=$arr[id]&add=1'>[szerkeszt]</a><br/><a href='facebook.php?pid=$arr[pid]' target='_blank'>[előnézet]</a></td>";
		echo "<td align='center'><a href='?delete=$arr[id]' rel='facebox'>[töröl]</a></td>";
	echo "</tr>";
}
echo "<tr><td colspan='7' style='text-align:center;' class='header'><a href='?add=1'>Új tétel hozzáadása</a></td></tr>";
echo "</table>";
if($CURUSER[userclass] == 255)
{
	echo "<hr/>";
	echo "<table>";
	echo "<tr class='header'><td colspan='2'>Aktivált partnerek</td></tr>";

	$query = $mysql->query("SELECT * FROM partners WHERE facebook_id <> ''");
	while($arr = mysql_fetch_assoc($query))
	{
		echo "<tr>";
			echo "<td>$arr[hotel_name]</td>";
			echo "<td>
			<a href='http://www.facebook.com/profile.php?id=$arr[facebook_id]' target='_blank'>Facebook &raquo;</a>
			<a href='facebook.php?pid=$arr[pid]' target='_blank'>Előnézet &raquo;</a>
				</td>";
		echo "</tr>";
	}
	echo "</table>";
}
foot();
?>