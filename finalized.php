<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER[userclass] < 50)
	header("location:index.php");
	
head('Véglegesítési statisztika');




?>

<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href='#' class='current'>Véglegesítés</a></li>
	</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<?
/*
$query = $mysql->query("SELECT email, offer_id FROM customers WHERE paid = 0 AND voucher = 0 AND name <> 'Pult' AND facebook <> 1 AND inactive = 0 AND checked = 0 GROUP by email ORDER BY added DESC");

$i = 1;
while($arr = mysql_fetch_assoc($query))
{	
	$domain = explode("@",$arr[email]);
	$domain = $domain[1];
	
	$domains[$domain]++;
	
	//echo "$i. $arr[offer_id] $arr[email]<hr/>";
	$i++;
}

array_multisort($domains,SORT_DESC);

echo "<table>";
echo "<tr class='header'><td colspan='2'>Kitöltetlen domain statisztika</td></td>";
foreach($domains as $domain => $count)
{
	echo "<tr>";
		echo "<td>$domain</td>";
		echo "<td align='right'>$count</td>";
	echo "</tr>";
	$total = $total + $count;
}
	echo "<tr class='header'>";
		echo "<td>Összesen</td>";
		echo "<td align='right'>$total</td>";
	echo "</tr>";
echo "</table><hr/>";
*/
$query = $mysql->query("SELECT name,cid,email,hash,paid,inactive,added,offer_id, paid_date, checked_date, first_click_date FROM customers WHERE `added` >= DATE_SUB(CURDATE(), INTERVAL 2 MONTH) ORDER BY added DESC");


$i = 1;

echo "<table>";

echo "<tr class='header'>";
		echo "<td align='center'>-</td>";
		echo "<td align='center'>ID</td>";
		echo "<td align='center'>Hozzáadva</td>";
		echo "<td align='center'>Kattintás</td>";
		echo "<td align='center'>Kitöltve</td>";
		echo "<td align='center'>Eltelt</td>";
		echo "<td align='center'>Fizetve</td>";
		echo "<td align='center'>Átfutás</td>";
		echo "<td align='center'>Kiküld</td>";

	echo "</tr>";
	
	
while($arr = mysql_fetch_assoc($query)) {

	if($arr[inactive] == 1)
		$class = 'purple';
	elseif($arr[paid] == 1)
		$class = 'green';
	else
		$class = '';
		
	if($arr[first_click_date] == '0000-00-00 00:00:00')
	{
		$arr[first_click_date] = '';
	}
	
	if($arr[checked_date] == '0000-00-00 00:00:00')
	{
		$arr[checked_date] = '';
		$checkdiff = '';
	}
	else
	{
		$checkdiff = _date_diff($arr[checked_date],$arr[added]);
	}
		
	if($arr[paid_date] == '0000-00-00 00:00:00')
	{
		$paiddiff = '';
		$arr[paid_date] = '';
	}
	else
	{
		$paiddiff = _date_diff($arr[paid_date],$arr[added],'days');
	}
		
		
	echo "<tr class='$class'>";
		echo "<td align='center'>$i</td>";
		echo "<td align='center'>$arr[offer_id]</td>";
		echo "<td align='center'>$arr[added]</td>";
		echo "<td align='center'>$arr[first_click_date]</td>";
		echo "<td align='center'>$arr[checked_date]</td>";
		echo "<td align='center'>$checkdiff</td>";

		echo "<td align='center'>$arr[paid_date]</td>";
		echo "<td align='center'>$paiddiff</td>";
		
		
		$subject = "Indul6unk vásárlás véglegesítése";
		$body = "Kedves $arr[name]!%0A%0AKöszönjük hogy nálunk vásárolt.%0A%0AKérjük szánjon 1 percet az alábbi oldalra, mert rengeteg plusz munkától és%0Atelefonálgatástól kímélheti meg magát.%0A%0Ahttp://admin.indulhatunk.hu/customers/$arr[hash]/$arr[cid]%0A%0A
Az utalvány sorszáma: $arr[offer_id]";


	if($arr[checked_date] == '' && $arr[inactive] == 0 && $arr[paid] == 0)		
		echo "<td align='center' class='blue'><a href='mailto:$arr[email]?subject=$subject&body=$body'>kiküld</a></td>";
	else
		echo "<td></td>";
			
	echo "</tr>";
	
	$i++;	
}
echo "</table></div></div>";
foot();
?>