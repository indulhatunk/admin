<?php
/*
 * fizu.php
 * Author: gergely.solymosi@gmail.com
 **
 */

/* bootstrap file */
$noheader = 'no';
include("inc/init.inc.php");

userlogin();

if($CURUSER[userclass] < 50)
    header("location: index.php");


$vat = 0.27;
$i = 0;



$confirmation_date_to = empty($_GET[to_date]) ? date('Y-m-t').' 23:59:59' : $_GET[to_date] . ' 23:59:59';
$confirmation_date_from = empty($_GET[from_date]) ? date('Y-m-01').' 00:00:00' : $_GET[from_date] . ' 00:00:00';


$company_invoice = empty($_GET[company])? 'szallasoutlet' : $_GET[company];


$voucher_type = array(
    // Szerződés összege * 10%
    "l10" => array(
        "car",
        "hotel",
        "hotel_hu",
        "other",
        "parking",
        "parking_foreign",
        "touroperation",
        "transfer_foreign",
        "transfer_local",
        "visa",
    ),
    // Normál Iroda 3,5 + Saját 2,5
    "null" => array(
        "insurance",
        "voucher",
        "package",
    ),
    // (Utasok száma * 7000) Normál Iroda 3,5 + Saját 2,5 
    // 7000 felett extra ((Visszzaigazolt összeg-7000) * 2%)
    // Levonás nem jár érte, ha nem érte el a 7000 ft-os elvárt összeget
    "l7000_m2" => array (
        "plane"
    ),
    // (Utasok száma * 5000) Normál Iroda 3,5 + Saját 2,5
    // 7000 felett extra ((Visszzaigazolt összeg-5000) * 2%)
    // Levonás nem jár érte, ha nem érte el a 5000 ft-os elvárt összeget
    "l5000_m2" => array(
        "plane_lowcost"
    )    
);



$search  = array(',','!','*','.','/','%!%',";","(",")", " ", "&", "Ăˇ", "Ă�", "Ă©", "Ă‰", "Ă­", "ĂŤ", "Ăł", "Ă“", "Ă¶", "Ă–", "Ĺ‘", "Ĺ�", "Ăş", "Ăš", "ĂĽ", "Ăś", "Ĺ±", "Ĺ°");
$replace = array("","","","","","","","","-", "-", "a", "A", "e", "E", "i", "I", "o", "O", "o", "O", "o", "O", "u", "U", "u", "U", "u", "u");

$sql = "SELECT
    COUNT(partners.pid) as count,
    office_id,
    CONCAT(partner_offices.city,
        ' ',
        partner_offices.address) AS addr,
        GROUP_CONCAT(partners.company_name)
        FROM
        partners
        LEFT JOIN
        partner_offices ON partner_offices.id = partners.office_id
        WHERE
        userclass >= 40 AND partners.pid <> 897
        AND disabled = 0
        AND position = 'Értékesítő'
        GROUP BY office_id
        ORDER BY office_id , position ASC";

$query = $mysql->query($sql);

while($row = mysql_fetch_assoc($query)) {
    $iroda_count[$row[office_id]] = $row;
}

$sql = "SELECT 
    partners.company_name AS agent_name,
    partners.office_id AS agent_officeid,
    customers_tour.agent_id,
    customers_tour.offer_id,
    customers_tour.tour_category,
    customers_tour.name AS customer_name,
    customers_tour.city AS customer_city,
    customers_tour.email AS customer_email,
    customers_tour.phone AS customer_phone,
    customers_tour.offer_name,
    tour_operator.company_name,
    customers_tour.added,
    customers_tour.from_date,
    customers_tour.adults,
    
    CONCAT(partner_offices.city,
            ' ',
            partner_offices.address) AS addr,
    customers_tour.total,
    customers_tour.final_total,
    customers_tour.yield,
    case customers_tour.tour_category
        when 'car' then (customers_tour.total*0.1)
        when 'hotel' then (customers_tour.total*0.1)
        when 'hotel_hu' then (customers_tour.total*0.1)
        when 'other' then (customers_tour.total*0.1)
        when 'parking' then (customers_tour.total*0.1)
        when 'parking_foreign' then (customers_tour.total*0.1)
        when 'touroperation' then (customers_tour.total*0.1)
        when 'transfer_foreign' then (customers_tour.total*0.1)
        when 'transfer_local' then (customers_tour.total*0.1)
        when 'visa' then (customers_tour.total*0.1)
        when 'plane' then (customers_tour.adults*7000)+(customers_tour.final_total*0.02)
        when 'plane_lowcost' then (customers_tour.adults*5000)+(customers_tour.final_total*0.02)
        when 'insurance' then 0
        when 'voucher' then 0
        when 'package' then customers_tour.total*0.1
        end AS min_yield,

    (customers_tour.total - customers_tour.final_total - customers_tour.insurance_value - IF(customers_tour.own_storno = 1,
        ROUND((customers_tour.storno_insurance_base / 100) * customers_tour.storno_insurance),
        0)) AS diff,
    null AS extra_yield,
    SUM(customers_tour_payment.value) AS tpaid,
    (customers_tour.total - SUM(customers_tour_payment.value)) AS remaining_amount,
    customers_tour.insurance_value,
    
    #customers_tour.storno_insurance_base,
    IF(customers_tour.own_storno = 1, ROUND((customers_tour.storno_insurance_base / 100) * customers_tour.storno_insurance),0) as insurance_base_value ,
    customers_tour.insurance_yield_value,
    customers_tour.voucher_value,
    customers_tour.status,
    (customers_tour.total - customers_tour.final_total - customers_tour.insurance_value - IF(customers_tour.own_storno = 1,
        ROUND((customers_tour.storno_insurance_base / 100) * customers_tour.storno_insurance),
        0) + customers_tour.yield + customers_tour.insurance_yield_value) AS profit,
    ((customers_tour.total - customers_tour.final_total - customers_tour.insurance_value - IF(customers_tour.own_storno = 1,
        ROUND((customers_tour.storno_insurance_base / 100) * customers_tour.storno_insurance),
        0) + customers_tour.yield + customers_tour.insurance_yield_value) - customers_tour.total_income) AS profit_diff,
    customers_tour.total_income,
    customers_tour.position_number
FROM
    customers_tour
        LEFT JOIN
    partners ON partners.pid = customers_tour.agent_id
        LEFT JOIN
    partner_offices ON partner_offices.id = partners.office_id
        LEFT JOIN
    partners AS tour_operator ON customers_tour.partner_id = tour_operator.coredb_id
        AND customers_tour.partner_id != 0
        LEFT JOIN
    customers_tour_payment ON customers_tour.id = customers_tour_payment.customer_id
        AND customers_tour_payment.is_transfer = 0
WHERE
    company_invoice = '$company_invoice'
        AND confirmation_date <= '$confirmation_date_to'
        AND confirmation_date >= '$confirmation_date_from'
        AND customers_tour.status >= 4
        AND partners.hasYield = 1
GROUP BY customers_tour.id
ORDER BY customers_tour.agent_id,from_date ASC";


$query = $mysql->query($sql);



while($row = mysql_fetch_assoc($query)){ 
    /**
     * Tételes
     */
   $row['extra_yield'] = $row['diff']+$row['yield']- $row['min_yield'];
   
   
   if($row['extra_yield'] < 0) {
       $row['extra_yield'] = 0;
   }
   
   if($row['tour_category'] == "package" && ($row['partner_id'] != 267001322 || $row['partner_id'] != 267001331)) {
       $row['extra_yield'] = 0;
   }
   $alldata[] = $row;
   /**
    * IRODA
    */
   if(empty($iroda_count[$row['agent_officeid']])) {
       $iroda_count[$row['agent_officeid']][count] = 1;
   }
   
   $office_data[] = $row;
   if(empty($offices[$row['agent_officeid']])) { 
       $offices[$row['agent_officeid']]['addr'] = $row['addr'];
       $offices[$row['agent_officeid']]['total'] = $row['total'];
       $offices[$row['agent_officeid']]['profit'] = $row['profit'];
       $offices[$row['agent_officeid']]['net_profit'] = round(($row['profit']/(1+$vat)));
       $offices[$row['agent_officeid']]['net_office_yield'] = round(($row['profit']/(1+$vat))*0.035);
       $offices[$row['agent_officeid']]['count'] = round($iroda_count[$row['agent_officeid']][count]);
       $offices[$row['agent_officeid']]['net_office_yield_per_person'] = round(((((($row['profit']-$row['extra_yield']))/(1+$vat))*0.035)/$iroda_count[$row['agent_officeid']][count]));
       //$offices[$row['agent_officeid']]['extra_yield'] += $row['extra_yield'];
   }
   else {
       $offices[$row['agent_officeid']]['total'] += $row['total'];
       $offices[$row['agent_officeid']]['profit'] += $row['profit'];
       $offices[$row['agent_officeid']]['net_profit'] += round($row['profit']/(1+$vat));
       $offices[$row['agent_officeid']]['net_office_yield'] += round(($row['profit']/(1+$vat))*0.035);
       $offices[$row['agent_officeid']]['net_office_yield_per_person'] += round(((($row['profit']-$row['extra_yield'])/(1+$vat))*0.035)/$iroda_count[$row['agent_officeid']][count]);
       //$offices[$row['agent_officeid']]['extra_yield'] += $row['extra_yield'];
   }   

} 
mysql_data_seek($query, 0);
while($row = mysql_fetch_assoc($query)){
    /**
     * Tételes
     */
    $row['extra_yield'] = $row['diff']+$row['yield']- $row['min_yield'];
     
   if($row['extra_yield'] < 0) {
       $row['extra_yield'] = 0;
   }
   
   if($row['tour_category'] == "package" && ($row['partner_id'] != 267001322 || $row['partner_id'] != 267001331)) {
       $row['extra_yield'] = 0;
   }
    /**
     * Értékesítő
     */
    if(empty($agents[$row['agent_id']])) {
        $agents[$row['agent_id']]['agent_name'] = $row['agent_name'];
        $agents[$row['agent_id']]['addr'] = $row['addr'];
        $agents[$row['agent_id']]['total'] = $row['total'];
        $agents[$row['agent_id']]['profit'] = $row['profit'];
        $agents[$row['agent_id']]['net_profit'] = round($row['profit']/(1+$vat));
        $agents[$row['agent_id']]['net_own_yield'] = round(($row['profit']/(1+$vat))*0.025);
        $agents[$row['agent_id']]['net_office_yield'] = round($offices[$row['agent_officeid']]['net_office_yield_per_person']);
        $agents[$row['agent_id']]['extra_yield'] = round(($row['extra_yield']/(1+$vat))*0.125);
        //$agents[$row['agent_id']]['sum_yield'] = $agents[$row['agent_id']]['net_own_yield'] + $agents[$row['agent_id']]['net_office_yield'] + $agents[$row['agent_id']]['extra_yield'];
        $agents[$row['agent_id']]['sum_yield'] = $agents[$row['agent_id']]['net_own_yield'] + round(&$offices[$row['agent_officeid']]['net_office_yield_per_person']); + $agents[$row['agent_id']]['extra_yield'];
         
    }
    else {
        $agents[$row['agent_id']]['total'] += $row['total'];
        $agents[$row['agent_id']]['profit'] += $row['profit'];
        $agents[$row['agent_id']]['net_profit'] += round($row['profit']/(1+$vat));
        $agents[$row['agent_id']]['net_own_yield'] += round(($row['profit']/(1+$vat))*0.025);
        $agents[$row['agent_id']]['net_office_yield'] = round(&$offices[$row['agent_officeid']]['net_office_yield_per_person']);
        $agents[$row['agent_id']]['extra_yield'] += round(($row['extra_yield']/(1+$vat))*0.125);
        //$agents[$row['agent_id']]['sum_yield'] = $agents[$row['agent_id']]['net_own_yield'] + $agents[$row['agent_id']]['net_office_yield'] + $agents[$row['agent_id']]['extra_yield'];
        $agents[$row['agent_id']]['sum_yield'] = $agents[$row['agent_id']]['net_own_yield'] + round(&$offices[$row['agent_officeid']]['net_office_yield_per_person']) + $agents[$row['agent_id']]['extra_yield'];
    
    }
}

//print_r($agents); die();

$path = realpath('lib/PHPExcel') . '/PHPExcel.php';
include_once($path);
$doc = new PHPExcel();

$doc->setActiveSheetIndex(0);
$header = array(
    "Kapcsolattartó",
    "Iroda",
    "Kapcsolattartó ID",
    "Vocuher ID",
    "Típus",
    "Várásló neve",
    "Várásló cím",
    "Várásló email",
    "Várásló tel.",
    "Út neve",
    "Utazásszervező",
    "Reg. idő",
    "Utazás indulás",
    "Felnőttek száma",
    "Iroda címe",
    "Szerződés összege",
    "Visszaigazolt összege",
    "Visszaigazolt jutalék",
    "Elvárt jutalék",
    "Árrés / eltérés",
    "Extra jutalék",
    "Fizetett",
    "Hátralék",
    "Biztosítás",
    "Stornó bizt.",
    "Bizt. jutalék",
    "Utalvány",
    "Státusz",
    "Haszon",
    "Eltérés",
    "total_income",
    "position_number"
);
$doc->getActiveSheet()  ->setTitle("Összes tétel")
->freezePane('A1')
->fromArray($header, null, 'A1')
->fromArray($alldata, null, 'A2');
foreach(range('A','U') as $columnID) {
    $doc->getActiveSheet()->getColumnDimension($columnID)
    ->setAutoSize(true);
}

$header = array(
    "Kapcsolattartó",
    "Iroda",
    "Bruttó Forgalom",
    "Bruttó Haszon",
    "Nettó Haszon",
    "Saját jutalék",
    "Iroda jutalék",
    "Extra jutalék",
    "Jutalék összesen"
);


$doc->createSheet(1);
$doc->setActiveSheetIndex(1);
$doc->getActiveSheet()  ->setTitle("Szervező")
->freezePane('A1')
->fromArray($header, null, 'A1')
->fromArray($agents, null, 'A2');
foreach(range('A','H') as $columnID) {
    $doc->getActiveSheet()->getColumnDimension($columnID)
    ->setAutoSize(true);
}
for ($row = 2; $row <= count($agents)+1; $row++) {
    $doc->getActiveSheet()
    ->setCellValue(
        'J' . $row,
        '=SUM(F'.$row.':H'.$row.')'
        );
}

$header = array(
    "Iroda",
    "Bruttó Forgalom",
    "Bruttó Haszon",
    "Nettó Haszon",
    "Iroda jutalék",
    "Iroda létszám",
    "Iroda jutalék/fő"
);
$doc->createSheet(2);
$doc->setActiveSheetIndex(2);
$doc->getActiveSheet()  ->setTitle("Iroda")
->freezePane('A1')
->freezePane('A2')
->fromArray($header, null, 'A1')
->fromArray($offices, null, 'A2');
foreach(range('A','G') as $columnID) {
    $doc->getActiveSheet()->getColumnDimension($columnID)
    ->setAutoSize(true);
}

$header = array(
    "Count",
    "Iroda ID",
    "Iroda cím",
    "Szervezők"
);
$doc->createSheet(3);
$doc->setActiveSheetIndex(3);
$doc->getActiveSheet()  ->setTitle("Iroda COUNT")
->freezePane('A1')
->fromArray($header, null, 'A1')
->fromArray($iroda_count, null, 'A2');
foreach(range('A','D') as $columnID) {
    $doc->getActiveSheet()->getColumnDimension($columnID)
    ->setAutoSize(true);
}

$doc->setActiveSheetIndex(1);

// Redirect output to a clientâ€™s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="jutalek-'.date('Ymdhis').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
$objWriter->save('php://output');
exit;

?>