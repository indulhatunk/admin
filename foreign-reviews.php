<?
/*
 * reviews.php 
 *
 * the review page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");

userlogin();

head('Értékelések');


?>

<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
		<li><a href="#" class="current">Értékelések</a></li>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<script src='//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js'></script>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css"/>
	<script>
		
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "currency-pre": function (a) {
        a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
        return parseFloat(a);
    },
    "currency-asc": function (a, b) {
        return a - b;
    },
    "currency-desc": function (a, b) {
        return b - a;
    }
});



		$(document).ready(function() {
			$('.sortable').DataTable({
				
				"aoColumns": [
				{ "sType": "currency" },
				null,
				{ "sType": "string" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				null,
		],
		
				"order": [[ 5, "desc" ]],
				bFilter: false, bInfo: false, "bPaginate": false,
		} );

});
</script>
<?


//echo " <a href=\"reviews.php\"  class=\"white button\">Összes hozzászólás</a> ";
//echo " <a href=\"reviews.php?report=1\"  class=\"red button\">Jelentett hozzászólások</a> <div class='cleaner'></div>";

$query = $mysql->query("SELECT * FROM `customers_tour_reviews` ORDER BY id DESC");



echo "<table class='sortable'>";

echo "<form method='post'>";
	echo "<thead><tr class='header' id='row-$arr[id]'>";
		echo "<td width='10'>#</td>";
		echo "<td width='125'>Dátum</td>";
		echo "<td>Seller</td>";
		echo "<td>ID</td>";
		echo "<td>Név</td>";
		echo "<td>Színvonal</td>";
		echo "<td>Kommunikáció</td>";
		echo "<td>Tartalom</td>";	
		echo "<td>Megjelenés</td>";	
		echo "<td>Használhatóság</td>";	
		echo "<td>Általános</td>";	
		echo "<td>Timesave</td>";
		echo "<td>Visszatér</td>";	
		echo "<td>Forrás</td>";	
		echo "<td>Illetékmentes</td>";	
		echo "<td>Belföld</td>";	
		echo "<td>Ajánlaná</td>";	
		echo "<td>Komment</td>";
		
			
	echo "</tr></thead>";
	

$i=1;


while($arr = mysql_fetch_assoc($query))
{
		$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $arr[cid] LIMIt 1"));
		$agent = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $offer[agent_id] LIMIt 1"));

		if($arr[modecomment] == '')
			$arr[modecomment] = ' ';
			
		echo "<tr class='$class' id='row-$arr[id]'>";
		echo "<td width='10'>$i</td>";
		echo "<td width='125'>$arr[added]</td>";
		echo "<td>$agent[username]</td>";
		echo "<td><a href='/passengers.php?add=1&editid=$offer[id]&year=&month=' target='_blank'><b>$offer[offer_id]</b></a></td>";
		echo "<td>$offer[name]</td>";
		echo "<td align='center'>$arr[level]</td>";
		echo "<td align='center'>$arr[communication]</td>";
		echo "<td align='center'>$arr[content]</td>";	
		echo "<td align='center'>$arr[look]</td>";	
		echo "<td align='center'>$arr[usability]</td>";	
		echo "<td align='center'>$arr[general]</td>";	
		echo "<td align='center'>$arr[timesave]</td>";
		echo "<td align='center'>$arr[probability]</td>";	
		echo "<td align='center'>".str_replace("[","ő",$arr[source])."</td>";	
		echo "<td align='center'>$arr[taxfree]</td>";	
		echo "<td align='center'>$arr[freeacc]</td>";	
		echo "<td align='center'>$arr[refer]</td>";	
		echo "<td align='center'>$arr[modecomment]</td>";
		
			
	echo "</tr>";
	
	$clean = $clean + $arr[cleanliness];
	$room = $room + $arr[room];
	$pricevalue = $pricevalue + $arr[pricevalue];
	$food = $food + $arr[food];
	$service = $service + $arr[service];
	
	$i++;
}


echo "</table></div></div>";
foot();
?>