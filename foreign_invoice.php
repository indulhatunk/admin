<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();


$yr = (int)$_POST[year]; 
$inv = (int)$_POST[foreign_invoice_number];

if($yr <> '' && $inv <> '')
{
	
	echo "update $_POST[invoice_state]";
	
	if($_POST[invoice_state] == '0000-00-00 00:00:00' && $_POST[invoice] == 1)
	{
		$mysql->query("UPDATE customers SET foreign_arrival_date = NOW() WHERE foreign_invoice_number = $inv AND year(foreign_invoice_date) = $yr");
		writelog("$CURUSER[username] set invoice arrived on foreign invoice $yr/$inv");
	}
	elseif($_POST[invoice_state] == '0000-00-00 00:00:00' && $_POST[transfer] == 1)
	{
		$mysql->query("UPDATE customers SET foreign_paid_date = NOW() WHERE foreign_invoice_number = $inv AND year(foreign_invoice_date) = $yr");
		writelog("$CURUSER[username] set transfer sent on foreign invoice $yr/$inv");
	}	
	elseif($_POST[invoice_state] <> '0000-00-00 00:00:00' && $_POST[transfer] == 1)
	{
		$mysql->query("UPDATE customers SET foreign_paid_date = NOW() WHERE foreign_invoice_number = $inv AND year(foreign_invoice_date) = $yr");
		writelog("$CURUSER[username] set transfer sent on foreign invoice $yr/$inv");
	}
	

}

if($CURUSER[userclass] < 50)
	$extrapid = "AND customers.pid = $CURUSER[pid]";
else
	$extrapid = '';

	//header("location: index.php");

head('Foreign balances');

echo message($msg);
?>

<script>

$(document).ready(function() {


  $('.reseller_invoice').change(function() {
  	var invoice_id = $(this).attr("invoice_id");
  	var year = $(this).attr("year");

  	if (confirm('Biztosan megérkezett a '+year+'/'+ invoice_id+' sz. számla?')) {
    	$("#form-"+year+"-"+invoice_id).submit();
    } else {
   		return false;
   	}
  });
  
    $('.reseller_transfer').change(function() {
  	var invoice_id = $(this).attr("invoice_id");
  	var year = $(this).attr("year");

  	if (confirm('Biztosan megérkezett a '+year+'/'+ invoice_id+' sz. ellenértéke?')) {
    	$("#form-"+year+"-"+invoice_id).submit();
    } else {
   		return false;
   	}
  });

  
 }); 
 
</script>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
	
			<li><a href='?showinvoices=1' class='<? if($_GET[all] <> 1 && $_GET[showletter] <> 1) echo "current";?>'>Balance</a></li>

<? if($CURUSER[userclass] > 50) { ?>
	<li><a href='foreignstats.php'>Külföldi kimutatás</a></li>
	<li><a href='?showletter=1' class='<? if($_GET[showletter] == 1) echo "current";?>'>Levelek</a></li>
<? } ?>

		<? if($CURUSER[userclass] > 40) { ?>
				<li><a href='/foreignstats.php?accounts=1&create_invoice=1' rel='facebox'>Elszámolás elkészítése</a></li>
		<? } ?>
	</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<?
	
	if($_GET[showletter] == 1)
	{
		$lastdate = mysql_fetch_assoc($mysql->query("SELECT foreign_invoice_date FROM customers GROUP BY foreign_invoice_date ORDER BY foreign_invoice_date DESC LIMIT 1"));
		$lastdate = $lastdate[foreign_invoice_date];
		
		if($_GET[sure] <> 1)
			echo message("Biztosan el szeretné küldeni az elszámolást: <a href='?showletter=1&sure=1'>Igen!</a>   <a href='foreign_invoice.php'>Nem!</a>");
		else
			echo message("Sikeresen elküldte az elszámoást");
	}	
	
	echo "<table>";
	
	echo "<tr class='header'>";
			echo "<td>ID</td>";
			echo "<td>$lang[date]</td>";
			echo "<td><a href='?orderby=partner'>$lang[partner]</a></td>";
			echo "<td>$lang[email]</td>";
			
			if($CURUSER[userclass] > 50) { 
				echo "<td>Számla</td>";
				echo "<td>Utalás</td>";
			}
			echo "<td>Info</td>";
			echo "<td>Transfer</td>";

		echo "</tr>";


if($_GET[orderby] == 'partner')
			$orderby = 'hotel_name';
		else
			$orderby = 'customers.pid, foreign_invoice_number';



	$query = $mysql->query("SELECT * FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE foreign_invoice_number <> '' $extrapid GROUP BY foreign_invoice_number ORDER BY $orderby ASC");
	while($arr = mysql_fetch_assoc($query))
	{
		//echo "$arr[reseller_invoice] $arr[reseller_id]<hr/>";
		
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));
		$year = explode("-",$arr[foreign_invoice_date]);
		$year = $year[0];
		
		
		if($arr[foreign_paid_date] <> '0000-00-00' && $arr[foreign_arrival_date] <> '0000-00-00 00:00:00')
			$cls = 'grey';
		elseif($arr[foreign_arrival_date] <> '0000-00-00 00:00:00' && $arr[foreign_paid_date] == '0000-00-00')
			$cls = 'red';
		else
			$cls = '';
		echo "<form method='post' id='form-$year-$arr[foreign_invoice_number]'>
			<input type='hidden' name='year' value='$year'/>
			<input type='hidden' name='foreign_invoice_number' value='$arr[foreign_invoice_number]'/><tr class='$cls'>
			<input type='hidden' name='invoice_state' value='$arr[foreign_arrival_date]'/><tr class='$cls'>
			";
			
			if($partner[is_amex] == 1)	
				$amex = "<img src='/images/amex.png' width='20'/>";
			else
				$amex = '';

			echo "<td align='center'>$year/".str_pad($arr[foreign_invoice_number], 5, "0", STR_PAD_LEFT)."</td>";
			echo "<td align='center' width='80'>$arr[foreign_invoice_date]</td>";
			echo "<td>$partner[hotel_name]<br/>$partner[company_name] $amex</td>";
			echo "<td>$partner[email]</td>";
					
			if($arr[foreign_arrival_date] <> '0000-00-00 00:00:00')
			{
				$invoice_arrived = 'selected';
				$tr = '';
			}
			else	
			{
				$invoice_arrived = '';
				$tr = 'disabled';
			}
				
			if($arr[foreign_paid_date] <> '0000-00-00')
				$transfer_arrived = 'selected';
			else	
				$transfer_arrived = '';
		
		if($CURUSER[userclass] > 0) { 
			echo "<td align='center'><select name='invoice' class='reseller_invoice' invoice_id='$arr[foreign_invoice_number]' year='$year'>
				<option value='0'>számlára vár</option>
				<option value='1' $invoice_arrived>számla itt</option>
			</select></td>";
			echo "<td align='center'><select name='transfer' class='reseller_transfer' invoice_id='$arr[foreign_invoice_number]' year='$year' $tr>
				<option value='0'>kiegyenlítésre vár</option>
				<option value='1' $transfer_arrived>utalva ($arr[foreign_paid_date])</option>
			</select></td>";
		}
			echo "<td align='center'><a href='info/show_accounting_foreign.php?pid=$arr[pid]&year=$year&invoice_number=$arr[foreign_invoice_number]' rel='facebox'>[show]</a></td>";
			if($arr[foreign_paid_date] == '0000-00-00')
				$arr[foreign_paid_date] = '';
				
			echo "<td align='center'>$arr[foreign_paid_date]</td>";
		echo "</tr></form>";
		
		if($arr[foreign_invoice_date] == $lastdate && $_GET[showletter] == 1)
		{	
			
			$letter = getForeignAccount($arr[foreign_invoice_number], $year, $arr[pid]);
			
			if($_GET[sure] == 1)
			{
			sendAmazon("Hotel Outlet current balance, invoice request",$letter,"it@indulhatunk.hu",$name='',$fromemail = 'miklos.szabo@indulhatunk.hu', $fromname = 'Miklos Szabo', $source = '',1);
			
			sendAmazon("Hotel Outlet current balance, invoice request",$letter,$partner[email],$name='',$fromemail = 'miklos.szabo@indulhatunk.hu', $fromname = 'Miklos Szabo', $source = '',1);

				echo "elkuldve: $partner[email]<hr/>";
			}			
		}

	}
	echo "</table>";
	
	
	echo $letters;
	
 ?>
</div></div>
<? foot(); ?>