<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not



$db = new OCI8DB;
$dbconn = $db->dbconn();



$delete = $_POST[delete];
$default = (int)$_GET["default"];
$sure = $_GET[sure];
if(!empty($_POST[delete]))
{
	userlogin();

	foreach($_POST[delete] as $data)
	{
		$file = (int)$data;
		deleteImage($CURUSER[coredb_id],$CURUSER[coredb_package],$file);
	}
	processImages($CURUSER[coredb_id],$CURUSER[coredb_package]);
	$msg = 'Sikeresen törölte a képet!';

}

if($default > 0)
{
	userlogin();		
	setDefaultImage($CURUSER[coredb_id],$CURUSER[coredb_package],$default);
	$msg = 'Sikeresen beállította az alapértelmezett képet!';

}
if(!empty($_FILES))
{
	$result = array();

$result['time'] = date('r');
$result['addr'] = substr_replace(gethostbyaddr($_SERVER['REMOTE_ADDR']), '******', 0, 6);
$result['agent'] = $_SERVER['HTTP_USER_AGENT'];

if (count($_GET)) {
	$result['get'] = $_GET;
}
if (count($_POST)) {
	$result['post'] = $_POST;
}
if (count($_FILES)) {
	$result['files'] = $_FILES;
}

$error = false;

if (!isset($_FILES['Filedata']) || !is_uploaded_file($_FILES['Filedata']['tmp_name'])) {
	$error = 'Invalid Upload';
}


if (!$error && $_FILES['Filedata']['size'] > 2 * 1024 * 1024)
{
	$error = 'A fájlok mérete csak 2 MB-nál kevesebb lehet';
}

	$gallery = getOracleSQL("SELECT IMAGE_NUM FROM ACCOMODATION WHERE ID = '$_GET[coredb_package]'");
	$gallery = $gallery[0];
	$gallery = $gallery[IMAGE_NUM]+1;
	
	$path = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/uploaded_images/accomodation/$_GET[coredb_id]/$_GET[coredb_package]";

	if($gallery <= 9)
		$gallery = "0".$gallery;
		
	makedir("$path/orig/");
	move_uploaded_file($_FILES['Filedata']['tmp_name'], "$path/orig/$gallery.jpg");
   
if ($error) {

	$return = array(
		'status' => '0',
		'error' => $error
	);

} else {

	$return = array(
		'status' => '1',
		'name' => $_FILES['Filedata']['name']
	);

	// Our processing, we get a hash value from the file
	$return['hash'] = md5_file("$path/orig/$gallery.jpg");

	// … and if available, we get image data
	$info = @getimagesize("$path/orig/$gallery.jpg");

	if ($info) {
		$return['width'] = $info[0];
		$return['height'] = $info[1];
		$return['mime'] = $info['mime'];
	}

}

	resizeimage($_GET[coredb_id],$_GET[coredb_package],$gallery);

if (isset($_REQUEST['response']) && $_REQUEST['response'] == 'xml') {
	// header('Content-type: text/xml');

	// Really dirty, use DOM and CDATA section!
	echo '<response>';
	foreach ($return as $key => $value) {
		echo "<$key><![CDATA[$value]]></$key>";
	}
	echo '</response>';
} else {
	header('Content-type: application/json');

	echo json_encode($return);
}
	die;
}

userlogin();
head("Beállítások");

echo message($msg);
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='gallery.php' class='<? if(empty($_GET)) echo "current";?>'>Galéria</a></li>
						<li><a href='?upload=1' class='<? if($_GET[upload] == 1) echo "current";?>'>Képek feltöltése</a></li>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>
<?
if($_GET[upload] == 1)
{
	?>
	
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/mootools/1.2.2/mootools.js"></script>
	<script type="text/javascript" src="/inc/js/uploader/Swiff.Uploader.js"></script>
	<script type="text/javascript" src="/inc/js/uploader/Fx.ProgressBar.js"></script>
	<script type="text/javascript" src="http://github.com/mootools/mootools-more/raw/master/Source/Core/Lang.js"></script>
	<script type="text/javascript" src="/inc/js/uploader/FancyUpload2.js"></script>

	<script type="text/javascript">
		//<![CDATA[

window.addEvent('domready', function() { // wait for the content

	// our uploader instance 
	
	var up = new FancyUpload2($('demo-status'), $('demo-list'), { // options object
		// we console.log infos, remove that in production!!
		verbose: true,
		
		// url is read from the form, so you just have to change one place
		url: $('form-demo').action,
		
		// path to the SWF file
		path: '/inc/js/uploader/Swiff.Uploader.swf',
		
		// remove that line to select all files, or edit it, add more items
		typeFilter: {
			'Images (*.jpg, *.jpeg, *.gif, *.png)': '*.jpg; *.jpeg; *.gif; *.png'
		},
		
		// this is our browse button, *target* is overlayed with the Flash movie
		target: 'demo-browse',
		
		// graceful degradation, onLoad is only called if all went well with Flash
		onLoad: function() {
			$('demo-status').removeClass('hide'); // we show the actual UI
			$('demo-fallback').destroy(); // ... and hide the plain form
			
			// We relay the interactions with the overlayed flash to the link
			this.target.addEvents({
				click: function() {
					return false;
				},
				mouseenter: function() {
					this.addClass('hover');
				},
				mouseleave: function() {
					this.removeClass('hover');
					this.blur();
				},
				mousedown: function() {
					this.focus();
				}
			});

			// Interactions for the 2 other buttons
			
		//	$('demo-clear').addEvent('click', function() {
		//		up.remove(); // remove all files
		//		return false;
		//	});

			$('demo-upload').addEvent('click', function() {
				up.start(); // start upload
				return false;
			});
		},
		
		
		onSelectFail: function(files) {
			files.each(function(file) {
				new Element('li', {
					'class': 'validation-error',
					html: file.validationErrorMessage || file.validationError,
					title: MooTools.lang.get('FancyUpload', 'removeTitle'),
					events: {
						click: function() {
							this.destroy();
						}
					}
				}).inject(this.list, 'top');
			}, this);
		},
		
		/**
		 * This one was directly in FancyUpload2 before, the event makes it
		 * easier for you, to add your own response handling (you probably want
		 * to send something else than JSON or different items).
		 */
		onFileSuccess: function(file, response) {
			var json = new Hash(JSON.decode(response, true) || {});
			
			if (json.get('status') == '1') {
				file.element.addClass('file-success');
				file.info.set('html', '<strong>Feltöltve:</strong> ' + json.get('width') + ' x ' + json.get('height') + 'px, <em>' + json.get('mime') + '</em>)');
			} else {
				file.element.addClass('file-failed');
				file.info.set('html', '<strong>Hiba:</strong> ' + (json.get('error') ? (json.get('error') + ' #' + json.get('code')) : response));
			}
		},
		
		/**
		 * onFail is called when the Flash movie got bashed by some browser plugin
		 * like Adblock or Flashblock.
		 */
		onFail: function(error) {
			switch (error) {
				case 'hidden': // works after enabling the movie and clicking refresh
					alert('To enable the embedded uploader, unblock it in your browser and refresh (see Adblock).');
					break;
				case 'blocked': // This no *full* fail, it works after the user clicks the button
					alert('To enable the embedded uploader, enable the blocked Flash movie (see Flashblock).');
					break;
				case 'empty': // Oh oh, wrong path
					alert('A required file was not found, please be patient and we fix this.');
					break;
				case 'flash': // no flash 9+ :(
					alert('To enable the embedded uploader, install the latest Adobe Flash plugin.')
			}
		}
		
	});
	
});
		//]]>
	</script>

	<div class="container">
	<div>
	<form action="/gallery.php?coredb_id=<?=$CURUSER[coredb_id]?>&coredb_package=<?=$CURUSER[coredb_package]?>" method="post" enctype="multipart/form-data" id="form-demo">

	<fieldset id="demo-fallback">
		<legend>Képek feltöltése</legend>
		
		<label for="demo-photoupload">
			<input type="file" name="Filedata" />
		</label>
	</fieldset>

	<div id="demo-status" class="hide">
		<a href="#"  class='btn' id="demo-browse">Képek tallózása</a>

		<div>
			<strong class="overall-title"></strong><br />
			<img src="/images/progress-bar/bar.gif" class="progress overall-progress" />
		</div>
		<div>
			<strong class="current-title"></strong><br />
			<img src="/images/progress-bar/bar.gif" class="progress current-progress" />
		</div>
		<div class="current-text"></div>
	</div>

	<ul id="demo-list"></ul>

</form>		</div>
<a href="#" class='btn' id="demo-upload">Képek feltöltése</a>

	</div>
	
	
	</div></div>
	<?
	foot();
	die;
}
?>
<form method='post'>
<div class='cleaner'></div>
<?
	$gallery = getOracleSQL("SELECT IMAGE_NUM FROM ACCOMODATION WHERE ID = '$CURUSER[coredb_package]'");
	$gallery = $gallery[0];
	
	if($_GET[delete] > 0)
	{
		for($i=1;$i<$gallery[IMAGE_NUM];$i++)
		{
		}
		
	}
	echo "<div class='gallery'>";

	for($i=1;$i<$gallery[IMAGE_NUM];$i++)
	{
		if($i <= 9)
			$z = "0".$i;
		else
			$z = $i;
			
		if($i == 1)
		{
			$class = 'default';
			$default = '';
		}
		else
		{
			$class = '';
			$default = "| <a href='?default=$i' class='setdefaultimage'>borítónak</a>";
		}
		$rand = rand(1,1000000000000);
		echo "
		<div class='galleryitem $class'><a href='http://static.last-minute-belfold.hu/uploaded_images/accomodation/$CURUSER[coredb_id]/$CURUSER[coredb_package]/orig/$z.jpg' rel='facebox'><img src='http://static.last-minute-belfold.hu/uploaded_images/accomodation/$CURUSER[coredb_id]/$CURUSER[coredb_package]/137_137/$z.jpg?id=$rand'/></a><br/>
		<div style=''><input class='imagetodel' type='checkbox' value='$i' name='delete[]'>törlés $default</div></div>";
		
	}
	echo "<div class='cleaner'></div></div><hr/>";

?>
<input type='button' value='Mindent kijelöll' class='selectall'/> <input type='submit' value='Kijelölt képek törlése' class='delimage'/>
</form>

	</div></div>
<?
foot();
?>