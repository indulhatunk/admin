<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");


	userlogin();
	
	if($CURUSER[userclass] < 30)
		header("Location: index.php");
		
	head("Lealkudtuk ajánlatok");
?>
<style>
	.lealkudtukitems img { width:100px; }
	.item-img { float:left; margin:0 20px 0 0; }
	.item { clear:both; height:100px; display:block;  padding:0 0 5px 0;}
	.item-bargain { padding:0 0 10px 0; float:right; font-size:35px;}
	.item-title { font-size:15px; display:block; font-weight:bold; padding:0 0 20px 0;}
	.item-description { display:block;}
	
	.item-original-price { display:block;	}
	.item-price { display:block; }
</style>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
							<li><a href='getOffers.php'>Összes ajánlat</a></li>
							<li><a href='newsletter.php?type=outlet'>Hírlevelek</a></li>

							<li><a href='?solved=0' class='current'>Lealkudtuk.hu-n levő aktív tételek</a></li>
					</ul>


	<div class="clear"></div>
</div>
<div class='contentpadding'>

<?
$xml = new SimpleXMLElement('http://www.vatera.hu/sztarajanlat/affiliate_feed/', LIBXML_NOCDATA, true);

//debug($xml);

foreach($xml->category as $category)
{
	if($category[id] == 10015)
	{
		foreach($category->item as $item)
		{
			$name = end(explode("/",$item->name));
			
			if(strlen($name) == 3)
				$codes[$name] = (string)$item->url;
			else
				$others[$name] = (string)$item->url;
		}	
	}
	
}
	/*
	echo "<div style='' class='lealkudtukitems'><div>";
	for($i = 1;$i<15;$i++)
	{
		//echo "<div><div>";
		$body = file_get_contents("http://www.lealkudtuk.hu/udules-wellness-c1?pn=$i");
		
		$body = str_replace(' href="/',' href="http://www.lealkudtuk.hu/',$body);
		$body = str_replace('target="_parent"','target="_blank"',$body);


	
		
		$data = explode('<div id="lealkudtuk-main-itemlist">',$body);
		$data = explode('<div class="lealkudtuk-pager">',$data[1]);		

		
			if(trim($data[0]) <> '')
			{
			//	echo "<h2>$i. oldal</h2>";
			//	echo $data[0];
				
				

        preg_match_all("/\<a.*href=\"(.*?)\".*?\>(.*)\<\/a\>+/", $data[0], $matches);
				
				
		$z = 0;
		foreach($matches[2] as $m)
		{
			if($m <> '')
			{
				$code = end(explode("/",$m));
				if(strlen($code) == 3)
				{
					$codes[$code] = $matches[1][$z];
				}
				else
				{
					
					$others[$m] = $matches[1][$z];
				}	
			}
			$z++;
		}
	
				
		}
	}
*/


echo "<table>";
echo "<tr class='header'>";
	echo "<td colspan='2'>ID</td>";
	echo "<td>Hotel</td>";
	echo "<td>Vatera -tól</td>";
	echo "<td>Vatera -ig</td>";
	echo "<td>Ár</td>";
	echo "<td>%</td>";
	echo "<td>Érv.</td>";
	echo "<td>&raquo;</td>";

echo "</tr>";
$h = 1;
foreach($codes as $code => $url)
{
	
	$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE outer_name = '$code' LIMIT 1"));
	
	$count = mysql_fetch_assoc($mysql->query("SELECT count(cid) as cnt FROM customers WHERE offers_id = '$offer[id]'"));

	if($offer[sub_partner_id] > 0)
		$offer[partner_id] = $offer[sub_partner_id];
		
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$offer[partner_id]' LIMIT 1"));

	echo "<tr>";
		echo "<td width='20' align='center'>$h</td>";
		echo "<td>$offer[outer_name]</td>";
		echo "<td>$partner[hotel_name]</td>";
		echo "<td>".str_replace(" 00:00:00",'',$offer[vatera_from])."</td>";
		echo "<td>".str_replace(" 00:00:00",'',$offer[vatera_to])."</td>";
		
		echo "<td align='right'>".formatPrice($offer[actual_price])."</td>";
		
		$discount = round($offer[actual_price]/$offer[normal_price]*100);
		echo "<td align='right'>$discount%</td>";

		$diff = _date_diff($offer[vatera_to],date("Y-m-d"),'dates');
		echo "<td>$diff</td>";
		
		
		echo "<td align='right'>$count[cnt] db</td>";

			echo "<td><a href='$url' target='_blank'>tovább &raquo;</a></td>";

	echo "</tr>";
	$h++;
}

echo "</table>";

echo "<br/><br/><table>";

echo "<tr class='header'><td colspan='5'>Egyéb ajánlatok</td></tr>";

$i = 1;
foreach($others as $offer => $url)
{

		/*$b = file_get_contents("$url");
		
		$body = explode("Eladó:",$b);
		$body = explode("</a>",$body[1]);
		
		$seller = strip_tags($body[0]);

		$price = explode("Lealkudott ár:",$b);
		$price = explode('<meta itemprop="price"',$price[1]);
		$price = strip_tags($price[0]);
*/

		


echo "<tr>";
		echo "<td width='20' align='center'>$i</td>";
		echo "<td>$offer</td>";
		echo "<td>$seller</td>";
		echo "<td align='right'>$price</td>";
		echo "<td><a href='$url' target='_blank'>tovább &raquo;</a></td>";

	echo "</tr>";


$i++;
}
echo "</table>";

	echo "</div>";
	echo "</div>";
	
	echo foot();
	
?>