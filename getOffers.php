<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();


if($_GET[printid] > 0)
{
echo "<a href=\"/vouchers/print_offer.php?id=$_GET[printid]&color=blue&affil=$CURUSER[pid]&print=1\">[Kék nyomtatása]</a><br/><a href=\"/vouchers/print_offer.php?id=$_GET[printid]&color=green&affil=$CURUSER[pid]&print=1\">[Zöld nyomtatása]</a><br/><a href=\"/vouchers/print_offer.php?id=$_GET[printid]&color=red&affil=$CURUSER[pid]&print=1\">[Piros nyomtatása]</a>";
die;
}


if($_GET[notify] > 0 && $_GET[pid] > 0)
{
echo "<a href=\"/info/sendfullhouse.php?pid=$_GET[pid]&offer=$_GET[notify]\" rel='facebox'>[teltház ért.]</a><br/><a href=\"/info/sendfullhouse.php?pid=$_GET[pid]&offer=$_GET[notify]&contract=1\" rel='facebox'>[info ért.]</a><br/>";
die;
}




if($_GET[orderby] == 'validity')
{
	$orderby = "offers.validity";
}
if($_GET[orderby] == 'conversion')
{
	$orderby = "offers.an_conversion_rate";
}
else
	$orderby = "offers.active,partners.hotel_name";
	
if($_POST[search] <> '' && $CURUSER[userclass] > 90)
		$extrareseller = "AND outer_name = '".strtolower($_POST[search])."'";
elseif($CURUSER[extra_condition] <> '')
		$extrareseller = "$CURUSER[extra_condition] ";
elseif($CURUSER[userclass] < 30)
	$extrareseller = "AND (vatera_to < NOW()) AND start_date < NOW() AND end_date > NOW() AND affiliate_lira <> 1  AND affiliate_lajos <> 1 AND pid <> 3121 ";

elseif($CURUSER[abroad] == 1)
	$extrareseller = 'AND  offers.start_date <= NOW() AND offers.end_date > NOW() AND affiliate_lira = 0';
elseif($_GET[active] == 'all')
{
	if($_GET[year] == '')
		$extrareseller = 'AND year(offers.added) = '.date("Y");
	if($_GET[year] > 0)
		$extrareseller = "AND year(offers.added) = $_GET[year]";
	else
		$extrareseller = '';
}
else
	$extrareseller = 'AND active = 1 AND offers.end_date > NOW()';
	//$extrareseller = 'AND offers.start_date <= NOW() AND offers.end_date > NOW() ';

if($_GET[cop] == 1 && $_GET[sure] <> 1)
{

	die("<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan másolni szeretné a <b><font color='red'>$_GET[outer_name]</font></b> jelű ajánlatot?</h1><br/>  <a href=\"?editid=$_GET[editid]&cop=1&parent=$_GET[parent]&&sure=1\" class='button green'>$lang[yes]</a> <a href=\"customers.php\" class='button red closefacebox'>$lang[no]</a><div class='cleaner'></div>");
	
}
if($_GET[cop] == 1 && $_GET[editid] > 0 && $_GET[sure] == 1)
{

	$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = '$_GET[editid]' LIMIT 1"));
	
	
	$oname = generateOuterName();
	
	$check = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE outer_name = '$oname' LIMIT 1"));
		
	if($check[id] > 0)
	{
		die("Létező sorszám! Frissíts az oldalon!");
	}
	else
	{
		
		$offer[outer_name] = $oname;
		$offer[id] = '';
		$_POST[pdf_image] = 0;
		$offer[offer_comment] = '';
		$offer[quantity_sold] = 0;
		$offer[parent_id] = $_GET['parent'];
		$offer[added] = 'NOW()';
		
		
		$partnerdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$offer[partner_id]' LIMIT 1"));
	
	if($partnerdata[is_qr] == 1)
		$offer[is_qr] = 1;


		$mysql->query_insert("offers",$offer);
		$id = mysql_insert_id();


		writelog("$CURUSER[username] copied offer $offer[parent_id] > $id");
		
		header("Location: /getOffers.php?editid=$id&add=1");
			die;
	}
	

}
if($CURUSER[userclass] >= 40)
{

$name = $_POST[name];
$editID = (int)$_POST[id];

if($_POST[facebook] <> 1)
	$_POST[facebook] = 0;
	
if($_POST[abroad] == 'on')
	$_POST[abroad] = 1;
else
	$_POST[abroad] = 0; 

if($_POST[last_minute] == 'on')
	$_POST[last_minute] = 1;
else
	$_POST[last_minute] = 0; 
	
if($_POST[no_plus_one_day] == 'on')
	$_POST[no_plus_one_day] = 1;
else
	$_POST[no_plus_one_day] = 0; 
	
/*if($_POST[reseller_enabled] == 'on')
	$_POST[reseller_enabled] = 1;
else
	$_POST[reseller_enabled] = 0; 
*/	

if($_POST[affiliate_lajos] == 1)
{
	$lajosprice = $_POST[reseller_price]-$_POST[reseller_price]*0.07;

	$wholethousand = round($lajosprice,-3);

	if($wholethousand > $lajosprice)
		$wholethousand = $wholethousand-1000;
	
		$remainder = $lajosprice - $wholethousand;


		if($remainder < 490)
			$lajosvalue = $wholethousand+490;
		else
			$lajosvalue = $wholethousand+990;

	$_POST[lajos_price] = $lajosvalue; 
}



$_POST[promo_codes] = serialize($_POST[promo_codes]);
$_POST[categories] = serialize($_POST[categories]);

//unset($_POST[promotion]);

if($_POST[normal_price] > 0)
	$_POST[discount_percent] = 100-round($_POST[actual_price]/$_POST[normal_price]*100);
	

if($name <> '' && $editID == '')
{
	$_POST[added] = 'NOW()';
	
//	$_POST[company_invoice] = $_POST[company_invoice];

	//check qr code
	$partnerdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$_POST[partner_id]' LIMIT 1"));
	
	if($partnerdata[is_qr] == 1)
		$_POST[is_qr] = 1;
		
		
	$mysql->query_insert("offers",$_POST);
	$id = mysql_insert_id();
	
	writelog("$CURUSER[username] inserted offer #$id");

	$msg = "Sikeresen beillesztette!";
	
	header("Location: /getOffers.php?editid=$id&add=1");

}
if($name <> '' && $editID <> '')
{

for($i = 1;$i<=9;$i++)
{	
	if($_FILES["file$i"]["name"] <> '')
	{
		makedir("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$_POST[id]");
		
      if(move_uploaded_file($_FILES["file$i"]["tmp_name"],"/var/www/lighttpd/admin.indulhatunk.info/images/offers/$_POST[id]/$i.jpg"))
     	  $msg.= "Kép feltöltve!";
      else
      	$msg.= "hiba a Kép feltöltése közben";
    	
	  $_POST[pdf_image] = 1;  	 
   	   writelog("$CURUSER[username] uploaded an image for offer #$_POST[id] / $i");
	}
}

	$editcheck = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = $_POST[id]"));
	
	if($editcheck[active] == 1 && $_POST[active] == 0)
		$_POST[inactivate_date] = 'NOW()';
		
	$_POST[last_update] = 'NOW()';

	//print_r($_POST);
	$mysql->query_update("offers",$_POST,"id=$_POST[id]");
	$msg = "Sikeresen szerkesztette!";
	
   writelog("$CURUSER[username] edited offer #$_POST[id]");

	$id = $_POST[id];
	
	header("Location: /getOffers.php?editid=$id&add=1");
}

/*
if(!empty($promotion))
{
		$mysql->query("DELETE FROM offers_promotion WHERE offer_id = $id");
		
		foreach($promotion as $promo)
		{
			$pr = array();
			$pr[offer_id] = $id;
			$pr[promotion_id] = $promo;
			$mysql->query_insert("offers_promotion",$pr);
		}
}
*/
}


if($_GET[add] <> 1)	
	head("Ajánlatok megtekintése");
else
{
	$editid = (int)$_REQUEST[editid];

	if($editid > 0 )
	{
		$editQr = mysql_query("SELECT * FROM offers WHERE id = $editid");
		$editarr = mysql_fetch_assoc($editQr);
	
		$pdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $editarr[partner_id] LIMIT 1"));
	
	}
	head("$editarr[outer_name] - Ajánlatok megtekintése",1);
}

echo message($msg);

?>
<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,source,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote",
			theme_advanced_buttons2 : "link,unlink,anchor,image,cleanup,code,|,forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,iespell,media,advhr,|,visualchars",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
extended_valid_elements: "iframe[class|src|frameborder=0|alt|title|width|height|align|name]",
			// Example content CSS (should be your site CSS)
			//content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>

<?
$add = $_GET[add];
$translate = $_GET[translate];


if($add == 1)
{
	$show = '';
}
else
	$show = 'display:none';
	
	
if($translate == 1)
{
	$hidetranslate = 'display:none';
}
else
	$hidetranslate = '';
?>

<? if($CURUSER[userclass] > 90) { ?>
<fieldset style="border:1px solid black;-moz-opacity:0.8;opacity:0.80;width:215px;height:24px;padding:5px;position:fixed;top:10px;right:10px;background-color:white;">
<form method="post" id="sform" action="getOffers.php">
<input type="text" name="search"  style='height:20px;width:150px;margin:1px 5px 0 0;padding:0;float:left;' value="<?=$cookie?>" autofocus/><a href="#"; style='display:block; background:none; color:black; border:1px solid black; height:10px;padding:5px; margin:1px 0 0 0;float:left;width:30px;float:left;' id='submit'>Ok</a>
</form>
</fieldset>
<? } ?>

<? if($_GET[add] == 1) {
	echo "<div style='padding:5px'>";
	}
	else{?>

<div class='content-box'>

<div class='content-box-header'>
					<ul class="content-box-tabs">
					
	<? if($CURUSER[userclass] >= 40) { ?>
		<li><a href="?add=1" class="<? if($_GET[add] == 1) echo "current";?>">Új</a></li>
	<!--	<li><a href="?active=reseller" class="<? if($_GET[active] == 'reseller') echo "current";?>">Viszonetaldói ajánlatok</a></li>-->
		<li><a href="?active=vatera" class="<? if($_GET[active] == 'vatera') echo "current";?>">Vatera</a></li>
		<li><a href="?active=outlet" class="<? if($_GET[active] == 'outlet') echo "current";?>">Outlet</a></li>
		<li><a href="?active=ro" class="<? if($_GET[active] == 'ro') echo "current";?>">Outlet.ro</a></li>
		<!--<li><a href="?active=closed" class="<? if($_GET[active] == 'closed') echo "current";?>">Zárt</a></li>-->
		<li><a href="?active=sk" class="<? if($_GET[active] == 'sk') echo "current";?>">Outlet.sk</a></li>
		<!--<li><a href="?active=okazii" class="<? if($_GET[active] == 'okazii') echo "current";?>">Okazii</a></li>
		<li><a href="?active=affiliate" class="<? if($_GET[active] == 'affiliate') echo "current";?>">Affiliate</a></li>-->
		<li><a href="getLealkudtuk.php" class="" target='_blank'><b>Lealkudtuk</b></a></li>
		<li><a href="?showfree=1" class="<? if($_GET[showfree] == 1) echo "current";?>">Kapacitás</a></li>

	<? } ?>

		<li><a href="?active=1" class="<? if($_GET[active] == 1 || ($_GET[active] == '' && $_GET[add] <> 1) && $_GET[showfree] <> 1) echo "current";?>"><?=$lang[activeoffers]?></a></li>
		

		<li><a href="?active=all" class="<? if($_GET[active] == 'all') echo "current";?>"><?=$lang[alloffers]?></a></li>
		
	</ul>
					<div class="clear"></div>
</div>

<div class='contentpadding'>
<? } ?>
<?
if($_GET[showfree] == 1)
{
?>
<form>

<table style='width:360px;margin:0 auto;'>
<tr>
	<td class='lablerow'>3 nap 2 éjes csomagok</td>
	<td><input type='hidden' name='showfree' value='1'/>
		<input type='text' name='from_date' class='dpick' placeholder='Érkezés időpontja' value='<?=$_GET[from_date]?>'/></td>
</tr>
<tr>
	<td colspan='2' class='lablerow' align='center'><input type='submit' value='Ellenőrzés &raquo;'/></td>
</tr>
</table>
<hr/>
<?
	$offers = mysql_query("SELECT * FROM offers WHERE ((start_date <= NOW() AND end_date >= NOW()) OR (vatera_from <= NOW() AND vatera_to >= NOW())) AND active  = 1 ORDER BY actual_price ASC");
	echo "<table>";
	
	echo "<tr class='header'>";
			echo "<td>Hotel</td>";
			echo "<td>Érvényesség</td>";

			echo "<td>Ár</td>";
			echo "<td>1. típus</td>";
			echo "<td>2. típus</td>";
			echo "<td>3. típus</td>";
			echo "<td>Frissítve</td>";	
			echo "<td>&raquo;</td>";
			
			
		echo "</tr>";
		
		
	while($arr = mysql_fetch_assoc($offers))
	{
		if($arr[sub_partner_id] > 0)
			$arr[partner_id] = $arr[sub_partner_id];
		
	
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[partner_id]"));
	
	if($_GET[from_date] <> '')
	{
		$todate =  date('Y-m-d', strtotime($_GET[from_date]. ' +1 days'));
		
		$check1 =  mysql_fetch_assoc($mysql->query("SELECT * FROM fullhouse WHERE pid = $arr[partner_id] AND (date = '$_GET[from_date]' or date = '$todate') AND roomtype = 0 LIMIT 1"));
		$check2 =  mysql_fetch_assoc($mysql->query("SELECT * FROM fullhouse WHERE pid = $arr[partner_id] AND (date = '$_GET[from_date]'  or date = '$todate') AND roomtype = 1 LIMIT 1"));
		$check3 =  mysql_fetch_assoc($mysql->query("SELECT * FROM fullhouse WHERE pid = $arr[partner_id] AND (date = '$_GET[from_date]'  or date = '$todate') AND roomtype = 2 LIMIT 1"));

		if($check1[id] <> '')
			$class1 = 'red';
		else
			$class1 = 'green';
			
		if($check2[id] <> '')
			$class2 = 'red';
		else
			$class2 = 'green';
			
		if($check3[id] <> '')
			$class3 = 'red';
		else
			$class3 = 'green';
			
	}
	$lastupdate =  mysql_fetch_assoc($mysql->query("SELECT * FROM fullhouse WHERE pid = $arr[partner_id] ORDER BY added DESC limit 1"));
	$lupdate = explode(" ",$lastupdate[added]);
	
	
	if((strtotime($lupdate[0]) < strtotime('10 days ago')))
		$updateclass = 'red';
	else
		$updateclass = '';
		
		
	if((strtotime($arr[expiration_date]) < strtotime($_GET[from_date])))
		$expclass = 'red';
	else
		$expclass = '';
		
		echo "<tr>";
			echo "<td><a href='http://www.szallasoutlet.hu/".clean_url2($partner[city])."/".clean_url2($partner[hotel_name])."/".clean_url2($arr[name])."+$arr[id]?utm_source' target='_blank'><b>$partner[hotel_name]</b></a></td>";
			echo "<td class='$expclass'>$arr[expiration_date]</td>";

			echo "<td align='right'>".formatPrice($arr[actual_price])."</td>";
			echo "<td class='$class1'>$partner[roomtype1_name]</td>";
			echo "<td class='$class2'>$partner[roomtype2_name]</td>";
			echo "<td class='$class3'>$partner[roomtype3_name]</td>";
			echo "<td class='$updateclass'>$lupdate[0]</td>";	
			echo "<td><a href='http://admin.indulhatunk.hu/fullhouse/$partner[pid].jpg' target='_blank'>&raquo;</a></td>";
			
			
		echo "</tr>";
	}
	echo "</table></form>";
	echo "</div></div>";
	echo foot();
	die;
}

?>

<div style='<?=$show?>'>
<form method="post" action="getOffers.php" enctype="multipart/form-data" id='offerform'>
<div style='height:30px;background-color:#e7e7e7;opacity:0.7; border-top:1px solid #c4c4c4;bottom:0;position:fixed;width:100%;margin-left:-11px;padding:0;text-align:center;padding:3px 0 3px 0;'>
	
</div>


<script>
$().ready(function() {
	$("#validatedata").click(function(){
		var body  = $("#templateBody").val();
		var dayNum = $("#dayNum").val()*1;
		var expiration = $("#expiration_date").val();
		
		var inputDate = new Date(expiration);
		var today = new Date();

		

//Get today's date
//

		
		$("#errors").html('');
		
		var findme = '/more.jpg) no-repeat; width: 715px; height: 120px;">&nbsp;</div>';

		if ( body.indexOf(findme) > -1 ) {
				$("#errors").append('<font color="white">- OK: /more.jpg) no-repeat; width: 715px; height: 120px;"&gt;&amp;nbsp;&lt;/div&gt; </font><br/>');
			} else {
				$("#errors").append('<font color="c0392b"><b>- Hiányzik: /more.jpg) no-repeat; width: 715px; height: 120px;"&gt;&amp;nbsp;&lt;/div&gt;</b></font> <br/>');
		}

	var findme = '<strong>Az aj&aacute;nlat tartalma</strong>';

		if ( body.indexOf(findme) > -1 ) {
				$("#errors").append('<font color="white">- OK: &lt;strong>Az aj&amp;aacute;nlat tartalma&lt&lt;/strong></font><br/>');
			} else {
				$("#errors").append('<font color="c0392b"><b>- Hiányzik: &lt;strong>Az aj&amp;aacute;nlat tartalma&lt;/strong></b></font><br/>');
		}
		
		if(dayNum == 0) {
			$("#errors").append('<font color="c0392b"><b>- Hiányzik: Éjek száma</b></font><br/>');	
		} else {
			$("#errors").append('<font color="white">- OK: Éjek száma</font><br/>');
		}


		if(inputDate.setHours(0,0,0,0) < today.setHours(0,0,0,0))
		{
			$("#errors").append('<font color="c0392b"><b>- Hiányzik: A múltban van a lejárat dátuma, nem lesz teltházas lista!</b></font><br/>');	
		} else {
			$("#errors").append('<font color="white">- OK: Lejárat dátuma</font><br/>');
		}

		$("#errors").fadeIn();
		
		$('#errors').delay(5000).fadeOut(400)
	//alert(body);
		
		///templateBody2
			//alert('aaa');
		
	});
	
	
	
	
	
	$("#moredates").click(function() {
		$("#morerow").fadeOut();
		$(".moredates").fadeIn();
		return false; 
	});
	
	$("#partnerID").change(function() {
	
		if($("#spartnerID").val()*1 == 0)
		{
			if($('option:selected', this).attr('foreign')*1 == 1)
				$("#abroad").prop('checked', true);
			else
				$("#abroad").prop('checked', false);
		}
		
	});
	
	$("#spartnerID").change(function() {
	
		//if($("spartnerID").val()*1 == 0)
		//{
			if($('option:selected', this).attr('foreign')*1 == 1)
				$("#abroad").prop('checked', true);
			else
				$("#abroad").prop('checked', false);
		//}
		
	});
	
	$("#contract_type").change(function() {
		
		if($(this).val()*1 == 1)
			$(".isagency").fadeIn();
		else
			$(".isagency").fadeOut();
	});


});


</script>

<? 

if($editid > 0 )
{

/*
	$editQr = mysql_query("SELECT * FROM offers WHERE id = $editid");
	$editarr = mysql_fetch_assoc($editQr);
	
	$pdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $editarr[partner_id] LIMIT 1"));

*/
	if($_GET[cop] <> 1)
		echo "<input type=\"hidden\" name=\"id\" value=\"$editarr[id]\"/>";
}


if($editarr[language] == 'hu' || $editarr[language] == '')
		$sh = 'display:none';
	else
		$sh = '';

if($editarr[contract_type] == '0' || $editarr[contract_type] == '')
		$sh2 = 'display:none';
	else
		$sh2 = '';
	
if($editarr[pdf_title] == '')
		$sh3 = 'display:none';
	else
		$sh3 = '';
?>

<style>
	.trow { background-color:#e7e7e7; font-weight:bold; text-align:center; }
	.moredates { display:none; }
	.lselect { width:200px; }
	.required { background-color:#e7e7e7; font-weight:bold;}
	.en { <?=$sh?> }
	.isagency { <?=$sh2?> }
	.grrow { background-color:#e7e7e7;}
	.trbox {  width:250px !important; margin:0;height:70px; border:1px solid #d5d5d5; font-family:arial; font-size:12px;}
	.sep { height:3px; background-color:#e7e7e7; }
	.pdfdata { <?=$sh3?>}
	.maskeddate { /*font-size:10px !important;*/ width:110px !important; }
	body { background-color:#ecf0f1 !important;}
	table { background-color:white; }
</style>
<div id='errors' style='display:none; border-top:1px solid #27ae60; background-color:#2ecc71;padding:10px;bottom:37px;position:fixed;width:100%;margin-left:-11px;font-size:11px;text-align:left;'></div>


<div style='height:30px;bottom:0;position:fixed;width:100%;margin-left:-11px;padding:0;text-align:center;padding:3px 0 3px 0;'>
<input type="button" value="Ellenőrzés" id='validatedata'/>&raquo;<input type="submit" value="Mentés" />
</div>

<div style="width:1220px;">


	
<?


$partnerQuery = mysql_query("SELECT pid,hotel_name,tid, country FROM partners INNER JOIN templates ON templates.partnerID = partners.pid WHERE userclass < 100 order by partners.hotel_name asc");

if($editarr[outer_name]=='' || $_GET[cop] == 1) 
	{
		$oname = generateOuterName();
		//check
		$check = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE outer_name = '$oname' LIMIT 1"));
		
		$parent = $_GET[parent];
		if($check[id] > 0)
		{
			$checkmark = '<img src="/images/cross.png" width="25"/>';
		}
	}	
	else 
	{
	
		if($editarr[parent_id] == 0)
		{
			$c = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE partner_id = '$editarr[partner_id]' AND id <> '$editarr[id]' AND id < '$editarr[id]' ORDER BY id DESC LIMIT 1"));
			$editarr[parent_id] = $c[id];
		}
		$parent = $editarr[parent_id];
		$oname =  $editarr[outer_name];
		$prnt= "/ <span style='font-size:10px;'><a href='/getOffers.php?editid=$editarr[parent_id]&add=1' target='_blank'>".$editarr[parent_id] ." másolata</a></span>";
	}
?>



<table style='width:100%'>
	<tr class='header'><td><a href='/getOffers.php'>&laquo; Vissza</a></td><td colspan='3'>Ajánlat szerkesztése</td></tr>
	<tr><td class="required">Azonosító</td><td><input type="hidden" name="outer_name" value="<?=$oname?>"/><?=$oname?> <?=$checkmark?>  / <?echo "$editarr[id] $prnt"?><input type="hidden" name="parent_id"  value="<?=$parent?>"/></td>
	<td class='en grrow' style='font-weight:bold;text-align:center;'>Beszerzési adatok</td>
	<td rowspan='52' valign='top' width='720'>
		<!-- -->
			
	<table style='width:720px;float:right;'>
		<tr>
		<td  width='745' valign='top'><textarea name="main_description" rows="100" cols="20" style="width: 740px" class="tinymce"  id="templateBody"><?=$editarr[main_description]?></textarea></td>
		</tr>
		
				<?
	if($editarr[id] > 0)
	{
		if($editarr[sub_partner_id] > 0)	
			$spid = $editarr[sub_partner_id];
		else
			$spid = $editarr[partner_id];
			
		$checkpartner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $spid LIMIT 1"));
		if(@fopen("../szallasoutlet.hu/images/logo/$checkpartner[coredb_id].jpg",'r') == TRUE)
			$logo = "<a href='http://www.szallasoutlet.hu/images/logo/$checkpartner[coredb_id].jpg' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='15'/></a>";
		else
			$logo = "<a href='/images.php?id=$pdata[coredb_id]' target='_blank'><img src='http://admin.indulhatunk.info/images/cross1.png' width='15'/></a>";
		
		
		if(@fopen("vouchers/partners2/$checkpartner[coredb_id].jpg",'r') == TRUE)
			$voucher = "<a href='http://admin.indulhatunk.info/vouchers/partners2/$checkpartner[coredb_id].jpg' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='15'/></a>";
		else
			$voucher = "<a href='/images.php?id=$checkpartner[coredb_id]' target='_blank'><img src='http://admin.indulhatunk.info/images/cross1.png' width='15'/></a>";
			
			
		$catlist = array("property_check" => "SZÉP kártya","property_balaton" => "Balaton", "property_wellness" => "Wellness", "property_child"=> "Gyermekbarát","property_mountain" => "Hegyvidék","property_water"=> "Vízpart","property_castle"=> "Kastélyszálló","property_spa"=> "Gyógyfürdő");
		
		foreach($catlist as $cat => $ttl)
		{
			if($checkpartner[$cat] == 1)
			{
				$ctlist.="$ttl, ";
			}
			
		}
		
		if($checkpartner[latitude] <> '')
		{
			$gcls = '';
			$glink = "van";
		}
		else
		{
			$gcls = 'red';
			$glink = "<a href=''></a>";
		}
		
		if($ctlist == '')
			$ctcat = 'red';				
		echo "<tr><td>
		<table>
			<tr class='trow'><td width='80'>Logo</td><td width='80'>Voucher kép</td><td width='80'>Galéria</td><td width='80'>GPS</td><td>Kategória</td></tr>
			<tr><td align='center'>$logo</td><td align='center'>$voucher</td><td align='center'><a href='http://admin.indulhatunk.hu/accomodation.php?id=$pdata[coredb_id]&add=1' target='_blank'>$pdata[image_num] db &raquo;</a></td>
			<td class='$gcls' align='center'>$glink</td>
			<td class='$ctcat'>$ctlist (<a href='/partners.php?edit=$checkpartner[pid]' style='font-weight:bold' target='_blank'>+</a>)</td></tr>	
		</table>
		</td></tr>";
	}
?>
<tr>
<td>
	<?

 if($editarr[id] > 0) { ?>
<table style='width:100%;margin:0 auto;'>
<tr class='required'><td colspan='3'>PDF adatok <? if($editarr[pdf_title] == '') echo "<a href='#' id='showpdf'>[kitöltés]</a>"?><a class='pdfdata' href='/vouchers/print_offer.php?id=<?=$editarr[id]?>&color=red&affil=3&print=1'>[előnézet]</a></td>
</tr>
<tr class='pdfdata'><td class='required'>Cím</td><td><input type="text" class="long" name="pdf_title" value="<?=$editarr[pdf_title]?>" style='width:230px;'/></td>
<td rowspan='4'>
	<table>

<?		
if($editarr[pdf_image] == 0)	{
	$image = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE partner_id = $editarr[partner_id] AND pdf_image = 1 ORDER BY id DESC LIMIT 1"));
?>	
<style>
	.pdfphotos { display:none;}
</style>
<tr id='pdf_photo'>
	<td colspan='2'>
		Korábbi ajánlat képtárának használata <a href='#' id='pdf_photos'>[új képek feltöltése]</a>
	</td>
</tr>
<? } ?>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/1.jpg")) {  echo "<a href='images/offers/$editarr[id]/1.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>1. kép (800x447px)</td><td><input type="file" name="file1" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/2.jpg")) {  echo "<a href='images/offers/$editarr[id]/2.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>2. kép (145x110px)</td><td><input type="file" name="file2" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/3.jpg")) {  echo "<a href='images/offers/$editarr[id]/3.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>3. kép (145x110px)</td><td><input type="file" name="file3" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/4.jpg")) {  echo "<a href='images/offers/$editarr[id]/4.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>4. kép (145x110px)</td><td><input type="file" name="file4" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/5.jpg")) {  echo "<a href='images/offers/$editarr[id]/5.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>5. kép (145x110px)</td><td><input type="file" name="file5" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/6.jpg")) {  echo "<a href='images/offers/$editarr[id]/6.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>6. kép (145x110px)</td><td><input type="file" name="file6" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/7.jpg")) {  echo "<a href='images/offers/$editarr[id]/7.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>Nyitó (690x493px)</td><td><input type="file" name="file7" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/8.jpg")) {  echo "<a href='images/offers/$editarr[id]/8.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>XML1 (574x298)</td><td><input type="file" name="file8" id="file" /> 
</td></tr>
<tr class='pdfphotos'><td class='required'><? if (file_exists("/var/www/lighttpd/admin.indulhatunk.info/images/offers/$editarr[id]/9.jpg")) {  echo "<a href='images/offers/$editarr[id]/9.jpg' rel='facebox'><img src='/images/check1.png' width='15'/></a>"; }  ?>XML 2 (572x283)</td><td><input type="file" name="file9" id="file" /> 
</td></tr>
</table>
</td></tr>
<tr class='pdfdata'><td class='required'>Alcím</td><td><input type="text" class="long" name="pdf_short_title" value="<?=$editarr[pdf_short_title]?>" style='width:230px;'/></td></tr>
<tr class='pdfdata'><td class='required'>Ajánlat neve</td><td><input type="text" class="long" name="pdf_offer_name" value="<?=$editarr[pdf_offer_name]?>" style='width:230px;'/></td></tr>
<tr class='pdfdata'><td class='required'>Leírás</td><td><textarea name="pdf_description"  class="trbox" style='width:300px !important;height:150px'><?=$editarr[pdf_description]?></textarea></td></tr>


</table>

<!--
<fieldset>
	<legend>Affiliate adatok</legend>
	<ul>
	<li><label>Líra</label>
		<select name="affiliate_lira">
			<option value="1" <?if($editarr[affiliate_lira]==1)echo"selected";?>>igen</option>
			<option value="0" <?if($editarr[affiliate_lira]==0)echo"selected";?>>nem</option>
		</select>
	</li>
	</ul>
</fieldset>
-->

<!--
<table>
	<tr class='required'><td colspan='2'>Képek</td></tr>
	<? for($i=1;$i<=10;$i++) { ?>
	<tr>
		<td class='lablerow'><?=$i?>. kép</td>
		<td><input type="text" name="image<?=$i?>" value="<?=$editarr["image".$i]?>" style='width:530px !important'/></td>
	</tr>
	<? } ?>
</table>-->
<? } ?>

</td>
</tr>

<? if($editarr[id] > 0) { ?>
<tr>
	<td id='logdata'>
	
		
	
	</td>
</tr>


<tr>
	<td id='owndata'>
	
	</td>
</tr>
<? } ?>
	</table>
		<!-- -->
		
	</td>
	</tr>

<tr><td class="required">Értékesítő cég</td><td>
<select name="company_invoice" class='lselect' style="width:250px;">
	
	<? echo getCompany($editarr[company_invoice], 'select'); ?>
	<!--	<option value="szallasoutlet" <?if($editarr[company_invoice]=='szallasoutlet')echo"selected";?>>SzállásOutlet Kft.</option>
		<option value="hoteloutlet" <?if($editarr[company_invoice]=='hoteloutlet')echo"selected";?>>HotelOutlet Kft.</option>
		<option value="indusz" <?if($editarr[company_invoice]=='indusz')echo"selected";?>>Indulhatunk Utazásszervező Kft.</option>
		<option value="indulhatunk" <?if($editarr[company_invoice]=='indulhatunk')echo"selected";?>>Indulhatunk Média Kft.</option> -->
	</select>
</td><td class='en grrow' rowspan='7'></td></tr>

<tr><td class="required">Partner</td><td><select name="partner_id" style="width:250px;" id="partnerID">
	<option value="0">Válasszon</option>
	<?
		while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
			if($editarr[partner_id] == $partnerArr[pid])
				$selected = "selected";
			else
				$selected = '';
				
			if($partnerArr[country] <> 'hu')
				$foreign = 1;
			else
				$foreign = 0;
			echo "<option value=\"$partnerArr[pid]\" foreign='$foreign' $selected>$partnerArr[hotel_name] $partnerArr[pid] </option>\n";
		}
	?>
</select></td>
</tr>
<tr><td class="required">Alpartner</td><td><select name="sub_partner_id" style="width:250px;" id="spartnerID">
	<option value="0">Válasszon</option>
	<?
	$partnerQuery = mysql_query("SELECT pid,hotel_name,country FROM partners  WHERE userclass < 100  order by partners.hotel_name asc");

		while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
			if($editarr[sub_partner_id] == $partnerArr[pid])
				$selected = "selected";
			else
				$selected = '';
				
			if($partnerArr[country] <> 'hu')
				$foreign = 1;
			else
				$foreign = 0;
				
			echo "<option value=\"$partnerArr[pid]\" foreign='$foreign' $selected>$partnerArr[hotel_name] $partnerArr[pid] </option>\n";
		}
	?>
</select></td></tr>
<tr><td class="required">Külföldi ajánlat?</td><td><input type="checkbox" name="abroad" id='abroad' <?if($editarr[abroad] == 1) echo "checked";?>/> <!--<input type="text" name="country" id="country"  placeholder='Ország neve' value="<?=$editarr[country]?>"/> -->

	<select name='currency' id='fc_curr'>	
		<option value='HUF' <? if($editarr[currency]=='' || $editarr[currency] == 'HUF') echo "selected"; ?>>HUF alap</option>
		<option value='EUR' <? if($editarr[currency] == 'EUR') echo "selected"; ?>>EUR alap</option>
		<option value='CZK' <? if($editarr[currency] == 'CZK') echo "selected"; ?>>CZK alap</option>
		<option value='RON' <? if($editarr[currency] == 'RON') echo "selected"; ?>>RON alap</option>

	</select>
	<select name="language" id='offerlanguage'>
		<option value="hu" <?if($editarr[language]=="hu")echo"selected";?>>magyar utalvány</option>
		<option value="en" <?if($editarr[language]=="en")echo"selected";?>>angol utalvány</option>
	</select>


</td></tr>
<tr><td class="required">Last Minute</td><td>
	<input type="checkbox" name="last_minute" id='last_minute' <?if($editarr[last_minute] == 1) echo "checked";?>/>
	</td></tr>
<tr><td class="required">Aktív</td><td>
	<select name="active" class='lselect'>
		<option value="1" <?if($editarr[active]==1)echo"selected";?>>Igen</option>
		<option value="0" <?if($editarr[active]==0)echo"selected";?>>Nem</option>
	</select></td></tr>
<tr><td class="required">Mennyiség <a href='#' class='tooltip' title='0 = korlátlan'>(i)</a></td><td><input type="text" name="quantity" class="numeric formCalculate2" value="<?=$editarr[quantity]?>"/> db / <?=$editarr[quantity_sold]?> eladott </td></tr>



<!--<tr><td class="required">Joker</td><td>
<select name='joker'>
	<option value='0' <? if($editarr[joker] == 0) echo "selected"; ?>>nem</option>
	<option value='20' <? if($editarr[joker] == 20) echo "selected"; ?>>20 %</option>

</select></td></tr>-->


<tr><td class="required">Éjszakák száma</td><td><input type="text" name="days" class="numeric" id="dayNum" value="<? if($editarr[days] == '') echo 2; else echo $editarr[days];?>"/> éj  <input type="checkbox" name="no_plus_one_day" <?if($editarr[no_plus_one_day]==1) echo "checked";?> /> Nincs +1 nap</td></tr>


<tr><td class="required">Ajánlat neve</td><td><input type="text" class="long" name="name" value="<?=$editarr[name]?>" style='width:230px;'/></td>
<td class='en'><input type="text" class="long" name="en_name" value="<?=$editarr[en_name]?>"/></td></tr>
<tr><td class="required">Ajánlat rövid neve</td><td><input type="text"  class="long" name="shortname" value="<?=$editarr[shortname]?>"  style='width:230px;'/></td>
<td class='en'><input type="text"  class="long" name="en_shortname" value="<?=$editarr[en_shortname]?>"/></td></tr>


<tr><td class="required">Normál ár</td><td><input type="text" name="normal_price" class="numeric" value="<?=$editarr[normal_price]?>"/> Ft</td>
<td class='en'><input type="text" name="fc_normal_price" class="numeric" value="<?=$editarr[fc_normal_price]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>



<tr><td class="required">Kedvezményes ár</td><td><input type="text" name="actual_price" class="numeric" value="<?=$editarr[actual_price]?>"/> Ft 
<select name="price_category" style='width:130px;'>
		<option value="0" <?if($editarr['price_category']==0) echo "selected";?>>Árkategória</option>
		<option value="19" <?if($editarr['price_category']==19) echo "selected";?>>10 000 Ft alatt</option>
		<option value="1" <?if($editarr['price_category']==1) echo "selected";?>>10-15 000 Ft - regg.</option>
		<option value="2" <?if($editarr['price_category']==2) echo "selected";?>>10-15 000 Ft - fp.</option>
		<option value="3" <?if($editarr['price_category']==3) echo "selected";?>>15-20 000 Ft - regg.</option>
		<option value="4" <?if($editarr['price_category']==4) echo "selected";?>>15-20 000 Ft - fp.</option>
		<option value="5" <?if($editarr['price_category']==5) echo "selected";?>>20-25 000 Ft - regg.</option>
		<option value="6" <?if($editarr['price_category']==6) echo "selected";?>>20-25 000 Ft - fp.</option>
		<option value="7" <?if($editarr['price_category']==7) echo "selected";?>>25-30 000 Ft - regg.</option>
		<option value="8" <?if($editarr['price_category']==8) echo "selected";?>>25-30 000 Ft - fp.</option>
		<option value="9" <?if($editarr['price_category']==9) echo "selected";?>>30-35 000 Ft - regg.</option>
		<option value="10" <?if($editarr['price_category']==10) echo "selected";?>>30-35 000 Ft - fp.</option>
		<option value="11" <?if($editarr['price_category']==11) echo "selected";?>>35-40 000 Ft - regg.</option>
		<option value="12" <?if($editarr['price_category']==12) echo "selected";?>>35-40 000 Ft - fp.</option>
		<option value="13" <?if($editarr['price_category']==13) echo "selected";?>>40-45 000 Ft - regg.</option>
		<option value="14" <?if($editarr['price_category']==14) echo "selected";?>>40-45 000 Ft - fp.</option>
		<option value="15" <?if($editarr['price_category']==15) echo "selected";?>>45-50 000 Ft - regg.</option>
		<option value="16" <?if($editarr['price_category']==16) echo "selected";?>>45-50 000 Ft - fp.</option>
		<option value="17" <?if($editarr['price_category']==17) echo "selected";?>>50 000 Ft felett - regg.</option>
		<option value="18" <?if($editarr['price_category']==18) echo "selected";?>>50 000 Ft felett - fp.</option>
	</select>
</td>
<td class='en'><input type="text" name="fc_actual_price" class="numeric" value="<?=$editarr[fc_actual_price]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span> </td></tr>






<!--<tr><td class='required'>Promóciós ár</td><td><input type="text" name="sale" class="numeric" value="<?=$editarr[sale]?>"/> Ft-->
<!--<hr/>
<tr><td class='required'>Viszonteladói ajánlat</td><td> <input type="checkbox" name="reseller_enabled" <?if($editarr[reseller_enabled]==1) echo "checked";?> />
<tr><td class='required'>Viszonteladás kezdete</td><td> <input type="text" name="reseller_from_date" value='<?=$editarr[reseller_from_date]?>'  class="maskeddate"/>

<tr><td class='required'>Viszonteladói ár</td><td><input type="text" name="reseller_price" class="numeric" value="<?=$editarr[reseller_price]?>"/> Ft
-->
<!--<tr><td class='required'>Lajos ár</td><td><input type="text" name="reseller_price" class="numeric" disabled value="<?=$editarr[lajos_price]?>"/> Ft-->


<? if($pdata[vat_extra_service] == 0)
	{
		$fpnot = "red";
		$fpinfo = "<a href='#' class='tooltip' title='TILOS! A hotel 18%-os ÁFA kategóriájú!'>(i)</a>";
	}
?>




<tr class='<?=$fpnot?>'><td class='required'>Félpanziós felár</td><td><input type="hidden" name="plus_half_board"  class="numeric" value="<?=$editarr[plus_half_board]?>"/> <input type="text" id="plus_half_board_text" class="numeric formCalculate" value="<?=$editarr[plus_half_board]*$editarr[days]?>"/> <select id="plus_half_board" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select> <?=$fpinfo?>  <!--<input type="text" id="plus_half_board" class="numeric formCalculate"/> Ft / <span class="changedDayNum">X</span> nap --></td>
<td class='en'><input type="text" name="fc_plus_half_board"  class="numeric" value="<?=$editarr[fc_plus_half_board]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Hétvégi felár</td><td><input type="hidden" name="plus_weekend_plus"  class="numeric" value="<?=$editarr[plus_weekend_plus]?>"/> <input type="text" class="numeric formCalculate2" id="plus_weekend_plus_text" value="<?=$editarr[plus_weekend_plus]*2?>"/> Ft<!--<select class="formCalculate" id="plus_weekend_plus"><option value="0">Ft / nap</option><option value="1">Ft/csom.</option></select>  Ft / nap VAGY <input type="text" id="plus_weekend_plus" class="numeric formCalculate"/> Ft / <span class="changedDayNum">X</span> nap --></td><td class='en'><input type="text" name="fc_plus_weekend_plus"  class="numeric" value="<?=$editarr[fc_plus_weekend_plus]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Pótágy felár</td><td><input type="hidden" name="plus_bed"  class="numeric" value="<?=$editarr[plus_bed]?>"/>  <input type="text" id="plus_bed_text" class="numeric formCalculate" value="<?=$editarr[plus_bed]*$editarr[days]?>"/> <select id="plus_bed" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select>  </td><td class='en'><input type="text" name="fc_plus_bed" class="numeric" value="<?=$editarr[fc_plus_bed]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Félpanzió + pótágy felár</td><td><input type="hidden" name="plus_bed_plus_food"  class="numeric" value="<?=$editarr[plus_bed_plus_food]?>"/>  <input type="text" id="plus_bed_plus_food_text" class="numeric formCalculate" value="<?=$editarr[plus_bed_plus_food]*$editarr[days]?>"/> <select id="plus_bed_plus_food" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select></td><td class='en'><input type="text" name="fc_plus_bed_plus_food" class="numeric" value="<?=$editarr[fc_plus_bed_plus_food]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span> </td>
</tr>
<tr class='sep'><td colspan='2'></td><td class='en'></td></tr>
<tr><td class='required'><font color='red'><b>Gyermek 1 felár* <a href='#' class='tooltip' title='Ha az adott gyermekár 0 Ft, akkor 1 Ft-ot kell megadni!'>(i)</a></b></font></td><td> <input type="text" name="plus_child1_name"  class="numeric" value="<?=$editarr[plus_child1_name]?>" style='width:20px;'/> <input type="hidden"  class="numeric" name="plus_child1_value" value="<?=$editarr[plus_child1_value]?>"/> <input type="text" id="plus_child1_value_text" class="numeric formCalculate" value="<?=$editarr[plus_child1_value]*$editarr[days]?>"/> <select id="plus_child1_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select></td><td class='en'><input type="text"  class="numeric" name="fc_plus_child1_value" value="<?=$editarr[fc_plus_child1_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'><font color='red'><b>Gyermek 2 felár* <a href='#' class='tooltip' title='Ha az adott gyermekár 0 Ft, akkor 1 Ft-ot kell megadni!'>(i)</a></b></font></td><td> <input type="text" name="plus_child2_name"  class="numeric" value="<?=$editarr[plus_child2_name]?>" style='width:20px;'/> <input type="hidden"  class="numeric" name="plus_child2_value" value="<?=$editarr[plus_child2_value]?>"/> <input type="text" id="plus_child2_value_text" class="numeric formCalculate" value="<?=$editarr[plus_child2_value]*$editarr[days]?>"/> <select id="plus_child2_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select></td><td class='en'><input type="text"  class="numeric" name="fc_plus_child2_value" value="<?=$editarr[fc_plus_child2_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span> </td></tr>
<tr><td class='required'><font color='red'><b>Gyermek 3 felár* <a href='#' class='tooltip' title='Ha az adott gyermekár 0 Ft, akkor 1 Ft-ot kell megadni!'>(i)</a></b></font></td><td> <input type="text" name="plus_child3_name"  class="numeric" value="<?=$editarr[plus_child3_name]?>" style='width:20px;'/> <input type="hidden"  class="numeric" name="plus_child3_value" value="<?=$editarr[plus_child3_value]?>"/>  <input type="text" id="plus_child3_value_text" class="numeric formCalculate" value="<?=$editarr[plus_child3_value]*$editarr[days]?>"/> <select id="plus_child3_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select></td><td class='en'><input type="text"  class="numeric" name="fc_plus_child3_value" value="<?=$editarr[fc_plus_child3_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span> </td></tr>
<tr class='sep'><td colspan='2'></td><td class='en'></td></tr>
<tr><td class='required'>Szoba 1 felár</td><td> <input type="text" name="plus_room1_name" value="<?=$editarr[plus_room1_name]?>" style='width:60px;'/> <input type="hidden" name="plus_room1_value" class="numeric"  value="<?=$editarr[plus_room1_value]?>"/><input type="text" id="plus_room1_value_text" class="numeric formCalculate" value="<?=$editarr[plus_room1_value]*$editarr[days]?>"/> <select id="plus_room1_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select></td><td class='en'><input type="text" name="en_plus_room1_name" value="<?=$editarr[en_plus_room1_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_room1_value" class="numeric"  value="<?=$editarr[fc_plus_room1_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Szoba 2 felár</td><td> <input type="text" name="plus_room2_name" value="<?=$editarr[plus_room2_name]?>" style='width:60px;'/> <input type="hidden" name="plus_room2_value" class="numeric"  value="<?=$editarr[plus_room2_value]?>"/> <input type="text" id="plus_room2_value_text" class="numeric formCalculate" value="<?=$editarr[plus_room2_value]*$editarr[days]?>"/> <select id="plus_room2_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select></td><td class='en'><input type="text" name="en_plus_room2_name" value="<?=$editarr[en_plus_room2_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_room2_value" class="numeric"  value="<?=$editarr[fc_plus_room2_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Szoba 3 felár</td><td> <input type="text" name="plus_room3_name" value="<?=$editarr[plus_room3_name]?>" style='width:60px;'/> <input type="hidden" name="plus_room3_value" class="numeric"  value="<?=$editarr[plus_room3_value]?>"/> <input type="text" id="plus_room3_value_text" class="numeric formCalculate" value="<?=$editarr[plus_room3_value]*$editarr[days]?>"/> <select id="plus_room3_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select></td><td class='en'><input type="text" name="en_plus_room3_name" value="<?=$editarr[en_plus_room3_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_room3_value" class="numeric"  value="<?=$editarr[fc_plus_room3_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr class='sep'><td colspan='2'></td><td class='en'></td></tr>
<tr><td class='required'>Egyéb 1 felár</td><td> 

<select width='100' style='width:80px !important'  name="plus_other1_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_other1_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>

<!--<input type="text" name="plus_other1_name" value="<?=$editarr[plus_other1_name]?>" style='width:60px;'/>--><input type="hidden" name="plus_other1_value"  class="numeric" value="<?=$editarr[plus_other1_value]?>"/> <input type="text" id="plus_other1_value_text" class="numeric formCalculate" value="<?=$editarr[plus_other1_value]*$editarr[days]?>"/> <select id="plus_other1_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select><a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_other1_name" value="<?=$editarr[en_plus_other1_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_other1_value"  class="numeric" value="<?=$editarr[fc_plus_other1_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Egyéb 2 felár</td><td>


<select width='100' style='width:80px !important'  name="plus_other2_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_other2_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>
<!-- <input type="text" name="plus_other2_name" value="<?=$editarr[plus_other2_name]?>" style='width:60px;'/> -->

<input type="hidden" name="plus_other2_value" class="numeric" value="<?=$editarr[plus_other2_value]?>"/> <input type="text" id="plus_other2_value_text" class="numeric formCalculate" value="<?=$editarr[plus_other2_value]*$editarr[days]?>"/> <select id="plus_other2_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select><a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_other2_name" value="<?=$editarr[en_plus_other2_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_other2_value"  class="numeric" value="<?=$editarr[fc_plus_other2_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Egyéb 3 felár</td><td> <!--<input type="text" name="plus_other3_name" value="<?=$editarr[plus_other3_name]?>" style='width:60px;'/>-->


<select width='100' style='width:80px !important'  name="plus_other3_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_other3_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>
 <input type="hidden" name="plus_other3_value" class="numeric"  value="<?=$editarr[plus_other3_value]?>"/><input type="text" id="plus_other3_value_text" class="numeric formCalculate" value="<?=$editarr[plus_other3_value]*$editarr[days]?>"/> <select id="plus_other3_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select><a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_other3_name" value="<?=$editarr[en_plus_other3_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_other3_value"  class="numeric" value="<?=$editarr[fc_plus_other3_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>


<tr><td class='required'>Egyéb 4 felár (rejtett) <a href='#' class='tooltip' title='Rejtett feár! Az outlen nem jelenik meg, csak az adminban és a véglegesítő oldalon!'>(i)</a></td><td> 

<!--<input type="text" name="plus_other4_name" value="<?=$editarr[plus_other4_name]?>" style='width:60px;'/> -->


<select width='100' style='width:80px !important'  name="plus_other4_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_other4_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>

<input type="hidden" name="plus_other4_value"  class="numeric" value="<?=$editarr[plus_other4_value]?>"/> <input type="text" id="plus_other4_value_text" class="numeric formCalculate" value="<?=$editarr[plus_other4_value]*$editarr[days]?>"/> <select id="plus_other4_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select><a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_other4_name" value="<?=$editarr[en_plus_other4_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_other4_value"  class="numeric" value="<?=$editarr[fc_plus_other4_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Egyéb 5 felár (rejtett) <a href='#' class='tooltip' title='Rejtett feár! Az outlen nem jelenik meg, csak az adminban és a véglegesítő oldalon!'>(i)</a></td><td> <!--<input type="text" name="plus_other5_name" value="<?=$editarr[plus_other5_name]?>" style='width:60px;'/>-->
<select width='100' style='width:80px !important'  name="plus_other5_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_other5_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>

 <input type="hidden" name="plus_other5_value" class="numeric" value="<?=$editarr[plus_other5_value]?>"/> 



<input type="text" id="plus_other5_value_text" class="numeric formCalculate" value="<?=$editarr[plus_other5_value]*$editarr[days]?>"/> <select id="plus_other5_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select><a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_other5_name" value="<?=$editarr[en_plus_other5_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_other5_value"  class="numeric" value="<?=$editarr[fc_plus_other5_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Egyéb 6 felár (rejtett) <a href='#' class='tooltip' title='Rejtett feár! Az outlen nem jelenik meg, csak az adminban és a véglegesítő oldalon!'>(i)</a></td><td>

<!-- <input type="text" name="plus_other6_name" value="<?=$editarr[plus_other6_name]?>" style='width:60px;'/> -->



<select width='100' style='width:80px !important'  name="plus_other6_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_other6_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>
<input type="hidden" name="plus_other6_value" class="numeric"  value="<?=$editarr[plus_other6_value]?>"/><input type="text" id="plus_other6_value_text" class="numeric formCalculate" value="<?=$editarr[plus_other6_value]*$editarr[days]?>"/> <select id="plus_other6_value" class="chage_calculate"><option value="1">Ft/csom.</option><option value="0">Ft / nap</option></select><a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_other6_name" value="<?=$editarr[en_plus_other6_name]?>" style='width:100px;'/> <input type="text" name="fc_plus_other6_value"  class="numeric" value="<?=$editarr[fc_plus_other6_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr class='sep'><td colspan='2'></td><td class='en'></td></tr>
<tr><td class='required'>Egyszeri 1 felár <a href='#' class='tooltip' title='"kötelező szót használva a megnevezésben, a felár kötelezővé válik!"'>(i)</a></td><td> <!--<input type="text" name="plus_single1_name" value="<?=$editarr[plus_single1_name]?>" style='width:60px;'/> -->
<select width='100' style='width:80px !important'  name="plus_single1_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_single1_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>
<input type="text" name="plus_single1_value" class="numeric" value="<?=$editarr[plus_single1_value]?>"/> Ft <a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_single1_name" value="<?=$editarr[en_plus_single1_name]?>" style='width:60px;'/> <input type="text" name="fc_plus_single1_value" class="numeric" value="<?=$editarr[fc_plus_single1_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Egyszeri 2 felár <a href='#' class='tooltip' title='"kötelező szót használva a megnevezésben, a felár kötelezővé válik!"'>(i)</a></td><td> <!--<input type="text" name="plus_single2_name" value="<?=$editarr[plus_single2_name]?>" style='width:60px;'/>-->
<select width='100' style='width:80px !important'  name="plus_single2_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_single2_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>
 <input type="text" name="plus_single2_value" class="numeric" value="<?=$editarr[plus_single2_value]?>"/> Ft <a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td><td class='en'><input type="text" name="en_plus_single2_name" value="<?=$editarr[en_plus_single2_name]?>" style='width:60px;'/> <input type="text" name="fc_plus_single2_value" class="numeric" value="<?=$editarr[fc_plus_single2_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td></tr>
<tr><td class='required'>Egyszeri 3 felár <a href='#' class='tooltip' title='"kötelező szót használva a megnevezésben, a felár kötelezővé válik!"'>(i)</a></td><td> <!--<input type="text" name="plus_single3_name" value="<?=$editarr[plus_single3_name]?>" style='width:60px;'/>-->
<select width='100' style='width:80px !important'  name="plus_single3_name">
	<option value=''></option>
	<? $qr = $mysql->query("SELECT trim(name) as name FROM offers_extra_prices ORDER BY name ASC"); 
		while($a = mysql_fetch_assoc($qr))
		{
			if(strtolower(trim($editarr[plus_single3_name])) == strtolower(trim($a[name])))
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='".trim($a[name])."' $selected>$a[name]</option>";	
		}
	?>
</select>
 <input type="text" name="plus_single3_value" class="numeric" value="<?=$editarr[plus_single3_value]?>"/> Ft <a href='/getOffers_extra.php?add=1' rel='facebox'>+</a></td>
<td class='en'><input type="text" name="en_plus_single3_name" value="<?=$editarr[en_plus_single3_name]?>" style='width:60px;'/> <input type="text" name="fc_plus_single3_value" class="numeric" value="<?=$editarr[fc_plus_single3_value]?>"/> <span class='fc_curr'><? if($editarr[currency] <> 'HUF') echo $editarr[currency]?></span></td>
</tr>




<tr><td class='required'>Az ár nem tartalmazza</td><td><textarea class="trbox" name="not_include"><?=$editarr[not_include]?></textarea></td>
<td class='en'><textarea class="trbox" name="en_not_include"><?=$editarr[en_not_include]?></textarea></td></tr>
<tr><td class='required'>Érvényesség</td><td><textarea class="trbox" name="validity"><?=$editarr[validity]?></textarea></td><td class='en'><textarea class="trbox" name="en_validity"><?=$editarr[en_validity]?></textarea></td>
</tr>
<tr><td class='required'><font color='red'>Lejárat? <a href='#' class='tooltip' title='A teltházas lista csak az aktív utalványokhoz készül el!'>(i)</a></font></td><td><input class="maskeddate" type='text' id='expiration_date' name="expiration_date" value='<?=$editarr[expiration_date]?>'/></td></tr>



<?
	$today = date("Y-m-d H:i:s");
	
	
	//$today = '2009-11-01';
	$finish = strtotime('+21 day', strtotime($today));
	$finish = date("Y-m-d H:i:s", $finish);

?>


<script>


function formatDate(date1) {
  return date1.getFullYear() + '-' +
    (date1.getMonth() < 9 ? '0' : '') + (date1.getMonth()+1) + '-' +
    (date1.getDate() < 10 ? '0' : '') + date1.getDate();
}

	function setExpDate(formDate,interval){
    // set number of days to add
    ///var interval = 30;
    var startDate = new Date(Date.parse(formDate));
   // document.write('start: ' + startDate);
    var expDate = startDate;
    expDate.setDate(startDate.getDate() + interval);
 //   document.write('<br>expire: ' + expDate);
 
  	
 	return formatDate(expDate);
};


	$(document).ready(function() {
	
	
	<?
	if($editarr[id] > 0){
		echo "$('#logdata').load('/log.php?showedit='+$editarr[id]);";
		echo "$('#owndata').load('/info/checkown.php?offerid='+$editarr[id]);";

	}
	?>
	
	
	$('#pdf_photos').click(function() {
		$("#pdf_photo").hide();
		$(".pdfphotos").show();
		return false;
	});


	$('#showpdf').click(function() {
		$(this).hide();
		$(".pdfdata").show();
		return false;
	});
	
	 $('#cleardates').click(function() {
	 
	 var r=confirm("Biztosan törli az összes dátumot?");
	 if (r==true)
	 {
	 	$(".deldate").val('');
		 $("#start_date").val('');
	 	$("#end_date").val('');
	 	$("#quaestor_start_date").val('');
	 	$("#quaestor_end_date").val('');	 
	  	$("#vatera_from").val('');
	  	$("#vatera_to").val('');	
    }
	else
	{ }
	 	return false;
	 });
	 
	/* $('#end_date').click(function() {
	 	if($("#auto_dates").is(":checked")) 
		 	$(this).val('');
		 	
	 });


	 $('#vatera_to').click(function() {
	 	if($("#auto_dates").is(":checked")) 
		 	$(this).val('');
		 	
	 });


	 $('#vatera_to').on('change', function() {
	 		if($(this).val() != '' && $("#auto_dates").is(":checked"))
	 		{	 		 	
	 		 	$("#start_date").val(setExpDate($(this).val(),1));
	 		 	$("#end_date").val(setExpDate($(this).val(),7));
	 		 	$("#closed_start_date").val(setExpDate($(this).val(),8));
	 		 	$("#closed_end_date").val(setExpDate($(this).val(),16));
	 		 	$("#quaestor_start_date").val(setExpDate($(this).val(),17));
	 		 	$("#quaestor_end_date").val(setExpDate($(this).val(),24));
	 		}
	 		
	});
	
		 
	 $('#end_date').on('change', function() {
	 		if($(this).val() != '' && $("#auto_dates").is(":checked"))
	 		{	 		 	
	 		 	$("#closed_start_date").val(setExpDate($(this).val(),1));
	 		 	$("#closed_end_date").val(setExpDate($(this).val(),9));
	 		 	$("#quaestor_start_date").val(setExpDate($(this).val(),10));
	 		 	$("#quaestor_end_date").val(setExpDate($(this).val(),17));
	 		}
	 		
	});*/
	


		
	});
	
	
</script>
<tr><td class='required'>Fix fizetési határidő</td><td><input type="text" name="fix_due_date" class="maskeddate" value="<?if($editarr[fix_due_date] == '0000-00-00') echo ''; else echo $editarr[fix_due_date]; ?>"/></td></tr>



<tr><td class='required'>Lealkudtuk</td><td><input type="text" name="vatera_from" id='vatera_from' class="maskeddate" value="<?if($editarr[vatera_from] == '') echo ''; else echo $editarr[vatera_from]; ?>"/> - <input type="text" name="vatera_to" id='vatera_to' id='vatera_to' class="maskeddate" value="<?if($editarr[vatera_to] == '') echo ''; else echo $editarr[vatera_to]; ?>"/> <a href='#' id='cleardates'>(x)</a></td></tr>


<tr><td class='required'>Outlet</td><td><input type="text"  id='start_date' name="start_date" class="maskeddate" value="<?if($editarr[start_date] == '') echo $today; else echo $editarr[start_date]; ?>"/> - <input type="text" name="end_date"  id='end_date' class="maskeddate" value="<?if($editarr[end_date] == '') echo $finish; else echo $editarr[end_date]; ?>"/></td></tr>

<!--<tr><td class='required'>Zártkörű dátum</td><td><input type="text" name="closed_start_date" id='closed_start_date'  class="maskeddate" value="<?=$editarr[closed_start_date]?>"/> - <input type="text" name="closed_end_date"  id='closed_end_date' class="maskeddate" value="<?=$editarr[closed_end_date];?>"/></td></tr>-->


<tr><td class='required'>Quaestor</td><td><input type="text"  id='quaestor_start_date' name="quaestor_start_date" class="maskeddate" value="<?if($editarr[quaestor_start_date] == '') echo ''; else echo $editarr[quaestor_start_date]; ?>"/> - <input type="text"  id='quaestor_end_date' name="quaestor_end_date" class="maskeddate" value="<?if($editarr[quaestor_end_date] == '') echo ''; else echo $editarr[quaestor_end_date]; ?>"/></td></tr>


<tr class='required' id='morerow'><td colspan='2' align='center'><a href='#' id='moredates'>további dátumok &raquo;</a></td></tr>
<tr class='moredates'><td class='required'>CazareOutlet.ro</td><td><input type="text" name="ro_start_date" class="maskeddate deldate" value="<?if($editarr[ro_start_date] == '') echo ''; else echo $editarr[ro_start_date]; ?>"/> - <input type="text" name="ro_end_date" class="maskeddate deldate" value="<?if($editarr[ro_end_date] == '') echo ''; else echo $editarr[ro_end_date]; ?>"/></td></tr>

<tr class='moredates'><td class='required'>TravelOutlet.sk</td><td><input type="text" name="sk_start_date" class="maskeddate deldate" value="<?if($editarr[sk_start_date] == '') echo ''; else echo $editarr[sk_start_date]; ?>"/> - <input type="text" name="sk_end_date" class="maskeddate deldate" value="<?if($editarr[sk_end_date] == '') echo ''; else echo $editarr[sk_end_date]; ?>"/></td></tr>

<tr class='moredates'><td class='required'>Okazii.ro</td><td><input type="text" name="okazii_from" class="maskeddate deldate" value="<?if($editarr[okazii_from] == '') echo ''; else echo $editarr[okazii_from]; ?>"/> - <input type="text" name="okazii_to" class="maskeddate deldate" value="<?if($editarr[okazii_to] == '') echo ''; else echo $editarr[okazii_to]; ?>"/></td></tr>


<tr class='moredates'><td class='required'>Qponverzum.hu</td><td><input type="text" name="qponverzum_start_date" class="maskeddate deldate" value="<?if($editarr[qponverzum_start_date] == '') echo ''; else echo $editarr[qponverzum_start_date]; ?>"/> - <input type="text" name="qponverzum_end_date" class="maskeddate deldate" value="<?if($editarr[qponverzum_end_date] == '') echo ''; else echo $editarr[qponverzum_end_date]; ?>"/></td></tr>

<tr class='moredates'><td class='required'>Odpadnes.sk (Rival.cz)</td><td><input type="text" name="rival_start_date" class="maskeddate from_date deldate" value="<?if($editarr[rival_start_date] == '') echo ''; else echo $editarr[rival_start_date]; ?>"/> - <input type="text" name="rival_end_date" class="maskeddate to_date deldate" value="<?if($editarr[rival_end_date] == '') echo ''; else echo $editarr[rival_end_date]; ?>"/></td></tr>

<tr class='moredates'><td class='required'>Zlavy.sk (Paylo.sk)</td><td><input type="text" name="paylo_start_date" class="maskeddate from_date deldate" value="<?if($editarr[paylo_start_date] == '') echo ''; else echo $editarr[paylo_start_date]; ?>"/> - <input type="text" name="paylo_end_date" class="maskeddate to_date deldate" value="<?if($editarr[paylo_end_date] == '') echo ''; else echo $editarr[paylo_end_date]; ?>"/></td></tr>

<!-- -->
<tr class='moredates'><td class='required'>Zlavy.sme.sk</td><td><input type="text" name="zlavy_start_date" class="maskeddate from_date deldate" value="<?if($editarr[zlavy_start_date] == '') echo ''; else echo $editarr[zlavy_start_date]; ?>"/> - <input type="text" name="zlavy_end_date" class="maskeddate to_date deldate" value="<?if($editarr[zlavy_end_date] == '') echo ''; else echo $editarr[zlavy_end_date]; ?>"/></td></tr>

<tr class='moredates'><td class='required'>Redutti.ro</td><td><input type="text" name="redutti_start_date" class="maskeddate from_date deldate" value="<?if($editarr[redutti_start_date] == '') echo ''; else echo $editarr[redutti_start_date]; ?>"/> - <input type="text" name="redutti_end_date" class="maskeddate to_date deldate" value="<?if($editarr[redutti_end_date] == '') echo ''; else echo $editarr[redutti_end_date]; ?>"/></td></tr>

<tr class='moredates'><td class='required'>Adulmec.ro</td><td><input type="text" name="adulmec_start_date" class="maskeddate from_date deldate" value="<?if($editarr[adulmec_start_date] == '') echo ''; else echo $editarr[adulmec_start_date]; ?>"/> - <input type="text" name="adulmec_end_date" class="maskeddate to_date deldate" value="<?if($editarr[adulmec_end_date] == '') echo ''; else echo $editarr[adulmec_end_date]; ?>"/></td></tr>


<tr><td class='required'>Speciális megjegyzés</td><td><textarea class="trbox" name="special_comment"><?=$editarr[special_comment]?></textarea></td></tr>

<tr><td class='required'>Ajánlat info <div style='font-size:10px'>(pl. speciális karácsonyi ajánlat)</div></td><td><textarea class="trbox" name="offer_comment"><?=$editarr[offer_comment]?></textarea></td></tr>


<tr><td class='required'>Nincs véglegesítő <a href='#' class='tooltip' title='Véglegesítő e-mail letiltása'>(i)</a></td><td> <input type="checkbox" name="facebook" value="1" <?if($editarr[facebook]==1) echo "checked";?> /></td></tr>
<tr><td class='required'>Sorrend <a href='#' class='tooltip' title='Átlagos látogatószám, az oldalon levő sorrend ez alapján dől el'>(i)</a></td><td> <input type="text" name="score" value="<?=$editarr[score]?>"/></td></tr>

<tr><td class='required'>Egyedi voucher <a href='#' class='tooltip' title='A hotel saját utalványa'>(i)</a></td><td> <input type="checkbox" name="special_voucher" value="1" <?if($editarr[special_voucher]==1) echo "checked";?> /></td></tr>

<tr><td class='required'>Oldalak</td><td>
	<select name="location" class='lselect'>
		<option value="none" <?if($editarr[location]=="none")echo"selected";?>>egyik sem</option>
		<option value="vatera" <?if($editarr[location]=="vatera")echo"selected";?>>vatera</option>
		<option value="teszvesz" <?if($editarr[location]=="teszvesz")echo"selected";?>>teszvesz</option>
		<option value="licittravel" <?if($editarr[location]=="licittravel")echo"selected";?>>licittravel</option>
		<option value="both" <?if($editarr[location]=="both")echo"selected";?>>vatera+teszvesz</option>
	</select>
</td></tr>

<tr><td class='required'>Termék típusa</td><td>
	<select name="offer_type" class='lselect'>
		<option value="travel" <?if($editarr[offer_type]=="travel")echo"selected";?>>utazás</option>
		<option value="service" <?if($editarr[offer_type]=="service")echo"selected";?>>szolgáltatás (nincs szép kártya)</option>
	</select>

</td></tr>

<tr><td class='required'>Útlemondási biztosítás</td><td>
	<select name="storno_insurance_enabled" class='lselect'>
		<option value="0" <?if($editarr[storno_insurance_enabled]==0)echo"selected";?>>tiltva</option>
		<option value="1" <?if($editarr[storno_insurance_enabled]==1)echo"selected";?>>engedélyezve</option>
	</select>

</td></tr>
<tr><td class="required">Értékesítés típusa <a href='#' class='tooltip' title='Utazásszervezői értékesítés esetén nincs utasszámla és van ÁFA nyilatkozat!'>(i)</a></td><td>
<select name="contract_type" id='contract_type' class='lselect'>
		<option value="0" <?if($editarr[contract_type]=='0')echo"selected";?>>bizományos értékesítés</option>
		<option value="1" <?if($editarr[contract_type]=='1')echo"selected";?>>utazásszervezői értékesítés</option>
	</select>
</td></tr>

<tr><td class="required">Egyedi promóció hozzárendelés</td><td>

<?
	
	$promos = unserialize($editarr[promo_codes]);
	if(!is_array($promos))
		$promos = array();
	$prs = $mysql->query("SELECT * FROM promotion WHERE scope = 'unique' ORDER BY code ASC" );
	while($pr = mysql_fetch_assoc($prs))
	{
		if(in_array($pr[code],$promos))
			$checked = 'checked';
		else
			$checked = '';
		echo "<input type='checkbox' name='promo_codes[]' value='$pr[code]' $checked/> $pr[code]<br/>";
	}
?>
</td></tr>

<tr><td class="required">Kategóriák<br/>
<a href='/getOffer_categories.php' target='_blank'>[szerkesztés]</a>
</td><td>
<?
	
	$cats = unserialize($editarr[categories]);
	if(!is_array($cats))
		$cats = array();
		
 	$cat = $mysql->query("SELECT * FROM offer_categories WHERE extra_query = '' ORDER BY title ASC");
 	while($c = mysql_fetch_array($cat))
 	{
	 	if(in_array("cat".$c[id],$cats))
			$checked = 'checked';
		else
			$checked = '';
		echo "<input type='checkbox' name='categories[]' value='cat".$c[id]."' $checked/> $c[title]<br/>";
 	}	
?>

</td></tr>


<tr class='isagency'><td class="required" class="required">Jutalékalap</td><td><input type="text" name="foreign_yield_base" value="<?=$editarr[foreign_yield_base]?>" class='numeric'/> Ft</td></tr>
<tr class='isagency'><td class="required" class="required">ÁSZF URL</td><td><input type="text" class="long" name="tos" value="<?=$editarr[tos]?>" style='width:230px;'/></td></tr>



	</table>
	</div>
<div class="cleaner"></div>
</div>
</form>
<?

if($show == '')
{
	foot();
	die;
}

if($CURUSER[userclass] > 30 && $_GET[active] == 'all')
{
	echo "<div style='text-align:center;padding:0 0 0 0;margin-bottom:10px;'><a href='?active=all&year=2010'>2010</a> | <a href='?active=all&year=2011'>2011</a> | <a href='?active=all&year=2012'>2012</a> | <a href='?active=all&year=2013'>2013</a> | <a href='?active=all&year=2014'>2014</a> | <a href='?active=all&year=2015'>2015</a> | <a href='?active=all&year=2016'>2016</a> | <a href='?active=all&year=2017'>2017</a> | <a href='?active=all&year=2018'>2018</a></div>";
}
echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td colspan='2'>ID</td>";
	echo "<td>Hotel</td>";
	echo "<td>$lang[offer]</td>";
	echo "<td>$lang[city]</td>";
	
	//echo "<td>Vatera id</td>";
	//echo "<td>Ajánlat rövid neve</td>";
	echo "<td><a href='?active=$_GET[active]&orderby=validity'>$lang[validity]</a></td>";

	echo "<td width='70'>$lang[price]</td>";
	
//	if($CURUSER[userclass] > 50)
//		echo "<td width='70'>Viszonteladói ára</td>";

	//echo "<td>Elonezet</td>";
	echo "<td>$lang[edit]</td>";
	
		if($CURUSER[userclass] > 5)
		{
			echo "<td>".$lang[shorttrans]."</td>";
			//echo "<td>".$lang[shortcat]."</td>";
			echo "<td>".$lang[cntrct]."</td>";
			echo "<td><a href='?active=$_GET[active]&orderby=conversion'>Analytics</a></td>";
		}

echo "</tr>";


if($_GET[active] == 'reseller')
	$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE reseller_enabled = 1 AND end_date > NOW() ORDER BY $orderby ASC LIMIT 200";
elseif($_GET[active] == 'okazii')
	$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE okazii_from <= NOW() AND okazii_to > NOW() ORDER BY $orderby ASC LIMIT 200";
elseif($_GET[active] == 'vatera')
	$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE vatera_from <= NOW() AND vatera_to > NOW() AND active = 1 ORDER BY $orderby ASC LIMIT 200";
elseif($_GET[active] == 'outlet')
	$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE start_date <= NOW() AND end_date > NOW() AND active = 1 ORDER BY $orderby ASC LIMIT 200";
elseif($_GET[active] == 'ro' || $_GET[active] == 'sk' || $_GET[active] == 'closed')
	$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE $_GET[active]_start_date <= NOW() AND $_GET[active]_end_date > NOW() AND active = 1 ORDER BY $orderby ASC LIMIT 200";
elseif($_GET[active] == 'affiliate')
	$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE affiliate_lira = 1 ORDER BY $orderby ASC LIMIT 200";
elseif($_GET[active] <> 'all')
	$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id WHERE offers.id > 0 $extrareseller ORDER BY $orderby ASC";
else
{
	if($_GET[year] == '')
	{
		$qr = 'SELECT * from offers WHERE id = 0 LIMIT 1';
	}
	else
	{
		$qr = "SELECT offers.is_qr as qr, offers.*,partners.* FROM offers INNER JOIN partners ON partners.pid = offers.partner_id $extrareseller ORDER BY $orderby ASC";
	}
}

$query = mysql_query($qr); 

while($arr = mysql_fetch_assoc($query)) {

	if($_GET[active] == 'all')
		$class = 'blue';
	else
		$class = '';
		
	if($arr[company_invoice] == 'indulhatunk')
			$logo = 'ilogo_small.png';
	elseif($arr[company_invoice] == 'szallasoutlet')
			$logo = 'szo_logo.png';
		else
			$logo = 'ologo_small.png';
			
	if($arr[qr] == 1)
			$qr = '<img src="/images/qr2.jpg" width="20"/>';
		else
			$qr = '';

			
	
	if($arr[affiliate_lira] == 1)
			$alogo = '<img src="/images/rajongoklogo.png" width="20" />';
		else
			$alogo = '';

if(strlen($arr[outer_name]) > 3)
			$outer_name = '-';
		else	
			$outer_name = $arr[outer_name];

	if($arr[parent_id] > 0)
		$parid = "<span style='font-size:10px;'><br/>($arr[parent_id])</span>";
	else
		$parid = '';


		if(inrange($arr[vatera_from],$arr[vatera_to], date("Y-m-d H:i:s")) == true)
		{
			$class = 'blue';
		}
		else
			$class = '';
		
	
		if($arr[sub_partner_id] > 0)
			$arr[pid] = $arr[sub_partner_id];
		
		$p = mysql_fetch_assoc($mysql->query("SELECT property_wellness,hotel_name,city FROM partners WHERE pid = $arr[pid] LIMIT 1"));
		

			if($p[property_wellness] == 1)
				$extracode = "<a href='#' alt='Wellness ajánlat' title='Wellness ajánlat'>(✓)</a>";
			else
				$extracode = "";
							


		
	echo "<tr class=\"$class\">";
		echo "<td>$arr[id] $parid<br/>$outer_name
		</td>";
		
			if($arr[is_top] == 1)
				$top = "<img src='/images/star.png' width='15' alt='Kiemelt TOP partner' title='Kiemelt TOP partner'/>";
			else
				$top = "";


				
		echo "<td width='15'><img src='/images/$logo'/> $alogo $qr $top</td>";
		
		if($CURUSER[userclass] < 50)
			echo "<td><b>$p[hotel_name] $extracode</b><br/>";
		else
			echo "<td><a href='/partners.php?edit=$arr[pid]' target='_blank'><b>$arr[hotel_name] $extracode</b></a><br/>";
		
			echo "</td>";
	//	echo "<td><div style='word-wrap: break-word;width:200px;'></div></td>";
		echo "<td><a href='/info/offer.php?id=$arr[id]' rel='facebox'><b>$arr[name]</b></a><hr/>$arr[shortname]</td>";
		echo "<td>$arr[city]</td>";
		
	/*	$validity = explode("Az ajándékutalvány ",$arr[validity]);
		$validity = explode("-",$validity[1]);
		
		if(strlen($validity[0]) > 30)
		{
			$validity = explode(".",$validity[0]);
			$validity = $validity[0];
		}
		else
		{
			$validity = $validity[0];
		}
		*/
		
		if($CURUSER[userclass] < 50)
			$explink = '';
		else
			$explink = "<a href='/info/sendfullhouse.php?pid=$arr[pid]&offer=$arr[id]&contract=2' alt='Ajánlatának érvényessége hamarosan lejár levél küldése' title='Ajánlatának érvényessége hamarosan lejár levél küldése' rel='facebox'>&raquo;</a>";
		echo "<td align='center'>$arr[expiration_date] $explink</td>";
		
		
		//show reseller price, in case of reseller account
	//	if($arr[reseller_price] > 0 && $CURUSER[userclass] == 5)
	//		echo "<td align='right'>".formatPrice($arr[reseller_price])."</td>";
	//	else
			echo "<td align='right'>".formatPrice($arr[actual_price])."</td>";
		
	//	if($CURUSER[userclass] > 50)
	//		echo "<td align='right'>".formatPrice($arr[reseller_price])."</td>";
		if($CURUSER[userclass] > 38)
		{
			$editlink = "<a href=\"?editid=$arr[id]&add=1\" target='_blank'>[szerkeszt]</a><br/><a href=\"?editid=$arr[id]&cop=1&add=1&parent=$arr[id]&outer_name=$arr[outer_name]\" rel='facebox'>[másol]</a><br/>
			<!--<a href=\"?notify=$arr[id]&pid=$arr[pid]\" rel='facebox'>[értesítők]</a><br/>-->
			<a href=\"/info/sendfullhouse.php?pid=$arr[pid]&offer=$arr[id]\" rel='facebox'>[teltház]</a><a href=\"/info/sendfullhouse.php?pid=$arr[pid]&offer=$arr[id]&contract=1\" rel='facebox'>[info]</a><br/>";

			//$lc = mysql_fetch_assoc($mysql->query("SELECT cid, hash FROM customers WHERE checked = 0 AND offers_id = $arr[id] LIMIT 1"));
			
			$editlink.="<a href='http://www.szallasoutlet.hu/getoffer/$arr[id]' target='_blank'>[számolás]</a><br/>";
			
		}
		elseif($CURUSER[userclass] == 30)
			$editlink = "<a href=\"getOffersRo.php?editid=$arr[id]&add=1\">[".$lang[translation]."]</a><br/>";

		else
		{
			$exitlink = "";
			$extreseller = "&reseller=1";
		}	
	
	if($_GET[showcount] == 1)
{
	$total = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS cnt FROM customers WHERE offers_id = $arr[id]"));
	echo "<td align='right'>$total[cnt]&nbsp;db</td>";
}	
else
{

	if($arr[pdf_title] <> '' && $CURUSER[pid] <> 2999)
	{
		$printlinks = "<br/><a href=\"?printid=$arr[id]&showprint=1\" rel='facebox'>[nyomtatás]</a>";
	}
	elseif($arr[pdf_title] <> '' && $CURUSER[pid] == 2999)
	
		$printlinks = "<br/><a href=\"/vouchers/print_offer.php?id=$arr[id]&color=purple&affil=$CURUSER[pid]&print=1\">[Nyomtatás]</a>";
	else
	{
		$printlinks = '';
	}
	
	if($CURUSER[abroad] <> 1)
	{
		if($CURUSER[userclass] > 50)
			$elink = "<a href='http://www.szallasoutlet.hu/".clean_url2($p[city])."-szallas/".clean_url2($p[hotel_name])."-$arr[id]?utm_source=fbgroup&utm_campaign=affiliate&utm_medium=affiliate' target='_blank'>[FB]</a><br/>";
		else
			$elink = "";
		echo "<td>$editlink<a href=\"preview.php?tid=$arr[id]&type=offer$extreseller\" target='_blank'>[".$lang['showoffer']."]</a><br/><a href='http://www.szallasoutlet.hu/".clean_url2($p[city])."-szallas/".clean_url2($p[hotel_name])."-$arr[id]' target='_blank'>[outlet]</a>$elink<a href=\"fullhouse.php?pid=$arr[pid]\">[".$lang['fullrooms']."]</a>
		
		$printlinks";
	}
	else
	{
		
		echo "<td><a href=\"getOffersRo.php?editid=$arr[id]&add=1\">[szerkeszt]</a><br/><a href=\"preview.php?tid=$arr[id]&type=offer$extreseller&abroad=1\" target='_blank'>[".$lang['showoffer']."]</a></td>";
	}
}
	if($arr['ro_main_description'] <> '')
		$rotitle = "<a href='getOffersRo.php?editid=$arr[id]&add=1'  target='_blank'><img src='http://admin.indulhatunk.hu/images/flags/ro.png' width='20'/></a>";
	else
		$rotitle = '';
		
	if($arr['sk_main_description'] <> '')
		$sktitle = "<a href='getOffersRo.php?editid=$arr[id]&add=1&country=sk' target='_blank'><img src='http://admin.indulhatunk.hu/images/flags/sk.png' width='20'/></a>";
	else
		$sktitle = '';
		
	if($sktitle == '' && $rotitle == '')
		$rotitle = "<a href='getOffersRo.php?editid=$arr[id]&add=1'  target='_blank'>&raquo;</a>";
	
	
	if($arr['price_category'] == 0)
		$cat = '!!!';
	else
		$cat = '';

		
	if($CURUSER['userclass'] > 5)
	{
					
		echo "<td width='20' align='center'>$rotitle $sktitle</td>";
		
	//	echo "<td>$cat</td>";
		
		
		$checkcontract = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE company = '$arr[company_invoice]' and partner_id = $arr[partner_id] and type = 'outlet' LIMIT 1"));
		
		if($checkcontract[id] == '')
			echo "<td class='red' width='20'><a href='/vouchers/print_contract.php?id=1&pid=$arr[pid]&company=$arr[company_invoice]&print=1'>letölt&nbsp;&raquo;</a></td>";
		else
			echo "<td></td>";

		
	echo "<td align='right' width='20' style='font-size:10px;'><a href='#' title='Oldalletöltés'>$arr[an_visits]</a> / <a href='#' title='Egyedi látogató'>$arr[an_pageviews]</a><br/>
								<a href='#' title='AVG oldalon töltött idő'>".round($arr[an_time_on_site],2)." mp</a><br/>
								<a href='#' title='Visszafordulási arány'>".round($arr[an_bounce],2)."%</a>
								<a href='#' title='Konverzió'>".round($arr[an_conversion_rate],2)."%</a></td>";
	}
	echo "</tr>";
}
echo "</table>";

echo "</div></div>";
foot();
?>