<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");


$rode = (int)$_GET[desc];

if($rode > 0)
{

	$info = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = $rode LIMIT 1"));
	
	echo $info[$ccode."main_description"];
	die;
}


if($_GET[country] == '' || $_GET[country] == 'ro')
{
	$ccode = 'ro_';
	$title = $lang['romaniantrans'];
	$cnt = $lang[romanian];
}
elseif($_GET[country] == 'sk')
{
	$ccode = 'sk_';
	$title = $lang['slovakiantrans'];
	$cnt = $lang[slovak];
}


//check if user is logged in or not
userlogin();

if($CURUSER[userclass] < 30)
	$extrareseller = "AND reseller_enabled = 1 AND pdf_title <> '' AND reseller_from_date <= NOW() AND reseller_from_date <> '0000-00-00'";

if($CURUSER[abroad] == 1)
	$extrareseller = 'AND affiliate_lira = 0';
	
if($CURUSER[userclass] >= 30)
{

$name = $_POST[$ccode."name"];
$editID = (int)$_POST[id];


if($name <> '' && $editID <> '')
{

	
	$mysql->query_update("offers",$_POST,"id=$_POST[id]");
	$msg = "Sikeresen szerkesztette!";
	
   writelog("$CURUSER[username] edited $ccode offer #$_POST[id]");

	$id = $_POST[id];
}

}


head($title,1);



echo message($msg);

?>
<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
	
		$("#tselect").change(function(){
			var id = $(this).val()*1;
			
			if(id > 0)
				$("#templateBody2").load("/getOffersRo.php?desc="+id); 
			
		});
		
		
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,fullscreen,noneditable,template,spellchecker",
			
			spellchecker_languages : "+Hungarian=hu,Romanian=ro,Slovak=sk,English=en,Swedish=sv",

			// Theme options
			theme_advanced_buttons1 : "code,spellchecker,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,preview",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
extended_valid_elements: "iframe[class|src|frameborder=0|alt|title|width|height|align|name]",
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>

<?
$add = $_GET[add];



?>


<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					


		

		<li><a href="getOffers.php"><?=$lang[alloffers]?></a></li>
		<li><a href="getOffersRo.php?editid=<?=$_GET[editid]?>&add=1&country=ro" class='<? if($_GET[country] == 'ro' || $_GET[country] == '') echo "current" ?>'><?=$lang[romaniantrans]?></a></li>
		<li><a href="getOffersRo.php?editid=<?=$_GET[editid]?>&add=1&country=sk" class='<? if($_GET[country] == 'sk') echo "current" ?>'><?=$lang[slovakiantrans]?></a></li>

		
	</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>


<form method="post" enctype="multipart/form-data">
<div style="width:1220px;">

<?
$editid = (int)$_REQUEST[editid];
if($editid > 0 )
{
	$editQr = mysql_query("SELECT * FROM offers WHERE id = $editid");
	$editarr = mysql_fetch_assoc($editQr);
	
	if($_GET[cop] <> 1)
		echo "<input type=\"hidden\" name=\"id\" value=\"$editarr[id]\"/>";
}

if($editarr[sub_partner_id] > 0)
	$editarr[partner_id] = $editarr[sub_partner_id];
	

$partnerQuery = mysql_fetch_assoc(mysql_query("SELECT pid,hotel_name FROM partners WHERE pid = $editarr[partner_id]"));


?>



<fieldset  style="width:100%;background-color:#f9f9f9;<?=$show?>">
<legend><?=$lang[template]?></legend>
<ul>


<li><label><?=$lang[outername]?></label><input type="text"  style="width:30px;" readonly="true" value="<?=$editarr[outer_name]?>"/>/<input type="text"  style="width:40px;" readonly="true" value="<?=$editarr[parent_id]?>"/>
<li><label>Partner</label><input type="text"  style="width:250px;" readonly="true" value="<?=$partnerQuery[hotel_name]?>"/>

<li><label class="required"><?=$lang[offer_name]?></label><input type="text" class="long"  value="<?=$editarr[name]?>" style='width:250px;'/> <input type="text" class="long" name="<?=$ccode?>name" value="<?=$editarr[$ccode."name"]?>" style='width:250px;'/>

<li><label class="required"><?=$lang[offer_shortname]?></label><input type="text"  class="long" value="<?=$editarr[shortname]?>"  style='width:250px;'/> <input type="text"  class="long" name="<?=$ccode?>shortname" value="<?=$editarr[$ccode."shortname"]?>"  style='width:250px;'/>


<li><label>1. <?=$lang[room_extra]?> </label> <input type="text" value="<?=$editarr[plus_room1_name]?>" style='width:250px;'/>  <input type="text" name='<?=$ccode?>plus_room1_name' value="<?=$editarr[$ccode."plus_room1_name"]?>" style='width:250px;'/> 
<li><label>2. <?=$lang[room_extra]?></label> <input type="text" value="<?=$editarr[plus_room2_name]?>" style='width:250px;'/> <input type="text" name='<?=$ccode?>plus_room2_name' value="<?=$editarr[$ccode."plus_room2_name"]?>" style='width:250px;'/> 

<li><label>3. <?=$lang[room_extra]?></label> <input type="text" value="<?=$editarr[plus_room3_name]?>" style='width:250px;'/> <input type="text" name='<?=$ccode?>plus_room3_name' value="<?=$editarr[$ccode."plus_room3_name"]?>" style='width:250px;'/> 

<li><label>1. <?=$lang['other_extra']?></label> <input type="text" value="<?=$editarr[plus_other1_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_other1_name"]?>" name='<?=$ccode?>plus_other1_name' style='width:250px;'/>
<li><label>2. <?=$lang['other_extra']?></label> <input type="text" value="<?=$editarr[plus_other2_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_other2_name"]?>" name='<?=$ccode?>plus_other2_name' style='width:250px;'/>

<li><label>3. <?=$lang['other_extra']?></label> <input type="text" value="<?=$editarr[plus_other3_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_other3_name"]?>" name='<?=$ccode?>plus_other3_name' style='width:250px;'/>

<li><label>4. <?=$lang['other_extra']?></label> <input type="text" value="<?=$editarr[plus_other4_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_other4_name"]?>" name='<?=$ccode?>plus_other4_name' style='width:250px;'/>

<li><label>5. <?=$lang['other_extra']?></label> <input type="text" value="<?=$editarr[plus_other5_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_other5_name"]?>" name='<?=$ccode?>plus_other5_name' style='width:250px;'/>

<li><label>6. <?=$lang['other_extra']?></label> <input type="text" value="<?=$editarr[plus_other6_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_other6_name"]?>" name='<?=$ccode?>plus_other6_name' style='width:250px;'/>

<li><label>1. <?=$lang['single_extra']?></label> <input type="text" value="<?=$editarr[plus_single1_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_single1_name"]?>" name='<?=$ccode?>plus_single1_name' style='width:250px;'/> 
<li><label>2. <?=$lang['single_extra']?></label> <input type="text" value="<?=$editarr[plus_single2_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_single2_name"]?>" name='<?=$ccode?>plus_single2_name' style='width:250px;'/> 

<li><label>3. <?=$lang['single_extra']?></label> <input type="text" value="<?=$editarr[plus_single3_name]?>" style='width:250px;'/> <input type="text" value="<?=$editarr[$ccode."plus_single3_name"]?>" name='<?=$ccode?>plus_single3_name' style='width:250px;'/> 

<li><label><?=$lang[validity]?></label><textarea class="tarea" style='width:250px !important;'><?=$editarr[validity]?></textarea> <textarea class="tarea" style='width:250px !important;' name='<?=$ccode?>validity'><?=$editarr[$ccode."validity"]?></textarea>


<li><label class="required">Árfolyam</label><input type="text"  class="long" name="<?=$ccode?>exchange_rate" value="<?=$editarr[$ccode."exchange_rate"]?>"  style='width:50px;'/><?=$editarr[$ccode."currency"]?>



<li><label>Román Outlet</label><input type="text" name="ro_start_date" class="maskeddate" id='from_date' value="<?if($editarr["ro_start_date"] == '') echo ''; else echo $editarr["ro_start_date"]; ?>"/> - <input type="text" name="ro_end_date" class="maskeddate" id='to_date'  value="<?if($editarr["ro_end_date"] == '') echo ''; else echo $editarr["ro_end_date"]; ?>"/> <a href='#' id='cpdate'>(összeset kitölt)</a>


<li><label>Szlovák Outlet</label><input type="text" name="sk_start_date" class="maskeddate from_date" value="<?if($editarr["sk_start_date"] == '') echo ''; else echo $editarr["sk_start_date"]; ?>"/> - <input type="text" name="sk_end_date" class="maskeddate to_date" value="<?if($editarr["sk_end_date"] == '') echo ''; else echo $editarr["sk_end_date"]; ?>"/>

<script>
$().ready(function() {

$("#cpdate").click(function(){
	var from_date =  $("#from_date").val();
	var to_date =  $("#to_date").val();
		
	$(".to_date").val(to_date);
	$(".from_date").val(from_date);
	return false;
});

});

</script>

<li><label>Okazii.ro</label><input type="text" name="okazii_from" class="maskeddate from_date" value="<?if($editarr[okazii_from] == '') echo ''; else echo $editarr[okazii_from]; ?>"/> - <input type="text" name="okazii_to" class="maskeddate to_date" value="<?if($editarr[okazii_to] == '') echo ''; else echo $editarr[okazii_to]; ?>"/>


<li><label>Odpadnes.sk (Rival.cz)</label><input type="text" name="rival_start_date" class="maskeddate from_date" value="<?if($editarr[rival_start_date] == '') echo ''; else echo $editarr[rival_start_date]; ?>"/> - <input type="text" name="rival_end_date" class="maskeddate to_date" value="<?if($editarr[rival_end_date] == '') echo ''; else echo $editarr[rival_end_date]; ?>"/>

<li><label>Zlavy.sk (Paylo.sk)</label><input type="text" name="paylo_start_date" class="maskeddate from_date" value="<?if($editarr[paylo_start_date] == '') echo ''; else echo $editarr[paylo_start_date]; ?>"/> - <input type="text" name="paylo_end_date" class="maskeddate to_date" value="<?if($editarr[paylo_end_date] == '') echo ''; else echo $editarr[paylo_end_date]; ?>"/>

<!-- -->
<li><label>Zlavy.sme.sk</label><input type="text" name="zlavy_start_date" class="maskeddate from_date" value="<?if($editarr[zlavy_start_date] == '') echo ''; else echo $editarr[zlavy_start_date]; ?>"/> - <input type="text" name="zlavy_end_date" class="maskeddate to_date" value="<?if($editarr[zlavy_end_date] == '') echo ''; else echo $editarr[zlavy_end_date]; ?>"/>

<li><label>Redutti.ro</label><input type="text" name="redutti_start_date" class="maskeddate from_date" value="<?if($editarr[redutti_start_date] == '') echo ''; else echo $editarr[redutti_start_date]; ?>"/> - <input type="text" name="redutti_end_date" class="maskeddate to_date" value="<?if($editarr[redutti_end_date] == '') echo ''; else echo $editarr[redutti_end_date]; ?>"/>

<li><label>Adulmec.ro</label><input type="text" name="adulmec_start_date" class="maskeddate from_date" value="<?if($editarr[adulmec_start_date] == '') echo ''; else echo $editarr[adulmec_start_date]; ?>"/> - <input type="text" name="adulmec_end_date" class="maskeddate to_date" value="<?if($editarr[adulmec_end_date] == '') echo ''; else echo $editarr[adulmec_end_date]; ?>"/>

<!-- -->


<? if($CURUSER[userclass] >= 90) { ?>
<li><label>Fordítás típusa</label><select name='<?=$ccode?>translation_type'>
	<option value='0' <?if($editarr[$ccode.'translation_type'] == 0) echo 'selected';?>>alap fordítás</option>
	<option value='1' <?if($editarr[$ccode.'translation_type'] == 1) echo 'selected';?>>apró módosítás</option>
	<option value='2' <?if($editarr[$ccode.'translation_type'] == 2) echo 'selected';?>>ingyenes módosítás</option>

<? } ?>
	
</select>


</fieldset>

	<br/><br/>
	
<div class="cleaner"></div>


<!-- <select id="tselect">
	<option value='0'>Minta template</option>
	<?
		if($editarr[sub_partner_id] > 0 )
			$select = "sub_partner_id = '$editarr[sub_partner_id]'";
		else
			$select = "partner_id = '$editarr[partner_id]'";

		$qr = $mysql->query("SELECT * FROM offers WHERE $select AND ".$ccode."main_description <> '' order by id DESC");
		
		while($list = mysql_fetch_assoc($qr))
		{
			$added = explode(" ",$list[added]);
			$added = $added[0];
			echo "<option value='$list[id]'>$added - $list[name] ($list[outer_name])</option>";
			
		}
	?>
</select> -->

<div class="cleaner"></div>
<div style='height:10px;'></div>

<?
	if($editarr[parent_id] > 0)
	{
		$parent = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = $editarr[parent_id]"));
		
			$diff = new HtmlDiff($parent["main_description"],$editarr["main_description"] );
			$comp = $diff->build();
		
			echo "<fieldset><legend>$lang[comparison] (<ins>$lang[add]</ins>&nbsp;&nbsp;<del>$lang[remove]</del>)</legend>$comp</fieldset>";

	}
	
	
?>

<div style="float:left;width:600px;padding:0 0 0 5px;<?=$show?><?=$hidetranslate?>">

<div style='font-weight:bold;padding:0px 0 10px 0'><?=$cnt?>:</div>
	<textarea name="<?=$ccode?>main_description" rows="100" cols="20" style="width: 600px" class="tinymce"  id="templateBody2"><?=$editarr[$ccode."main_description"]?></textarea>

</div>

<div style="float:left;width:600px;padding:0 5px 0 0px;<?=$show?><?=$hidetranslate?>">	
<div style='font-weight:bold;padding:0px 0 10px 0'><?=$lang[hungarian]?>:</div>

<textarea rows="100" cols="20" style="width: 600px" class="tinymce"  id="templateBody"><?=$editarr[main_description]?></textarea>

</div>

<div class="cleaner"></div>

<center>
<br/><br/>



<script>
$().ready(function() {
	$("#validero").click(function(){
		var body  = $("#templateBody2").val();
		
		$("#errors").html('');
		
		var findme = '/more.jpg) no-repeat; width: 715px; height: 120px;">&nbsp;</div>';

		if ( body.indexOf(findme) > -1 ) {
				$("#errors").append('<font color="green">- OK: /more.jpg) no-repeat; width: 715px; height: 120px;"&gt;&amp;nbsp;&lt;/div&gt; </font><br/>');
			} else {
				$("#errors").append('- <?=$lang[missing]?>: /more.jpg) no-repeat; width: 715px; height: 120px;"&gt;&amp;nbsp;&lt;/div&gt; <br/>');
		}


	<? if($_GET[country] == 'sk') { ?>
	var findme = '<strong>PONUKA OBSAHUJE</strong>';

		if ( body.indexOf(findme) > -1 ) {
				$("#errors").append('<font color="green">- OK: &lt;strong>PONUKA OBSAHUJE&lt;/strong></font><br/>');
			} else {
				$("#errors").append('- <?=$lang[missing]?>: &lt;strong>PONUKA OBSAHUJE&lt;/strong><br/>');
		}

	<? } else { ?>
	
		var findme = '<strong>OFERTA CONȚINE</strong>';

		if ( body.indexOf(findme) > -1 ) {
				$("#errors").append('<font color="green">- OK: &lt;strong>OFERTA CONȚINE&lt;/strong></font><br/>');
			} else {
				$("#errors").append('- <?=$lang[missing]?>: &lt;strong>OFERTA CONȚINE&lt;/strong><br/>');
		}
		
	<? } ?>


	//alert(body);
		
		///templateBody2
			//alert('aaa');
		
	});
});


</script>

<div id='errors' style='color:red; font-weight:bold;padding:0 0 10px 0; text-align:left; '></div>
<input type="button" value="<?=$lang[check]?>" id='validero'/>

<input type="submit" value="<?=$lang[save]?>" />
</center>




</div>
</form>
<?

if($show == '')
{
	foot();
	die;
}
?>