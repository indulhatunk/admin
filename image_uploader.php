<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

head("Képek kezelése");
?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?type=voucher" class="<? if($_GET[type] == '' || $_GET[type] == 'voucher') echo "current";?>">Voucher feltöltése</a></li>
		<li><a href="?type=logo"  class="<? if($_GET[type] == 'logo') echo "current";?>">Logo feltöltése</a></li>
		<li><a href="?type=newsletter"  class="<? if($_GET[type] == 'newsletter') echo "current";?>">Hírlevél képek</a></li>
		<li><a href="?type=resize"  class="<? if($_GET[type] == 'resize') echo "current";?>">Partnerek képeinek átméretezése</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>


</div>
</div>
<?

foot();
?>
</body>
