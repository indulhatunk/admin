<?php
/* 
* * Grando webAPI v1.0 * *
* connection example with NuSOAP client
* requires NuSOAP lib linked
*
* The NuSOAP lib downloadable from http://sourceforge.net/projects/nusoap/
*
* @author http://www.grando.hu/
* 2011
*/
	require('lib/nusoap.php');
	
	/*
	* create connection
	*/
	$client = new nusoap_client('http://static.grando.hu/upload/wsdl/grando-seller.wsdl', true);
	$client->response_timeout = 3000;
	if (($err = $client->getError())) {
		echo "Constructor error: ". $err ."\n";
	}

	/*
	* function prepare
	*/
	$function_name = 'user_do_login';
	$function_params = 
	array(
		'form_name' => 'loginform', 
		'email' => 'forro.tamas@indul6unk.hu', 
		'password' => 'Tomika001'
	);
	
	/*
	* calling the remote function
	*/
	$result = $client->call(
		$function_name,
		$function_params
	);
	
	/*
	* Handle the result
	*/
	if ($client->fault) {
		echo "Fault: ";
		print_r($result);
		echo "\n";
	} else {
		$err = $client->getError();
		if ($err) {
			echo "Error: ". $err ."\n". $result ."\n";
		} else {
			echo "Your session id is:". $result['session_id'] ."\n";
		}
	}
?>
