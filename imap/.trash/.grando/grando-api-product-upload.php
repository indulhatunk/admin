<?php
/* 
* * Grando webAPI v1.0 * *
* product list example
* works with both cliens (NuSOAP, PEAR) if the success handling is 
* similar to relecant code part in connection example
*
* @author http://www.grando.hu/
* 2011
*/



	/*
	* include the connection and the login part
	*/
	include_once('grando-api-connection-nusoap.php');
	/* OR */
	/* delete as appropriate */
	
	/*
	* Set the session id get from the connection and login part
	*/
	
	/** connection 
	
	echo "<pre>";
	
	ini_set("soap.wsdl_cache_enabled", "0");
	$client = new SoapClient('http://static.grando.hu/upload/wsdl/grando-seller.wsdl');
	print_r($client->__getFunctions());
	
	echo "<hr/>";
	$session = $client->user_do_login('loginform','forro.tamas@indul6unk.hu','Tomika001');
	print_r($session->session_id);
	
	$session_id = $session->session_id;
	
	
	/** connection end **/
	
	
	$session_id = $result['session_id'];
	
	/*
	* prepare the new function
	*/
	$function_name = 'product_do_edit';
	$function_params = array(
			'form_name' => 'product',
			'session_id' => $session_id,
			'sellerProductId' => '', // unique seller product id
			'title' => 'Product title',
			'manufacturer' => 'test', // name of manufacturer
			'category' => 0, // grando category id
			'sellerCategory' => '', // seller category tree string
			'statusId' => 1, 
			'price' => 20,
			'discountPrice' => 10,
			'warranty' => 3,
			'weight' => 159,
			'disableShipping' => false,
			'description' => 'Product description text',
			'isbn13' => '',
			'gtin' => '',
			'productCode' => '',
			'warehouseCode' => '',
			'sellerPageLink' => '', // seller page full link
			'videoLink' => '', // video link for product
			'availability' => array(
				array('sellerParkId' => 1 , 'value' => '-1'),
				array('sellerParkId' => 2 , 'value' => '*'),
			),
			'imageList' => array(
				array(
					'imageId' => 1,
					'image' => 'http://www.rpgfan.com/pics/souloftheultimatenation/art-001.jpg',
					'default' => true,
				),
				array(
					'imageId' => 2,
					'image' => 'http://www.rpgfan.com/pics/souloftheultimatenation/art-002.jpg',
					'default' => false,
				),
			)
		);

	/*
	* calling the remote function
	* the options and the client came from the connection part
	*/
	$result = $client->call(
		$function_name,
		$function_params
	);
	
	echo "<hr/>";
	/*
	* Handle the result
	*/
//	if ($client->fault) {
		print_r($result);
		echo "\n";
//	} else {
	//	echo "Your session id is:". $result['session_id'] ."\n";
//	}
?>
