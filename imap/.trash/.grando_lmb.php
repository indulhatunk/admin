<?
header('Content-type: text/xml; charset=utf-8');
include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");


function format_date_hun($date) {
	$elements = explode("-",$date);
	$day = $elements[2];
	$day = explode(" ",$day);
	$day = $day[0];
	
	$month = (int)$elements[1];
	$year = $elements[0];
	$months =array(
		'JAN',
		'FEB',
		'MAR',
		'APR',
		'MAY',
		'JUN',
		'JUL',
		'AUG',
		'SEP',
		'OCT',
		'NOV',
		'DEC',
	);
	$humonths =array(
		'',
		'jan.',
		'feb.',
		'márc.',
		'ápr.',
		'máj.',
		'jún.',
		'júl.',
		'aug.',
		'szept.',
		'okt.',
		'nov.',
		'dec.',
	);
//	$month = str_replace($months,$humonths,$month);
	$date = "$year. ".$humonths[$month]." $day.";
	return $date;
}


function getreviews($id = 1, $type = 0)
{
	global $mysql;
	
	$partner = '';
	
	$partner = mysql_fetch_assoc($mysql->query("SELECT pid FROM partners WHERE coredb_id = '$id'"));
	
	$query = $mysql->query("SELECT * FROM reviews WHERE partner_id = '$partner[pid]' ORDER BY id DESC ");
	
	
	if(mysql_num_rows($query) == 0)
	{
		$reviews = "";
	}	
	else 
	{
	$sums = $mysql->query("SELECT 
			sum(cleanliness) as clean,
			sum(room) as room,
			sum(pricevalue) as pricevalue,
			sum(food) as food,
			sum(service) as service
			
						  FROM reviews WHERE partner_id = '$partner[pid]'");
	
	$sums = mysql_fetch_assoc($sums);
	
	$count = mysql_num_rows($query);
	
	$cleanliness = number_format($sums[clean] / $count,2,",","");
	$room = number_format($sums[room] / $count,2,",","");
	$pricevalue = number_format($sums[pricevalue] / $count,2,",","");
	$food = number_format($sums[food] / $count,2,",","");
	$service = number_format($sums[service] / $count,2,",","");
	
	$totalpoint = number_format(($sums[clean]+$sums[room]+$sums[pricevalue]+$sums[food]+$sums[service])/$count/5,2,",","");
	$cp = ($cleanliness/5)*100;
	$rp = ($room/5)*100;
	$pp = ($pricevalue/5)*100;
	$fp = ($food/5)*100;
	$sp = ($service/5)*100;
	
	if($type == 0)
	{
		$margin = 'style="margin:0 0 0 35px"';
	}
	$reviews = "
	<div class='ratings'>
		<div class='totalreview'>
			<b>$count</b> vásárló véleménye alapján: 
			<div class='reviewrating' $margin>$totalpoint</div>
		</div>
		
		<div class='graphs'>
			<ul>
				<li><label>Tisztaság</label><div class='line'><div class='graphline' style='width:$cp%'></div></div> <div class='reviewpoint'>$cleanliness</div> <div class='cleaner'></div></li>
				<li><label>Szoba</label><div class='line'><div class='graphline' style='width:$rp%'></div></div> <div class='reviewpoint'>$room</div> <div class='cleaner'></div></li>
				<li><label>Kiszolgálás</label><div class='line'><div class='graphline' style='width:$sp%'></div></div>  <div class='reviewpoint'>$service</div><div class='cleaner'></div></li>
				<li><label>Étel</label><div class='line'><div class='graphline' style='width:$fp%'></div></div>  <div class='reviewpoint'>$food</div> <div class='cleaner'></div></li>
				<li><label>Ár/érték</label><div class='line'><div class='graphline' style='width:$pp%'></div></div> <div class='reviewpoint'>$pricevalue</div> <div class='cleaner'></div></li>
			</ul>
		</div>
		<div class='cleaner'></div>
	</div>
	";
	
	if($type == 0)
	{	
	
	$z = 1;
	while($arr = mysql_fetch_assoc($query))
	{
		if($arr[comment] <> '')
			
			$total = number_format(round(($arr[cleanliness]+$arr[room]+$arr[pricevalue]+$arr[service]+$arr[food])/5,2),2);
			$img = rand(1,13);
			
			$dt = format_date_hun($arr[added]);
			
			if($arr[comment] <> '' && $z <= 10)
			{
			
			//echo "SELECT name FROM customers WHERE hash = '$arr[offer_id]'<hr/>";
			$reviews.= "		
			
			<div class='reviewimage'><img src='http://last-minute-belfold.hu/images/buddy/$img.jpg' width='40'/></div>
			<div class='reviewtext'>
			<div class='blue' style='padding:0 0 5px 0;'><b>$arr[name] - $dt</b></div>
			$arr[comment]</div>
			<div class='reviewrating'>$total</div>
			<div class='cleaner'></div>
		";
			}
		$z++;
	}
	
	}
	}
	
	if($reviews <> '')
	{
	return "<div class='lmbyellowsmall'>
			<div class='lmbgreensmalltop3'>Beszámolók</div>
			
			<div class='lmbgreensmallcontent'>
			
			<!-- ratings -->
			".$reviews."
				<!-- ratings -->				
				
			</div>
			<div class='lmbgreensmallbottom'>
			</div>
		
		</div>";
	}
	else
		return false;
	
}




$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

$db = new OCI8DB;
$dbconn = $db->dbconn();

$memcached = new Memcache;
$memcached->connect('127.0.0.1', 11211) or die ("Could not connect"); //connect to memcached server

$accomodation = cachedSQL("SELECT ACCOMODATION.PACKAGE_ORIG_OFFER_ID, DESCRIPTION AS DESCRIPTION, ACCOMODATION.NAME, ACCOMODATION.ID, ACCOMODATION.LMB_MIN_PRICE, ACCOMODATION.PARTNER_ID, ACCOMODATION.CITY_ID, ACCOMODATION.SPECIAL_URL FROM ACCOMODATION WHERE LMB_DAY1 = 1 OR LMB_DAY2 = 1 OR LMB_DAY3 > 0"); //dbms_lob.substr(ACCOMODATION.DESCRIPTION, 3900, 1 )
	
$g = 1;
foreach($accomodation as $item)
{
	$video = '';
	$partner = cachedSQL("SELECT * FROM PARTNER WHERE ID = $item[PARTNER_ID]");
	$partner = $partner[0];
	
	
	$orig = cachedSQL("SELECT  DESCRIPTION FROM ACCOMODATION WHERE ID = $item[PACKAGE_ORIG_OFFER_ID]"); //dbms_lob.substr(ACCOMODATION.DESCRIPTION, 3000, 1 ) AS
	$orig = $orig[0];
	
	$city = cachedSQL("SELECT NAME FROM CITY WHERE ID = $item[CITY_ID]");
	$city = $city[0];
	
	$price = cachedSQL("SELECT PRICE,DAY_NUM FROM PRICES WHERE PRICES.TO_DATE >= SYSDATE AND PRICES.FROM_DATE <= SYSDATE AND PRICES.OFFER_ID = $item[ID] AND ROWNUM = 1");
	$price= $price[0];
	
	$xmlitems.="
		<product>
		<title>".htmlspecialchars("$partner[NAME] - $item[NAME]")."</title>
		<seller_product_id>$item[ID]</seller_product_id>
		<status>1</status>
		<price>".$price[PRICE]*2 ."</price>
		<discount_price>".$item[LMB_MIN_PRICE]*$price[DAY_NUM]*2 ."</discount_price>
		<warranty>1</warranty>
		<manufacturer></manufacturer>
		<weight></weight>
		<category>16</category>
		<disable_shipping>0</disable_shipping>
		<photo_url_1>http://static.last-minute-belfold.hu/uploaded_images/accomodation/$item[PARTNER_ID]/$item[PACKAGE_ORIG_OFFER_ID]/orig/01.jpg</photo_url_1>
		<photo_url_2>http://static.last-minute-belfold.hu/uploaded_images/accomodation/$item[PARTNER_ID]/$item[PACKAGE_ORIG_OFFER_ID]/orig/02.jpg</photo_url_2>
		<photo_url_3>http://static.last-minute-belfold.hu/uploaded_images/accomodation/$item[PARTNER_ID]/$item[PACKAGE_ORIG_OFFER_ID]/orig/03.jpg</photo_url_3>
		<photo_url_4>http://static.last-minute-belfold.hu/uploaded_images/accomodation/$item[PARTNER_ID]/$item[PACKAGE_ORIG_OFFER_ID]/orig/04.jpg</photo_url_4>
		<seller_category>Szállás > $city[NAME] > ".htmlspecialchars($partner[NAME])."</seller_category>
		<video_link>".urlencode($partner[VIDEO_URL])."</video_link>
		<isbn13></isbn13>
		<gtin></gtin>
		<product_code></product_code>
		<warehouse_code></warehouse_code>
		<page_link>http://www.last-minute-belfold.hu/sz%C3%A1ll%C3%A1sok/".clean_url($city[NAME])."/".clean_url($partner[NAME])."+$item[PACKAGE_ORIG_OFFER_ID]/$item[SPECIAL_URL]+$item[ID]/</page_link>
		<availability_235>*</availability_235>";


	$childSelect = '';
	
	$reviews = '';
	$reviews = getreviews($item[PARTNER_ID]);
	
	$packageprice = $item[LMB_MIN_PRICE]*$price[DAY_NUM];
				if($partner[CHILD_1] <> "")
		$childSelect.="$partner[CHILD_1] év alatt: ".formatPrice($packageprice-($packageprice*($partner[CHILD_1_DISCOUNT]/100)))."<br/>";
	if($partner[CHILD_2] <> "" && $partner[CHILD_3] == '')
		$childSelect.="$partner[CHILD_1] és $partner[CHILD_2] év között: ".formatPrice($packageprice-($packageprice*($partner[CHILD_2_DISCOUNT]/100)))." <br/>";
	if($partner[CHILD_2] <> "" &&  $partner[CHILD_3] <> "")
		$childSelect.="$partner[CHILD_1] és $partner[CHILD_2] év között: ".formatPrice($packageprice-($packageprice*($partner[CHILD_2_DISCOUNT]/100)))."<br/>";
	
	if($partner[CHILD_3] <> "" && $partner[CHILD_2] <> "" && $partner[CHILD_1] <> "")
		$childSelect.="$partner[CHILD_2] és $partner[CHILD_3] év között: ".formatPrice($packageprice-($packageprice*($partner[CHILD_3_DISCOUNT]/100)))."<br/>";
	elseif($partner[CHILD_3] == "" && $partner[CHILD_2] <> "" && $partner[CHILD_1] <> "")
		$childSelect.="$partner[CHILD_2] év felett: ".formatPrice($packageprice)."<br/>";
	else
		$childSelect.="$partner[CHILD_1] év felett:  ".formatPrice($packageprice)."<br/>";
		
		if($partner[VIDEO_URL] <> '') {
		$video = "<div class='lmbyellowsmall'><div class='lmbgreensmalltop2'>Videó</div>
				<div class='lmbgreensmallcontent'>
				<iframe width='320' height='200' src='$partner[VIDEO_URL]' frameborder='0' allowfullscreen></iframe>				
				</div>
				<div class='lmbgreensmallbottom'>
				</div></div>";
		} 


$content = "			
				
<!-- real content -->
<style>

#lmbcontent { font-family:tahoma; font-size:14px; }

#lmbcontent img { padding:0; margin:0; }
#lmbleftside { width:600px; float:left;}

.lmblcleaner,.cleaner { clear:both; }

#lmbrightside {  width:368px; float:left; margin:0 0 0 10px; }

.lmbyellowbigtop { background:url('http://admin.indulhatunk.info/images/grando/yellowbigtop.png'); width:579px; height:55px; padding:17px 0 0 17px; color:white; font-size:22px; font-weight:bold; }

.lmbgreenbigtop { background:url('http://admin.indulhatunk.info/images/grando/greenbigtop.png'); width:516px; height:55px; padding:17px 0 0 80px; color:white; font-size:22px; font-weight:bold; }

.lmbgreenbottom { background:url('http://admin.indulhatunk.info/images/grando/greenbigbottom.png'); width:597px; height:13px; }

.lmbgreytop { background:url('http://admin.indulhatunk.info/images/grando/greytop.png'); width:577px; height:27px; padding:25px 0 0 20px; color:white; font-weight:bold; font-size:18px; }

.packagetitle { border-bottom:1px dotted #929292; padding:0 0 3px 0; width:555px;  }

.lmbgreybottom { background:url('http://admin.indulhatunk.info/images/grando/greybottom.png'); width:597px; height:7px; }

.lmbitemtitle { padding:0 0 10px 0;}

.lmbitemtitle a { text-decoration:none; color:#ff860f; font-weight:bold; font-size:16px;  }

.tablecell a {  text-decoration:none; color:#ff860f; font-weight:bold; }

.imagecell { float:left; width:50px; margin:-3px 0 0 0;}

.lmbitem { border-bottom:1px dotted #929292; padding:0 0 5px 0; margin:0 0 5px 0; }

.lmbgreycontent { width:557px; padding:5px 20px 10px 20px;margin:0; background:url('http://admin.indulhatunk.info/images/grando/greycontent.png'); color:white; font-weight:bold; }

.lmbbigcenter { width:554px; padding:5px 20px 10px 20px; border-right:1px solid #d9d9d9;  border-left:1px solid #d9d9d9; margin:0; font-size:12px; }

.lmbyellowbigbottom { background:url('http://admin.indulhatunk.info/images/grando/yellowbigbottom.png'); width:579px; height:83px; padding:30px 0 0 17px; color:white; font-size:16px; font-weight:bold; margin:0 0 10px 0; }

.lmbyellowbigcenter p { padding:0; margin:0; }

.lmbgreensmallcontent { width:326px; padding:5px 20px 10px 20px; border-right:1px solid #d9d9d9;  border-left:1px solid #d9d9d9; margin:0; font-size:12px; }

.lmbgreensmalltop { background:url('http://admin.indulhatunk.info/images/grando/greensmalltop.png'); width:288px; height:57px; padding:17px 0 0 80px;  color:white; font-size:22px; font-weight:bold; }

.lmbgreensmalltop2 { background:url('http://admin.indulhatunk.info/images/grando/greensmalltop2.png'); width:288px; height:57px; padding:17px 0 0 80px;  color:white; font-size:22px; font-weight:bold; }

.lmbgreensmalltop3 { background:url('http://admin.indulhatunk.info/images/grando/greensmalltop3.png'); width:288px; height:57px; padding:17px 0 0 80px;  color:white; font-size:22px; font-weight:bold; }

.hotelname { font-size:18px; color:#22aeb1; font-weight:bold;}

.bluetitle {  font-size:18px; color:#22aeb1; font-weight:bold; padding:0 0 10px 0; }
.hoteladdress { font-size:12px; margin:5px 0 10px 0;}

.tablecell { float:left; width:100px; font-size:12px; font-weight:normal;}

.lmbgreensmallbottom { background:url('http://admin.indulhatunk.info/images/grando/greensmallbottom.png'); width:368px; height:10px;}
/*reviews*/
.reviewimage { float:left; width:40px;}
.reviewtext { float:left; width:175px; padding:0 10px 10px 10px; border-bottom:1px solid #e6e6e6; margin:0 0 10px 0;  }
.reviewrating { float:left; width:48px; height:33px; color:white; font-weight:bold; padding:15px 0 0 0; text-align:center; background:url('http://last-minute-belfold.hu/images/comment.png') no-repeat; margin:0 0 0 40px;}
.line { width:50px; height:10px; float:left;background:#B9B9B9; -moz-border-radius: 3px; border-radius: 3px;}
.graphs { width:180px; float:right;  }
.graphs label { display:block; width:80px; float:left; }
.ratings { border:1px solid #e6e6e6; padding:5px; margin:0 0 10px 0; }
.ratings ul { padding:0; margin:0;}
.ratings ul li { border:none; padding:0; list-style:none; }
.reviewpoint { float:left; font-weight:bold;padding:0 0 0 5px;}
.totalreview { float:left; width:120px; text-align:center; border-right:1px solid #e6e6e6; margin:0 10px 0 0; }
.graphline { height:10px; background:url('http://last-minute-belfold.hu/images/bar.png'); -moz-border-radius: 3px; border-radius: 3px;}


.tab_container .reviewrating { margin:0;}
.reviewdata { width:306px;margin:0px 0 30px 0; border:none}
.reviewdata .ratings { }
.reviewdata .line { width:70px;}
.reviewdata .graphs { width:200px; padding:10px 0 0 0;}

.reviewdata .totalreview { width:75px;}
.reviewdata .reviewrating { margin:0 0 0 5px;}

</style>
<div id='lmbcontent'>
	<div id='lmbleftside'>
		<div class='lmbyellowbig'>
			<div class='lmbyellowbigtop'>
				$item[NAME]
			</div>
			<div class='lmbbigcenter'>
			<table>".$item[DESCRIPTION]->load()."</table>
				<hr/>
				<div class='bluetitle'>Gyerekkedvezmények:</div>
				
	$childSelect
			
					
			</div>
			<div class='lmbyellowbigbottom'>
				Az áraink két főre értendőek.<br/>Az ajánlatunkkal ma, holnap és holnap után tudsz a szállodába menni.<br/>Üdülési csekket elfogadunk.
			</div>		
		</div>
		
		<div class='lmbgreenbig'>
			<div class='lmbgreenbigtop'>
				A szállodáról
			</div>
			<div class='lmbbigcenter'>
				<table>".$orig[DESCRIPTION]->load()."</table>
			</div>
			<!--<div class='lmbgreenbottom'></div>-->
			<div class='lmbgreytop'><div class='packagetitle'>További csomagajánlatok:</div></div>
			<div class='lmbgreycontent'>
				
				<!-- item 
				<div class='lmbitem'>
					<div class='lmbitemtitle'>
						<a href='#'>Echo kalandozás</a>
					</div>
					<div class='lmbiteminfo'>
						<div class='imagecell'><img src='http://admin.indulhatunk.info/images/grando/suitcase.png'/></div>
						<div class='tablecell'>3 nap / 2 éj</div>
						<div class='tablecell' style='width:200px;'>-35% kedvezmény</div>
						<div class='tablecell' style='text-align:right;'>16 250 Ft</div>
						<div class='tablecell' style='text-align:right;'><a href='#'>tovább &raquo;</a></div>
						<div class='cleaner'></div>
					</div>
				</div>
				 item -->
						</div>
			<div class='lmbgreybottom'></div>
			
		</div>
	</div>
	<div id='lmbrightside'>
		<div class='lmbgreensmall'>
			<div class='lmbgreensmalltop'>Itt vagyunk</div>
			<div class='lmbgreensmallcontent'>	
				<div class='hotelname'>$partner[NAME]</div>
				<div class='hoteladdress'>$city[NAME], $partner[ADDRESS]</div>
				
				<img src='http://maps.googleapis.com/maps/api/staticmap?size=320x325&zoom=15&maptype=roadmap&markers=size:mid%7Ccolor:red%7C".urlencode($city[NAME].", ".$partner[ADDRESS])."&sensor=true' />
			</div>
			<div class='lmbgreensmallbottom'>
			</div>
		
		</div>
		
		$video
		
		$reviews

	</div>
	<div class='cleaner'></div>
</div>

<!-- real content ends here -->";

//echo $content."<hr/>";
	$xmlitems.="<description><![CDATA[".$content."]]></description>
		</product>
		";


$g++;		
}
	
$xml = '<?xml version="1.0" encoding="UTF-8" ?><products>';
$xml.= $xmlitems;
$xml.='</products>';

echo $xml;	
?>