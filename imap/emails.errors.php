<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("../inc/init.inc.php");

$domains = array();
exec("eximstats -nt -nr -t0 -q0 -tnl -txt /var/log/exim4/mainlog", $domains);

$domains = implode("\n",$domains);

$errors = explode("List of errors",$domains);
$errors = $errors[1];

$blocks = explode("\n\n",$errors);

$i = 1;
$rowcount = count($blocks);

foreach($blocks as $block)
{
	if($i > 1 && $i < $rowcount)
	{
		
		$block = str_replace("    ","",trim($block));
		
		$data = array();
		
		$email = explode(" ",$block);
		$email = $email[1];
		
		$domain = explode("@",$email);
		$domain = $domain[1];
		
		$data[email] = $email;
		$data[domain] = $domain;
		$data[added] = 'NOW()';
		$data[body] = $block;
		$data[hash] = md5($block);
		
		$mysql->query_insert_ignore("emails_log",$data);
	}
	
	$i++;
}

echo "done";
die;
?>