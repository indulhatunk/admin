<?
/*
 * customers.php 
 *
 * customers_tour page
 *
*/

/* bootstrap file */
include("../inc/tour.init.inc.php");

function geturl( $url )
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false,
        
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}


$query = $mysql_tour->query("SELECT * FROM partner WHERE partnertype_id = 2 ORDER BY name ASC");

while($arr = mysql_fetch_assoc($query))
{
	$photos = array();
	//fiataloknak ajanlott, idosebbeknek ajanlott
	$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = $arr[city_id]"));
	$url = "https://api.foursquare.com/v2/venues/search?near=".urlencode($city[name])."&client_id=EECMJAF0O4EN30YVTBD1IF0KRVH0YVS5Z0OUO1Z52CVFLRTH&client_secret=ER1MSE4XO3JCXD1LN4TNIGYPJUFWO33ZWMLMBA3XXTHJEJRQ&query=".urlencode($arr[name])."&v=20120810"; //oauth_token=SGGIPT4CCQ2AZOSS1G22S2DFR243Q5LSTLCFOBVW3RQIJNZY
	
	$url = geturl($url);

	$venueinfo = json_decode($url[content],true);
	
		
	if($venueinfo[meta][code] == 403)
		die($venueinfo[meta][errorDetail]); 
		
	
	
	$venueinfo = $venueinfo[response][venues][0];
	
	
	$venueid = $venueinfo[id];
	
	$stats = $venueinfo[stats];
	
	echo "$arr[id] / $venueinfo[name]<hr/>";
	if($venueid > 0)
	{
		
		$imageurl = geturl("https://api.foursquare.com/v2/venues/$venueid/photos?&client_id=EECMJAF0O4EN30YVTBD1IF0KRVH0YVS5Z0OUO1Z52CVFLRTH&client_secret=ER1MSE4XO3JCXD1LN4TNIGYPJUFWO33ZWMLMBA3XXTHJEJRQ&v=20120810");
		
		
		$imagedata = json_decode($imageurl[content],true);

		
		
		if( $imagedata[response][photos][count] == 0)
			$images = array();
		else
			$images = $imagedata[response][photos][groups][1][items];


		
		foreach($images as $item)
		{
			$photos[] =  $item[prefix]."295x295".$item[suffix];
		}
		
		$photos = implode("|",$photos);
		$mysql_tour->query("UPDATE partner SET fsq_venue_id = '$venueid', fsq_checkins = '$stats[checkinsCount]', fsq_been_here = '$stats[usersCount]', fsq_photos = '$photos' , fsq_name = '".mysql_real_escape_string($venueinfo[name])."', fsq_checked = 1 WHERE id = $arr[id] ");
	}
	else
	{
			$mysql_tour->query("UPDATE partner SET fsq_checked = 1 WHERE id = $arr[id] ");

	}
	
	//echo "$arr[name]<hr/>";
}

echo "done";
?>