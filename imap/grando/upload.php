<?php
	include('functions.inc.php');
	

	//get all products
	$activeproducts = getProducts($session_id);
	
	//disable every product daily
	foreach ($activeproducts as $activeproduct)
	{
		disableProduct($session_id,$activeproduct[sellerProductId]);
	}

	//upload all current products
	
$partners = cachedSQL("SELECT ACCOMODATION.PARTNER_ID FROM ACCOMODATION WHERE LMB_DAY1 = 1 OR LMB_DAY2 = 1 OR LMB_DAY3 > 0 GROUP BY PARTNER_ID"); //dbms_lob.substr(ACCOMODATION.DESCRIPTION, 3900, 1 )

foreach($partners as $partner)
{

	$accomodation = cachedSQL("SELECT * FROM ACCOMODATION WHERE PARTNER_ID = $partner[PARTNER_ID] AND (LMB_DAY1 = 1 OR LMB_DAY2 = 1 OR LMB_DAY3 > 0) ORDER BY LMB_MIN_PRICE ASC");
	$accomodation = $accomodation[0];

	$description = cachedSQL("SELECT DESCRIPTION FROM ACCOMODATION WHERE ID = $accomodation[PACKAGE_ORIG_OFFER_ID]");
	$description = $description[0];

	if($description[DESCRIPTION] <> '')
		$description = $description[DESCRIPTION]->load();
	else
		$description = '';
		
	$partner = cachedSQL("SELECT * FROM PARTNER WHERE ID = $accomodation[PARTNER_ID]");
	$partner = $partner[0];

	$city = cachedSQL("SELECT NAME FROM CITY WHERE ID = $accomodation[CITY_ID]");
	$city = $city[0];
	
	$price = cachedSQL("SELECT PRICE,DAY_NUM FROM PRICES WHERE PRICES.TO_DATE >= SYSDATE AND PRICES.FROM_DATE <= SYSDATE AND PRICES.OFFER_ID = $accomodation[ID] AND ROWNUM = 1");
	$price= $price[0];


	$product = array(
		'sellerProductId' => "$accomodation[PACKAGE_ORIG_OFFER_ID]",
		'title' => "$partner[NAME]",
		'sellerCategory' => "$city[NAME] / $partner[NAME]",
		'sellerPageLink' => "http://szallas.grando.hu/szállások/".clean_url($city[NAME])."/".clean_url($partner[NAME])."+$accomodation[PACKAGE_ORIG_OFFER_ID]/",
		'description' => $description,
		'price' => round($price[PRICE]/$price[DAY_NUM]),
		'discountPrice' => $accomodation[LMB_MIN_PRICE], //*$price[DAY_NUM]*2
		'partnerId' => $partner[ID]
	);
	
	uploadProduct($session_id,$product);
	
	echo "$partner[NAME] uploaded $accomodation[LMB_MIN_PRICE]*$price[DAY_NUM]*2<hr/>\n";

}
	/*******/
	

?>
