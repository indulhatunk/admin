<?
header('Content-type: text/html; charset=utf-8');

include("../inc/tour.init.inc.php");

MNBArfolyamLetoltes();


$body = 'Felhívjuk szíves figyelmét, hogy szállás utalványának #NOTIFICATION#<br/><br/>

<b>Ajánlat neve: #OFFER#</b><br/>
<b>Fizetendő összeg: #PRICE#</b><br/><br/>

<b>Az alábbi módok egyikén kérjük, a megadott fizetési határidőig (2017.05.15) egyenlítse ki vásárlása ellenértékét.</b><br/><br/>

<b>Átutalás:</b><br/>
Kérjük az alábbi bankszámlára utaljon:<br/>
#COMPANY#<br/>
Erste bank:  #BANK#<br/><br/>

<b>Készpénz:</b><br/>
Címünk: 1056 Budapest, Váci utca u 9. 21-es kaputelefon<br/>
Nyitva tartás hétfő és péntek 8-tól 18 óráig<br/><br/>

<!--<b>SZÉP kártya:</b><br/>

Amennyiben SZÉP kártyával szeretne fizetni, de nem szállás alszámlán van az összeg,<br/>
amit utazásához fel szeretne használni, úgy kérjük, hogy fizetési szándékát a vtl@indulhatunk.hu e-mail címre jelezze.<br/><br/>-->

Amennyiben a kérdéses összeget felszólításunk kézhezvétele előtt már<br/>
rendezte, kérjük, tekintse levelünket tárgytalannak.<br/><br/>

Szállás Outlet<br/>

vtl@indulhatunk.hu<br/>
+36 70 930 7874<br/><br/>';

$subject = $lang['paymentnotification_subject1'];
$subject2 = $lang['paymentnotification_subject2'];
$notification1 = $lang['paymentnotification_1'];
$notification2 = $lang['paymentnotification_2'];
$notification3 = $lang['paymentnotification_3'];
$notification4 = $lang['paymentnotification_4'];
$sql = "SELECT * FROM
    customers
WHERE
    paid = 0
        AND notification4 = '0000-00-00 00:00:00'
        AND inactive = 0
        AND discount_code IN ('KISOPRES1720' , 'KISOPRES1717', 'KISOPRES1725')";
//print $sql; die();
$query = $mysql->query($sql);
while($arr = mysql_fetch_assoc($query))
{

	if (strpos($arr[offer_id],'TVO-') !== false) {
		include("../inc/languages/sk.lang.php");
	}
	elseif (strpos($arr[offer_id],'CZO-') !== false) {
		include("../inc/languages/ro.lang.php");
	}
	else  {
		include("../inc/languages/hu.lang.php");
	}
	if($lang[currency] == '')
		$lang[currency] = 'Ft';

		
$subject = $lang['paymentnotification_subject1'];
$subject2 = $lang['paymentnotification_subject2'];
//$body = $lang['paymentnotification'];
$notification1 = $lang['paymentnotification_1'];
$notification2 = $lang['paymentnotification_2'];
$notification3 = $lang['paymentnotification_3'];
$notification4 = $lang['paymentnotification_4'];



	if($arr[sub_pid] > 0)
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[sub_pid]"));
	else
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid]"));
			
	$partnerName = str_replace("|",'',$partner[hotel_name]);
	
	if(substr($arr[offer_id],0,3) == 'CZO')
	{
		$company = '"S.C. HOTEL OUTLET RO S.R.L.';
		$bank = 'BCR România: RO27RNCB0193131558720001';
		$sendletter = 0;
		
	}
	elseif(substr($arr[offer_id],0,3) == 'TVO')
	{
		$company = 'Hotel Outlet s.r.o.';
		$bank = 'Tatra Bank: IBAN SK97 1100 0000 0029 2589 6282 BIC (SWIFT) TATRSKBX';
		$sendletter = 0;
	}
	else
	{
		
			$companyinfo = getCompany($arr[company_invoice],'single');
			$company = $companyinfo[company];
			$bank = $companyinfo[account_no] . "<br/>Nemzetközi bankszámlaszámunk: " . $companyinfo[account_no_iban];
			$sendletter = 1;
    
	}
	
	/*elseif($arr[company_invoice] == 'indulhatunk')
	{
		$company = 'Indulhatunk.hu Kft.';
		$bank = '11600006-00000000-41873494';
		$sendletter = 1;
	}
	elseif($arr[company_invoice] == 'szallasoutlet')
	{
		$company = 'SzállásOutlet Kft.';
		$bank = '11600006-00000000-51899097';
		$sendletter = 1;
	}
	elseif($arr[company_invoice] == 'indusz')
	{
		$company = 'Indulhatunk utazásszervező Kft.';
		$bank = '11600006-00000000-65875957';
		$sendletter = 1;
	}
	else
	{
		$company = 'Hotel Outlet Kft.';
		$bank = '11600006-00000000-49170520';
		$sendletter = 1;
	}
	*/
	$notification4 = "fizetési határideje 2017.05.15-én lejár.";
	$from = array("#NAME#","#NOTIFICATION#","#OFFER#","#PRICE#","#COMPANY#","#BANK#");
	$to = array("$arr[name]","$notification4","$partnerName ($arr[offer_id])",formatPrice($arr[orig_price]-$arr[discount_value],0,0,$lang[currency],0,1),$company,$bank); // $arr[bid_name]
	$message = str_replace($from,$to,$body);
	
	//print $message; //die();
	
	//$data[notification1] = 'NOW()';
	//$data[notification2] = 'NOW()';
	//$data[notification3] = 'NOW()';
	$data[notification4] = 'NOW()';
	
	if($sendletter == 1)
	{
	    //$arr[email] = 'it@indulhatunk.hu'; 
		$mysql->query_update("customers",$data,"cid=$arr[cid]");
		sendEmail($subject." - $arr[offer_id] - $partner[hotel_name]",$message,$arr[email],$arr[name],$lang['adminemail'],$lang['adminname']);
		echo "$message<hr/>";//die();
	}
}


echo "extra notifications sent!";
?>