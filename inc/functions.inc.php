<?

/********

********/

 $szamok = array();  
 $szamok['0'] = 'nulla';  
 $szamok['1'] = 'egy';  
 $szamok['2'] = 'kettő';  
 $szamok['3'] = 'három';  
 $szamok['4'] = 'négy';  
 $szamok['5'] = 'öt';  
 $szamok['6'] = 'hat';  
 $szamok['7'] = 'hét';  
 $szamok['8'] = 'nyolc';  
 $szamok['9'] = 'kilenc';  
 $szamok['10'] = 'tíz';  
 $szamok['20'] = 'húsz';  
 $szamok['30'] = 'harminc';  
 $szamok['40'] = 'negyven';  
 $szamok['50'] = 'ötven';  
 $szamok['60'] = 'hatvan';  
 $szamok['70'] = 'hetven';  
 $szamok['80'] = 'nyolcvan';  
 $szamok['90'] = 'kilencven';  
 $szamok['100'] = 'száz';  
 $szamok['1000'] = 'ezer';  
 $szamok['1000000'] = 'millió';  
 $szamok['1000000000'] = 'milliárd';  
 $szamok['1000000000000'] = 'trillió';  
 $szamok['1000000000000000'] = 'quadrillió';  
 $szamok['1000000000000000000'] = 'quintillió';  
 $szamok['1000000000000000000000'] = 'sextrillió';  
 $szamok['1000000000000000000000000'] = 'septrillió';  
 $szamok['1000000000000000000000000000'] = 'oktrillió';  
  
 function tortel($tort){  
  global $tortek;  
  
  $tortek[] = $tort;  
  $tortek[] = 'tíz'.$tort;  
  $tortek[] = 'szaz'.$tort;  
 }  
  
 $tortek = array();  
 $tortek[] = 'tized';  
 $tortek[] = 'szazad';  
  
 tortel('ezred');  
 tortel('millimod');  
 tortel('milliardod');  
 tortel('trilliomod');  
 tortel('quadrilliomod');  
 tortel('quintrilliomod');  
 tortel('sextrilliomod');  
 tortel('septrilliomod');  
 tortel('oktrilliomod');  
  
 function szam_1($szam, $darabszam = 0){  
  global $szamok;  
  
  if(!$szam)  
   return($szamok['0']);  
  if(($szam == '2') && ($darabszam & 1))  
   return('két');  
  return($szamok[$szam]);  
 }  
  
 function szam_2($szam, $darabszam = 0){  
  global $szamok;  
  
  if(!$szam[1])  
   return($szamok[$szam[0].'0']);  
  
  switch($szam[0]){  
   case '1': $return = 'tizen'; break;  
   case '2': $return = 'huszon'; break;  
   default: $return = $szamok[$szam[0].'0']; break;  
  }  
  
  if($szam[1])  
   $return .= szam_1($szam[1], $darabszam);  
  return($return);  
 }  
  
 function szam_3($szam, $darabszam = 0){  
  global $szamok;  
  
  if(($szam[0] != '1') || ($darabszam & 2))  
   $return = szam($szam[0], 1);  
  @$return .= $szamok['100'];  
  if($szam = intval($szam[1].$szam[2]))  
   $return .= szam(strval($szam), $darabszam);  
  return($return);  
 }  
  
 function szam_4($szam, $darabszam = 0){  
  global $szamok;  
  
  if((intval($szam) > 1999) || ($darabszam & 4))  
   $return = szam(intval(substr($szam, -strlen($szam), strlen($szam) - 3)), 1);  
  @$return .= $szamok['1000'];  
  if($tmp = intval(substr($szam, -3, 3))){  
   if(intval($szam) > 1999)  
    $return .= ' - ';  
   $return .= szam(strval($tmp), $darabszam);  
  }  
  return($return);  
 }  
  
 function szam_5($szam, $darabszam = 0){return szam_4($szam, $darabszam);}  
 function szam_6($szam, $darabszam = 0){return szam_4($szam, $darabszam);}  
  
 function _szam($szam, $darabszam = 0){  
  global $szamok;  
  
  $strlen = intval(strlen($szam) / 3) * 3;  
  if($strlen == strlen($szam))  
   $strlen -= 3;  
  if(!($darabszam & 8))  
   $return = szam(intval(substr($szam, 0, strlen($szam) - $strlen)), 1);  
  if(($darabszam & 16) && preg_match('/^[0-9]0+$/', $szam))  
   @$return .= ' ';  
  @$return .= $szamok['1'.str_repeat('0', $strlen)];  
  if(($tmp = szam(substr($szam, -$strlen), $darabszam)) && ($tmp != $szamok['0']))  
   $return .= ' - '.$tmp;  
  return($return);  
 }  
  
 function szam($szam, $darabszam = 0){  
  if(!strlen($szam = preg_replace('/^0+0/', '0', $szam)))  
   return(null);  
  if($szam[0] == '-')  
   return('mínusz '.szam(substr($szam, 1), $darabszam));  
  if(count($tmp = preg_split('/\./', $szam, 2)) - 1){  
   global $szamok, $tortek;  
  
   $tort = @$tortek[strlen($tmp[1] = preg_replace('/0+$/', '', $tmp[1])) - 1];  
   if(($tmp[1] = szam(intval($tmp[1]))) && ($tmp[1] != $szamok['0']))  
    return szam($tmp[0]).' egész '.$tmp[1].' '.$tort;  
   $szam = $tmp[0];  
  }  
  $szam = preg_replace('/^0+0/', '0', $szam);  
  if(($tmp = strlen($szam)) < 7)  
   return(call_user_func('szam_'.$tmp, $szam, $darabszam));  
  return(_szam($szam,$darabszam));  
 } 

/*******
*******/

function updatourprice($id =1)
{
	global $mysql_tour;

	$pricemonths  = array();
	
	$mysql_tour->query("UPDATE tour SET aleph_ok = 0, min_price = 0, month1_min_price = 0, month2_min_price = 0, month3_min_price = 0, month4_min_price = 0, month5_min_price = 0, month6_min_price = 0, month7_min_price = 0, month8_min_price = 0, month9_min_price = 0, month10_min_price = 0, month11_min_price = 0, month12_min_price = 0 WHERE id = $id");


	$plist = $mysql_tour->query("SELECT * FROM prices WHERE from_date > NOW() AND offer_id = $id AND accomodation = 0");
		
	while($arr = mysql_fetch_assoc($plist))
	{
		$prices[] = $arr[price];
		
		$month= date("n",strtotime($arr[from_date]));
 	 	$pricemonths[$month][] = $arr[price];				 	
	}
	
	
	$eup = '';
	foreach($pricemonths as $month => $mpricelist)
	{
		$mp = @min($mpricelist);
		$eup.=", month".$month."_min_price = $mp";
	}
	
	$minprice = @min($prices);

	if(!empty($prices))
		$inactive = ', aleph_ok = 1';
		
	$mysql_tour->query("UPDATE tour SET min_price = '0'  $inactive $eup WHERE id = $id");
	
	updateminprices($id);
		
		
}


function updateminprices($id = 0)
{
	global $mysql_tour;
	
	if($id > 0)
		$eid = "AND id = $id";
	else
		$eid = "aleph_ok = 1 AND disabled = 0";
		
	$qr = $mysql_tour->query("SELECT * FROM tour WHERE id > 0 $eid ORDER BY partner_id DESC ");
	
	$i = 1;
	while($arr = mysql_fetch_assoc($qr))
	{
		$lengths = $mysql_tour->query("SELECT nights FROM prices WHERE accomodation = 0 AND offer_id = $arr[id] GROUP BY nights ORDER By nights aSC ");
		
		$l = array();
		while($list = mysql_fetch_assoc($lengths))
		{
			$l[]="$list[nights]";
		}
		
		if(in_array("7",$l))
			$class = 'blue';
		else
			$class = '';
		$l = implode(", ",$l);
		
	
		$beds = $mysql_tour->query("SELECT bed_number FROM prices WHERE accomodation = 0 AND offer_id = $arr[id] GROUP BY bed_number ORDER By bed_number aSC ");
		
		$b = array();
		while($bedlist = mysql_fetch_assoc($beds))
		{
			$b[]="$bedlist[bed_number]";
		}
		
		
		if(!in_array("2",$b))
		{
			$cl = 'red';
			
			$notwoperson++;
		}
		else
			$cl = '';
		
		
		$childprice = mysql_fetch_assoc($mysql_tour->query("SELECT child1_price FROM prices WHERE accomodation = 0 AND offer_id = $arr[id] LIMIT 1 "));
		
		if($childprice[child1_price] > 0)
		{
			$cprice = 1;
			$cp++;
		}
		else
			$cprice = '';

		$b = implode(", ",$b);


		$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id] AND from_date > NOW() AND bed_number = 2 AND nights = 7 AND child1_price =0 AND child2_price = 0 ORDER BY price ASC LIMIT 1 "));

		$cl1 = '';
		
		if($minprice[price] == '')
		{
		
			$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id] AND from_date > NOW() AND bed_number = 2 AND nights = 7 AND child1_price = 0 AND child2_price = 0 ORDER BY price ASC LIMIT 1 "));
			if($minprice[price] == '')
			{
				
				$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id] AND from_date > NOW() AND bed_number = 3 AND nights = 7 AND child1_price = 0 AND child2_price = 0 ORDER BY price ASC LIMIT 1 "));
				if($minprice[price] == '')
				{
					$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number = 4 AND nights = 7 AND child1_price = 0 AND child2_price = 0 ORDER BY price ASC LIMIT 1 "));
					if($minprice[price] == '')
					{
						$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number = 2 AND child1_price = 0 AND child2_price = 0   ORDER BY price ASC LIMIT 1 "));
						if($minprice[price] == '')
						{
						
						$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number = 3 AND child1_price = 0 AND child2_price = 0   ORDER BY price ASC LIMIT 1 "));
						if($minprice[price] == '')
						{


						$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number = 2 AND (child1_price > 0 OR child2_price > 0) ORDER BY price ASC LIMIT 1 "));
						if($minprice[price] == '')
						{
						
						$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number = 3 AND child1_price = 0 AND child2_price = 0 ORDER BY price ASC LIMIT 1 "));
						if($minprice[price] == '')
						{
						$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number = 2 AND (child1_price > 0 OR child2_price > 0) ORDER BY price ASC LIMIT 1 "));

									
						if($minprice[price] == '')
						{
								$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number > 2  ORDER BY price ASC LIMIT 1 "));
						if($minprice[price] == '')
						{
							$minprice = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE accomodation = 0 AND offer_id = $arr[id]  AND from_date > NOW() AND bed_number = 1  ORDER BY price ASC LIMIT 1 "));
						if($minprice[price] == '')
						{
							
							$nosevendays++;
						}
						}
						}
						}
						}
						}
						}
					}
				}
			}
		}
		
		
		
		if($minprice[id] > 0)
		{
			$mysql_tour->query("UPDATE prices SET is_min_price = 1, min_price = $minprice[price] WHERE id = $minprice[id]");
			$mysql_tour->query("UPDATE tour SET  min_price = $minprice[price] WHERE id = $arr[id]");

		}

		$i++;
		
	}
}


function age($birthday){
    list($year,$month,$day) = explode("-",$birthday);
    $year_diff  = date("Y") - $year;
    $month_diff = date("m") - $month;
    $day_diff   = date("d") - $day;
    if ($month_diff < 0) //$day_diff < 0 || 
      $year_diff--;
    return $year_diff." év";
  }
  
  function generateTourID($company = '')
{
	global $mysql;
	
	
	$abc= array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	$num= array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");


	$string =strtoupper($abc[rand(0,25)]);
	$string.=strtoupper($abc[rand(0,25)]);
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	
	if(isduplicatevoucher($string) == true)
	{
		return generateTourID($company);
	}
	else
	{
		return $string;
	}
}

function isduplicateinvoice($string)
{
	global $mysql;
	$check = $mysql->query("SELECT * FROM log_invoice WHERE short_id = '$string' LIMIT 1");
	if(mysql_num_rows($check) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function generateInvoiceID($company = '')
{
	global $mysql;
	$abc= array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	$num= array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	$string =strtoupper($abc[rand(0,25)]);
	$string.=strtoupper($abc[rand(0,25)]);
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	
	if(isduplicateinvoice($string) == true)
	{
		return generateInvoiceID($company);
	}
	else
	{
		return $string;
	}
}

function inrange($start_date, $end_date, $date_from_user)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date_from_user);

  // Check that user date is between start & end
  return (($user_ts > $start_ts) && ($user_ts < $end_ts));
}

function formatDateLong($date,$sep = "-") {
	$date = explode(" ",$date);
	$elements = explode($sep,$date[0]);
	$day = $elements[2];
	$month = (int)$elements[1];
	$year = $elements[0];

	$humonths =array(
		'',
		'január',
		'február',
		'március',
		'április',
		'május',
		'június',
		'július',
		'augusztus',
		'szeptember',
		'október',
		'november',
		'december',
	);
	$month = $humonths[$month];
	$date = $year.". $month $day";
	return $date;
}


function formatDate($date, $language = 'hu') {
	global $months;
	global $romonths;
	global $enmonths;
	
	$elements = explode("-",$date);
	$day = $elements[2];
	$month = (int)$elements[1];
	$year = $elements[0];


	
	if($language == 'ro')
	{
		$month = $romonths[$month];
		$date = "$day $month $year";
	}
	elseif($language == 'sk')
	{
		$month = $enmonths[$month];
		$date = "$day/$month/$year";
	}
	elseif($language == 'en')
	{
		$month = $enmonths[$month];
		$date = "$day/$month/$year";
	}
	else
	{
		$month = $months[$month];
		$date = $year.". $month $day.";
	}
	return $date;
}
function formatbytes($bytes,$precision = 2) {
	
	$bytes = $bytes*1024;
	
	  $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
   
    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        return $bytes . ' B';
 
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        return round($bytes / $kilobyte, $precision) . ' KB';
 
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        return round($bytes / $megabyte, $precision) . ' MB';
 
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        return round($bytes / $gigabyte, $precision) . ' GB';
 
    } elseif ($bytes >= $terabyte) {
        return round($bytes / $terabyte, $precision) . ' TB';
    } else {
        return $bytes . ' B';
    }
   } 



class OCI8DB {

	function dbconn() {
		/*$conn = oci_connect('coredb', 'coredb', 'static.pihenni.hu/xe','AL32UTF8');
		if (!$conn) {
			trigger_error("Could not connect to database", E_USER_ERROR);
			return false;
		}
		return $conn;*/
	}

}

function resizeimage($coredb,$package,$image)
{
	global $dbconn;
	$key = $image;
	
	$path = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/uploaded_images/accomodation/$coredb/$package";
	
		makedir("$path/orig/");
		makedir("$path/79_79/");	
		makedir("$path/90_90/");
		makedir("$path/137_119/");
		makedir("$path/137_137/");
		makedir("$path/169_172/");
		makedir("$path/171_99/");
		makedir("$path/177_124/");
		makedir("$path/297_222/");
		makedir("$path/188_75/");
		makedir("$path/364_252/");
		makedir("$path/73_54/");


	Image("$path/orig/$key.jpg", '1.43:1', '177x',"$path/177_124/$key.jpg");
	Image("$path/orig/$key.jpg", '1.73:1', '171x',"$path/171_99/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1.015', '169x',"$path/169_172/$key.jpg");
    Image("$path/orig/$key.jpg", '1:1', '137x',"$path/137_137/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1', '90x',"$path/90_90/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1', '79x',"$path/79_79/$key.jpg");
	Image("$path/orig/$key.jpg", '1.15:1', '137x',"$path/137_119/$key.jpg");
 	Image("$path/orig/$key.jpg", '1.335:1', '297x',"$path/297_222/$key.jpg");
	Image("$path/orig/$key.jpg", '2.5:1', '188x',"$path/188_75/$key.jpg");
	Image("$path/orig/$key.jpg", '1.445:1', '364x',"$path/364_252/$key.jpg");
	Image("$path/orig/$key.jpg", '1.4:1', '73x',"$path/73_54/$key.jpg");

	$directory = @dirList("$path/orig");
	$query  = "UPDATE ACCOMODATION SET IMAGE_NUM = ".count($directory).", IMAGE_GEN = '1' WHERE ID = $package";
	$stmt = oci_parse($dbconn, $query);
	oci_execute($stmt, OCI_DEFAULT);
	oci_commit($dbconn);

}

function resizelicit($offer,$image)
{
	global $dbconn;
	$key = $image;
	
	$path = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/uploaded_images/licittravel/$offer";
	
		makedir("$path/orig/");
		makedir("$path/79_79/");	
		makedir("$path/90_90/");
		makedir("$path/137_119/");
		makedir("$path/137_137/");
		makedir("$path/169_172/");
		makedir("$path/171_99/");
		makedir("$path/177_124/");
		makedir("$path/297_222/");
		makedir("$path/188_75/");
		makedir("$path/364_252/");
		makedir("$path/73_54/");
		makedir("$path/140_85/");


	Image("$path/orig/$key.jpg", '1.65:1', '140x',"$path/140_85/$key.jpg");
	Image("$path/orig/$key.jpg", '1.43:1', '177x',"$path/177_124/$key.jpg");
	Image("$path/orig/$key.jpg", '1.73:1', '171x',"$path/171_99/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1.015', '169x',"$path/169_172/$key.jpg");
    Image("$path/orig/$key.jpg", '1:1', '137x',"$path/137_137/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1', '90x',"$path/90_90/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1', '79x',"$path/79_79/$key.jpg");
	Image("$path/orig/$key.jpg", '1.15:1', '137x',"$path/137_119/$key.jpg");
 	Image("$path/orig/$key.jpg", '1.335:1', '297x',"$path/297_222/$key.jpg");
	Image("$path/orig/$key.jpg", '2.5:1', '188x',"$path/188_75/$key.jpg");
	Image("$path/orig/$key.jpg", '1.445:1', '364x',"$path/364_252/$key.jpg");
	Image("$path/orig/$key.jpg", '1.4:1', '73x',"$path/73_54/$key.jpg");



}


function setDefaultImage($coredb,$package,$image)
{
	if($image <= 9)
		$image = "0".$image;
		
		$path = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/uploaded_images/accomodation/$coredb/$package/orig";
		rename("$path/$image.jpg", "$path/0.jpg");
		processImages($coredb,$package);
}
function deleteImage($coredb,$package,$image)
{
	if($image <= 9)
		$image = "0".$image;

		$image = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/uploaded_images/accomodation/$coredb/$package/orig/$image.jpg";
		//echo $image;
		unlink($image);
}
function cleanup($page) {
	return preg_replace('!\s+!smi', ' ', str_replace("hotel-world.hu","indulhatunk.hu",$page));  
	return $page;
}

function processImages($coredb,$package){
	
	global $dbconn;
	
	$path = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/uploaded_images/accomodation/$coredb/$package";
	
		makedir("$path/orig/");
		makedir("$path/79_79/");	
		makedir("$path/90_90/");
		makedir("$path/137_119/");
		makedir("$path/137_137/");
		makedir("$path/140_85/");
		makedir("$path/169_172/");
		makedir("$path/171_99/");
		makedir("$path/177_124/");
		makedir("$path/297_222/");
		makedir("$path/188_75/");
		makedir("$path/364_252/");
		makedir("$path/73_54/");

	$directory = @dirList("$path/orig");

	natsort($directory);
	$key = 1;
	
	foreach($directory as $k => $image)
	{
		rename("$path/orig/$image", "$path/orig/s_$image");
	}
	foreach($directory as $k => $image)
	{
		
		if($key <= 9)
			$key = "0".$key;
		
		
	rename("$path/orig/s_$image", "$path/orig/$key.jpg");
	Image("$path/orig/$key.jpg", '1.65:1', '140x',"$path/140_85/$key.jpg");
	Image("$path/orig/$key.jpg", '1.43:1', '177x',"$path/177_124/$key.jpg");
	Image("$path/orig/$key.jpg", '1.73:1', '171x',"$path/171_99/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1.015', '169x',"$path/169_172/$key.jpg");
    Image("$path/orig/$key.jpg", '1:1', '137x',"$path/137_137/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1', '90x',"$path/90_90/$key.jpg");
	Image("$path/orig/$key.jpg", '1:1', '79x',"$path/79_79/$key.jpg");
	Image("$path/orig/$key.jpg", '1.15:1', '137x',"$path/137_119/$key.jpg");
 	Image("$path/orig/$key.jpg", '1.335:1', '297x',"$path/297_222/$key.jpg");
	Image("$path/orig/$key.jpg", '2.5:1', '188x',"$path/188_75/$key.jpg");
	Image("$path/orig/$key.jpg", '1.445:1', '364x',"$path/364_252/$key.jpg");
	Image("$path/orig/$key.jpg", '1.4:1', '73x',"$path/73_54/$key.jpg");
	
		$key++;
		//update image num
	} 

	//update image number
	$query  = "UPDATE ACCOMODATION SET IMAGE_NUM = ".count($directory).", IMAGE_GEN = '1' WHERE ID = $package";
	$stmt = oci_parse($dbconn, $query);
	oci_execute($stmt, OCI_DEFAULT);
	oci_commit($dbconn);

	return true;
}


function Image($image, $crop = null, $size = null,$fileName = "") {
    $image = ImageCreateFromString(file_get_contents($image));

    if (is_resource($image) === true) {
        $x = 0;
        $y = 0;
        $width = imagesx($image);
        $height = imagesy($image);

        /*
        CROP (Aspect Ratio) Section
        */

        if (is_null($crop) === true) {
            $crop = array($width, $height);
        } else {
            $crop = array_filter(explode(':', $crop));

            if (empty($crop) === true) {
                    $crop = array($width, $height);
            } else {
                if ((empty($crop[0]) === true) || (is_numeric($crop[0]) === false)) {
                        $crop[0] = $crop[1];
                } else if ((empty($crop[1]) === true) || (is_numeric($crop[1]) === false)) {
                        $crop[1] = $crop[0];
                }
            }

            $ratio = array(0 => $width / $height, 1 => $crop[0] / $crop[1]);

            if ($ratio[0] > $ratio[1]) {
                $width = $height * $ratio[1];
                $x = (imagesx($image) - $width) / 2;
            }

            else if ($ratio[0] < $ratio[1]) {
                $height = $width / $ratio[1];
                $y = (imagesy($image) - $height) / 2;
            }

        }

        /*
        Resize Section
        */

        if (is_null($size) === true) {
            $size = array($width, $height);
        }

        else {
            $size = array_filter(explode('x', $size));

            if (empty($size) === true) {
                    $size = array(imagesx($image), imagesy($image));
            } else {
                if ((empty($size[0]) === true) || (is_numeric($size[0]) === false)) {
                        $size[0] = round($size[1] * $width / $height);
                } else if ((empty($size[1]) === true) || (is_numeric($size[1]) === false)) {
                        $size[1] = round($size[0] * $height / $width);
                }
            }
        }

       $result = ImageCreateTrueColor($size[0], $size[1]);

        if (is_resource($result) === true) {
            ImageSaveAlpha($result, true);
            ImageAlphaBlending($result, true);
            ImageFill($result, 0, 0, ImageColorAllocate($result, 255, 255, 255));
            ImageCopyResampled($result, $image, 0, 0, $x, $y, $size[0], $size[1], $width, $height);

            ImageInterlace($result, true);
            ImageJPEG($result, $fileName, 100);
        }
    }

    return false;
}

function makedir($dir, $mode = 0777) {
  if (is_dir($dir) || @mkdir($dir,$mode)) return TRUE;
  if (!makedir(dirname($dir),$mode)) return FALSE;
  return @mkdir($dir,$mode);
}

function dirList ($directory) 
{
    $results = array();
    $handler = opendir($directory);
	    while ($file = readdir($handler)) {

        if ($file != '.' && $file != '..')
            $results[] = $file;
    }
    closedir($handler);
    return $results;

}
function format_date($date,$delimiter = '.')
{	
	if($delimiter == "-")
	{
		$date = date("Y-m-d",strtotime($date));
	}
	else
	{
		$date = date("Y.m.d.",strtotime($date));
	}
	return $date;
}
function oracle_connect()
{

	$db = new OCI8DB;
	$dbconn = $db->dbconn();
}

function generateHash($length = "")
{	
	$code = md5(uniqid(rand(), true));
	if ($length != "") return substr($code, 0, $length);
	else return $code;
}
function _date_diff($time1 = '',$time2 = '',$format = 'hours')
{
	if($format == 'hours')
		$diff = round((strtotime($time1) - strtotime($time2))/3600, 1).' óra';
	else
		$diff = round((strtotime($time1) - strtotime($time2))/3600/24, 1).' nap';
	return $diff;
}
function accomodationType($dayDate) {
	
	global $dayOffs;
		$today = date('N', strtotime($dayDate));
			
	//$today = date('N', strtotime($dayDate));
	
//	echo $today."<hr/>";
	foreach($dayOffs as $dayFrom => $dayTo) {
		if($dayDate >= $dayFrom && $dayDate <= $dayTo ) {
			$celebration = TRUE;
		
			$nowdate = strtotime($dayDate);

			$thendate = strtotime($dayTo);
			$datediff = ($thendate - $nowdate);
			$dLeft = ($datediff/60/60/24)+1;
		//	echo "ma unnep van";
		}
		//if($dayDate <= $dayFrom && $dayDate <= $dayTo)
		//	z
	}
		
		if($celebration == TRUE) {
			$typeQuery = "ACCOMODATION.ENABLED_DAYS = 1 AND PRICES.DAY_NUM <= $dLeft AND";
		}

		else {
			if($today == 7) { //sunday
				$typeQuery = "(ACCOMODATION.ENABLED_DAYS = 3 or ACCOMODATION.ENABLED_DAYS = 4) AND";
			}
			elseif($today == 1) {
				$typeQuery = "((ACCOMODATION.ENABLED_DAYS = 3 AND PRICES.DAY_NUM < 5) OR ACCOMODATION.ENABLED_DAYS = 4) AND ACCOMODATION.ENABLED_DAYS > 0 AND";
			}
			elseif($today == 2) {
				$typeQuery = "((ACCOMODATION.ENABLED_DAYS = 3 AND PRICES.DAY_NUM < 4) OR ACCOMODATION.ENABLED_DAYS = 4) AND ACCOMODATION.ENABLED_DAYS > 0 AND";
			}
			elseif($today == 3) {
				$typeQuery = "((ACCOMODATION.ENABLED_DAYS = 3 AND PRICES.DAY_NUM < 3) OR ACCOMODATION.ENABLED_DAYS = 4) AND ACCOMODATION.ENABLED_DAYS > 0 AND";
			}
			elseif($today == 4) {
				$typeQuery = "((ACCOMODATION.ENABLED_DAYS = 3 AND PRICES.DAY_NUM < 2) or ACCOMODATION.ENABLED_DAYS = 4) AND ACCOMODATION.ENABLED_DAYS > 0 AND";
			}
			elseif($today == 5) {
				$typeQuery = "((ACCOMODATION.ENABLED_DAYS = 2 AND PRICES.DAY_NUM = 2) OR  ACCOMODATION.ENABLED_DAYS = 4) AND ACCOMODATION.ENABLED_DAYS > 0 AND";
			}
			elseif($today == 6) {
				$typeQuery = "((ACCOMODATION.ENABLED_DAYS = 2 AND  PRICES.DAY_NUM < 2) OR ACCOMODATION.ENABLED_DAYS = 4)  AND ACCOMODATION.ENABLED_DAYS > 0 AND";
			}
		/*	if($today == 5 || $today == 6) {
				$typeQuery = "(ACCOMODATION.ENABLED_DAYS = 2 or ACCOMODATION.ENABLED_DAYS = 4) AND";
			}
			elseif($today == 7 || $today < 5) {
				$typeQuery = "(ACCOMODATION.ENABLED_DAYS = 3 or  ACCOMODATION.ENABLED_DAYS = 4) AND";
			}
			else {
				$typeQuery = "ACCOMODATION.ENABLED_DAYS = 4 AND";
			}
			*/
		}
		return $typeQuery;
}

function getOracleSQL($query)
{
	global $dbconn;	
 
		$stmt = parseQuery($dbconn, $query);
		$result = array();
		while($arr = oci_fetch_assoc($stmt)) 
		{
			$result[] = $arr;
			
		}
		if(empty($result) == TRUE)
		{
		//	echo "empty <hr/>";
			$result = array();
			//$result[xyz] = 1;
		}
//	if($result[1] == '')
//		$result = $result[0];	 
//	print_r($result);
	//echo "<hr/>";
	return $result;


}
function debug($text)
{
	echo "<pre>";
	print_r($text);
	echo "</pre>";
	
}

function lookup($string){
 
   $string = str_replace (" ", "+", urlencode($string));
   $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";
 
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $details_url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $response = json_decode(curl_exec($ch), true);
 
   // If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
   if ($response['status'] != 'OK') {
    return null;
   }
 
   $geometry = $response['results'][0]['geometry'];
 
    $longitude = $geometry['location']['lng'];
    $latitude = $geometry['location']['lat'];
 
    $array = array(
        'latitude' => $geometry['location']['lat'],
        'longitude' => $geometry['location']['lng'],
        'location_type' => $geometry['location_type'],
    );
 
    return $array;
 
}



function cachedSQL($query,$time = 43200)  {
	global $memcached;
	global $dbconn;	
 
//create an index key for memcache
	$key = md5('query'.$query);
 
	//lookup value in memcache
	
	$result = $memcached->get($key);
	//check if we got something back
	if($result == null) {
	//	echo "<font color=red> $key = not cached $query <hr/></font>";
		$stmt = parseQuery($dbconn, $query);
		
		$result = array();
		while($arr = oci_fetch_assoc($stmt)) 
		{
			$result[] = $arr;
			
		}
		//pre($result);
			
		if(empty($result) == TRUE)
		{
		//	echo "empty <hr/>";
			$result = array();
			$result[xyz] = 1;
		}
		
		$memcached->set($key,$result,$time);
	}
	else {
	//	echo "cached $query<hr/>";
		$result = $result;
	}
 
	//print_r($result);
	//echo "<hr/>";
	return $result;
}
 /*
function sendEmail($subject,$body,$to)
{
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= "From: Szallasoutlet.hu <vtl@indulhatunk.hu>\r\n";

	// Mail it
	mail($to,iconv('utf-8','ISO-8859-2',$subject), $body ,iconv('utf-8','ISO-8859-2',$headers));
}
*/

function sendEmailPlain($subject,$body,$to,$fromemail = 'vtl@indulhatunk.hu', $fromname = 'Szállás Outlet' )
{
	global $outletmail;
	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("$fromname") . '?='; 
	
	$headers .= "From: $from <$fromemail>\r\n";
	
	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	mail($to,$subject, $body ,iconv('utf-8','utf-8',$headers));
}


function sendNotification($subject,$body,$to,$name='',$fromemail = 'ertesito@hoteloutlet.hu', $fromname = 'Szállás Outlet' )
{
	global $outletmail;
	global $outletmailNew;
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("$fromname") . '?='; 
	
	$headers .= "From: $from <$fromemail>\r\n";


	/*$file = "newsletters/outlet/index.php";
	$fh = fopen($file, 'r');
	$data = fread($fh, filesize($file));
	fclose($fh);*/
	
	$data = $outletmailNew;



	$body = str_replace("###BODY###",$body,$data);
	$body = str_replace("###SUBJECT###",$subject,$body);
	$body = str_replace("###NAME###",$name,$body);
	
	$body = str_replace("vasarlas.szallasoutlet.hu","ertesito.hoteloutlet.hu",$body);
	$body = str_replace("szallasoutlet.hu","ertesito.hoteloutlet.hu",$body);
	$body = str_replace("indulhatunk.info","Szállás Outlet",$body);

	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	
	echo $body;
	
	$t = explode(",",$to);
	
	foreach($t as $email)
	{
		mail(trim($email),$subject, $body ,iconv('utf-8','utf-8',$headers),"-f $fromemail");
	}
}




function sendAmazon($subject,$body,$to,$name='',$fromemail = 'vtl@indulhatunk.hu', $fromname = 'Indulhatunk.hu', $source = '',$amazon = 0)
{
	 sendEmail($subject,$body,$to,$name,$fromemail, $fromname, $source);
}



function sendEmail($subject,$body,$to,$name='',$fromemail = 'vtl@indulhatunk.hu', $fromname = 'Szállás Outlet', $source = '',$amazon = 0,$disablesignature = 0, $tags = array('admin-mail'))
{
	global $lang;
	global $outletmail;
	global $outletmailNew;
	global $rajongokmail;
	global $indulhatunkmail;
	global $indulhatunkmailnew;
	global $mysql;
	
	
	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("$fromname") . '?='; 
	
	$headers .= "From: $from <$fromemail>\r\n";

	$offices = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter_templates WHERE id = 21 LIMIT 1"));
	$offices = $offices[body];
	/*$file = "newsletters/outlet/index.php";
	$fh = fopen($file, 'r');
	$data = fread($fh, filesize($file));
	fclose($fh);*/
	
	$plainbody = strip_tags(str_replace("<br/>","\n",$body)); 
	
	if($source == 'rajongok')
	{
		$template = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter_templates WHERE id = 25 LIMIT 1"));
		$data = $template[body];
		//$data=  $indulhatunkmailnew;
	}
	elseif($source == 'plain')
		$data= $body;
	elseif($source == 'indulhatunk')
	{
		$template = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter_templates WHERE id = 23 LIMIT 1"));
		$data = $template[body];
		//$data=  $indulhatunkmailnew;
	}
	elseif($source == 'horizonttravel')
	{
		$template = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter_templates WHERE id = 38 LIMIT 1"));
		$data = $template[body];
		//$data=  $indulhatunkmailnew;
	}

	elseif($source == 'fullhtml')
	{
		$data = $body;
		//$data=  $indulhatunkmailnew;
	}
	else
	{
		$template = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter_templates WHERE id = 22 LIMIT 1"));
		$data = $template[body];
		//$data=  $indulhatunkmailnew;
	}
	
	$body = str_replace("###BODY###",$body,$data);
	$body = str_replace("###SUBJECT###",$subject,$body);
	$body = str_replace("###NAME###",$name,$body);
	
	
	$body = str_replace("###DEAR###",$lang[dear],$body);
	$body = str_replace("###THANKS###",$lang[thanks],$body);
	
	if($disablesignature == 1)
		$body = str_replace("###SIGNATURE###",'',$body);
	else
		$body = str_replace("###SIGNATURE###",$lang[signature],$body);
	$body = str_replace("###SZALLASURL###",$lang[szallasurl],$body);
	$body = str_replace("###LETTERFOOTER###",$lang[letterfooter],$body);
	$body = str_replace("###TOPIMAGE###",$lang[countrycodelong],$body);
	$body = str_replace("###CONTACTUS###",$lang['#CONTACTUS#'],$body);
	$body = str_replace("###DOWNLOADAPP###",$lang['#DOWNLOADAPP#'],$body);
	$body = str_replace("#TOPBANNER#",$lang['#TOPBANNERLETTER#'],$body);

	$body = str_replace("###LETTERAPP###",$lang['#DOWNLOADAPP#'],$body);
	$body = str_replace("###DOWNLOADAPP###",$lang['#DOWNLOADAPP#'],$body);

	if($lang[countrycode] == 'hu')
		$body = str_replace("[@offices]",$offices,$body);
	else
		$body = str_replace("[@offices]","",$body);

		$officedata = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter_templates WHERE id = 39 LIMIT 1"));

	if($lang[countrycode] == 'hu')
		$body = str_replace("[@offices_outlet]",$officedata[body],$body);
	else
		$body = str_replace("[@offices_outlet]","",$body);

	//print $body; die();

	//$template->set("offices_outlet",$officedata[body]);
	
	
	/*$body = str_replace("vasarlas.szallasoutlet.hu","vasarlas.indulhatunk.com",$body);
*/
	$body = str_replace("vasarlas.szallasoutlet.hu","admin.indulhatunk.hu",$body);
	//$body = str_replace("Üdvözlettel: az indulhatunk.info csapata","",$body);
	//$body = str_replace("szallasoutlet.hu ","",$body);

	$plainbody = str_replace("vasarlas.szallasoutlet.hu","admin.indulhatunk.hu",$plainbody);

	$sb = $subject;
	
	
	//$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	
	$t = explode(",",$to);
	
	foreach($t as $email) 
	{
		$domain = explode("@",$email);
		$domain = $domain[1];
		
		$hash = md5($email.$sb.time());
		
		
		$body = str_replace("###HASH###",$hash,$body);



		$url = 'https://api.sparkpost.com/api/v1/transmissions';
   
		$data = array(
		"content" => array(
			"from" => array(
				"name" => $fromname,
				"email" => $fromemail
			),
			"reply_to" => "$fromname <$fromemail>",
			"subject" => $subject,
			"html" => $body,
			"text" => strip_tags($body)
		),
	    "recipients" => array(
	    	array(
	    		"address" => $to,  
	    		"name"  => $name
	    	)
	    )
	    );                                                                    
		$data = json_encode($data);                                                                                   
		   
		                                                                                                                     
		$ch = curl_init($url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                               
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',    
		    'Authorization: 9acef253edaf3f270bb7e42072fdea3a6ea3ecde',                                                                            
		    'Content-Length: ' . strlen($data))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = curl_exec($ch);       
	
/*
		
		  $mandrill = new Mandrill('Z-UBthxSVOy4s9wpLVrbtw');
    $message = array(
        'html' => $body,
        'subject' => $subject,
        'from_email' => $fromemail,
        'from_name' => $fromname,
        'to' => array(
            array(
                'email' => $to,
                'name' => $name,
                'type' => 'to'
            )
        ),
        'headers' => array('Reply-To' =>  $fromemail),
        'important' => true,
        'track_opens' => 1,
        'track_clicks' => 1,
        'auto_text' => 1,
        'auto_html' => 1,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => null,
        'view_content_link' => null,
        'signing_domain' => 'indulhatunk.hu',
        'return_path_domain' => null,
        'merge' => true,
        'tags' => $tags,
        'google_analytics_domains' => array('indulhatunk.hu'),
        'google_analytics_campaign' => 'admin-notification',
     );
    $async = 1;
    $ip_pool = 'Main Pool';
   $result = $mandrill->messages->send($message, $async, $ip_pool);
   */
   
	}
}

/*
function sendEmail($subject,$body,$to,$name='',$fromemail = 'vtl@indulhatunk.hu', $fromname = 'Szállás Outlet' )
{
	global $outletmail;
	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("$fromname") . '?='; 
	
	$headers .= "From: $from <$fromemail>\r\n";


	$data = $outletmail;

	$body = str_replace("###BODY###",$body,$data);
	$body = str_replace("###SUBJECT###",$subject,$body);
	$body = str_replace("###NAME###",$name,$body);
	
	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	$t = explode(",",$to);
	
	foreach($t as $email)
	{
		mail(trim($email),$subject, $body ,iconv('utf-8','utf-8',$headers));
	}
}

*/

function sendEmailRajongok($subject,$body,$to,$name='')
{
	global $rajongokmail;
	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("Szállás Rajongók") . '?='; 
	$headers .= "From: $from <foglalas@szallasrajongok.hu>\r\n";


	/*$file = "newsletters/outlet/index.php";
	$fh = fopen($file, 'r');
	$data = fread($fh, filesize($file));
	fclose($fh);*/
	
	$data = $rajongokmail;

	$body = str_replace("###BODY###",$body,$data);
	$body = str_replace("###SUBJECT###",$subject,$body);
	$body = str_replace("###NAME###",$name,$body);
	
	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	$t = explode(",",$to);
	
	foreach($t as $email)
	{
		mail(trim($email),$subject, $body ,iconv('utf-8','utf-8',$headers));
	}
	//mail($to,$subject, $body ,iconv('utf-8','utf-8',$headers));
}

function sendEmailLmsz($subject,$body,$to,$name='')
{
	global $mumail;
	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("Modok Angéla - MostUtazz.hu ügyfélszolgálat") . '?='; 
	$headers .= "From: $from <foglalas@indulhatunk.hu>\r\n";


	/*$file = "newsletters/outlet/index.php";
	$fh = fopen($file, 'r');
	$data = fread($fh, filesize($file));
	fclose($fh);*/
	
	$data = $mumail;

	$body = str_replace("###BODY###",$body,$data);
	$body = str_replace("###SUBJECT###",$subject,$body);
	$body = str_replace("###NAME###",$name,$body);
	
	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	$t = explode(",",$to);
	
	foreach($t as $email)
	{
		mail(trim($email),$subject, $body ,iconv('utf-8','utf-8',$headers));
	}
	//mail($to,$subject, $body ,iconv('utf-8','utf-8',$headers));
}
function sendEmailGrando($subject,$body,$to,$name='')
{
	global $grandoemail;
	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("Grando.hu szállás") . '?='; 
	$headers .= "From: $from <foglalas@indulhatunk.hu>\r\n";


	/*$file = "newsletters/outlet/index.php";
	$fh = fopen($file, 'r');
	$data = fread($fh, filesize($file));
	fclose($fh);*/
	
	$data = $grandoemail;

	$body = str_replace("###BODY###",$body,$data);
	$body = str_replace("###SUBJECT###",$subject,$body);
	$body = str_replace("###NAME###",$name,$body);
	
	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	$t = explode(",",$to);
	
	foreach($t as $email)
	{
		mail(trim($email),$subject, $body ,iconv('utf-8','utf-8',$headers));
	}
	//mail($to,$subject, $body ,iconv('utf-8','utf-8',$headers));
}
function sendEmailLmb($subject,$body,$to,$name='')
{
	global $mumail;
	
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	$from = '=?utf-8?B?' . base64_encode("Last-minute-belföld.hu") . '?='; 
	$headers .= "From: $from <foglalas@indulhatunk.hu>\r\n";


	/*$file = "newsletters/outlet/index.php";
	$fh = fopen($file, 'r');
	$data = fread($fh, filesize($file));
	fclose($fh);*/
	
	$data = $mumail;

	$body = str_replace("###BODY###",$body,$data);
	$body = str_replace("###SUBJECT###",$subject,$body);
	$body = str_replace("###NAME###",$name,$body);
	
	$subject = '=?utf-8?B?' . base64_encode($subject) . '?='; 
	
	$t = explode(",",$to);
	
	foreach($t as $email)
	{
		mail(trim($email),$subject, $body ,iconv('utf-8','utf-8',$headers));
	}
	//mail($to,$subject, $body ,iconv('utf-8','utf-8',$headers));
}


function removeSpecialChars($text) 
{
	$from= array("é", "á", "í", "ó", "ö", "ő", "ü", "ű", "ú","Í","Ü","Á","Ő","É");
	$to=array("e", "a", "i", "o", "o", "o", "u", "u", "u","i","u","A","O","E");
	$string= strtoupper(str_replace($from,$to,$text));
	return $string;
}
function commitQuery($query) {
	global $dbconn;
	$st = oci_parse($dbconn, $query);
	oci_execute($st, OCI_DEFAULT);
	oci_commit($dbconn);
	//echo "$query<hr/>";
	return $st;
}

function parseQuery($dbconn,$query) {
	$st = oci_parse($dbconn, $query);
	oci_execute($st, OCI_DEFAULT);
	
	//echo "$query<hr/>";

	return $st;
}
function userlogin() {
	global $mysql;
	global $_COOKIE;
	
    unset($GLOBALS["CURUSER"]);
     if (empty($_COOKIE["pid"]) || empty($_COOKIE["password"]))
        header("Location: https://admin.indulhatunk.hu/login.php");
    $id = (int)$_COOKIE["pid"];
    if (!$id || strlen($_COOKIE["password"]) != 32)
         header("Location: login.php");
         $mysql->query("SET NAMES 'utf8'");
    $res = $mysql->query("SELECT * FROM partners WHERE pid = $id");
    $row = mysql_fetch_array($res);
    if (!$row)
         header("Location: https://admin.indulhatunk.hu/login.php");
    if ($_COOKIE["password"] !== md5($row["password"]))
         header("Location: https://admin.indulhatunk.hu/login.php");
         
    $row[reseller_office] =  $_COOKIE[officecode];
    $GLOBALS["CURUSER"] = $row;
    
    
    $mysql->query("UPDATE partners SET last_login = NOW(), ip = '$_SERVER[REMOTE_ADDR]' WHERE pid = $id");
}
function logout() {
	session_destroy();
	setcookie("username", "", time()+360000,'/', '.indulhatunk.hu');
	setcookie("password", "", time()+360000,'/', '.indulhatunk.hu');
	setcookie("pid", "", time()+360000,'/', '.indulhatunk.hu');
	setcookie("officecode", "", time()+360000,'/', '.indulhatunk.hu');
	setcookie("showdetails", '', time()+86400*10,'/', '.indulhatunk.hu');

	header("location: login.php");
}

// ==============================================================================================================
// ==============================================================================================================
/**
 * xml2array(): XML adatokból tömböt (array) készít
 *
 * @param string $contents			XML adatok
 * @param string $get_attributes	attribútumokkal is? (1= igen)
 * @return array					tömb
 */
function xml2array($contents, $get_attributes=1) { 
    if(!$contents) return array(); 

    if(!function_exists('xml_parser_create')) { 
        //print "'xml_parser_create()' function not found!"; 
        return array(); 
    } 
    //Get the XML parser of PHP - PHP must have this module for the parser to work
    // $contents => $xml_values 
    $parser = xml_parser_create(); 
    xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 ); 
    xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 ); 
    xml_parse_into_struct( $parser, $contents, $xml_values);
    xml_parser_free( $parser ); 

    if(!$xml_values) return;	//Hmm... 

    //Initializations 
    $xml_array = array(); 
    $parent = array();	// parents volt! 
    $opened_tags = array(); 
    $arr = array(); 

    $current = &$xml_array; 

    //Go through the tags. 
    foreach($xml_values as $data) { 
        unset($attributes,$value);//Remove existing values, or there will be trouble
        // A tömböt szétszedjük elemeire: $tag, $type, $level, $attributes 
        extract($data);//We could use the array by itself, but this cooler.

        $result = ''; 
        if($get_attributes) {//The second argument of the function decides this. 
            $result = array(); 
            if(isset($value)) $result['value'] = $value;  // original $result['value'] = $value; // $result = $value 

            //Set the attributes too. 
            if(isset($attributes)) { 
                foreach($attributes as $attr => $val) {
                	 
                    if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr' 
                    /**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */ 
                } 
            } 
        } elseif(isset($value)) { 
            $result = $value; 
        } 

        //See tag status and do the needed. 
        if($type == "open") {//The starting of the tag '<tag>' 
            $parent[$level-1] = &$current; 

            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag 
                $current[$tag] = $result; 
                $current = &$current[$tag]; 

            } else { //There was another element with the same tag name 
                if(isset($current[$tag][0])) { 
                    array_push($current[$tag], $result); 
                } else { 
                    $current[$tag] = array($current[$tag],$result); 
                } 
                $last = count($current[$tag]) - 1; 
                $current = &$current[$tag][$last]; 
            } 

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />' 
            //See if the key is already taken. 
            if(!isset($current[$tag])) { //New Key 
                $current[$tag] = $result; 

            } else { //If taken, put all things inside a list(array) 
                if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array... 
                        or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) { 
                    array_push($current[$tag],$result); // ...push the new element into that array. 
                } else { //If it is not an array... 
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value 
                } 
            } 

        } elseif($type == 'close') { //End of tag '</tag>' 
            $current = &$parent[$level-1]; 
        } 
    } 

    return($xml_array); 
}
// --------------------------------------------------------------------------------------------------------------
/**
 * NapiMNBArfolyamLetoltes(): napi MNB árfolyam letöltése
 *
 * @return unknown
 */
function NapiMNBArfolyamLetoltes() {
	$arfolyamok = false;

	$bdy = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	$bdy.= "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">";
	$bdy.= "<soap:Body>";
	$bdy.= "<GetCurrentExchangeRates xmlns=\"http://www.mnb.hu/webservices/\" />";
	$bdy.= "</soap:Body>";
	$bdy.= "</soap:Envelope>\r\n";
	
	$req = "POST /arfolyamok.asmx HTTP/1.1\r\n";
	$req.= "Host: www.mnb.hu\r\n";
	$req.= "Connection: Close\r\n";
	$req.= "Content-Type: text/xml; charset=utf-8\r\n";
	$req.= "Content-Length: ".strlen($bdy)."\r\n";
	$req.= "SOAPAction: \"http://www.mnb.hu/webservices/GetCurrentExchangeRates\"\r\n\r\n";
	
	$fs = fsockopen("www.mnb.hu", 80);
	fwrite($fs, $req.$bdy);
	$s = '';
	while (!feof($fs))
	{
	    $s .= fgets($fs);
	}
	fclose($fs);
	// Levesszük a fejlécet...
	$s = strstr($s, '<?xml');
	
	$data1 = xml2array($s);
	
	// Lekérdezés adatainak kiszedése...
	$s = $data1['soap:Envelope']['soap:Body']['GetCurrentExchangeRatesResponse']['GetCurrentExchangeRatesResult']['value'];
	$data2 = xml2array($s);
	$arfolyamok = array('datum'=> $data2['MNBCurrentExchangeRates']['Day']['attr']['date'], 'arfolyamok'=>$data2['MNBCurrentExchangeRates']['Day']['Rate'] );
	return $arfolyamok; 
}
// --------------------------------------------------------------------------------------------------------------
/**
 * MNBArfolyamLetoltes():
 *
 * @param unknown_type $currency
 * @param unknown_type $datum
 * @return unknown
 */
function MNBArfolyamLetoltes($currency = 'EUR', $datum = '') {
	global $mysql;
	
	$arfolyamok = false;
	
	
	// Ha van megadva dátum, akkor azt kéri le, egyébként meg az aktuálisat!
	if ($datum == '')
		$datum = date("Y-m-d");
		

		$bdy = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		$bdy.= "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">";
		$bdy.= "<soap:Body>";
		$bdy.= "<GetExchangeRates xmlns=\"http://www.mnb.hu/webservices/\">";
			$bdy.= "<startDate>".$datum."</startDate>";
			$bdy.= "<endDate>".$datum."</endDate>";
			$bdy.= "<currencyNames>".$currency."</currencyNames>";
		$bdy.= "</GetExchangeRates>";
		$bdy.= "</soap:Body>";
		$bdy.= "</soap:Envelope>\r\n";
		
		$req = "POST /arfolyamok.asmx HTTP/1.1\r\n";
		$req.= "Host: www.mnb.hu\r\n";
		$req.= "Connection: Close\r\n";
		$req.= "Content-Type: text/xml; charset=utf-8\r\n";
		$req.= "Content-Length: ".strlen($bdy)."\r\n";
		$req.= "SOAPAction: \"http://www.mnb.hu/webservices/GetExchangeRates\"\r\n\r\n";	// GetCurrentExchangeRates
		
		$fs = fsockopen("www.mnb.hu", 80);
		fwrite($fs, $req.$bdy);
		$s = '';
		while (!feof($fs))
		{
		    $s .= fgets($fs);
		}
		fclose($fs);
		// Levesszük a fejlécet...
		$s = strstr($s, '<?xml');
		
		$data1 = xml2array($s);
		
		// Lekérdezés adatainak kiszedése...
		$s = $data1['soap:Envelope']['soap:Body']['GetExchangeRatesResponse']['GetExchangeRatesResult']['value'];
		$data2 = xml2array($s);
		// Ha van erre a napra letölthető, akkor beállítja....
		if (isset($data2['MNBExchangeRates']['Day'])) {
			$arfolyamok = array('datum'=> $data2['MNBExchangeRates']['Day']['attr']['date'], 'arfolyamok'=>$data2['MNBExchangeRates']['Day']['Rate'] );
		// Egyébként üres.
		} else {
			$arfolyamok = false;
		}
	// 	
	
	
	$data = array();
	
	$data[value] = str_replace(",",".",$arfolyamok[arfolyamok][value]);
	$data[last_update] = 'NOW()';
	$data[day] = $arfolyamok[datum];

	$buy = mysql_fetch_assoc($mysql->query("SELECT * FROM settings WHERE id = 3 LIMIT 1"));
	$sell =  mysql_fetch_assoc($mysql->query("SELECT * FROM settings WHERE id = 2 LIMIT 1"));
	
	$data[buy] = $data[value]+$buy[value];
	$data[sell] = $data[value]-$sell[value];

	$mysql->query_update("settings",$data,"id=1");
	writelog("MNB was updated $data[day]: $data[value]");
	
	//$ret = array("date" => $arfolyamok[datum], 'value' => ));
//	return $ret; 
}
function writelog($log) {
	global $mysql;
	$mysql->query("INSERT INTO log (date,event) VALUES(NOW(),'$log')") or die(mysql_error()); 
}
function sendMail($to,$subject,$message) {
	$headers = "From: billing@indulhatunk.hu\r\n" .
        'X-Mailer: PHP/' . phpversion() . "\r\n" .
        "MIME-Version: 1.0\r\n" .
        "Content-Type: text/html; charset=utf-8\r\n" .
        "Content-Transfer-Encoding: 8bit\r\n\r\n";

		mail($to, $subject, ($message), $headers); 
}

function head_customers()
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <html xmlns="http://www.w3.org/1999/xhtml" lang="hu-HU"> 
<head>
	<link rel="stylesheet" type="text/css" media="all"   href="/inc/customers.css"/>
	<script src="/inc/js/jquery.js" type="text/javascript"></script>
	<script src="/inc/js/facebox.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.mask.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.watermark.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.numeric.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.date.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.datepicker.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.stickyscroll.js" type="text/javascript"></script>
	<script src="/inc/js/customer.js?ver=1.1" type="text/javascript"></script>
	
	<link href="/inc/jquery.fancybox.css" media="screen" rel="stylesheet" type="text/css"/>
	<script src="/inc/js/jquery.fancybox.js" type="text/javascript"></script> 
	
	<script type="text/javascript" src="http://www.google.com/jsapi?key=AIzaSyAJ-aWUxJbmDE6JyBu558mRp76kfeAjmLk"></script>
	<script type="text/javascript" charset="utf-8">
         google.load("maps", "2.x");
	</script>
    <title>Szállás Outlet | Indulhatunk.hu | Last-Minute-Belföld.hu - Vásárlás véglegesítése</title>
</head>
<body>
<div id='fb-root'></div>
<div id="container">
	<div id='header'></div>

	<div id="content">
<?
}
function foot_customers($cid = 0,$disable_faq = 0, $company = 'hoteloutlet') { 

global $lang;

global $mysql;

if($company == 'szallasoutlet')
{
	$company_name = 'SzállásOutlet Kft.';
	$company_name = '';
	$company_bank = 'Erste bank: 11600006-00000000-51899097<br/>Nemzetközi bankszámlaszámunk: HU63 11600006-00000000-51899097 SWIFT: GIBA HU HB';
	
	$tax = '23686355-2-41';
	$regno = '01-09-973465';
	$engno = 'U-001481';
	$bank = '11600006 - 00000000 - 51899097';
	$iban = 'HU63 11600006 - 00000000 - 51899097 SWIFT: GIBA HUHB';
}
elseif($company == 'indusz')
{
	$company_name = 'Indulhatunk utazásszervező Kft.';
	$company_bank = 'Erste Bank: 11600006 - 00000000 - 65875957<br/><b>IBAN:</b> HU63 11600006 00000000 65875957 <b>SWIFT:</b> GIBAHUHB';
	
	$tax = '24856850-2-41';
	$regno = '01-09-186167';
	$engno = 'U-001531';
	$bank = '11600006 - 00000000 - 65875957';
	$iban = 'HU63 11600006 - 00000000 - 65875957 SWIFT: GIBA HUHB';
}
elseif($company == 'professional') {
    $company_name = 'Professional Outlet Kft. ';
    $company_name = '';
    $company_bank = 'Erste bank: 11600006-00000000-76663561';
    
    $tax = '25520103-2-41';
    $regno = '01-09-279883';
    $engno = '';
    $bank = '11600006-00000000-76663561';
    $iban = '';
}
else
{
	$company_name = 'Hotel Outlet Kft.';
	$company_bank = 'Erste bank: 11600006 - 00000000 - 49170520<br/>Nemzetközi bankszámlaszámunk: HU63 11600006 - 00000000 - 49170520 SWIFT: GIBA HU HB';
	
	$tax = '23417555-2-41 ';
	$regno = '01-09-964685';
	$engno = 'U-001292';
	$bank = '11600006 - 00000000 - 49170520';
	$iban = 'HU63 11600006-00000000-49170520 SWIFT: GIBA HUHB';
}

if($cid > 0)
{
	$customer = mysql_fetch_assoc($mysql->query("SELECT offers_id,pid, sub_pid, offer_id FROM customers WHERE cid = $cid LIMIT 1"));
	
	if($customer[sub_pid] > 0)
		$pid = $customer[sub_pid];
	else
		$pid = $customer[pid];
		
	$partnerArr = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $pid LIMIT 1"));
	
	$offer = mysql_fetch_assoc($mysql->query("SELECT special_voucher FROM offers WHERE id = $customer[offers_id] LIMIT 1"));

	
	
}
?>


<div class="rightside">

<? if($disable_faq == 0) { ?>
	<div class="righttopgrey">Fontos tudnivalók</div>
<div class='rightcontentgrey'>
	
	<ul>
		<li><a rel="facebox" href="#bookinginfo" style="background:url('/images/bell.png');">Foglalni szeretnék!</a></li>
		<!--<li><a rel="facebox" href="#checkinfo" style="background:url('/images/ucs.png');">Üdülési csekkem van!</a></li>-->
		<li><a rel="facebox" href="#szepinfo" style="background:url('/images/ucs.png');">SZÉP kártyám van!</a></li>
		<li><a rel="facebox" href="#wheniget" style="background:url('/images/cal.png');">Mikor kapom meg az utalványt!</a></li>
		<li><a href="#asagift" rel="facebox" style="background:url('/images/gf.png');">Ajándékba vettem az utalványt</a></li>
		
	</ul>
	
	<div class="leftTopOther">
		<div id="bookinginfo" style="display:none">	
			<b>Foglalási szándékát kérjük az alábbi telefonon, illetve email címen jelezze.</b><br/><br/>
			<b><?=str_replace("|","",$partnerArr[hotel_name]);?></b><br/>
			<? echo "$partnerArr[zip] $partnerArr[city] $partnerArr[address]";?><br/>
			<? echo "$partnerArr[reception_phone]";?><br/>
			<a href='mailto:<?=$partnerArr[reception_email]?>'><?=$partnerArr[reception_email]?></a>
			
			<? if($offer[special_voucher] <> 1 ){ ?>
			<br/><br/>
			Foglalásnál kérjük hivatkozzon az alábbi utalvány számra:  <br/>
			<b><?=$customer[offer_id]?></b><br/><br/>
			Foglalni akár már most is tud, véglegesíteni csak a fizetés után.
			<? } ?>
		</div>
	</div>
	<div class="leftTopOther">
	
		<div id="szepinfo" style="display:none">	
		SZÉP kártyájának szállás zsebéről online és személyesen is fizethet (1056 Budapest, Váci utca 9. 21-es kapucsengő), a további zsebek érvényességéről kérjük érdeklődjön a <a href='mailto:vtl@indulhatunk.hu'>vtl@indulhatunk.hu</a> címen
		</div>
		<div id="checkinfo" style="display:none">	

<div style='text-align:center; padding:10px;font-weight:bold; font-size:14px; color:red; font-size:14px;'><b>FIGYELEM: Csak a 2012. december 31-ig beérkezett üdülési csekket áll módunkban elfogadni! Kérjük az idő rövidsége miatt hozzák be irodánkba! Köszönjük!</b></div>

Címünk: Hotel Outlet<br/>1056 Budapest, Váci utca 9. 2. em./3. (21-es kaputelefon)<br/>

<br/><br/>
Az Üdülési csekkeket elfogadni az alábbiak teljesülésével tudjuk: 
<ul>
	<li>- A csekk tulajdonosa a csekkeket aláírásával ellátta, </li>
	<li>- A csekk tulajdonosának lakcímkártyáján szereplő cím megjelölésre került, </li>
</ul>
		</div>
	</div>
	<div class="leftTopOther">
		<div id="wheniget" style="display:none">	
<b>Akár már most is megkaphatja az utalványát, ugyanis letöltheti magának.</b><br/>
(az utalvánnyal foglalni már tud, de a foglalását csak akkor véglegesítik, ha már ki is fizette az ellenértékét)<br/><br/>

<b>Személyesen bejön hozzánk,</b><br/> 
azonnal megkapja<br/><br/>
<b>Postai úton kéri,</b><br/>
az utalás, vagy az üdülési csekk megérkezése napján postára adjuk, ami kb 2 nap<br/>
díja: <?=$postage_fee_tax?> Ft<br/><br/>


		</div>
	</div>
	<div class="leftTopOther">
		<div id="asagift" style="display:none">	
	<b>Az utalvány nem névre szóló, azaz a felhasználási idő alatt bármikor elajándékozhatja. </b><br/><br/>
	FONTOS! Az utalvány pénzre nem váltható, azaz tovább értékesíteni tilos.
		</div>
	</div>

</div>
<div class='rightbottomgrey'></div>



<? if($partnerArr[coredb_id] && $partnerArr[image_num]) { ?>
		<div class='rightinfo'>
		<div class='righttop'>Képek</div>
		<div class='rightcontent partnerimage'>

		<? 
		
	
		
		for($i=1;$i<=$partnerArr[image_num];$i++)
		{	
			if($i < 10)
				$z = "0".$i;
			else
				$z = $i;
				
			if($i > 6)
				$hidden = 'hidden';
			
			echo "<a href='http://static.indulhatunk.hu/uploaded_images/accomodation/$partnerArr[coredb_id]/$partnerArr[coredb_package]/orig/$z.jpg' class='fancy_gallery $hidden' rel='fancy_gallery' title='$partnerArr[hotel_name] $i/$partnerArr[image_num]'><img src='http://static.indulhatunk.hu.hu/uploaded_images/accomodation/$partnerArr[coredb_id]/$partnerArr[coredb_package]/137_137/$z.jpg'/></a>";		
		}
		?>
		</div>
		<div class='rightbottom'></div>
	</div>
</div>
<? } ?>

<? } ?>

	<div class='rightinfo'>
		<div class='righttop'>Elérhetőségeink</div>
		<div class='rightcontent'>
			<div style='color:#d53501; font-weight:bold; padding:0 0 10px 0;font-size:18px'><?=$company_name?></div>
			<b>E-mail:</b> <a href='mailto:vtl@indulhatunk.hu'>vtl@indulhatunk.hu</a><br/>
			<b>Erste Bank:</b> <?=$bank?><br/>
			<span style='font-size:10px'><?=$iban?></span><br/>
			<b>Adószám:</b> <?=$tax?> <br/>
			<b>Cégj.:</b>  <?=$regno?> <br/>
			<b>MKEH eng.:</b>  <?=$engno?> 
			
			<div class='map' address='Budapest Váci utca 9.'></div>
		</div>
		<div class='rightbottom'></div>
	</div>
</div>



</div>
<div class="cleaner"></div>
</div>




</div>
</div>
<?=$lang[secretclub]?>
<div id="footer">	
	<div class='szeptitle'>SZÉP kártya elfogadóhely</div>
	<a href='http://otpszepkartya.hu' target="_blank"><img src='/images/otp.png' width='100' alt='OTP SZÉP'/></a>
	<a href='http://mkbszepkartya.hu' target="_blank"><img src='/images/mkb.png' width='100' alt='MKB SZÉP'/></a>
	<a href='http://szepkartya.kh.hu' target="_blank"><img src='/images/kh.png' width='100' alt='K&amp;H SZÉP'/></a>
	<br/>
	<a href='http://payu.hu/view/smarty/uploaded_images/120104100228-fizetesi-tajekoztato-20120102.pdf' alt='PayU Vásárlói tájékoztató' title='PayU Vásárlói tájékoztató' target="_blank"><img src='/images/payu.png' alt='PayU'/></a>

	<div class='copyright'>Minden jog fenntartva &copy;<?=date("Y");?> <?=$company_name?> | <a href='http://www.szallasoutlet.hu/felhasznalasi-feltetelek' target='_blank' style='color:black;'>Felhasználási feltételek</a></div>
</div>

</body>
</html>
<?
	
}
function head($title = '',$nomenu=0,$print=0,$countrycode = '',$newjquery=0) {
	global $CURUSER;
	//print_r($CURUSER);
	global $lang;
	global $mysql;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <html xmlns="http://www.w3.org/1999/xhtml" lang="hu-HU"> 
<head>
	<meta charset="UTF-8">
	<? if($newjquery == 1) { ?>
	<!--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" />
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
	<script>
	;(function($){
		  $.fn.datepicker.dates['hu'] = {
				days: ["vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat"],
				daysShort: ["vas", "hét", "ked", "sze", "csü", "pén", "szo"],
				daysMin: ["V", "H", "K", "Sze", "Cs", "P", "Szo"],
				months: ["január", "február", "március", "április", "május", "június", "július", "augusztus", "szeptember", "október", "november", "december"],
				monthsShort: ["jan", "feb", "már", "ápr", "máj", "jún", "júl", "aug", "sze", "okt", "nov", "dec"],
				today: "ma",
				weekStart: 1,
				clear: "töröl",
				titleFormat: "yyyy. MM",
				format: "yyyy.mm.dd"
			};
		}(jQuery));
	</script>
	<? } else { ?>
	
	<script src="/inc/js/jquery.js" type="text/javascript"></script>
	<? if($print == 1) { ?>
	
	
	<script src="/inc/js/jquery.date<?=$countrycode?>.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.datepicker.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.datepicker.multimonth.js" type="text/javascript"></script>
	

		<? } else {  ?>
		
	<script src="/inc/js/facebox.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.mask.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.watermark.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.numeric.js" type="text/javascript"></script>

	<script src="/inc/js/jquery.date.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.datepicker.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.datepicker.multimonth.js" type="text/javascript"></script>
	<script src="/inc/js/image-picker.js" type="text/javascript"></script>
	
	<script src="/inc/js/jquery.autocomplete.min.js" type="text/javascript"></script>


	<script src="/inc/js/pageload.js?ver=0.912" type="text/javascript"></script>


<link href="/inc/jquery.fancybox.css" media="screen" rel="stylesheet" type="text/css"/>
	<script src="/inc/js/jquery.fancybox.js" type="text/javascript"></script> 

	<script src="/inc/js/md5.js" type="text/javascript"></script>

		
	<? } } ?>
	
	<link rel="stylesheet" href="/inc/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/inc/style.css?<?=date("Y-m-d")?>" type="text/css" media="all" />
<title><?=strip_tags($title)?></title>

</head>

<? if($nomenu == 1) {
	echo "<style>body { background:none; }</style>";
}?>
<body>


<? if($nomenu <> 1) { ?> 

<div id='container'>


<div id="leftcontainer" class='non-print'>


			<div id="topHeader">
				<div id="logo"><a href="/" class="logo"><img src='/images/newlogo.png' style='width:120px;'/></a></div>
				<? if(!$CURUSER) { ?>
				
					<ul id="main-nav">
                     <li><a href="http://www.indulhatunk.hu" class="nav-top-item no-submenu" target='_blank'>Indulhatunk.hu</a></li>
                     <li><a href="http://www.szallasoutlet.hu" class="nav-top-item no-submenu" target='_blank'>SzállásOutlet.hu</a></li>
                     <li><a href="http://www.mostutazz.hu/" class="nav-top-item no-submenu" target='_blank'>MostUtazz.hu</a></li> 
					</ul>
				 <? } else {  ?>
				 <div class='statusbar'><?=$lang['welcome']?> <?=$CURUSER['company_name']?>!
				 	<br/><br/> <a href="/settings.php"><?=$lang['settings']?></a> | <a href="/logout.php"><?=$lang['logout']?></a>
				 </div>
				
				 <!-- -->
		<script>
				</script>
		<ul id="main-nav">
		
		<? if($CURUSER[userclass] == 255) { ?>
			<li><a href="/panel.php" class="nav-top-item no-submenu">Vezérlőpult</a></li>
			
		<? 
		}
		
	 if($CURUSER[userclass] == 30) { ?>
			<li><a href="/getOffers.php" class="nav-top-item no-submenu"><?=$lang['offers']?></a></li>
			
		<? 
		}
		
		if( $CURUSER[userclass] == 51 || $CURUSER[username] == "konyveles" || $CURUSER[userclass] == 255) { //accounting 
		?>
		<li><a href="/partners.php" class="nav-top-item no-submenu">Partnerek</a></li>
		<li><a href="/list_invoice.php" class="nav-top-item no-submenu">Számlák</a></li>
		<li><a href="/accountant.php" class="nav-top-item no-submenu">Számla Export</a></li>
		<li><a href="/invoice.php" class="nav-top-item no-submenu">Elszámolások, számlák</a></li>
		<li><a href="/foreign_invoice.php" class="nav-top-item no-submenu">Külföldi elszámolás</a></li>
		<li><a href="/accountant_test.php" class="nav-top-item no-submenu">Számla ellenőrzés (teszt)</a></li>
		<li><a href="/bankdispatch.php" class="nav-top-item no-submenu">Banki utalások</a></li>
		<li><a href="/checkout.php" class="nav-top-item no-submenu">Pénztár</a></li>

		<?
		}
			
		elseif( $CURUSER[userclass] == 55 || $CURUSER[userclass] == 255) { //accounting 
		?>
			<li><a href='#' class="nav-top-item">Indulhatunk.hu &darr;</a>
			<ul>
				<li><a href="/offers.php">Főoldali ajánlatok</a></li>
				<li><a href="/cities.php">Városok</a></li>
				<li><a href="/accomodation.php">Szállás ajánlatok</a></li>
				<li><a href="/accstats.php">Szállás kimutatás</a></li>

				<li><a href="/pages.php">Témaoldalak</a></li>
				<li><a href="/presentation.php">Bemutatók</a></li>
				<li><a href="/cities.php">Városok</a></li>
				<li><a href="/categories.php">Szolgáltatások, kategóriák</a></li>
				<li><a href="/flush.php" rel='facebox'>Cache törlése</a></li>
			</ul>
		</li>

		<li><a href='/newsletter.php' class='nav-top-item no-submenu'>Hírlevelek</a></li>
		<li><a href="/partners.php" class="nav-top-item no-submenu">Partnerek</a></li>

		<?
		}
		elseif($CURUSER[userclass] == 5)
		{
		?>
			<li><a href="/getOffers.php" class="nav-top-item no-submenu">Ajánlatok</a></li>
			<li><a href="/customers.php" class="nav-top-item no-submenu">Vásárlók kezelése</a></li>
			
			<? if($CURUSER[reseller_office] == 0 && $CURUSER[pid] <> '3557') { ?>
				<li><a href="/reseller-invoices.php" class="nav-top-item no-submenu">Elszámolások</a></li>
				<li><a href="/reseller-statistics.php" class="nav-top-item no-submenu">Statisztika</a></li>

			<? } ?>
			<? if($CURUSER[username] == 'mindenkupon.hu' || $CURUSER[username] == 'zleindler') { ?>
				<li><a href="/bankdispatch.php" class="nav-top-item no-submenu">Bank</a></li>
				<li><a href="/list_invoice.php" class="nav-top-item no-submenu">Számlák</a></li>
			<? } ?>
			<? if($CURUSER[id] == 3175) { ?>
				<li><a href="/invoices.php" class="nav-top-item no-submenu">VTL elszámolások</a></li>
			<? } ?>
			<? if($CURUSER[pid] == 3557) { ?>
				<li><a href="/reseller-green.php" class="nav-top-item no-submenu">Green elszámolások</a></li>
			<? } ?>
			
			
			<?/* if($CURUSER[pid] <> 2999) { ?>
				<li><a href="/reseller-iframe.php" class="nav-top-item no-submenu">Banner, felületek</a></li>
			
			<li><a href="/reseller-media.php" class="nav-top-item no-submenu">Média, dokumentumok</a></li>
			<? }*/ ?>
		<?
		}
		//left menubar for general users
		elseif($CURUSER[userclass] < 90)
		{
		?>
			<? 
			//if not grando or vatera user logs in, than display a different menu
			if($CURUSER["username"] <> 'vatera' && $CURUSER[username] <> 'grando') {?>
				<li><a href="/invoices.php" class="nav-top-item <? if($_SERVER[SCRIPT_NAME] == '/invoices.php') echo "current";?>"><?=$lang[invoices]?> &darr;</a>
					<ul>
					<? if($CURUSER[has_foreign] == 1) { ?>
							<li><a href='/foreign_invoice.php' class='<? if(trim($_SERVER[SCRIPT_NAME]) == '/foreign_invoice.php') echo "current";?>'>Balances</a></li>
					<? } else {  ?>
						<li><a href='/invoices.php?type=vtl' class='<? if(trim($_SERVER[QUERY_STRING]) == 'type=vtl') echo "current";?>'>Szállás Outlet</a></li>
						<li><a href='/invoices.php?type=lmb' class='<? if(trim($_SERVER[QUERY_STRING]) == 'type=lmb') echo "current";?>'>MostUtazz.hu</a></li>
					<? } ?>
					</ul>
				</li>
				<? if($CURUSER[has_foreign] == 1) { ?>
				<li><a href="/customers.php" class="nav-top-item no-submenu <? if($_SERVER[SCRIPT_NAME] == '/customers.php') echo "current";?>"><?=$lang[vatera_customers]?></a></li>

				<? } else { ?>
				<li><a href="/invoices.php" class="nav-top-item <? if($_SERVER[SCRIPT_NAME] == '/customers.php' || $_SERVER[SCRIPT_NAME] == '/lmb.php') echo "current";?>"><?=$lang[customers]?> &darr;</a>
					<ul>
						<li><a href='/customers.php' class='<? if($_SERVER[SCRIPT_NAME] == '/customers.php') echo "current";?>'><?=$lang[vatera_customers]?></a></li>
						<li><a href='/lmb.php' class='<? if($_SERVER[SCRIPT_NAME] == '/lmb.php') echo "current";?>'><?=$lang[lmb_customers]?></a></li>
					</ul>
				</li>
				<? } ?>
			<!--	<li><a href="/customers.php" class="nav-top-item no-submenu <? if($_SERVER[SCRIPT_NAME] == '/customers.php') echo "current";?>"><?=$lang[vatera_customers]?></a></li> -->
			<? if($CURUSER[userclass] == 255 || $CURUSER[pid] == 922 || $CURUSER[pid] == 3135) { ?>
			<li><a href='#' class="nav-top-item <? if($_SERVER[SCRIPT_NAME] == '/editoffers.php' || $_SERVER[SCRIPT_NAME] == '/rooms.php' || $_SERVER[SCRIPT_NAME] == '/gallery.php' || $_SERVER[SCRIPT_NAME] == '/settings.php') echo "current";?>">Facebook Szállás &darr;</a>
			<ul>
			<? if($CURUSER["userclass"] >= 90) { ?>
				<li><a href='/categories.php'>Szolgáltatás kategóriák</a></li>
			<? } ?>
				<li><a href='/settings.php' class="<? if($_SERVER[SCRIPT_NAME] == '/settings.php') echo "current";?>">Alapértelmezett beállítások</a></li>
				<li><a href='/gallery.php'  class="<? if($_SERVER[SCRIPT_NAME] == '/gallery.php') echo "current";?>">Képgaléria</a></li>
				<li><a href='/rooms.php' class="<? if($_SERVER[SCRIPT_NAME] == '/rooms.php') echo "current";?>">Szobatípusok</a></li>
				<li><a href='/editoffers.php' class="<? if($_SERVER[SCRIPT_NAME] == '/editoffers.php') echo "current";?>">Ajánlatok szerkesztése</a></li>
				<li><a href='#'><strike>Foglalások</strike>(hamarosan)</a></li>
				<!--<li><a href='/fullhouse.php'  class="<? if($_SERVER[SCRIPT_NAME] == '/fullhouse.php') echo "current";?>">Teltházas napok</a></li>-->
			</ul>
			</li>

		<? } ?>
			<? }
			
			else { ?>
				<ul>
					<li><a href='/lmb.php' class='nav-top-item no-submenu'><?=$lang[lmb_customers]?></a></li>
				</ul>
			<? } ?>
			<!-- <li><a href="/lmb.php" class="nav-top-item no-submenu <? if($_SERVER[SCRIPT_NAME] == '/lmb.php') echo "current";?>"><?=$lang[lmb_customers]?></a></li> -->
			<? if($CURUSER["username"] <> 'vatera'  && $CURUSER[username] <> 'grando') {?>
			
			<? if($CURUSER[coredb_id] > 0 && $CURUSER[has_foreign] <> 1)  { ?>
						<li><a href='/requests.php' class='nav-top-item no-submenu  <? if(trim($_SERVER[SCRIPT_NAME]) == '/requests.php') echo "current";?>'>Lekérések</a></li>

						<? } ?>
			<? if($CURUSER[passengers] <> 1) { ?>			
				<li><a href="/fullhouse.php" class="nav-top-item no-submenu <? if($_SERVER[SCRIPT_NAME] == '/fullhouse.php') echo "current";?>"><?=$lang[fullhouse]?></a></li>
				<!--<li><a href="/facebookoffers.php" class="nav-top-item no-submenu <? if($_SERVER[SCRIPT_NAME] == '/facebookoffers.php') echo "current";?>"><b>FacebookAJÁNLATOK</b></a></li>-->
			
				<? if($CURUSER[has_foreign] <> 1) { ?>

				<li><a href="/reviews.php" class="nav-top-item <? if($_SERVER[SCRIPT_NAME] == '/reviews.php') echo "current";?>"><?=$lang[reviews]?> &darr;</a>
					<ul>
						<li><a href='/reviews.php' class='<? if(trim($_SERVER[QUERY_STRING]) == '' && $_SERVER[SCRIPT_NAME] == '/reviews.php') echo "current";?>'>Minden értékelés</a></li>
						<li><a href='/reviews.php?report=1' class='<? if(trim($_SERVER[QUERY_STRING]) == 'report=1') echo "current";?>'>Jelentett értékelések</a></li>
					</ul>
				</li>
				<? } ?>
			<? } ?>	
			<? } ?>
		<? }
		
		if($CURUSER[userclass] >= 90)  { ?>

		<li><a href='/partners.php' class="nav-top-item no-submenu <? if($_SERVER[SCRIPT_NAME] == '/partners.php') echo "current";?>">Partnerek</a>
		<? if($CURUSER[vatera] == 1) { ?>
		<li><a href='#' class="nav-top-item <? if($_SERVER[SCRIPT_NAME] == '/customers.php') echo "current";?>">Utalványok &darr;</a>
			<ul>
				<li><a href="/customers.php" class="<? if($_SERVER[SCRIPT_NAME] == '/customers.php') echo "current";?>">Vásárlók kezelése</a></li>
				<!--<li><a href="/inactive.php">Inaktív vásárlók</a></li>
				<li><a href="/emails.php">Feldolgozatlan levelek</a></li> -->
				<? if($CURUSER[userclass] == 255)  { ?>
					<li><a href="/assets.php" class='adminfeature'>Kintévőségek kezelése</a></li>
					<li><a href="/barterstats.php" class='adminfeature'>Ajánlat eladási statisztika</a></li>
				<? } ?>
				
				<li><a href="/foreignstats.php">Külföldi vásárlók</a></li>
				
				
				<!--<li><a href="/check_list.php">Üdülési csekk alapítvány</a></li>-->
				
					<li><a href="/bankdispatch.php">Utalás keresése</a></li>
				<li><a href="/create_ask.php" class="<? if($_SERVER[SCRIPT_NAME] == '/create_ask.php') echo "current";?>">Díjbekérők</a></li>

				<!--<li><a href="/followup.php">Utánkövetés</a></li>-->
			</ul>
		</li>
		<li><a href="/checkout.php" class="nav-top-item no-submenu <? if($_SERVER[SCRIPT_NAME] == '/checkout.php') echo "current";?>">Pénztár</a></li>

		<? }
		} 
		if($CURUSER[lmb] == 1 && $CURUSER[userclass] >= 90)  { ?>
		
		<li><a href='#' class="nav-top-item">Mostutazz.hu&darr;</a>
			<ul>
				<li><a href="/lmb.php">MU admin</a></li>
				<li><a href="/lmb_check.php">MU ajánlatok listázása</a></li>
				<li><a href="/reviews.php">Értékelések</a></li>

			</ul>
		</li>
		<? }
		
		if($CURUSER[userclass] == 40 && $CURUSER[userclass] <> 51)
		{
		?>
			<li><a href='/newsletter.php' class='nav-top-item no-submenu'>Hírlevelek</a></li>
			<li><a href="/getOffers.php" class="nav-top-item no-submenu">Ajánlatok</a></li>
			<li><a href="/partners.php" class="nav-top-item no-submenu">Partnerek</a></li>


		<?
		}
		
		if($CURUSER[userclass] >= 20 && $CURUSER[userclass] <> 51 && $CURUSER[userclass] <> 40 && $CURUSER[userclass] <> 55 && $CURUSER[passengers] <> 1)  { ?>
		
		
		
		<li><a href='#' class="nav-top-item <? if($_SERVER[SCRIPT_NAME] == '/ltOffers.php' || $_SERVER[SCRIPT_NAME] == '/ltBids.php' || $_SERVER[SCRIPT_NAME] == '/ltCustomers.php' || $_SERVER[SCRIPT_NAME] == '/ltRestaurants.php') echo "current";?>">LicitTravel &darr;</a>
			<ul>
				<li><a href="/ltOffers.php">Ajánlatok</a></li>
				<li><a href="/partners.php">Partnerek</a></li>
				<li><a href="/questions.php">Kérdések</a></li>
				<li><a href="/ltBids.php">Licitek</a></li>
				<li><a href="/ltCustomers.php">Vásárlók</a></li>
		
				<li><a href="/ltStats.php">Statisztika</a></li>

				<li><a href="/ltRestaurants.php">Éttermek képei</a></li>
				<li><a href="/ltUsers.php">Auto Felhasználók</a></li>
				<li><a href="/ltPages.php">Statikus tartalom</a></li>
				<li><a href="/newsletter.php?type=licit">Hírlevél</a></li>

				<li><a href="/ltLog.php">Log</a></li>

			</ul>
		</li>
		<? }
		
		if($CURUSER[indulhatunk] == 1 && $CURUSER[userclass] >= 90)  { ?>
		<li><a href='#' class="nav-top-item">Indulhatunk.hu &darr;</a>
			<ul>
				<li><a href="/offers.php">Főoldali ajánlatok</a></li>
				<li><a href="/cities.php">Városok</a></li>
				<li><a href="/accomodation.php">Szállás ajánlatok</a></li>
				<li><a href="/accstats.php">Szállás kimutatás</a></li>

				<li><a href="/pages.php">Témaoldalak</a></li>
				<li><a href="/presentation.php">Bemutatók</a></li>
				<li><a href="/cities.php">Városok</a></li>

				<li><a href="/requests.php">Ajánlatkérések</a></li>
				<li><a href="/categories.php">Szolgáltatások, kategóriák</a></li>
				<li><a href="/flush.php" rel='facebox'>Cache törlése</a></li>
			</ul>
		</li>
		
		<? }
		 
		 if($CURUSER[passengers] == 1 || $CURUSER[Passengers] == 1 || $CURUSER[username] == 'forrotamas')
		{
		?>
			<li><a href='/passengers.php' class="nav-top-item  no-submenu <? if($_SERVER[SCRIPT_NAME] == '/passengers.php') echo "current";?>" class=''>Utasok</a></li>
		<? }
		 
		 if($CURUSER[passengers] == 1 && ($CURUSER[username] == 'berki.janos' || $CURUSER[username] == 'gergely.solymosi'))
		{
		?>
			<li><a href='/passengers_check2.php?checkout=&company_invoice=szallasoutlet&agent_id=0&datefrom=2016-11-01&dateto=2017-08-31&items=all&export=0' class="nav-top-item  no-submenu <? if($_SERVER[SCRIPT_NAME] == '/passengers.php') echo "current";?>" class=''>Utasok ellenőrzése</a></li>
		
		<?
		}
		if($CURUSER[outlet] == 1 && $CURUSER[userclass] >= 90)  { ?>
		<li><a href='#' class="nav-top-item">Szállás-Outlet &darr;</a>
			<ul>
				<li><a href="/partnercampaigns.php">Kampányok</a></li>
				<li><a href="/templates.php">Template-ek kezelése</a></li>
				<li><a href="/getOffers.php">Ajánlatok kezelése</a></li>
				<li><a href="/promotion.php">Promóciós kategóriák</a></li>
				<li><a href="/images.php">Logók és képek</a></li>
				<li><a href="/outlet_check.php">Outlet ajánlatok listázása</a></li>
				<li><a href="/questions.php"><b>Kérdések</b></a></li>
				<li><a href='/newsletter.php'>Hírlevelek</a></li>
				<!--<li><a href="/outlet_images.php" rel='facebox'><b>Képek frissítése</b></a></li>-->
				<li><a href="/getOffers_extra.php">Extra felárak</a></li>

				<li><a href="/placestats.php">Helyszíni fizetések</a></li>

				<li><a href="/reseller-stats.php">Viszonteladói statisztika</a></li>
				<li><a href="/barterstats.php">Eladási statisztika</a></li>
				<li><a href="/campaign-stats.php">Kampány statisztika</a></li>



				<li><a href="/newsletters.php"><b>Hírlevelek</b></a></li>
				<li><a href="/newsletters-reseller.php"><b>Viszonteladói hírlevél</b></a></li>
				<li><a href="/newsletters_indulhatunk.php"><b>Indulhatunk hírlevél</b></a></li>


			</ul>
		</li>
		
		
		
		<? }
		if($CURUSER[own] == 1 && $CURUSER[userclass] >= 90)  { ?>
					<li><a href='/own_vouchers.php'  class="nav-top-item  no-submenu <? if($_SERVER[SCRIPT_NAME] == '/own_vouchers.php') echo "current";?>">Saját eladás</a>
			
				</li>
		<? } ?>
		
		
		<? 
		if($CURUSER[username] == 'zleindler')  { ?>
			<li><a href="/list_invoice.php" class="<? if($_SERVER[SCRIPT_NAME] == '/list_invoice.php') echo "current";?>"><font color='red'>Műveletek számlákon</font></a></li>

		<? } ?>		
		
		<? if($CURUSER[accounting] == 1) { ?>
			<li><a href='#'  class="nav-top-item <? if($_SERVER[SCRIPT_NAME] == '/weekly-fast.php' || $_SERVER[SCRIPT_NAME] == '/invoice.php' || $_SERVER[SCRIPT_NAME] == '/lmb-invoice.php' || $_SERVER[SCRIPT_NAME] == '/list_invoice.php') echo "current";?>">Pénzügyek  &darr;</a>
			<ul>
	
			<li><a href="/lmb_invoice.php">LMB pénzügyek</a></li>
			<li><a href="/weekly-transfer.php">Heti utalás</a></li>
			<?
			if($CURUSER["userclass"] == 255)  {
			?>
				<!--<li><a href="/lmb_fivedays.php"  class='adminfeature'>LMB értesítők (minden hónap 5.-ig)</a></li>
				<li><a href="/invoice/create_invoice_lmb.php" class='adminfeature'>LMB számlázás (minden hónap 10-én)</a></li>
				<li><a href="/weekly.php">Heti számlák</a></li>-->
				<li><a href="/weekly-fast.php"  class="<? if($_SERVER[SCRIPT_NAME] == '/weekly-fast.php') echo "current";?>">Heti VTL számlák</a></li>
				<li><a href="/partnerstats.php" class="<? if($_SERVER[SCRIPT_NAME] == '/partnerstats.php') echo "current";?>">Eladási statisztika</a></li>
			<? }?>
			<li><a href="/invoice.php" class="<? if($_SERVER[SCRIPT_NAME] == '/invoice.php') echo "current";?>">VTL számlák kezelése</a></li>
			<!--<li><a href="/accounts.php">Partneregyenleg</a></li>-->			
			<?
			if($CURUSER["userclass"] == 255 || $CURUSER["userclass"] > 100 || $CURUSER[accounting] == 1)  {
			?>
				<!--<li><a href="/statistics.php">Kimutatások</a></li>-->
			
			<li><a href="/bankdispatch.php" class="<? if($_SERVER[SCRIPT_NAME] == '/bankdispatch.php') echo "current";?>">Bank importálás</a></li>
			<li><a href="/list_invoice.php" class="<? if($_SERVER[SCRIPT_NAME] == '/list_invoice.php') echo "current";?>"><font color='red'>Műveletek számlákon</font></a></li>

			<!--<li><a href="charts.php">&raquo; Grafikonok</a></li>-->
			<? }?>
			</ul>
		</li>

		<? }
	
		 if( $CURUSER[userclass]>= 40) { ?>
		 	<li><a href='https://sites.google.com/a/indulhatunk.hu/intranet/'  class="nav-top-item no-submenu" target='_blank'>Tudásbázis</a>
			<li><a href='#'  class="nav-top-item">Egyéb  &darr;</a>
				<ul>
					<li><a href="/users.php" >Telefonkönyv</a></li>
					<li><a href="/ticket.php">Feladatok</a></li>
					<?
						if($CURUSER["userclass"] == 255)  {
					?>
					<li><a href="/information.php">Főoldali hírek kezelése</a></li>
					<li><a href="/news.php">Külföldi hírek kezelése</a></li>
					<li><a href="/domains.php">Domainek</a></li>
					<li><a href="/analytics.php">Analitika</a></li>
					<li><a href="/outlet_stats.php">Napi outlet stat</a></li>
					<li><a href="/partnerstats.php">Értékesítési stat</a></li>
					<li><a href="http://manage.indulhatunk.info"  target='_blank'>E-mail fiókok</a></li>
					<? } ?>
					<li><a href="/log.php">Log</a></li>
				</ul></li>
				
			<li><a href='https://drive.google.com/folderview?id=0B-1NHUaiMl90fjE0Tzg5VFBYT1ctc2ZwMUJJOE5xNDhveEZzWkpXemE2ajBLdW9SdXV4Qlk&usp=sharing'  class="nav-top-item no-submenu" target='_blank'>Nyomdai anyagok</a>
			<li><a href='http://mail.indulhatunk.hu'  class="nav-top-item no-submenu" target='_blank'>Webmail</a>

		<? } ?>
		</ul>
		<div class='chlang'>		
		<form method='get' action='/chlang.php'>
			<select name='language' onchange="submit()">
				<option value='0'>select language</option>
				<option value='hu'>Hungarian</option>
				<option value='en'>English</option>
				<option value='ro'>Romanian</option>
				<option value='sk'>Slovak</option>

			</select>
		</form>
		</div>
		 <!-- -->
		 <? } ?>
	
			</div>
</div>

<div id='content'>
<div class='wrapper'>

<h2 class='non-print'><?=$title?></h2>

<? } //nomenu end?>


<? if($CURUSER[coredb_id] > 0 && $CURUSER[pid] == 935) {
?>

<ul id="mainNav" class="fiveStep">
		<li class="<? if($_SERVER[SCRIPT_NAME] == '/gallery.php' || $_SERVER[SCRIPT_NAME] == '/rooms.php' || $_SERVER[SCRIPT_NAME] == '/editoffers.php' ) echo "done"; else echo "current"; ?>"><a href='/settings.php?showdetails=1'><em>1. lépés: alapadatok</em><span>Töltse ki az alapvető információkat!</span></a></li>
		<li class="<? if($_SERVER[SCRIPT_NAME] == '/rooms.php' || $_SERVER[SCRIPT_NAME] == '/editoffers.php') echo "done"; elseif($_SERVER[SCRIPT_NAME] == '/gallery.php') echo "current";?>"><a href="/gallery.php"><em>2. lépés: Képgaléria</em><span>Töltsön fel néhány képet!</span></a></li>
		<li class="<? if($_SERVER[SCRIPT_NAME] == '/rooms.php') echo "current"; elseif($_SERVER[SCRIPT_NAME] == '/editoffers.php') echo "done";?>"><a href="/rooms.php"><em>3. lépés: Szobatípusok</em><span>Milyen szobatípusokkal rendelkezik?</span></a></li>
		<li class="mainNavNoBg <? if($_SERVER[SCRIPT_NAME] == '/editoffers.php') echo "current";?>"><a href="/editoffers.php"><em>4. lépés: Ajánlatok</em> <span>Készítse el az ajánlatait!</span></a></li>
	</ul>
<div class='cleaner'></div>
<?
} ?>
<?if($CURUSER){?>
<?

if($CURUSER[userclass] > 50)
{
	$emailstotal = mysql_fetch_assoc($mysql->query("SELECT count(id) as cnt FROM emails WHERE paired = 0 AND inactive = 0"));
	
	if($emailstotal[cnt] > 0)
		echo "<div class='redmessage'><a href='emails.php'>$emailstotal[cnt] darab feldolgozatlan VTL megrendelés található a rendszerben! Ellenőrizd &raquo;</a></div>";
}
if($CURUSER[vtl_due] == 1)
	echo "<div class='redmessage'><a href='invoices.php'>".$lang['invoicerequest']."</a></div>";

if($CURUSER[lmb_due] == 1)
	echo "<div class='redmessage'><a href='/invoices.php?type=lmb'>".$lang['lmbinvoice']."</a></div>";

if($CURUSER[password] == 'tamas')
	echo "<div class='redmessage'><a href='/settings.php'>".$lang['easypassword']."</a></div>";
 }
 
if($CURUSER[userclass] == 0 && $CURUSER[username] <> '')
{
?>
<a href='/fullhouse.php'>
<div class='calendarmessage' style=' color:black;'>

		<div style='font-size:20px;font-weight:bold;margin:0 0 5px 0;text-align:center;border-bottom:1px solid #df8f8f;padding:0 0 5px 0;'><?=$lang['fullhouserequest']?></div>


	<div style='float:left'><img src='/images/calendar.png'/></div>
	<div style='float:left; margin:5px 0 0 10px;width:740px;font-weight:normal;'>
	
	<div>
		<?=$lang['fullhousenotification']?>
		

	</div>
	
	</div>
	
	<div class='cleaner'></div>
</div>
</a>

<?

}


}
function foot() {

global $lang;
global $tmpstamp;
global $CURUSER;
?>
<div class="cleaner"></div>
<div class='cpright'> 
All rights reserved ©<?=date("Y")?> - <a href='http://indulhatunk.info'>A jövő kulcsa: indulhatunk.info</a> | <a href='#'>Fel</a> 
<? if($CURUSER[userclass] == 255)
{
	echo "(".(microtime(true) - $tmpstamp)."s)";
}
?>
</div>

</div>
</div>
<div class='cleaner'></div>
</div>	

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1346485-1', 'auto');
  ga('send', 'pageview');
</script>
<script type="text/javascript">
(function(){
var s = document.createElement('script');
var x = document.getElementsByTagName('script')[0];
s.type = 'text/javascript';
s.async = true;
s.src = ('https:'==document.location.protocol?'https://':'http://')
+ 'eu-sonar.sociomantic.com/js/2010-07-01/adpan/indulhatunk-hu';
x.parentNode.insertBefore( s, x );
})();
</script>

</body>
</html>
<?
}

//formatPrice($pplus,0,1,$arr[currency],1)
function formatPrice($price,$noformat = 0,$nbsp = 0, $currency = 'Ft',$dash = 0,$donotexchange = 0)
{	

	if($currency == 'RON' && $donotexchange == 1)
	{
		global $exrate;
		$price = round($price/$exrate);
		
	}
	if($currency == 'RON' && $donotexchange == 0)
	{
		global $exrate;
		$price = round($price/$exrate);
		
	}
	if($currency == 'EUR' && $donotexchange == 1)
	{
		global $exrateeur;
		$price = round($price/$exrateeur);
		
	}
	if($dash == 1 && $price == 0)
	{
		$price = '-';
	}
	else
	{
		if($noformat == 1)
			$price =  number_format($price, 0, ',', ' '). "";
		elseif($noformat == 4)
			$price =  number_format($price, 0, ',', ''). "";

		elseif($noformat == 2)
			$price =  number_format($price, 0, ',', '.'). " $currency";
		elseif($noformat == 3)
			$price =  number_format($price, 0, ',', ''). " $currency";
		else
			$price =  number_format($price, 0, ',', ' '). " $currency";
			
		if($nbsp == 1)
			$price = str_replace(" ","&nbsp;",$price);
	}
	return $price;
}


function formatPrice_foreign_invoice($price,$noformat = 0,$nbsp = 0, $currency = 'Ft',$dash = 0,$donotexchange = 0)
{	

	if($currency == 'RON' && $donotexchange == 0)
	{
		global $exrate;
		$price = round($price/$exrate);
		
	}
	elseif($currency == 'RON' && $donotexchange > 0)
	{
		global $exrate;
		$price = round($price);
		
	}
	if($currency == 'EUR' && $donotexchange > 1)
	{
		global $exrateeur;
		$price = round($price);
		
	}
	if($currency == 'EUR' && $donotexchange == 1)
	{
		global $exrateeur;
		$price = round($price/$exrateeur);
		
	}
	if($dash == 1 && $price == 0)
	{
		$price = '-';
	}
	else
	{
		if($noformat == 1)
			$price =  number_format($price, 0, ',', ' '). "";
		elseif($noformat == 4)
			$price =  number_format($price, 0, ',', ''). "";

		elseif($noformat == 2)
			$price =  number_format($price, 0, ',', '.'). " $currency";
		elseif($noformat == 3)
			$price =  number_format($price, 0, ',', ''). " $currency";
		else
			$price =  number_format($price, 0, ',', ' '). " $currency";
			
		if($nbsp == 1)
			$price = str_replace(" ","&nbsp;",$price);
	}
	return $price;
}
function validatePost($array) 
{
	foreach($array as $key => $data)
	{
		if(validString($data) == false)
		{
			echo message("ERROR");
			
			foot();
			die;
		}		
	}
	return true;
}
function validateForm($url)
{
	if ($url == "")
	  return false;

	// The following characters are allowed in user names
	$allowedchars = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyzAÁBCDEÉFGHJIÍKLMNOÓÖŐPQRSTUÚÜŰVWXYZ012345678@()|9+-_/%.=? "; 

	$allowedchars = array("a","á","b","c","d","e","é","f","g","h","i","í","j","k","l","m","n","o","ó","ö","ő","p","q","r","s","t","u","ú","ü","ű","v","w","x","y","z","A","Á","B","C","D","E","É","F","G","H","J","I","Í","K","L","M","N","O","Ó","Ö","Ő","P","Q","R","S","T","U","Ú","Ü","Ű","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","@","(",")","|","9","+","-","_","/","%",".","=","?"," ",""); 
	

	for ($i = 0; $i < strlen($url); ++$i)
	{
		$uri = (string)$url[$i];
		echo $uri."<hr/>";
	  if (!in_array($uri,$allowedchars) == true)
	    return false;
	}
	return true;
}
function validString($url)
{
	if ($url == "")
	  return true;

	// The following characters are allowed in user names
	$allowedchars = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyzAÁBCDEÉFGHJIÍKLMNOÓÖŐPQRSTUÚÜŰVWXYZ012345678@()&|9+-_/%.=?:<> "; 

	for ($i = 0; $i < strlen($url); ++$i)
	  if (strpos($allowedchars, $url[$i]) === false)
	  {
		//	echo $url[$i];
		    return false;
	 }
	return true;
}
function validurl($url)
{
	if ($url == "")
	  return false;

	// The following characters are allowed in user names
	$allowedchars = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyzAÁBCDEÉFGHJIÍKLMNOÓÖŐPQRSTUÚÜŰVWXYZ012345678@()|9+-_/%.=? "; 

	for ($i = 0; $i < strlen($url); ++$i)
	  if (strpos($allowedchars, $url[$i]) === false)
	    return false;

	return true;
}

function generateVoucher($string) {
	global $mysql;
	

	
	$monthArray = array(
		1 => 'JANUÁR',
		2 => 'FEBRUÁR',
		3 => 'MÁRCIUS',
		4 => 'ÁPRILIS',
		5 => 'MÁJUS',
		6 => 'JÚNIUS',
		7 => 'JÚLIUS',
		8 => 'AUGUSZTUS',
		9 => 'SZEPTEMBER',
		10 => 'OKTÓBER',
		11 => 'NOVEMBER',
		12 => 'DECEMBER',
	);
	$year = date("Y");
	$day = date("d");
	$validity = str_replace("|","<br/>",$voucherArr[validity]);
	$not_include = str_replace("|","<br/>",$voucherArr[not_include]);;
?>
<div id="container">
<? for($i=1;$i<=3;$i++){ ?>
	<!-- voucher start -->
	<div class="voucher">
		<div class="number">Nr&deg; <?=$string?>-<?=$i?><br/><span class="until">ÉRV.: 2011. szeptember 21-ig</span></div>
		<div class="address"><b>Belga söröző</b> 1011 Budapest, Bem rakpart 12.</div>
		<div class="value">
			<div class="price">5000 Ft</div>
			ÉRTÉKŰ UTALVÁNY
		</div>
		<div class="valid">BEVÁLTHATÓ</div>
		<div class="mainAddress"><b>Belgian Brasserie Henri</b><br/>
			1011 Budapest, Bem rakpart 12.,  www.belgasorozo.com
		</div>
	
	<div class="moreInfoTitle">BEVÁLTHATÓ TOVÁBBÁ:</div>
	<div class="items">
		<div class="voucherItem">
			Pater Marcus Belga Apátsági<br/>
			Söröző és Étterem<br/>
			<span class="voucherAddress">1011 Budapest, Apor Péter u. 1.<br/>
			www.patermarcus.hu</span>
		</div>
		<div class="voucherItem">
			Belga Sörök Háza<br/>
			<span class="voucherAddress">1011 Budapest, Halász utca 2.<br/>
			www.belgasorokhaza.hu</span>
		</div>
		<div class="voucherItem">
			Horgásztanya Vendéglő<br/>
			<span class="voucherAddress">1011 Budapest, Fő utca 27.<br/>
			www.horgasztanyavendeglo.hu</span>
		</div>
		<div class="voucherItem">
			Dunaperti Matróz Kocsma<br/>
			<span class="voucherAddress">1011 Budapest, Halász u. 1.<br/>
			www.matrozkocsma.hu</span>
		</div>
		<div class="cleaner"></div>
	</div>		

	</div>
	<!-- voucher end -->
<?}?>

</div>
<pagebreak/>
<?
}
function generateVoucherDarshan($string) {
	global $mysql;
	

	
	$monthArray = array(
		1 => 'JANUÁR',
		2 => 'FEBRUÁR',
		3 => 'MÁRCIUS',
		4 => 'ÁPRILIS',
		5 => 'MÁJUS',
		6 => 'JÚNIUS',
		7 => 'JÚLIUS',
		8 => 'AUGUSZTUS',
		9 => 'SZEPTEMBER',
		10 => 'OKTÓBER',
		11 => 'NOVEMBER',
		12 => 'DECEMBER',
	);
	$year = date("Y");
	$day = date("d");
	$validity = str_replace("|","<br/>",$voucherArr[validity]);
	$not_include = str_replace("|","<br/>",$voucherArr[not_include]);;
?>
<div id="container">
<? for($i=1;$i<=3;$i++){ ?>
	<!-- voucher start -->
	<div class="voucher">
		<div class="number">Nr&deg; <?=$string?>-<?=$i?><br/><span class="until">ÉRV.: 2011. szeptember 21-ig</span></div>
		<div class="address"><b>Darshan Udvar</b> 1088 Budapest Krúdy Gyula u. 7.</div>
		<div class="value">
			<div class="price">5000 Ft</div>
			ÉRTÉKŰ UTALVÁNY
		</div>
		<div class="valid">BEVÁLTHATÓ</div>
		<div class="mainAddress"><b>Darshan Udvar</b><br/>
			1088 Budapest Krúdy Gyula u. 7.,  Asztalfoglalás: +36 1 266-554, 11h-tól
		</div>
	<!--
	<div class="moreInfoTitle">BEVÁLTHATÓ TOVÁBBÁ:</div>
	<div class="items">
		<div class="voucherItem">
			Pater Marcus Belga Apátsági<br/>
			Söröző és Étterem<br/>
			<span class="voucherAddress">1011 Budapest, Apor Péter u. 1.<br/>
			www.patermarcus.hu</span>
		</div>
		<div class="voucherItem">
			Belga Sörök Háza<br/>
			<span class="voucherAddress">1011 Budapest, Halász utca 2.<br/>
			www.belgasorokhaza.hu</span>
		</div>
		<div class="voucherItem">
			Horgásztanya Vendéglő<br/>
			<span class="voucherAddress">1011 Budapest, Fő utca 27.<br/>
			www.horgasztanyavendeglo.hu</span>
		</div>
		<div class="voucherItem">
			Dunaperti Matróz Kocsma<br/>
			<span class="voucherAddress">1011 Budapest, Halász u. 1.<br/>
			www.matrozkocsma.hu</span>
		</div>
		<div class="cleaner"></div>
	</div>		
	-->
	</div>

	<!-- voucher end -->
<?}?>

</div>
<pagebreak/>
<?
}
/*
function generatePdfV2($id=1,$reseller = 0) {
	global $mysql;
	
	$voucherData = $mysql->query("SELECT customers.company_invoice,customers.payment,customers.invoice_name,customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE customers.cid = $id LIMIT 1");
	$voucherArr = mysql_fetch_assoc($voucherData);
	
	if($voucherArr[sub_pid] > 0)
	{
		$voucherData = $mysql->query("SELECT customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.sub_pid WHERE customers.cid = $id LIMIT 1");
		$voucherArr = mysql_fetch_assoc($voucherData);
	
	}
	$offerData = $mysql->query("SELECT * FROM offers  WHERE id = $voucherArr[offers_id] LIMIT 1");
	$offerArr = mysql_fetch_assoc($offerData);
	$month = date("n");
	
	
	
	//https://admin.indulhatunk.info/vouchers/partners2/<?=$voucherArr[coredb_id].jpg
	
	if($voucherArr[sub_pid] > 0)
	{
	
		$logopic = $voucherArr[pid];
	}
	else
	{
		$logopic = $voucherArr[coredb_id];
	}
	if(!file_exists("/var/www/lighttpd/admin.indulhatunk.info/vouchers/partners2/$logopic.jpg"))
	{
		$image = 'https://admin.indulhatunk.info/vouchers/partners2/nopic.jpg';
	}
	else
		$image = "https://admin.indulhatunk.info/vouchers/partners2/$logopic.jpg";
		//die('Az utalvány nem tölthető jelenleg. Kérjük értesítsen minket a problémáról a vtl@indulhatunk.hu e-mail címen. Köszönjük!');
	
	if($offerArr[language] == 'en')
	{
		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "Nr.";
		$language[price] = "PRICE";
		
		$language[name] = "en_name";
		$language[shortname] = "en_shortname";
		$language[plus_room1_name] = "en_plus_room1_name";
		$language[plus_room2_name] = "en_plus_room2_name";
		$language[plus_room3_name] = "en_plus_room3_name";
		$language[plus_other1_name] = "en_plus_other1_name";
		$language[plus_other2_name] = "en_plus_other2_name";
		$language[plus_other3_name] = "en_plus_other3_name";
		$language[plus_other4_name] = "en_plus_other4_name";
		$language[plus_other5_name] = "en_plus_other5_name";
		$language[plus_other6_name] = "en_plus_other6_name";
		$language[plus_validity] = "en_validity";
		$language[plus_not_include] = "en_not_include";

		$language[validity] = "en_validity";
		$language[not_include] = "en_not_include";
		$language[services] = "Paid extra services";
		$language[child] = "child";
		$language[booking] = "Booking";
		$language[booking_text] = "Please refer to the Nr. of the voucher, and do not forget to present voucher at the reception at the time of the check-in.";
		$language[validity] = "Validity";
		$language[not_include] = "Other extras to be paid at the hotel reception";
		
		$language[plus_weekend] = "Weekend extra price";
		$language[plus_extra_bed] = "Extra bed extra price";
		$language[night] = "night";
		$language[plus_one_night] ="+1 night stay additional price";
		$language[package] = "package";
	}
	else
	{
		$language = array();
		$language[voucher] = 'Ajándékutalvány';
		$language[id] = "SORSZÁM";
		$language[price] = "ÁRA";
		
		$language[name] = "name";
		$language[shortname] = "shortname";
		$language[plus_room1_name] = "plus_room1_name";
		$language[plus_room2_name] = "plus_room2_name";
		$language[plus_room3_name] = "plus_room3_name";
		$language[plus_other1_name] = "plus_other1_name";
		$language[plus_other2_name] = "plus_other2_name";
		$language[plus_other3_name] = "plus_other3_name";
		$language[plus_other4_name] = "plus_other4_name";
		$language[plus_other5_name] = "plus_other5_name";
		$language[plus_other6_name] = "plus_other6_name";
		$language[plus_validity] = "validity";
		$language[plus_not_include] = "not_include";
		$language[package] = "csomag";
		
		$language[validity] = "validity";
		$language[not_include] = "not_include";
		$language[services] = "Az ajándékutalvány a következő plusz szolgáltatásokra jogosítja";
		$language[child] = "gyermek";
		$language[booking] = "Foglalás";
		$language[booking_text] = "Kérjük, hivatkozzon a lapon található sorszámra, illetve, az utalványt szíveskedjék magával hozni és érkezéskor a szálloda recepcióján leadni.";
		$language[validity] = "Érvényesség";
		$language[not_include] = "Külön fizetendő";
		$language[plus_weekend] = "Hétvégi felár";
		$language[plus_extra_bed] = "Pótágy felár";
		$language[night] = "éj";
		$language[plus_one_night] ="Maradjon még egy éjszakát";
	}
	
	$monthArray = array(
		1 => 'JANUÁR',
		2 => 'FEBRUÁR',
		3 => 'MÁRCIUS',
		4 => 'ÁPRILIS',
		5 => 'MÁJUS',
		6 => 'JÚNIUS',
		7 => 'JÚLIUS',
		8 => 'AUGUSZTUS',
		9 => 'SZEPTEMBER',
		10 => 'OKTÓBER',
		11 => 'NOVEMBER',
		12 => 'DECEMBER',
	);
	$year = date("Y");
	$day = date("d");
	$validity = str_replace("|","",$offerArr[$language[plus_validity]]);
	$not_include = str_replace("|","",$offerArr[$language[plus_not_include]]);;
?>
<div id="container">
	<div id="header">
		<div id="voucher"><?=$language[voucher]?></div>
		<div id="descr">
			<div id="date"><?=$year;?>. <?=$monthArray[$month];?> <?=$day;?>.</div>
			<div id="serial"><?=$language[id]?>: N&deg;<?=$voucherArr[offer_id];?></div>
			<div id="price"><?if($voucherArr[gift] == 0) {?> <?=$language[price]?>: <span class="bigPrice"><?=formatPrice($voucherArr[orig_price],1);?></span> Ft <?}?></div>		
			<div class="cleaner"></div>
		</div>
	</div>
	<div id="title">
		<?=str_replace("|","",$voucherArr[hotel_name]);?>	
		<div id="voucherImageAddress"><?=$voucherArr[zip];?> <?=$voucherArr[city];?> <?=$voucherArr[address];?> tel.: <?=$voucherArr[reception_phone];?>/<?=$voucherArr[reception_email];?> </div>
	
	</div>
	<div id="subTitle">
		<div class="bid_title"><?=$offerArr[$language[shortname]]?>	</div>
		<?=$offerArr[$language[name]]?>	
	</div>
	<div id="voucherContent">
	<div id="info">
		<div id="infoLeft">
		
		<b><?=$language[services]?>:</b>
		
		<ul class="plusService">
		<!-- offer properties -->
		<?if($offerArr[plus_half_board]<>0) {
		
			$class ='';
			if($voucherArr[plus_half_board] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"f>Félpanzió <?=formatPrice($offerArr[plus_half_board])?>/<?=$language[night]?></li>
		<?}?>

		<?if($offerArr[plus_weekend_plus]<>0) {
		
			$class ='';
			if($voucherArr[plus_weekend_plus] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[plus_weekend]?> <?=formatPrice($offerArr[plus_weekend_plus]*2)?></li>
		<?}?>

		<?if($offerArr[plus_bed]<>0) {
		
		$class ='';
			if($voucherArr[plus_bed] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[plus_extra_bed]?> <?=formatPrice($offerArr[plus_bed])?>/<?=$language[night]?></li>
		<?}?>

		<?if($offerArr[plus_bed_plus_food]<>0) {
		
		$class ='';
			if($voucherArr[plus_bed_plus_food] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>">Félpanzió + pótágy felár <?=formatPrice($offerArr[plus_bed_plus_food])?>/<?=$language[night]?></li>
		<?}?>

<?if($voucherArr[plus_child1_value]<>0) {?>
<li class="bold">1. <?=$language[child]?>
	<?
	if($voucherArr[plus_child1_value]==1 || ($voucherArr[plus_child1_value] >1 && $voucherArr[plus_child1_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig: ".formatPrice($offerArr[plus_child1_value])."/$language[night]";
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között: ".formatPrice($offerArr[plus_child2_value])."/$language[night]";
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett: ".formatPrice($offerArr[plus_child3_value])."/$language[night]";
	?>
</li>
<?}?>

<?if($voucherArr[plus_child2_value]<>0) {?>
<li class="bold">2. <?=$language[child]?>
	<?
	if($voucherArr[plus_child2_value]==1 || ($voucherArr[plus_child2_value] >1 && $voucherArr[plus_child2_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig: ".formatPrice($offerArr[plus_child1_value])."/$language[night]";
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között: ".formatPrice($offerArr[plus_child2_value])."/$language[night]";
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett: ".formatPrice($offerArr[plus_child3_value])."/$language[night]";
	?>
</li>
<?}?>

<?if($voucherArr[plus_child3_value]<>0) {?>
<li class="bold">3. <?=$language[child]?>
	<?
	if($voucherArr[plus_child3_value]==1  || ($voucherArr[plus_child3_value] >1 && $voucherArr[plus_child3_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig: ".formatPrice($offerArr[plus_child1_value])."/$language[night]";
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között: ".formatPrice($offerArr[plus_child2_value])."/$language[night]";
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett: ".formatPrice($offerArr[plus_child3_value])."/$language[night]";
	?>	
</li>
<?}?>

	<?if($offerArr[plus_room1_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_room1_name]?> szoba felár <?=formatPrice($offerArr[plus_room1_value])?>/<?=$language[night]?></li>
	<?}?>

	<?if($offerArr[plus_room2_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_room2_name]?> szoba felár <?=formatPrice($offerArr[plus_room2_value])?>/<?=$language[night]?></li>
	<?}?>

	<?if($offerArr[plus_room3_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_room3_name]?> szoba felár <?=formatPrice($offerArr[plus_room3_value])?>/<?=$language[night]?></li>
	<?}?>

	<?if($offerArr[plus_other1_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_other1_name]?> felár <?=formatPrice($offerArr[plus_other1_value])?>/<?=$language[night]?></li>
	<?}?>

	<?if($offerArr[plus_other2_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_other2_name]?> felár <?=formatPrice($offerArr[plus_other2_value])?>/<?=$language[night]?></li>
	<?}?>

	<?if($offerArr[plus_other3_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_other3_name]?> felár <?=formatPrice($offerArr[plus_other3_value])?>/<?=$language[night]?></li>
	<?}?>
	

	<?if($offerArr[plus_other4_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other4_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_other4_name]?> felár <?=formatPrice($offerArr[plus_other4_value])?>/<?=$language[night]?></li>
	<?}?>
	
	<?if($offerArr[plus_other5_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other5_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_other5_name]?> felár <?=formatPrice($offerArr[plus_other5_value])?>/<?=$language[night]?></li>
	<?}?>
	
	
	<?
	if($offerArr[plus_other6_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other6_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_other6_name]?> felár <?=formatPrice($offerArr[plus_other6_value])?>/<?=$language[night]?></li>
	<?
	}
	?>
	<?
	if($offerArr[plus_single1_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_single1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_single1_name]?> felár <?=formatPrice($offerArr[plus_single1_value])?>/<?=$language[package]?></li>
	<?}?>
	<?
	if($offerArr[plus_single2_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_single2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_single2_name]?> felár <?=formatPrice($offerArr[plus_single2_value])?>/<?=$language[package]?></li>
	<?}?>
	<?
	if($offerArr[plus_single3_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_single3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[plus_single3_name]?> felár <?=formatPrice($offerArr[plus_single3_value])?>/<?=$language[package]?></li>
	<?}?>
	
	
	<?if( $voucherArr[plus_days]>0) {
	
	
	$class ='';
	if($voucherArr[plus_days] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
	<li class="<?=$class?>"><?=$language[plus_one_night]?> <?=formatPrice($voucherArr[plus_days])?></li>
<?}?>
</ul>
		<!-- offer properties end -->
		</div>
		<div id="infoRight"><div id="subT"><?=$language[booking]?>:</div>
			<div id="room">
				<?=$language[booking_text]?>
			</div>
	<div id="st">
		<?=$language[validity]?>:
	</div>
	<div id="validity">
		<?=$validity?>
	</div>
	
	<div id="notIncludeTitle">
		<?=$language[not_include]?>
	</div>
	<div id="notInclude">
		<?=$not_include?>
	</div>


		</div>
		<div class="cleaner"></div>
	</div>
	
	<div id="voucherImage"><img src="<?=$image?>"/></div>
	
		<div id="footer">
		AZ UTALVÁNYON SZEREPLŐ PLUSZ KEDVEZMÉNYES SZOLGÁLTATÁSOK ÁRAI CSAK A KIÁLLÍTÁSKOR ÉRVÉNYESEK!<br/><br/>
		AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ
	<div class='notice'>
		A Hotel Outlet Kft. a jelen voucheren feltüntetett szálloda bizományosi értékesítője, <br/>így kizárja minden felelősségét a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.<br/>
		A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag <br/>és közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti. <br/>
		A bizományos felelőssége kizárólag a voucher ellenértékének a szálloda részére történő befizetésére terjed ki. <br/>
		A szállodai szolgáltatási jogviszony a voucheren feltüntetett szálloda és a voucher tulajdonosa között jön létre.
	</div>
	</div>
	</div>
	<div id="outlet">
	<? if($reseller == 0) { ?>
		www.szállásoutlet.hu
	<? } ?>
	</div>
	<div id="copyright">printed by indulhatunk.info &copy;<?=date("Y")?> - All rights reserved. (#<?=$voucherArr[cid]?>)</div>
</div>

<?
}
*/
/* depicted

function generatePdfV2($id=1,$reseller = 0) {
	global $mysql;
	global $lang;
	
	$voucherData = $mysql->query("SELECT customers.reseller_id,customers.type,customers.company_invoice,customers.payment,customers.invoice_name,customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE customers.cid = $id LIMIT 1");
	$voucherArr = mysql_fetch_assoc($voucherData);
	
	if($voucherArr[sub_pid] > 0)
	{
		$voucherData = $mysql->query("SELECT customers.reseller_id,customers.type,customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.sub_pid WHERE customers.cid = $id LIMIT 1");
		$voucherArr = mysql_fetch_assoc($voucherData);
	
	}
	$offerData = $mysql->query("SELECT * FROM offers  WHERE id = $voucherArr[offers_id] LIMIT 1");
	$offerArr = mysql_fetch_assoc($offerData);
	$month = date("n");
	
	
	if($voucherArr[company_invoice] == 'szallasoutlet')
		$company = 'SzállásOutlet Kft.';
	else
		$company = 'Hotel Outlet Kft.';
	
	//https://admin.indulhatunk.info/vouchers/partners2/<?=$voucherArr[coredb_id].jpg
	
	if($voucherArr[sub_pid] > 0)
	{
	
		$logopic = $voucherArr[sub_pid];
	}
	else
	{
		$logopic = $voucherArr[coredb_id];
	}
	if(!file_exists("/var/www/lighttpd/admin.indulhatunk.info/vouchers/partners2/$logopic.jpg"))
	{
		$image = 'http://admin.indulhatunk.info/vouchers/partners2/nopic.jpg';
	}
	else
		$image = "http://admin.indulhatunk.info/vouchers/partners2/$logopic.jpg";
		//die('Az utalvány nem tölthető jelenleg. Kérjük értesítsen minket a problémáról a vtl@indulhatunk.hu e-mail címen. Köszönjük!');

	if($offerArr[language] == 'en')
	{
		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "Nr.";
		$language[price] = "PRICE";
		
		$language[name] = "en_name";
		$language[shortname] = "en_shortname";
		$language[plus_room1_name] = "en_plus_room1_name";
		$language[plus_room2_name] = "en_plus_room2_name";
		$language[plus_room3_name] = "en_plus_room3_name";
		$language[plus_other1_name] = "en_plus_other1_name";
		$language[plus_other2_name] = "en_plus_other2_name";
		$language[plus_other3_name] = "en_plus_other3_name";
		$language[plus_other4_name] = "en_plus_other4_name";
		$language[plus_other5_name] = "en_plus_other5_name";
		$language[plus_other6_name] = "en_plus_other6_name";
		
		$language[currency] = 'Ft';

		$language[plus_single1_name] = "en_plus_single1_name";
		$language[plus_single2_name] = "en_plus_single2_name";
		$language[plus_single3_name] = "en_plus_single3_name";


		$language[plus_validity] = "en_validity";
		$language[plus_not_include] = "en_not_include";

		$language[validity] = "en_validity";
		$language[not_include] = "en_not_include";
		$language[services] = "Paid extra services";
		$language[child] = "child";
		$language[booking] = "Booking";
		$language[booking_text] = "Please refer to the Nr. of the voucher, and do not forget to present voucher at the reception at the time of the check-in.";
		$language[validity] = "Validity";
		$language[not_include] = "Other extras to be paid at the hotel reception";
		
		$language[plus_weekend] = "Weekend extra price";
		$language[plus_extra_bed] = "Extra bed extra price";
		$language[night] = "night";
		$language[plus_one_night] ="+1 night stay additional price";
		$language[package] = "package";
		$language[halfboard] = 'halfboard';
		
		if(substr($voucherArr[offer_id],0,3) == 'CZO')
		{

				$language[notification] = 'PREŢURILE EXTRA SERVICIILOR CU REDUCERE, CARE FIGUREAZĂ PE VOUCHER SUNT VALABILE NUMAI LA EMITERE!<br/><br/>
		TICHETUL NU POATE FI SCHIMBAT PE BANI';
		
		$language[notification2] = 'S.C. Hotel Outlet Ro S.R.L. este comisionarul care valorifică serviciile hotelului indicat pe voucher, <br/>
		astfel exclude orice răspundere faţă de lipsa din orice motiv a serviciilor hoteliere.
<br/>
		Cumpărătorul voucherului va lua la cunoştinţă că orice pretenţie de despăgubire sau restituire de bani poate fi prezentată exclusiv şi direct hotelului care este indicat pe voucher.<br/>
		Răspunderea comisionarului se extinde exclusiv la vărsarea contravalorii voucherului în contul hotelului.<br/>
		Relaţia juridică de servicii hoteliere se înfiinţează între hotelul indicat pe voucher şi proprietarul voucherului.';

		}
		else
		{
			
			$language[notification] = 'AZ UTALVÁNYON SZEREPLŐ PLUSZ KEDVEZMÉNYES SZOLGÁLTATÁSOK ÁRAI CSAK A KIÁLLÍTÁSKOR ÉRVÉNYESEK!<br/><br/>
					AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ';
					
					$language[notification2] = "A $company. a jelen voucheren feltüntetett szálloda bizományosi értékesítője, <br/>így kizárja minden felelősségét a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.<br/>
					A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag <br/>és közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti. <br/>
					A bizományos felelőssége kizárólag a voucher ellenértékének a szálloda részére történő befizetésére terjed ki. <br/>
					A szállodai szolgáltatási jogviszony a voucheren feltüntetett szálloda és a voucher tulajdonosa között jön létre.";
	
		}

		$language[lang] = 'en';

	}
	elseif(substr($voucherArr[offer_id],0,3) == 'CZO')
	{
		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "Seria";
		$language[price] = "Preț";
		$language[currency] = 'RON';
		$language[name] = "ro_name";
		$language[shortname] = "ro_shortname";
		$language[plus_room1_name] = "ro_plus_room1_name";
		$language[plus_room2_name] = "ro_plus_room2_name";
		$language[plus_room3_name] = "ro_plus_room3_name";
		$language[plus_other1_name] = "ro_plus_other1_name";
		$language[plus_other2_name] = "ro_plus_other2_name";
		$language[plus_other3_name] = "ro_plus_other3_name";
		$language[plus_other4_name] = "ro_plus_other4_name";
		$language[plus_other5_name] = "ro_plus_other5_name";
		$language[plus_other6_name] = "ro_plus_other6_name";
		
			
		$language[plus_single1_name] = "ro_plus_single1_name";
		$language[plus_single2_name] = "ro_plus_single2_name";
		$language[plus_single3_name] = "ro_plus_single3_name";


		$language[plus_validity] = "ro_validity";
		$language[plus_not_include] = "ro_not_include";

		$language[validity] = "ro_validity";
		$language[not_include] = "ro_not_include";
		$language[services] = "Se plătește separat";
		$language[child] = "copii";
		$language[booking] = "Rezervare";
		$language[booking_text] = "Vă rugăm să comunicați seria de pe foaie, și să aduceți voucherul cu Dv. pentru a-l preda la sosire la recepția hotelului.";
		$language[validity] = "Valabilitate";
		$language[not_include] = "Se plătește separat";
		
		$language[plus_weekend] = "Supliment de weekend";
		$language[plus_extra_bed] = "Supliment pat suplimentar";
		$language[night] = "nopate";
		$language[plus_one_night] ="Mai rămâneți o noapte";
		$language[package] = "package";
		$language[halfboard] = 'Demipensiune';
		
			$language[notification] = 'PREŢURILE EXTRA SERVICIILOR CU REDUCERE, CARE FIGUREAZĂ PE VOUCHER SUNT VALABILE NUMAI LA EMITERE!<br/><br/>
		TICHETUL NU POATE FI SCHIMBAT PE BANI';
		
		$language[notification2] = 'S.C. Hotel Outlet Ro S.R.L. este comisionarul care valorifică serviciile hotelului indicat pe voucher, <br/>
		astfel exclude orice răspundere faţă de lipsa din orice motiv a serviciilor hoteliere.
<br/>
		Cumpărătorul voucherului va lua la cunoştinţă că orice pretenţie de despăgubire sau restituire de bani poate fi prezentată exclusiv şi direct hotelului care este indicat pe voucher.<br/>
		Răspunderea comisionarului se extinde exclusiv la vărsarea contravalorii voucherului în contul hotelului.<br/>
		Relaţia juridică de servicii hoteliere se înfiinţează între hotelul indicat pe voucher şi proprietarul voucherului.';

		$language[lang] = 'ro';


	}
		elseif(substr($voucherArr[offer_id],0,3) == 'TVO')
	{
	

		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "S.ČÍSLO";
		$language[price] = "Cena";
		$language[currency] = 'EUR';
		$language[name] = "sk_name";
		$language[shortname] = "sk_shortname";
		$language[plus_room1_name] = "sk_plus_room1_name";
		$language[plus_room2_name] = "sk_plus_room2_name";
		$language[plus_room3_name] = "sk_plus_room3_name";
		$language[plus_other1_name] = "sk_plus_other1_name";
		$language[plus_other2_name] = "sk_plus_other2_name";
		$language[plus_other3_name] = "sk_plus_other3_name";
		$language[plus_other4_name] = "sk_plus_other4_name";
		$language[plus_other5_name] = "sk_plus_other5_name";
		$language[plus_other6_name] = "sk_plus_other6_name";
		
			
		$language[plus_single1_name] = "sk_plus_single1_name";
		$language[plus_single2_name] = "sk_plus_single2_name";
		$language[plus_single3_name] = "sk_plus_single3_name";


		$language[plus_validity] = "sk_validity";
		$language[plus_not_include] = "sk_not_include";

		$language[validity] = "sk_validity";
		$language[not_include] = "sk_not_include";
		$language[services] = "Hotelová poukážka Vás oprávňuje na využitie nasledujúcich extra služieb";
		$language[child] = "Dieťa ";
		$language[booking] = "Rezervácia";
		$language[booking_text] = "Prosím zapamätajte si poradové číslo uvedené na poukážke, alebo si ju zo sebou vezmite a odovzdajte ju na recepcii hotela. ";
		$language[validity] = "Doba platnosti";
		$language[not_include] = "Cena nezahŕňa ";
		
		$language[plus_weekend] = "Príplatok: víkend";
		$language[plus_extra_bed] = "Príplatok: prístelka";
		$language[night] = "noc";
		$language[plus_one_night] ="Zostaňte ešte jednu noc";
		$language[package] = "package";
		$language[halfboard] = 'polpenzia';
		
			$language[notification] = 'CENY EXTRA SLUŽIEB UVEDENÝCH NA POUKÁŽKE SÚ PLATNÉ LEN V ČASE JEJ VYSTAVENIA!<br/><br/>POUKÁŽKU NIE JE MOŽNÉ ZAMENIŤ ZA HOTOVOSŤ. ';
		
		$language[notification2] = 'Travel Outlet s.r.o. je komisionálnym / mediačným predajcom hotela uvedeného na poukážke, <br/>čím sa zrieka zodpovednosti za neposkytnutie služby zo strany hotela z akýchkoľvek dôvodov. <br/>Zákazník, ktorý si zakúpi poukážku, berie na vedomie, že požiadavky súvisiace s vrátením peňazí alebo odškodnením si  môže uplatniť výlučne<br/> a priamo u hotela uvedeného na poukážke. <br/>Zodpovednosť sprostredkovateľa sa vzťahuje len na úhradu protihodnoty poukážky hotelu. <br/> Právny vzťah týkajúci sa služieb poskytovaných hotelom vzniká medzi hotelom uvedeným na poukážke a vlastníkom poukážky. ';

		$language[lang] = 'sk';


	}
	elseif($voucherArr[reseller_id] == 2999)
	{

		$language = array();
		$language[voucher] = 'Szállodautalvány';
		$language[id] = "SORSZÁM";
		$language[price] = "ÁRA";
		
		$language[name] = "name";
		$language[shortname] = "shortname";
		$language[plus_room1_name] = "plus_room1_name";
		$language[plus_room2_name] = "plus_room2_name";
		$language[plus_room3_name] = "plus_room3_name";
		$language[plus_other1_name] = "plus_other1_name";
		$language[plus_other2_name] = "plus_other2_name";
		$language[plus_other3_name] = "plus_other3_name";
		$language[plus_other4_name] = "plus_other4_name";
		$language[plus_other5_name] = "plus_other5_name";
		$language[plus_other6_name] = "plus_other6_name";
		
		$language[plus_single1_name] = "plus_single1_name";
		$language[plus_single2_name] = "plus_single2_name";
		$language[plus_single3_name] = "plus_single3_name";
		
		
		
		$language[currency] = 'Ft';
		$language[plus_validity] = "validity";
		$language[plus_not_include] = "not_include";
		$language[package] = "csomag";
		
		$language[validity] = "validity";
		$language[not_include] = "not_include";
		$language[services] = "Az szállodautalvány a következő plusz szolgáltatásokra jogosítja";
		$language[child] = "gyermek";
		$language[booking] = "Foglalás";
		$language[booking_text] = "Kérjük, hivatkozzon a lapon található sorszámra, illetve, az utalványt szíveskedjék magával hozni és érkezéskor a szálloda recepcióján leadni.";
		$language[validity] = "Érvényesség";
		$language[not_include] = "Külön fizetendő";
		$language[plus_weekend] = "Hétvégi felár";
		$language[plus_extra_bed] = "Pótágy felár";
		$language[night] = "éj";
		$language[halfboard] = 'félpanzió';
		$language[extra] = "felár";

		$language[plus_one_night] ="Maradjon még egy éjszakát";
		
		$language[notification] = 'AZ UTALVÁNYON SZEREPLŐ PLUSZ KEDVEZMÉNYES SZOLGÁLTATÁSOK ÁRAI CSAK A KIÁLLÍTÁSKOR ÉRVÉNYESEK!<br/><br/>
		AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ';
		
		$language[notification2] = '
		
		
A QUAESTOR UTAZÁSSZERVEZŐ KFT. kizár minden felelősséget a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.<br/>
A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag es <br/>
közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti.<br/>
A szallodai szolgáltatási jogviszony a voucheren feltüntetett szálloda es a voucher tulajdonosa között jön létre.';
		$language[lang] = 'hu';
		
		$offerArr[validity] = str_replace("ajándékutalvány",'szállodautalvány',$offerArr[validity]);
		
	}
	else
	{
		$language = array();
		$language[voucher] = 'Ajándékutalvány';
		$language[id] = "SORSZÁM";
		$language[price] = "ÁRA";
		
		$language[name] = "name";
		$language[shortname] = "shortname";
		$language[plus_room1_name] = "plus_room1_name";
		$language[plus_room2_name] = "plus_room2_name";
		$language[plus_room3_name] = "plus_room3_name";
		$language[plus_other1_name] = "plus_other1_name";
		$language[plus_other2_name] = "plus_other2_name";
		$language[plus_other3_name] = "plus_other3_name";
		$language[plus_other4_name] = "plus_other4_name";
		$language[plus_other5_name] = "plus_other5_name";
		$language[plus_other6_name] = "plus_other6_name";
		
		$language[plus_single1_name] = "plus_single1_name";
		$language[plus_single2_name] = "plus_single2_name";
		$language[plus_single3_name] = "plus_single3_name";
		
		
		
		$language[currency] = 'Ft';
		$language[plus_validity] = "validity";
		$language[plus_not_include] = "not_include";
		$language[package] = "csomag";
		
		$language[validity] = "validity";
		$language[not_include] = "not_include";
		$language[services] = "Az ajándékutalvány a következő plusz szolgáltatásokra jogosítja";
		$language[child] = "gyermek";
		$language[booking] = "Foglalás";
		$language[booking_text] = "Kérjük, hivatkozzon a lapon található sorszámra, illetve, az utalványt szíveskedjék magával hozni és érkezéskor a szálloda recepcióján leadni.";
		$language[validity] = "Érvényesség";
		$language[not_include] = "Külön fizetendő";
		$language[plus_weekend] = "Hétvégi felár";
		$language[plus_extra_bed] = "Pótágy felár";
		$language[night] = "éj";
		$language[halfboard] = 'félpanzió';
		$language[extra] = "felár";

		$language[plus_one_night] ="Maradjon még egy éjszakát";
		
		$language[notification] = 'AZ UTALVÁNYON SZEREPLŐ PLUSZ KEDVEZMÉNYES SZOLGÁLTATÁSOK ÁRAI CSAK A KIÁLLÍTÁSKOR ÉRVÉNYESEK!<br/><br/>
		AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ';
		
		$language[notification2] = "A $company a jelen voucheren feltüntetett szálloda bizományosi értékesítője, <br/>így kizárja minden felelősségét a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.<br/>
		A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag <br/>és közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti. <br/>
		A bizományos felelőssége kizárólag a voucher ellenértékének a szálloda részére történő befizetésére terjed ki. <br/>
		A szállodai szolgáltatási jogviszony a voucheren feltüntetett szálloda és a voucher tulajdonosa között jön létre.";
		$language[lang] = 'hu';
	}
	

	$validity = str_replace("|","",$offerArr[$language[plus_validity]]);
	$not_include = str_replace("|","",$offerArr[$language[plus_not_include]]);;



$from = array('ț','ș','ș','Ț','Ș');
$to = array('ţ','ş','ş','Ţ','Ş');

foreach($language as $key => $value)
{	
	$language[$key] = str_replace($from,$to,$language[$key]);
}	

foreach($offerArr as $key => $value)
{	
	$offerArr[$key] = str_replace($from,$to,$offerArr[$key]);
}	

if($voucherArr[reseller_id] == 2999)
{
	$file = 'quaestor';
}
elseif($reseller == 1)
{
	$file = 'empty';
}
else
{
	if($offerArr[is_qr] == 1)
		$file = '10';
	elseif(substr($voucherArr[offer_id],0,3) == 'CZO')
		$file = '8';
	elseif(substr($voucherArr[offer_id],0,3) == 'TVO')
		$file = '9';
	elseif($voucherArr[type] == 1 || $voucherArr[type] == 2 || $voucherArr[type] == 6 || $voucherArr[type] == 7)
		$file = '4';
	else
		$file = '5';
}

if($offerArr[is_qr] == 1)
{
	if($voucherArr[is_qr] == 0)
	{
		$mysql->query("UPDATE customers SET is_qr = 1 WHERE cid = $voucherArr[cid]");
	}
?>
	<link rel="stylesheet" href="/inc/voucher_qr.css" type="text/css" media="all" />
<?
}
else { 
?>
	<link rel="stylesheet" href="/inc/voucher_normal.css" type="text/css" media="all" />

<? } ?>


<div id="container" style="background:url('http://admin.indulhatunk.info/vouchers/<?=$file?>.jpg') no-repeat;">
	<div id="header">
		<div id="voucher"><? if($offerArr[is_qr] <> 1) echo $language[voucher]?></div>
		<div id="descr">
			<div id="date" style='text-transform:uppercase;'><?=mb_strtoupper(formatDate(date("Y-m-d"),$language[lang]),'UTF-8')?></div>
			<div id="serial"><?=$language[id]?>: N&deg;<?=$voucherArr[offer_id];?></div>
			<div id="price"><?if($voucherArr[gift] == 0) {?> <?=$language[price]?>: <span class="bigPrice"><?=formatPrice($voucherArr[orig_price],1);?></span> Ft <?}?></div>		
			<div class="cleaner"></div>
		</div>
	</div>
	<div id="title">
		<?=str_replace("|","",$voucherArr[hotel_name]);?>	
		<div id="voucherImageAddress"><?=$voucherArr[zip];?> <?=$voucherArr[city];?> <?=$voucherArr[address];?> tel.: <?=$voucherArr[reception_phone];?>/<?=$voucherArr[reception_email];?> </div>
	
	</div>
	<div id="subTitle">
		<div class="bid_title"><?=$offerArr[$language[shortname]]?>	</div>
		<?=$offerArr[$language[name]]?>	
	</div>
	<div id="voucherContent">
	<div id="info">
		<div id="infoLeft">
		
		<b><?=$language[services]?>:</b>
		
		<ul class="plusService">
		<!-- offer properties -->
		<?if($offerArr[plus_half_board]<>0) {
		
			$class ='';
			if($voucherArr[plus_half_board] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[halfboard]?> <?if($voucherArr[gift] == 0) {?> <?=formatPrice($offerArr[plus_half_board],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
		<?}?>

		<?if($offerArr[plus_weekend_plus]<>0) {
		
			$class ='';
			if($voucherArr[plus_weekend_plus] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[plus_weekend]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_weekend_plus]*2,0,0,$language[currency])?> <? } ?></li>
		<?}?>

		<?if($offerArr[plus_bed]<>0) {
		
		$class ='';
			if($voucherArr[plus_bed] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[plus_extra_bed]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_bed],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
		<?}?>

		<?if($offerArr[plus_bed_plus_food]<>0) {
		
		$class ='';
			if($voucherArr[plus_bed_plus_food] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>">Félpanzió + pótágy felár <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_bed_plus_food],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
		<?}?>

<?if($voucherArr[plus_child1_value]<>0) {?>
<li class="bold">1. <?=$language[child]?>
	<?
	if($voucherArr[plus_child1_value]==1 || ($voucherArr[plus_child1_value] >1 && $voucherArr[plus_child1_value]==$offerArr[plus_child1_value]))
	{
		if($voucherArr[gift] == 0)
			echo	"0-$offerArr[plus_child1_name]: ".formatPrice($offerArr[plus_child1_value],0,0,$language[currency])."/$language[night]";
		else
			echo	"0-$offerArr[plus_child1_name]";

	}
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child2_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]: ".formatPrice($offerArr[plus_child2_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]";

	}
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child3_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child2_name]+: ".formatPrice($offerArr[plus_child3_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child2_name]+";

	}
	?>
</li>
<?}?>

<?if($voucherArr[plus_child2_value]<>0) {?>
<li class="bold">2. <?=$language[child]?>
	<?
	if($voucherArr[plus_child2_value]==1 || ($voucherArr[plus_child2_value] >1 && $voucherArr[plus_child2_value]==$offerArr[plus_child1_value]))
	{
		if($voucherArr[gift] == 0)
			echo	"0-$offerArr[plus_child1_name]: ".formatPrice($offerArr[plus_child1_value],0,0,$language[currency])."/$language[night]";
		else
			echo	"0-$offerArr[plus_child1_name]";

	}
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child2_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]: ".formatPrice($offerArr[plus_child2_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]";

	}
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child3_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child2_name]+: ".formatPrice($offerArr[plus_child3_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child2_name]+";

	}
	?>
</li>
<?}?>

<?if($voucherArr[plus_child3_value]<>0) {?>
<li class="bold">3. <?=$language[child]?>
	<?
	if($voucherArr[plus_child3_value]==1  || ($voucherArr[plus_child3_value] >1 && $voucherArr[plus_child3_value]==$offerArr[plus_child1_value]))
	{
		if($voucherArr[gift] == 0)
			echo	"0-$offerArr[plus_child1_name]: ".formatPrice($offerArr[plus_child1_value],0,0,$language[currency])."/$language[night]";
		else
			echo	"0-$offerArr[plus_child1_name]";

		
	}
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child2_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]: ".formatPrice($offerArr[plus_child2_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]";

	}
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child3_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child2_name]+: ".formatPrice($offerArr[plus_child3_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child2_name]+";

	}
	?>	
</li>
<?}?>

	<?if($offerArr[plus_room1_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_room1_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?> <?=formatPrice($offerArr[plus_room1_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>

	<?if($offerArr[plus_room2_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_room2_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_room2_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>

	<?if($offerArr[plus_room3_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_room3_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_room3_value],0,0,$language[currency])?>/<?=$language[night]?><? } ?></li>
	<?}?>

	<?if($offerArr[plus_other1_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other1_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other1_value],0,0,$language[currency])?>/<?=$language[night]?><? } ?></li>
	<?}?>

	<?if($offerArr[plus_other2_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other2_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other2_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>

	<?if($offerArr[plus_other3_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other3_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other3_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>
	

	<?if($offerArr[plus_other4_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other4_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other4_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other4_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>
	
	<?if($offerArr[plus_other5_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other5_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other5_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other5_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>
	
	
	<?
	if($offerArr[plus_other6_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_other6_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other6_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other6_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?
	}
	?>
	<?
	if($offerArr[plus_single1_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_single1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_single1_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_single1_value],0,0,$language[currency])?>/<?=$language[package]?> <? } ?></li>
	<?}?>
	<?
	if($offerArr[plus_single2_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_single2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_single2_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_single2_value],0,0,$language[currency])?>/<?=$language[package]?> <? } ?></li>
	<?}?>
	<?
	if($offerArr[plus_single3_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_single3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_single3_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_single3_value],0,0,$language[currency])?>/<?=$language[package]?> <? } ?></li>
	<?}?>
	
	
	<?if( $voucherArr[plus_days]>0) {
	
	
	$class ='';
	if($voucherArr[plus_days] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
	<li class="<?=$class?>"><?=$language[plus_one_night]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($voucherArr[plus_days],0,0,$language[currency])?> <? } ?></li>
<?}?>
</ul>
		<!-- offer properties end -->
		</div>
		<div id="infoRight"><div id="subT"><?=$language[booking]?>:</div>
			<div id="room">
				<?=$language[booking_text]?>
			</div>
	<div id="st">
		<?=$language[validity]?>:
	</div>
	<div id="validity">
		<?=$validity?>
	</div>
	
	<div id="notIncludeTitle">
		<?=$language[not_include]?>
	</div>
	<div id="notInclude">
		<?=$not_include?>
	</div>


		</div>
		<div class="cleaner"></div>
	</div>
	
	<div id="voucherImage"><img src="<?=$image?>"/></div>
	
		<div id="footer">
		<?=$language[notification]?>
	<div class='notice'>
		<?=$language[notification2]?>
		</div>
	</div>
	</div>
	<div id="outlet">
	 <? if($reseller == 0 ) { ?>
		<? 	if($voucherArr[type] == 1 || $voucherArr[type] == 2 || $voucherArr[type] == 6 || $voucherArr[type] == 7) { ?>	
			www.lealkudtuk.hu
		<? 
		} elseif(substr($voucherArr[offer_id],0,3) == "CZO") { ?>
			www.cazareoutlet.ro
		<? 
		} elseif(substr($voucherArr[offer_id],0,3) == "TVO") { ?>
			www.traveloutlet.sk
		<?
		} else { ?>
			www.szállásoutlet.hu
		<? } ?> 
		
		<? } ?>
	</div>
	<div  id='qrcode'><img src='http://chart.apis.google.com/chart?cht=qr&chs=150x150&chld=Q|0&chl=<?=$voucherArr[offer_id];?>' width='70'/></div>
	<div id="copyright">printed by indulhatunk.info &copy;<?=date("Y")?> - All rights reserved. (#<?=$voucherArr[cid]?>)</div>
</div>

<?
}
*/

function generatePdfV2($id=1,$reseller = 0) {
	global $mysql;
	global $lang;
	
	$voucherData = $mysql->query("SELECT customers.reseller_id,customers.type,customers.company_invoice,customers.payment,customers.invoice_name,customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE customers.cid = $id LIMIT 1");
	$voucherArr = mysql_fetch_assoc($voucherData);
	
	if($voucherArr[sub_pid] > 0)
	{
		$voucherData = $mysql->query("SELECT customers.reseller_id,customers.type,customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.sub_pid WHERE customers.cid = $id LIMIT 1");
		$voucherArr = mysql_fetch_assoc($voucherData);
	
	}
	$offerData = $mysql->query("SELECT * FROM offers  WHERE id = $voucherArr[offers_id] LIMIT 1");
	$offerArr = mysql_fetch_assoc($offerData);
	$month = date("n");
	
	
	if($voucherArr[company_invoice] == 'szallasoutlet')
		$company = 'SzállásOutlet Kft.';
	elseif($voucherArr[company_invoice] == 'indusz')
		$company = 'Indulhatunk Utazásszervező Kft.';
	else
		$company = 'Hotel Outlet Kft.';
	
	//https://admin.indulhatunk.info/vouchers/partners2/<?=$voucherArr[coredb_id].jpg
	
	if($voucherArr[sub_pid] > 0)
	{
	
		$logopic = $voucherArr[coredb_id];
	}
	else
	{
		$logopic = $voucherArr[coredb_id];
	}
	if(!file_exists("/var/www/lighttpd/admin.indulhatunk.info/vouchers/partners2/$logopic.jpg"))
	{
		$image = 'http://admin.indulhatunk.hu/vouchers/partners2/nopic.jpg';
	}
	else
		$image = "http://admin.indulhatunk.hu/vouchers/partners2/$logopic.jpg";
		//die('Az utalvány nem tölthető jelenleg. Kérjük értesítsen minket a problémáról a vtl@indulhatunk.hu e-mail címen. Köszönjük!');
	/****
	- Hotelová poukážka Vás oprávňuje na využitie nasledujúcich extra služieb
-	dieťa 
-	rezervácia 
-	Prosím zapamätajte si poradové číslo uvedené na poukážke, alebo si ju zo sebou vezmite a odovzdajte ju na recepcii hotela. 
-	Doba platnosti
-	Cena nezahŕňa 
-	Víkendový príplatok
-	Príplatok za prístelku 
-	noc
-	polpenzia
-	príplatok
-	Zostaňte ešte jednu noc 
-	CENY EXTRA SLUŽIEB UVEDENÝCH NA POUKÁŽKE SÚ PLATNÉ LEN V ČASE JEJ VYSTAVENIA!<br/><br/>POUKÁŽKU NIE JE MOŽNÉ ZAMENIŤ ZA HOTOVOSŤ. 
Travel Outlet s.r.o. je komisionálnym / mediačným predajcom hotela uvedeného na poukážke, <br/>čím sa zrieka zodpovednosti za neposkytnutie služby zo strany hotela z akýchkoľvek dôvodov. <br/>Zákazník, ktorý si zakúpi poukážku, berie na vedomie, že požiadavky súvisiace s vrátením peňazí alebo odškodnením si  môže uplatniť výlučne<br/> a priamo u hotela uvedeného na poukážke. <br/>Zodpovednosť sprostredkovateľa sa vzťahuje len na úhradu protihodnoty poukážky hotelu. <br/> Právny vzťah týkajúci sa služieb poskytovaných hotelom vzniká medzi hotelom uvedeným na poukážke a vlastníkom poukážky. 
-	

*/
	if($offerArr[language] == 'en' && $offerArr[company_invoice] <> 'indusz')
	{
		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "Nr.";
		$language[price] = "PRICE";
		
		$language[name] = "en_name";
		$language[shortname] = "en_shortname";
		$language[plus_room1_name] = "en_plus_room1_name";
		$language[plus_room2_name] = "en_plus_room2_name";
		$language[plus_room3_name] = "en_plus_room3_name";
		$language[plus_other1_name] = "en_plus_other1_name";
		$language[plus_other2_name] = "en_plus_other2_name";
		$language[plus_other3_name] = "en_plus_other3_name";
		$language[plus_other4_name] = "en_plus_other4_name";
		$language[plus_other5_name] = "en_plus_other5_name";
		$language[plus_other6_name] = "en_plus_other6_name";
		
		$language[currency] = 'Ft';

		$language[plus_single1_name] = "en_plus_single1_name";
		$language[plus_single2_name] = "en_plus_single2_name";
		$language[plus_single3_name] = "en_plus_single3_name";


		$language[plus_validity] = "en_validity";
		$language[plus_not_include] = "en_not_include";

		$language[validity] = "en_validity";
		$language[not_include] = "en_not_include";
		$language[services] = "Paid extra services";
		$language[child] = "child";
		$language[booking] = "Booking";
		$language[booking_text] = "Please refer to the Nr. of the voucher, and do not forget to present voucher at the reception at the time of the check-in.";
		$language[validity] = "Validity";
		$language[not_include] = "Other extras to be paid at the hotel reception";
		
		$language[plus_weekend] = "Weekend extra price";
		$language[plus_extra_bed] = "Extra bed extra price";
		$language[night] = "night";
		$language[plus_one_night] ="+1 night stay additional price";
		$language[package] = "package";
		$language[halfboard] = 'halfboard';
		
		if(substr($voucherArr[offer_id],0,3) == 'CZO')
		{

				$language[notification] = 'PREŢURILE EXTRA SERVICIILOR CU REDUCERE, CARE FIGUREAZĂ PE VOUCHER SUNT VALABILE NUMAI LA EMITERE!<br/><br/>
		TICHETUL NU POATE FI SCHIMBAT PE BANI';
		
		$language[notification2] = 'S.C. Hotel Outlet Ro S.R.L. este comisionarul care valorifică serviciile hotelului indicat pe voucher, <br/>
		astfel exclude orice răspundere faţă de lipsa din orice motiv a serviciilor hoteliere.
<br/>
		Cumpărătorul voucherului va lua la cunoştinţă că orice pretenţie de despăgubire sau restituire de bani poate fi prezentată exclusiv şi direct hotelului care este indicat pe voucher.<br/>
		Răspunderea comisionarului se extinde exclusiv la vărsarea contravalorii voucherului în contul hotelului.<br/>
		Relaţia juridică de servicii hoteliere se înfiinţează între hotelul indicat pe voucher şi proprietarul voucherului.';

		$voucherArr[reception_phone] = '+40 373 781 939';
		$voucherArr[reception_email] = 'booking@cazareoutlet.ro';
		
		}
		elseif(substr($voucherArr[offer_id],0,3) == 'TVO')
		{

			$language[notification] = 'CENY EXTRA SLUŽIEB UVEDENÝCH NA POUKÁŽKE SÚ PLATNÉ LEN V ČASE JEJ VYSTAVENIA!<br/><br/>POUKÁŽKU NIE JE MOŽNÉ ZAMENIŤ ZA HOTOVOSŤ. ';
		
		$language[notification2] = 'Hotel Outlet s.r.o. je komisionálnym / mediačným predajcom hotela uvedeného na poukážke, <br/>čím sa zrieka zodpovednosti za neposkytnutie služby zo strany hotela z akýchkoľvek dôvodov. <br/>Zákazník, ktorý si zakúpi poukážku, berie na vedomie, že požiadavky súvisiace s vrátením peňazí alebo odškodnením si  môže uplatniť výlučne<br/> a priamo u hotela uvedeného na poukážke. <br/>Zodpovednosť sprostredkovateľa sa vzťahuje len na úhradu protihodnoty poukážky hotelu. <br/> Právny vzťah týkajúci sa služieb poskytovaných hotelom vzniká medzi hotelom uvedeným na poukážke a vlastníkom poukážky. ';

		$voucherArr[reception_phone] = '+421 908 093 577';
		$voucherArr[reception_email] = 'booking@hoteloutlet.sk';

		}

		else
		{
			
			$language[notification] = 'AZ UTALVÁNYON SZEREPLŐ PLUSZ KEDVEZMÉNYES SZOLGÁLTATÁSOK ÁRAI CSAK A KIÁLLÍTÁSKOR ÉRVÉNYESEK!<br/><br/>
					AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ';
					
					$language[notification2] = "A $company. a jelen voucheren feltüntetett szálloda bizományosi értékesítője, <br/>így kizárja minden felelősségét a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.
					A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag és közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti. 
					A bizományos felelőssége kizárólag a voucher ellenértékének a szálloda részére történő befizetésére terjed ki.
					A szállodai szolgáltatási jogviszony a voucheren feltüntetett szálloda és a voucher tulajdonosa között jön létre.<br/>
					A vásárlót a voucher az ahhoz kötődő szállodai ajánlatban szereplő szállodai csomag és a vásárló által előzetesen igényelt és a voucheren zöld kiemeléssel szereplő plusz szolgáltatások igénybe vételére jogosítja fel.";
	
		}

		$language[lang] = 'en';

	}
	elseif(substr($voucherArr[offer_id],0,3) == 'CZO')
	{
		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "Seria";
		$language[price] = "Preț";
		$language[currency] = 'RON';
		$language[name] = "ro_name";
		$language[shortname] = "ro_shortname";
		$language[plus_room1_name] = "ro_plus_room1_name";
		$language[plus_room2_name] = "ro_plus_room2_name";
		$language[plus_room3_name] = "ro_plus_room3_name";
		$language[plus_other1_name] = "ro_plus_other1_name";
		$language[plus_other2_name] = "ro_plus_other2_name";
		$language[plus_other3_name] = "ro_plus_other3_name";
		$language[plus_other4_name] = "ro_plus_other4_name";
		$language[plus_other5_name] = "ro_plus_other5_name";
		$language[plus_other6_name] = "ro_plus_other6_name";
		
			
		$language[plus_single1_name] = "ro_plus_single1_name";
		$language[plus_single2_name] = "ro_plus_single2_name";
		$language[plus_single3_name] = "ro_plus_single3_name";


		$language[plus_validity] = "ro_validity";
		$language[plus_not_include] = "ro_not_include";

		$language[validity] = "ro_validity";
		$language[not_include] = "ro_not_include";
		$language[services] = "Se plătește separat";
		$language[child] = "copii";
		$language[booking] = "Rezervare";
		$language[booking_text] = "Vă rugăm să comunicați seria de pe foaie, și să aduceți voucherul cu Dv. pentru a-l preda la sosire la recepția hotelului.";
		$language[validity] = "Valabilitate";
		$language[not_include] = "Se plătește separat";
		
		$language[plus_weekend] = "Supliment de weekend";
		$language[plus_extra_bed] = "Supliment pat suplimentar";
		$language[night] = "nopate";
		$language[plus_one_night] ="Mai rămâneți o noapte";
		$language[package] = "package";
		$language[halfboard] = 'Demipensiune';
		
			$language[notification] = 'PREŢURILE EXTRA SERVICIILOR CU REDUCERE, CARE FIGUREAZĂ PE VOUCHER SUNT VALABILE NUMAI LA EMITERE!<br/><br/>
		TICHETUL NU POATE FI SCHIMBAT PE BANI';
		
		$language[notification2] = 'S.C. Hotel Outlet Ro S.R.L. este comisionarul care valorifică serviciile hotelului indicat pe voucher, <br/>
		astfel exclude orice răspundere faţă de lipsa din orice motiv a serviciilor hoteliere.
<br/>
		Cumpărătorul voucherului va lua la cunoştinţă că orice pretenţie de despăgubire sau restituire de bani poate fi prezentată exclusiv şi direct hotelului care este indicat pe voucher.<br/>
		Răspunderea comisionarului se extinde exclusiv la vărsarea contravalorii voucherului în contul hotelului.<br/>
		Relaţia juridică de servicii hoteliere se înfiinţează între hotelul indicat pe voucher şi proprietarul voucherului.';

		$language[lang] = 'ro';
		
		$voucherArr[reception_phone] = '+40 373 781 939';
		$voucherArr[reception_email] = 'booking@cazareoutlet.ro';




	}
		elseif(substr($voucherArr[offer_id],0,3) == 'TVO')
	{
	
	/*
		/****
	- Hotelová poukážka Vás oprávňuje na využitie nasledujúcich extra služieb
-	
-	rezervácia 
-	 
-	 
-	
-	
-	príplatok
-	 
-	

-	
*/
		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "S.ČÍSLO";
		$language[price] = "Cena";
		$language[currency] = 'EUR';
		$language[name] = "sk_name";
		$language[shortname] = "sk_shortname";
		$language[plus_room1_name] = "sk_plus_room1_name";
		$language[plus_room2_name] = "sk_plus_room2_name";
		$language[plus_room3_name] = "sk_plus_room3_name";
		$language[plus_other1_name] = "sk_plus_other1_name";
		$language[plus_other2_name] = "sk_plus_other2_name";
		$language[plus_other3_name] = "sk_plus_other3_name";
		$language[plus_other4_name] = "sk_plus_other4_name";
		$language[plus_other5_name] = "sk_plus_other5_name";
		$language[plus_other6_name] = "sk_plus_other6_name";
		
			
		$language[plus_single1_name] = "sk_plus_single1_name";
		$language[plus_single2_name] = "sk_plus_single2_name";
		$language[plus_single3_name] = "sk_plus_single3_name";


		$language[plus_validity] = "sk_validity";
		$language[plus_not_include] = "sk_not_include";

		$language[validity] = "sk_validity";
		$language[not_include] = "sk_not_include";
		$language[services] = "Hotelová poukážka Vás oprávňuje na využitie nasledujúcich extra služieb";
		$language[child] = "Dieťa ";
		$language[booking] = "Rezervácia";
		$language[booking_text] = "Prosím zapamätajte si poradové číslo uvedené na poukážke, alebo si ju zo sebou vezmite a odovzdajte ju na recepcii hotela. ";
		$language[validity] = "Doba platnosti";
		$language[not_include] = "Cena nezahŕňa ";
		
		$language[plus_weekend] = "Príplatok: víkend";
		$language[plus_extra_bed] = "Príplatok: prístelka";
		$language[night] = "noc";
		$language[plus_one_night] ="Zostaňte ešte jednu noc";
		$language[package] = "package";
		$language[halfboard] = 'polpenzia';
		
			$language[notification] = 'CENY EXTRA SLUŽIEB UVEDENÝCH NA POUKÁŽKE SÚ PLATNÉ LEN V ČASE JEJ VYSTAVENIA!<br/><br/>POUKÁŽKU NIE JE MOŽNÉ ZAMENIŤ ZA HOTOVOSŤ. ';
		
		$language[notification2] = 'Hotel Outlet s.r.o. je komisionálnym / mediačným predajcom hotela uvedeného na poukážke, <br/>čím sa zrieka zodpovednosti za neposkytnutie služby zo strany hotela z akýchkoľvek dôvodov. <br/>Zákazník, ktorý si zakúpi poukážku, berie na vedomie, že požiadavky súvisiace s vrátením peňazí alebo odškodnením si  môže uplatniť výlučne<br/> a priamo u hotela uvedeného na poukážke. <br/>Zodpovednosť sprostredkovateľa sa vzťahuje len na úhradu protihodnoty poukážky hotelu. <br/> Právny vzťah týkajúci sa služieb poskytovaných hotelom vzniká medzi hotelom uvedeným na poukážke a vlastníkom poukážky. ';

		$language[lang] = 'sk';
		$voucherArr[reception_phone] = '+421 908 093 577';
		$voucherArr[reception_email] = 'booking@hoteloutlet.sk';


	}
	elseif($voucherArr[reseller_id] == 2999)
	{

		$language = array();
		$language[voucher] = 'Szállodautalvány';
		$language[id] = "SORSZÁM";
		$language[price] = "ÁRA";
		
		$language[name] = "name";
		$language[shortname] = "shortname";
		$language[plus_room1_name] = "plus_room1_name";
		$language[plus_room2_name] = "plus_room2_name";
		$language[plus_room3_name] = "plus_room3_name";
		$language[plus_other1_name] = "plus_other1_name";
		$language[plus_other2_name] = "plus_other2_name";
		$language[plus_other3_name] = "plus_other3_name";
		$language[plus_other4_name] = "plus_other4_name";
		$language[plus_other5_name] = "plus_other5_name";
		$language[plus_other6_name] = "plus_other6_name";
		
		$language[plus_single1_name] = "plus_single1_name";
		$language[plus_single2_name] = "plus_single2_name";
		$language[plus_single3_name] = "plus_single3_name";
		
		
		
		$language[currency] = 'Ft';
		$language[plus_validity] = "validity";
		$language[plus_not_include] = "not_include";
		$language[package] = "csomag";
		
		$language[validity] = "validity";
		$language[not_include] = "not_include";
		$language[services] = "Az szállodautalvány a következő plusz szolgáltatásokra jogosítja";
		$language[child] = "gyermek";
		$language[booking] = "Foglalás";
		$language[booking_text] = "Kérjük, hivatkozzon a lapon található sorszámra, illetve, az utalványt szíveskedjék magával hozni és érkezéskor a szálloda recepcióján leadni.";
		$language[validity] = "Érvényesség";
		$language[not_include] = "Külön fizetendő";
		$language[plus_weekend] = "Hétvégi felár";
		$language[plus_extra_bed] = "Pótágy felár";
		$language[night] = "éj";
		$language[halfboard] = 'félpanzió';
		$language[extra] = "felár";

		$language[plus_one_night] ="Maradjon még egy éjszakát";
		
		$language[notification] = 'AZ UTALVÁNYON SZEREPLŐ PLUSZ KEDVEZMÉNYES SZOLGÁLTATÁSOK ÁRAI CSAK A KIÁLLÍTÁSKOR ÉRVÉNYESEK!<br/><br/>
		AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ';
		
		$language[notification2] = '
		
		
A QUAESTOR UTAZÁSSZERVEZŐ KFT. kizár minden felelősséget a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.<br/>
A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag es
közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti.
A szallodai szolgáltatási jogviszony a voucheren feltüntetett szálloda es a voucher tulajdonosa között jön létre.';
		$language[lang] = 'hu';
		
		$offerArr[validity] = str_replace("ajándékutalvány",'szállodautalvány',$offerArr[validity]);
		
	}
	else
	{
		$language = array();
		$language[voucher] = 'Ajándékutalvány';
		$language[id] = "SORSZÁM";
		$language[price] = "ÁRA";
		
		$language[name] = "name";
		$language[shortname] = "shortname";
		$language[plus_room1_name] = "plus_room1_name";
		$language[plus_room2_name] = "plus_room2_name";
		$language[plus_room3_name] = "plus_room3_name";
		$language[plus_other1_name] = "plus_other1_name";
		$language[plus_other2_name] = "plus_other2_name";
		$language[plus_other3_name] = "plus_other3_name";
		$language[plus_other4_name] = "plus_other4_name";
		$language[plus_other5_name] = "plus_other5_name";
		$language[plus_other6_name] = "plus_other6_name";
		
		$language[plus_single1_name] = "plus_single1_name";
		$language[plus_single2_name] = "plus_single2_name";
		$language[plus_single3_name] = "plus_single3_name";
		
		
		
		$language[currency] = 'Ft';
		$language[plus_validity] = "validity";
		$language[plus_not_include] = "not_include";
		$language[package] = "csomag";
		
		$language[validity] = "validity";
		$language[not_include] = "not_include";
		$language[services] = "Az ajándékutalvány a következő plusz szolgáltatásokra jogosítja";
		$language[child] = "gyermek";
		$language[booking] = "Foglalás";
		$language[booking_text] = "Kérjük, hivatkozzon a lapon található sorszámra, illetve, az utalványt szíveskedjék magával hozni és érkezéskor a szálloda recepcióján leadni.";
		$language[validity] = "Érvényesség";
		$language[not_include] = "Külön fizetendő";
		$language[plus_weekend] = "Hétvégi felár";
		$language[plus_extra_bed] = "Pótágy felár";
		$language[night] = "éj";
		$language[halfboard] = 'félpanzió';
		$language[extra] = "felár";

		$language[plus_one_night] ="Maradjon még egy éjszakát";
		
		$language[notification] = 'AZ UTALVÁNYON SZEREPLŐ PLUSZ KEDVEZMÉNYES SZOLGÁLTATÁSOK ÁRAI CSAK A KIÁLLÍTÁSKOR ÉRVÉNYESEK!<br/><br/>
		AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ';
		
		$language[notification2] = "A $company a jelen voucheren feltüntetett szálloda bizományosi értékesítője, <br/>így kizárja minden felelősségét a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.
		A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag és közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti. 
		A bizományos felelőssége kizárólag a voucher ellenértékének a szálloda részére történő befizetésére terjed ki. 
		A szállodai szolgáltatási jogviszony a voucheren feltüntetett szálloda és a voucher tulajdonosa között jön létre.
					A vásárlót a voucher az ahhoz kötődő szállodai ajánlatban szereplő szállodai csomag és a vásárló által előzetesen igényelt és a voucheren zöld kiemeléssel szereplő plusz szolgáltatások igénybe vételére jogosítja fel.";
		$language[lang] = 'hu';
	}
	

	$validity = str_replace("|","",$offerArr[$language[plus_validity]]);
	$not_include = str_replace("|","",$offerArr[$language[plus_not_include]]);;



$from = array('ț','ș','ș','Ț','Ș');
$to = array('ţ','ş','ş','Ţ','Ş');

foreach($language as $key => $value)
{	
	$language[$key] = str_replace($from,$to,$language[$key]);
}	

foreach($offerArr as $key => $value)
{	
	$offerArr[$key] = str_replace($from,$to,$offerArr[$key]);
}	

if($voucherArr[reseller_id] == 2999)
{
	$file = 'quaestor';
}
elseif($voucherArr[reseller_id] == 3625)
{
	$file = 'utazzlast';
}
elseif($reseller == 1 && $voucherArr[gift_type] < 10)
{
	$file = 'empty';
}
else
{

	if($voucherArr[gift_type] > 10)
		$file = $voucherArr[gift_type];
	elseif(substr($voucherArr[offer_id],0,3) == 'CZO' && $offerArr[is_qr] == 1)
		$file = '17';
	elseif(substr($voucherArr[offer_id],0,3) == 'TVO' && $offerArr[is_qr] == 1)
		$file = '17';
	elseif($offerArr[is_qr] == 1)
		$file = '10';
	elseif(substr($voucherArr[offer_id],0,3) == 'CZO')
		$file = '8';
	elseif(substr($voucherArr[offer_id],0,3) == 'TVO')
		$file = '9';
	elseif($voucherArr[type] == 1 || $voucherArr[type] == 2 || $voucherArr[type] == 6 || $voucherArr[type] == 7)
		$file = '4';
	else
		$file = '5';
}

if($voucherArr[gift_type] > 10)
{
?>
	<link rel="stylesheet" href="/inc/voucher_gift.css" type="text/css" media="all" />
<?
}
elseif($offerArr[is_qr] == 1)
{
	if($voucherArr[is_qr] == 0)
	{
		$mysql->query("UPDATE customers SET is_qr = 1 WHERE cid = $voucherArr[cid]");
	}
?>
	<link rel="stylesheet" href="/inc/voucher_qr.css" type="text/css" media="all" />
<?
}
else { 
?>
	<link rel="stylesheet" href="/inc/voucher_normal.css" type="text/css" media="all" />

<? } ?>

<?
if($voucherArr[reseller_id] == 2999 || $voucherArr[reseller_id] == 3625)
{
?>
	<style>
		#qrcode { display:block !important; margin-left:345px;}
		#descr { padding-bottom:12px !important}
	</style>
<? } ?>

<div id="container" style="background:url('http://admin.indulhatunk.hu/vouchers/<?=$file?>.jpg') no-repeat;">
	<div id="header">
		<div id="voucher" <? if($voucherArr[gift_type] == 12 || $voucherArr[gift_type] == 16 ) echo "style='color:white;'"?>><? if($offerArr[is_qr] == 1 && $voucherArr[gift_type] < 10 &&$voucherArr[reseller_id] <> 2999 && $voucherArr[reseller_id] <> 3625) echo ""; else echo $language[voucher]?></div>
		<div id='gift_box' <? if($voucherArr[gift_type] == 12 || $voucherArr[gift_type] == 16 ) echo "style='color:white;'"?>>
			<div id='gift_title'><?=$voucherArr[gift_title]?>!</div>
			<div id='gift_body' style='overflow:hidden;'><?=nl2br($voucherArr[gift_body])?></div>
		</div>
		<div id="descr">
			<div id="date" style='text-transform:uppercase;'><?=mb_strtoupper(formatDate(date("Y-m-d"),$language[lang]),'UTF-8')?></div>
			<div id="serial"><?=$language[id]?>: N&deg;<?=$voucherArr[offer_id];?></div>
			<div id="price"><?if($voucherArr[gift] == 0) {?> <?=$language[price]?>: <span class="bigPrice"><?=formatPrice($voucherArr[orig_price],1);?></span> Ft <?}?></div>		
			<div class="cleaner"></div>
		</div>
	</div>
	<div id="title">
		<?=str_replace("|","",$voucherArr[hotel_name]);?>	
		<div id="voucherImageAddress"><?=$voucherArr[zip];?> <?=$voucherArr[city];?> <?=$voucherArr[address];?> tel.: <?=$voucherArr[reception_phone];?>/<?=$voucherArr[reception_email];?> </div>
	
	</div>
	<div id="subTitle">
		<div class="bid_title"><?=$offerArr[$language[shortname]]?>	</div>
		<?=$offerArr[$language[name]]?>	
	</div>
	<div id="voucherContent">
	<div id="info">
		<div id="infoLeft">
		
		<b><?=$language[services]?>:</b>
		
		<ul class="plusService">
		<!-- offer properties -->
		<?if($offerArr[plus_half_board]<>0) {
		
			$class ='';
			if($voucherArr[plus_half_board] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[halfboard]?> <?if($voucherArr[gift] == 0) {?> <?=formatPrice($offerArr[plus_half_board],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
		<?}?>

		<?if($offerArr[plus_weekend_plus]<>0) {
		
			$class ='';
			if($voucherArr[plus_weekend_plus] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[plus_weekend]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_weekend_plus]*2,0,0,$language[currency])?> <? } ?></li>
		<?}?>

		<?if($offerArr[plus_bed]<>0) {
		
		$class ='';
			if($voucherArr[plus_bed] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>"><?=$language[plus_extra_bed]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_bed],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
		<?}?>

		<?if($offerArr[plus_bed_plus_food]<>0) {
		
		$class ='';
			if($voucherArr[plus_bed_plus_food] > 0)
				$class = 'bold';
			else
				$class = 'strike';
		?>
			<li class="<?=$class?>">Félpanzió + pótágy felár <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_bed_plus_food],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
		<?}?>

<?if($voucherArr[plus_child1_value]<>0) {?>
<li class="bold">1. <?=$language[child]?>
	<?
	if($voucherArr[plus_child1_value]==1 || ($voucherArr[plus_child1_value] >1 && $voucherArr[plus_child1_value]==$offerArr[plus_child1_value]))
	{
		if($voucherArr[gift] == 0)
			echo	"0-$offerArr[plus_child1_name]: ".formatPrice($offerArr[plus_child1_value],0,0,$language[currency])."/$language[night]";
		else
			echo	"0-$offerArr[plus_child1_name]";

	}
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child2_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]: ".formatPrice($offerArr[plus_child2_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]";

	}
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child3_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child2_name]+: ".formatPrice($offerArr[plus_child3_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child2_name]+";

	}
	?>
</li>
<?}?>

<?if($voucherArr[plus_child2_value]<>0) {?>
<li class="bold">2. <?=$language[child]?>
	<?
	if($voucherArr[plus_child2_value]==1 || ($voucherArr[plus_child2_value] >1 && $voucherArr[plus_child2_value]==$offerArr[plus_child1_value]))
	{
		if($voucherArr[gift] == 0)
			echo	"0-$offerArr[plus_child1_name]: ".formatPrice($offerArr[plus_child1_value],0,0,$language[currency])."/$language[night]";
		else
			echo	"0-$offerArr[plus_child1_name]";

	}
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child2_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]: ".formatPrice($offerArr[plus_child2_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]";

	}
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child3_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child2_name]+: ".formatPrice($offerArr[plus_child3_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child2_name]+";

	}
	?>
</li>
<?}?>

<?if($voucherArr[plus_child3_value]<>0) {?>
<li class="bold">3. <?=$language[child]?>
	<?
	if($voucherArr[plus_child3_value]==1  || ($voucherArr[plus_child3_value] >1 && $voucherArr[plus_child3_value]==$offerArr[plus_child1_value]))
	{
		if($voucherArr[gift] == 0)
			echo	"0-$offerArr[plus_child1_name]: ".formatPrice($offerArr[plus_child1_value],0,0,$language[currency])."/$language[night]";
		else
			echo	"0-$offerArr[plus_child1_name]";

		
	}
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child2_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]: ".formatPrice($offerArr[plus_child2_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name]";

	}
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child3_value])
	{
		if($voucherArr[gift] == 0)
			echo "$offerArr[plus_child2_name]+: ".formatPrice($offerArr[plus_child3_value],0,0,$language[currency])."/$language[night]";
		else
			echo "$offerArr[plus_child2_name]+";

	}
	?>	
</li>
<?}?>

	<?if($offerArr[plus_room1_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_room1_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?> <?=formatPrice($offerArr[plus_room1_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>

	<?if($offerArr[plus_room2_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_room2_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_room2_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>

	<?if($offerArr[plus_room3_value]<>0) {
	
	$class ='';
	if($voucherArr[plus_room3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_room3_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_room3_value],0,0,$language[currency])?>/<?=$language[night]?><? } ?></li>
	<?}?>

	<?if($offerArr[plus_other1_value]<>0) {
	$extracount++;
	$class ='';
	if($voucherArr[plus_other1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other1_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other1_value],0,0,$language[currency])?>/<?=$language[night]?><? } ?></li>
	<?}?>

	<?if($offerArr[plus_other2_value]<>0) {
	$extracount++;
	$class ='';
	if($voucherArr[plus_other2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other2_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other2_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>

	<?if($offerArr[plus_other3_value]<>0) {
	$extracount++;	
	$class ='';
	if($voucherArr[plus_other3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other3_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other3_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>
	

	<?if($offerArr[plus_other4_value]<>0) {
	$extracount++;	
	$class ='';
	if($voucherArr[plus_other4_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other4_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other4_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>
	
	<?if($offerArr[plus_other5_value]<>0) {
	$extracount++;
	$class ='';
	if($voucherArr[plus_other5_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other5_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other5_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?}?>
	
	
	<?
	if($offerArr[plus_other6_value]<>0) {
	$extracount++;
	$class ='';
	if($voucherArr[plus_other6_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_other6_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_other6_value],0,0,$language[currency])?>/<?=$language[night]?> <? } ?></li>
	<?
	}
	?>
	<?
	if($offerArr[plus_single1_value]<>0) {
	$extracount++;	
	$class ='';
	if($voucherArr[plus_single1_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_single1_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_single1_value],0,0,$language[currency])?>/<?=$language[package]?> <? } ?></li>
	<?}?>
	<?
	if($offerArr[plus_single2_value]<>0) {
	$extracount++;	
	$class ='';
	if($voucherArr[plus_single2_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_single2_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_single2_value],0,0,$language[currency])?>/<?=$language[package]?> <? } ?></li>
	<?}?>
	<?
	if($offerArr[plus_single3_value]<>0) {
	$extracount++;
	$class ='';
	if($voucherArr[plus_single3_value] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
		<li class="<?=$class?>"><?=$offerArr[$language[plus_single3_name]]?> <?=$language[extra]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($offerArr[plus_single3_value],0,0,$language[currency])?>/<?=$language[package]?> <? } ?></li>
	<?}?>
	
	
	<?if( $voucherArr[plus_days]>0) {
	
	
	$class ='';
	if($voucherArr[plus_days] > 0)
		$class = 'bold';
	else
		$class = 'strike';
	?>
	<li class="<?=$class?>"><?=$language[plus_one_night]?> <?if($voucherArr[gift] == 0) {?><?=formatPrice($voucherArr[plus_days],0,0,$language[currency])?> <? } ?></li>
<?}?>
</ul>
		<!-- offer properties end -->
		</div>
		<div id="infoRight"><div id="subT"><?=$language[booking]?>:</div>
			<div id="room">
				<?=$language[booking_text]?>
			</div>
	<div id="st">
		<?=$language[validity]?>:
	</div>
	<div id="validity">
		<?=$validity?>
	</div>
	
	<div id="notIncludeTitle">
		<?=$language[not_include]?>
	</div>
	<div id="notInclude">
		<?=$not_include?>
	</div>


		</div>
		<div class="cleaner"></div>
	</div>
	<?
		 if($extracount < 5) { ?>
		<div id="voucherImage"><img src="<?=$image?>"/></div>
	<? } else {?>
		<div style='clear:both;margin-top:30px;'></div>
	<? } ?>
		<div id="footer">
		<?=$language[notification]?>
	<div class='notice'>
		<?=$language[notification2]?>
		</div>
	</div>
	</div>
	<div id="outlet">
	 <? if($reseller == 0 ) { ?>
		<? 	if($voucherArr[type] == 1 || $voucherArr[type] == 2 || $voucherArr[type] == 6 || $voucherArr[type] == 7) { ?>	
			www.lealkudtuk.hu
		<? 
		} elseif(substr($voucherArr[offer_id],0,3) == "CZO") { ?>
			www.cazareoutlet.ro
		<? 
		} elseif(substr($voucherArr[offer_id],0,3) == "TVO") { ?>
			www.hoteloutlet.sk
		<?
		} elseif($voucherArr[reseller_id] <> 2999) { ?>
			www.szállásoutlet.hu
		<? } ?> 
		
		<? } ?>
	</div>
	<div  id='qrcode' ><img src='http://chart.apis.google.com/chart?cht=qr&chs=150x150&chld=Q|0&chl=<?=$voucherArr[offer_id];?>' <? if($offerArr[is_qr] <> 1) echo "style='display:none;'"?>/></div>
	<div id="copyright">printed by indulhatunk.hu &copy;<?=date("Y")?> - All rights reserved. (#<?=$voucherArr[cid]?>)</div>
</div>

<?
}

function generatePdf($id=1) {
	global $mysql;
	
	$voucherData = $mysql->query("SELECT customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE customers.cid = $id LIMIT 1");
	$voucherArr = mysql_fetch_assoc($voucherData);
	$month = date("n");
	
	$monthArray = array(
		1 => 'JANUÁR',
		2 => 'FEBRUÁR',
		3 => 'MÁRCIUS',
		4 => 'ÁPRILIS',
		5 => 'MÁJUS',
		6 => 'JÚNIUS',
		7 => 'JÚLIUS',
		8 => 'AUGUSZTUS',
		9 => 'SZEPTEMBER',
		10 => 'OKTÓBER',
		11 => 'NOVEMBER',
		12 => 'DECEMBER',
	);
	$year = date("Y");
	$day = date("d");
	$validity = str_replace("|","<br/>",$voucherArr[validity]);
	$not_include = str_replace("|","<br/>",$voucherArr[not_include]);;
?>
<div id="container">
	<div id="header">
		<div id="date"><?=$year;?>. <?=$monthArray[$month];?> <?=$day;?>.</div>
		<div id="serial">SORSZÁM: N&deg;<?=$voucherArr[offer_id];?></div>
		<div id="price">ÁRA: <span class="bigPrice"><?=formatPrice($voucherArr[orig_price],1);?></span> Ft</div>		
		<div class="cleaner"></div>
	</div>
	<div id="title">
		<?=str_replace("|","",$voucherArr[hotel_name]);?>		
	</div>
	<div id="subTitle">
		<?=str_replace("|","",$voucherArr[bid_name]);?>	
	</div>
	<div id="voucherImage"><img src="http://admin.indulhatunk.hu/vouchers/partners/<?=$voucherArr[coredb_id]?>.jpg"/></div>
	<div id="voucherImageTitle"><?=str_replace("|","",$voucherArr[hotel_name]);?></div>
	<div id="voucherImageAddress"><?=$voucherArr[zip];?> <?=$voucherArr[city];?> <?=$voucherArr[address];?> | <?=$voucherArr[phone];?> </div>
	<div id="roomTitle">Szobafoglalás:</div>
	<div id="room">
		Kérjük, hivatkozzon a lapon található sorszámra, illetve, az utalványt szíveskedjék<br/>
		 magával hozni és érkezéskor a szálloda recepcióján leadni.
	</div>
	<div id="validity">
		<?=$validity?>
	</div>
	
	<div id="notIncludeTitle">
		külön fizetendő:
	</div>
	<div id="notInclude">
		<?=$not_include?>
	</div>
	<div id="footer">
		AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ

	</div>
</div>

<pagebreak/>
<?
}
function generatePdfStats($id=1) {
	global $mysql;
	
	if($id <> 1)
	{
		$voucherQr = $mysql->query("SELECT * FROM customers WHERE print_id = '$id'  ORDER BY cid DESC");

	}
	else
	{
		$voucherData = $mysql->query("SELECT MAX(print_date) AS date FROM customers ORDER BY cid ASC ");
		$voucherArr = mysql_fetch_assoc($voucherData);
		$voucherDate = $voucherArr[date];
		$voucherQr = $mysql->query("SELECT * FROM customers WHERE print_date = '$voucherDate'  ORDER BY cid DESC");
	}
	//echo $voucherDate;
	
	
	$statTable ="<pagebreak/>";
	$statTable.= "<br/><br/><br/><br/><table border=1 cellspacing=0 cellpadding=3 style='margin:0 0 0 80px'>";
	$statTable.= "<tr>
					<td>Voucher</td>
					<td>Név</td>
					<td>Cím</td>
				</tr>";
	while($arr = mysql_fetch_assoc($voucherQr))
	{
		$statTable.="<tr>
		
		<td>$arr[offer_id]</td>
		<td>$arr[name]</td>
		<td>$arr[zip] $arr[city] $arr[address]</td>
		
		</tr>";
	}
	$statTable.="</tr>";


	return $statTable;
}
function generateEnvelope($id=1) {
	global $mysql;
	
	$voucherData = $mysql->query("SELECT customers.cid,customers.address,customers.city,customers.zip,customers.name FROM customers WHERE customers.cid = $id LIMIT 1 ");
	$voucherArr = mysql_fetch_assoc($voucherData);
	
$envelope = '';
$envelope.="<div id=\"envelopeAddress\">";
$envelope.="<span class=\"customerName\">$voucherArr[name]</span> <font-size='10px'>(#$voucherArr[cid])</font><br/><br/>";
$envelope.="$voucherArr[city]<br/>";
$envelope.="$voucherArr[address]<br/>";
$envelope.="$voucherArr[zip]<br/>";
$envelope.="</div>";
$envelope.="<pagebreak/>";
	return $envelope;
}
function generatePost($id=1) {
	global $mysql;
	
	$voucherData = $mysql->query("SELECT customers.cid,customers.address,customers.city,customers.zip,customers.name FROM customers WHERE customers.cid = $id LIMIT 1");
	$voucherArr = mysql_fetch_assoc($voucherData);
	
$envelope = '';


$envelope.="<div id=\"postAddress\">";
$envelope.="<div id=\"priX\"></div>";

$envelope.="<div id=\"postX\"></div>";
$envelope.="<div class=\"postText\"><span class=\"customerName\">$voucherArr[name]</span> (#$voucherArr[cid])<br/>";
$envelope.="$voucherArr[city]<br/>";
$envelope.="$voucherArr[address]<br/>";
$envelope.="$voucherArr[zip]</div> ";

$envelope.="</div>";
$envelope.="<pagebreak/>";
	return $envelope;
}

/*
function generateNumber($company = '')
{

	$abc= array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	$num= array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	if($company == 'hoteloutlet')
		$string = "SZO-";
	elseif($company == 'tour')
		$string = 'IND-'.date("Y")."/";
	else
		$string = "VTL-";

	$string.=strtoupper($abc[rand(0,25)]);
	$string.=strtoupper($abc[rand(0,25)]);
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	
	//return "VTL-".$helper[id];
	return $string;
}
*/

function isduplicatevoucher($string)
{
	global $mysql;
	
	$check = $mysql->query("SELECT * FROM customers WHERE offer_id = '$string' LIMIT 1");
	if(mysql_num_rows($check) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}

	
}
function generateNumber($company = '')
{
	global $mysql;
	
	
	$abc= array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	$num= array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	if($company == 'hoteloutlet')
		$string = "SZO-";
	if($company == 'indusz')
		$string = "ISZ-";
	elseif($company == 'szallasoutlet')
		$string = "SZO-";
	elseif($company == 'professional')
		$string = "PRO-";
	elseif($company == 'tour')
		$string = 'IND-'.date("Y")."/";
	elseif($company <> '')
		$string = "$company";
	else
		$string = "SZO-";

	$string.=strtoupper($abc[rand(0,25)]);
	$string.=strtoupper($abc[rand(0,25)]);
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	$string.=$num[rand(0,9)];
	
	if(isduplicatevoucher($string) == true)
	{
		return generateNumber($company);
	}
	else
	{
		return $string;
	}
}

function generateOuterName()
{
	$abc= array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");


	$string= ($abc[rand(0,25)]);
	$string.= ($abc[rand(0,25)]);	
	$string.= ($abc[rand(0,25)]);	
	return $string;
}

//emailer script
 function get_mime_type(&$structure) {
   $primary_mime_type = array("TEXT", "MULTIPART","MESSAGE", "APPLICATION", "AUDIO","IMAGE", "VIDEO", "OTHER");
   if($structure->subtype) {
   	return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;
   }
   	return "TEXT/PLAIN";
   }
   function get_part($stream, $msg_number, $mime_type, $structure = false,$part_number    = false) {
   
   	if(!$structure) {
   		$structure = imap_fetchstructure($stream, $msg_number);
   	}
   	if($structure) {
   		if($mime_type == get_mime_type($structure)) {
   			if(!$part_number) {
   				$part_number = "1";
   			}
   			$text = imap_fetchbody($stream, $msg_number, $part_number);
   			if($structure->encoding == 3) {
   				return imap_base64($text);
   			} else if($structure->encoding == 4) {
   				return imap_qprint($text);
   			} else {
   			return $text;
   		}
   	}
   
		if($structure->type == 1) /* multipart */ {
   		while(list($index, $sub_structure) = each($structure->parts)) {
   			if($part_number) {
   				$prefix = $part_number . '.';
   			}
   			$data = get_part($stream, $msg_number, $mime_type, $sub_structure,$prefix .    ($index + 1));
   			if($data) {
   				return $data;
   			}
   		} // END OF WHILE
   		} // END OF MULTIPART
   	} // END OF STRUTURE
   	return false;
   } // END OF FUNCTION
   
  function message($text) {
		
		if($text <> '')
			$message = "<div class='message'>$text</div>";
		else
			$message = '';
			
	return $message;
}

function getStatistics($dateMinus,$year=0,$company='')
{
	global $mysql;
	if($year == 0)	
		$year = date("Y");
$monthlyStats = $mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus  AND year(customers.paid_date) = $year  AND customers.inactive=0  AND customers.facebook = 0");//AND invoice_created <> 1
$monthlyArr = mysql_fetch_assoc($monthlyStats);


$otpArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 10)  AND customers.inactive=0  AND customers.facebook = 0"));//AND invoice_created <> 1 

$mkbArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 11)  AND customers.inactive=0  AND customers.facebook = 0"));//AND invoice_created <> 1 

$khArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 12)  AND customers.inactive=0  AND customers.facebook = 0"));//AND invoice_created <> 1 


$checkArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 5)  AND customers.inactive=0  AND customers.facebook = 0"));//AND invoice_created <> 1 

$placeArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND  (payment = 6)  AND customers.inactive=0  AND customers.facebook = 0  "));//AND invoice_created <> 1 

$transferArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 1)  AND customers.inactive=0  AND customers.facebook = 0 "));//AND invoice_created <> 1 

$creditArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 9)  AND customers.inactive=0  AND customers.facebook = 0 "));//AND invoice_created <> 1 

$cashArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 2)  AND customers.inactive=0  AND customers.facebook = 0  "));//AND invoice_created <> 1 

$postpaidArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1  AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 3)  AND customers.inactive=0  AND customers.facebook = 0  "));//AND invoice_created <> 1 

$deliveryArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (payment = 4)  AND customers.inactive=0  AND customers.facebook = 0 "));//AND invoice_created <> 1 

$onlineArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (shipment = 2)  AND customers.inactive=0  AND customers.facebook = 0  "));//AND invoice_created <> 1 

$postArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND (shipment = 4)  AND customers.inactive=0  AND customers.facebook = 0 "));//AND invoice_created <> 1 




//teszvesz
$tvstats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year  AND customers.facebook = 0 AND type = 2 "));

//vatera
$vtstats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND MONTH(customers.paid_date) = $dateMinus  AND year(customers.paid_date) = $year AND customers.facebook = 0 AND type = 1"));
//lealkudtuk
$lastats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year AND customers.facebook = 0 AND type = 6 "));

$allegroyield = ($tvstats[whiteSum] + $vtstats[whiteSum] + $lastats[whiteSum])*0.05;
//licittravel
$listats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND MONTH(customers.paid_date) = $dateMinus AND year(customers.paid_date) = $year  AND customers.facebook = 0 AND type = 3 "));
$liyield = $listats[whiteSum]*0.07;
//checkyield
$checkyield = $checkArr[whiteSum]*0.075;
?>


<div style="float:left;padding:0 0 0 10px;">

<h1><?=$dateMinus?>. havi egyenleg</h1>

<table>
<tr class="grey" style='border-bottom:2px solid black;'>
	<td>Teljes (<?=$monthlyArr[whiteCount]?> db) ~<?=formatPrice(round($monthlyArr[whiteSum]/$monthlyArr[whiteCount]))?></td>
	<td align="right"><?=formatPrice($monthlyArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>Üdülési csekk (<?=$checkArr[whiteCount]?> db) ~<?=round($checkArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($checkArr[whiteSum])?> </td>
</tr>
<!-- -->
<tr class="white">
	<td>OTP SZÉP (<?=$otpArr[whiteCount]?> db) ~<?=round($otpArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($otpArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>MKB SZÉP (<?=$mkbArr[whiteCount]?> db) ~<?=round($mkbArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($mkbArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>K&amp;H SZÉP (<?=$khArr[whiteCount]?> db) ~<?=round($khArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($khArr[whiteSum])?> </td>
</tr>
<!-- -->
<tr class="white">
	<td>Helyszínen (<?=$placeArr[whiteCount]?> db) ~<?=round($placeArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($placeArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>Átutalás (<?=$transferArr[whiteCount]?> db) ~<?=round($transferArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($transferArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>Bankkártya (<?=$creditArr[whiteCount]?> db) ~<?=round($creditArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($creditArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>Készpénz (<?=$cashArr[whiteCount]?> db) ~<?=round($cashArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($cashArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>Utánvét (<?=$postpaidArr[whiteCount]?> db) ~<?=round($postpaidArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($postpaidArr[whiteSum])?> </td>
</tr>
<tr class="white">
	<td>Futár (<?=$deliveryArr[whiteCount]?> db) ~<?=round($deliveryArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($deliveryArr[whiteSum])?> </td>
</tr>


<!--

<tr class="grey" style='border-top:2px solid black;'>
	<td>Online (<?=$onlineArr[whiteCount]?> db) ~<?=round($onlineArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($onlineArr[whiteSum])?> </td>
</tr>

<tr class="grey" style='border-bottom:2px solid black;'>
	<td>Posta (<?=$postArr[whiteCount]?> db) ~<?=round($postArr[whiteSum]/$monthlyArr[whiteSum]*100)?>% </td>
	<td align="right"><?=formatPrice($postArr[whiteSum])?> </td>
</tr> -->
<!--

<tr class="red">
	<td>Egyenleg</td>
	<td align="right"><?=formatPrice($monthlyArr[whiteSum]-$checkArr[whiteSum])?></td>
</tr> -->
<tr class="green">
	<td>Jutalék</td>
	<td align="right"><?=formatPrice($monthlyArr['yield']*1.25-$allegroyield-$liyield-$checkyield)?></td>
</tr>

</table>
</div>
<?
}

function getShortStats($company='')
{
	global $mysql;
	

$dateNow = date("n");
$yearNow = date("Y");

$monthlyStats = $mysql->query("SELECT count(*) AS whiteCount, sum(actual_price) AS whiteSum FROM customers WHERE  inactive=0 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0");
$monthlyArr = mysql_fetch_assoc($monthlyStats);


$monthlyStats2 = $mysql->query("SELECT count(*) AS whiteCount, sum(actual_price) AS whiteSum FROM customers WHERE  inactive=0  AND type <> 4 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0");
$monthlyArr2 = mysql_fetch_assoc($monthlyStats2);


$checkedStats = $mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE checked = 1  AND inactive=0 AND type <> 4 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0");
$checkedArr = mysql_fetch_assoc($checkedStats);

$totalQr = $mysql->query("SELECT sum(orig_price) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."' AND customers.facebook = 0");
$totalArr = mysql_fetch_assoc($totalQr);

$minusQr = $mysql->query("SELECT sum(plus_half_board*2+plus_weekend_plus+plus_bed_plus_food*2+plus_child1_value*2+plus_child2_value*2+plus_child3_value*2+plus_room1_value*2+plus_room2_value*2+plus_room3_value*2+plus_other1_value*2+plus_other2_value*2+plus_other3_value*2+plus_days+plus_bed*2) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0");
$minusArr = mysql_fetch_assoc($minusQr);


//teszvesz
$tvstats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."' AND customers.facebook = 0 AND type = 2"));
$tvminus = mysql_fetch_assoc($mysql->query("SELECT sum(plus_half_board*2+plus_weekend_plus+plus_bed_plus_food*2+plus_child1_value*2+plus_child2_value*2+plus_child3_value*2+plus_room1_value*2+plus_room2_value*2+plus_room3_value*2+plus_other1_value*2+plus_other2_value*2+plus_other3_value*2+plus_days+plus_bed*2) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0 AND customers.type = 2"));

//vatera
$vtstats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0 AND type = 1"));
$vtminus = mysql_fetch_assoc($mysql->query("SELECT sum(plus_half_board*2+plus_weekend_plus+plus_bed_plus_food*2+plus_child1_value*2+plus_child2_value*2+plus_child3_value*2+plus_room1_value*2+plus_room2_value*2+plus_room3_value*2+plus_other1_value*2+plus_other2_value*2+plus_other3_value*2+plus_days+plus_bed*2) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0 AND customers.type = 1"));

//lealkudtuk
$lastats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."' AND customers.facebook = 0 AND type = 6"));
$laminus = mysql_fetch_assoc($mysql->query("SELECT sum(plus_half_board*2+plus_weekend_plus+plus_bed_plus_food*2+plus_child1_value*2+plus_child2_value*2+plus_child3_value*2+plus_room1_value*2+plus_room2_value*2+plus_room3_value*2+plus_other1_value*2+plus_other2_value*2+plus_other3_value*2+plus_days+plus_bed*2) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0 AND customers.type = 6"));

//grando
$grstats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."' AND customers.facebook = 0 AND type = 7"));
$grminus = mysql_fetch_assoc($mysql->query("SELECT sum(plus_half_board*2+plus_weekend_plus+plus_bed_plus_food*2+plus_child1_value*2+plus_child2_value*2+plus_child3_value*2+plus_room1_value*2+plus_room2_value*2+plus_room3_value*2+plus_other1_value*2+plus_other2_value*2+plus_other3_value*2+plus_days+plus_bed*2) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0 AND customers.type = 7"));


//licittravel
$listats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."' AND customers.facebook = 0 AND type = 3"));
$liminus = mysql_fetch_assoc($mysql->query("SELECT sum(plus_half_board*2+plus_weekend_plus+plus_bed_plus_food*2+plus_child1_value*2+plus_child2_value*2+plus_child3_value*2+plus_room1_value*2+plus_room2_value*2+plus_room3_value*2+plus_other1_value*2+plus_other2_value*2+plus_other3_value*2+plus_days+plus_bed*2) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."'  AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0 AND customers.type = 3"));

//outlet
$otstats =  mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum FROM customers WHERE  inactive=0 AND month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."' AND customers.facebook = 0 AND type = 4"));
$otminus = mysql_fetch_assoc($mysql->query("SELECT sum(plus_half_board*2+plus_weekend_plus+plus_bed_plus_food*2+plus_child1_value*2+plus_child2_value*2+plus_child3_value*2+plus_room1_value*2+plus_room2_value*2+plus_room3_value*2+plus_other1_value*2+plus_other2_value*2+plus_other3_value*2+plus_days+plus_bed*2) AS whiteSum FROM customers  WHERE  customers.inactive=0 AND  month(added) = '".date("n")."' AND YEAR(added) = '".date("Y")."'  AND customers.facebook = 0 AND customers.type = 4"));


$onlineArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE MONTH(customers.added) = '".date("n")."' AND YEAR(customers.added) = '".date("Y")."' AND (shipment = 2)  AND customers.inactive=0  AND customers.facebook = 0 AND checked = 1  "));//AND invoice_created <> 1 


$postArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE MONTH(customers.added) = '".date("n")."' AND YEAR(customers.added) = '".date("Y")."' AND (shipment = 4)  AND customers.inactive=0  AND customers.facebook = 0  AND checked = 1  "));//AND invoice_created <> 1 

$personArr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE MONTH(customers.added) = '".date("n")."' AND YEAR(customers.added) = '".date("Y")."' AND (shipment = 3)  AND customers.inactive=0  AND customers.facebook = 0  AND checked = 1   "));//AND invoice_created <> 1 

$post2Arr = mysql_fetch_assoc($mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield_vtl/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE MONTH(customers.added) = '".date("n")."' AND YEAR(customers.added) = '".date("Y")."' AND (shipment = 5)  AND customers.inactive=0  AND customers.facebook = 0   AND checked = 1  "));//AND invoice_created <> 1 


$tot = $totalArr[whiteSum] - $minusArr[whiteSum];
if($tot == 0)
	$tot = 1;
?>
<!-- stat form -->
<div style="float:left;padding:10px 0 0 10px;">
<table>
<tr class="grey">
	<td>-</td>
	<td align="right">Teszvesz</td>
	<td align="right">Vatera</td>
	<td align="right">Lealkudtuk</td>
	
	<td align="right">Grando</td>
	
	<td align="right">Licittravel</td>
	<td align="right">Outlet</td>
	<td align="right">Összesen</td>
</tr>

<tr class="blue">
	<td>Kiküldött</td>
	
	<td align="right"><?=$tvstats[whiteCount]?></td>
	<td align="right"><?=$vtstats[whiteCount]?></td>
	<td align="right"><?=$lastats[whiteCount]?></td>
	<td align="right"><?=$grstats[whiteCount]?></td>
	<td align="right"><?=$listats[whiteCount]?></td>
	<td align="right"><?=$otstats[whiteCount]?></td>
	<td align="right"  class='grey'><?=($monthlyArr[whiteCount])?> db</td>
</tr>
<tr class="white">
	<td>Kitöltött (<?=round($checkedArr[whiteCount]/$monthlyArr2[whiteCount]*100)?> %)</td>
	<td colspan='6'>
		<table>
			<tr>
				<td>Online</td>
				<td><?=$onlineArr[whiteCount]?></td>
			</tr>
			<tr>
				<td>Utánvét</td>
				<td><?=$post2Arr[whiteCount]?></td>
			</tr>
			<tr>
				<td>Személyes</td>
				<td><?=$personArr[whiteCount]?></td>
			</tr>
			<tr>
				<td>Posta</td>
				<td><?=$postArr[whiteCount]?></td>
			</tr>
		
		</table>
	
	</td>
	<td align="right"  class='grey'><?=($checkedArr[whiteCount])?> db </td>
</tr>
<tr class="red">
	<td>Alap</td>
	
	<td align="right"><?=formatPrice($tvstats[whiteSum]-$tvminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($vtstats[whiteSum]-$vtminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($lastats[whiteSum]-$laminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($grstats[whiteSum]-$grminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($listats[whiteSum]-$liminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($otstats[whiteSum]-$otminus[whiteSum])?></td>
	
	<td align="right"  class='grey'><?=formatPrice($tot)?></td>
</tr>
<tr class="green">
	<td>Extra (<?=round($minusArr[whiteSum]/$tot*100)?>%) </td>
	
	<td align="right"><?=formatPrice($tvminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($vtminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($laminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($grminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($liminus[whiteSum])?></td>
	<td align="right"><?=formatPrice($otminus[whiteSum])?></td>

	
	<td align="right"  class='grey'><?=formatPrice($minusArr[whiteSum])?></td>
</tr>

</table>
</div>

<!-- stat form end -->
<div class="cleaner"></div>
</div>
<?
}


/**** bitly ****/

/* make a URL small */
function bitly($url,$format = 'xml',$version = '2.0.1')
{
  //create the URL
  $bitly = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login=indulhatunk&apiKey=R_debc4a711b83fa45fcb87b7fe3ef13ea&format='.$format;

  //get the url
  //could also use cURL here
  $response = file_get_contents($bitly);
  
  //parse depending on desired format
  if(strtolower($format) == 'json')
  {
    $json = @json_decode($response,true);
    return $json['results'][$url]['shortUrl'];
  }
  else //xml
  {
    $xml = simplexml_load_string($response);
    return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
  }
}

/* usage */


function getLongStats($company='') {
	global $mysql;
	
	
		
	$whiteQr = $mysql->query("SELECT count(customers.cid) AS whiteCount, sum(customers.orig_price) AS whiteSum , sum(customers.orig_price*(partners.yield/100)) AS yield FROM customers 
	INNER JOIN partners ON partners.pid = customers.pid WHERE customers.paid = 0 and customers.voucher = 0 AND customers.inactive=0 AND facebook = 0");
$whiteArr = mysql_fetch_assoc($whiteQr);

$greenQr = $mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND voucher = 1 AND invoice_created <> 1  AND customers.inactive=0 AND facebook = 0");
$greenArr = mysql_fetch_assoc($greenQr);

$redQr = $mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 0 AND voucher = 1 AND type <> 5  AND customers.inactive=0 AND facebook = 0");
$redArr = mysql_fetch_assoc($redQr);


$blueQr = $mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND voucher = 0  AND customers.inactive=0 AND facebook = 0");
$blueArr = mysql_fetch_assoc($blueQr);

$greyQr = $mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND voucher = 1 AND invoice_created = 1  AND customers.inactive=0 AND facebook = 0");
$greyArr = mysql_fetch_assoc($greyQr);
 
$inactiveQr = $mysql->query("SELECT count(*) AS whiteCount, sum(orig_price) AS whiteSum, sum(customers.orig_price*(partners.yield/100)) AS yield FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE customers.inactive=1 AND facebook = 0");
$inactiveArr = mysql_fetch_assoc($inactiveQr);
/*
if($filter == "white" || $filter == '')
	$filterSelect = "AND paid = 0 AND voucher = 0";
if($filter == "green")
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created <> 1";
if($filter == "blue")
	$filterSelect = "AND paid = 1 AND voucher = 0";
if($filter == "red")
	$filterSelect = "AND paid = 0 AND voucher = 1";
if($filter == "grey")
	$filterSelect = "AND paid = 1 AND voucher = 1 AND invoice_created = 1";
		
*/
$totalCount = $whiteArr[whiteCount] + $greenArr[whiteCount] + $redArr[whiteCount] + $blueArr[whiteCount] + $greyArr[whiteCount]+$inactiveArr[whiteCount];
$totalIncome = $whiteArr[whiteSum] + $greenArr[whiteSum] + $redArr[whiteSum] + $blueArr[whiteSum] + $greyArr[whiteSum];
$totalYield = $whiteArr['yield'] + $greenArr['yield'] + $redArr['yield'] + $blueArr['yield'] + $greyArr['yield'];

?>
<div class="collapse2">
<div style="float:left" >
<h1>Összesített egyenleg</h1>
<table>
<tr class="grey">
	<td>Típus</td>
	<td>Eladott (db)</td>
	<td>Összes bevétel</td>
<!--	<td>Összes jutalék</td>-->
</tr>
<tr class="white">
	<td>Elintézendő</td>
	<td align="right"><?=$whiteArr[whiteCount]?> db</td>
	<td align="right"><?=formatPrice($whiteArr[whiteSum])?></td>
	<!--<td align="right"><?=formatPrice($whiteArr['yield']*1.25)?></td>-->
</tr>
<tr class="green">
	<td>Fizetett+kiállított</td>
	<td align="right"><?=$greenArr[whiteCount]?> db</td>
	<td align="right"><?=formatPrice($greenArr[whiteSum])?></td>
	<!--<td align="right"><?=formatPrice($greenArr['yield']*1.25)?></td>-->
</tr>
<tr class="red">
	<td>Kiállított+<strike>Fizetett</strike></td>
	<td align="right"><?=$redArr[whiteCount]?> db</td>
	<td align="right"><?=formatPrice($redArr[whiteSum])?></td>
	<!--<td align="right"><?=formatPrice($redArr['yield']*1.25)?></td>-->
</tr>
<tr class="blue">
	<td>Fizetett</td>
	<td align="right"><?=$blueArr[whiteCount]?> db</td>
	<td align="right"><?=formatPrice($blueArr[whiteSum])?></td>
	<!--<td align="right"><?=formatPrice($blueArr['yield']*1.25)?></td>-->
</tr>
<tr class="grey">
	<td>Számlázott (<?=formatPrice($greyArr[whiteSum]/$greyArr[whiteCount])?>)</td>
	<td align="right"><?=$greyArr[whiteCount]?> db</td>
	<td align="right"><?=formatPrice($greyArr[whiteSum])?></td>
	<!--<td align="right"><?=formatPrice($greyArr['yield']*1.25)?></td>-->
</tr>
<tr class="pink">
	<td>Inaktiv</td>
	<td align="right"><?=$inactiveArr[whiteCount]?> db</td>
	<td align="right">(<?=formatPrice($inactiveArr[whiteSum])?>)</td>
	<!--<td align="right">(<?=formatPrice($inactiveArr['yield']*1.25)?>)</td>-->
</tr>
<tr class="white">
	<td>Összesen</td>
	<td align="right"><?=$totalCount?> db</td>
	<td align="right"><?=formatPrice($totalIncome)?></td>
	<!--<td align="right"><?=formatPrice($totalYield*1.25)?></td>-->
</tr>
</table>

</div>
<?
}



function clean_url3($name) {
	$name = trim($name);
	
		$special = array(
		'á',
		'í',
		'ű', 
		'ő',
		'ü',
		'ö',
		'ú',
		'ó',
		'é',
		'Á',
		'Í',
		'Ű', 
		'Ő',
		'Ü',
		'Ö',
		'Ú',
		'Ó',
		'É',
		'/',
		'!',
		'&',
		'*',
		'=',
		'?',
		'(',
		')',
		'%',
		'.',
		'"',
		'\'',
		',',
		';',
		'`',
		' ',
		'+',
		':',
		'\'',
		'bull',
		'#',
		']',
		'[',
		'û',
		'Õ',
		'+',
		); 
		
		$to = array(
		'a',
		'i',
		'u', 
		'o',
		'u',
		'o',
		'u',
		'o',
		'e',
		'A',
		'I',
		'U', 
		'O',
		'U',
		'O',
		'U',
		'O',
		'E',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'-',
		'+bull',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		);
		$special = str_replace($special,$to,trim($name));
		$special = str_replace("-","+",$special);
		$special = str_replace("+++++","+",$special);
		$special = str_replace("++++","+",$special);
		$special = str_replace("++++","+",$special);
		$special = str_replace("++","+",$special);
		
		/*$special = str_replace("++","+",$special);
		$special = str_replace("+-+","+",$special);
		$special = str_replace("","+",$special);
		*/
		$last = substr($special, -1);
		if($last == "+")
			$special = substr($special, 0, -1);
					
		$special = str_replace("++","+",$special);

	return strtolower($special);
}



function clean_url_specialchar($name) {
		$name = trim($name);
		$special = array('/','!','&','*','=',' ','+','?','(',')','%','.','"','\'','í','Í',',','bull;','`','–'); //here you can add as many char. you
		$to = array('+','+','','','+','+','+','','','','','','','','i','I','','','+','-');
		$special_url = str_replace($special,$to,trim($name));
		$special = str_replace("++","+",$special_url);
		$special = str_replace("++","+",$special);
		$special = str_replace("+-+","+",$special);
		$last = substr($special, -1);
		if($last == "+")
			$special = substr($special, 0, -1);
	return $special;
}
function clean_url_indulhatunk($name) {
	$special = array(
		'á',
		'í',
		'ű', 
		'ő',
		'ü',
		'ö',
		'ú',
		'ó',
		'é',
		'Á',
		'Í',
		'Ű', 
		'Ő',
		'Ü',
		'Ö',
		'Ú',
		'Ó',
		'É',
		'/',
		'!',
		'&',
		'*',
		'=',
		'?',
		'(',
		')',
		'%',
		'.',
		'"',
		'\'',
		',',
		';',
		'`',
		' ',
		'+',
		':',
		'\'',
		'bull',
		'#',
		']',
		'[',
		'û',
		'Õ',
		'Č',
		'Š',
		'ý',
		'č',
		'ä',
		'ß',
		'ø',
		'æ',
		'õ',
		'ñ',
		'à',
		'Å',
		
		'à',
		'â',
		'ã',
		'ä',
		'ç',
		'è',
		'ê',
		'ë',
		'ì',
		'î',
		'ï',
		'ñ',
		'ò',
		'ô',
		'õ',
		'ù',
		'ú',
		'û',
		'ü',
		'ý',
		'ÿ',
		'À',
		'Â',
		'Ã',
		'Ä',
		'Ç',
		'È',
		'Ê',
		'Ë',
		'Ì',
		'Í',
		'Î',
		'Ï',
		'Ñ',
		'Ò',
		'Ó',
		'Ô',
		'Ù',
		'Û',
		'Ý',
		
		
'ă',
'Ă',
'â',
'Â',
'î',
'Î',
'ș',
'Ș',
'ț',
'Ț',
'î',
'ă',
'ş',
'Ş',
'ţ',
'Ţ',	
'ş',
'–',

		); 
		
		$to = array(
		'a',
		'i',
		'u', 
		'o',
		'u',
		'o',
		'u',
		'o',
		'e',
		'A',
		'I',
		'U', 
		'O',
		'U',
		'O',
		'U',
		'O',
		'E',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-bull',
		'-',
		'-',
		'-',
		'-',
		'-',
		'C',
		'S',
		'y',
		'c',
		'a',
		'ss',
		'o',
		'ae',
		'o',
		'n',
		'a',
		'A',
		'a',
'A',
'i',
'I',
's',
's',
't',
'T',
'i',
'a',
's',
'S',
't',
'T',
's',
'-',
		);
		$special = str_replace($special,$to,trim($name));
		$special = str_replace("-----","-",$special);
		$special = str_replace("----","-",$special);
		$special = str_replace("---","-",$special);
		$special = str_replace("--","-",$special);
		
		
		    $table = array(
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Ă'=>'A', 'Ā'=>'A', 'Ą'=>'A', 'Æ'=>'A', 'Ǽ'=>'A',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'ă'=>'a', 'ā'=>'a', 'ą'=>'a', 'æ'=>'a', 'ǽ'=>'a',

        'Þ'=>'B', 'þ'=>'b', 'ß'=>'Ss',

        'Ç'=>'C', 'Č'=>'C', 'Ć'=>'C', 'Ĉ'=>'C', 'Ċ'=>'C',
        'ç'=>'c', 'č'=>'c', 'ć'=>'c', 'ĉ'=>'c', 'ċ'=>'c',

        'Đ'=>'Dj', 'Ď'=>'D', 'Đ'=>'D',
        'đ'=>'dj', 'ď'=>'d',

        'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ĕ'=>'E', 'Ē'=>'E', 'Ę'=>'E', 'Ė'=>'E',
        'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'ę'=>'e', 'ė'=>'e',

        'Ĝ'=>'G', 'Ğ'=>'G', 'Ġ'=>'G', 'Ģ'=>'G',
        'ĝ'=>'g', 'ğ'=>'g', 'ġ'=>'g', 'ģ'=>'g',

        'Ĥ'=>'H', 'Ħ'=>'H',
        'ĥ'=>'h', 'ħ'=>'h',

        'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'İ'=>'I', 'Ĩ'=>'I', 'Ī'=>'I', 'Ĭ'=>'I', 'Į'=>'I',
        'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'į'=>'i', 'ĩ'=>'i', 'ī'=>'i', 'ĭ'=>'i', 'ı'=>'i',

        'Ĵ'=>'J',
        'ĵ'=>'j',

        'Ķ'=>'K',
        'ķ'=>'k', 'ĸ'=>'k',

        'Ĺ'=>'L', 'Ļ'=>'L', 'Ľ'=>'L', 'Ŀ'=>'L', 'Ł'=>'L',
        'ĺ'=>'l', 'ļ'=>'l', 'ľ'=>'l', 'ŀ'=>'l', 'ł'=>'l',

        'Ñ'=>'N', 'Ń'=>'N', 'Ň'=>'N', 'Ņ'=>'N', 'Ŋ'=>'N',
        'ñ'=>'n', 'ń'=>'n', 'ň'=>'n', 'ņ'=>'n', 'ŋ'=>'n', 'ŉ'=>'n',

        'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ō'=>'O', 'Ŏ'=>'O', 'Ő'=>'O', 'Œ'=>'O',
        'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ō'=>'o', 'ŏ'=>'o', 'ő'=>'o', 'œ'=>'o', 'ð'=>'o',

        'Ŕ'=>'R', 'Ř'=>'R',
        'ŕ'=>'r', 'ř'=>'r', 'ŗ'=>'r',

        'Š'=>'S', 'Ŝ'=>'S', 'Ś'=>'S', 'Ş'=>'S',
        'š'=>'s', 'ŝ'=>'s', 'ś'=>'s', 'ş'=>'s',

        'Ŧ'=>'T', 'Ţ'=>'T', 'Ť'=>'T',
        'ŧ'=>'t', 'ţ'=>'t', 'ť'=>'t',

        'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ũ'=>'U', 'Ū'=>'U', 'Ŭ'=>'U', 'Ů'=>'U', 'Ű'=>'U', 'Ų'=>'U',
        'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ũ'=>'u', 'ū'=>'u', 'ŭ'=>'u', 'ů'=>'u', 'ű'=>'u', 'ų'=>'u',

        'Ŵ'=>'W', 'Ẁ'=>'W', 'Ẃ'=>'W', 'Ẅ'=>'W',
        'ŵ'=>'w', 'ẁ'=>'w', 'ẃ'=>'w', 'ẅ'=>'w',

        'Ý'=>'Y', 'Ÿ'=>'Y', 'Ŷ'=>'Y',
        'ý'=>'y', 'ÿ'=>'y', 'ŷ'=>'y',

        'Ž'=>'Z', 'Ź'=>'Z', 'Ż'=>'Z', 'Ž'=>'Z',
        'ž'=>'z', 'ź'=>'z', 'ż'=>'z', 'ž'=>'z',

        '“'=>'"', '”'=>'"', '‘'=>"'", '’'=>"'", '•'=>'-', '…'=>'...', '—'=>'-', '–'=>'-', '¿'=>'?', '¡'=>'!', '°'=>' degrees ',
        '¼'=>' 1/4 ', '½'=>' 1/2 ', '¾'=>' 3/4 ', '⅓'=>' 1/3 ', '⅔'=>' 2/3 ', '⅛'=>' 1/8 ', '⅜'=>' 3/8 ', '⅝'=>' 5/8 ', '⅞'=>' 7/8 ',
        '÷'=>' divided by ', '×'=>' times ', '±'=>' plus-minus ', '√'=>' square root ', '∞'=>' infinity ',
        '≈'=>' almost equal to ', '≠'=>' not equal to ', '≡'=>' identical to ', '≤'=>' less than or equal to ', '≥'=>' greater than or equal to ',
        '←'=>' left ', '→'=>' right ', '↑'=>' up ', '↓'=>' down ', '↔'=>' left and right ', '↕'=>' up and down ',
        '℅'=>' care of ', '℮' => ' estimated ',
        'Ω'=>' ohm ',
        '♀'=>' female ', '♂'=>' male ',
        '©'=>' Copyright ', '®'=>' Registered ', '™' =>' Trademark ',
    );

    $special = strtr($special, $table);
    
		/*$special = str_replace("++","+",$special);
		$special = str_replace("+-+","+",$special);
		$special = str_replace("","+",$special);
		*/
		$last = substr($special, -1);
		if($last == "-")
			$special = substr($special, 0, -1);
			
	return strtolower($special);

}



function clean_url($name) {
		$special = array('/','!','&','*','=',' ','+','?','(',')','%','.','"','\'','í','Í',',','bull;','`','ă','Ă','â','Â','î','Î','ș','Ș','ț','Ț','î','ă',); //here you can add as many char. you
		$to = array('+','+','','','+','+','+','','','','','','','','i','I','','','+','a','A','a','A','i','I','s','S','t','T','i','a');
		$special_url = str_replace($special,$to,trim($name));
		$special = str_replace("++","+",$special_url);
		$special = str_replace("++","+",$special);
		$special = str_replace("+-+","+",$special);
		$last = substr($special, -1);
		if($last == "+")
			$special = substr($special, 0, -1);
	return $special;
}
/**/
function clean_url_licit($name) {
		$special = array(
		'á',
		'í',
		'ű', 
		'ő',
		'ü',
		'ö',
		'ú',
		'ó',
		'é',
		'Á',
		'Í',
		'Ű', 
		'Ő',
		'Ü',
		'Ö',
		'Ú',
		'Ó',
		'É',
		'/',
		'!',
		'&',
		'*',
		'=',
		'?',
		'(',
		')',
		'%',
		'.',
		'"',
		'\'',
		',',
		';',
		'`',
		' ',
		'+',
		':',
		'\'',
		'bull',
		'#',
		']',
		'[',
		'û',
		'Õ',
		); 
		
		$to = array(
		'a',
		'i',
		'u', 
		'o',
		'u',
		'o',
		'u',
		'o',
		'e',
		'A',
		'I',
		'U', 
		'O',
		'U',
		'O',
		'U',
		'O',
		'E',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+',
		'+bull',
		'+',
		'+',
		'+',
		'+',
		'+',
		);
		$special = str_replace($special,$to,trim($name));
		$special = str_replace("+++++","+",$special);
		$special = str_replace("++++","+",$special);
		$special = str_replace("+++","+",$special);
		$special = str_replace("++","+",$special);
		
		/*$special = str_replace("++","+",$special);
		$special = str_replace("+++","+",$special);
		$special = str_replace("","+",$special);
		*/
		$last = substr($special, +1);
		if($last == "+")
			$special = substr($special, 0, +1);
			
	return strtolower($special);
}


function clean_url2($name) {
		$special = array(
		'á',
		'í',
		'ű', 
		'ő',
		'ü',
		'ö',
		'ú',
		'ó',
		'é',
		'Á',
		'Í',
		'Ű', 
		'Ő',
		'Ü',
		'Ö',
		'Ú',
		'Ó',
		'É',
		'/',
		'!',
		'&',
		'*',
		'=',
		'?',
		'(',
		')',
		'%',
		'.',
		'"',
		'\'',
		',',
		';',
		'`',
		' ',
		'+',
		':',
		'\'',
		'bull',
		'#',
		']',
		'[',
		'û',
		'Õ',
		
		'à',
		'â',
		'ã',
		'ä',
		'ç',
		'è',
		'ê',
		'ë',
		'ì',
		'î',
		'ï',
		'ñ',
		'ò',
		'ô',
		'õ',
		'ù',
		'ú',
		'û',
		'ü',
		'ý',
		'ÿ',
		'À',
		'Â',
		'Ã',
		'Ä',
		'Ç',
		'È',
		'Ê',
		'Ë',
		'Ì',
		'Í',
		'Î',
		'Ï',
		'Ñ',
		'Ò',
		'Ó',
		'Ô',
		'Ù',
		'Û',
		'Ý',
		'ă',
'Ă',
'â',
'Â',
'î',
'Î',
'ș',
'Ș',
'ț',
'Ț',
'î',
'ă',
'ş',
'Ş',
'ţ',
'Ţ',	
		
		); 
		
		$to = array(
		'a',
		'i',
		'u', 
		'o',
		'u',
		'o',
		'u',
		'o',
		'e',
		'A',
		'I',
		'U', 
		'O',
		'U',
		'O',
		'U',
		'O',
		'E',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-',
		'-bull',
		'-',
		'-',
		'-',
		'-',
		'-',
		
		'a',
		'a',
		'a',
		'a',
		'c',
		'e',
		'e',
		'e',
		'i',
		'i',
		'i',
		'n',
		'o',
		'o',
		'o',
		'u',
		'u',
		'u',
		'u',
		'y',
		'y',
		'A',
		'A',
		'A',
		'A',
		'C',
		'E',
		'E',
		'E',
		'I',
		'I',
		'I',
		'I',
		'N',
		'O',
		'O',
		'O',
		'U',
		'U',
		'Y',
		'a',
'A',
'a',
'A',
'i',
'I',
's',
'S',
't',
'T',
'i',
'a',
's',
'S',
't',
'T',			);
		$special = str_replace($special,$to,trim($name));
		$special = str_replace("-----","-",$special);
		$special = str_replace("----","-",$special);
		$special = str_replace("---","-",$special);
		$special = str_replace("--","-",$special);
		
		/*$special = str_replace("++","+",$special);
		$special = str_replace("+-+","+",$special);
		$special = str_replace("","+",$special);
		*/
		$last = substr($special, -1);
		if($last == "-")
			$special = substr($special, 0, -1);
			
	return strtolower($special);
}
/* .121 generate transfer file*/
function generateTransferFile($company = 'indulhatunk',$items = array(),$comment = '')
{
	if($company == 'indulhatunk')
	{
		$tax_no = "14960251";
		$account = "116000060000000041873494";
		$company_name = "Indulhatunk.hu kft";
	}
	elseif($company == 'hoteloutlet')
	{
		$tax_no = "23417555";
		$account = "116000060000000049170300";
		$company_name = "Hotel Outlet kft";
	}
	elseif($company == 'indusz')
	{
		$tax_no = "24856850";
		$account = "116000060000000065875957";
		$company_name = "Indulhatunk utazasszervezo kft";
	}
	
	/***transfer header, foter *////

	$date = date("Ymd");
	$code = "PEA";
	
	$from_company = str_pad($company_name, 35, " ", STR_PAD_RIGHT); 
	$comment_main = str_pad("$week heti elszamolas", 70, " ", STR_PAD_RIGHT); 
	
	$head.= "01ATUTAL0A$tax_no    ".$date."0002".$account."".$date."".$code."".$from_company."".$comment_main."\r\n";
	
	$sum = 0;
	
	//rows
		$i = 1;
		foreach($items as $item)
		{
			$value = round($item[value]);
	
			$totalinv = str_pad($value, 10, "0", STR_PAD_LEFT);
			$inner_id = str_pad($i, 6, "0", STR_PAD_LEFT); 
			$account_no = str_replace("-",'',trim($item[account_no]));
			$partner_id = str_pad(substr(trim($item[partner_id]),0,22), 24, " ", STR_PAD_RIGHT); 
			$partner_name = str_pad(substr(removeSpecialChars(trim($item[partner_name])),0,32), 35, " ", STR_PAD_RIGHT); 
			$partner_address = str_pad(substr(trim(removeSpecialChars($item[partner_address])),0,32), 35, " ", STR_PAD_RIGHT); 
			$cmnt = str_pad(substr($item[comment],0,65), 70, " ", STR_PAD_RIGHT); 

			$sum = $sum+$value;
			
			
			$transactions.="02".$inner_id."00000000".$totalinv."".$account_no."".$partner_id."".$partner_name."".$partner_address."".$partner_name."".$cmnt."\r\n";	
			$i++;
		}		
		
	$count = str_pad(count($items), 6, "0", STR_PAD_LEFT); 
	$totaltransfer = str_pad($sum, 16, "0", STR_PAD_LEFT); 
	$foot= "03".$count."".$totaltransfer;

	$transfer =  $head;
	$transfer.= $transactions;
	$transfer.= $foot."\r\n";

	return $transfer;

}



function getForeignAccount($invoice_number, $year, $pid) { 

	global $mysql;
	
$part = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$pid' LIMIT 1"));
	
		
		$totalitems = 0;
		$totalcur = 0;
		$g = 1;
		/******/

if($part[is_amex] == 0)
	$amexquestion = "Can we pay with AMEX?<br/><br/>";
	
	
$data = "<link rel='stylesheet' href='/inc/style.css' type='text/css' media='all' />";

$data.= "<div style='width:1000px;'>
	Dear $part[contact],<br/><br/>

According to our records, we have the following unpaid bookings.<br/><br/>
If it's correct, please send us the invoice of these bookings. If not, please let us know as soon as possible.<br/><br/>

Please ensure, that you put the following reference number on your invoice: <b>$year/".str_pad($invoice_number, 5, "0", STR_PAD_LEFT)."</b><br/><br/>


";
		
		
		
			$data.= "<table border='1'>
			<tr class='header'><td colspan='33'>$part[hotel_name] - $year/".str_pad($invoice_number, 5, "0", STR_PAD_LEFT)."</td></tr>

			<tr class='header'>";
				$data.= "<td></td>";
				$data.= "<td>Paid</td>";
				$data.= "<td>Transfer</td>";
				//$data.= "<td></td>";
				$data.= "<td></td>";
				$data.= "<td></td>";
				$data.= "<td></td>";
				$data.= "<td>Weekend</td>";
				$data.= "<td>HB</td>";
				$data.= "<td>Child 1</td>";
				$data.= "<td>Child 2</td>";
				$data.= "<td>Child 3</td>";
				$data.= "<td>Extra day</td>";
				$data.= "<td>Plus bed</td>";
				$data.= "<td>$arr[en_plus_other1_name]</td>";
				$data.= "<td>$arr[en_plus_other2_name]</td>";
				$data.= "<td>$arr[en_plus_other3_name]</td>";
				
				$data.= "<td>$arr[en_plus_other4_name]</td>";
				$data.= "<td>$arr[en_plus_other5_name]</td>";
				$data.= "<td>$arr[en_plus_other6_name]</td>";
				
				$data.= "<td>$arr[en_plus_room1_name]</td>";
				$data.= "<td>$arr[en_plus_room2_name]</td>";
				$data.= "<td>$arr[en_plus_room3_name]</td>";
				
				$data.= "<td>$arr[en_plus_single1_name]</td>";
				$data.= "<td>$arr[en_plus_single1_name]</td>";
				$data.= "<td>$arr[en_plus_single1_name]</td>";


				$data.= "<td>Total</td>";
			$data.= "</tr>";
			
			
			
			
		/******/
			$customers = $mysql->query("SELECT * FROM customers WHERE pid = $pid  AND foreign_invoice_number = '$invoice_number' AND year(foreign_invoice_date) = $year ORDER BY sub_pid, paid_date ASC");
			while($a = mysql_fetch_assoc($customers))
			{

				$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $a[sub_pid]"));

				$arr = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = $a[offers_id]"));
				//$data.=  "<tr><td>$a[offer_id] $a[orig_price]</td></tr>";
				
					$ch1 = 0;
				$ch2 = 0;
				$ch3 = 0;
				$hb = 0;
				$wk = 0;
				$pb = 0;
				$pplus = 0;
				$pplus2 = 0;
				$pplus3 = 0;
				$pplus4 = 0;
				$pplus5 = 0;
				$pplus6 = 0;
				$rplus1 = 0;
				$rplus2 = 0;
				$rplus3 = 0;
				$plusone = 0;
				$foreign = 0;
				$foreign = $arr[fc_actual_price];
				
				if($a[plus_child1_value] > 0)
				{	
					if($a[plus_child1_value] == $arr[plus_child1_value])
					{
						$ch1 = $arr[fc_plus_child1_value];
					}
					elseif($a[plus_child1_value] == $arr[plus_child2_value])
					{
						$ch1 = $arr[fc_plus_child2_value];
					}
					elseif($a[plus_child1_value] == $arr[plus_child3_value])
					{
						$ch1 = $arr[fc_plus_child3_value];
					}
					
					$foreign = $foreign + $ch1;
				}
				if($a[plus_child2_value] > 0)
				{
										if($a[plus_child2_value] == $arr[plus_child1_value])
					{
						$ch2 = $arr[fc_plus_child1_value];
					}
					elseif($a[plus_child2_value] == $arr[plus_child2_value])
					{
					
						$ch2 = $arr[fc_plus_child2_value];
					}
					elseif($a[plus_child2_value] == $arr[plus_child3_value])
					{
						$ch2 = $arr[fc_plus_child3_value];
					}
					
					$foreign = $foreign + $ch2;
				}
				if($a[plus_child3_value] > 0)
				{
					if($a[plus_child3_value] == $arr[plus_child1_value])
					{
						$ch3 = $arr[fc_plus_child1_value];
					}
					elseif($a[plus_child3_value] == $arr[plus_child2_value])
					{
						$ch3 = $arr[fc_plus_child2_value];
					}
					elseif($a[plus_child3_value] == $arr[plus_child3_value])
					{
						$ch3 = $arr[fc_plus_child3_value];
					}
					
					$foreign = $foreign + $ch3;
				}
				if($a[plus_weekend_plus] > 0)
				{
					$foreign = $foreign + $arr[fc_plus_weekend_plus];
					$wk = $arr[fc_plus_weekend_plus];
				}
				if($a[plus_half_board] > 0)
				{
					$foreign = $foreign + $arr[fc_plus_half_board];
					$hb = $arr[fc_plus_half_board];
				}
			
				if($a[plus_bed] > 0)
				{
					$foreign = $foreign + $arr[fc_plus_bed];
					$pb = $arr[fc_plus_bed];
				}
				
				if($a[plus_other1_value] > 0)
				{
					$foreign = $foreign + $arr[fc_plus_other1_value];
					$pplus = $arr[fc_plus_other1_value];
				}
			
			
			if($a[plus_other2_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_other2_value];
				$pplus2 = $arr[fc_plus_other2_value];
			}
			if($a[plus_other3_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_other3_value];
				$pplus3 = $arr[fc_plus_other3_value];
			}
			if($a[plus_other4_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_other4_value];
				$pplus4 = $arr[fc_plus_other4_value];
			}
			if($a[plus_other5_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_other5_value];
				$pplus5 = $arr[fc_plus_other5_value];
			}
			if($a[plus_other6_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_other6_value];
				$pplus6 = $arr[fc_plus_other6_value];
			}
			
			if($a[plus_room1_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_room1_value];
				$rplus1 = $arr[fc_plus_room1_value];
			}
			
			if($a[plus_room2_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_room2_value];
				$rplus2 = $arr[fc_plus_room2_value];
			}
			
			if($a[plus_room3_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_room3_value];
				$rplus3 = $arr[fc_plus_room3_value];
			}


			if($a[plus_single1_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_single1_value];
				$splus1 = $arr[fc_plus_single1_value];
			}
			
			if($a[plus_single2_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_single2_value];
				$splus2 = $arr[fc_plus_single2_value];
			}
			
			if($a[plus_single3_value] > 0)
			{
				$foreign = $foreign + $arr[fc_plus_single3_value];
				$splus3 = $arr[fc_plus_single3_value];
			}





					
				if($a[plus_days] > 0)
				{
					$foreign = $foreign + (($arr[fc_actual_price]+$hb+$ch1+$ch2+$ch3+$pb+$rplus1+$rplus2+$rplus3+$pplus+$pplus2+$pplus3+$pplus4+$pplus5+$pplus6)/$arr[days]);
					$plusone = (($arr[fc_actual_price]+$hb+$ch1+$ch2+$ch3+$pb+$rplus1+$rplus2+$rplus3+$pplus+$pplus2+$pplus3+$pplus4+$pplus5+$pplus6)/$arr[days]);
				//	$data.= "$arr[fc_actual_price]+$hb+$ch1+$ch2+$ch3+$pb+$rplus1+$rplus2+$rplus3+$pplus+$pplus2+$pplus3+$pplus4+$pplus5+$pplus6)/$arr[days]<hr/>";
				}
					
			
			
			
			//ormatPrice($price,$noformat = 0,$nbsp = 0, $currency = 'Ft',$dash = 0)
			$add = explode(" ",$a[paid_date]);
			$tr = explode(" ",$a[foreign_paid_date]);
			
			if($tr[0] == '0000-00-00')
			{
				$tr[0] = '-';
				$trclass = '';
			}
			else
				$trclass = 'yellow';
			
			if($a[pid] == 3112)	
			{
				
				//$data.= "UPDATE customers SET foreign_paid_date = NOW() WHERE cid = $a[cid];<hr/>";
			}
		
			if($a[inactive] == 1)
			{
				$deleted = "[CANCELLED]";
				$foreign = 0;
			}
			else
			{
				$deleted = "";
			}
				
				$data.= "<tr class='$trclass'>";
					$data.= "<td width='20' align='center'>$g.</td>";
					$data.= "<td width='100' align='center'>$add[0]</td>";
					$data.= "<td width='80' align='center'>$tr[0]</td>";
					$data.= "<td width='80'><b>$a[offer_id]</b></td>";
					$data.= "<td width='80'><b>$offer[hotel_name]</b></td>";

					$data.= "<td width='80'><b>$deleted $a[name]</b></td>";
				//	$data.= "<td align='right'>".formatPrice($a[orig_price],0,1)."</td>";
					$data.= "<td align='right'>".formatPrice($wk,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($hb,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($ch1,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($ch2,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($ch3,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($plusone,0,1,$arr[currency],1,2)."</td>";

					$data.= "<td align='right'>".formatPrice($pb,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($pplus,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($pplus2,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($pplus3,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($pplus4,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($pplus5,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($pplus6,0,1,$arr[currency],1,2)."</td>";
					
					$data.= "<td align='right'>".formatPrice($rplus1,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($rplus2,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($rplus3,0,1,$arr[currency],1,2)."</td>";
					
					
					$data.= "<td align='right'>".formatPrice($splus1,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($splus2,0,1,$arr[currency],1,2)."</td>";
					$data.= "<td align='right'>".formatPrice($splus3,0,1,$arr[currency],1,2)."</td>";
					


					$data.= "<td align='right' class='grey'>".formatPrice($foreign,0,1,$arr[currency],1,2)."</td>";
	
			$data.= "</tr>";
			$g++;
			
			$totalcur = $totalcur +$foreign; 
			
			$tc[$arr[currency]] = $tc[$arr[currency]] + $foreign;
			$totalitems++;
			}
	
	
	foreach($tc as $curr => $val)
		$data.= "<tr class='header'><td colspan='18' align='left'>Total ($curr)</td><td align='right' colspan='9'>".formatPrice($val,1)." $curr</td></tr>";

		$data.= "</table></div></div>";

$data.= "
<br/><br/>
$amexquestion

Thank you for your cooperation in advance.<br/><br/>

Yours faithfully,<br/>

Miklos Szabo<br/>
SzállásOutlet Kft.<br/>
<a href='mailto:miklos.szabo@indulhatunk.hu'>miklos.szabo@indulhatunk.hu</a>

</div>";

	return $data;
}


function num_files($partnerid,$offerid,$type) 
{ 
	$dir = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/$type/$partnerid/$offerid/orig/";
	//get all image files with a .jpg extension.
	$images = glob($dir . "*.jpg");
	//print each file name
	$z=0;
	foreach($images as $image)
	{
		$size = filesize($image);
		
		if($size > 0)
		{
			$z++;
		}	
	//	echo "$image >>> $size\n\n";
		
	}

	return $z;
    //return count(glob("/var/www/hosts/pihenni.hu/htdocs/static.pihenni/$type/$partnerid/$offerid/orig/*")); 
}


 function imageCreateCorners($sourceImageFile, $radius,$percent='',$color='red',$simple = 0) {
    # test source image
    if (file_exists($sourceImageFile)) {
      $res = is_array($info = getimagesize($sourceImageFile));
      } 
    else 
    {
	    
    }
    # open image
    if ($res) {
      $w = $info[0];
      $h = $info[1];
      switch ($info['mime']) {
        case 'image/jpeg': $src = imagecreatefromjpeg($sourceImageFile);
          break;
        case 'image/gif': $src = imagecreatefromgif($sourceImageFile);
          break;
        case 'image/png': $src = imagecreatefrompng($sourceImageFile);
          break;
        default: 
          $res = false;
        }
      
      
      if($simple == 0)
      {
       $watermark = imagecreatefrompng('./images/'.$color.'dot.png');
     //  $percent = 33;
       $white = imagecolorallocate($src,0xFF,0xFF,0xFF);

       imagecopy($src, $watermark, -20, -20, 0, 0, 92, 92); 
   imagettftext($src, 13, 0, 5, 35, $white, "./inc/fonts/t.ttf", "-$percent%");
  	 	}


      }

    # create corners
    if ($res) {

      $q = 10; # change this if you want
      $radius *= $q;

      # find unique color
      do {
        $r = rand(0, 255);
        $g = rand(0, 255);
        $b = rand(0, 255);
        }
      while (imagecolorexact($src, $r, $g, $b) < 0);

      $nw = $w*$q;
      $nh = $h*$q;

      $img = imagecreatetruecolor($nw, $nh);
      $alphacolor = imagecolorallocatealpha($img, $r, $g, $b, 127);
      imagealphablending($img, false);
      
      




      imagesavealpha($img, true);
/*
      $dot = imagecreatefrompng('./images/reddot.png');
      imagecopy($img, $dot, 0, 0, 0, 0, 100, 100);
*/


      imagefilledrectangle($img, 0, 0, $nw, $nh, $alphacolor);

      imagefill($img, 0, 0, $alphacolor);
      imagecopyresampled($img, $src, 0, 0, 0, 0, $nw, $nh, $w, $h);

      imagearc($img, $radius-1, $radius-1, $radius*2, $radius*2, 180, 270, $alphacolor);
      imagefilltoborder($img, 0, 0, $alphacolor, $alphacolor);
      imagearc($img, $nw-$radius, $radius-1, $radius*2, $radius*2, 270, 0, $alphacolor);
      imagefilltoborder($img, $nw-1, 0, $alphacolor, $alphacolor);
      imagearc($img, $radius-1, $nh-$radius, $radius*2, $radius*2, 90, 180, $alphacolor);
      imagefilltoborder($img, 0, $nh-1, $alphacolor, $alphacolor);
      imagearc($img, $nw-$radius, $nh-$radius, $radius*2, $radius*2, 0, 90, $alphacolor);
      imagefilltoborder($img, $nw-1, $nh-1, $alphacolor, $alphacolor);
      imagealphablending($img, true);
      imagecolortransparent($img, $alphacolor);




      # resize image down
      $dest = imagecreatetruecolor($w, $h);
      imagealphablending($dest, false);
      imagesavealpha($dest, true);
      imagefilledrectangle($dest, 0, 0, $w, $h, $alphacolor);
      imagecopyresampled($dest, $img, 0, 0, 0, 0, $w, $h, $nw, $nh);





	 
 /**/
 
 
      # output image
      $res = $dest;
      imagedestroy($src);
      imagedestroy($img);
      }

    return $res;
    }
function head_customers_new($cid = 0)
{
	global $lang;
	global $mysql;
	
	$offerArr = mysql_fetch_assoc($mysql->query("SELECT * FROM customers WHERE cid = '$cid' LIMIT 1"));
	

	$off = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = '$offerArr[offers_id]' LIMIT 1"));


if($offerArr[sub_pid] > 0)
{
	$partnerQuery = $mysql->query("SELECT * FROM partners WHERE hotel_name <> ''AND pid = '$offerArr[sub_pid]' LIMIT 1");
	$partnerArr = mysql_fetch_assoc($partnerQuery);

}
else
{
	$partnerQuery = $mysql->query("SELECT * FROM partners WHERE hotel_name <> ''AND pid = '$offerArr[pid]' LIMIT 1");
	$partnerArr = mysql_fetch_assoc($partnerQuery);
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <html xmlns="http://www.w3.org/1999/xhtml" lang="hu-HU"> 
<head>
	<!--<link rel="stylesheet" type="text/css" media="all"   href="/inc/customers.css"/>-->
	<script src="/inc/js/jquery.js" type="text/javascript"></script>
	<script src="/inc/js/facebox.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.mask.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.watermark.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.numeric.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.date.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.datepicker.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.stickyscroll.js" type="text/javascript"></script>
		<script src="/inc/js/image-picker.js" type="text/javascript"></script>

	<script src="/inc/js/customer.js" type="text/javascript"></script>
	
	
	
	<link href="/inc/jquery.fancybox.css" media="screen" rel="stylesheet" type="text/css"/>
	<script src="/inc/js/jquery.fancybox.js" type="text/javascript"></script> 
	
	
	

<style>

body { padding:0; margin:0;  background:url('/images/bg_cs.jpg') top center no-repeat;  font-family:arial; font-size:12px;  text-align:center; color:#252525;}
	
	.final_methods a { color:#252525; text-decoration:none; }
	.alert { padding-top:10px !important;}
	
	.alert a { font-weight:bold; color:black; text-decoration:none; }
	#container { text-align:left; width:985px; ; margin:0 auto; } 
	
	#header { height:115px;  width:100%; border-bottom:1px solid #b8b8b6; margin:0 0 10px 0;}
	
	.map { margin:10px 0 0 0; width:293px; height:250px; border:1px solid #d9d9d9;}

	
	.hidden { display:none; }
		
	#footer { background-color:#eceae3; padding:20px; margin:10px 0 0 0; border-top:1px solid #b8b8b6; }
	#footer img{ margin:0 10px 0 10px;  } 	
	
	.welcome { font-size:24px; color:black; text-align:center;  padding:0 0 10px 0;  font-weight:bold; text-align:center; }
	
	
	.note { background-color:#ae2c01; border-radius:5px; text-align:center; padding:10px; font-weight:bold; color:white; margin:10px 0 10px 0; } 
	
	
	.partnerimage img { width:98px; margin:0 1px 1px 0;}
	
	.szeptitle { font-weight:bold; text-align:center; padding:0 0 10px 0;}
	.hiddenbox,.hiddenbox2,.hiddenbox3,.hiddenbox5,.info{ display:none; }
	
	.hiddenbox4 { display:none;}
	
	.long { float:left; }
	
	 ul { padding:0; margin:0; }
	 
	 ul li { padding:0; margin:0; list-style:none; } 
	 
	 label { display:block; width:200px; float:left; font-weight:bold;padding:6px 0 0 0; font-size:12px; }
	 
	 .cleaner { clear:both; }
	 
	 .leftTop {  cursor: hand; cursor: pointer; padding:10px 20px 10px 20px; margin:15px 0 0 0; font-weight:bold; font-size:16px; background:url('/images/down.jpg') no-repeat;background-position:950px 3px !important; background-color:#252525; color:white;   }
	 
	  .activetop {  background:url('/images/up.jpg')  no-repeat !important;  background-color:#af2c01 !important; background-position:950px 3px !important;}
 
 	 /*.leftTopOther { padding:5px; font-weight:bold; text-align:center; background-color:#E2EFB3; color:#90AB32; margin:10px 5px 0 5px; border:2px solid #90AB32;}

 	 .leftTopOther a { color:#90AB32; font-weight:bold; text-decoration:none; }
*/
	 .leftItem {  padding:10px 20px 10px 20px; background-color:#f3f3f3;  }
	 
	 
	 .righttop { margin:10px 0 0 0; color:white;font-weight:bold;font-size:14px; padding: 33px 0 0 20px; width:324px; height:33px; background:url('/images/rightheader.png'); }
	 
	 .righttopgrey { margin:10px 0 0 0; color:white;font-weight:bold;font-size:14px; padding: 30px 0 0 20px; width:324px; height:33px; background:url('/images/righttopgrey.png'); }
	 
	 .rightcontent { line-height:18px; padding:10px 20px 10px 25px; background:url('/images/rightcontent.png'); width:299px; }
	 	 .rightcontent a { color:#d53501; font-weight:normal;  }

	 .rightbottom { background:url('/images/rightfooter.png'); width:344px; height:21px;  }
	 
	  .rightbottomgrey { background:url('/images/rightbottomgrey.png'); width:344px; height:16px;  }
	 .rightcontentgrey { line-height:18px; padding:0px 30px 10px 21px; background:url('/images/rightcontentgrey.png'); width:293px; }

	 input[type=checkbox] { float:left; margin:6px; } 
	 
	 input[type=text] { border:1px solid #cacaca; font-size:14px; border-radius:2px; background-color:white; width:305px; padding:5px; margin:1px; } 
 
	 textarea { border:1px solid #cacaca; font-size:14px; border-radius:2px; background-color:white; font-family:arial; padding:3px; margin:1px;  width:260px;height:55px }
	 select { width:240px; }
	 
	 #zip, #invoice_zip { width:40px; }
	 
	 #city, #invoice_city { width:248px; }
	 .booking_info { width:245px; float:left;background-color:#6faa00;border:5px solid white; padding:10px; }
	 .booking_info a { color:white; text-transform:uppercase; text-decoration:none; }
	 .btitle { text-align:center; text-transform:uppercase; font-weight:bold;}
	/* #submit { background:url('/images/save.png'); width:145px; height:35px; margin:6px auto; display:block; padding:12px 0 0 0; text-align:center; text-decoration:none; color:white;  font-weight:bold; font-size:18px;} 
	 
	  #submit:hover { text-shadow:1px 1px #000;} 
	*/
    
    #leftSide { width:985px;  margin:0; color:black; padding:0px;   }	
    
    .rightside {}
    
    .total { border-top:2px solid #af2c01; margin:10px 0 0 0; font-size:16px; font-weight:bold;}
    
    .hiddeninfo { color:black; display:none }
    
    .bold{ font-weight:bold; font-size:12px; text-decoration:none; background-color:#e9e7e0;  }
    
    .bold a { font-weight:bold;}
    
    table td { padding:5px;}
    table { 
  border-collapse:collapse;
}

    .bold a { color:red }
    
    a img { border:none }
    
    .offertitle { color:#d53501; font-weight:bold; text-transform:uppercase; text-align:center; font-size:18px;  padding:10px 0 10px 0; }
    
    
/*** facebox ****/

#facebox {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 100;
  text-align: left;
}


#facebox .popup{
  position:relative;
  border:3px solid rgba(0,0,0,0);
  -webkit-border-radius:5px;
  -moz-border-radius:5px;
  border-radius:5px;
  -webkit-box-shadow:0 0 18px rgba(0,0,0,0.4);
  -moz-box-shadow:0 0 18px rgba(0,0,0,0.4);
  box-shadow:0 0 18px rgba(0,0,0,0.4);
}

#facebox .content {
  display:table;
  width: 370px;
  padding: 10px;
  background: #fff;
  -webkit-border-radius:4px;
  -moz-border-radius:4px;
  border-radius:4px;
}

#facebox .content > p:first-child{
  margin-top:0;
}
#facebox .content > p:last-child{
  margin-bottom:0;
}

#facebox .close{
  position:absolute;
  top:5px;
  right:5px;
  padding:2px;
  background:#fff;
}
#facebox .close img{
  opacity:0.3;
}
#facebox .close:hover img{
  opacity:1.0;
}

#facebox .loading {
  text-align: center;
}

#facebox .image {
  text-align: center;
}

#facebox img {
  border: 0;
  margin: 0;
}

#facebox_overlay {
  position: fixed;
  top: 0px;
  left: 0px;
  height:100%;
  width:100%;
}

.facebox_hide {
  z-index:-100;
}

.facebox_overlayBG {
  background-color: #000;
  z-index: 99;
}

.note2 { padding:10px 0 5px 0; font-size:11px;  margin:0px 0 10px 0; border-bottom:1px solid #b8b8b6; }
/*** facebox *****/
.offerid { font-size:18px; font-weight:bold; text-align:center; padding:10px 0 10px 0; }

.copyright { font-size:11px; padding:10px 0 0 0; width:920px; margin:10px auto; border-top:1px dotted #b8b8b6; }
.rightcontentgrey ul li a { width:259px; height:14px; display:block; border-bottom:1px dotted #362e2c; padding:10px 0 10px 45px; margin:0 0 5px 0; color:black; font-weight:bold; text-decoration:none; }

.extraprices { padding-top:10px;}
.extraprices label { width:250px;}

.moreitems a { color:black; text-decoration:none;}

#step2 { display:none;}
#step3 { display:none; }

.notetext { font-size:10px; padding:15px 10px 10px 10px }
	#cntop {  padding:10px; color:white; background-color:#af2c01;}
	.cnimage { float:left; width:190px;}
	.cnimage img { border:5px solid white;}
	.cninfo { float:left; padding:0 20px 0 20px; width:430px}
	.clear { clear:both; }
	.cntitle { text-transform:uppercase; font-size:25px; font-weight:normal;}
	.cninfo a { color:white; text-decoration:none;}
	.coffer { font-size:18px; padding:10px 0 0 0;}
	.cnmenu { background-color:#252525; padding:10px;}
	.cnmenu a { color:white; text-decoration:none; font-weight:normal; font-size:14px; }
	.cnmenu li { float:left; width:215px;padding:0 0px 0px 25px; background:url('/images/sbadge.jpg') no-repeat; background-position:0px -2px;}
	#content { background-color:white;}
	.welcometext { text-align:center;  }
	fieldset { border-radius:5px; width:920px; border:1px solid #cfcfcf;}
	legend { text-transform:uppercase; font-weight:bold; color:#af2c01; font-size:14px;}
	#invoicedatamore { display:none; }
	.nextbutton { display:block; width:144px; height:28px; font-size:14px; background:url('/images/obutton.png') no-repeat; color:white; text-transform:uppercase; font-weight:bold; text-align:center; text-decoration:none; padding:10px 0 0 0; margin:10px 0 0 800px;}
	.offer_id { font-size:18px; font-weight:bold; padding:0 0 10px 0; text-align:center;}
	.totals { font-weight:bold; font-size:18px; border-top:1px solid #afafaf; }
	.extraprices tr:hover { background-color:white;}
	.extraprices td {}
	.boldrow { font-weight:bold;}
	.invalid { border:1px solid red !important; }
	.booking_phone { padding:10px 0 0px 0; font-size:20px;text-align:center;font-weight:bold; }
	.booking_voucher { border-top:1px solid white; margin:5px; text-align:center;padding:5px 0 0 0; text-transform:uppercase; font-weight:bold; font-size:12px;}
	.booking_mail { text-align:center; font-weight:bold;  }
	.booking_address { padding:10px 0 10px 0;}
	.booking_voucher_id { text-align:center; font-weight:bold; padding:0px 0 0px 0; font-size:20px; }
	.tooltip{
   			display: inline;
    		position: relative;
		}
		
		.tooltip:hover:after{
    		background: #333;
    		background: rgba(0,0,0,.8);
    		border-radius: 5px;
    		bottom: 26px;
    		color: #fff;
    		content: attr(title);
    		left: 20%;
    		padding: 5px 15px;
    		position: absolute;
    		z-index: 98;
    		width: 220px;
		}
		
		.tooltip:hover:before{
    		border: solid;
    		border-color: #333 transparent;
    		border-width: 6px 6px 0 6px;
    		bottom: 20px;
    		content: "";
    		left: 50%;
    		position: absolute;
    		z-index: 99;
		}
	
	/* image picker */

ul.thumbnails.image_picker_selector {
  overflow: auto;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
  padding: 0px;
  margin: 0px; }
  ul.thumbnails.image_picker_selector ul {
   width:500px;
    list-style-image: none;
    list-style-position: outside;
    list-style-type: none;
    padding: 0px;
    margin: 0px; }
  ul.thumbnails.image_picker_selector li.group_title {
    float: none; }
  ul.thumbnails.image_picker_selector li {
    margin: 1px;
    clear:none !important;
    float: left !important;}
    ul.thumbnails.image_picker_selector li .thumbnail {
      padding: 3px;
      border: 1px solid #dddddd; }
    ul.thumbnails.image_picker_selector li .thumbnail.selected {
      background: #0088cc; }


/* image picker */

 <? if (strpos($offerArr[offer_id],'CZO-') !== false || strpos($offerArr[offer_id],'TVO-') !== false) {
	echo "#header{ display:none !important } #content { padding-top:10px; "; 
	 }?>
	
	</style>
	
	
	<script type="text/javascript" src="http://www.google.com/jsapi?key=AIzaSyAJ-aWUxJbmDE6JyBu558mRp76kfeAjmLk"></script>
	<script type="text/javascript" charset="utf-8">
         google.load("maps", "2.x");
         
         $(document).ready(function() {
        
		$(".leftTop").click(function(){
		
		 	var success = $("#c_success").val();
		 	
		 	if(success == 1)
		 	{
				var step = $(this).attr("pageid");
			
				$(".leftTop").removeClass('activetop');
				$("#title"+step).addClass('activetop');
				
				$(".leftItem").hide();
				$("#step"+step).fadeIn();
			}
			else
			{
				alert('<?=$lang[use_next]?>');				
			}
			
			
			return false;
			
		});
				
				
         	$(".nextbutton").click(function(){
         		
         		 
	         	var error = 0;
	         	
	         	$(".invalid").removeClass('invalid');
	         	
	         	var step = $(this).attr('step')*1;
	         	var stepminus = step - 1;
	         	
	         	var name = $("#c_name").val();
	         	var email = $("#c_email").val();
	         	var phone = $("#c_phone").val();
	         	var city = $("#invoice_city").val();
	         	var address = $("#address").val();
	           	var zip = $("#zip").val();
			   	var taxno = $("#taxno").val();
			   	var invname = $("#ct_invoice_name").val();
	         	
	         	var invoice_zip = $("#invoice_zip").val();
	         	var invoice_address = $("#invoice_address").val();
	         	var invoice_city = $("#city").val();
	         	
	         	if(stepminus == 1)
	         	{
				 	if(name == '')
				 	{
				 		$("#c_name").addClass('invalid');
				 		error = 1;
				 	}
				 	if(phone == '')
				 	{
				 		$("#c_phone").addClass('invalid');
				 		error = 1;
				 	}
				 	if(city == '')
				 	{
				 		$("#invoice_city").addClass('invalid');
				 		error = 1;
				 	}
				 	if(email == '')
				 	{
				 		$("#c_email").addClass('invalid');
				 		error = 1;
				 	}
				 	if(address == '')
				 	{
				 		$("#address").addClass('invalid');
				 		error = 1;
				 	}
				 	if(zip == '')
				 	{
				 		$("#zip").addClass('invalid');
				 		error = 1;
				 	}
				 	if(invname != '')
				 	{
					 	
					 	if(taxno == '')
					 	{
					 		$("#taxno").addClass('invalid');
					 		error = 1;
					 	}
					 	if(invoice_address == '')
					 	{
					 		$("#invoice_address").addClass('invalid');
					 		error = 1;
					 	}
					 	if(invoice_zip == '')
					 	{
					 		$("#invoice_zip").addClass('invalid');
					 		error = 1;
					 	}
					 	if(invoice_city == '')
					 	{
					 		$("#city").addClass('invalid');
					 		error = 1;
					 	}
				 	
					}	
			
				 }
				 else if(stepminus == 2)
				 {
				 
				 	
				 	
					 if($("#insurance_value").is(':checked'))
					 {
					 
					 var ins_name1 = $("#ins_name1").val();
				 	var ins_name2 = $("#ins_name1").val();
						 	
					 
					 if(ins_name1 == '')
					 {
						 $("#ins_name1").addClass('invalid');
					 		error = 1;
						 
					 }
					 if(ins_name2 == '')
					 {
						 $("#ins_name2").addClass('invalid');
					 		error = 1;
						 
					 }
					 
					 	
						if($("#insurance_accept").is(':checked'))
						{
							
						}
						else
						{
						 	alert("El kell fogadnia a biztosítás szerződési feltételeit!");
						 	error = 1;
						}
						 
					 }
				 }
				 
				 if(error == 0)
				 {	
				 	 $("#title"+stepminus).removeClass('activetop');
				 	 $("#title"+step).addClass('activetop');
					 $("#step"+stepminus).hide();
					 $("#step"+step).fadeIn();
					 
					 if(step == 3)
					 	$("#c_success").val(1);
					 	
					return false;
				 }
				 else
				 {
					 alert('<?=$lang[correct_fields]?>');				 
				 }
	         	return false;
	         		         	
         	});
         	
         	/****/
         	
   $(".newpayment").click(function () {
	   
	
	  
	  $('#shipments').html('');
	  var curval = $(this).val()*1 ;
	  
	if(curval == 1 || curval == 9)
	{
		$('#shipments').html("<?=$lang[shipment_normal]?>");
	}


	if(curval == 2)
	{
		$('#shipments').html("<?=$lang[shipment_special]?>");
		$('input:radio[name="shipment"]').filter('[value="2"]').attr('checked', true);
	}
	if($(this).val() == 10 || $(this).val() == 11 || $(this).val() == 12)
	{
		$('#shipments').html("<?=$lang[shipment_normal]?><?=$lang[shipment_special]?>");
	}
	
	});
         	/****/
         });
	</script>
    <title><?=$lang[final_title]?></title>
    
   
</head>
<body>
<div id='fb-root'></div>
<div id="container">
	<div id='header'></div>

	<div id="content">
	
	<!-- -->
	
		


<div id='cntop'>
	<div class='cnimage'><? echo"<a href='http://static.indulhatunk.hu/uploaded_images/accomodation/$partnerArr[coredb_id]/$partnerArr[coredb_package]/orig/01.jpg' class='fancy_gallery $hidden' rel='fancy_gallery' title='$partnerArr[hotel_name] $i/$partnerArr[image_num]'><img src='http://static.indulhatunk.hu/uploaded_images/accomodation/$partnerArr[coredb_id]/$partnerArr[coredb_package]/177_124/01.jpg'/></a>";?>
	</div>
	<div class='cninfo'>
	
	
			<div class='cntitle'><?=str_replace("|","",$partnerArr[hotel_name]);?></div>
			<div class='coffer'><?=$off[$lang['prefix']."name"]?></div>
			
			<div class='booking_address'><? echo "$partnerArr[zip] $partnerArr[city] $partnerArr[address]";?>	</div>

			<? if($off[special_voucher] <> 1 ){ ?>
			
			<div style='padding:10px 0 0 0 '>
			<?=$lang[reference_text1]?> <b><?=$editarr[offer_id]?></b><br/>
			<?=$lang[reference_text2]?>
			</div>
			<? } ?>


	</div>
	<div class='booking_info'>
	<div style='margin-top:-30px;margin-left:-1px;'><img src='/images/caps.png'/></div>
	
		<div class='btitle'><?=$lang['booking_info']?></div>
		
		
<? if (strpos($offerArr[offer_id],'CZO-') !== false) {
?>
	<div class='booking_phone'>+40 373 781 939
		</div>
		
		<div class='booking_mail'>
		<a href='mailto:booking@cazareoutlet.ro'>booking@cazareoutlet.ro</a>				
		</div>

<?}
elseif (strpos($offerArr[offer_id],'TVO-') !== false) {
?>
	<div class='booking_phone'>+421 908 093 577</div>
		
		<div class='booking_mail'>
		<a href='mailto:booking@hoteloutlet.sk'>booking@hoteloutlet.sk</a>				
		</div>
<?}else {?>
	<div class='booking_phone'><? echo "$partnerArr[reception_phone]";?>
		</div>
		
		<div class='booking_mail'>
		<a href='mailto:<?=$partnerArr[reception_email]?>'><?=$partnerArr[reception_email]?></a>				
		</div>
<? } ?>

	


		<div class='booking_voucher'><?=$lang[voucher_id]?></div>
		<div class='booking_voucher_id'> <b><? echo "$offerArr[offer_id]"?></b></div>

		
	
				
		
		</div>
	
	
	<div class='cleaner'></div>
</div>
	<div class='cnmenu'>
		<ul>
			<li><a href='#bookinginfo' rel="facebox" ><?=$lang[final_menu1]?></a></li>
			<li><a href='#szepinfo' rel="facebox" ><?=$lang[final_menu2]?></a></li>
			<li><a href='#wheniget' rel="facebox" ><?=$lang[final_menu3]?></a></li>
			<li><a href='#asagift' rel="facebox" ><?=$lang[final_menu4]?></a></li>
		</ul>
		<div class='cleaner'></div>
	</div>


<!-- -->

		<div id="bookinginfo" style="display:none">	
			<?=$lang[iwantbook]?>
			
			<? if($offer[special_voucher] <> 1 ){ ?>
				<?=str_replace("#OFFER_ID#",$offerArr[offer_id],$lang[iwantbook2])?>
			<? } ?>
		</div>
		<div id="szepinfo" style="display:none">	
			<?=$lang[szepcard]?>
		</div>

		<div id="wheniget" style="display:none">	
			<?=$lang[wheni]?>
		</div>

		<div id="asagift" style="display:none">	
	<?=$lang[giftok]?>
		</div>


<!-- -->

<? if($off[special_comment] <> '')
	echo '<div class="note" style="text-align:left;color:white;">'.nl2br($off[special_comment]).'</div>';
?>


<?
}

function removechars($text){
	$trans = array(
		"&aacute;"=>"á",
		"&eacute;"=>"é",
		"&iacute;"=>"í",
		"&ouml;"=>"ö",
		"&oacute;"=>"ó",
		"&ocirc;"=>"ő",
		"&uacute;"=>"ú",
		"&uuml;"=>"ü",
		"&ucirc;"=>"ű",
		"&Aacute;"=>"Á",
		"&Eacute;"=>"É",
		"&Iacute;"=>"Í",
		"&Ouml;"=>"Ö",
		"&Oacute;"=>"Ó",
		"&Ocirc;"=>"Ő",
		"&Uacute;"=>"Ú",
		"&Uuml;"=>"Ü",
		"&Ucirc;"=>"Ű",
		);
 
	return strtr($text, $trans);
}

    function time_ago($date)

{

if(empty($date)) {

return "No date provided";

}

$periods = array("másodperce", "perce", "órája", "napja", "hete", "hónapja", "éve");

$lengths = array("60","60","24","7","4.35","12","10");

$now = time();

$unix_date = strtotime($date);

// check validity of date

if(empty($unix_date)) {

return "Bad date";

}

// is it future date or past date

if($now > $unix_date) {

$difference = $now - $unix_date;

$tense = "";

} else {

$difference = $unix_date - $now;
$tense = "from now";}

for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {

$difference /= $lengths[$j];

}

$difference = round($difference);

if($difference != 1) {

$periods[$j].= "";

}

return "$difference $periods[$j]";

}


function round_five($num) {
    return round($num*2,-1)/2;
}


function isduplicatepartner($string)
{
	global $mysql_tour;
	
	$check = $mysql_tour->query("SELECT * FROM partner WHERE code = '$string' LIMIT 1");
	if(mysql_num_rows($check) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}

	
}




function generatePartnerID($company = '')
{
	global $mysql_tour;
	
	
	$abc= array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
	$num= array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");


	$string =strtoupper($abc[rand(0,25)]);
	$string.=strtoupper($abc[rand(0,25)]);
	$string.=strtoupper($abc[rand(0,25)]);
	
	if(isduplicatepartner($string) == true)
	{
		return generatePartnerID($company);
	}
	else
	{
		return $string;
	}
}


function getmode($offer_id)
{

	global $mysql_tour;
	
		$mode = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM `offer_property` 
INNER JOIN field_property_core_name ON field_property_core_name.field_property_id = offer_property.property_id 
INNER JOIN field_property_core ON field_property_core.id = offer_property.property_id
WHERE offer_id = $offer_id AND category_id = 1 LIMIT 1"));

	return $mode;

}

function gettourtype($offer_id)
{

	global $mysql_tour;
	global $_COOKIE;
		$mode = parsemysql("SELECT NAME FROM `offer_property` 
INNER JOIN field_property_core_name ON field_property_core_name.field_property_id = offer_property.property_id 
INNER JOIN field_property_core ON field_property_core.id = offer_property.property_id
WHERE offer_id = $offer_id AND category_id = 2 ");


		foreach($mode as $mdata => $val)
		{
			$m[$val[NAME]] = $val[NAME];
		}
		
	
		$mode = strtolower(implode(", ",$m));
		
		if($mode == '')
			$mode = '-';
	return $mode;

}

function sendsms($text = "",$phone, $processed = 0)
{
	global $mysql_sms;
	
	if(clearphone($phone) <> '')
	{
		$sms = array();
		$sms[processed] = $processed;
		$sms[number] = clearphone($phone);
		$sms[text] = encodesms($text);
		
		$mysql_sms->query_insert("outbox",$sms);
	}
}


function encodesms($string, $reverse = 0)
{

	$from = array("Á","Í","Ü","Ű","Ö","Ő","Ó","É","á","í","ü","ű","ö","ő","ó","é","Ú","ú","\n");	
	$to = array("Á","Í","Ü","Ű","Ö","Ő","Ó","É","á","í","ü","ű","ö","ő","ó","é","Ú","ú","\n");	
/*
//	$to = array("Ã€","ÃŒ","Ãœ","Ãœ","Ã–","Ã–","Ã’","Ã‰","Ã ","Ã¬","Ã¼","Ã¼","Ã¶","Ã¶","Ã²","Ã©","Ã™","Ã¹","
");	
*/	
	if($reverse == 1)
		return str_replace($to,$from,$string);
	else
		return $string;
		//return str_replace($from,$to,$string);
}

function clearphone($phone)
{
	$from = array(" ","-","+",'/','!','_',',','(',')','vagy',"\'");
	$to = array("","","","","",'',';','','',';','');
	
	
	$phone = trim(str_replace($from,$to,$phone));
	$phone = explode(";",$phone);
	$phone = $phone[0];
	
	$prefix = substr($phone, 0, 2);
	
	if($prefix =="06") {
		$phone = "36".substr($phone,2);
	}
	if($prefix =="00") {
		$phone = substr($phone,2);
	}
	
	if(($prefix == "20" || $prefix == "30" || $prefix == "70")&& strlen($phone) == 9)
	{
		$phone = "36".$phone;
	}
	
	$newprefix = substr($phone, 0, 2);
	$extraprefix = substr($phone, 0, 4);

	if(strlen($phone) <> 11 || $newprefix <> 36 || $extraprefix == '3690')
		$phone  = '';
	else
		$phone = '+'.$phone;
	
	return $phone;	
}

function array2csv($array) {
    $csv = array();
    foreach ($array as $item) {
        if (is_array($item)) {
            $csv[] = array2csv($item);
        } else {
            $csv[] = $item;
        }
    }
    return implode(',', $csv);
} 

function getCompany($company = 'szallasoutlet', $type = 'select', $has_outlet = 1)
{
	global $mysql;
	

	$companies = '';

	if($type == 'select')
	{
		if($has_outlet == 1)
			$extra = "AND has_outlet = 1";
		
		$query = $mysql->query("SELECT * FROM companies WHERE id > 0 $extra ORDER by id ASC");
		
		while($arr = mysql_fetch_assoc($query))
		{
			if($company == $arr['company_name'])
				$selected = 'selected';
			else
				$selected = '';
		
			$companies.= "<option value='".$arr['company_name']."' $selected>$arr[company]</option>";
		}
	}
	else
	{
		$companies = mysql_fetch_assoc($mysql->query("SELECT * FROM companies WHERE company_name = '$company' LIMIT 1"));
	}
	//print_r($companies); die();
	return $companies;
}

function insertProperty($offer_id,$property_id)
	{
		global $mysql_tour;

		$property = array();
		$property[property_id] = $property_id;
		$property[offer_id] = $offer_id;

		$mysql_tour->query_insert("offer_property", $property);


	}
	
	function getProtocol() {
	    if (isset($_SERVER['HTTPS']) &&
	        ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
	        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
	        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
	            $protocol = 'https://';
	        }
        else {
            $protocol = 'http://';
        }
        return $protocol;
	}
?>