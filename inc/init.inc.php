<?
if(isset($_COOKIE['username'])) {
    if($_COOKIE['username'] == 'gergely.solymosi') {
        //error_reporting(E_ALL);
        //ini_set('display_errors', 'On');
    }
}

if(!isset($noheader)) {
    header('Content-type: text/html; charset=utf-8');
}
include("config.inc.php");
include("mysql.class.php");
include("functions.inc.php");
include("recaptcha.class.php");
include("template.class.php");
include("amazon-ses.php");
include("diff.class.php");
include("mandrill/Mandrill.php");

session_start();


$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

if(isset($_COOKIE['language'])) {
    if($_COOKIE['language'] <> '')
    {
    	if(strlen($_COOKIE['language']) <> 2)
    		die;
    		
    	include("languages/".$_COOKIE['language'].".lang.php");
    }
    else
    {
    	include("languages/hu.lang.php");
    }
}
else
{
    include("languages/hu.lang.php");
}

$tmpstamp = microtime(true);

?>