$(document).ready(function() {

	$("#taxno").mask("99999999-9-99");
	
	/*
		function drawMap(result, map) {
						if (result.Status.code == G_GEO_SUCCESS) {
						var geocode = result.Placemark[0].Point.coordinates;

						var center = new GLatLng(geocode[1], geocode[0]);

						map.setCenter(center, 15);
						
						map.openInfoWindowHtml(center,"<b>Szállás Outlet</b><br/>1056 Budapest, Váci utca 9.<br/>21-es kapucsengő<br/><br/>");
                   
                   
						map.addOverlay(new GMarker(center));
						}
						}

						var geo = new GClientGeocoder();

						$('.map').each(
						function(n, elm) {
						var map = new GMap2($(elm).get(0));
						map.addControl(new GLargeMapControl3D());
		        map.addControl(new GMapTypeControl());

						geo.getLocations($(elm).attr('address'), function(result) {drawMap(result, map)});
						}
						);*/
		});
	
	
	
$(document).ready(function() {


$("input.numeric").numeric();
$(".maskeddate").mask("9999-99-99");
$("a.fancy_gallery").fancybox();
  
$('a[rel*=facebox]').facebox();
	 
function addCommas(nStr) {
	 nStr += '';
	 var x = nStr.split('.');
	 var x1 = x[0];
	 var x2 = x.length > 1 ? '.' + x[1] : '';
	 var rgx = /(\d+)(\d{3})/;
	 while (rgx.test(x1)) {
	    x1 = x1.replace(rgx, '$1' + ' ' + '$2');
	 }
	return x1 + x2;
}

function reCalculate()
{
	var dayNum = parseInt($(".days").val());
	var origPrice = parseInt($(".orig_price").val());
	var discountPrice = parseInt($(".discount_value").val());
	if($(".plus_half_board").is(':checked'))
		var half_board = $(".plus_half_board").val();
	else
		var half_board = 0;
	
	if($(".plus_weekend_plus").is(':checked'))
		var weekend = $(".plus_weekend_plus").val();
	else
		var weekend = 0;
		
	if($(".plus_bed").is(':checked'))
		var bed = $(".plus_bed").val();
	else
		var bed = 0;
		
	if($(".plus_bed_plus_food").is(':checked'))
		var bed_food = $(".plus_bed_plus_food").val();
	else
		var bed_food = 0;
	
	if($(".room1_value").is(':checked'))
		var room1  = $(".room1_value").val();
	else
		var room1 = 0;
		
	if($(".room2_value").is(':checked'))
		var room2  = $(".room2_value").val();
	else
		var room2 = 0;
	
	if($(".room3_value").is(':checked'))
		var room3  = $(".room3_value").val();
	else
		var room3 = 0;
		
	if($(".other1_value").is(':checked'))
		var other1 = $(".other1_value").val();
	else
		var other1 = 0;
		
	if($(".other2_value").is(':checked'))
		var other2 = $(".other2_value").val();
	else
		var other2 = 0; 
		
	if($(".other3_value").is(':checked'))	
		var other3 = $(".other3_value").val();
	else
		var other3 = 0;
		
	if($(".other4_value").is(':checked'))	
		var other4 = $(".other4_value").val();
	else
		var other4 = 0;
		
	if($(".other5_value").is(':checked'))	
		var other5 = $(".other5_value").val();
	else
		var other5 = 0;
		
	
	if($("#insurance_value").is(':checked'))	
		var insurance = 1;
	else
		var insurance = 0;


		
	if($(".other6_value").is(':checked'))	
		var other6 = $(".other6_value").val();
	else
		var other6 = 0;
		
	
	if($(".single1_value").is(':checked'))	
		var single1 = $(".single1_value").val();
	else
		var single1 = 0;
	
	if($(".single2_value").is(':checked'))	
		var single2 = $(".single2_value").val();
	else
		var single2 = 0;
		
	if($(".single3_value").is(':checked'))	
		var single3 = $(".single3_value").val();
	else
		var single3 = 0;

	
	
	
	var child1  = $(".child1_value").val();
	var child2  = $(".child2_value").val();

	if(child1 === undefined)
		child1 = 0;
		
	if(child2 === undefined)
		child2 = 0;
	
	
	//console.log("child1 - "+child1);
	//console.log("child2 - "+child2);
	
	if(child1 == 999)
		child1 = 0;
	
	if(child2 == 999)
		child2 = 0;

	var plusDays = origPrice/dayNum + half_board*1  +bed*1 + bed_food*1 + child1*1 + child2*1 + room1*1 + room2*1 + room3*1 + other1*1 + other2*1 + other3*1 + other4*1 + other5*1 + other6*1;

	if($(".plus_days").is(':checked'))
		var plusDay = plusDays;
	else
		var plusDay = 0;

	var total = origPrice*1 + half_board*dayNum + weekend*2 +bed*dayNum + bed_food*dayNum + child1*dayNum + child2*dayNum + room1*dayNum + room2*dayNum + room3*dayNum + other1*dayNum + other2*dayNum + other3*dayNum + other4*dayNum + other5*dayNum + other6*dayNum + plusDay*1 + single1*1  + single2*1  + single3*1; // + parseInt(bed)*dayNum + parseInt(bed_food)*dayNum  + parseInt(room1)*dayNum  + parseInt(room2)*dayNum  + parseInt(room3)*dayNum  + parseInt(other1)*dayNum  + parseInt(other2)*dayNum  + parseInt(other3)*dayNum  + parseInt(plusDay) + parseInt(child1)*dayNum  + parseInt(child2)*dayNum ;

	if(insurance == 1)
	{
		var origtotal = total;
		
		total = total + Math.round(total*0.025);
		
		
		$("#insurance_total").html('');
		$("#insurance_total").append(addCommas(parseInt(origtotal*0.025))+' Ft');
		$("#insurance_val").val(parseInt(origtotal*0.025));
	
	}
	else
	{
		var origtotal = total;
		
		
		$("#insurance_total").html('');
		$("#insurance_total").append(addCommas(parseInt(origtotal*0.025))+' Ft');
		$("#insurance_val").val(parseInt(0));
		
	}

	$("#plusDays").html('');
  	$("#plusDays").append(addCommas(parseInt(plusDays))+' Ft');
	$(".plus_days").val(parseInt(plusDays));
	//var plusDayText = $(".plusDays")
	if(discountPrice > 0) {
		total = total - discountPrice; 
	}
 	$("#totalPrice").html('');
  	$("#totalPrice").append(addCommas(parseInt(total))+' Ft');
  
	//console.log(plusDay);
	//console.log(total);
}



reCalculate();

$("#delsubmit").click(function () {
	var reason = $("#reason").val()*1;
	var service = $("#service").val()*1;
	
	if(reason == 0 || service == 0)
	{
		alert("Kérem töltse ki az űrlapot!")
		return false;
	}
	else
	{
		return true;
	}
	
});

$(".submit").click(function () {
	//alert('aaa');
	
	var tos = $("#tos").val()*1;
	
	if(tos == 1)
	{
		var disclaimer = $('input:radio[name=disclaimer]:checked').val()*1;
		
		if(isNaN(disclaimer))
		{
			alert('A nyilatkozat kitöltése kötelező')
			return false;
		}
		else{
			
	
			if($("#toscheckbox").prop("checked"))
			{
		 	  	$("#customerForm").submit();
		   	}
			else
			{
			alert('Az ÁSZF elfogadása kötelező');
			}
		}
	}
	else
	{
		if($(".shipment").val()*1 == 0 || $(".shipment").val()*1 == ''  )
		{
			alert("Kérjem válassza ki az átvétel módját!");
		}
		else
		{
			var taxno = $("#taxno").val();
			var invname = $("#ct_invoice_name").val();
			
			if(invname != '' && taxno == '')
			{
				alert("Az adószám megadása kötelező!");
			}
			else
				$("#customerForm").submit();
	 	}
	  }
   return false;
});

$("#insurance_value").change(function () {
	
	if($(this).is(':checked'))
	{
		$("#qbeinsurance").show();
	}
	else
	{
		$("#qbeinsurance").hide();
	}
    reCalculate();
});

$(".plus_half_board").change(function () {
    reCalculate();
});
$(".plus_weekend_plus").change(function () {
    reCalculate();
});
$(".plus_bed").change(function () {
	reCalculate();
});
$(".plus_bed_plus_food").change(function () {
  	reCalculate();
});
$(".room1_value").change(function () {
	reCalculate();
});
$(".room2_value").change(function () {
	reCalculate();
});
$(".room3_value").change(function () {
     reCalculate();
 });
$(".other1_value").change(function () {
     reCalculate();
});
$(".other2_value").change(function () {
     reCalculate();
});
$(".other3_value").change(function () {
	reCalculate();
});

$(".other4_value").change(function () {
	reCalculate();
});

$(".other5_value").change(function () {
	reCalculate();
});

$(".other6_value").change(function () {
	reCalculate();
});

$(".single1_value").change(function () {
	reCalculate();
});
$(".single2_value").change(function () {
	reCalculate();
});
$(".single3_value").change(function () {
	reCalculate();
});

$(".plus_days").change(function () {
	reCalculate();
});
$(".child1_value").change(function () {
    reCalculate();
});
$(".child2_value").change(function () {
   reCalculate();
});

$(".child3_value").change(function () {
   alert('Amennyiben 3. gyereket is szeretne vinni, állítsa be az életkorát, és felvesszük Önnel a kapcsolatot!\n\nFONTOS: Kérjük addig NE fizesse az utalványt!');
});


/*
<option value="1" <?if($editarr[shipment]==1)echo"selected";?>>Kérjük válasszon</option>
				<option value="2" <?if($editarr[shipment]==2)echo"selected";?>>Online kérem</option>
				<option value="3" <?if($editarr[shipment]==3)echo"selected";?>>Személyes átvétel: 1052 Petőfi Sándor utca 6. Kaputelefon: 40</option>
				<option value="4" <?if($editarr[shipment]==4)echo"selected";?>>Ajánlott levélként: +375 Ft</option>
				<option value="5" <?if($editarr[shipment]==5)echo"selected";?>>Postai utánvéttel: +1 250 Ft</option>
*/
$(".shipment").change(function () {
	$(".hiddenbox3").hide();
	if($(this).val() == 2)
	{
		$("#online").slideToggle('slow');
	}
	if($(this).val() == 3)
	{
		$("#personal").slideToggle('slow');
	}
	if($(this).val() == 4)
	{
		$("#post1").slideToggle('slow');
	}
	if($(this).val() == 5)
	{
		$("#post2").slideToggle('slow');
	}
});

$("#szepvalue").change(function () {
	var maxval = $("#max_checkout").val()*1;
	var currval = $("#szepvalue").val()*1;
	
	if(currval > maxval)
	{
		alert("Ön nem fizethet többet mint az utalvány ára!");
		$(this).val(maxval);
	}
});

$("#szepsubmit").click(function () {

	var currval = $("#szepvalue").val()*1;
	if(currval == 0)
	{
		alert("Kérem adja meg az összeget!");
		return false;
	}
});

$(".payment").change(function () {

	$(".hiddenbox2").hide();
	$(".hiddenbox9").hide();
	$(".hiddenbox5").hide();
	$("#checkComment").hide();
	if($(this).val() == 1)
	{
		$("#transfer").slideToggle('slow');
		$('.shipment option').remove();
		$('.shipment').append('<option value="0">Kérjük válasszon</option><option value="2">Online kérem</option><option value="4">Ajánlott levélként: +420 Ft</option>');

		//$(".payment").add('<option>a</option>');
	}
	if($(this).val() == 9)
	{
		$("#credit").slideToggle('slow');
		$('.shipment option').remove();
		$('.shipment').append('<option value="0">Kérjük válasszon</option><option value="2">Online kérem</option><option value="4">Ajánlott levélként: +420 Ft</option>');

		//$(".payment").add('<option>a</option>');
	}

	if($(this).val() == 2)
	{
		$("#cash").slideToggle('slow');
		$('.shipment option').remove();
		$('.shipment').append('<option value="0">Kérjük válasszon</option><option value="3">Személyes átvétel</option>'); //<option value="4">Ajánlott levélként: +395 Ft</option><option value="2">Online kérem</option>

	}
	if($(this).val() == 5)
	{
		$("#check2").slideToggle('slow');
		$("#checkComment").slideToggle('slow');
		$('.shipment option').remove();
		$('.shipment').append('<option value="0">Kérjük válasszon</option><option value="2">Online kérem</option><option value="3">Személyes átvétel</option><option value="4">Ajánlott levélként: +420 Ft</option>');

	}
	if($(this).val() == 10 || $(this).val() == 11 || $(this).val() == 12)
	{
		$("#szep").slideToggle('slow');
		$("#cash").slideToggle('slow');
		$("#checkComment").slideToggle('slow');
		$('.shipment option').remove();
		$('.shipment').append('<option value="0">Kérjük válasszon</option><option value="3">Személyes átvétel</option><option value="2">Online kérem</option><option value="4">Ajánlott levélként: +420 Ft</option>');

	}
	if($(this).val() == 3)
	{
		$("#post").slideToggle('slow');
		$('.shipment option').remove();
		$('.shipment').append('<option value="5">Postai utánvéttel: +1 250 Ft</option>');

	}
});


 $('#book').click(function() {
    $('#bookbox').slideToggle('slow');
    return false;
  });
  
   $('#invoice').click(function() {
 	$(this).hide();
 	$('#invoicebox').hide();
    $('#invoicedata').slideToggle('slow');
    $('#invoicedatamore').slideToggle('slow');
    
    return false;
  });
  
  $('#check').click(function() {
    $('#checkbox').slideToggle('slow');
    return false;
  });
  $('#when').click(function() {
    $('#whenbox').slideToggle('slow');
    return false;
  });
  $('#gift').click(function() {
    $('#giftbox').slideToggle('slow');
    return false;
  });
   
   $("#checks").watermark('Ha nem a licitáló nevére szól az üdülési csekk, kérjük, a fizetési módnál felugró mezőbe írja bele a neveket és a címeket.');
   
   
   $(".ipicker").imagepicker();
var elem = $("#chars");
$("#limited").limit_characters(300, elem);
});

