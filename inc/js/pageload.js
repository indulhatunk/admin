$(document).ready(function() {

$('.foreignaccount').click(function(){	
	$('.foreignbankinfo').show();
	$(this).hide();
	return false;
	
});


	$('.nlpartner').change(function(){
			var nlcity = $(this).find("option:selected").attr('city');
			var nlhotel = $(this).find("option:selected").attr('hotelname');
			
			if($("#nl_city").val() == '')
			{
				$("#nl_city").val(nlcity);
				
			}
			
			if($("#nl_name").val() == '')
			{
				$("#nl_name").val(nlhotel);
				
			}
		});
		
		

$('.floatnumber').keydown(function(event) {

    var code = event.which;
    var foo = 0;
    // prevent if already dot
    if (code != 8 && code != 46) {
        if ((foo == 0) && (code != 190) && (event.which < 46 || event.which > 59)) {
            event.preventDefault();
        } // prevent if not number/dot
    }
    if ($(this).val().indexOf('.') > -1) {
        foo = 1;
        if (code == 190)
            event.preventDefault();
     }

});


function formatnumber(nStr) {
	 nStr += '';
	 var x = nStr.split('.');
	 var x1 = x[0];
	 var x2 = x.length > 1 ? '.' + x[1] : '';
	 var rgx = /(\d+)(\d{3})/;
	 while (rgx.test(x1)) {
	    x1 = x1.replace(rgx, '$1' + ' ' + '$2');
	 }
	return x1 + x2;
}

function reInit() {
   $('a[rel*=facebox]').facebox();
}



$("#jokercheckbox").click(function() {
		
		var discount = Math.round($("#price").val()*1*0.2);
		
		if($(this).is(':checked') == false)
		{
			$("#jokervalue").val(0);
			$("#jokervalue").val(0);
			$("#jokerhidden").val(0);

		}
		else
		{	
			
			$("#jokervalue").val(discount);
			$("#jokerhidden").val(discount);
		}
		
});


function recalcchange()
{

var sumvalue = 0;
var printid = 'multiple';

	$("#sumtotal").show();
	$(".sumbox input:checked").each(function() {
	
		//alert($(this).val()*1);
    	sumvalue = sumvalue + $(this).val()*1;
    	
    	printid = printid+','+ $(this).attr('printid');
     });
     
     var ptotal;
     var paidtotal;
    var change; 
     ptotal = $("#ptotal").val()*1;
     paidtotal = $("#paidtotal").val()*1;
	
	change = sumvalue-ptotal-paidtotal;
	
	if(change >= 0)
		change = 0;
	else
		change = change*-1;
		
	if(sumvalue == 0)
		$("#sumtotal").hide();
	
	$("#sumtvalue").html(formatnumber(sumvalue)+" Ft");
	$("#chtotal").html(formatnumber(change)+" Ft");
	$("#numbers").html("<a href='/vouchers/print.php?cid="+printid+"' rel='facebox iframe'>Nyomtatás</a>");



}
$('.sumvalue').click(function(){	
	recalcchange();
	reInit();

});


$("#prepaid").change(function(){
	var totalvalue = $("#totalvalue").val()*1;
	var prepaid = $(this).val()*1;
	
	var discount = $("#voucher_val").val()*1;
	
	totalvalue = totalvalue-discount;
	
	if(prepaid > totalvalue)
		alert("Az előleg nem lehet több, mint a fizetendő összeg");
	else
	{
		
		var pc;
		
		pc = (prepaid/totalvalue)*100;
		//var pc = pc.toFixed(0);
		
		$("#ppvalue").val(pc);
	}

});
$("#ptotal").keyup(function(){
		recalcchange();

});

$("#paidtotal").keyup(function(){
		recalcchange();

});



$(".accountNo").mask("99999999-99999999-99999999");
$(".simpledate").mask("9999-99-99");

$('#partnertype').click(function(){	
		var type;
		type = $(this).val()*1;
		
		if(type == 5)
		{
			$('.hotelinfo').hide();
			$('.resellerinfo').show();
		}
		if(type == 0)
		{
			$('.hotelinfo').show();
			$('.resellerinfo').hide();
		}
});


$('#previewbtn').click(function(){	
		var affil;
		affil = $(this).attr('affil')*1;
		window.open('http://akciós-szállás.hu/?affil='+affil);
});


$("#hotel_name").keyup(
    function(){
      var val = $("#hotel_name").val();
   	  var editid = $("#editid").val();
   	  
   	  if(editid == '')
   	  {
   	  	//alert('aaaaaa');
   		 val = val.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
   		   $("#username").val(val);
      }      
    }
);

$('#savepartner').click(function(){
		
		var company_name = $("#company_name").val();
		var hotel_name = $("#hotel_name").val();
		var email = $("#email").val();
		var username = $("#username").val();
		var password = $("#password").val();
		var account_no = $("#account_no").val();
		var zip = $("#zip").val();
		var city = $("#city").val();
		var address = $("#address").val();
		var contact = $("#contact").val();
		var contact_phone = $("#contact_phone").val();
		var partnertype = $("#partnertype").val();
		var yield_vtl = $("#yield_vtl").val();
		var yield_reseller_base = $("#yield_reseller_base").val();
		var status = $("#status").val()*1;
		var hiddenpartner = $("#unique_partner").val()*1;
		
		var error = 0;
		
 		$("#pform input").removeClass("invalid");

			
		if(partnertype == 0 && status == 0 && hiddenpartner == 0)
		{
			if(yield_vtl == '' || yield_vtl == 0)
  			{
  			error = 1;
  			$("#yield_vtl").addClass('invalid');
  			}
		}
		
		if(partnertype == 5 && status == 0)
		{
			if(yield_reseller_base == '' || yield_reseller_base == 0)
  			{
  			error = 1;
  			$("#yield_reseller_base").addClass('invalid');
  			}
		}
 		
			
		if(company_name == '')
  		{
  			error = 1;
  			$("#company_name").addClass('invalid');
  		}
  		if(hotel_name == '')
  		{
  			error = 1;
  			$("#hotel_name").addClass('invalid');
  		}
  		if(email == '' && status == 0)
  		{
  			error = 1;
  			$("#email").addClass('invalid');
  		}
  		if(username == '' && status == 0)
  		{
  			error = 1;
  			$("#username").addClass('invalid');
  		}
  		if(password == '' && status == 0)
  		{
  			error = 1;
  			$("#password").addClass('invalid');
  		}
  		
  		if(account_no == '' && status == 0)
  		{
  			error = 1;
  			$("#account_no").addClass('invalid');
  		}
  		
  		if(zip == '' && status == 0)
  		{
  			error = 1;
  			$("#zip").addClass('invalid');
  		}
  		
  		if(city == '' && status == 0)
  		{
  			error = 1;
  			$("#city").addClass('invalid');
  		}
  		
  		if(address == '' && status == 0)
  		{
  			error = 1;
  			$("#address").addClass('invalid');
  		}
  		if(contact == '')
  		{
  			error = 1;
  			$("#contact").addClass('invalid');
  		}
  		if(contact_phone == '' && status == 0)
  		{
  			error = 1;
  			$("#contact_phone").addClass('invalid');
  		}
  		if(error == 1)
		{ 
  			alert("Kérem minden mezőt töltsön ki!");
  			return false;
  		}
  		else
  		{
  			return true;
  		}
  });
  
  
  
  $('#subscribebtn').click(function(){

	 var email = $("#sbemail").val();
	 var name = $("#sbname").val();
	 var error = 0;
	 
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

	 if(name == '' || email == '')	
	 {
	 	alert("Minden mező kitöltése kötelező");
	 	error = 1;
	 }
	 else if(!emailReg.test(email))
	{
		alert("Hibás e-mail cím");
		error = 1;
	}
	
	
	 if(error == 0)
	 	 $(this).parent().submit();
	 else
	 {
	 	return false;
	 }
});


$('#customersubmit').click(function(){
		
		var name = $("#customername").val();
		var phone = $("#customerphone").val();
		var email = $("#customeremail").val();
		var error = 0;
		
 		$("#cform input").removeClass("invalid");


 			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			  
			  
		if(!emailReg.test(email))
		{
			alert("Az e-mail cím formátuma nem megfelelő!");
			$("#customeremail").addClass('invalid');
			error = 1
		}
			
			
		if(name == '')
  		{
  			error = 1;
  			$("#customername").addClass('invalid');
  		}
  		if(phone == '')
  		{
  			error = 1;
  			$("#customerphone").addClass('invalid');
  		}
  		if(email == '')
  		{
  			error = 1;
  			$("#customeremail").addClass('invalid');
  		}
  		
  		if(error == 1)
		{ 
  			//alert("Kérem minden mezőt töltsön ki!");
  			return false;
  		}
  		else
  		{
  			return true;
  		}
  });
	
	
$('.backbutton').click(function(){
		parent.history.back();
		return false;
	});
	
	
	$('#showinvdata').click(function(){
		$("#invoicebutton").hide();
		$(".invdata").fadeIn();
		return false;
	});
	
	
  $(".showinfo").click(function()
  {
  
  	var id = $(this).attr('id');
  	
  
	   $("#info-"+id).slideToggle(500);
  });
  
  $("#invoice_partner").change(function () {
  		var name = $('#invoice_partner option:selected').attr('company_name');
  		var zip = $('#invoice_partner option:selected').attr('zip');
  		var city = $('#invoice_partner option:selected').attr('city');
  		var address = $('#invoice_partner option:selected').attr('address');
  		var email = $('#invoice_partner option:selected').attr('email');
  		var tax = $('#invoice_partner option:selected').attr('tax_no');
  		
  		
  		$("#invoice_name").val(name);
  		$("#invoice_zip").val(zip);
  		$("#invoice_city").val(city);
  		$("#invoice_address").val(address);
  		$("#invoice_email").val(email);
  		$("#invoice_tax").val(tax);
  		
  });
  
  
	$("#monthChange").change(function () {
		
		var url = $(this).val();
		
		window.location.replace("customers.php?"+url);
		return false;
         //$(this).parent().submit();
    });
    
    
    $("#fc_curr").change(function () {
		
		$(".fc_curr").html($(this).val());
		 //$(this).parent().submit();
    });


	$("#offerlanguage").change(function () {
		var thisval = $(this).val();
		
		if(thisval == 'en')
			$(".en").show();
		else
			$(".en").hide();
		return false;
    });
    

$(".gross").keyup(function() {
	var id = $(this).attr("id");
	id = id.split('-');
	id = id[1];
	var grossval = $(this).val()*1;
	
	if($("#tax-"+id).val() == 'AHK')
		var tax = 0;
	else
		var tax = $("#tax-"+id).val()*1;
	
	var netval = Math.round(grossval/(1+tax/100));
	
	$("#net-"+id).val(netval);
	
});
$(".net").keyup(function() {
	
	var id = $(this).attr("id");
	id = id.split('-');
	id = id[1];
	var netval = $(this).val()*1;
	
	if($("#tax-"+id).val() == 'AHK')
		var tax = 0;
	else
		var tax = $("#tax-"+id).val()*1;
	
	var grossval = Math.round(netval*(1+tax/100));
	
	$("#gross-"+id).val(grossval);

});


$(".invoiceitem").keyup(function() {
	var id = $(this).attr("id");
	id = id.split('-');
	id = id[1];
	$("#quantity-"+id).val(1);
	$("#quantity2-"+id).val("db");
});


$(".tax").change(function() {
	
	var id = $(this).attr("id");
	id = id.split('-');
	id = id[1];
	var grossval = $("#gross-"+id).val()*1;
	
	if($(this).val() == 'AHK')
		var tax = 0;
	else
		var tax = $(this).val()*1;
	
	var netval = Math.round(grossval/(1+tax/100));
	
	$("#net-"+id).val(netval);
});

	
$(".child1value").keyup(function() {
	
	$(".child1valuereal").val($(this).val());
});




$(".child2value").keyup(function() {
	
	$(".child2valuereal").val($(this).val());
});

$(".child3value").keyup(function() {
	
	$(".child3valuereal").val($(this).val());
});
	
 $(".resolve").change(function () {
         alert('Sikeresen megoldotta, gratulalunk! Csak igy tovabb!');
         $(this).parent().submit();
        });
        
	 $('a[rel*=facebox]').facebox();
  $(".collapse").hide();
  
  
	$(".selectall").toggle(
function(){
  $(".imagetodel").attr('checked', true)
},
function(){
   $(".imagetodel").attr('checked', false)
});

/*
.click(function()
  	{
  		$(".delimage").attr('checked', true);
  	}); 
 */ 
  /****/
  	$(".delimage").click(function()
 	{
 		var isSelected = confirm('Biztosan törölni szeretné a kijelölt képeket?');
		if(isSelected == true)
		{	
			alert('Kérem várjon türelmesen, a folyamat sokáig is eltarthat');
			$(".gallery").hide();
			//$(".gallery").html('Töltés');
			return true;
		}				
		else
		{
			return false;
		}
	});
	
	$(".setdefaultimage").click(function()
 	{
 			alert('Kérem várjon türelmesen, a folyamat sokáig is eltarthat');
			//$(".gallery").hide();
			//$(".gallery").html('Töltés');
			return true;
	});

  /***/



  //toggle the componenet with class msg_body
  $(".show").click(function()
  {
  	   $(".collapse").slideToggle(600);
  }); 
  
  Date.format = 'yyyy-mm-dd';
  
 // $(".maskeddate").mask("9999-99-99 99:99:99");
  $('.maskeddate').datePicker({clickInput:true, startDate: '2010-01-01'})
  
    $('.dpick').datePicker({clickInput:true, startDate: '2010-01-01'})


   $('.dfilter').datePicker({clickInput:true, startDate: '2010-01-01'})


$('#lt_name').watermark('Név');

$('#lt_email').watermark('E-mail cím');

$('#lt_from_date').watermark('Kezdő dátum');
$('#lt_to_date').watermark('Záró dátum');
$('#lt_content').watermark('Tartalom');


  $("input.numeric").numeric();
   
  $("#submit").click(function()
  {
  	  //alert("aa");
  	  $("#sform").submit();
   }); 

	/* getoffer form */
	$('.formCalculate').change(function() {
		var thisItem = $(this).attr("id");
		var origBox = thisItem.replace('_text', '');
		
		if($('#'+origBox).val()==1)
			origValue = Math.round($(this).val() / $("#dayNum").val());
		else
			origValue = $(this).val();

		$('input[name='+origBox+']').val(origValue);

		//alert(origValue);
		//$('input[name='+thisItem+']').val(newValue);
		
	//	var thisItem = $(this).attr("id");
	//	var newValue = Math.round($(this).val() / $("#dayNum").val());
  	//	$('input[name='+thisItem+']').val(newValue);
  		//alert('changed2');
	});
	
	$('#plus_weekend_plus_text').change(function() {
		var half = Math.round($(this).val()/2);
		//alert(half);
		$('input[name=plus_weekend_plus]').val(half);
		
	});
	
	$('.chage_calculate').change(function() {
		var thisValue = $(this).val();
		var thisItem = $(this).attr("id");
		var origValue = '';
		
		if(thisValue == 1)
			origValue = Math.round($("#"+thisItem+"_text").val() / $("#dayNum").val());
		else
			origValue = $("#"+thisItem+"_text").val();
			
		$('input[name='+thisItem+']').val(origValue);
		//alert(origValue);
		
		//var thisItem = $(this).attr("id");
		//var newValue = Math.round($(this).val() / $("#dayNum").val());
  		//$('input[name='+thisItem+']').val(newValue);
  		
  		//alert('changed');
	});
	
	$('#dayNum').change(function() {
  		$('.changedDayNum').html('<b>'+$(this).val()+'</b>');
	});



$('.cleared_status').change(function() {

		
		var thisID = $(this).attr('id');
		var isSelected = confirm('Biztosan megérkezett a '+thisID+' sz. számla?');
		
		
		if(isSelected == true)
		{	
			var thisID = $(this).attr('id');
			var thisVal = $(this).val();
			$(this).hide('slow');
			$.get('info/arrived_invoice.php?invoice='+thisID+'&status='+thisVal, function(data) {
 				
  				alert('A számla sikeresen kiegyenlítve');
			});
			//alert(thisID+' kiegyenlitve '+thisVal);	
		}				
		else
		{}
	});
	
$('.cleared').click(function() {
		var isSelected = confirm('Biztosan ki szeretné egyenlíteni a számlát?');
		
		
		if(isSelected == true)
		{	
			var thisID = $(this).attr('id');
			$(this).hide('slow');
			$.get('info/cleared.php?invoice='+thisID, function(data) {
 				
  				alert('A számla sikeresen kiegyenlítve');
			});
			
		}				
		else
		{}
	});
	
	
	$('#create_invoice').click(function() {
		var isSelected = confirm('Biztosan ki szeretné állítani a számlát?');
		
		
		if(isSelected == true)
		{	
			$("#invoicesubmit").submit();			
		}				
		else
		{}
	});
	
	
	$('.report').click(function() {
		var isSelected = confirm('Biztosan jelenteni szeretné az értékelést?');
		
		
		if(isSelected == true)
		{	
			var thisID = $(this).attr('id');
			
 			
 			$.get('/reviews.php?reported='+thisID, function(data) {
 				
  				//alert('A számla sikeresen kiegyenlítve');
			});
			
			$("#row-"+thisID).addClass('red');
			
		}				
		else
		{}
	});
	
	$('.invoice_arrived').click(function() {
	
		var thisID = $(this).attr('id');
		var isSelected = confirm('Biztosan megérkezett a '+thisID+' sz. számla?');
		var thisID = $(this).attr('id');
		
		if(isSelected == true)
		{	
			$(this).hide('slow');
			$.get('info/arrived_invoice.php?invoice='+thisID, function(data) {
 				
  				alert('A számla sikeresen kiegyenlítve');
			});
			
		}				
		else
		{}
	});
	
	
	$('.cleared_lmb').click(function() {
		var isSelected = confirm('Biztosan ki szeretné egyenlíteni a számlát?');
		
		
		if(isSelected == true)
		{	
			var thisID = $(this).attr('id');
			$(this).hide('slow');
			$.get('info/cleared_lmb.php?invoice='+thisID, function(data) {
 				
  				alert('A számla sikeresen kiegyenlítve');
			});
			
		}				
		else
		{}
	});
	
	$('.alertbox').click(function() {
		var isSelected = confirm('A vendég biztosan távozott?');
		
		
		if(isSelected == true)
		{	
			var thisID = $(this).attr('id');
			$(this).hide('slow');
			$.get('info/arrived.php?id='+thisID, function(data) {
 				
  				alert('A vendég sikeresen távozott');
			});
			
		}				
		else
		{}
	});
	
	
	$(".vatera_date").click(function() {
		var id = $(this).attr('id');
		
		if($(this).is(':checked') == false)
			var vatera_date = 1;
		else
		{
			var vatera_date = 2;
		}
		 $.ajax({  
            type: "GET",  
            url: "info/setvoucher.php",  
            data: "id="+ id +"&vatera_date="+vatera_date,  
            success: function(){  
     
  
            }  
        });  
   	});
   	
   		$(".licittravel_date").click(function() {
		var id = $(this).attr('id');
		
		if($(this).is(':checked') == false)
			var vatera_date = 1;
		else
		{
			var vatera_date = 2;
		}
		 $.ajax({  
            type: "GET",  
            url: "info/setvoucher.php",  
            data: "id="+ id +"&licittravel_date="+vatera_date,  
            success: function(){  
     
  
            }  
        });  
   	});
   	
   	$(".on_sale").click(function() {
		var id = $(this).attr('id');
		
		if($(this).is(':checked') == false)
			var vatera_date = 1;
		else
		{
			var vatera_date = 2;
		}
		 $.ajax({  
            type: "GET",  
            url: "info/setvoucher.php",  
            data: "id="+ id +"&on_sale="+vatera_date,  
            success: function(){  
     
  
            }  
        });  
   	});
   	
   	$(".outlet_date").click(function() {
		var id = $(this).attr('id');
		
		if($(this).is(':checked') == false)
			var outlet_date = 1;
		else
		{
			var outlet_date = 2;
		}
		 $.ajax({  
            type: "GET",  
            url: "info/setvoucher.php",  
            data: "id="+ id +"&outlet_date="+outlet_date,  
            success: function(){  
     
  
            }  
        });  
   	});

   	
});


/** new design **/

$(document).ready(function() {
			$("#main-nav li ul").hide(); // Hide all sub menus
		$("#main-nav li a.current").parent().find("ul").slideToggle("slow"); // Slide down the current menu item's sub menu
		
		$("#main-nav li a.nav-top-item").click( // When a top menu item is clicked...
			function () {
				$(this).parent().siblings().find("ul").slideUp("normal"); // Slide up all sub menus except the one clicked
				$(this).next().slideToggle("normal"); // Slide down the clicked sub menu
				return false;
			}
		);
		
		$("#main-nav li a.no-submenu").click( // When a menu item with no sub menu is clicked...
			function () {
				window.location.href=(this.href); // Just open the link instead of a sub menu
				return false;
			}
		); 

    // Sidebar Accordion Menu Hover Effect:
		
		$("#main-nav li .nav-top-item").hover(
			function () {
				$(this).stop().animate({ paddingRight: "25px" }, 200);
			}, 
			function () {
				$(this).stop().animate({ paddingRight: "15px" });
			}
		);
		
		
		
		
		$("ul.tabbed li").click(function() {

		$("ul.tabbed li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
	


$("#lusername").keyup(function(){
	
		var val = $(this).val();
		
		if(val == 'quaestor')
			$("#office").show();
		//alert('aaaaa');

});

$(".ipicker").imagepicker();

//var elem = $("#chars");


var elem = $("#chars");
$("#limited").limit_characters(300, elem);
$("a.fancy_gallery").fancybox();
 
 
 $('.chk').click(function(){	
 
 
 	return false;
 });
 
  $('#delcoupon').click(function(){	
 
 	$("#voucher_value").html('<font color="red"><b>Törölve!</b></font>');
	$("#voucher_val").val('');
 	return false;
 });
 
 
 
   $("#coupon_code").change(function() { 


var usr = $("#coupon_code").val();
 
if(usr.length >= 3)
{
	
	
	$.get("info/checkpromo.php?tour=1&code="+usr, function(data){
	if(data > 0)
	{
		$("#voucher_value").html('<b><font color="green">'+data+' Ft</font></b>');
		$("#voucher_val").val(data);

	} 
	else 
	{ 
		$("#voucher_value").html('<font color="red"><b>Érvénytelen kód!</b></font>');
		$("#voucher_val").val('');
	}
});

}
return false;
});



 
   $("#discount_code").change(function() { 


$("#promo_status").html('');
var usr = $("#discount_code").val();
var oid = $("#offers_id").val()*1;

 
if(usr.length >= 3)
{
	
	
	$.get("info/checkpromo.php?code="+usr+"&id="+oid, function(data){
	if(data > 0)
	{
		 if(data > 50)
	        var type = ' Ft';
	    else
	        var type = '%'
	        
		$("#promo_status").html('<b><font color="green">-'+data+' '+type+' kedvezmény</font></b>');
	} 
	else 
	{ 
		$("#promo_status").html('<font color="red"><b>Érvénytelen kuponkód!</b></font>');
	}
});

}
});


		});
/* */