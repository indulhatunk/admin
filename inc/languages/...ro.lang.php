<?
/*
-----------------
Language: Hungarian
-----------------
*/

$lang = array();

$lang['login'] = 'Intrare';
$lang['username'] = 'Nume utilizator';
$lang['password'] = 'Parola';
$lang['invaliduserlogin'] = 'Nume sau Parolă incorectă';

$lang['vatera_customers'] = 'Clienţi Cazare Outlet';
$lang['lmb_customers'] = 'Clienţi LMB';
$lang['invoices'] = 'Facturi';
$lang['reviews'] = 'Evaluări';
$lang['tickets'] = 'Tichet greşeli';
$lang['phonebook'] = 'Agenda telefon';
$lang['other'] = 'Altele';
$lang['customers'] = 'Clienţ';
$lang['yes'] = 'Da';
$lang['no'] = 'Nu';
$lang['delete_item'] = 'Ştergere?';
$lang['deleted'] = 'Ştergere reusită';
$lang['customer_added'] = 'Adăugare client';
$lang['customer_edited'] = "Editare client!";
$lang['home'] = 'Pagina principală';
$lang['manage_customers'] = "Gestionare clienţi";
$lang['statistics'] = "Statistică";
$lang['new_customer'] = "Introducere client nou";
$lang['postpaid'] = "Ramburs";
$lang['cash'] = "Cash";
$lang['transfer'] = "Transfer";
$lang['delivery'] = "Courier";
$lang['t_check'] = "T. check";
$lang['online'] = 'Online';
$lang['o_online'] = 'online';
$lang['facebook'] = 'Facebook';
$lang['credit_card'] = 'Card credit';
$lang['place'] = "Faţa locului";
$lang['logout'] = "Ieşire";
$lang['settings'] = "Setări";

$lang['welcome'] = "Bine aţi venit";
$lang['print'] = "printare";
$lang['notify'] = "notificare";
$lang['print_vouchers'] = "Printare Voucher";
$lang['print_envelopes'] = "Printare plicuri";
$lang['print_post'] = "Printare postate";
$lang['export'] = "Export";
$lang['active'] = "Activ";
$lang['paid_printed'] = "Plătit+ + Emis";
$lang['notpaid_printed'] = "Emis + <strike>Plătit</strike>";
$lang['paid_notprinted'] = "Plătit";
$lang['billed'] = "Facturat";
$lang['select'] = 'Alegeţi';
$lang['no_results'] = 'Nici o găsire!';
$lang['id'] = "ID";
$lang['date'] = "Data";
$lang['customer_name'] = "Nume client";
$lang['partner'] = 'Partener';
$lang['item_desc'] = "Descriere licitaţie";
$lang['price'] = 'Preţ';
$lang['payment'] = "Modalitate de plată";
$lang['customer_left'] = "Ieşit";
$lang['paid'] = "Plătit";
$lang['tools'] = 'Mijloace';
$lang['post'] = 'Poştă';
$lang['bill'] = 'Factură';
$lang['comment'] = 'Observaţii';
$lang['address'] = 'Adresă';
$lang['bill_address'] = 'Adresă factura';
$lang['bill_name'] = 'Nume factură';
$lang['plus_services'] = "Servicii suplimentare";
$lang['paid_date'] = "Data plăţii";
$lang['print_date'] = "Data emiterii";
$lang['deadline'] = "Termen de plată";
$lang['phone'] = "Telefon";
$lang['email'] = "E-mail";
$lang['offer'] = "Ofertă";
$lang['offer_type'] = "Tipul ofertei";
$lang['offer_price'] = "Preţ original";
$lang['validity'] = "Valabilitate";
$lang['not_include'] = "Nu conţine";
$lang['customer_details'] = "Date cumpărător";
$lang['change_laguage'] = 'Alege limba';

$lang['halfboard'] = 'HB';
$lang['weekend_extra'] = 'Suprapreţ weekend';
$lang['bed_extra'] = 'Suprapreţ pat';
$lang['bed_extra'] = 'Rămâneţi incă o noapte';
$lang['birthday'] = 'Data naştere';
$lang['fullhouse'] = 'Date pline';

$lang['months'][1] = "Ianuarie";
$lang['months'][2] = "Februarie";
$lang['months'][3] = "Martie";
$lang['months'][4] = "Aprilie";
$lang['months'][5] = "Mai";
$lang['months'][6] = "Iunie";
$lang['months'][7] = "Iulie";
$lang['months'][8] = "August";
$lang['months'][9] = "Septembrie";
$lang['months'][10] = "Octombrie";
$lang['months'][11] = "Noiembrie";
$lang['months'][12] = "Decembrie";
$lang['not_paid'] = 'Nu este plătit!';

$lang['customer_booked'] = 'Confirmare';
$lang['customer_arrival'] = 'Sosire';


$lang['easypassword'] = 'Parola Dumneavoastră este prea simplă! Vă rugăm schimbați-o imediat!';
$lang['invoicerequest'] = '';
$lang['lmbinvoice'] = '';
$lang['fullhouserequest'] = 'ATENȚIE! Nu mai sunt locuri / Full house';
$lang['fullhousenotification'] = "Tabelul cu gradele de ocupare al hoteluli actualizat de Dv. oferă informații proaspete și actuale Clienților noștri, care decid asupra achiziționării în funcție de numărul de locuri libere și perioadele ocupate.<br/><br/>

Actualizarea permanentă și corectă a gradului de ocupare este important atât pentru noi cât și pentru Dv. în vederea maximalizării numărului de rezervări și a minimalizării anulărilor. <br/><br/>

<b>Vă mulțumim pentru colaborare și ajutor!</b> <br/><br/>
";

$lang['booking_details'] = 'Disponibilitate';

$lang['fullrooms'] = 'Stoc epu.';
$lang['freerooms'] = 'Disponibil ';
$lang['roomdetails'] = '*Disponibilitatea afișată este actualizat pentru camerele din prețul ofertei de bază.<br/>
	*Dacă doriți alt tip de cameră, vă rugăm să ne contactați.<br/>
	*Datele prezentate sunt orientative.';
	
$lang['prefix'] = 'ro_';
?>