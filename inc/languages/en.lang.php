<?
/*
-----------------
Language: English
-----------------
*/

$lang = array();

$lang['login'] = 'Login';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['invaliduserlogin'] = 'Invalid username os password!';

$lang['vatera_customers'] = 'Customers';
$lang['lmb_customers'] = 'LMB customers';
$lang['invoices'] = 'Invoices';
$lang['reviews'] = 'Reviews';
$lang['tickets'] = 'Tickets';
$lang['phonebook'] = 'Phonebook';
$lang['other'] = 'Others';
$lang['customers'] = 'Customers';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['delete_item'] = 'Do you really want to delete this item?';
$lang['deleted'] = 'Successfully deleted!';
$lang['customer_added'] = 'Successfully added!';
$lang['customer_edited'] = "Successfully edited!";
$lang['home'] = 'Home';
$lang['manage_customers'] = "Manage customers";
$lang['statistics'] = "Statistics";
$lang['new_customer'] = "New customer";
$lang['postpaid'] = "Post paid";
$lang['cash'] = "Cash";
$lang['transfer'] = "Transfer";
$lang['delivery'] = "Delivery";
$lang['t_check'] = "Traveller's check";
$lang['online'] = 'Online';
$lang['o_online'] = 'online';
$lang['facebook'] = 'Facebook';
$lang['credit_card'] = 'Credit card';
$lang['place'] = "Place";
$lang['logout'] = "Logout";
$lang['settings'] = "Settings";
$lang['bookings'] = "Booking calendar";

$lang['welcome'] = "Welcome";
$lang['print'] = "print";
$lang['notify'] = "notify";
$lang['print_vouchers'] = "Print vouchers";
$lang['print_envelopes'] = "Print envelopes";
$lang['print_post'] = "Print post.";
$lang['export'] = "Export";
$lang['active'] = "Active";
$lang['paid_printed'] = "Paid + printed";
$lang['notpaid_printed'] = "Printed + <strike>paid</strike>";
$lang['paid_notprinted'] = "Paid";
$lang['billed'] = "Billed";
$lang['select'] = 'Select';
$lang['no_results'] = 'No results!';
$lang['id'] = "ID";
$lang['date'] = "Date";
$lang['customer_name'] = "Customer name";
$lang['partner'] = 'Partner';
$lang['item_desc'] = "Description";
$lang['price'] = 'Price';
$lang['payment'] = "Payment";
$lang['customer_left'] = "Check-out";
$lang['paid'] = "Paid";
$lang['tools'] = 'Tools';
$lang['post'] = 'Post';
$lang['bill'] = 'Bill';
$lang['comment'] = 'Comment';
$lang['address'] = 'Address';
$lang['bill_address'] = 'Billing address';
$lang['bill_name'] = 'Billing name';
$lang['plus_services'] = "Plus services";
$lang['paid_date'] = "Paid on";
$lang['print_date'] = "Printed on";
$lang['deadline'] = "Deadline";
$lang['phone'] = "Phone";
$lang['email'] = "E-mail";
$lang['offer'] = "Offer";
$lang['offer_type'] = "Offer type";
$lang['offer_price'] = "Original price";
$lang['validity'] = "Validity";
$lang['not_include'] = "Not include";
$lang['customer_details'] = "Customer details";
$lang['change_laguage'] = 'Change language';
$lang['birthday'] = 'Birthday';
$lang['fullhouse'] = 'Fullhouse';

$lang['months'][1] = "January";
$lang['months'][2] = "February";
$lang['months'][3] = "March";
$lang['months'][4] = "April";
$lang['months'][5] = "May";
$lang['months'][6] = "June";
$lang['months'][7] = "July";
$lang['months'][8] = "August";
$lang['months'][9] = "September";
$lang['months'][10] = "October";
$lang['months'][11] = "November";
$lang['months'][12] = "December";
$lang['not_paid'] = 'Unpaid!';

$lang['customer_booked'] = 'Booked';
$lang['customer_arrival'] = 'Arrival';




$lang['easypassword'] = 'Your password is too easy! Please modify it. More info &raquo;';
$lang['invoicerequest'] = 'You have some outstanding invoices! Please issue them! &raquo;';
$lang['lmbinvoice'] = '';
$lang['fullhouserequest'] = 'Attention: please fill the fullhouse calendar';
$lang['fullhousenotification'] = 'Dear Partner,<br/><br/>Please fill our fullhouse calendar, because based on this, we can notify our customers about your free rooms.
<b>Thank you for the cooperation!</b> <br/><br/>';



$lang['offers'] = 'Offers';
$lang['activeoffers'] = 'Active offers';
$lang['alloffers'] = 'All offers';
$lang['translation'] = 'translation';
$lang['fullrooms'] = 'fullhouse';
$lang['showoffer'] = 'show';
$lang['edit'] = 'Edit';
$lang[shortcat] = 'Cat.';
$lang[shorttrans] = 'Trans.';
$lang[voucher_id] = "Voucher";
$lang['final_name'] = 'Name';
$lang['reason'] = 'Reason';

$lang['romaniantrans'] = 'Romanian translation';
$lang['slovakiantrans'] = 'Slovakian translation';
$lang['template'] = 'Template';
$lang['outername'] = 'Outer name';
$lang['offer_name'] = 'Offer title';
$lang['offer_shortname'] = 'Offer short title';
$lang['room_extra'] = 'additional room';
$lang['other_extra'] = 'extra price';
$lang['single_extra'] = 'single price';
$lang['comparison'] = 'Comparison';
$lang['add'] = 'Add';
$lang['remove'] = 'Remove';
$lang['hungarian'] = 'Hungarian';
$lang['check'] = 'Check';
$lang['save'] = 'Save';
$lang['missing'] = 'Missing';
$lang['romanian'] = 'Romanian';
$lang['slovak'] = 'Slovakian';
$lang[lastdel] = "Last deleted customers";

$lang[basedonhotel] = "*this information is based on your reception's confirmation!";

$lang[currency] = 'RON';

?>