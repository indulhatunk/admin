<?
/*
-----------------
Language: Hungarian
-----------------
*/

$lang = array();

$lang['login'] = 'Belépés';
$lang['username'] = 'Felhasználónév';
$lang['password'] = 'Jelszó';
$lang['invaliduserlogin'] = 'Érvénytelen felhasználónév vagy jelszó!';

$lang['vatera_customers'] = 'SzállásOutlet vásárlók';
$lang['lmb_customers'] = 'LMB vásárlók';
$lang['invoices'] = 'Számlák, elszámolások';
$lang['reviews'] = 'Értékelések';
$lang['tickets'] = 'Hibajegyek';
$lang['phonebook'] = 'Telefonkönyv';
$lang['other'] = 'Egyéb';
$lang['customers'] = 'Vásárlók';
$lang['yes'] = 'Igen';
$lang['no'] = 'Nem';
$lang['delete_item'] = 'Biztosan törölni szeretné a tételt?';
$lang['deleted'] = 'Sikeresen törölte az elemet!';
$lang['customer_added'] = 'Sikeresen felvitte a vásárlót';
$lang['customer_edited'] = "Sikeresen szerkesztette a vásárlót!";
$lang['home'] = 'Főoldal';
$lang['manage_customers'] = "Vásárlók kezelése";
$lang['statistics'] = "Statisztika";
$lang['new_customer'] = "Új vásárló felvitele";
$lang['postpaid'] = "Utánvét";
$lang['cash'] = "Készpénz";
$lang['letterfooter'] = "<b><a href='http://www.indulhatunk.hu/elerhetosegeink?utm_source=outlet_footer'>Irodáink</a></b>  |  <b>E-mail:</b> info@szallasoutlet.hu  |  <b>Telefonszám:</b> +3670 930 7874  |  <b>Nyitvatartás:</b> H-V: 8-22 óráig";
$lang['transfer'] = "Átutalás";
$lang['delivery'] = "Futár";
$lang['t_check'] = "Üdülési csekk";
$lang['online'] = 'Online';
$lang['o_online'] = 'online';
$lang["#LETTERAPP#"] = "<table style='font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; border-radius:5px;' bgcolor='#c2c2c2' cellpadding='3' cellspacing='0'>
							<tbody><tr>
								<td style='padding-top:4px; text-transform:uppercase; white-space:nowrap;' align='left' valign='middle'>&nbsp;<strong>###DOWNLOADAPP###</strong>&nbsp;</td>
							</tr>
							<tr>
								<td style='font-size:11px;' align='center'>&nbsp;&nbsp;<strong>
								<a href='#' target='_blank' style='color:#333333; text-decoration:none;'>
								<img src='http://www.szallasoutlet.hu/images/play.png' alt='Google Play' border='0' height='29' width='90'></a> &nbsp; 
								<a href='https://itunes.apple.com/app/szallasoutlet/id719703666?mt=8' target='_blank' style='color:#333333; text-decoration:none;'>
								<img src='http://www.szallasoutlet.hu/images/store.png' alt='App Store' border='0' height='29' width='103'></a>
</strong>&nbsp;&nbsp;</td>
							</tr>
						</tbody></table>";
						
$lang["#CONTACTUS#"] = "Lépj kapcsolatba velünk";
$lang["#DOWNLOADAPP#"] = "Szállás Outlet már a Mobilodon is!";

$lang['facebook'] = 'Facebook';
$lang['credit_card'] = 'Bankkártya';
$lang['place'] = "Helyszinen";
$lang['logout'] = "kijelentkezés";
$lang['settings'] = "beállítások";
$lang['bookings'] = "Foglalások";
$lang['slabel'] = "Keresés";
$lang['welcome'] = "Üdvözöljük";
$lang['print'] = "nyomtat";
$lang['notify'] = "értesítő";
$lang['print_vouchers'] = "Voucherek nyomtatása";
$lang['print_envelopes'] = "Borítékok nyomtatása";
$lang['print_post'] = "Feladóv. nyomtatása";
$lang['export'] = "Exportálás";
$lang['active'] = "Aktív";
$lang['paid_printed'] = "Fizetett"; //+ kiállított
$lang['notpaid_printed'] = "Kiállított + <strike>fizetett</strike>";
$lang['paid_notprinted'] = "Fizetett";
$lang['billed'] = "Elszámolt";
$lang['select'] = 'Kérem válasszon';
$lang['no_results'] = 'Nincs találat!';
$lang['id'] = "ID";
$lang['date'] = "Dátum";
$lang['customer_name'] = "Vásárló neve";
$lang['partner'] = 'Partner';
$lang['item_desc'] = "Licit leírása";
$lang['price'] = 'Ár';
$lang['payment'] = "Fizetés módja";
$lang['customer_left'] = "Távozott";
$lang['paid'] = "Fizetve";
$lang['tools'] = 'Eszközök';
$lang['post'] = 'Posta';
$lang['bill'] = 'Számla';
$lang['comment'] = 'Megjegyzés';
$lang['address'] = 'Cím';
$lang['bill_address'] = 'Szla. cím';
$lang['bill_name'] = 'Szla. név';
$lang['plus_services'] = "Plusz szolgáltatások";
$lang['paid_date'] = "Fizetés dátuma";
$lang['print_date'] = "Kiállítás dátuma";
$lang['deadline'] = "Fizetési határidő";
$lang['phone'] = "Telefonszám";
$lang['email'] = "E-mail";
$lang['offer'] = "Ajánlat";
$lang['offer_type'] = "Ajánlat típusa";
$lang['offer_price'] = "Eredeti ár";
$lang['validity'] = "Érvényesség";
$lang['not_include'] = "Nem tartalmazza";
$lang['customer_details'] = "Vásárló adatai";
$lang['change_laguage'] = 'Nyelv választása';

$lang['halfboard'] = 'Félpanzió';
$lang['weekend_extra'] = 'Hétvégi felár';
$lang['bed_extra'] = 'Pótágy felár (felnőtt)';
$lang['more_night'] = 'Maradjon még egy éjszakát';
$lang['birthday'] = 'Születésnap';
$lang['fullhouse'] = 'Teltházas időpontok';

$lang['months'][1] = "január";
$lang['months'][2] = "február";
$lang['months'][3] = "március";
$lang['months'][4] = "április";
$lang['months'][5] = "május";
$lang['months'][6] = "június";
$lang['months'][7] = "július";
$lang['months'][8] = "augusztus";
$lang['months'][9] = "szeptember";
$lang['months'][10] = "október";
$lang['months'][11] = "november";
$lang['months'][12] = "december";
$lang['not_paid'] = 'Nincs fizetve!';

$lang['customer_booked'] = 'Visszaigazolva';
$lang['customer_arrival'] = 'Érkezés';


$lang['paymentnotification_subject1'] = "Fizetési emlékeztető";
$lang['paymentnotification_subject2'] = "Fizetési felszólítás";

$lang['paymentnotification_1'] = "fizetési határideje 2 nap múlva lejár.";
$lang['paymentnotification_2'] = "fizetési határideje a mai napon lejár.";
$lang['paymentnotification_3'] = "fizetési határideje 1 hete lejárt.";
$lang['paymentnotification_4'] = "fizetési határideje 2 hete lejárt.";

$lang['paymentnotification'] = 'Kedves #NAME#! <br/>

Felhívjuk szíves figyelmét, hogy szállás utalványának #NOTIFICATION#<br/><br/>

<b>Ajánlat neve: #OFFER#</b><br/>
<b>Fizetendő összeg: #PRICE#</b><br/><br/>

<b>Az alábbi módok egyikén kérjük, 48 órán belül egyenlítse ki vásárlása ellenértékét.</b><br/><br/>

<b>Átutalás:</b><br/>
Kérjük az alábbi bankszámlára utaljon:<br/>
#COMPANY#<br/>
Erste bank:  #BANK#<br/><br/>

<b>Készpénz:</b><br/>
Címünk: 1056 Budapest, Váci utca u 9. 21-es kaputelefon<br/>
Nyitva tartás hétfő és péntek 8-tól 18 óráig<br/><br/>

<!--<b>SZÉP kártya:</b><br/>

Amennyiben SZÉP kártyával szeretne fizetni, de nem szállás alszámlán van az összeg,<br/>
amit utazásához fel szeretne használni, úgy kérjük, hogy fizetési szándékát a vtl@indulhatunk.hu e-mail címre jelezze.<br/><br/>-->

Amennyiben a kérdéses összeget felszólításunk kézhezvétele előtt már<br/>
rendezte, kérjük, tekintse levelünket tárgytalannak.<br/><br/>

Szállás Outlet<br/>

vtl@indulhatunk.hu<br/>
+36 70 930 7874<br/><br/>';



$lang['moreinfo'] = 'További információ:';
$lang['ccpayment'] = 'Bankkártyás fizetés:';

$lang['easypassword'] = 'Az Ön jelszava túl egyszerű! Kérem azonnal változtassa meg! További információ &raquo;';
$lang['invoicerequest'] = 'Ön még nem küldött el minden számlát cégünk felé! Kérjük mihamarabb pótolja! További információ &raquo;';
$lang['lmbinvoice'] = 'Az Ön LMB egyenlegén tartozás mutatkozik. Kérjük mihamarabb pótolja! További információ &raquo;';
$lang['fullhouserequest'] = 'FIGYELEM - Teltházas lista';
$lang['fullhousenotification'] = 'Az <b>Önök</b> által karbantartott foglaltsági adatok táblázat friss, azonnali információt nyújt Ügyfeleinknek, akik vásárlási döntésüket a szabad helyek és a foglalt időszakok alapján hozzák meg<br/><br/>

A pontos foglaltság folyamatos karbantartása <b>közös érdekünk</b> a foglalások számának maximalizálása és a <b>lemondások számának minimalizálása</b> érdekében. <br/><br/>

<b>Együttműködésüket, segítségüket köszönjük!</b> <br/><br/>
';


$lang['booking_details'] = 'Foglaltsági adatok';

$lang['freerooms'] = 'foglalható';
$lang['roomdetails'] = '*A foglaltsági adatok az ajánlat alapárában foglalt szobatípusra vonatkoznak!<br/>
	*Amennyiben egyéb szobatípust választana, kérjük érdeklődjön ügyintézőinktől!<br/>
	*A megadott adatok tájékoztató jellegűek!';
	
/*final page data*/
$lang['prefix'] = '';
$lang['reference_text1'] = 'Foglalásnál hivatkozzon az alábbi utalványszámra:';
$lang['reference_text2'] = 'Foglalni akár már most is tud, véglegesíteni csak a fizetés után.';
$lang['booking_info'] = 'Foglalni itt tud:';

$lang['final_menu1'] = 'Foglalni szeretnék!';
$lang['final_menu2'] = 'SZÉP kártyám van!';
$lang['final_menu3'] = 'Mikor kapom meg az utalványt?';
$lang['final_menu4'] = 'Ajándékba vettem az utalványt!';
$lang['final_map'] = 'megnézem a térképen';

$lang['final_welcome'] = 'Kedves #NAME#';
$lang['final_welcometext'] = "Töltse ki az alábbi űrlapot, mivel ezzel rengeteg plusz munkától és telefonálgatástól kímélheti meg magát.";
$lang['final_endtext'] = "- A *-gal jelzett mezők kitöltése kötelező! Az utalvány kifizetése után a számlázási adatokat nem áll módunkban módosítani!";
$lang['final_endtext2'] = "- A Vásárlók kérdéseire adott válaszok, különösen a szabad férőhelyekre vonatkozó kérdések esetén tájékoztató jellegűek. Az Eladó a kérdéseket legjobb tudása és a legfrissebb információk szerint válaszolja meg. A szabad időpontokra és specifikus, az ajánlatban nem szereplő információkra vonatkozóan minden esetben a szálloda által adott információ az irányadó.";

$lang['final_title'] = 'Szállás Outlet vásárlás véglegesítése';
$lang['customer_details'] = '1. Vásárló adatai';
$lang['offer_details'] = '2. Felárak, egyéb szolgáltatások';
$lang['customer_details'] = '1. Vásárló adatai';
$lang['customer_details'] = '1. Vásárló adatai';
$lang['final_save'] = 'Mentés';

$lang['final_name'] = 'Neve';
$lang['reason'] = 'Oka';

$lang['final_address'] = 'Utca, házszám';
$lang['final_city'] = 'Irányítószám, város';
$lang['final_email'] = 'E-mail cím';
$lang['final_phone'] = 'Telefonszám';
$lang['final_birth'] = 'Születési dátum';

$lang['final_company_invoice'] = 'Céges számlát szeretnék';
$lang['personal_data'] = 'Személyes adatok';
$lang['final_invoice_details'] = 'Számlázási adatok';
$lang['invoice_name'] = 'Számlázási név';
$lang['invoice_tax'] = 'Adószám';
$lang['invoice_address'] = 'Utca, házszám';
$lang['invoice_city'] = 'Irányítószám, város';
$lang['next'] = 'tovább &raquo;';
$lang['payment_info'] = '3. Fizetési információk';
$lang['payment_method'] = 'Fizetés és átvétel módja';
$lang['payment_mode'] = 'Fizetés módja';
$lang['shipment_mode'] = 'Átvétel módja';

$lang['final_footer'] = "	

	
	
	<table border='0' cellspacing='0' cellpadding='0' style='width:985px;margin:0 auto;'>
		<tr>
			
			<td>
				<div style='float:left; border:1px solid #d4d2cc; padding:2px; margin:2px; border-radius:5px;background-color:white;'><a href='http://otpszepkartya.hu' target='_blank'><img src='/images/otp.png' height='40' alt='OTP SZÉP'/></a>
				<a href='http://mkbszepkartya.hu' target='_blank'><img src='/images/mkb.png' height='40' alt='MKB SZÉP'/></a>
				<a href='http://szepkartya.kh.hu' target='_blank'><img src='/images/kh.png' height='40' alt='K&amp;H SZÉP'/></a></div>
			</td>
			<td width='350' align='center'><a href='http://www.veddaneten.hu/webaruhaz/Szallas-Outlet/163/' target='_blank'><img src='http://www.szallasoutlet.hu/images/va.png' width='80'/></a></td>
			<td>
				<a href='http://payu.hu/view/smarty/uploaded_images/120104100228-fizetesi-tajekoztato-20120102.pdf' alt='PayU Vásárlói tájékoztató' title='PayU Vásárlói tájékoztató' target='_blank'><img src='/images/payu.png' alt='PayU'/></a>
			</td>
		</tr>
	</table>
	
	
	
	<div class='cleaner'></div>
	<div class='copyright'>Minden jog fenntartva &copy;".date('Y')." Indulhatunk utazásszervező Kft. | <a href='http://www.szallasoutlet.hu/felhasznalasi-feltetelek' target='_blank' style='color:black;'>Felhasználási feltételek</a></div>
";
$lang['message'] = "Üzenet az eladónak:";

$lang['cc_text'] = 'Bankkártyájával online fizethet, a mentés gomb megnyomása után!';
$lang['cc_text2'] = 'Bankkártyájával online fizethet!';

$lang['cash_text'] = 'Irodáink: 5. kerület Váci utca 9., 6. kerület: Teréz krt. 27., 11. kerület: Bartók B. út 44., Nyíregyháza: Kossuth tér 8.';
$lang['transfer_text'] = "A termék árát ön átutalással is kiegyenlítheti. Átutalása során kérem az utalványszámát mindenképpen tűntesse fel! A bankszámlaszámot a véglegesítés után e-mailben kapja meg!";

$lang['online_text'] = 'Utalványát most letöltheti!';
$lang['post_text'] = 'Az befizetés megérkezése napján postára adjuk, ami ~2 napot vesz igénybe. Díja: 445 Ft';
$lang['personal_text'] = $lang['cash_text'];

$lang['szep_text'] = "SZÉP kártyájának szállás zsebéről Online fizethet. Amennyiben egyéb zsebről szeretne fizetni, kérem vegye fel velünk a kapcsolatot!";
$lang['message2'] = 'Időpont-egyeztetéssel kapcsolatos információt csak a Hotel recepcióján tudnak adni!';

$lang['payments_normal'] = "<input type='radio' name='payment' value='9' class='newpayment' checked> <a href='#' title='$lang[cc_text]' class='tooltip'>Bankkártya (?)</a><br/>
			<input type='radio' name='payment' value='1' class='newpayment'> <a href='#' title='$lang[transfer_text]' class='tooltip'>Átutalás (?)</a><br/>
			<input type='radio' name='payment' value='2' class='newpayment'> <a href='#' title='$lang[cash_text]' class='tooltip'>Készpénz (?)</a><br/>
";
$lang['payments_szep'] = "<input type='radio' name='payment' class='newpayment' value='10'> <a href='#' title='$lang[szep_text]' class='tooltip'>OTP SZÉP kártya (?)</a><br/>
					<input type='radio' name='payment' class='newpayment' value='11'> <a href='#' title='$lang[szep_text]' class='tooltip'>MKB SZÉP kártya (?)</a><br/>
					<input type='radio' name='payment' class='newpayment' value='12'> <a href='#' title='$lang[szep_text]' class='tooltip'>K&amp;H SZÉP kártya (?)</a><br/>
";

$lang['shipment_normal'] = "<input type='radio' name='shipment' value='2' checked/> <a href='#' title='$lang[online_text]' class='tooltip'>Online kérem (?)</a><br/><input type='radio' name='shipment' value='4'/> <a href='#' title='$lang[post_text]' class='tooltip'>Ajánlott levélként (?)</a><br/>";
$lang['shipment_special'] = "<input type='radio' name='shipment' value='2'/><a href='#' title='$lang[personal_text]' class='tooltip'>Személyes átvétel (?)</a><br/>";
$lang['contact_seller'] = 'Üzenet az eladónak';
$lang['extraprices'] = 'További szolgáltatások';

$lang['offer_id'] = "Utalvány sorszáma";

$lang['plusnight'] = '/ éj';

$lang['child1'] = '1. gyermek kora';
$lang['child2'] = '2. gyermek kora';
$lang['child3'] = '3. gyermek kora';
$lang['please_choose'] = 'Kérjük válasszon';

$lang['child_text1'] = 'éves korig';
$lang['child_text2'] = 'éves kor között';
$lang['child_text3'] = 'éves kor felett';

$lang['room_extra'] = 'szoba felár';

$lang['extra_price'] = 'felár';

$lang['asagift'] = 'Az utalványon ne szerepeljen az ár';

$lang['package'] = 'csomag';

$lang['halfboard_weekend'] = 'Félpanzió + pótágy';

$lang['required'] = 'kötelező';
$lang['totals'] = 'Összesen';
$lang['voucher_info'] = 'Utalvány adatai';
$lang['normal_price'] = 'Alapár';

$lang['correct_fields'] = 'Kérem minden mezőt töltsön ki!';
$lang['use_next'] = 'Kérem használja a tovább gombot!';


$lang['iwantbook'] = '<b>Foglalási szándékát kérjük a fenti zöld részben található elérhetőségeken jelezze.</b><br/><br/>';

$lang['iwantbook2'] = 'Foglalásnál kérjük hivatkozzon az alábbi utalvány számra:  <br/><br/>
			<b>#OFFER_ID#</b><br/><br/>
			Foglalni akár már most is tud, véglegesíteni csak a fizetés után.';

$lang['szepcard'] = 'OTP SZÉP kártyájának szállás zsebéről online fizethet. <br/><br/>
A további zsebek érvényességéről kérjük érdeklődjön a <a href="mailto:vtl@indulhatunk.hu" style="color:#252525">vtl@indulhatunk.hu</a> címen
';
$lang['wheni'] = '<b>Akár már most is megkaphatja az utalványát, ugyanis letöltheti magának.</b><br/>
(az utalvánnyal foglalni már tud, de a foglalását csak akkor véglegesítik, ha már ki is fizette az ellenértékét)<br/><br/>

<b>Ha személyesen bejön hozzánk,</b> azonnal megkapja<br/><br/>
<b>Ha postai úton kéri:</b><br/>
az utalás megérkezése napján postára adjuk, ami kb 2 nap<br/>
díja: 445 Ft<br/><br/>';
$lang['giftok'] = '<b>Az utalvány nem névre szóló, azaz a felhasználási idő alatt bármikor elajándékozhatja. </b><br/><br/>
	<b>FONTOS!</b> <br/>Az utalvány pénzre nem váltható, azaz továbbértékesíteni tilos.';

$lang['calc_storno'] = 'QBE kalkulált útlemondási biztosítás';
$lang['calculated_storno'] = '<a href="#stornoinsurance" style="color:#252525;" class="tooltip" title="Ne bízza a véletlenre! Kössön útlemondási biztosítást" rel="facebox" >QBE útlemondási biztosítás (2,5%)*</a>
	<div style="display:none;width:500px !important;" id="stornoinsurance">
	<div style="font-weight:bold;font-size:18px;">Miért kössön biztosítást?</div>
	
	<div style="padding:20px 0 0 0;">
	
	Ha az utas betegség, baleset vagy káresemény miatt nem tud az utazáson részt venni, a biztosító megfizeti az utas kárának a biztosításban meghatározott mértékét. Az akadályoztatás okának és a bizonyítás módjának részleteit a biztosítási szerződés tartalmazza. 
	
		
	<div style="padding:20px 0 0 0">
		További információ: <a href="/documents/calc_storno.pdf" target="blank" style="color:#252525;font-weight:bold;">Kalkulált útlemondási biztosítás szabályzat</a>
	</div>	
	</div>
	</div>
	
	
'; 

$lang['passenger1_name'] = "1. utas neve";
$lang['passenger2_name'] = "2. utas neve";
$lang['passenger3_name'] = "3. utas neve";
$lang['passenger4_name'] = "4. utas neve";
$lang['passenger5_name'] = "5. utas neve";
$lang['passenger6_name'] = "6. utas neve";

$lang['insurance_accept'] = 'Az útlemondási szerződés feltételeit elfogadom!';

$lang['insurance_top'] = "- Kérem adja meg az utasok <b>személyi okmányban szereplő</b> neveit.<br/>
- Az utasok neveit a fizetés után nem áll módunkban módosítani!<br/>
- Biztosítási igényével QBE Atlasz biztosítóhoz fordulhat!";

$lang['please_transfer'] = "<b>Kérjük az alábbi bankszámlára utaljon</b><br/><br/>";
$lang['please_transfer2'] = "A közleménybe pedig kérjük írja be a nevét és az utalvány sorszámát:<br/>";

$lang['postpaid_text'] = '<div style="text-align:left;color:#e6e1dc; background-color:#000000; border:solid black 1px; padding:0.5em 1em 0.5em 1em; overflow:auto;font-size:small; font-family:monospace; "><br />
</div>';

$lang['online_special'] = '"Ezt az utalványt kivételesen csak személyesen, vagy postai úton áll módunkban elküldeni Önnek, mivel a szolgáltatás csak a szálloda által kiállított utalvánnyal vehető igénybe! <br/><br/>"';

$lang['download1'] = "Töltse le az utalványt az alábbi linken:";

$lang['download_btn'] = 'letöltés  &raquo;';

$lang['personal_shipment'] = 'Személyes átvétel';
$lang['simple_post'] = 'Ajánlott levélként';

$lang['final_top'] = "Köszönjük, hogy beállította a vásárlási adatait.";

$lang['final_letter_top'] = "<div class='offertitle'>Köszönjük, hogy beállította a vásárlási adatait.</div><br/><br/>Foglalási szándékát kérjük az alábbi elérhetőségeken jelezze:<br/><br/>";
$lang['final_letter_center'] = "Érvényes visszaigazolást igényelni a szállodaban csak az utalvány ellenértékének befizetése után lehet. <br/><b>A szabad helyek gyorsan fogynak, így kérjük foglalási szándékát a szállás hely felé mihamarabb jelezze.</b><br/><br/>";
$lang['final_letter_footer'] = "<br/><br/><a href='https://www.facebook.com/groups/szallasoutlet/' target='_blank' style='text-decoration:none;'>Szeretne első kézből értesülni titkos ajánlatainkról? Lépjen be a zártkörű SzállásOutlet Klubba, ahol limitált számban, exkluzív, sehol máshol meg nem jelenő ajánlatainkat is foglalhatja. »</a><br/><br/>
<span style='font-size:10px'>A fenti fizetési határidő, a szálláshely fizetési határidő utáni elfoglalása esetén érvényes. <br/>
Amennyiben a szállodai tartózkodás megkezdése a meghatározott fizetési határidő előtt történik, úgy az utalvány ellenértékének rendezése a szállás elfoglalása előtt két nappal esedékes. <br/>
Kérjük, amennyiben átutalással rendezi a fizetést, szíveskedjen figyelembe venni az átutalás átfutási idejét. </span>
";

$lang['contact_mode'] = "Foglalási szándékát kérjük az alábbi elérhetőségeken jelezze:";
$lang['pmode'] = 'Ön az alábbi fizetési módot választotta:';
$lang['smode'] = 'Ön az alábbi átvételi módot választotta:';
$lang['invoice_total'] = "Fizetendő összeg:";
$lang['due_date'] = "Fizetési határidő:";
$lang['befast'] = "<b>A szabad helyek gyorsan fogynak, így kérjük foglalási szándékát a szállás hely felé mihamarabb.</b><br/>";

$lang['transaction_closed'] = "A megadott utalványt már letöltötték!";
$lang['more_voucher'] = "További utalványok";
$lang['voucher_id'] = "Utalványszám";

$lang['officecode'] = "Irodakód";

$lang['qsubject'] = "Szállás Outlet kérdés";

$lang['qbody'] =  "Az alábbi időpontban (#ADDED#) feltett  kérdésére a válaszunk:<br/>
			<b>#QUESTION#</b><br/><br/>
			#REPLY#<br/><br/>
			
			<b><font color=red>Kérjük, hogy a gyorsabb válasz érdekében további kérdéseit az alábbi oldalon tegye fel:</font>
			<br/><br/>#URL#</b>
";
$lang['adminemail'] = 'vtl@indulhatunk.hu'; 
$lang['adminname'] = 'Szállás Outlet'; 
$lang['countrycode'] = 'hu';
$lang['countrycodelong'] = 'hu.';
$lang["dear"] = 'Kedves';
$lang["thanks"] = 'Köszönjük, hogy minket választott - szallasoutlet.hu ';
$lang["signature"] = 'Üdvözlettel: az indulhatunk csapata';
$lang["szallasurl"] = "szallasoutlet.hu";
$lang['offer_name'] = 'Ajánlat neve';
$lang['ask'] = 'Kérdezek &raquo;';
$lang['yourquestion'] = 'Az Ön kérdése';
$lang['prefix'] = '';
$lang['replysoon'] = 'Köszönjük kérdését, hamarosan válaszolunk!';


$lang['offers'] = 'Ajánlatok';
$lang['activeoffers'] = 'Aktív ajánlatok';
$lang['alloffers'] = 'Összes ajánlat';
$lang['translation'] = 'fordítás';
$lang['fullrooms'] = 'teltház';
$lang['showoffer'] = 'megtekint';
$lang['edit'] = 'Szerkeszt';
$lang['shortcat'] = 'Kat.';
$lang['shorttrans'] = 'Ford.';

$lang['romaniantrans'] = 'Román fordítás';
$lang['slovakiantrans'] = 'Szlovák fordítás';
$lang['template'] = 'Template';
$lang['outername'] = 'Ajánlat VTL neve';
$lang['offer_name'] = 'Ajánlat neve';
$lang['offer_shortname'] = 'Ajánlat rövid neve';
$lang['room_extra'] = 'szoba felár';
$lang['other_extra'] = 'egyéb felár';
$lang['single_extra'] = 'egyszeri felár';
$lang['comparison'] = 'Összehasonlítás';
$lang['add'] = 'Beszúrás';
$lang['remove'] = 'Törlés';
$lang['hungarian'] = 'Magyar';
$lang['check'] = 'Ellenőrzés';
$lang['save'] = 'Mentés';
$lang['missing'] = 'Hiányzik';
$lang['romanian'] = 'Román';
$lang['slovak'] = 'Szlovák';

$lang['specnotification'] = '<!--<div id="leftSide">
			<div class="leftTop">Ünnepi nyitvatartás - Irodánk címe: 1056 Budapest, Váci utca 9. 21-es kapucsengő</div>
			<div class="leftItem">
<table style="width:500px;margin:0 auto;">
	<tr>
		<td>
		<b>december 7. szombat:</b> 8-18 óráig<br/>
<b>december 8. vasárnap:</b> 	10-18 óráig<br/>
<b>december 14-15.: </b>		10-18 óráig<br/>
<b>december 21. szombat:</b> 8-18 óráig<br/></td>
		<td width="30"></td>
		<td>
<b>december 22. vasárnap:</b> 10-18 óráig<br/>
<b>december 23. hétfő:</b>	8-18 óráig<br/>
<b>december 24-január 1.</b>	Zárva<br/>
<b>nyitás:</b> január 2. csütörtök </td>
		
	</tr>
</table>



</div>
			
			</div>-->';
			
$lang['cntrct'] = 'Sz.';


$lang['paysubject'] = "Fizetés teljesítése";
$lang['paybody'] = "Köszönjük a vásárlását.<br/>
	Befizetését lekönyveltük.<br/><br/>
	
	Kérjük az utalványt nyomtatott formában leadni az érkezéskor a recepción. Ha nem találja vagy kérdése van, vegye fel a kapcsolatot az ügyfélszolgálatunkkal.<br/><br/>
	
	Kívánjuk, hogy az ajándékutalványunk felhasználása sok örömet okozzon, és kellemes időtöltés mellett egy igazán jót pihenjen.<br/><br/>
	Reméljük hamarosan újra a vásárlóink között köszönthetjük.<br/><br/>
	
	<br/><br/><a href='https://www.facebook.com/groups/szallasoutlet/' target='_blank' style='text-decoration:none'>Legyen tagja a zártkörű SzállásOutlet Klubnak, ahol korlátozott számban foglalható titkos ajánlatainkat érheti el! Különleges akciók, meghosszabbított foglalási időszak, csak itt elérhető ajánlatok. Érdemes csatlakozni! »</a><br/><br/>
	
	
	
	
Foglalási szándékát kérjük az alábbi telefonon jelezze:<br/><br/>";

$lang['referenceto'] = "Kérjük hivatkozzon az alábbi sorszámra a foglalása során:";
$lang['lastdel'] = "Utolsó 15 törölt VTL utas";
$lang['basedonhotel'] = "*a megadott adatok tájékoztató jellegűek, a szálloda recepciója által megadott információn alapul!";
$lang['secretclub'] = "";
// "<a href='http://facebook.com/groups/szallasoutlet/' target='_blank'><img src='http://admin.indulhatunk.hu/images/secret2.png' style='margin:10px 0 0 0'/></a>";

$lang['cpaytext'] = "ÖN A BANKKÁRTYÁS FIZETÉSI MÓDOT VÁLASZTOTTA!";
$lang['cpaycomment'] = "<ul>
			<li>- A tovább gombra kattintva átirányítjuk a PayU fizetési felületére!</li>
			<li>- További információ: <a href='http://www.szallasoutlet.hu/felhasznalasi-feltetelek#szepinfo'>itt</a>.</li>

			<li>- Probléma, reklamáció esetén kérjük tájékoztasson minket a <b>vtl@indulhatunk.hu</b> címen!</li>
		</ul>";
		
$lang['voucher'] = "utalvány";


$lang['topay'] = "Fizetendő összeg";
$lang['paynext'] = "Tovább a fizetési felületre";
$lang['item'] = 'tétel';
$lang['currency'] = '';

$lang['invoice_letter_subject'] = "Indulhatunk.hu számla - ";
$lang['invoice_letter_content'] = "A Mellékletben találja a PDF formátumú elektronikus számlát.<br/><br/>Kellemes pihenést kivánunk!";


$lang["#TOPBANNERLETTER#"] = "
";
$lang['extra_discount'] = "Extra kedvezmény, kupon kód:";
?>