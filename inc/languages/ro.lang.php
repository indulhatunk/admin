<?

/*
-----------------
Language: Hungarian
-----------------
*/

$lang = array();

$lang[currency] = 'RON';
$lang['login'] = "Intrare";
$lang['username'] = "Nume utilizator";
$lang['password'] = "Parola";
$lang['invaliduserlogin'] = "Nume sau Parolă incorectă";
$lang['vatera_customers'] = "Clienţi Cazare Outlet";
$lang['lmb_customers'] = "Clienţi LMB";
$lang['invoices'] = "Facturi";
$lang['reviews'] = "Evaluări";
$lang['tickets'] = "Tichet greşeli";
$lang['phonebook'] = "Agenda telefon";
$lang['other'] = "Altele";
$lang['customers'] = "Clienţi";
$lang['yes'] = "Da";
$lang['no'] = "Nu";
$lang['delete_item'] = "Ştergere?";
$lang['deleted'] = "Ştergere reusită";
$lang['customer_added'] = "Adăugare client reusită";
$lang['customer_edited'] = "Editare client reusită!";
$lang['home'] = "Pagina principală";
$lang['manage_customers'] = "Gestionare clienţi";
$lang['statistics'] = "Statistică";
$lang['new_customer'] = "Introducere client nou";
$lang['postpaid'] = "Ramburs";
$lang['cash'] = "Cash";
$lang['letterfooter'] = "<b>E-mail:</b> booking@cazareoutlet.ro  |  <b>Telefon:</b> +40 373 781 939";
$lang['#DOWNLOADAPP#'] = "";
$lang['#LETTERAPP#'] = "";
$lang['#CONTACTUS#'] = "Contactați-ne";
$lang['transfer'] = "Transfer bancar";
$lang['delivery'] = "Courier";
$lang['t_check'] = "T. check";
$lang['online'] = "Online";
$lang['o_online'] = "online";
$lang['facebook'] = "Facebook";
$lang['credit_card'] = "Card credit";
$lang['place'] = "Pe loc";
$lang['logout'] = "Ieşire";
$lang['settings'] = "Setări";
$lang['welcome'] = "Bine aţi venit";
$lang['print'] = "printare";
$lang['notify'] = "notificare";
$lang['print_vouchers'] = "Printare Voucher";
$lang['print_envelopes'] = "Printare plicuri";
$lang['print_post'] = "Printare postate";
$lang['export'] = "Export";
$lang['active'] = "Activ";
$lang['paid_printed'] = "Plătit + Emis";
$lang['notpaid_printed'] = "Emis + <strike>Plătit</strike>";
$lang['paid_notprinted'] = "Plătit";
$lang['billed'] = "Facturat";
$lang['select'] = "Alegeţi";
$lang['no_results'] = "Nici o găsire!";
$lang['id'] = "ID";
$lang['date'] = "Data";
$lang['customer_name'] = "Nume client";
$lang['partner'] = "Partener";
$lang['item_desc'] = "Descriere licitaţie";
$lang['price'] = "Preţ";
$lang['payment'] = "Modalitate de plată";
$lang['customer_left'] = "Ieşit";
$lang['paid'] = "Plătit";
$lang['tools'] = "Mijloace";
$lang['post'] = "Poştă";
$lang['bill'] = "Factură";
$lang['comment'] = "Observaţii";
$lang['address'] = "Adresă";
$lang['bill_address'] = "Adresă factura";
$lang['bill_name'] = "Nume factură";
$lang['plus_services'] = "Servicii suplimentare";
$lang['paid_date'] = "Data plăţii";
$lang['print_date'] = "Data emiterii";
$lang['deadline'] = "Termen de plată";
$lang['phone'] = "Telefon";
$lang['email'] = "E-mail";
$lang['offer'] = "Ofertă";
$lang['offer_type'] = "Tipul ofertei";
$lang['offer_price'] = "Preţ original";
$lang['validity'] = "Valabilitate";
$lang['not_include'] = "Nu conţine";
$lang['customer_details'] = "Date cumpărător";
$lang['change_laguage'] = "Alege limba";
$lang['halfboard'] = "Demipensiune";
$lang['weekend_extra'] = "Suprapreţ weekend";
$lang['bed_extra'] = "Pat suplimentar pt. adulți";
$lang['more_night'] = "Rămâneţi incă o noapte";
$lang['birthday'] = "Ziua de naştere";
$lang['fullhouse'] = "Date pline";
$lang['months'] = "Array";
$lang['not_paid'] = "Nu este plătit!";
$lang['customer_booked'] = "Confirmat";
$lang['customer_arrival'] = "Sosire";
$lang['easypassword'] = "Parola Dumneavoastră este prea simplă! Vă rugăm schimbați-o imediat!";
$lang['invoicerequest'] = "Încă nu ați trimis toate facturile solicitate. Vă rog să faceți cât mai curând. Aflați mai multe!";
$lang['lmbinvoice'] = "În soldul LMB există datorare.";
$lang['fullhouserequest'] = "ATENȚIE! Nu mai sunt locuri / Full house";
$lang['fullhousenotification'] = "Tabelul cu gradele de ocupare al hoteluli actualizat de Dv. oferă informații proaspete și actuale Clienților noștri, care decid asupra achiziționării în funcție de numărul de locuri libere și perioadele ocupate.<br/><br/>

Actualizarea permanentă și corectă a gradului de ocupare este important atât pentru noi cât și pentru Dv. în vederea maximalizării numărului de rezervări și a minimalizării anulărilor. <br/><br/>

<b>Vă mulțumim pentru colaborare și ajutor!</b> <br/><br/>";
$lang['booking_details'] = "Disponibilitate";
$lang['fullrooms'] = "epuizat";
$lang['freerooms'] = "Disponibil";
$lang['roomdetails'] = "*Disponibilitatea afișată este valabilă, pentru tipul de cameră afișat în prețul de bază al ofertei!<br/>
	*Dacă doriți alt tip de cameră, vă rugăm să ne contactați. Informațiile afișate au caracter informativ!<br/>";
$lang['prefix'] = "ro_";
$lang['reference_text1'] = "La rezervare, vă rog să referați la numărul voucherului.";
$lang['reference_text2'] = "Puteți să rezervați acum, dar finalizarea este posibil numai după plată.";
$lang['booking_info'] = "Rezervați aici:";
$lang['final_menu1'] = "Vreau să rezerv!";
$lang['final_menu2'] = "Am cartelă Kogălniceanu!";
$lang['final_menu3'] = "Când primesc voucherul?";
$lang['final_menu4'] = "Am cumpărat în dar!";
$lang['final_map'] = "Hartă";
$lang['final_welcome'] = "Stimate";
$lang['final_welcometext'] = "Completați formularul de mai jos să vă salvați de munca suplimentară.";
$lang['final_endtext'] = "Câmpurile marcate cu * sunt obligatorii! După efectuarea plății informațiile de facturare nu putem modifica.";
$lang['final_endtext2'] = "Răspunsurile despre spațiul liber sunt orientative. Răspunsurile sunt bazate pe cele mai recente informații. Despre datele disponibile și informații specifice care nu sunt incluse în ofertă informațiile vor vi furnizate de către hotel.";
$lang['final_title'] = "Finalizare";
$lang['offer_details'] = "2. Prețuri suplimentare, alte servicii";
$lang['final_save'] = "Salvare";
$lang['final_name'] = "Numele";
$lang['final_address'] = "Srada și numărul";
$lang['final_city'] = "Codul poștal, oraș";
$lang['final_email'] = "e-mail";
$lang['final_phone'] = "Telefon";
$lang['final_birth'] = "Data nașterii";
$lang['final_company_invoice'] = "Factură pe firmă";
$lang['personal_data'] = "Date personale";
$lang['final_invoice_details'] = "Date facturare";
$lang['invoice_name'] = "Nume facturare";
$lang['invoice_tax'] = "Codul fiscal";
$lang['invoice_address'] = "Strada, numărul";
$lang['invoice_city'] = "Codul poștal, oraș";
$lang['next'] = "Mai departe";
$lang['payment_info'] = "3. Informații despre plată";
$lang['payment_method'] = "Plata și livrare";
$lang['payment_mode'] = "Modalitatea de plată";
$lang['shipment_mode'] = "Modalitatea de livrare";
$lang['final_footer'] = "";
$lang['message'] = "Mesaj către vânzător";
$lang['cc_text'] = "Puteți plăti online cu cardul de credit, după apăsarea butonului Salvare. Save!";
$lang['cc_text2'] = "Cu card de credit puteți plăti online!";
$lang['cash_text'] = "Biroul:";
$lang['transfer_text'] = "Puteți plăti prin transfer bancar.";
$lang['online_text'] = "Descărcați voucherul acum!";
$lang['post_text'] = "După sosirea plății ne vom trimite voucherul prin poștă.";
$lang['personal_text'] = "Biroul:";
$lang['szep_text'] = "Plătiți cu cardul Kogălniceanu";
$lang['message2'] = "Informații privind disponibilitatea.";
$lang['payments_normal'] = "";
$lang['payments_szep'] = "";
$lang['shipment_normal'] = "";
$lang['shipment_special'] = "";
$lang['contact_seller'] = "Mesaj către vânzător";
$lang['extraprices'] = "Alte servicii";
$lang['offer_id'] = "Numărul voucherului";
$lang['plusnight'] = "/ noapte";
$lang['child1'] = "1. anișorii copilului";
$lang['child2'] = "2. anișorii copilului";
$lang['child3'] = "3. anișorii copilului";
$lang['please_choose'] = "Vă rugăm selectați";
$lang['child_text1'] = "până la";
$lang['child_text2'] = "între";
$lang['child_text3'] = "după";
$lang['room_extra'] = "preț suplimentar de cameră";
$lang['extra_price'] = "preț suplimentar";
$lang['asagift'] = "Nu afișați prețul ofertei pe voucher";
$lang['package'] = "pachet";
$lang['halfboard_weekend'] = "Demipensiune + pat suplimentar";
$lang['required'] = "obligatorii";
$lang['totals'] = "Total";
$lang['voucher_info'] = "Datele voucherului";
$lang['normal_price'] = "Preț de bază";
$lang['correct_fields'] = "Vă rugăm completați toate câmpurile!";
$lang['use_next'] = "Vă rugăm, utilizați butonul Mai departe!";
$lang['iwantbook'] = "Vă rog să rezervați la numărul de telefon menționat în secția verde.";
$lang['iwantbook2'] = "Când faceți rezervarea, menționați numărul voucherului";
$lang['szepcard'] = "";
$lang['wheni'] = "Puteți să primiți voucherul chiar acum, putați să descărcați!";
$lang['giftok'] = "Voucherul poate fi dăruit. Voucherul nu poate fi schimbat pentru bani, redistribuirea este strict interzis.";
$lang['calc_storno'] = "Calculat QBE - asigurare de anulare";
$lang['calculated_storno'] = "Asigurare de anulare";
$lang['passenger1_name'] = "1. Numele pasagerului";
$lang['passenger2_name'] = "2. Numele pasagerului";
$lang['passenger3_name'] = "3. Numele pasagerului";
$lang['passenger4_name'] = "4. Numele pasagerului";
$lang['passenger5_name'] = "5. Numele pasagerului";
$lang['passenger6_name'] = "6. Numele pasagerului";
$lang['insurance_accept'] = "Sunt de acord cu termenii de anulare.";
$lang['insurance_top'] = "Vă rog să completați numele pasagerilor conform buletin.";
$lang['please_transfer'] = "Vă rugăm să utilizați următorul cont bancar:";
$lang['please_transfer2'] = "Când faceți transfer bancar, menționați numele și numărul voucherului.";
$lang['postpaid_text'] = "";
$lang['online_special'] = "";
$lang['download1'] = "Descărcați voucherul la linkul de mai jos:";
$lang['download_btn'] = "Descărcare";
$lang['personal_shipment'] = "Ridicare personală";
$lang['simple_post'] = "Scrisoare recomandată";
$lang['final_top'] = "Vă mulțumim că ați completat formularul.";
$lang['final_letter_top'] = "";
$lang['final_letter_center'] = "";
$lang['final_letter_footer'] = "";
$lang['contact_mode'] = "Vă rog să rezervați la numărul de telefon:";
$lang['pmode'] = "Ați ales următoarea modalitate de plată:";
$lang['smode'] = "Ați ales următorul modul de primire:";
$lang['invoice_total'] = "Suma:";
$lang['due_date'] = "Termen de plată:";
$lang['befast'] = "Vă rugăm să efectuați rezervarea la hotel în cel mai scurt timp posibil.";
$lang['transaction_closed'] = "Voucherul a fost descărcat!";
$lang['more_voucher'] = "Alte vouchere";
$lang['voucher_id'] = "Numărul voucherului";


$lang[qsubject] = "Cazare Outlet: Intrebare";

$lang[qbody] =  "

La intrebarile din data (#ADDED#) raspunsul nostru este:<br/>
			<b>#QUESTION#</b><br/><br/>
			#REPLY#<br/><br/>
			
			<b><font color=red>In interesul raspunselor mai rapide, va rugam ca intrebarile ulterioare sa le scrieti pe urmatorul site:</font>
			<br/><br/>#URL#</b>
";
$lang[adminemail] = 'office@cazareoutlet.ro'; 
$lang[adminname] = 'Cazare Outlet'; 
$lang[countrycode] = 'ro';
$lang['countrycodelong'] = 'ro.';
$lang["dear"] = 'Stimată/Stimate';
$lang["thanks"] = 'Vă mulțumim, că ne-ați ales pe noi  - cazareoutlet.ro';
$lang["signature"] = 'Cu respect: echipa Cazare Outlet';
$lang["szallasurl"] = "cazareoutlet.ro";

$lang[offer_name] = 'Oferta Denumire';
$lang[ask] = 'Cere &raquo';
$lang[yourquestion] = 'Intrebarea dvs';
$lang[replysoon] = 'Vă mulțumesc pentru întrebare, răspunde în curând';

$lang['offers'] = 'Oferte';
$lang['activeoffers'] = 'Oferte active';
$lang['alloffers'] = 'Toate Ofertele';
$lang['translation'] = 'traduceri';
$lang['fullrooms'] = 'Epuizat';
$lang['showoffer'] = 'vizualizare';
$lang['edit'] = 'Editare';
$lang[shortcat] = 'Kat.';
$lang[shorttrans] = 'Trad..';

$lang['romaniantrans'] = 'Traducere, Romana';
$lang['slovakiantrans'] = 'Traducere, Slovaca';
$lang['template'] = 'Template';
$lang['outername'] = 'Denumire oferta'; 
$lang['offer_name'] = 'Denumire oferta';
$lang['offer_shortname'] = 'denumire scurta, oferta';
$lang['room_extra'] = 'supliment camera';
$lang['other_extra'] = 'supliment extra';
$lang['single_extra'] = 'supliment unic';
$lang['comparison'] = 'Comparare';
$lang['add'] = 'inserare';
$lang['remove'] = 'Eliminare';
$lang['hungarian'] = 'Maghiar';
$lang['check'] = 'Control';
$lang['save'] = 'Salvare';
$lang['missing'] = 'Pierdut';
$lang['romanian'] = 'Roman';
$lang['slovak'] = 'Slovaca';



$lang[paysubject] = "Plata a fost realizat";
$lang[paybody] = "Plata Dvs. a fost luat în considerare.<br/>
Dorim că utilizarea voucherului să vă provoacă o ședere plăcută cu o mulțime de plăcere și odihnă.<br/>
Sperăm că în curând veți reveni din nou la rândul clienților noștri.<br/>
Pentru rezervare vă rugăm să contactați:<br/><br/>";

$lang[referenceto] = "Kérjük hivatkozzon az alábbi sorszámra a foglalása során:";


$lang['paymentnotification_subject1'] = "Înștiințare de plată";
$lang['paymentnotification_subject2'] = "Somație de plată";

$lang['paymentnotification_1'] = "termen de plată scadent în 2 zile.";
$lang['paymentnotification_2'] = "termenul de plată expiră azi.";
$lang['paymentnotification_3'] = "termen de plată expirat de o săptămână.";
$lang['paymentnotification_4'] = "termen de plată expirat de 2 săptămâni.";

$lang['paymentnotification'] = '
Stimate (ă) #NAME#! <br/>

Vă reamintim scadența voucherului Dumneavoastră: #NOTIFICATION#<br/><br/>

<b>Numele ofertei: #OFFER#</b><br/>
<b>Suma de plată: #PRICE#</b><br/><br/>

<b>Vă rugăm, să achitați contravaloarea comenzii confirmate printr-una dintre modalitățile de mai jos. După primirea confirmării aveți la dispoziție 24 de ore pentru efectuarea plății!</b><br/><br/>

<b>Virament bancar:</b><br/>
Vă rugăm să virați suma de plată în următorul cont:<br/>
#COMPANY#<br/>
BCR:  #BANK#<br/><br/>


În cazul în care ați achitat deja suma respectivă înainte de a primi prezenta înștiințare, <br/>
vă rugăm să nu luați în considerare prezenta scrisoare.<br/><br/>

Cu respect:<br/><br/>
office@cazareoutlet.ro<br/>
Tel.: +40 373 781 939<br/><br/>';


$lang['invoice_letter_subject'] = "Factura - ";
$lang['invoice_letter_content'] = "In atasament gasiti factura electronica in format PDF!<br/><br/>Va dorim odihna placuta!";




?>