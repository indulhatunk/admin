<?
$lang = array();
$lang['login'] = "Login";
$lang['username'] = "Meno";
$lang['password'] = "Heslo";
$lang['invaliduserlogin'] = "Neplatné meno alebo heslo!";
$lang['vatera_customers'] = "Zákazníci Hotel Outlet SK";
$lang['lmb_customers'] = "LMB zákazníci";
$lang['invoices'] = "Účty, faktúry";
$lang['reviews'] = "Hodnotenia";
$lang['tickets'] = "Lístky";
$lang['phonebook'] = "Tel.zoznam";
$lang['other'] = "Iné";
$lang['customers'] = "Zákazníci";
$lang['yes'] = "Áno";
$lang['no'] = "Nie";
$lang['delete_item'] = "Určite chcete položku vymazať?";
$lang['deleted'] = "Položka bola vymazaná!";
$lang['customer_added'] = "Zákazník pridaný";
$lang['customer_edited'] = "Zmena údajov zákazníka úspešná!";
$lang['home'] = "Hlavná stránka";
$lang['manage_customers'] = "Správa zákazníkov";
$lang['statistics'] = "Štatistika";
$lang['new_customer'] = "Pridať nového zákazníka";
$lang['postpaid'] = "Dobierka";
$lang['cash'] = "Hotovosť";
$lang['transfer'] = "Prevod";
$lang['delivery'] = "Kuriér";
$lang['t_check'] = "Üdülési csekk";
$lang['online'] = "Online";
$lang['o_online'] = "online";
$lang['facebook'] = "Facebook";
$lang['credit_card'] = "Platobná karta";
$lang['place'] = "Na mieste";
$lang['letterfooter'] = "<b>E-mail:</b> booking@hoteloutlet.sk | <b>Telefonné číslo:</b> +421 908 093 577 |  <b>Otváracia doba:</b> Po-Pi: 8.00-18.00 ";
$lang['logout'] = "odhlásiť";
$lang['settings'] = "nastavenia";
$lang['#DOWNLOADAPP#'] = "";
$lang['#LETTERAPP#'] = "";
$lang["#CONTACTUS#"] = "Kontaktujte nás";
$lang['welcome'] = "Vitajte";
$lang['print'] = "tlačiť";
$lang['notify'] = "notifikácia";
$lang['print_vouchers'] = "Tlačiť poukážku";
$lang['print_envelopes'] = "Tlačiť obálku";
$lang['print_post'] = "Feladóv. nyomtatása";
$lang['export'] = "Exportovať";
$lang['active'] = "Aktívne";
$lang['paid_printed'] = "Zaplatené + vystavené";
$lang['notpaid_printed'] = "Vystavené + <strike>zaplatené</strike>";
$lang['paid_notprinted'] = "Zaplatené";
$lang['billed'] = "Účtované";
$lang['select'] = "Prosím vyberte si";
$lang['no_results'] = "Žiadne výsledky!";
$lang['id'] = "ID";
$lang['date'] = "Dátum";
$lang['customer_name'] = "Meno zákazníka";
$lang['partner'] = "Partner";
$lang['item_desc'] = "Popis položky";
$lang['price'] = "Cena";
$lang['payment'] = "Spôsob platby";
$lang['customer_left'] = "Odišiel";
$lang['paid'] = "Zaplatené";
$lang['tools'] = "Nástroje";
$lang['post'] = "Pošta";
$lang['bill'] = "Účet";
$lang['comment'] = "Poznámka";
$lang['address'] = "Adresa";
$lang['bill_address'] = "Fa. adresa";
$lang['bill_name'] = "Fa. meno";
$lang['plus_services'] = "Služby naviac";
$lang['paid_date'] = "Dátum platby";
$lang['print_date'] = "Dátum vystavenia";
$lang['deadline'] = "Splatnosť";
$lang['phone'] = "Telefónne číslo";
$lang['email'] = "E-mail";
$lang['offer'] = "Ponuka";
$lang['offer_type'] = "Typ ponuky";
$lang['offer_price'] = "Pôvodná cena";
$lang['validity'] = "Platnosť";
$lang['not_include'] = "Nezahŕňa";
$lang['customer_details'] = "1. Údaje zákazníka";
$lang['change_laguage'] = "Vybrať jazyk";
$lang['halfboard'] = "Polpenzia";
$lang['weekend_extra'] = "Víkendová akcia";
$lang['bed_extra'] = "Akcia na prístelok (dospelí)";
$lang['more_night'] = "Ostaňte ešte jednu noc";
$lang['birthday'] = "Narodeniny";
$lang['fullhouse'] = "Plne obsadené";
$lang['months'] = "Array";
$lang['not_paid'] = "Nezaplatené!";
$lang['customer_booked'] = "Potvrdené";
$lang['customer_arrival'] = "Príchod";
$lang['easypassword'] = "Vaše heslo je príliš krátke! Prosím, ihneď ho zmeňte. Ďalšie informácie »";
$lang['invoicerequest'] = "Doposiaľ ste nám nezaslali všetky faktúry. Prosím, bezodkladne doložiť! Ďalšie informácie »";
$lang['lmbinvoice'] = "Na vašom LMB zostatku evidujeme nedoplatok. Prosím, bezodkladne vyrovnať! Ďalšie informácie »";
$lang['fullhouserequest'] = "POZOR - Plne obsadené";
$lang['fullhousenotification'] = "<b>Zoznam </b> údajov o obsadenosti ktorý spravujete, poskytuje okamžité a aktuálne informácie našim Zákazníkom, ktorí sa rozhodujú o kúpe na základe voľných miest a obsadenosti <br/><br/>

Neustála správa presných údajov o obsadenosti <b>je našim spoločným záujmom</b> pre maximalizáciu rezervácií a <b>a minimalizáciu stornovaných rezervácií.</b> <br/><br/>

<b>Ďakujeme Vám za pomoc a spoluprácu!</b> <br/><br/>";
$lang['booking_details'] = "Údaje obsadenosti";
$lang['freerooms'] = "voľné miesta";
$lang['roomdetails'] = "*Údaje o obsadenosti sa vzťahujú na izby rezervované v základnej cenovej!<br/>
	*Pokiaľ máte záujem o iné izby, prosím kontaktujte našich správcov!<br/>
	*Uvedené údaje majú orientačný charakter!";
$lang['prefix'] = "sk_";
$lang['reference_text1'] = "";
$lang['reference_text2'] = "Rezerváciu môžete realizovať už teraz, ale dokončená bude až po zaplatní";
$lang['booking_info'] = "Rezervácie tu:";
$lang['final_menu1'] = "Chcem rezervovať!";
$lang['final_menu2'] = "";
$lang['final_menu3'] = "Kedy obdržím poukážku?";
$lang['final_menu4'] = "Chcem poukážku darovať!";
$lang['final_map'] = "pohľadať na mape";
$lang['final_welcome'] = "Vážená/ý#NAME#";
$lang['final_welcometext'] = "Vyplňte nasledujúci dotazník, vyhnete sa tým množstvu zbytočnej práce a telefonovania.";
$lang['final_endtext'] = "- Políčka označené *sú povinné! Po zaplatení poukážky nie je možné zmeniť fakturačné údaje!";
$lang['final_endtext2'] = "- Odpovede na otázky zákazníkov týkajúce sa predovšetkým voľných miest majú informatívny  charakter. Poskytovateľ odpovedá na otázky podľa svojho najlepšieho vedomia a najnovších informácií. V súvislosti s voľnými termínmi a špecifickými informáciami, ktoré nie sú zahrnuté v ponuke, sú smerodajné údaje poskytnuté hotelom.";
$lang['final_title'] = "Hotel Outlet SK dokončenie nákupu";
$lang['offer_details'] = "2. Polovičné ceny, ostatné služby";
$lang['final_save'] = "Uložiť";
$lang['final_name'] = "Meno";
$lang['final_address'] = "Ulica, číslo";
$lang['final_city'] = "PSČ, obec / mesto";
$lang['final_email'] = "E-mail";
$lang['final_phone'] = "Telefónne číslo";
$lang['final_birth'] = "Dátum narodenia";
$lang['final_company_invoice'] = "Chcem faktúru na firmu";
$lang['personal_data'] = "Osobné údaje";
$lang['final_invoice_details'] = "Fakturačné údaje";
$lang['invoice_name'] = "Fakturačné meno";
$lang['invoice_tax'] = "DIČ";
$lang['invoice_address'] = "Ulica, číslo";
$lang['invoice_city'] = "PSČ, obec / mesto";
$lang['next'] = "ďalej »";
$lang['payment_info'] = "3. Údaje platby";
$lang['payment_method'] = "Spôsob platby a doručenia";
$lang['payment_mode'] = "Spôsob platby";
$lang['shipment_mode'] = "Spôsob doručenia";
$lang['final_footer'] = "<table border='0' cellspacing='0' cellpadding='0' style='width:985px;margin:0 auto;'>
		<tr>
			
			<td>
			
			</td>
			<td width='350' align='center'></td>
			<td>
				<a href='http://payu.hu/view/smarty/uploaded_images/120104100228-informacie-o platbe-20120102.pdf' alt='PayU informacie pre zákazníka' title='PayU informácie pre zákazníka' target='_blank'><img src='/images/payu.png' alt='PayU'/></a>
			</td>
		</tr>
	</table>
	
	
	
	<div class='cleaner'></div>
	<div class='copyright'>Všetky práva vyhradené ©2013 Hotel Outlet SK s.r.o. | <a href='http://www.hoteloutlet.sk/podmienky-uzivania' target='_blank' style='color:black;'>Podmienky užívania</a></div>";
$lang['message'] = "Odkaz pre predajcu:";
$lang['cc_text'] = "Po kliknutí na políčko uložiť môžete platiť online platobnou kartou!";
$lang['cc_text2'] = "Môžete platiť online platobnou kratou!";
$lang['cash_text'] = "Irodánk: 1056 Budapest, Váci utca 9. (21-es kaputelefon) nyitva tartás hétfő és péntek között 8-18 óráig.";
$lang['transfer_text'] = "Cenu produktu môžete uhradiť prevodom. Pri prevode prosím uveďte číslo poukážky!";
$lang['online_text'] = "Môžete si stiahnuť poukážku!";
$lang['post_text'] = "V deň pripísania prostriedkov na náš účet odošleme poukážku poštou, čo môže trvať ~2 dni.";
$lang['personal_text'] = "Irodánk: 1056 Budapest, Váci utca 9. (21-es kaputelefon) nyitva tartás hétfő és péntek között 8-18 óráig.";
$lang['szep_text'] = "SZÉP kártyájának szállás zsebéről Online fizethet. Amennyiben egyéb zsebről szeretne fizetni, kérem vegye fel velünk a kapcsolatot!";
$lang['message2'] = "Informácie súvisiace s dohodnutím termínu poskytuje recepcia hotela!";
$lang['payments_normal'] = "<input type='radio' name='payment' value='9' class='newpayment' checked> <a href='#' title= 'Po kliknutí na políčko uložiť môžete platiť online platobnou kartou!' class='tooltip'>Platobná karta</a><br/>
			<input type='radio' name='payment' value='1' class='newpayment'> <a href='#' title= 'Cenu produktu môžete uhradiť prevodom. Pri prevode prosím uveďte číslo poukážky!' class='tooltip'>Prevod</a><br/>
			<input type='radio' name='payment' value='2' class='newpayment'> <a href='#' title='Irodánk: 1056 Budapest, Váci utca 9. (21-es kaputelefon) nyitva tartás hétfő és péntek között 8-18 óráig.' class='tooltip'>Készpénz</a><br/>";
$lang['payments_szep'] = "<input type='radio' name='payment' class='newpayment' value='10'> <a href='#' title='SZÉP kártyájának szállás zsebéről Online fizethet. Amennyiben egyéb zsebről szeretne fizetni, kérem vegye fel velünk a kapcsolatot!' class='tooltip'>OTP SZÉP kártya</a><br/>
					<input type='radio' name='payment' class='newpayment' value='11'> <a href='#' title='SZÉP kártyájának szállás zsebéről Online fizethet. Amennyiben egyéb zsebről szeretne fizetni, kérem vegye fel velünk a kapcsolatot!' class='tooltip'>MKB SZÉP kártya</a><br/>
					<input type='radio' name='payment' class='newpayment' value='12'> <a href='#' title='SZÉP kártyájának szállás zsebéről Online fizethet. Amennyiben egyéb zsebről szeretne fizetni, kérem vegye fel velünk a kapcsolatot!' class='tooltip'>K&H SZÉP kártya</a><br/>";
$lang['shipment_normal'] = "<input type='radio' name='shipment' value='2' checked/> <a href='#' title=Môžete si stiahnuť poukážku!' class='tooltip'>Online </a><br/><input type='radio' name='shipment' value='4'/> <a href='#' title='V deň pripísania prostriedkov na náš účet odošleme poukážku poštou, čo môže trvať ~2 dni. Cena: 445 Ft' class='tooltip'>Doporučene</a><br/>";
$lang['shipment_special'] = "<input type='radio' name='shipment' value='2'/><a href='#' title='Irodánk: 1056 Budapest, Váci utca 9. (21-es kaputelefon) nyitva tartás hétfő és péntek között 8-18 óráig.' class='tooltip'>Személyes átvétel</a><br/>";
$lang['contact_seller'] = "Odkaz pre predajcu";
$lang['extraprices'] = "Ostatné služby";
$lang['offer_id'] = "Číslo poukážky";
$lang['plusnight'] = "/ noc";
$lang['child1'] = "1. vek dieťaťa";
$lang['child2'] = "2. vek dieťaťa";
$lang['child3'] = "3. vek dieťaťa";
$lang['please_choose'] = "Prosím vyberte si";
$lang['child_text1'] = "rokov";
$lang['child_text2'] = "rokov";
$lang['child_text3'] = "+ rokov";
$lang['room_extra'] = "izba akcia";
$lang['extra_price'] = "akcia";
$lang['asagift'] = "Cenu nezobraziť na poukážke";
$lang['package'] = "obal";
$lang['halfboard_weekend'] = "Polpenzia + prístelok";
$lang['required'] = "povinné";
$lang['totals'] = "Spolu";
$lang['voucher_info'] = "Údaje poukážky";
$lang['normal_price'] = "Bežná cena";
$lang['correct_fields'] = "Prosím vyplňte všetky políčka!";
$lang['use_next'] = "Prosím kliknite na políčko ďalej!";
$lang['iwantbook'] = "<b>Ak máte záujem realizovať rezerváciu, oznámte to na kontaktoch uvedených v hornej zelenej čast.</b><br/><br/>";
$lang['iwantbook2'] = "Pri rezervácii prosím uveďte nasledujúce číslo poukážky:  <br/><br/>
			<b>#OFFER_ID#</b><br/><br/>
			Rezerváciu môžete realizovať už teraz, ale dokončená bude až po zaplatení.";
$lang['szepcard'] = "SZÉP kártyájának szállás zsebéről online fizethet. <br/><br/>
A további zsebek érvényességéről kérjük érdeklődjön a <a href=\"mailto:vtl@indulhatunk.hu\" style=\"color:#252525\">vtl@indulhatunk.hu</a> címen";
$lang['wheni'] = "<b>Vašu poukážku môžete dostať aj okamžite, ak si ju stiahnete.</b><br/>
(rezerváciu môžete pomocou poukážky realizovať už teraz, ale dokončená bude až po uhradení protihodnoty)<br/><br/>

<!--<b>Pri osobnom odbere </b> ju dostanete na počkanie<br/><br/>
<b>Pri doručení poštou:</b><br/>
Ju pošleme v deň pripísania prostriedkov na náš účet, čo môže trvať cca 2 dni<br/>
cena: 445 Ft--><br/><br/>";
$lang['giftok'] = "<b>Poukážka nie je na meno, počas doby platnosti ju teda môžete kedykoľvek darovať. </b><br/><br/>
	<b>DÔLEŽITÉ!</b> <br/>Poukážka sa nedá zameniť za peniaze, jej ďalší predaj ja zakázaný.";
$lang['calc_storno'] = "QBE poistenie pre prípad zrušenia cesty";
$lang['calculated_storno'] = "<a href=\"#stornoinsurance\" style=\"color:#252525;\" class=\"tooltip\" title=\"Nenechajte to na náhodu! Poistite sa pre prípad zrušenia cesty\" rel=\"facebox\" > QBE poistenie pre prípad zrušenia cesty (2,5%)*</a>
	<div style=\"display:none;width:500px !important;\" id=\"stornoinsurance\">
	<div style=\"font-weight:bold;font-size:18px;\">Prečo sa poistiť?</div>
	
	<div style=\"padding:20px 0 0 0;\">
	
	Pokiaľ zákazník v dôsledku choroby, úrazu alebo poškodenia nemôže vycestovať, poisťovaňa mu vyplatí časť spôsobenej škody špecifikovanú v poistnej zmluve. Podrobnosti zdôvodnenia a dokazovania prekážok sú obsiahnuté v poistnej zmluve. 
	
		
	<div style=\"padding:20px 0 0 0\">
		Ďalšie informácie: <a href=\"/documents/calc_storno.pdf\" target=\"blank\" style=\"color:#252525;font-weight:bold;\">Pravidlá poistenia pre prípad zrušenia cesty </a>
	</div>	
	</div>
	</div>";
$lang['passenger1_name'] = "meno pasažiera 1";
$lang['passenger2_name'] = "meno pasažiera 2";
$lang['passenger3_name'] = "meno pasažiera 3";
$lang['passenger4_name'] = "meno pasažiera 4";
$lang['passenger5_name'] = "meno pasažiera 5";
$lang['passenger6_name'] = "meno pasažiera 6";
$lang['insurance_accept'] = "Súhlasím s podmienkami zmluvy o zrušení cesty!";
$lang['insurance_top'] = "- Prosím zadajte mená pasažierov <b> uvedené </b> v osobných dokladoch.<br/>
- Po zaplatení nie je možné zmeniť mená pasažierov!<br/>
- Ak máte záujem poistiť sa, obráťte sa na poisťovňu QBE Atlasz!";
$lang['please_transfer'] = "<b>Prosím prevod realizujte na nasledujúci účet</b><br/><br/>";
$lang['please_transfer2'] = "Do notifikácie prosím uveďte Vaše meno a poradové číslo poukážky:<br/>";
$lang['postpaid_text'] = "<div style=\"text-align:left;color:#e6e1dc; background-color:#000000; border:solid black 1px; padding:0.5em 1em 0.5em 1em; overflow:auto;font-size:small; font-family:monospace; \"><br />
</div>";
$lang['online_special'] = "\"Túto poukážku Vám môžeme doručiť len pri osobnom odbere alebo poštou, nakoľko služba sa dá využiť výlučne poukážkou, ktorú vystavil hotel! <br/><br/>\"";
$lang['download1'] = "Stiahnite si poukážku na tomto linku:";
$lang['download_btn'] = "stiahnuť »";
$lang['personal_shipment'] = "Osobný odber";
$lang['simple_post'] = "Doporučene";
$lang['final_top'] = "Ďakujeme, že ste si nastavili údaje pre nákup.";
$lang['final_letter_top'] = "<div class='offertitle'> Ďakujeme, že ste si nastavili údaje pre nákup.</div><br/><br/>Ak máte záujem o rezerváciu, kontaktujte nás prosím na telefónnom čísle:<br/><br/>";
$lang['final_letter_center'] = "Platnú notifikáciu môžete požadovať v hoteli až po uhradení protihodnoty poukážky. <br/><b>Voľné miesta sa rýchle zapĺňajú, preto Vás žiadame, aby ste rezerváciu zrealizovali čo najskôr.</b><br/><br/>";
$lang['final_letter_footer'] = "<br/><br/>
<span style='font-size:10px'>Hore uvedený termín splatnosti je platný pri rezervácii ubytovania po uplynutí termínu splatnosti. <br/>
Pokiaľ sa v hoteli ubytujete pred určeným termínom splatnosti, termín splatnosti protihodnoty poukážky je dva dni pred rezerváciou ubytovania. <br/>
V prípade, že máte záujem platbu realizovať prevodom, prosím berte do úvahy dodaciu lehotu platby. </span>";
$lang['contact_mode'] = "Ak máte záujem o rezerváciu, kontaktujte nás prosím na telefónnom čísle:";
$lang['pmode'] = "Vybarli ste si nasledujúci spôsob platby:";
$lang['smode'] = "Vybrali ste si nasledujúci spôsob dodania:";
$lang['invoice_total'] = "Suma spolu:";
$lang['due_date'] = "Dátum splatnosti:";
$lang['befast'] = "<b>Voľné miesta sa rýchle zapĺňajú, preto Vás žiadame, aby ste rezerváciu zrealizovali čo najskôr.</b><br/>";
$lang['transaction_closed'] = "Táto poukážka už bola stiahnutá!";
$lang['more_voucher'] = "Ďalšie poukážky";
$lang['voucher_id'] = "Číslo poukážky:";
$lang['officecode'] = "Kód pobočky";
$lang['qsubject'] = "Hotel Outlet Otázka";
$lang['qbody'] = "Na dole uvedenej dobe (#ADDED#) dávané otázky odpovedáme:<br/>
			<b>#QUESTION#</b><br/><br/>
			#REPLY#<br/><br/>
			
			<b><font color=red>Pre rýchlejší odpoveď prosíme ďalšie otázky vykonávať na nasledovnej stránke.</font>
			<br/><br/>#URL#</b>";
$lang['adminemail'] = "info@hoteloutlet.sk";
$lang['adminname'] = "Hotel Outlet";
$lang['countrycode'] = "sk";
$lang['countrycodelong'] = "sk.";
$lang['dear'] = "Milý/á";
$lang['thanks'] = "Ďakujeme, že ste si vybrali nás - hoteloutlet.sk";
$lang['signature'] = "S pozdravom: Hotel Outlet tím";
$lang['szallasurl'] = "hoteloutlet.sk";
$lang['offer_name'] = "Názov ponuky";
$lang['ask'] = "Spýtali sa";
$lang['yourquestion'] = "Vaša otázka";
$lang['replysoon'] = "Ďakujeme za Vašu otázku, čoskoro odpovedáme!";
$lang['offers'] = "Ponuky";
$lang['activeoffers'] = "Aktívne ponuky";
$lang['alloffers'] = "Všetky ponuky";
$lang['translation'] = "preklad";
$lang['fullrooms'] = "Máme obsadené";
$lang['showoffer'] = "pohľad";
$lang['edit'] = "Upraviť";
$lang['shortcat'] = "Kat.";
$lang['shorttrans'] = "Preklad";
$lang['romaniantrans'] = "Rumunský preklad";
$lang['slovakiantrans'] = "Slovenský preklad";
$lang['template'] = "Termplate";
$lang['outername'] = "VTL názov ponuky";
$lang['offer_shortname'] = "Krátky názov ponuky";
$lang['other_extra'] = "iné poplatky";
$lang['single_extra'] = "jednorázový poplatok";
$lang['comparison'] = "Porovnanie";
$lang['add'] = "Vložiť";
$lang['remove'] = "Zmazať";
$lang['hungarian'] = "Maďarský";
$lang['check'] = "Kontrolovanie";
$lang['save'] = "Uložiť";
$lang['missing'] = "Chýba";
$lang['romanian'] = "Rumunský";
$lang['slovak'] = "Slovenský";
$lang['specnotification'] = "<div id=\"leftSide\">
			<div class=\"leftItem\">
";
$lang['cntrct'] = "Edit";
$lang['paysubject'] = "Hotel Outlet: Uskutočnenie platby";
$lang['paybody'] = "Ďakujeme za nákup!<br/>
	Vašu platbu sme zaúčtovali.<br/><br/>
	Prajeme, že využitie darčekovej poukážky Vám spôsobuje veľa radosti, a pri príjemnej zábavy naozaj dobre odpočívali. <br/><br/>
	Dúfame, že čoskoro Vás môžeme privítať znova medzi našimi zákazníkmi.<br/><br/>";
$lang['referenceto'] = "Prosím odkazujte na dole uvedenej čísle počas rezervácie.";
$lang[currency] = 'EUR';

$lang[paysubject] = "Hotel Outlet: úhrada";

$lang[paybody] = "Ďakujeme za nákup.<br/>

         Platba zaúčtovaná.<br/><br/>

         Prajeme Vám, aby ste použitím nášho darčekového vouchera zažili veľa radosti a popri príjemne strávenom čase ste si mohli poriadne oddýchnuť.br/><br/>

         Dúfame, že Vás čoskoro privítame medzi našimi zákazníkmi.br/><br/>

        

         <br/><br/><a href='https://www.facebook.com/groups/szallasoutlet/' target='_blank' style='text-decoration:none'>Staňte sa členom nášho uzavretého Klubu SzállásOutlet, kde môžete nájsť tajné ponuky v limitovanom množstve! Špeciálne akcie, predĺžené rezervačné obdobie, len tu dostupné ponuky. Oplatí sa zapojiť! »</a><br/><br/>

        

Váš úmysel rezervovať nám prosím dajte vedieť na telefónnej čisle: br/><br/>";

 

$lang[referenceto] = "Prosím, aby ste sa odvolali na poradové číslo pri Vašej rezervacii:";

$lang[lastdel] = "Posledných 15 zrušených VTL cestujúcich.";

$lang[basedonhotel] = "*Uvedené údaje majú orientačný charakter, sú založené na informáciach poskytnutých hotelom!";

$lang[secretclub] = "";

 

$lang[cpaytext] = "VYBRALI STE SI PLATBU S PLATOBNOU KARTOU!";

$lang[cpaycomment] = "<ul>

                          <li>- Po kliknutí na tlačidlo Ďalej Vás presmerujeme na platobnú stránku PayU!</li>

                          <li>- Ďalšie informácie: <a href='http://www.hoteloutlet.sk/podmienky-uzivania'>tu</a>.</li>

 

                          <li>- V prípade problémov a reklamácii nás prosím upovedomte na emailovej adrese <b>info@hoteloutlet.sk</b>!</li>

                 </ul>";

                

$lang[voucher] = "voucher";

 

$lang[topay] = "Celková suma";

$lang[paynext] = "Ďalej na platobnú stránku";

$lang[item] = 'položka';



$lang['paymentnotification_subject1'] = "Platobná upomienka";
$lang['paymentnotification_subject2'] = "Výzva na úhradu platby";

$lang['paymentnotification_1'] = "končí o 2 dni.";
$lang['paymentnotification_2'] = "končí dnes.";
$lang['paymentnotification_3'] = "je týždeň po splatnosti.";
$lang['paymentnotification_4'] = "je 2 týždne po splatnosti.";

$lang['paymentnotification'] = 'Milý/á #NAME#! <br/>
 
Oznamujeme Vám, že platobná lehota Vašej poukážky #NOTIFICATION#<br/><br/>
 
<b>Názov ponuky: #OFFER#</b><br/>
<b>Suma na zaplatenie: #PRICE#</b><br/><br/>
 
<b> Prosím uhraďte Vašu potvrdenú objednávku jedným z nasledujúcich spôsobov. Po potvrdení, najneskôr do 24 hodín, prosím uhraďte celkovú cenu poukážky.</b><br/><br/>
 
<b>Prevod:</b><br/>
Prosíme Vás, aby ste peniaze previedli na dole uvenedný bankový účet: <br/>
#COMPANY#<br/>
TATRA BANKA:  #BANK#<br/><br/>
  
V prípade, že ste už uhrdili celkovú sumu pred prijatím tejto výzvy,<br/>
považujte tento list za bezpredmetný.<br/><br/>

S pozdravom

Hotel Outlet
info@hoteloutlet,sk<br/>
Tel.: +421 908 093 577<br/><br/>";
';

$lang['invoice_letter_subject'] = "Faktúra - ";
$lang['invoice_letter_content'] = "V prílohe môžete nájsť Vašu elektronickú faktúru vo formáte PDF. Prajeme Vám príjemný pobyt!";


?>