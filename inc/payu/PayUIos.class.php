<?php
/**
 * PayU Instant Order Status
 *
 * Requests and processes order status via HTTP
 * 
 */

require_once('PayUBase.class.php');

class PayUIos extends PayUBase{

    private $status;
    private $statusResponse;
    private $orderNumber;
    
    /*
     * Constructor of PayUIos class
     * 
     * @param mixed $config Configuration array or filename
     * @param boolean $debug Debug mode
     */
    public function __construct($config, $debug=false){
        parent::__construct($config, $debug);
    }
    
    /*
     * Requests and processes status for an orderNumber
     * 
     * @param string $orderNumber Order number in merchants system
     */
    private function init($orderNumber){
        if($orderNumber != $this->orderNumber){
            $this->requestStatus($orderNumber);
            $this->processResponse();
        }
    }
    
    /*
     * Requests status for an orderNumber
     * 
     * @param string $orderNumber Order number in merchants system
     */
    private function requestStatus($orderNumber){
        $hashArray = array(
            $this->merchantId,
            $orderNumber
        );
        $hash = $this->createHashString($hashArray);
        $getParams = array(
            "MERCHANT"=>$this->merchantId,
            "REFNOEXT"=>$orderNumber,
            "HASH"=>$hash
        );

        $this->statusResponse = $this->createSimpleRequest($this->iosUrl, $getParams);
    }
    
    /*
     * Processes response
     * 
     */
    public function processResponse(){
        if ($this->checkDom()) {
            $dom = new DOMDocument();
            $dom->loadXML($this->statusResponse);
            $order = $dom->getElementsByTagName("Order");
            foreach ($order->item(0)->childNodes as $item) {
                if ($item->nodeType == 1) {
                    $this->status[$item->tagName] = $item->nodeValue;
                }
            }
        } else {
            throw new Exception("DOM is not enabled");
        }
    }
    
    /*
     * Getter method for ORDER_STATUS
     * 
     * @param string $orderNumber Order number in merchants system
     */
    public function getOrderStatus($orderNumber){
        $this->init($orderNumber);
        return $this->status["ORDER_STATUS"];
    }
    
    /*
     * Getter method for REFNO
     * 
     * @param string $orderNumber Order number in merchants system
     */
    public function getOrderPayUNumber($orderNumber){
        $this->init($orderNumber);
        return $this->status["REFNO"];
    }
    
    /*
     * Getter method for PAYMETHOD
     * 
     * @param string $orderNumber Order number in merchants system
     */
    public function getOrderPaymethod($orderNumber){
        $this->init($orderNumber);
        return $this->status["PAYMETHOD"];
    }
    
    /*
     * Getter method for ORDER_DATE
     * 
     * @param string $orderNumber Order number in merchants system
     */
    public function getOrderDate($orderNumber){
        $this->init($orderNumber);
        return $this->status["ORDER_DATE"];
    }
    
    /*
     * Getter method for ORDER_STATUS
     * 
     * @param string $orderNumber Order number in merchants system
     */
    public function getStatus($orderNumber){
        $this->init($orderNumber);
        return $this->status;
    }
    
    /*
     * Returns raw repsonse of the request in XML
     * 
     * @param string $orderNumber Order number in merchants system
     */
    public function getRawStatus($orderNumber){
        $this->init($orderNumber);
        return $this->statusResponse;
    }
}

?>
