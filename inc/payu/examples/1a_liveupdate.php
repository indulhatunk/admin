<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="hu" lang="hu">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="hu" />
    </head>
    <body>
        <?php
        /*
         * Import LiveUpdate class
         */
        require_once('../PayULiveUpdate.class.php');

        
        /*
         * Use config file
         */
        $lu = new PayULiveUpdate('config.php');
        
        /*
         * or ese config array
         */
        /*
        $lu = new PayULiveUpdate(array(
            'MERCHANT' => "MERCHANT_ID",
            'SECRET_KEY' => "S3cr3tK3y" 
        ));
        */
        
        /*
         * Add product with array
         */
        $lu->addProduct(array(
            'name' => 'Demo product 1',
            //'group' => '2', // only if group exists in cPanel
            'code' => 'pu0001',
            'info' => 'Demo product 1',
            'price' => 333, // net price
            'qty' => 5, 
            'vat' => 27, // with VAT
            //'ver' => 1
        ));

        $lu->addProduct(array(
            'name' => 'Demo product 2',
            //'group' => '1', // only if group exists in cPanel
            'code' => 'pu0002',
            'info' => 'Demo product 2',
            'price' => 777, // gross price
            'qty' => 2,
            'vat' => 0, // no VAT when sending gross price!!!
            //'ver' => 1
        ));

        /*
         * or add product with an instance of the PayUProduct class
         */
        $prod3 = new PayUProduct();
        $prod3->name = 'Demo product 3';
        //$prod3->group = '1';
        $prod3->code = 'pu0003';
        $prod3->info = 'Demo product 3';
        $prod3->price = 999;
        $prod3->qty = 4;
        $prod3->vat = 0;
        //$prod3->ver = 1;

        /*
         * Don't forget adding product after creating it!!!
         */
        $lu->addProduct($prod3);
        
        
        /*
         * Set needed fields
         * Complete list: Technical documentation 1.2
         * 
         */
        $lu->setField("BILL_FNAME", "John");
        $lu->setField("BILL_LNAME", "Doe");
        $lu->setField("BILL_EMAIL", "john.doe@example.com"); // must be valid address
        $lu->setField("BILL_PHONE", "01"); //if not available send "01" instead
        
        $lu->setField("ORDER_DATE", date("Y-m-d H:i:s"));
        $lu->setField("ORDER_REF", "UNIQUE_000004");
        
	//ADVISE: keep BACK_REF URL in session
        $lu->setField("BACK_REF", "https://mysite.com/backref.php?dummy=param");
        $lu->setField("TIMEOUT_URL", "http://mysite.com/timeout.php");
        $lu->setField("LANGUAGE", "HU");        
        
        /*
         * Generate fields and print form
         */
        $display = $lu->createHtmlForm('myForm', 'link', "NEXT");
        //"NEXT" can be replaced to any text
        //mb_convert_encoding('Tovább az Árvíztűrő tükörfúrógép oldalára',"ISO-8859-2","UTF-8")
        echo $display;
        
        
        /*
         * Generate array of fields
         */
        //$display = $lu->createPostArray();
        
        
        /*
         * Print missing fields list
         */
        //print_r($lu->getMissing());
        
        ?>
    </body>
</html>