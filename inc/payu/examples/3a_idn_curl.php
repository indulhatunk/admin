<?php
        require_once('../PayUIdn.class.php');


        $idn = new PayUIdn('config.php');

        /*
         * Complete list of fields in Technical documentation 3.1 
         */
        $idn->setField("ORDER_REF", "123456");
        $idn->setField("ORDER_AMOUNT", "1234");
        $idn->setField("ORDER_CURRENCY", "HUF");
        $idn->setField("IDN_DATE", date("Y-m-d H:i:s"));
        
        //WARNING! If REF_URL is set processing response must be done on the target address !!!
        //Example code in file 3c_idn_process.php
        //$irn->setField("REF_URL", "http://mysite.com/3c_idn_process.php");
        

        //query server via cURL
        $resp = $idn->requestCurl();
        
        //create an array from the response
        $data = $idn->processResponse($resp);
        
        //check if received data is valid
        echo $idn->checkResponseHash($data);
        
        //process response when needed
        print_r($data);
        
        //Print missing fields
        //print_r($idn->getMissing());

?>
