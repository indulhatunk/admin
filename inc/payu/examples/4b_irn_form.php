<?php
        require_once('../PayUIrn.class.php');


        $irn = new PayUIrn('config.php');

        /*
         * Add product with array
         */
        $irn->addProduct(array(
            'code' => '123456', //PayU internal ID. (sent with IPN)
            'qty' => 2
        ));

        $irn->addProduct(array(
            'code' => '123457', //PayU internal ID. (sent with IPN)
            'qty' => 1,
        ));
        

        /*
         * or add product with an instance of the PayUProduct class
         */
        $prod3 = new PayUProduct();
        $prod3->code = '123458'; //PayU internal ID. (sent with IPN)
        $prod3->qty = 1;

        /*
         * Don't forget adding product after creating it!!!
         */
        $irn->addProduct($prod3);

        
        /*
         * Set needed fields
         * Complete list: Technical documentation 4.2
         * 
         */
        $irn->setField("ORDER_REF", "123456");
        $irn->setField("ORDER_AMOUNT", "1234");
        $irn->setField("AMOUNT", "234");
        $irn->setField("ORDER_CURRENCY", "HUF");
        $irn->setField("IRN_DATE", date("Y-m-d H:i:s"));
        
        //WARNING! If REF_URL is set processing response must be done on the target address !!!
        $irn->setField("REF_URL", "http://mysite.com/4c_irn_process.php");
        
        //query server via FORM submit
        echo $irn->createHtmlForm("test_form","link","Next");
        
        //create an array from the response
        $data = $irn->processResponse($resp);
        
        //check if received data is valid
        echo $irn->checkResponseHash($data);
        
        print_r($irn->getMissing());

      
        
        
?>
