<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");

//check if user is logged in or not
userlogin();



head('Főoldal');
/*
if( $CURUSER[userclass] == 51) {
head("Főoldal");
 ?>
	<li><b>Számlázás</b>
			<ul>

				<li><a href="invoice_list.php">Számlák</a></li>			
			</ul>
		</li>
	<?
	foot();
	die; 
	}




if($CURUSER["userclass"] < 90) {
	head("Főoldal");
	
		

echo "<div class='homeleft'>"; 
		?>
		<ul>
			<li><a href="lmb.php"><?=$lang[lmb_customers]?></a></li>
			<? if($CURUSER["username"] <> 'vatera') {?>
				<li><a href="customers.php"><?=$lang[vatera_customers]?></a></li>
				<li><a href="invoices.php"><?=$lang[invoices]?></a></li>
				<li><a href="reviews.php"><?=$lang[reviews]?></a></li>
				<li><a href="fullhouse.php"><?=$lang[fullhouse]?></a></li>
				<li><a href="facebookoffers.php"><b>FacebookAJÁNLATOK</b></a></li>
			<? } ?>

			
		</ul>
		
		
<?		
if($_GET["debug"] == 1)  {
?>
	<ul>
	<li><b>COREDB ideiglenes</b>
		<ul>
			<li><a href='categories.php'>Szolgáltatás kategóriák (CSAK ADMIN)</a></li>
			<li><a href='rooms.php'>Szobatípusok</a></li>
			<li><a href='settings.php'>Partner beállítások</a></li>
			<li><a href='editoffers.php'>Ajánlatok szerkesztése</a></li>
			<li><a href='gallery.php'>Képgalériák</a></li>
		</ul>
	
	
	</li>
	</ul>
<?
}

		echo "</div>";
	echo "<div class='homenews'>";
		$query = $mysql->query("SELECT * FROM information ORDER BY id DESC LIMIT 5");
		while($arr = mysql_fetch_assoc($query))
		{
			if($CURUSER[userclass] == 255)
				$editlink = "<a href='information.php?editid=$arr[id]'>[szerkeszt]</a>";
			echo "<div class='newsbox'>";
				echo "<div class='newstitle'>$arr[title] $editlink</div>";
				echo "<div class='newscontent'>
					$arr[body]
					<center>Hozzáadva: $arr[added]</center>
				</div>";
			echo "</div>";
		}
	echo "</div>";
	echo "<div class='cleaner'></div>";
	
	foot();
	die;
}


head("Főoldal");

echo "<div class='homeleft'>"; 



if($CURUSER[vatera] == 1)  {
?>

	<ul>
		<li><b>Vatera</b>
			<ul>
				<li><a href="partners.php">Partnerek kezelése</a></li>
				<li><a href="customers.php">Vásárlók kezelése</a></li>
			<!--	<li><a href="deleted.php">Törölt vásárlók kezelése</a></li> -->
				<li><a href="inactive.php">Inakív vásárlók kezelése</a></li>
				<li><a href="emails.php">Feldolgozatlan levelek kezelése</a></li>
				
				<? if($CURUSER[userclass] == 255)  { ?>
				<li><a href="assets.php">Kintévőségek kezelése</a></li>
				<li><a href="barterstats.php">Ajánlat eladási statisztika</a></li>
				<? } ?>
				<li><a href="followup.php">Utánkövetés</a></li>

			</ul>
		</li>
	</ul>
<?
}
if($CURUSER[own] == 1)  {
?>

	<ul>
		<li><b>Saját eladás</b>
			<ul>
				<li><a href="own_vouchers.php">Saját eladás</a></li>
				<li><a href="checkout.php">Pénztár</a></li>
				<li><a href="check_list.php">Üdülési csekk alapítvány</a></li>
			</ul>
		</li>
	</ul>
<?
}
if($CURUSER[outlet] == 1)  {
?>

	<ul>
		<li><b>Szállás-Outlet</b>
			<ul>
				<li><a href="templates.php">Template-ek kezelése</a></li>
				<li><a href="getOffers.php">Ajánlatok kezelése</a></li>
				<li><a href="promotion.php">Promóciós kategóriák</a></li>
				<li><a href="images.php">Logók és képek</a></li>
				<li><a href="outlet_check.php">Outlet ajánlatok listázása</a></li>
				<li><a href="questions.php"><b>Kérdések</b></a></li>
				<li><a href="outlet_images.php"><b>Képek frissítése</b></a></li>
				<li><a href="newsletters.php"><b>Hírlevelek</b></a></li>
			</ul>
		</li>
	</ul>
<?
}
if($CURUSER[lmb] == 1)  {
?>		
	<ul>
		<li><b>Last Minute belföld</b>
			<ul>
				<li><a href="lmb.php">LMB admin</a></li>
				<li><a href="lmb_check.php">LMB ajánlatok listázása</a></li>
				<li><a href="reviews.php">Értékelések</a></li>

			</ul>
		</li>
	</ul>
<?
} 
if($CURUSER[indulhatunk] == 1)  {?>
	<ul>
		<li><b>Indulhatunk.hu</b>
			<ul>
				<li><a href="offers.php">Főoldali ajánlatok</a></li>
				<li><a href="requests.php">Ajánlatkérések</a></li>
				<li><a href="flush.php" target="_blank">Cache törlése</a></li>
			</ul>
		</li>
	</ul>
<?
}
if($CURUSER["userclass"] == 255)  {
?>
	<ul>
		<li><b>Egyéb</b>
			<ul>
				<li><a href="information.php">Főoldali hírek kezelése</a></li>
				<li><a href="news.php">Külföldi hírek kezelése</a></li>
				<li><a href="domains.php">Domainek</a></li>
				<li><a href="log.php">Log</a></li>
			</ul>
		</li>
	</ul>
<?
}
if($CURUSER["accounting"] == 1)  {
?>
	<ul>
		<li><b>Pénzügyek</b>
			<ul>
	
			<li><a href="lmb_invoice.php">LMB számlák kezelése és havi értesítők küldése</a></li>

			<?
			if($CURUSER["userclass"] == 255)  {
			?>
				<li><a href="lmb_fivedays.php">LMB értesítők (minden hónap 5.-ig)</a></li>
				<li><a href="/invoice/create_invoice_lmb.php">LMB számlázás (minden hónap 10-én)</a></li>
			
			<? } 
			if($CURUSER["userclass"] == 255)  {
			?>				
				<li><a href="weekly.php">Heti VTL számlák</a></li>
			<? }?>
			<li><a href="invoice.php">VTL Számlák kezelése</a></li>
			<li><a href="accounts.php">Partner egyenleg</a></li>			
			<?
			if($CURUSER["userclass"] == 255 || $CURUSER["userclass"] == 99 )  {
			?>
			<li><a href="statistics.php">Kimutatások</a></li>
			
			<li><a href="list_invoice.php"><font color='red'>Műveletek számlákon</font></a></li>

			<!--<li><a href="charts.php">&raquo; Grafikonok</a></li>-->
			<? }?>
			</ul>
		</li>
	</ul>
<?
}
?>
	<ul>
<?if( $CURUSER[userclass] == 98) { ?>
 	<li><a href="partners.php">Partnerek kezelése</a></li>
	<li><a href="lmb_check.php">LMB ajánlatok listázása</a></li>
	<li><a href="outlet_check.php">Outlet ajánlatok listázása</a></li>

<? } ?>
	<? if( $CURUSER[userclass]> 50) { ?>
	<li><b><?=$lang[other]?></b>
			<ul>

				<li><a href="users.php"><?=$lang[phonebook]?></a></li>
				<li><a href="ticket.php"><?=$lang[tickets]?>, fejlesztések, ötletek</a></li>
			
			</ul>
		</li>
	<? } ?>

	</ul>
	
<?
*/

if($CURUSER[is_qr] == 1 || $CURUSER[userclass] > 10)
{
?>
<a href='https://admin.indulhatunk.hu/reader.php' style='background-color:#2ea2fd;color:white;padding:10px;text-align:center;display:block;font-weight:normal;color:white; width:70px;position:fixed;right:20px; top:0;border-bottom:1px solid white; border-right:1px solid white; border-left:1px solid white'>
	
<img src='/images/qr.png' width='40'/>
<div style='padding:10px 0 0 0'>Beolvasás</div>

</a>
	<!--<table border='0'>
		<tr>
			<td width='20'></td>
			<td width='20'><img src='/images/qr.png' width='20'/></td>
			<td width='20'></td>
			<td align='left'>QR kód beolvasása</td>
		</tr>
	</table>-->
<?
}
if($CURUSER[userclass] > 50)
{
?>

<?
}

if($CURUSER[userclass] > 50)
{
?>


<a href='/fullhouse.php'>
<div class='calendarmessage' style=' color:black;'>

		<div style='font-size:20px;font-weight:bold;margin:0 0 5px 0;text-align:center;border-bottom:1px solid #df8f8f;padding:0 0 5px 0;'>FIGYELEM - Teltházas lista</div>


	<div style='float:left'><img src='/images/calendar.png'/></div>
	<div style='float:left; margin:5px 0 0 10px;width:740px;font-weight:normal;'>
	
	<div>
		
Az <b>Önök</b> által karbantartott foglaltsági adatok táblázat friss, azonnali információt nyújt Ügyfeleinknek, akik vásárlási döntésüket a szabad helyek és a foglalt időszakok alapján hozzák meg<br/><br/>

A pontos foglaltság folyamatos karbantartása <b>közös érdekünk</b> a foglalások számának maximalizálása és a <b>lemondások számának minimalizálása</b> érdekében. <br/><br/>

<b>Együttműködésüket, segítségüket köszönjük!</b> <br/><br/>

	</div>
	
	</div>
	
	<div class='cleaner'></div>
</div>
</a>
<?
}


if($_SERVER[REMOTE_ADDR] == '89.133.88.109' && $CURUSER[coredb_id] > 0)
{

}
if($CURUSER[userclass] < 5)
{

$deleted = $mysql->query("SELECT * FROM customers WHERE inactive = 1 AND inactive_visible = 1 AND (pid = '$CURUSER[pid]' OR sub_pid = '$CURUSER[pid]') ORDER BY inactive_date DESC LIMIT 15");

	if(mysql_num_rows($deleted) > 0)
	{
	?>
		<div class='content-box'>
	<div class='content-box-header'>
		<h3><?=$lang[lastdel]?></h3>
	</div>
	<div class='contentpadding'>
		<table>
			<tr class='header'>
				<td width='10'>-</td>

				<td width='120'><?=$lang['date']?></td>
				<td width='80'><?=$lang[voucher_id]?></td>
				<td width='200'><?=$lang[final_name]?></td>
				<td><?=$lang['reason']?></td>
			</tr>
		<?
		$i = 1;
		while($arr = mysql_fetch_assoc($deleted))
		{	
			$reason = explode("Törlés indoka:",$arr[comment]);
			$reason = trim(end($reason));
			echo "<tr>
				<td align='center'>$i</td>
				<td>$arr[inactive_date]</td>
				<td>$arr[offer_id]</td>
				<td>$arr[name]</td>
				<td>$reason</td>
			</tr>";
			$i++;
		}
		?>
		</table>
	</div>
	
	</div>
	
	<?
	}
}

		$query = $mysql->query("SELECT * FROM information WHERE type = 'index' ORDER BY id DESC LIMIT 5");
		while($arr = mysql_fetch_assoc($query))
		{
			if($CURUSER[userclass] == 255)
				$editlink = "<a href='information.php?editid=$arr[id]'>[szerkeszt]</a>";
				?>
				<div class='content-box'>
<div class='content-box-header'>
	<h3><? echo "$arr[title] $editlink"; ?></h3>
</div>
<div class='contentpadding'>

<?=$arr[body] ?>
<center>Hozzáadva: <?=$arr[added]?></center>
</div>

</div>
				<?
		}

	echo "<div class='cleaner'></div>";
 foot(); ?>