<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("../inc/init.inc.php");

//check if user is logged in or not
userlogin();

$db = new OCI8DB;
$dbconn = $db->dbconn();

	$offer_id = (int)$_GET[offer];
	$edit = (int)$_GET[edit];

	if($edit > 0 )
	{
			$prices = getOracleSQL("SELECT * FROM PRICES WHERE ID = '$edit'");
			$prices = $prices[0];
	}
	else
	{
		$prices[FROM_DATE] = date("Y-m-d");
		$prices[TO_DATE] = date("Y-m-d");
		$prices[DAY_NUM] = 2;
		$prices[DESCRIPTION] = 'szoba / fő / félpanzió';
	}
	
	if($offer_id > 0 && $_POST[PRICE] > 0)
	{
		if($edit == 0)
		{
			//insert
			
			$stid = oci_parse($dbconn,"INSERT INTO PRICES (
 			ID,
 			OFFER_ID, 
 			PRICE, 
 			FROM_DATE,
 			TO_DATE, 
 			DESCRIPTION,
 			DAY_NUM,
 			ROOM_TYPE_ID,
 			PLUS_DAY
 	 		) 
 		VALUES
 		(
 			PRICES_SEQ.nextval, 
 			'$_POST[OFFER_ID]', 
 			'$_POST[PRICE]', 
 			to_date('$_POST[FROM_DATE]', 'yyyy-mm-dd'),
 			to_date('$_POST[TO_DATE]', 'yyyy-mm-dd'),
 			'$_POST[DESCRIPTION]',
 			'$_POST[DAY_NUM]',
 			'$_POST[ROOM_TYPE]',
 			'$_POST[PLUS_DAY]'
 			
 		)");
 		 	
 		writelog("$CURUSER[username] added price for offer #$_POST[OFFER_ID]");
	
		oci_execute($stid);		
		oci_commit($dbconn);
		
		header("location: ../editoffers.php?editid=$offer_id&add=1&price=1");

		}
		else
		{
		//update
		$stid = oci_parse($dbconn,"UPDATE PRICES SET DESCRIPTION = '$_POST[DESCRIPTION]', PRICE = '$_POST[PRICE]', FROM_DATE = to_date('$_POST[FROM_DATE]', 'yyyy-mm-dd'), ROOM_TYPE_ID = '$_POST[ROOM_TYPE]', TO_DATE = to_date('$_POST[TO_DATE]', 'yyyy-mm-dd'), DAY_NUM = '$_POST[DAY_NUM]' WHERE ID = '$_POST[ID]' AND OFFER_ID = '$_POST[OFFER_ID]'");
		oci_execute($stid);
		oci_commit($dbconn);
		
 		writelog("$CURUSER[username] added price for offer #$_POST[OFFER_ID]");
		//echo "UPDATE PRICE SET DESCRIPTION = '$_POST[DESCRIPTION]', PRICE = '$_POST[PRICE]', FROM_DATE = to_date('$_POST[FROM_DATE]', 'yyyy-mm-dd'), TO_DATE = to_date('$_POST[TO_DATE]', 'yyyy-mm-dd') WHERE ID = $_POST[ID] AND OFFER_ID = $_POST[OFFER_ID]";
		header("location: ../editoffers.php?editid=$offer_id&add=1&price=1");
		die;
		}
	
	}
?>


<script type="text/javascript">
$(document).ready(function() {

  $('.maskeddate').datePicker({clickInput:true})
  
  $("input.numeric").numeric();
  
  });
 </script>
 
 
<form method="post" action="/info/price.php?edit=<?=$edit?>&offer=<?=$offer_id?>">
<fieldset class='priceform'>
	<input type="hidden" name="ID" value="<?=$edit?>"/>
	<input type="hidden" name="OFFER_ID" value="<?=$offer_id?>"/>

		<legend>Ár szerkesztése</legend>
	<ul>
		<li><label>Szobatípus:</label>
			<select name='ROOM_TYPE'>
				<option value='0'>Általános ár</option>

				<? $origPartnerData = getOracleSQL("SELECT * FROM ROOM_TYPE WHERE PARTNER_ID = '$CURUSER[coredb_id]'");
					foreach($origPartnerData as $data)
					{
				?>
					<option value='<?=$data[ID]?>' <? if($data[ID] == $prices[ROOM_TYPE_ID]) echo "selected"?>><?=$data[NAME]?></option>
				<?	
					}
				?>
			</select>
		</li>
	
	
		<li><label>Ár:</label><input type='text' name='PRICE' id="orig_value" value="<?=$prices[PRICE]?>" class='numeric'/> Ft</li>
		<li><label>Plusz egy éjszaka ára:</label><input type='text' name='PLUS_DAY' id="orig_value" value="<?=$prices[PLUS_DAY]?>" class='numeric'/> Ft</li>
		<li><label>Érvényesség kezdete:</label><input type='text' name='FROM_DATE' class='maskeddate' style='width:70px;' value="<?=format_date($prices[FROM_DATE],"-")?>"/></li>
		<li><label>Érvényesség vége:</label><input type='text'  name='TO_DATE'  class='maskeddate' style='width:70px;'  value="<?=format_date($prices[TO_DATE],"-")?>"/></li>
		<li><label>Éjjelek száma</label><input type='text'  name='DAY_NUM'  style='width:40px;' class='numeric' value="<?=$prices[DAY_NUM]?>" class='numeric'/> éj</li>
		<li><label>Leírás</label><textarea name='DESCRIPTION' class='pricetextarea'><?=$prices[DESCRIPTION]?></textarea></li>
		<li><input type="submit" value="Mentés" /></li>

	</ul>
	</fieldset>
	</form>