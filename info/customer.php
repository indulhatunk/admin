<?
/*
 * customer.php 
 *
 * customer info page
 *
*/

/* bootstrap file */
include("../inc/init.inc.php");

userlogin();

if($CURUSER[userclass] == 5)
	$extraSelect = "AND reseller_id = '$CURUSER[pid]'";
elseif($CURUSER["vatera"] <> 1 && $CURUSER[pid] <> 850) 
	$extraSelect = "AND  (pid = '$CURUSER[pid]' or sub_pid = '$CURUSER[pid]') ";
else
{ }

$cid = (int)$_GET[cid];

if($cid > 0)
{

$query = $mysql->query("SELECT * FROM customers WHERE cid = $cid $extraSelect");
$arr = mysql_fetch_assoc($query);

$offerQr = $mysql->query("SELECT * FROM offers WHERE id = '$arr[offers_id]'");
		$offerArr = mysql_fetch_assoc($offerQr);
		
?>


	<?
			$partnerQuery = $mysql->query("SELECT * FROM partners WHERE pid = '$arr[pid]'  ORDER BY hotel_name ASC");
			$partnerArr = mysql_fetch_assoc($partnerQuery);
			$pname =  $partnerArr[hotel_name];
			
			$rq = $mysql->query("SELECT * FROM partners WHERE pid = '$arr[reseller_id]'  ORDER BY company_name ASC");
			$rarr = mysql_fetch_assoc($rq);
			$rname =  $rarr[company_name];
				
	if($arr[type] == 1) 
		$type = "Vatera";
	elseif($arr[type] == 2) 
		$type = "Teszvesz";
	elseif($arr[type] == 3) 
		$type = "Licittravel";
	elseif($arr[type] == 4) 
		$type = "Outlet";
	elseif($arr[type] == 5) 
		$type = "Pult";
	elseif($arr[type] == 6) 
		$type = "Lealkudtuk";
	elseif($arr[type] == 7) 
		$type = "Grando";
	elseif($arr[type] == 8) 
		$type = "Viszonteladó";
	
	/*if($arr[payment] == 1 || $arr[checkpaper_id] > 0) 
		$payment = "Átutalás";
	elseif($arr[payment] == 2) 
		$payment = "Készpénz";
	elseif($arr[payment] == 3) 
		$payment = "Utánvét";
	elseif($arr[payment] == 4) 
		$payment = "Futár";
	elseif($arr[payment] == 5) 
		$payment = "Üdülési csekk";
	elseif($arr[payment] == 6) 
		$payment = "Helyszinen";
	elseif($arr[payment] == 7) 
		$payment = "Online";
	elseif($arr[facebook] == 8) 
		$payment = "Facebook";
	*/
	
	if($arr[payment] == 1  || $arr[checkpaper_id] > 0) 
		$payment = $lang[transfer];
	elseif($arr[payment] == 2) 
		$payment = $lang[cash];
	elseif($arr[payment] == 3) 
		$payment = $lang[postpaid];
	elseif($arr[payment] == 4) 
		$payment = $lang[delivery];
	elseif($arr[payment] == 5) 
		$payment = $lang[t_check];
	elseif($arr[payment] == 6) 
		$payment = $lang[place];
	elseif($arr[payment] == 7) 
		$payment = $lang[online];
	elseif($arr[payment] == 8) 
		$payment = $lang[facebook];
	elseif($arr[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($arr[payment] == 10) 
		$payment = "OTP SZÉP kártya";
	elseif($arr[payment] == 11) 
		$payment = "MKB SZÉP kártya";
	elseif($arr[payment] == 12) 
		$payment = "K&H SZÉP kártya";
		
	$ppyear = explode("-","$arr[paid_date]");
	$ppyear = $ppyear[0];



	if($arr[postpone] == 1  && $CURUSER[userclass] <> 5 && $ppyear == 2012)
		$payment = $lang[t_check];
	elseif($arr[postpone] == 1  && $CURUSER[userclass] <> 5 && $ppyear == 2013)
		$payment = "OTP SZÉP kártya";

		
	if($CURUSER[language] == 'en')
		$prefix = 'en_';
	else
		$prefix = '';	
	?>
	
	
<style>
	label { font-weight:bold; }
	.lablerow { font-weight:Bold; width:140px; background-color:#e7e7e7}
	

table
{
    border-width: 0 0 1px 1px !important;
    border-spacing: 0 !important;
    border-radius:3px !important;
    border-collapse: collapse;
}
table, td
{
    border-color: #c4c4c4 !important;
    border-width:1px !important;
    border-style: solid !important;
	font-size:12px !important;
	font-family:arial;
}

</style>
<fieldset style='width:450px'>
		<legend><?=$lang[customer_details]?></legend>
		
		<table width='100%'>
			<tr>
				<td class='lablerow'><?=$lang[id]?>:</td>
				<td><?=$arr[offer_id]?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[partner]?>:</td>
				<td><?=$pname?></td>
			</tr>
			<? if($CURUSER[userclass] > 50 && $arr[reseller_id] > 0) { ?>
			<tr>
				<td class='lablerow'>Viszonteladó:</td>
				<td><?=$rname?></td>
			</tr>
			<? if($arr[reseller_invoice] <> '') { ?>
			<tr>
				<td class='lablerow'>Viszonteladó elszámolás:</td>
				<td><? echo date("Y",strtotime($arr[reseller_invoice_date]))."/".$arr[reseller_invoice]?></td>
			</tr>
			<? } ?>
			<? } ?>
			
			<tr>
				<td class='lablerow'><?=$lang[customer_name]?>:</td>
				<td><?=$arr[name]?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[address]?>:</td>
				<td><?=$arr[zip]?> <?=$arr[city]?> <?=$arr[address]?></td>
			</tr>
			<? if($arr[invoice_name] <> '') { ?>
			<tr>
				<td class='lablerow'><?=$lang[bill_name]?>:</td>
				<td><?=$arr[invoice_name]?></td>
			</tr>
			<? } 
				if($arr[invoice_address] <> '') {
			?>
			<tr>
				<td class='lablerow'><?=$lang[bill_address]?>:</td>
				<td><?=$arr[invoice_zip]?> <?=$arr[invoice_city]?> <?=$arr[invoice_address]?></td>
			</tr>
			<? } ?>
			<? if($arr[postpone_date] <> '0000-00-00 00:00:00' && $CURUSER[userclass] == 255) { ?>
			<tr>
				<td class='lablerow'>Utalás info:</td>
				<td><?=$arr[postpone_date]?></td>
			</tr>
			<? } ?>
			<? if($arr[transfer_sent] <> '0000-00-00 00:00:00' && $CURUSER[userclass] > 50) { ?>
			<tr>
				<td class='lablerow'>Utalás dátuma:</td>
				<td class='lablerow'><?=$arr[transfer_sent]?></td>
			</tr>
			<? } ?>
			<tr>
				<td class='lablerow'><?=$lang[print_date]?>:</td>
				<td><?=$arr[added]?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[paid_date]?>:</td>
				<td><? if($arr[paid_date] == '0000-00-00 00:00:00') echo "-"; else echo $arr[paid_date];?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[deadline]?>:</td>
				<td><?=$arr[due_date]?></td>
			</tr>
			<? if($arr[customer_left] <> '0000-00-00 00:00:00')
			{
			?>
			<tr class='orange'>
				<td class='lablerow'><?=$lang[customer_left]?>:</td>
				<td><?=$arr[customer_left]?></td>
			</tr>
			<?
			}
			?>
			
			<? if($CURUSER[userclass] > 50) { 
					$stats = array(
								0 => '-',
								1 => "van hely", 
								2 => "lekérve",
								3 => "visszaigazolva",
								4 => "értesítve");
								
								
			?>
			<!-- -->
			<tr>
				<td class='lablerow'>Külföldi státusz:</td>
				<td><?=$stats[$arr[foreign_status]]?></td>
			</tr>
			<tr>
				<td class='lablerow'>Külföldi érkezés:</td>
				<td><?=$arr[customer_booked]?></td>
			</tr>
			<? } ?>
			<!-- -->
			<tr>
				<td class='lablerow'><?=$lang[phone]?>:</td>
				<td><?=$arr[phone]?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[email]?>:</td>
				<td><a href="mailto:<?=$arr[email]?>"><?=$arr[email]?></a></td>
			</tr>
			
			<tr>
				<td class='lablerow'>PROMO:</td>
				<td><?=$arr[discount_code]?></td>
			</tr>
			<tr>
				<td class='lablerow'>Kedvezmény:</td>
				<td><?=formatPrice($arr[discount_value])?> (<?=$arr[discount_percent]?>%)</td>
			</tr>
			<? if($arr[birthday] <> '0000-00-00') { ?>
			<tr>
				<td class='lablerow'><?=$lang[birthday]?>:</td>
				<td><?=$arr[birthday]?></td>
			</tr>
			<? } ?>
			<tr>
				<td class='lablerow'><?=$lang[offer]?>:</td>
				<td><div style="float:left;width:300px;"><?=$offerArr[$prefix."name"]?></div></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[offer_type]?>:</td>
				<td> <?=$type?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[offer_price]?>:</td>
				<td><?=formatPrice($arr[actual_price])?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[price]?>:</td>
				<td><?=formatPrice($arr[orig_price]-$arr[discount_value])?></td>
			</tr>
			<!--<tr>
				<td class='lablerow'><?=$lang[payment]?>:</td>
				<td><?=$payment?></td>
			</tr>-->
			<?if($CURUSER[vatera]==1){?>
			<tr>
				<td class='lablerow'><?=$lang[post]?>:</td>
				<td><?=formatPrice($arr[post_fee])?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[comment]?>:</td>
				<td><?=$arr[comment]?></td>
			</tr>
			<?}?>
	
			<tr>
				<td class='lablerow'><?=$lang[validity]?>:</td>
				<td><div style="float:left;width:300px;"><?=str_replace("|","<br/>",$offerArr[$prefix."validity"])?></div><div class='cleaner'></div></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[not_include]?>:</td>
				<td><div style="float:left"><?=str_replace("|","<br/>",$offerArr[$prefix."not_include"])?></div><div class="cleaner"></div></td>
			</tr>
			<?if($CURUSER[userclass] > 50){?>
			
			<? if($arr[szep_successful] == 1) { ?>
			<tr>	
				<td class='lablerow'>SZÉP fizetve</td>
				<td><?=formatPrice($arr[szep_paid])?></td>
			</tr>
			<tr>	
				<td class='lablerow'>SZÉP fizetés dátum</td>
				<td><?=$arr[szep_paid_date]?></td>
			</tr>
			<tr>	
				<td class='lablerow'>SZÉP TXID</td>
				<td><?=$arr[szep_txid]?></td>
			</tr>
				<tr>	
				<td class='lablerow'>SZÉP auth</td>
				<td><?=$arr[szep_auth]?></td>
			</tr>
			<? } ?>
			<? if($arr[$arr[check_count]] > 0) { ?>
			<tr>	
				<td class='lablerow'>ÜCS DB</td>
				<td><?=$arr[check_count]?></td>
			</tr>
			<tr>
				<td class='lablerow'>ÜCS érték</td>
				<td><?=formatPrice($arr[check_value])?></td>
			</tr>
			<tr>
				<td class='lablerow'>ÜCS Dátum</td>
				<td><?=$arr[check_arrival]?></td>
			</tr>
			<tr>
				<td class='lablerow'>ÜCS ID</td>
				<td><?=$arr[checkpaper_id]?></td>
			</tr>
			<? } ?>
			<tr>
				<td class='lablerow'>Log</td>
				<td><a href='log.php?search=<?=$arr[offer_id]?>,<?=$arr[cid]?>' rel="facebox">A tétel eseményei</a></td>
			</tr>
			<tr>
				<td class='lablerow'>Levél</td>
				<td><a href='info/letter.php?hash=<?=$arr[email_id]?>' rel="facebox">Levél megtekintése</a></td>
			</tr>
		<?}?>
			</table>
			
	<!--<ul>
	
		<li><label><?=$lang[id]?>:</label><?=$arr[offer_id]?></li>
		<li><label><?=$lang[partner]?>:</label><?=$pname?></li>
		<li><label><?=$lang[customer_name]?>:</label><?=$arr[name]?></li>
		<li><label><?=$lang[address]?>:</label><?=$arr[zip]?> <?=$arr[city]?> <?=$arr[address]?></li>
		
		<? if($arr[invoice_name] <> '') { ?>
			<li><label><?=$lang[bill_name]?>:</label><?=$arr[invoice_name]?></li>
		<? } 
		if($arr[invoice_address] <> '') {
		?>
		<li><label><?=$lang[bill_address]?>:</label><?=$arr[invoice_zip]?> <?=$arr[invoice_city]?> <?=$arr[invoice_address]?></li>
		<? } ?>
		<? if($arr[postpone_date] <> '0000-00-00 00:00:00' && $CURUSER[userclass] == 255) { ?>
		
			<li><label>Utalás késleltetve:</label><?=$arr[postpone_date]?></li>
		
		<? } ?>
		<li><label><?=$lang[print_date]?>:</label><?=$arr[added]?></li>
		<li><label><?=$lang[paid_date]?>:</label><?=$arr[paid_date]?></li>
		<li><label><?=$lang[deadline]?>:</label><?=$arr[due_date]?></li>
		
		<li><label><?=$lang[phone]?>:</label><?=$arr[phone]?></li>
		<li><label><?=$lang[email]?>:</label><a href="mailto:<?=$arr[email]?>"><?=$arr[email]?></a></li>
		<? if($arr[birthday] <> '0000-00-00') { ?>
		
			<li><label><?=$lang[birthday]?>:</label><?=$arr[birthday]?></li>
		
		<? } ?>
		<li><label><?=$lang[offer]?>:</label><div style="float:left;width:300px;"><?=$offerArr[$prefix."name"]?></div></li>
		<li><label><?=$lang[offer_type]?>:</label> <?=$type?></li>
		<li><label><?=$lang[offer_price]?>:</label><?=formatPrice($arr[actual_price])?></li>

		<li><label><?=$lang[price]?>:</label><?=formatPrice($arr[orig_price])?></li>
		<li><label><?=$lang[payment]?>:</label><?=$payment?></li>
		

		<?if($CURUSER[vatera]==1){?>
		<li><label><?=$lang[post]?>:</label><?=formatPrice($arr[post_fee])?></li>
		<li><label><?=$lang[comment]?>:</label><?=$arr[comment]?></li>
		<?}?>
	
		<li><label><?=$lang[validity]?>:</label><div style="float:left;width:300px;"><?=str_replace("|","<br/>",$offerArr[$prefix."validity"])?></div></td></tr>
		<li><label><?=$lang[not_include]?>:</label><div style="float:left"><?=str_replace("|","<br/>",$offerArr[$prefix."not_include"])?></div><div class="cleaner"></div></li>
	
		<?if($CURUSER[userclass] > 50){?>
	
		<li><label>ÜCS DB</label><?=$arr[check_count]?></li>
		<li><label>ÜCS érték</label><?=formatPrice($arr[check_value])?></li>
		
		<li><label>ÜCS Dátum</label><?=$arr[check_arrival]?></li>
		<li><label>ÜCS ID</label><?=$arr[checkpaper_id]?></li>

		<li><label>Log</label><a href='log.php?search=<?=$arr[offer_id]?>,<?=$arr[cid]?>' rel="facebox">A tétel eseményei</a></li>
		<li><label>Levél</label><a href='info/letter.php?hash=<?=$arr[email_id]?>' rel="facebox">Levél megtekintése</a></li>
		<?}?>
		</ul>
	-->
</fieldset> 
<fieldset  style='width:450px'>
	<legend><?=$lang[plus_services]?>:</legend>
	

	<?
	$editarr = $arr;
	
	
	if($editarr[offer_id] <>"") {
		$offerQr = $mysql->query("SELECT * FROM offers WHERE id = $editarr[offers_id]");
		$offerArr = mysql_fetch_assoc($offerQr);
	?>
	<input type="hidden" name="days" value="<?=$offerArr[days]?>"/>
	<input type="hidden" name="actual_price" value="<?=$offerArr[actual_price]?>"/>

	<table width='100%'>
	<?
		if($editarr[plus_half_board]<>0) {?>
	<tr>
				<td class='lablerow'>Félpanzió / Half board </td>
				<td><?=formatPrice($offerArr[plus_half_board])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_weekend_plus]<>0) {?>
	<tr>
				<td class='lablerow'>Hétvégi felár / Weekend extra price</td>
				<td><?=formatPrice($offerArr[plus_weekend_plus]*2)?> (egyszeri díj)</td></tr>
<?}?>

<?if($editarr[plus_bed]<>0) {?>
<tr>
				<td class='lablerow'>Pótágy felár / Extra bed additional price</td>
				<td><?=formatPrice($offerArr[plus_bed])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_bed_plus_food]<>0) {?>
<tr>
				<td class='lablerow'>Félpanzió + pótágy felár</td>
				<td><?=formatPrice($offerArr[plus_bed_plus_food])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_child1_value]<>0) {?>
<tr>
				<td class='lablerow'>1. gyermek kora / 1st Child:</td>
				<td>
	<?
	if($editarr[plus_child1_value]==1  || ($editarr[plus_child1_value] >1 && $editarr[plus_child1_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig + ".formatPrice($offerArr[plus_child1_value])."/éj";
	elseif($editarr[plus_child1_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között + ".formatPrice($offerArr[plus_child2_value])."/éj";
	elseif($editarr[plus_child1_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett + ".formatPrice($offerArr[plus_child3_value])."/éj";
	?>
	</td></tr>
<?}?>

<?if($editarr[plus_child2_value]<>0) {?>
<tr>
				<td class='lablerow'>2. gyermek kora: / 2nd Child:</td>
				<td><input type="hidden" name="prev_child2" value="0" class="prev_child2"/>
		<?
	if($editarr[plus_child2_value]==1  || ($editarr[plus_child2_value] >1 && $editarr[plus_child2_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig + ".formatPrice($offerArr[plus_child1_value])."/éj";
	elseif($editarr[plus_child2_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között + ".formatPrice($offerArr[plus_child2_value])."/éj";
	elseif($editarr[plus_child2_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett + ".formatPrice($offerArr[plus_child3_value])."/éj";
	?>
</td></tr>
<?}?>

<?if($editarr[plus_child3_value]<>0) {?>
<tr>
				<td class='lablerow'>3. gyermek / 3rd Child:</td><td>	
	<?
	if($editarr[plus_child3_value]==1  || ($editarr[plus_child3_value] >1 && $editarr[plus_child3_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig + ".formatPrice($offerArr[plus_child1_value])."/éj";
	elseif($editarr[plus_child3_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között + ".formatPrice($offerArr[plus_child2_value])."/éj";
	elseif($editarr[plus_child3_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett + ".formatPrice($offerArr[plus_child3_value])."/éj";
	?>
</td></tr>
<?}?>

<?if($editarr[plus_room1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room1_name]?> szoba felár</td>
				<td><?=formatPrice($offerArr[plus_room1_value])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_room2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room2_name]?> szoba felár</td>
				<td><?=formatPrice($offerArr[plus_room2_value])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_room3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_room3_name]?> szoba felár</td>
				<td><?=formatPrice($offerArr[plus_room3_value])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_other1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other1_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_other1_value])?>/éj</td>
			</tr>
<?}?>

<?if($editarr[plus_other2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other2_name]?> felár</td>
				<td> <?=formatPrice($offerArr[plus_other2_value])?>/éj</td>
			</tr>
<?}?>

<?if($editarr[plus_other3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other3_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_other3_value])?>/éj</td>
			</tr>
<?}?>

<?if($editarr[plus_other4_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other4_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_other4_value])?>/éj</td>
			</tr>
<?}?>

<?if($editarr[plus_other5_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other5_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_other5_value])?>/éj</td>
			</tr>
<?}?>

<?if($editarr[plus_other6_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_other6_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_other6_value])?>/éj</td>
			</tr>
<?}?>

<?if($editarr[plus_single1_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single1_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_single1_value])?>/csomag</td>
			</tr>
<?}?>
<?if($editarr[plus_single2_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single2_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_single2_value])?>/csomag</td>
			</tr>
<?}?>
<?if($editarr[plus_single3_value]<>0) {?>
<tr>
				<td class='lablerow'><?=$offerArr[plus_single3_name]?> felár</td>
				<td><?=formatPrice($offerArr[plus_single3_value])?>/csomag</td>
			</tr>
<?}?>

<?if( $editarr[plus_days]>0) {?>

<tr>
				<td class='lablerow'>Maradjon még egy éjszakát / One more night stay additional price </td>
				<td><?=formatPrice($offerArr[actual_price]/$offerArr[days])?></td></tr>
<?}?>
<?if( $editarr[checks]<>'' && $CURUSER[language] <> 'en') {?>

<tr>
				<td class='lablerow'>Üdülési csekkek</td>
				<td><?=$editarr[checks]?></td></tr>
<?}
?>


<? if( $offerArr[tos]<>'') {?>
<hr/>
<tr>
				<td class='lablerow'>ÁSZF</td>
				<td><a href='<?=$offerArr[tos]?>' target='_blank'>letöltés</a></td></tr>
<? 
} 
if($editarr[disclaimer] <> '')
{
	if($editarr[disclaimer] == 1)
		$disclaimer = '"nem adóalanyként, vagy nem adóalanyi minőségében (azaz utasként) veszem igénybe,"';
	elseif($editarr[disclaimer] == 2)
		$disclaimer = '"adóalanyként saját nevében és javára, tehát végső felhasználóként (azaz utasként) veszem"';
	elseif($editarr[disclaimer] == 3)
		$disclaimer = '"<b><font color="red">adóalanyként saját nevében, de más javára (azaz nem utasként) veszem igénybe</font></b>"';
	echo "<tr>
				<td class='lablerow'>Nyilatkozat</td>
				<td>$disclaimer</td></tr>";

}



?>
	</table>
	<? /* ?>	
	<?
		if($editarr[plus_half_board]<>0) {?>
	<li><label>Félpanzió / Half board </label> <?=formatPrice($offerArr[plus_half_board])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_weekend_plus]<>0) {?>
	<li><label>Hétvégi felár / Weekend extra price</label> <?=formatPrice($offerArr[plus_weekend_plus]*2)?> (egyszeri díj)</td></tr>
<?}?>

<?if($editarr[plus_bed]<>0) {?>
<li><label>Pótágy felár / Extra bed additional price</label> <?=formatPrice($offerArr[plus_bed])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_bed_plus_food]<>0) {?>
<li><label>Félpanzió + pótágy felár</label> <?=formatPrice($offerArr[plus_bed_plus_food])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_child1_value]<>0) {?>
<li><label>1. gyermek kora / 1st Child:</label> 
	<?
	if($editarr[plus_child1_value]==1  || ($editarr[plus_child1_value] >1 && $editarr[plus_child1_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig + ".formatPrice($offerArr[plus_child1_value])."/éj";
	elseif($editarr[plus_child1_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között + ".formatPrice($offerArr[plus_child2_value])."/éj";
	elseif($editarr[plus_child1_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett + ".formatPrice($offerArr[plus_child3_value])."/éj";
	?>
	<div class='cleaner'></div>
</li>
<?}?>

<?if($editarr[plus_child2_value]<>0) {?>
<li><label>2. gyermek kora: / 2nd Child:</label> <input type="hidden" name="prev_child2" value="0" class="prev_child2"/>
		<?
	if($editarr[plus_child2_value]==1  || ($editarr[plus_child2_value] >1 && $editarr[plus_child2_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig + ".formatPrice($offerArr[plus_child1_value])."/éj";
	elseif($editarr[plus_child2_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között + ".formatPrice($offerArr[plus_child2_value])."/éj";
	elseif($editarr[plus_child2_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett + ".formatPrice($offerArr[plus_child3_value])."/éj";
	?>
	<div class='cleaner'></div>
</li>
<?}?>

<?if($editarr[plus_child3_value]<>0) {?>
<li><label>3. gyermek / 3rd Child:<: </label>	
	<?
	if($editarr[plus_child3_value]==1  || ($editarr[plus_child3_value] >1 && $editarr[plus_child3_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig + ".formatPrice($offerArr[plus_child1_value])."/éj";
	elseif($editarr[plus_child3_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között + ".formatPrice($offerArr[plus_child2_value])."/éj";
	elseif($editarr[plus_child3_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett + ".formatPrice($offerArr[plus_child3_value])."/éj";
	?>
</td></tr>
<?}?>

<?if($editarr[plus_room1_value]<>0) {?>
<li><label><?=$offerArr[plus_room1_name]?> szoba felár</label> <?=formatPrice($offerArr[plus_room1_value])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_room2_value]<>0) {?>
<li><label><?=$offerArr[plus_room2_name]?> szoba felár</label> <?=formatPrice($offerArr[plus_room2_value])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_room3_value]<>0) {?>
<li><label><?=$offerArr[plus_room3_name]?> szoba felár</label> <?=formatPrice($offerArr[plus_room3_value])?>/éj</td></tr>
<?}?>

<?if($editarr[plus_other1_value]<>0) {?>
<li><label><?=$offerArr[plus_other1_name]?> felár</label> <?=formatPrice($offerArr[plus_other1_value])?>/éj</label></td></tr>
<?}?>

<?if($editarr[plus_other2_value]<>0) {?>
<li><label><?=$offerArr[plus_other2_name]?> felár</label>  <?=formatPrice($offerArr[plus_other2_value])?>/éj</label></td></tr>
<?}?>

<?if($editarr[plus_other3_value]<>0) {?>
<li><label><?=$offerArr[plus_other3_name]?> felár</label> <?=formatPrice($offerArr[plus_other3_value])?>/éj</label></td></tr>
<?}?>

<?if($editarr[plus_other4_value]<>0) {?>
<li><label><?=$offerArr[plus_other4_name]?> felár</label> <?=formatPrice($offerArr[plus_other4_value])?>/éj</label></td></tr>
<?}?>

<?if($editarr[plus_other5_value]<>0) {?>
<li><label><?=$offerArr[plus_other5_name]?> felár</label> <?=formatPrice($offerArr[plus_other5_value])?>/éj</label></td></tr>
<?}?>

<?if($editarr[plus_other6_value]<>0) {?>
<li><label><?=$offerArr[plus_other6_name]?> felár</label> <?=formatPrice($offerArr[plus_other6_value])?>/éj</label></td></tr>
<?}?>

<?if($editarr[plus_single1_value]<>0) {?>
<li><label><?=$offerArr[plus_single1_name]?> felár</label> <?=formatPrice($offerArr[plus_single1_value])?>/csomag</label></td></tr>
<?}?>
<?if($editarr[plus_single2_value]<>0) {?>
<li><label><?=$offerArr[plus_single2_name]?> felár</label> <?=formatPrice($offerArr[plus_single2_value])?>/csomag</label></td></tr>
<?}?>
<?if($editarr[plus_single3_value]<>0) {?>
<li><label><?=$offerArr[plus_single3_name]?> felár</label> <?=formatPrice($offerArr[plus_single3_value])?>/csomag</label></td></tr>
<?}?>

<?if( $editarr[plus_days]>0) {?>

<li><label>Maradjon még egy éjszakát / One more night stay additional price </label> <?=formatPrice($offerArr[actual_price]/$offerArr[days])?></td></tr>
<?}?>
<?if( $editarr[checks]<>'' && $CURUSER[language] <> 'en') {?>

<li><label>Üdülési csekkek</label> <?=$editarr[checks]?></td></tr>
<?}
?>


<? if( $offerArr[tos]<>'') {?>
<hr/>
<li><label>ÁSZF</label> <a href='<?=$offerArr[tos]?>' target='_blank'>letöltés</a></td></tr>
<? 
} 
if($editarr[disclaimer] <> '')
{
	if($editarr[disclaimer] == 1)
		$disclaimer = '"nem adóalanyként, vagy nem adóalanyi minőségében (azaz utasként) veszem igénybe,"';
	elseif($editarr[disclaimer] == 2)
		$disclaimer = '"adóalanyként saját nevében és javára, tehát végső felhasználóként (azaz utasként) veszem"';
	elseif($editarr[disclaimer] == 3)
		$disclaimer = '"<b><font color="red">adóalanyként saját nevében, de más javára (azaz nem utasként) veszem igénybe</font></b>"';
	echo "<li><label>Nyilatkozat</label> $disclaimer</td></tr>";

}

*/

}
?>
</ul>
</fieldset>
<? if($CURUSER[userclass] < 5) { ?>
<fieldset  style='width:450px'>
	<legend><?=$lang[comment]?>:</legend>
	<form method='post' action='/customers.php'>
	<input type='hidden' name='cid' value='<?=$editarr[cid]?>'/>
	<textarea name='hotel_comment' style='width:430px; height:50px;'><?=$editarr[hotel_comment]?></textarea>
	<input type='submit' value='Mentés'/>
	</form>
</fieldset>
<? } ?>
<?
}
?>