<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("../inc/init.inc.php");

$pid = (int)$_GET[pid];
$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $pid"));

if($_GET[language] == 'ro')
{
	include("../inc/languages/ro.lang.php");
	head('',1,1,'.ro');
}
elseif($_GET[language] == 'sk')
{
	include("../inc/languages/sk.lang.php");
	head('',1,1,'.sk');
}
else
	head('',1,1);
?>
<style>
	.dp-applied { float:left; }
	.non-print,.roomtitle,.dp-nav-prev,.dp-nav-next  { display:none !important; }
	
</style>
<script type="text/javascript">

$(function()
{

	$('#multimonth').datePickerMultiMonth(
					{
						numMonths: 4,
						inline: true,
						selectMultiple:true
						<? if($CURUSER[userclass] == 5) 
							echo ", disableclick:1";
						?>
					}
				).bind(
					'dateSelected',
					function(event, date, $td, status)
					{
						Date.format = 'yyyy-mm-dd';
						var date = date.asString();
						var pid = $("#pid").val();
						
						
						$.get('info/fullhouse.php?pid='+pid+'&date='+date+'&roomtype=0', function(data) {
 							//alert('done');
						});

						
						
					}

				);
		
		
					
<?

//roomtype 1	
$query = $mysql->query("SELECT * FROM fullhouse WHERE pid = '$pid' AND date >= NOW() AND roomtype = 0 ORDER BY date ASC");
while($arr = mysql_fetch_assoc($query))
{
	echo "$('#multimonth .selecteddate-$arr[date]').addClass('selected'); \n";
}
?>	
			
});

</script>


<div style='width:695px;padding:10px 0 10px 0;border-bottom:2px solid #7b7b7b;'>
<div style='font-weight:bold;font-size:16px;text-align:center;'><?=$lang[booking_details]?></div>

<div style='font-size:11px;text-align:center;'>(<?=date("Y-m-d H:i:s")?>)</div>

<div id="multimonth" style='padding:0;margin:-20px 0 0 0;'></div>

<div class='cleaner'></div>

<table border='0' cellspacing='0' cellpadding='0' style='width:140px;margin:0 auto;'>
	<tr>
		<td style='background-color:#d90000;color:white;width:70px;height:10px;text-align:center;'><?=$lang['fullrooms']?></td>
		<td style='background-color:#70b000;color:white;width:70px;height:10px;text-align:center;'><?=$lang['freerooms']?></td>
	</tr>

</table>
<div style='font-size:11px;padding:10px 0 0 20px'>
	<?=$lang[roomdetails]?>
</div>
</div>