<?
header('Content-type: text/html; charset=utf-8');

include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");


$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

if($CURUSER["vatera"] <> 1 && $CURUSER["username"] <> 'vatera') {
	$pid = $CURUSER[pid];
	$partner = mysql_fetch_assoc($mysql->query("select * from partners where pid = $pid LIMIT 1"));
	$pid = $partner[coredb_id];
	$extraSelect = "AND partner_id = $pid";
} 

if($CURUSER["username"] == 'vatera') {
	$pid = $CURUSER[pid];
	$partner = mysql_fetch_assoc($mysql->query("select * from partners where pid = $pid LIMIT 1"));
	$pid = $partner[coredb_id];
	$extraSelect = "AND date > '2011-07-01 00:00:00'";
} 

$cid = (int)$_GET[cid];

if($cid > 0)
{
$query = $mysql->query("SELECT * FROM lmb WHERE id = $cid $extraSelect");
$editarr = mysql_fetch_assoc($query);
?>
<style>
	label { font-weight:bold; }
</style>
<fieldset>
	<legend>Vásárló adatai</legend>
		<ul>
			<li><label>Foglalás száma</label>LMB-<?=$editarr[realID]?></li>
			<li><label>Beérkezett</label><?=$editarr[date]?></li>
			<li><label>Vásárló neve</label> <?=$editarr[name]?></li>
			<li><label>Vásárló telefonszáma</label><?=$editarr[phone]?></li>
			<li><label>Vásárló e-mail címe</label><a href="mailto:<?=$editarr[email]?>"><?=$editarr[email]?></a></li>
			<li><label>Hotel neve</label><?=$editarr[partner_name]?> </li>
			<li><label>Hotel e-mail címe</label><a href='mailto:<?=$editarr[partner_email]?>'><?=$editarr[partner_email]?></a> </li>
			<li><label>Ajánlat neve</label><?=$editarr[accomodation_name]?> </li>
			<li><label>Napok száma</label><?=$editarr[days]?> nap</li>
			
			<li><label>Indulás</label> <?=$editarr[from_date]?></li>
			<li><label>Távozás</label> <?=$editarr[to_date]?></li>
		
			<li><label>Ár</label><?=formatPrice($editarr[total_price])?></li>
			<li><label>Felnőttek száma</label> <?=$editarr[adults]?></li>
			<li><label>1. gyermek kora</label>
			<select name="child1">
				<option value="101">Válasszon</option>
				<option value="<?=$editarr[child1Discount]?>" <?if($editarr[child1] == $editarr[child1Discount]) echo "selected"?>><?=$editarr[child1Name]?></option>
				<option value="<?=$editarr[child2Discount]?>" <?if($editarr[child1] == $editarr[child2Discount]) echo "selected"?>><?=$editarr[child2Name]?></option>
				<option value="<?=$editarr[child3Discount]?>" <?if($editarr[child1] == $editarr[child3Discount]) echo "selected"?>><?=$editarr[child3Name]?></option>
			</select>
			</li>
			<li><label>2. gyermek kora</label>
			<select name="child2">
				<option value="101">Válasszon</option>
				<option value="<?=$editarr[child1Discount]?>" <?if($editarr[child2] == $editarr[child1Discount]) echo "selected"?>><?=$editarr[child1Name]?></option>
				<option value="<?=$editarr[child2Discount]?>" <?if($editarr[child2] == $editarr[child2Discount]) echo "selected"?>><?=$editarr[child2Name]?></option>
				<option value="<?=$editarr[child3Discount]?>" <?if($editarr[child2] == $editarr[child3Discount]) echo "selected"?>><?=$editarr[child3Name]?></option>
			</select>
			</li>
			<li><label>3. gyermek kora</label>
			<select name="child3">
				<option value="101">Válasszon</option>
				<option value="<?=$editarr[child1Discount]?>" <?if($editarr[child3] == $editarr[child1Discount]) echo "selected"?>><?=$editarr[child1Name]?></option>
				<option value="<?=$editarr[child2Discount]?>" <?if($editarr[child3] == $editarr[child2Discount]) echo "selected"?>><?=$editarr[child2Name]?></option>
				<option value="<?=$editarr[child3Discount]?>" <?if($editarr[child3] == $editarr[child3Discount]) echo "selected"?>><?=$editarr[child3Name]?></option>
			</select>
			</li>
			
			<li><label>Megjegyzés</label> <?=$editarr[comment]?></li>	
			<? if($CURUSER[userclass] > 50) {?> 
				<li><label>Privát Megjegyzés</label> <?=$editarr[comment_private]?></li>	
			<? } ?>
			</ul>
	</fieldset>


<?
}
?>