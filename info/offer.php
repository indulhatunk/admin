<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("../inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER[userclass] < 50)
	$extrareseller = "AND reseller_enabled = 1";




$add = $_GET[add];
$translate = $_GET[translate];


$editid = (int)$_REQUEST[id];
if($editid > 0 )
{
	$editQr = mysql_query("SELECT * FROM offers WHERE id = $editid");
	$editarr = mysql_fetch_assoc($editQr);
	
	if($_GET[cop] <> 1)
		echo "<input type=\"hidden\" name=\"id\" value=\"$editarr[id]\"/>";
}


?>

<style>
	label { font-weight:bold; }
	.lablerow { font-weight:Bold; width:140px; background-color:#e7e7e7}
	

table
{
    border-width: 0 0 1px 1px !important;
    border-spacing: 0 !important;
    border-radius:3px !important;
    border-collapse: collapse;
}
table, td
{
    border-color: #c4c4c4 !important;
    border-width:1px !important;
    border-style: solid !important;
	font-size:12px !important;
	font-family:arial;
}

</style>
<fieldset style='width:450px'>
		<legend>Ajánlat adatai</legend>
<table>

<?
	if($editarr[sub_partner_id] > 0)
		$pid = $editarr[sub_partner_id];
	else
		$pid = $editarr[partner_id];
		
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $pid"));
?>
<tr><td class='lablerow'>Ajánlat VTL neve</td><td><?=$editarr[outer_name];?></td></tr>
<tr><td class='lablerow'>Ajánlat neve</td><td><?=$partner[hotel_name]?></td></tr>
<tr><td class='lablerow'>Ajánlat neve</td><td><?=$editarr[name]?></td></tr>
<tr><td class='lablerow'>Ajánlat rövid neve</td><td><?=$editarr[shortname]?></td></tr>

<tr><td class='lablerow'>Partner telefonszám</td><td><?=$partner[reception_phone]?></td></tr>
<tr><td class='lablerow'>Partner e-mail</td><td><?=$partner[reception_email]?></td></tr>

<tr><td class='lablerow'>Ajánlat linkje</td><td><a href='/preview.php?tid=<?=$editarr[id]?>&type=offer' target='_blank'>link</a></td></tr>
<? if ($CURUSER[userclass] > 50) { ?>

<? if($editarr[offer_comment] <> '') { ?>
	<tr class='red'><td class='lablerow'>Ajánlat info</td><td><?=$editarr[offer_comment]?></td></tr>
<? } ?>

<? } ?>
<tr><td class='lablerow'>Éjjelek száma</td><td><?=$editarr[days]?> éj</td></tr>
<tr><td class='lablerow'>Plusz 1 nap</td><td><? if($editarr[no_plus_one_day]==1) echo "<b>NEM FOGLALHATÓ</b>"; else echo "Foglalható";?></td></tr>
<? if ($CURUSER[userclass] > 50) { ?>

<tr><td class='lablerow'>Normál ár</td><td align='right'><?=$editarr[normal_price]?> Ft</td></tr>

<tr><td class='lablerow'>Kedvezményes ár</td><td align='right'><?=$editarr[actual_price]?> Ft</td></tr>
<? } ?>

<tr><td class='lablerow'>Viszonteladói ár</td><td align='right'><?=$editarr[actual_price]?> Ft</td></tr>

<? if($editarr[plus_half_board] > 0) { ?>
	<tr><td class='lablerow'>Félpanziós felár</td><td align='right'><?=$editarr[plus_half_board]?> Ft/csom </td></tr>
<? } ?> 

<? if($editarr[plus_weekend_plus] > 0) { ?>
	<tr><td class='lablerow'>Hétvégi felár</td><td align='right'><?=$editarr[plus_weekend_plus]*2?> Ft</td></tr>
<? } ?> 

<? if($editarr[plus_bed] > 0) { ?>
	<tr><td class='lablerow'>Pótágy felár</td><td align='right'><?=$editarr[plus_bed]*$editarr[days]?>Ft/csom.  </td></tr>
<? } ?> 

<? if($editarr[plus_bed_plus_food] > 0) { ?>
<tr><td class='lablerow'>Félpanzió + pótágy felár</td><td align='right'><?=$editarr[plus_bed_plus_food]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_child1_name] <> '') { ?>
<tr><td class='lablerow'><font color='red'><b>Gyermek 1 felár*</b></font></td><td align='right'> <?=$editarr[plus_child1_name]?> éves korig - <?=$editarr[plus_child1_value]*$editarr[days]?> Ft/csom.</td></tr>
<tr><td class='lablerow'><font color='red'><b>Gyermek 2 felár*</b></font></td><td align='right'> <?=$editarr[plus_child2_name]?> éves korig - <?=$editarr[plus_child2_value]*$editarr[days]?> Ft/csom.</td></tr>
<tr><td class='lablerow'><font color='red'><b>Gyermek 3 felár*</b></font></td><td align='right'> <?=$editarr[plus_child3_name]?> éves korig - <?=$editarr[plus_child3_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_room1_name]  <> '') { ?>
<tr><td class='lablerow'>Szoba 1 felár</td><td align='right'> <?=$editarr[plus_room1_name]?> - <?=$editarr[plus_room1_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_room2_name] <> '') { ?>
<tr><td class='lablerow'>Szoba 2 felár</td><td align='right'> <?=$editarr[plus_room2_name]?> - <?=$editarr[plus_room2_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_room3_name] <> '') { ?>
<tr><td class='lablerow'>Szoba 3 felár</td><td align='right'> <?=$editarr[plus_room3_name]?> - <?=$editarr[plus_room3_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_other1_name] <> '') { ?>
<tr><td class='lablerow'>Egyéb 1 felár</td><td align='right'> <?=$editarr[plus_other1_name]?> - <?=$editarr[plus_other1_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_other2_name] <> '') { ?>
<tr><td class='lablerow'>Egyéb 2 felár</td><td align='right'> <?=$editarr[plus_other2_name]?> - <?=$editarr[plus_other2_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_other3_name] <> '') { ?>
<tr><td class='lablerow'>Egyéb 3 felár</td><td align='right'> <?=$editarr[plus_other3_name]?> - <?=$editarr[plus_other3_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_other4_name] <> '') { ?>
<tr><td class='lablerow'>Egyéb 4 felár</td><td align='right'> <?=$editarr[plus_other4_name]?> - <?=$editarr[plus_other4_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_other5_name] <> '') { ?>
<tr><td class='lablerow'>Egyéb 5 felár</td><td align='right'> <?=$editarr[plus_other5_name]?> - <?=$editarr[plus_other5_value]*$editarr[days]?> Ft/csom.</td></tr>
<? } ?> 

<? if($editarr[plus_other6_name] <> '') { ?>
<tr><td class='lablerow'>Egyéb 6 felár</td><td align='right'> <?=$editarr[plus_other6_name]?> - <?=$editarr[plus_other6_value]*$editarr[days]?> Ft/csom.</td></tr>

<? } ?> 

<? if($editarr[plus_single1_name] <> '') { ?>
<tr><td class='lablerow'>Egyszeri 1 felár</td><td align='right'><?=$editarr[plus_single1_name]?> - <?=$editarr[plus_single1_value]?> Ft</td></tr>

<? } ?> 

<? if($editarr[plus_single2_name] <> '') { ?>
<tr><td class='lablerow'>Egyszeri 2 felár</td><td align='right'><?=$editarr[plus_single2_name]?> - <?=$editarr[plus_single2_value]?> Ft</td></tr>
<? } ?> 

<? if($editarr[plus_single3_name] <> '') { ?>
<tr><td class='lablerow'>Egyszeri 3 felár</td><td align='right'><?=$editarr[plus_single3_name]?> - <?=$editarr[plus_single3_value]?> Ft</td></tr>
<? } ?> 

<?
	$today = date("Y-m-d H:i:s");
	//$today = '2009-11-01';
	$finish = strtotime('+21 day', strtotime($today));
	$finish = date("Y-m-d H:i:s", $finish);
?>
<tr><td class='lablerow'>Kezdő dátum</td><td align='right'><? if($editarr[start_date] == '') echo $today; else echo $editarr[start_date]; ?></td></tr>
<tr><td class='lablerow'>Befejező dátum</td><td align='right'><? if($editarr[end_date] == '') echo $finish; else echo $editarr[end_date]; ?></td></tr>
<tr><td class='lablerow'><font color='red'  align='right'>Lejárat?</font></td><td align='right'><?=$editarr[expiration_date]?></td></tr>
<tr><td class='lablerow'>Érvényesség</td><td><?=$editarr[validity]?></td></tr>
<tr><td class='lablerow'>Az ár nem tartalmazza</td><td><?=$editarr[not_include]?></td></tr>


<? if($editarr[special_comment] <> '') { ?>
<tr><td class='lablerow'>Speciális megjegyzés</td><td><?=$editarr[special_comment]?></td></tr>
<? } ?>
</table>

</fieldset>