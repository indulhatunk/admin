<?
header('Content-type: text/html; charset=utf-8');

include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");


$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

if($CURUSER["userclass"] < 50) {
	header('location: ../index.php');
} 

$cid = (int)$_GET[cid];

if($cid > 0)
{
$query = $mysql->query("SELECT * FROM partners WHERE pid = $cid");
$editarr = mysql_fetch_assoc($query);
?>
<link rel="stylesheet" href="inc/style.css" type="text/css" media="all" />
<style>
	label { font-weight:bold; }
</style>
<fieldset>
		<legend>Partnerek kezelése</legend>
		<ul>
		<li><label>CoreDB ID:</label><?=$editarr[coredb_id]?></li>
		<li><label>CoreDB Fo csomag:</label><?=$editarr[coredb_package]?></li>

		<li><label>Ügyfélszám:</label><?=$editarr[partner_id]?></li>
		<li><label>Cég neve:*</label><?=$editarr[company_name]?></li>
		<li><label>Hotel neve:</label><?=$editarr[hotel_name]?></li>
		<li><label>Adószám:</label><?=$editarr[tax_no]?></li>
		<li><label>Kapcsolattartó neve:</label><?=$editarr[contact]?></li>
		<li><label>Bankszamlaszam:</label><?=$editarr[account_no]?></li>
		<li><label>Cím:</label><?=$editarr[zip]?> <?=$editarr[city]?> <?=$editarr[address]?></li>
		<li><label>Telefonszám:</label><?=$editarr[phone]?></li>
		<li><label>E-mail cím:</label><?=$editarr[email]?></li>
		<hr/>
		<li><label>Recepció e-mail:</label><?=$editarr[reception_phone]?></li>
		<li><label>Recepció telefonszám:</label><?=$editarr[reception_email]?></li>
		<hr/>
		
		<li><label>Jutalék:</label><?=$editarr['yield']?>%</li>
		<li><label>Felhasználónév:</label><?=$editarr[username]?></li>
		<li><label>Jelszó:</label><?=$editarr[password]?></li>
		<?
	if($editarr[property_main] == 1) 
		$type = "Üdülésicsekk";
	elseif($editarr[property_main] == 2) 
		$type = "Wellness";
	elseif($editarr[property_main] == 3) 
		$type = "Gyermekbarát";
	elseif($editarr[property_main] == 4) 
		$type = "Hegyvidék";
	elseif($editarr[property_main] == 5) 
		$type = "Vízpart";
	elseif($editarr[property_main] == 6) 
		$type = "Kastélyszálló";

		?>
		<li><label>Fő típusa:</label><?=$type?></li>

		<li><label>Üdülésicsekk:</label><?if($editarr[property_check]==1)echo "Igen";?></li>
		<li><label>Wellness:</label><?if($editarr[property_wellness]==1)echo "Igen";?></li>
		<li><label>Gyermekbarát:</label><?if($editarr[property_child]==1)echo "Igen";?></li>
		<li><label>Hegyvidék:</label><?if($editarr[property_mountain]==1)echo "Igen";?></li>
		<li><label>Vízpart:</label><?if($editarr[property_water]==1)echo "Igen";?></li>
		<li><label>Kastélyszálló:</label><?if($editarr[property_castle]==1)echo "Igen";?></li>
	</ul>
	</fieldset>

<?
}
?>