<?
/*
 * customer.php 
 *
 * customer info page
 *
*/

/* bootstrap file */
include("../inc/tour.init.inc.php");

userlogin();

if($CURUSER[userclass] < 5)
	$extraSelect = "AND partner_id = '$CURUSER[coredb_id]'";

$cid = (int)$_GET[cid];
if($cid > 0)
{

$arr = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM offer_requests WHERE id = $cid $extraSelect"));

$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $arr[partner_id]"));

$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM accomodation WHERE id = $arr[offer_id]"));


if($arr[fix] == '3day')
{
	$fix = "&plusmn; 3 nap";
}
elseif($arr[fix] == '7day')
{
	$fix = "&plusmn; 7 nap";
}
else
{
	$fix = "pontos dátum";
}
?>	
	
<style>
	label { font-weight:bold; }
	.lablerow { font-weight:Bold; width:140px; background-color:#e7e7e7}
	

table
{
    border-width: 0 0 1px 1px !important;
    border-spacing: 0 !important;
    border-radius:3px !important;
    border-collapse: collapse;
}
table, td
{
    border-color: #c4c4c4 !important;
    border-width:1px !important;
    border-style: solid !important;
	font-size:12px !important;
	font-family:arial;
}

</style>
<fieldset style='width:450px'>
		<legend>Foglaló adatai</legend>
		
		<table width='100%'>
			<tr>
				<td class='lablerow'><?=$lang[id]?>:</td>
				<td><?=$arr[offer_id]?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[partner]?>:</td>
				<td><?=$partner[name]?></td>
			</tr>
			<tr>
				<td class='lablerow'>Csomag:</td>
				<td><?=$offer[name]?></td>
			</tr>
			<tr>
				<td class='lablerow'>Érdeklődő neve:</td>
				<td><?=$arr[name]?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[phone]?>:</td>
				<td><?=$arr[phone]?></td>
			</tr>
			<tr>
				<td class='lablerow'><?=$lang[email]?>:</td>
				<td><a href="mailto:<?=$arr[email]?>"><?=$arr[email]?></a></td>
			</tr>
			<tr>
				<td class='lablerow'>Érkezés napja:</td>
				<td><?=formatdate($arr[startdate])?> (<?=$fix?>)</td>
			</tr>
			<tr>
				<td class='lablerow'>Éjszakák száma:</td>
				<td><?=$arr[days]?> éj</a></td>
			</tr>
			<tr>
				<td class='lablerow'>Személyek száma:</td>
				<td><?=$arr[adults]?> felnőtt + <?=$arr[child]?> gyermek </td>
			</tr>
			<tr>
				
			</tr>
			<tr>
				<td class='lablerow'>Megjegyzés:</td>
				<td><?=$arr[comm]?></a></td>
			</tr>
			<tr>
				<td colspan='2' align='center'>
				<form method="post" action="mailto:<?=$arr[email]?>" enctype="text/plain">
				<input type='submit' value='Választ írok!'/>
				</form></td>
			</tr>
			</table>
			
	
	</fieldset> 
<?
}
?>