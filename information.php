<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();



$edit = (int)$_POST[id];
$body = $_POST[body];

if($_GET[editid] > 0)
	$editarr = mysql_fetch_assoc($mysql->query("SELECT * FROM information WHERE id = $_GET[editid]"));
	
if($_POST[body] <> '' && $edit == 0)
{
	$_POST[added] = 'NOW()';
	$_POST[addedby] = $CURUSER[username];
	$mysql->query_insert("information",$_POST);
	writelog("$CURUSER[username] added a new item to the news list");

}
	
if($_POST[body] <> '' && $edit > 0)
{
	$mysql->query_update("information",$_POST,"id=$_POST[id]");
	writelog("$CURUSER[username] edited a $_POST[id] item to the news list");

}

head("Tudásbázis kezelése");
?>


<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>

<?
mysql_query("SET NAMES 'utf8'"); 
$cid = (int)$_GET[cid];

$type = $_GET[type];

if($type <> 'index')
	$type = 'wiki';
	
$qr = "SELECT * FROM information WHERE type = '$type' ORDER BY id DESC";
$query = mysql_query($qr); 

echo $msg;
?>



<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?type=wiki" class="<? if($_GET[type]<> 'index' && $_GET[add] <> 1) echo "current";?>">Tudásbázis</a></li>
	<? if($CURUSER[userclass] == 255) { ?>
		<li><a href="?type=index"  class="<? if($_GET[type]=='index') echo "current";?>">Hírek</a></li>
	<? } ?>
		<li><a href="?add=1" class="<? if($_GET[add]==1) echo "current";?>"></b>Új létrehozása</b></a></li>
	</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>

<?

if($_GET[add] == 1)
{
?>
<div class="partnerForm">
	<form method="POST" action='information.php'>
		<input type="hidden" name="id" value="<?=$editarr[id]?>">
	<fieldset class="news">
		<legend>Hirek kezelése</legend>
	<ul>
		<li>
			<label>A hír típusa</label>
			<select name='type'>
				<option value='wiki' <? if($editarr[type] == 'wiki' || $editarr[type] == '') echo "selected"?>>Tudásbázis</option>
			<? if($CURUSER[userclass] == 255) { ?>
				<option value='index' <? if($editarr[type] == 'index') echo "selected"?>>Főoldali hír</option>
			<? } ?>
			</select>
		</li>
		<li><label>Hír cime:</label><input type="text" name="title" value="<?=$editarr[title]?>"/></li>
		<li><label>Szövege</label></li>
		<li><textarea name="body" rows="30" cols="100" style="width: 40%" class="tinymce"><?=$editarr[body]?></textarea></li>
		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</fieldset>
	</form>
</div>
<?
}
else
{

echo "<ul>";
while($arr = mysql_fetch_assoc($query)) {

	if($arr[id] == 14)
		$class = '';
	else
		$class = 'hidden';
	echo "<li><a href='#' id='$arr[id]' class='showinfo'>$arr[title]</a>
		<div id='info-$arr[id]' class='$class kbinfo'>
		<div class='kbtitle'><a href='?editid=$arr[id]&add=1'>[szerkeszt]</a></div>
		$arr[body]
		</div>
	</li>";
}

echo "</ul>";
/*
echo "<table class=\"general\" style='width:100%'>";

echo "<tr class=\"header\">";
	echo "<td>Dátum</td>";
	echo "<td>Cím</td>";
	echo "<td>Szöveg</td>";
	echo "<td width='50'>Szerkeszt</td>";
echo "</tr>";



while($arr = mysql_fetch_assoc($query)) {
	echo "<tr class=\"$class\">";
		echo "<td width='120'>$arr[added]</td>";
		echo "<td>$arr[title]</td>";
		echo "<td>$arr[body]</td>";
		echo "<td><a href='?editid=$arr[id]&add=1'>[szerkeszt]</a></td>";
	echo "</tr>";

}
echo "</table>";
*/	

}
?>
</div></div>
<?
foot();
?>