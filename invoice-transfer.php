<?
/*
 * invoices.php 
 *
 * the invoice list page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

/**
 * https://admin.indulhatunk.hu/weekly-transfer.php?transfer_clear=JT-M2016/002855&company=szallasoutlet
 */
userlogin();


if($_GET[transfer_clear] <> '' && $_GET[sure] <> 1)
{
	echo "<h2>Biztosan elutalta a <b><font color='red'>$_GET[transfer_clear] / $_GET[company]</font></b> tételeket?</h1><br/>  <a href=\"invoice-transfer.php?transfer_clear=$_GET[transfer_clear]&sure=1&company=$_GET[company]\" class='button green'>$lang[yes]</a> <a href=\"#\" class='button red'>$lang[no]</a><div class='cleaner'></div>";
	die;
//	$mysql->query("UPDATE customers SET transfer_sent = '2012-01-12 10:00:00' WHERE invoice_number  = '$_GET[transfer_sent]'");
	
}

if(!empty($_GET[transfer_clear]) && $_GET[sure] == 1)
{


    //	if($arr[invoice_number] <> 'HO-M2012/000170' && $arr[invoice_number] <> 'HO-M2012/000183')
    $mysql->query("UPDATE customers SET transfer_sent = NOW() WHERE invoice_number  = '$_GET[transfer_clear]'");
    writelog("$CURUSER[username] SET manually transfer sent ON invoice $_GET[transfer_clear]");
    //	$mysql->query("UPDATE customers SET transfer_sent = '2012-02-02 13:30:00' WHERE invoice_number  = '$arr[invoice_number]'");
    //}
    if(empty($_GET[company])) $_GET[company] = 0;
    if(empty($_GET[hotel])) $_GET[hotel] = 0;
    
    header("Location: invoice.php?company=$_GET[company]&hotel=$_GET[hotel]");

}
?>