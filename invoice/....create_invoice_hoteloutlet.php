<?php
header('Content-type: text/html; charset=utf-8');
include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");

define( 'USER',		'1449-motor');
define( 'PASSWORD', 'motorpass');
define( 'API_KEY', 'D9-2ydLs');
define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
error_reporting(0);

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

$sure = $_GET[sure];

if($sure <> 1) {
	head();
	echo message("<b>Biztosan ki szeretne nyomtatni a heti HotelOutlet számlat? <a href=\"?sure=1\">Igen!</a> | <a href=\"../customers.php\">Nem!</a></b>");
	foot();
	die;
}


if($CURUSER["userclass"] <> 255) {
	header("location: customers.php");
}

$week = date("W")-1;
$month = date("n");
$year = date("Y");


if($month <10) {
	$zeroMonth = "0".$month;
}
else
	$zeroMonth = $month;
	

if($week == 0)
{
	$week = 52;
	$year = $year - 1;
}

echo "$year/$week<hr/>";


$week2minus = $week - 2;
				
if($week == 52 && $year == 2011)
	$tax = 1.27;
elseif($year >= 2012)
	$tax = 1.27;
else
	$tax = 1.25;
	
	$itemQuery = $mysql->query("SELECT partners.invoice_name, partners.invoice_zip, partners.invoice_city,partners.invoice_address,partners.hotel_name,partners.pid,partners.company_name,partners.tax_no,partners.phone,partners.email,partners.zip,partners.address,partners.city,partners.yield_vtl FROM customers
		INNER JOIN partners ON partners.pid = customers.pid 
		WHERE 
		(
			(
				week(customers.paid_date,3) = $week AND
				YEAR(paid_date) = $year AND
				(customers.payment <> 5 OR customers.invoice_name <> '') AND
				postpone = 0
			)
			OR
			(
				week(customers.check_arrival,3) = $week2minus AND
				YEAR(check_arrival) = $year AND
				customers.payment = 5 AND
				customers.checkpaper_id > 0
			)  
			
		)
			AND
			
			customers.paid = 1 AND 
			inactive = 0 AND
			customers.company_invoice = 'hoteloutlet' AND
			customers.facebook = 0 AND
			customers.invoice_created = 0
			
			GROUP BY customers.pid ORDER BY partners.company_name ASC"); //customers.invoice_created = 0 AND temp
			
	



echo ('Futtatás időpontja: <b>'.date('Y.m.d. H:i:s').'</b><br>');


// enable error reporting
use_soap_error_handler(true);

ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache

$kelt= date('Y-m-d');
$fiz_hat= date('Y-m-d');
$telj= date('Y-m-d');

	$client = new SoapClient(WSDL_FILE,
					array('login'=> USER, 'password'=> PASSWORD, 'trace' => 1,
							'features'=> SOAP_SINGLE_ELEMENT_ARRAYS,
							'exceptions' => true));
		
	
class TSzlaTetelek {
	public $tetel;
}

while($partnerArr = mysql_fetch_assoc($itemQuery)) {
		echo "<hr/>".$partnerArr[company_name]."";
$subtotal = 0;		


// SOAP hívás:
try {

	$itemsQuery = $mysql->query("SELECT * FROM 
		customers WHERE 
		
		pid = $partnerArr[pid] AND
		paid = 1 AND
		inactive = 0 AND
		(
			(
				week(customers.paid_date,3) = $week AND
				YEAR(paid_date) = $year AND
				(customers.payment <> 5 OR customers.invoice_name <> '') AND
				postpone = 0
			) 
			OR
			(
				week(customers.check_arrival,3) = $week2minus AND
				YEAR(check_arrival) = $year AND
				customers.payment = 5 AND
				customers.checkpaper_id > 0
			) 
			)
			
	AND 

	customers.facebook = 0 AND
	customers.company_invoice = 'hoteloutlet' AND
	customers.invoice_created = 0
	"); //	invoice_created = 0 AND
	
	

	$tetelek = new TSzlaTetelek();
	$stotal = 0;
	$place = 0;
	$itemcount = 0;
	while($itemArr = mysql_fetch_assoc($itemsQuery)) {

	
		if($itemArr[payment] == 6)
		{
			$place = $place+$itemArr[orig_price];
		}
		$stotal = $stotal+$itemArr[orig_price];
		
		
	
	$tetelek->tetel[] =array(
		//	'cikkszam' => 'pg2344/12c',
			'megnev' => "$itemArr[offer_id] / $itemArr[name] - ".formatPrice($itemArr[orig_price])." után járó jutalék",
		//	'besorolas_tip' => 'VTSZ',
		//	'besorolas' => '64.20.12',
			'mennyisegi_egy' => 'db',
			'mennyiseg' => 1,
			'afa_kulcs' => 27,
			'afa_nev' => '27%',
			//'netto_egysegar' => 6000,
			// Melyik egységárat szeretné megadni? Csak egyet adhat meg!
			'netto_egysegar' => $itemArr[orig_price]*($partnerArr[yield_vtl]/100),
			'kedvezmeny' => 0,
//			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
		);
		$subtotal=$subtotal+($itemArr[orig_price]*($partnerArr[yield_vtl]/100));
//		echo "elem db:<hr/>";
//		echo "elem osszesen:".$subtotal."<hr/>";
		$itemcount++;
	}
	
	if(($stotal - $place) <= 0)
		$cmt = "A SZÁMLA PÉNZÜGYI TELJESÍTÉST IGÉNYEL!";
	else
		$cmt = "A SZÁMLA PÉNZÜGYI TELJESÍTÉST NEM IGÉNYEL!";
	
	//echo "<hr/>total: $stotal<br/>place: $place <hr/>";	

	if($partnerArr[invoice_name] <> '')
		{
			$partnerArr[name] = $partnerArr[invoice_name];
			$partnerArr[zip] = $partnerArr[invoice_zip];
			$partnerArr[city] = $partnerArr[invoice_city];
			$partnerArr[address] = $partnerArr[invoice_address];
			$partnerArr[tax_no] = '';
		}
	// - Mit szeretne?
	$szla_tipus= 'N';	// Normál számla készítése
	$prefix= 'HO-';			// 'DIJ-'.'TR-'
	$SzlaKeszitRequest = array(
		'params' => array(
			'api_key' => API_KEY,
			'biz_tip' => 'E'.$szla_tipus.'S',	// Normál / Normál / Számla
			//'biz_tip' => 'NND',	// Elektronikus / Normál / Díjbekérő
			'gen_pdf' => 1,			// 0 - ne készítsen, 1 - készítsen PDF-et
			'prefix' => $prefix.'M',		// -> fej->vevo->tomb !	DIJ-	V
			'kerekitesi_mod' => 'vegosszesen',
			//	'kerekitesi_mod' => 'vegosszesen',
			'sajat_azon' => '3984753847539847937984987'
			//	'sajat_azon' => basename(__FILE__)		// Küldjük el a fájl nevét!
		),
		'data' => array(
			'szla_xml' => array(
				'stilus' => array(
					'megjelenes' => 'modern',
					'tipus' => 'normal',		// normal, ablakos, csekkes, egyszeru,
					'nyelv' => 1,				// 1 - magyar, 2 - magyar/angol, 3 - magyar/német
					//'csak_elonezet' => 1	// 1 - csak előnézet, más - elkészítése
										
					'van_kiallito' => '1',
					//'kiallito' => 'Tóth József',
					
					'van_atvevo' => '1'//,
					//'atvevo' => 'Szabó Sándor'		
				),
				'fej' => array(
					'vevo' => array(
						'nev' => "$partnerArr[company_name]|$partnerArr[hotel_name]",
						'adoszam' => "$partnerArr[tax_no]",
						'cim' => array( 'irsz' => "$partnerArr[zip]", 'varos'=> "$partnerArr[city]", 'utca'=>"$partnerArr[address]", 'orszag'=>'1'),
						'lev_cim' => array(  'irsz' => "$partnerArr[zip]", 'varos'=> "$partnerArr[city]", 'utca'=>"$partnerArr[address]", 'orszag'=>'1'),
						'email' => "$partnerArr[email]",
					//	'cejegyzekszam' => 'cégjegyzékszám',
					//	'euadoszam' => 'HU12345678',
					//	'megjegy' => 'Szerződött partnerünk|akár két sorban is'
					),
					'tomb' => $prefix.'M2011',			// DIJ-	V		DIJ-
					'kelt' => $kelt,
					'telj' => $telj,
					'fiz_hat' => $fiz_hat,
					'fiz_mod' => '2',		// 1 - Készpénz, 2 - Átutalás, 3 - Bankkártya, 4 - Hitel, 5 - Utánvét, 6 - Paypal,
					'info' => array(
						'fejlec_info' => '',
						'fejlec_info2' => '',
						'lablec_info' => "$cmt - info@indulhatunk.info",
						'lablec_info2' => ''
					),	
					'tipus' => $szla_tipus,			// Normál / Sztornó / Másolat
					'fizetve' => '', 	//'2010-05-25',
					'deviza' => $deviza,//,		// 'EUR'
					//'arfolyam' => $arfolyam,	// 234.3
					//'hivatkozas' => $prefix.'M'.'2010'.'/'.'000044', //,
					// Akár díjbekérőt is megadhat!
					//'hivatkozas' => 'DIJ-'.$prefix.'M'.'2010'.'/'.'000014' //,	.'V'	'DIJ-'.	
					 //'van_afa' => '1',
					// Ellenőrzés miatt:
				//	'netto_ossz' => round($subtotal,2),
				//	'brutto_ossz' => round($subtotal*$tax,2)+0.01
				),
				'tetelek' =>  $tetelek
			)
		)
	);


	
	echo "Számla adatok:<br/>";
	echo formatPrice($subtotal*$tax,2)." / $itemcount db tétel<hr/>";
	
	if($_GET[show] == 1)
	{
		echo "<pre>";
		print_r( $SzlaKeszitRequest );
		echo "</pre>";
		echo "<hr/>";
	}

			
	//$result = $client->SzlaKeszit($SzlaKeszitRequest);
	$hiba= false;
	
} catch (SoapFault $fault) {
	echo "SOAP exception: ".$fault->getMessage()."<br/>";
	$hiba= $fault->getMessage();
	
}	// try

echo "<br/>";

// Hibás?
if ($hiba || is_null($result) || is_soap_fault($result)) {
	if (isset($result)) {
		echo "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})<br/>";
	}	// if (isset($result))
	echo "Hiba: ".$hiba."<br/>";
// OK.
} else {
//	echo "Visszakapott adatok:<br/>";
//	echo "Saját azonosító:      <b>".$result->sajat_azon.'</b><br>';
//	echo "Tranzakció azonosító (több sorban!): <b>".chunk_split($result->tr_id,40,'<br>').'</b><br>';
	echo "Státusz:              <b>".$result->status.' </b><br>';
//	echo "Üzenetek:             ".'<br>';
	if (is_object($result->messages)) {
		foreach ($result->messages as $messages) {
			
			foreach ($messages as $message) {
				//var_dump($message->uzenet);
				if (isset($message->uzenet)) {
					echo "             (".$message->kod.") <b>".$message->uzenet.'</b>'.
						' (mező: '.$message->mezo.', adat: '.$message->adat.')<br>';
				}
				
			}
		}	// foreach ($result['messages'] as $uzenet)
		
	}	// if (is_array())
	if ($result->status=='Ok' || isset($result->szla_resp)) {
	///	echo "Számla:<br>";
		echo "- sorszám:                     <b>".$result->szla_resp->sorszam.'</b><br>';
		echo "- nettó (Ft) összesen:              <b>".$result->szla_resp->netto_ossz.' Ft</b><br>';
		echo "- bruttó (Ft) összesen:             <b>".$result->szla_resp->brutto_ossz.' Ft</b><br>';
		
		writelog($result->szla_resp->sorszam." invoice created");
	
		if (isset($result->szla_resp->pdf)) {
			$filename = "../invoices/vatera/".str_replace("/","_",$result->szla_resp->sorszam).".pdf";
			$fh = fopen($filename, 'w') or print("can't open file");
			fwrite($fh, base64_decode($result->szla_resp->pdf));
			fclose($fh);
			//// AND invoice_created = 0 AND
			
		$updateqr = "UPDATE customers SET invoice_created = 1,invoice_date = NOW(), invoice_number = '".$result->szla_resp->sorszam."' WHERE pid = $partnerArr[pid] AND paid = 1  AND customers.invoice_created = 0 AND company_invoice = 'hoteloutlet'
		 AND (
			(
				week(customers.paid_date,3) = $week AND
				YEAR(paid_date) = $year AND
				customers.payment <> 5
			)
			OR
			(
				week(customers.check_arrival,3) = $week2minus AND
				YEAR(check_arrival) = $year AND
				customers.payment = 5
			) 
			) 
			";
			
			$mysql->query("$updateqr"); 
		//writelog($updateqr);
		
			//invoice request log
		 	$invoice_log = array();
		 	$invoice_log[added] = 'NOW()';
		 	$invoice_log[invoice_number] = $result->szla_resp->sorszam;
		 	$invoice_log[request] = serialize($SzlaKeszitRequest);
		 	$invoice_log[type] = 'partner_vtl';
		 	$invoice_log[company] = 'hoteloutlet';
		 	$mysql->query_insert("log_invoice",$invoice_log);
		 
		 	if($result->szla_resp->brutto_ossz == 0)
		 	{
		 		//alert mail
		 		sendEmail("Nulla értékű számla","Nulla értékű számla: ".$result->szla_resp->sorszam,"it@indulhatunk.hu","");
		 	}
		 	
		 	
			writelog($result->szla_resp->sorszam." invoice saved");
		} else {
		}
		
					
	}	// if ($result->status=='Ok')
}	// if (is_soap_fault($result)) 
}//end of partner query

echo "<a href=\"../customers.php\">Vissza a vásárlók oldalra</a>";
// ==============================================================================================================
?>