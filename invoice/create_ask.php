<?php
header('Content-type: text/html; charset=utf-8');

include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");
include("create_invoice_user_ask.php");


$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

if($CURUSER["userclass"] < 90) {
	header("location: ../index.php");
}

$cid = (int)$_GET[cid];

if($cid > 0)
{
	create_user_invoice_ask($cid);
}
echo "Díjbekérő elküldve!";

?>