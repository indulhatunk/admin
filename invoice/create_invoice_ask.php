<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("../inc/init.inc.php");

//check if user is logged in or not
userlogin();

function create_invoice($invoice) {


	global $mysql;
	global $lang;
	
	error_reporting(0);
	if(class_exists('TSzlaTetelek') != true)
	{
 		class TSzlaTetelek {
			public $tetel;
		}
	
	}	
/*
define( 'USER',		'493-motor');
define( 'PASSWORD', 'motorpass');
define( 'API_KEY', '_OjXkza0');
define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
*/


$cmp = getCompany($_POST[company], 'single');

	define( 'USER',		$cmp[invoice_user]);
	define( 'PASSWORD', $cmp[invoice_pass]);
	define( 'API_KEY', $cmp[invoice_key]);
	define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
	$prefix= 'DIJ-';


/*
	if($_POST[company] == 'hoteloutlet')
{

	
}
elseif($_POST[company] == 'szallasoutlet')
{
	define( 'USER',		'2126-motor');
	define( 'PASSWORD', 'motorpass');
	define( 'API_KEY', 'MF3K0-dA');
	define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
	$prefix= 'DIJ-';
}
elseif($_POST[company] == 'horizonttravel')
{
	define( 'USER',		'2629-motor');
	define( 'PASSWORD', 'motorpass');
	define( 'API_KEY', '4JMQnP79');
	define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
	$prefix= 'DIJ-';
}
elseif($company == 'indusz')
{
	define( 'USER',		'2297-motor');
	define( 'PASSWORD', 'motorpass');
	define( 'API_KEY', 'Z36R1bK2');
	define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
	$prefix= 'DIJ-';
}
else
{
	define( 'USER',		'493-motor');
	define( 'PASSWORD', 'motorpass');
	define( 'API_KEY', '_OjXkza0');
	define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
	$prefix= 'DIJ-';
}
*/

use_soap_error_handler(true);

ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache

$client = new SoapClient(WSDL_FILE,
					array('login'=> USER, 'password'=> PASSWORD, 'trace' => 1,
							'features'=> SOAP_SINGLE_ELEMENT_ARRAYS,
							'exceptions' => true));
	
	$subtotal = 0;		
	// SOAP call:
	try {
		$tetelek = new TSzlaTetelek();
		foreach($invoice[megnev] as $key => $item) {
			
		if(trim($item) <> '')
		{
		
			if($invoice[afa_kulcs][$key] == 'AHK')
			{
				$afanev = '0%';
			}
			else
			{
				$afanev = $invoice[afa_kulcs][$key].'%';
			}
		$tetelek->tetel[] =array(
		//	'cikkszam' => 'pg2344/12c',
			'megnev' => $item,
			'besorolas_tip' => $invoice[besorolas_tip][$key],
			'besorolas' => $invoice[besorolas][$key],
			'mennyisegi_egy' => $invoice[mennyisegi_egy][$key],
			'mennyiseg' => $invoice[mennyiseg][$key],
			'afa_kulcs' => $invoice[afa_kulcs][$key],
			'afa_nev' => $afanev,
			'netto_egysegar' => $invoice[netto_egysegar][$key],
			'kedvezmeny' => 0,
		//'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
		);
		$subtotal=$subtotal+($itemArr[orig_price]);
		}
	}	

	$szla_tipus= 'N';
	
	$SzlaKeszitRequest = array(
		'params' => array(
			'api_key' => API_KEY,
			'biz_tip' => 'N'.$szla_tipus.'D',
			'gen_pdf' => 1,	
			'prefix' => $prefix.'M',
			'kerekitesi_mod' => 'vegosszesen',
			'sajat_azon' => '3984753847539847937984987'
		),
		'data' => array(
			'szla_xml' => array(
				'stilus' => array(
					'megjelenes' => 'modern',
					'tipus' => 'normal',
					'nyelv' => 1,
					'van_kiallito' => '1',
					'van_atvevo' => '1'//,
				),
				'fej' => array(
					'vevo' => array(
						'nev' => $invoice[nev],
						'adoszam' => $invoice[adoszam],
						'cim' => array( 'irsz' => $invoice[irsz], 'varos'=> $invoice[varos], 'utca'=>$invoice[cim], 'orszag'=>'1'),
						'lev_cim' => array(  'irsz' => $invoice[irsz], 'varos'=> $invoice[varos], 'utca'=>$invoice[cim], 'orszag'=>'1'),
						'email' => $invoice[email],
					//	'cejegyzekszam' => 'cégjegyzékszám',
					//	'euadoszam' => 'HU12345678',
						'megjegy' => $invoice[megjegy]
					),
					'tomb' => $prefix.'M'.date("Y"),
					'kelt' => $invoice[kelt],
					'telj' => $invoice[telj],
					'fiz_hat' => $invoice[fiz_hat],
					'fiz_mod' => $invoice[fiz_mod],
					'info' => array(
						'fejlec_info' => '',
						'fejlec_info2' => '',
						'lablec_info' => $invoice[lablec],
						'lablec_info2' => ''
					),	
					'tipus' => $szla_tipus,			// Normál / Sztornó / Másolat
					'fizetve' => $invoice[kelt], 	//'2010-05-25',
					'deviza' => $deviza,//,		// 'EUR'
				//	'netto_ossz' => round($subtotal/1.18,2),
				//	'brutto_ossz' => round($subtotal,2)
				),
				'tetelek' =>  $tetelek
			)
		)
	);

	$result = $client->SzlaKeszit($SzlaKeszitRequest);
	
	//echo "<pre>";
	//	print_r($SzlaKeszitRequest);
	//echo "</pre>";
	


	$hiba= false;

} catch (SoapFault $fault) {
	echo "SOAP exception: ".$fault->getMessage()."<br/>";
	$hiba= $fault->getMessage();
	
}	// try

if ($hiba || is_null($result) || is_soap_fault($result)) {
	if (isset($result)) {
		echo "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})<br/>";
	}	// if (isset($result))
	echo "Hiba: ".$hiba."<br/>";
} else {
	if (is_object($result->messages)) {
		foreach ($result->messages as $messages) {
			
			foreach ($messages as $message) {
				//var_dump($message->uzenet);
				if (isset($message->uzenet)) {
					echo "             (".$message->kod.") <b>".$message->uzenet.'</b>'.
						' (mező: '.$message->mezo.', adat: '.$message->adat.')<br>';
				}
				
			}
		}	// foreach ($result['messages'] as $uzenet)
		
	}	// if (is_array())
	if ($result->status=='Ok' || isset($result->szla_resp)) {
		
		writelog($result->szla_resp->sorszam." invoice created for item $itemArr[offer_id]");
	
		if (isset($result->szla_resp->pdf)) {
	
			if($month <10) 
				$zeroMonth = "0".$month;
			else
				$zeroMonth = $month;
					
			
			$fn = date("Y-m-d")."_".clean_url($invoice[nev])."_";
					
			$filename = "../invoices/dijbekero/$fn".str_replace("/","_",$result->szla_resp->sorszam).".pdf";
			$fh = fopen($filename, 'w') or print("can't open file");
			fwrite($fh, base64_decode($result->szla_resp->pdf));
			fclose($fh);
		 
	 //email
	 $random_hash = md5(date('r', time()));
    //set email headers
    $headers = "Return-path: <billing@indulhatunk.info>"."\r\n";
    $headers .= "Reply-to: <billing@indulhatunk.info>"."\r\n";
    $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\"\r\n";
    $headers .= "From: billing@indulhatunk.info <billing@indulhatunk.info>"."\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: INDULHATUNK-MAILER\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "\r\n\r\n";

    $attachment = chunk_split(base64_encode(file_get_contents("../invoices/vatera/$fn".str_replace("/","_",$result->szla_resp->sorszam).".pdf")));
    $prebody = "--PHP-mixed-{$random_hash}\r\n";
    $prebody .="Content-Type: text/plain; charset=\"utf-8\"\r\n";
    $prebody .="Content-Transfer-Encoding: 7bit\r\n";
    $prebody .="\r\n";
    $prebody .="Indulhatunk.hu számla";
    $prebody .="";
    $prebody .="\r\n";
    $prebody .="--PHP-mixed-{$random_hash}\r\n";
    $prebody .="Content-Type: application/pdf; name=\"".$result->szla_resp->sorszam.".pdf\"\r\n";
    $prebody .="Content-Transfer-Encoding: base64\r\n";
    $prebody .="Content-Disposition: attachment; filename=\"".$result->szla_resp->sorszam.".pdf\"\r\n\r\n";
    $prebody .="{$attachment}\r\n\r\n";
    $prebody .="--PHP-mixed-{$random_hash}--\r\n";

	
    @mail("$invoice[email]", "Indulhatunk.hu számla", $prebody, $headers);
   // @mail("attila.forro@indulhatunk.hu", "Indulhatunk.hu számla", $prebody, $headers);
		 //email end
		 
		 	//invoice request log
		 	$invoice_log = array();
		 	$invoice_log[added] = 'NOW()';
		 	$invoice_log[invoice_number] = $result->szla_resp->sorszam;
		 	$invoice_log[request] = serialize($SzlaKeszitRequest);
		 	$invoice_log[type] = 'user';
		 	$invoice_log[company] = $_POST[company];
		// 	$mysql->query_insert("log_invoice",$invoice_log);
		 
		 	if($result->szla_resp->brutto_ossz == 0)
		 	{
		 		//alert mail
		 		sendEmail("Nulla értékű számla","Nulla értékű számla: ".$result->szla_resp->sorszam,"it@indulhatunk.hu","");
		 	}
			
			writelog($result->szla_resp->sorszam." dijbekero created by $CURUSER[username]");
	}
	else 
	{}
		
					
	}
}	

echo "<a href=\"../invoices/dijbekero/$fn".str_replace("/","_",$result->szla_resp->sorszam).".pdf\">A számla letöltése</a>";
}

head();
create_invoice($_POST);
foot();
?>