<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("../inc/init.inc.php");

//check if user is logged in or not
userlogin();

function create_invoice($invoice) {


	global $mysql;

	error_reporting(0);
	if(class_exists('TSzlaTetelek') != true)
	{
 		class TSzlaTetelek {
			public $tetel;
		}
	
	}	
/*
define( 'USER',		'493-motor');
define( 'PASSWORD', 'motorpass');
define( 'API_KEY', '_OjXkza0');
define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
*/

$cmp = getCompany($_POST[company], 'single');

$prefix= $cmp[invoice_prefix];


if($invoice[deviza] <> '')
{
	$devsuffix = "V";
	$invoice[arfolyam] = $invoice[arfolyam];
}
else
{
	$invoice[arfolyam] = '';
}

require_once('soap.inc.php');
$client = createSOAPClient(trim($_POST[company]));
//debug($_POST[company]); die();

	
	$subtotal = 0;		
	// SOAP call:
	try {
		$tetelek = new TSzlaTetelek();
		foreach($invoice[megnev] as $key => $item) {
			
		if(trim($item) <> '')
		{
		
			if($invoice[afa_kulcs][$key] == 'AHK')
			{
				$afanev = '0%';
			}
			else
			{
				$afanev = $invoice[afa_kulcs][$key].'%';
			}
		$tetelek->tetel[] =array(
		//	'cikkszam' => 'pg2344/12c',
			'megnev' => $item,
			'besorolas_tip' => $invoice[besorolas_tip][$key],
			'besorolas' => $invoice[besorolas][$key],
			'mennyisegi_egy' => $invoice[mennyisegi_egy][$key],
			'mennyiseg' => $invoice[mennyiseg][$key],
			'afa_kulcs' => $invoice[afa_kulcs][$key],
			'afa_nev' => $afanev,
			'netto_egysegar' => $invoice[netto_egysegar][$key],
			'kedvezmeny' => 0,
		//'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
		);
		$subtotal=$subtotal+($itemArr[orig_price]);
		}
	}	

	$szla_tipus= 'N';
	
	$SzlaKeszitRequest = array(
		'params' => array(
			'api_key' => API_KEY,
			'biz_tip' => 'E'.$szla_tipus.'S',
			'gen_pdf' => 1,	
			'prefix' => $prefix.'M'.$devsuffix,
			'kerekitesi_mod' => 'vegosszesen',
			'sajat_azon' => '3984753847539847937984987'
		),
		'data' => array(
			'szla_xml' => array(
				'stilus' => array(
					'megjelenes' => 'modern',
					'tipus' => 'normal',
					'nyelv' => $invoice[language],
					'van_kiallito' => '1',
					'van_atvevo' => '1'//,
				),
				'fej' => array(
					'vevo' => array(
						'nev' => $invoice[nev],
						'adoszam' => $invoice[adoszam],
						'cim' => array( 'irsz' => $invoice[irsz], 'varos'=> $invoice[varos], 'utca'=>$invoice[cim], 'orszag'=>'1'),
						'lev_cim' => array(  'irsz' => $invoice[irsz], 'varos'=> $invoice[varos], 'utca'=>$invoice[cim], 'orszag'=>'1'),
						'email' => $invoice[email],
					//	'cejegyzekszam' => 'cégjegyzékszám',
					//	'euadoszam' => 'HU12345678',
						'megjegy' => $invoice[megjegy]
					),
					'tomb' => $prefix.'M'.$devsuffix. date("Y"),
					'kelt' => $invoice[kelt],
					'telj' => $invoice[telj],
					'fiz_hat' => $invoice[fiz_hat],
					'fiz_mod' => $invoice[fiz_mod],
					'info' => array(
						'fejlec_info' => '',
						'fejlec_info2' => '',
						'lablec_info' => $invoice[lablec],
						'lablec_info2' => ''
					),	
					'tipus' => $szla_tipus,			// Normál / Sztornó / Másolat
					'fizetve' => $invoice[kelt], 	//'2010-05-25',
					'deviza' => $invoice[deviza],
					'van_afa' => 1,//,	
					'arfolyam' => $invoice[arfolyam],//,	// 'EUR'
				//	'netto_ossz' => round($subtotal/1.18,2),
				//	'brutto_ossz' => round($subtotal,2)
				),
				'tetelek' =>  $tetelek
			)
		)
	);

	//debug($SzlaKeszitRequest);
	$result = $client->SzlaKeszit($SzlaKeszitRequest);


	$hiba= false;

} catch (SoapFault $fault) {
	echo "SOAP exception: ".$fault->getMessage()."<br/>";
	$hiba= $fault->getMessage();
	
}	// try

if ($hiba || is_null($result) || is_soap_fault($result)) {
	if (isset($result)) {
		echo "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})<br/>";
	}	// if (isset($result))
	echo "Hiba: ".$hiba."<br/>";
} else {
	if (is_object($result->messages)) {
		foreach ($result->messages as $messages) {
			
			foreach ($messages as $message) {
				//var_dump($message->uzenet);
				if (isset($message->uzenet)) {
					echo "             (".$message->kod.") <b>".$message->uzenet.'</b>'.
						' (mező: '.$message->mezo.', adat: '.$message->adat.')<br>';
				}
				
			}
		}	// foreach ($result['messages'] as $uzenet)
		
	}	// if (is_array())
	if ($result->status=='Ok' || isset($result->szla_resp)) {
		
		writelog($result->szla_resp->sorszam." invoice created for item $itemArr[offer_id]");
	
		if (isset($result->szla_resp->pdf)) {
	
			if($month <10) 
				$zeroMonth = "0".$month;
			else
				$zeroMonth = $month;
					
					
			$filename = "../invoices/vatera/".str_replace("/","_",$result->szla_resp->sorszam).".pdf";
			$fh = fopen($filename, 'w') or print("can't open file");
			fwrite($fh, base64_decode($result->szla_resp->pdf));
			fclose($fh);
		 
	 //email
	 $random_hash = md5(date('r', time()));
    //set email headers
    $headers = "Return-path: <penzugy@indulhatunk.hu>"."\r\n";
    $headers .= "Reply-to: <penzugy@indulhatunk.hu>"."\r\n";
    $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\"\r\n";
    $headers .= "From: Indulhatunk.hu <penzugy@indulhatunk.hu>"."\r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: INDULHATUNK-MAILER\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "\r\n\r\n";

    $attachment = chunk_split(base64_encode(file_get_contents("../invoices/vatera/".str_replace("/","_",$result->szla_resp->sorszam).".pdf")));
    $prebody = "--PHP-mixed-{$random_hash}\r\n";
    $prebody .="Content-Type: text/plain; charset=\"utf-8\"\r\n";
    $prebody .="Content-Transfer-Encoding: 7bit\r\n";
    $prebody .="\r\n";
    $prebody .="Kedves Partnerünk! Csatolva küldjük számlánkat. Üdvözlettel: az Indulhatunk csapata";
    $prebody .="";
    $prebody .="\r\n";
    $prebody .="--PHP-mixed-{$random_hash}\r\n";
    $prebody .="Content-Type: application/pdf; name=\"".$result->szla_resp->sorszam.".pdf\"\r\n";
    $prebody .="Content-Transfer-Encoding: base64\r\n";
    $prebody .="Content-Disposition: attachment; filename=\"".$result->szla_resp->sorszam.".pdf\"\r\n\r\n";
    $prebody .="{$attachment}\r\n\r\n";
    $prebody .="--PHP-mixed-{$random_hash}--\r\n";

	
    @mail("$invoice[email]", "Indulhatunk.hu számla", $prebody, $headers);
  // @mail("attila.forro@indulhatunk.hu", "Indulhatunk.hu számla", $prebody, $headers);
		 //email end
		 
		 	//invoice request log
		 	$invoice_log = array();
		 	$invoice_log[added] = 'NOW()';
		 	$invoice_log[invoice_number] = $result->szla_resp->sorszam;
		 	$invoice_log[request] = serialize($SzlaKeszitRequest);
		 	$invoice_log[type] = 'user';
		 	$invoice_log[company] = $_POST[company];
		 	$invoice_log[telj] = $telj;
		 	$mysql->query_insert("log_invoice",$invoice_log);
		 
		 	if($result->szla_resp->brutto_ossz == 0)
		 	{
		 		//alert mail
		 		sendEmail("Nulla értékű számla","Nulla értékű számla: ".$result->szla_resp->sorszam,"it@indulhatunk.hu","");
		 	}
			
			writelog($result->szla_resp->sorszam." invoice created by $CURUSER[username]");
	}
	else 
	{}
		
					
	}
}	

echo "<a href=\"../invoices/vatera/".str_replace("/","_",$result->szla_resp->sorszam).".pdf\">A számla letöltése</a>";
}

head();
create_invoice($_POST);
foot();
?>