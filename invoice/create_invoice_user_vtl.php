<?php
//header('Content-type: text/html; charset=utf-8');
function create_user_invoice_vtl($customer = 0, $company = 'indulhatunk')
{
    
    global $mysql;
    global $outletmailNew;
    global $lang;
    
    error_reporting(E_ALL);
    
    //define current year and month
    $month = date("n");
    $year  = date("Y");
    
    
    if (class_exists('TSzlaTetelek') != true) {
        class TSzlaTetelek
        {
            public $tetel;
        }
        
    }
    $sql       = "SELECT customers.tax_no,customers.phone,customers.email,customers.zip,customers.address,customers.city,customers.name,customers.invoice_name,customers.invoice_zip,customers.invoice_city,customers.invoice_address FROM customers
		INNER JOIN partners ON partners.pid = customers.pid 
		WHERE 
			customers.paid = 1 AND 
			customers.cid = $customer";
    //print $sql; //die();
    $itemQuery = $mysql->query($sql);
    
    
    $cmp = getCompany($company, 'single');
    
    
    
    if ($company == 'hoteloutlet') {
        $prefix = 'SZO-';
    } elseif ($company == 'indusz') {
        $prefix = 'IN-';
    } elseif ($company == 'szallasoutlet') {
        $prefix = 'SZOT-';
    } elseif ($company == 'professional') {
        $prefix = 'PR-';
    } else {
        $prefix = '';
    }
    $prefix = $cmp[invoice_prefix];
    
    $kelt = date('Y-m-d'); // 4+1+2+1+2
    $telj = date('Y-m-d'); // + 7 nap
    //$telj = '2014-06-30';
    require_once('soap.inc.php');
    $client = createSOAPClient($company);
    
    //debug($client); die();
    
    
    while ($partnerArr = mysql_fetch_assoc($itemQuery)) {
        $subtotal = 0;
        
        // SOAP call:
        try {
            $sql        = "SELECT * FROM customers WHERE paid = 1 AND	user_invoice_number = '' AND user_invoice_started = 0  AND cid = $customer LIMIT 1"; // 
            //print $sql;
            $itemsQuery = $mysql->query($sql); //  
            
            
            $tetelek = new TSzlaTetelek();
            
            while ($itemArr = mysql_fetch_assoc($itemsQuery)) {
                //debug($itemArr); die('irr'); 
                //prevent concurrent sessions
                //$mysql->query("UPDATE customers SET user_invoice_started = 1 WHERE paid = 1 AND customers.cid = $customer ");
                //prevent false invoice request & foreign invoice creation
                $foreignQuery = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = $itemArr[offers_id] LIMIT 1"));
                
                $pdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $itemArr[pid] LIMIT 1"));
                
                $partnerArr['name']    = empty($partnerArr[invoice_name]) ? $partnerArr['name'] : $partnerArr[invoice_name];
                $partnerArr['zip']     = empty($partnerArr[invoice_zip]) ? $partnerArr['zip'] : $partnerArr[invoice_zip];
                $partnerArr['city']    = empty($partnerArr[invoice_city]) ? $partnerArr['city'] : $partnerArr[invoice_city];
                $partnerArr['address'] = empty($partnerArr[invoice_address]) ? $partnerArr['name'] : $partnerArr[invoice_address];
                
                /*18-27*/
                
                if ($itemArr[payment] == 5 || $partnerArr[invoice_name] <> '' || $itemArr[payment] == 10 || $itemArr[payment] == 11 || $itemArr[payment] == 12) {
                    
                    $arr         = $itemArr;
                    $offer       = $foreignQuery;
                    $twentyseven = '';
                    
                    
                    if ($arr[plus_half_board] > 0) {
                        
                        $twentyseven = $twentyseven + $arr[plus_half_board] * $offer[days];
                    }
                    for ($z = 1; $z < 7; $z++) {
                        //	echo $arr["plus_other".$z."_value"]." &raquo; plus_other".$z."_value $arr[offer_id]<hr/>";
                        
                        $extra     = $arr["plus_other" . $z . "_value"];
                        $extraname = $offer["plus_other" . $z . "_name"];
                        
                        $check = mysql_fetch_assoc($mysql->query("SELECT * FROM offers_extra_prices WHERE trim(name) = \"" . trim($extraname) . "\""));
                        
                        if ($check[vat_category] == 27) {
                            $twentyseven = $twentyseven + $extra * $offer[days];
                            $cls         = 'blue';
                        } else {
                            $cls = '';
                            
                        }
                        
                        //	if($check[vat_category] == 27)
                        //		$class = 'red';
                    }
                    
                    for ($z = 1; $z < 4; $z++) {
                        $extra     = $arr["plus_single" . $z . "_value"];
                        $extraname = $offer["plus_single" . $z . "_name"];
                        
                        $check = mysql_fetch_assoc($mysql->query("SELECT * FROM offers_extra_prices WHERE trim(name) = '" . trim($extraname) . "'"));
                        
                        
                        if ($check[vat_category] == 27) {
                            $twentyseven = $twentyseven + $extra * $offer[days];
                            $cls         = 'blue';
                        } else {
                            $cls = '';
                            
                        }
                        //if($check[vat_category] == 27)
                        //		$class = 'red';
                        
                    }
                }
                
                
                
                /*18-27*/
                
                if ($company == 'hoteloutlet') {
                    if ($itemArr[pid] == 3181) {
                        $tax      = 27;
                        $tax_code = '27%';
                        $lablec   = 'A számla közvetített szolgáltatást tartalmaz. - info@indulhatunk.hu';
                        $prefix   = 'SZO-';
                        
                        $title = "Szállodai szolgáltatás az alábbi sorszám alapján $itemArr[offer_id]";
                        $szj   = "63.30";
                        
                    } elseif ($itemArr[payment] == 5 || $partnerArr[invoice_name] <> '' || $itemArr[payment] == 10 || $itemArr[payment] == 11 || $itemArr[payment] == 12) {
                        $tax      = 18;
                        $tax_code = '18%';
                        $lablec   = 'A számla közvetített szolgáltatást tartalmaz. - info@indulhatunk.hu';
                        $prefix   = 'SZO-';
                        $title    = "Szállodai szolgáltatás az alábbi sorszám alapján $itemArr[offer_id]";
                        $szj      = "63.30";
                        
                    } else {
                        $tax      = 'AHK';
                        $tax_code = '0%';
                        $lablec   = 'A névérték átadása nem tartozik az ÁFA törvény hatálya alá.|Az okirat a 47/2007. (XII. 29) PM rendelettel módosított 24/1995. (XI.22) PM rendeletnek felel meg.';
                        $prefix   = 'DSZO-';
                        $title    = "Szállodai szolgáltatás az alábbi sorszám alapján $itemArr[offer_id]";
                        $szj      = "63.30";
                    }
                } elseif ($company == 'indusz') {
                    $prefix = 'IN-';
                    $lablec = 'Az ÁFA megállapítása a különbözet szerinti szabályozás alapján történt, mely az Áfa tv. XV. fejezete alapján került megállapításra.';
                } elseif ($company == 'szallasoutlet' || $company == 'professional') {
                    if ($itemArr[pid] == 3181) {
                        $tax      = 27;
                        $tax_code = '27%';
                        $lablec   = 'A számla közvetített szolgáltatást tartalmaz. - info@indulhatunk.hu';
                        $prefix   = 'SZ-';
                        
                        if ($company == 'professional')
                            $prefix = 'PSZ-';
                        
                        $title = "Szállodai szolgáltatás az alábbi sorszám alapján $itemArr[offer_id]";
                        $szj   = "63.30";
                        
                    } elseif ($itemArr[payment] == 5 || $partnerArr[invoice_name] <> '' || $itemArr[payment] == 10 || $itemArr[payment] == 11 || $itemArr[payment] == 12) {
                        $tax      = 18;
                        $tax_code = '18%';
                        $lablec   = 'A számla közvetített szolgáltatást tartalmaz. - info@indulhatunk.hu';
                        $prefix   = 'SZ-';
                        if ($company == 'professional')
                            $prefix = 'PSZ-';
                        
                        $title = "Szállodai szolgáltatás az alábbi sorszám alapján $itemArr[offer_id]";
                        $szj   = "63.30";
                        
                    } else {
                        $tax      = 'AHK';
                        $tax_code = '0%';
                        $lablec   = 'A névérték átadása nem tartozik az ÁFA törvény hatálya alá.|Az okirat a 47/2007. (XII. 29) PM rendelettel módosított 24/1995. (XI.22) PM rendeletnek felel meg.';
                        $prefix   = 'D-';
                        
                        if ($company == 'professional')
                            $prefix = 'PD-';
                        
                        $title = "Szállodai szolgáltatás az alábbi sorszám alapján $itemArr[offer_id]";
                        $szj   = "63.30";
                    }
                } else //indulhatunk general invoice
                    {
                    $tax      = 18;
                    $tax_code = '18%';
                    $lablec   = 'A számla közvetített szolgáltatást tartalmaz. - info@indulhatunk.hu';
                    $prefix   = '';
                    
                    $title = "Szállodai szolgáltatás az alábbi sorszám alapján $itemArr[offer_id]";
                    $szj   = "63.30";
                    
                }
                
                
                if ($itemArr[payment] == 1)
                    $method = 2;
                elseif ($itemArr[payment] == 2)
                    $method = 1;
                elseif ($itemArr[payment] == 3)
                    $method = 5;
                elseif ($itemArr[payment] == 9)
                    $method = 3;
                else
                    $method = 2;
                
                //if cash, than fiz_hat = $kelt
                if ($itemArr[payment] == 2) {
                    $fiz_hat = $kelt;
                } else {
                    $fiz_hat = date('Y-m-d', mktime(0, 0, 0, /* hó */ substr($kelt, 5, 2), /* nap */ substr($kelt, 8, 2), /* év */ substr($kelt, 0, 4)) + 11 * 24 * 60 * 60);
                }
                
                
                if ($foreignQuery[abroad] == 1)
                    $ppfix = 'K-';
                else
                    $ppfix = '';
                
                
                if ($itemArr[discount_percent] < 50)
                    $discount_type = '%';
                else
                    $discount_type = 'Ft';
                
                if ($itemArr[discount_value] > 0)
                    $extratext = " - $itemArr[discount_percent] $discount_type kedvezménnyel";
                else
                    $extratext = '';
                
                
                if ($company == 'indusz') {
                    $yield = 0;
                    
                    //echo "$itemArr[orig_price]*($pdata[yield_vtl]/100)*1.27<hr/>";
                    if ($itemArr[is_qr] == 1) {
                        $yield = round($itemArr[orig_price] * ($pdata[yield_qr] / 100) * 1.27);
                        //$yield = $yield-$itemArr[discount_value];
                    } else {
                        $yield = round($itemArr[orig_price] * ($pdata[yield_vtl] / 100) * 1.27);
                    }
                    
                    //$yield = $yield-$itemArr[discount_value];
                    
                    if ($yield == 0) {
                        
                        echo "ERROR INDUSZ yield!<hr/>";
                    } elseif ($yield < 0) {
                        
                        echo "ERROR INDUSZ minus yield!<hr/>";
                    } else {
                        //echo " $itemArr[orig_price] - $yield - $itemArr[discount_value]";
                        
                        //$dc = $itemArr[orig_price]-$itemArr[discount_value];
                        $tetelek->tetel[] = array(
                            //	'cikkszam' => 'pg2344/12c',
                            'megnev' => "Belföldi utazási csomag: $itemArr[offer_id]" . $extratext,
                            'besorolas_tip' => 'SZJ',
                            'besorolas' => 79.12,
                            'mennyisegi_egy' => 'db',
                            'mennyiseg' => 1,
                            'afa_kulcs' => 'AHK',
                            'afa_nev' => "0%",
                            'brutto_egysegar' => $itemArr[orig_price] - $yield
                            //'kedvezmeny' => $itemArr[discount_percent],
                            //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                        );
                        
                        
                        $tetelek->tetel[] = array(
                            //	'cikkszam' => 'pg2344/12c',
                            'megnev' => "Belföldi utazási csomag: $itemArr[offer_id]",
                            'besorolas_tip' => 'SZJ',
                            'besorolas' => 79.12,
                            'mennyisegi_egy' => 'db',
                            'mennyiseg' => 1,
                            'afa_kulcs' => 27,
                            'afa_nev' => "27%",
                            'brutto_egysegar' => $yield - $itemArr[discount_value]
                            //'kedvezmeny' => $itemArr[discount_percent],
                            //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                        );
                        
                    }
                    
                    
                } elseif ($itemArr[payment] == 5 || $partnerArr[invoice_name] <> '' || $itemArr[payment] == 10 || $itemArr[payment] == 11 || $itemArr[payment] == 12) {
                    
                    echo "ELLENORIZD !!!! <hr/>";
                    $tetelek->tetel[] = array(
                        //	'cikkszam' => 'pg2344/12c',
                        'megnev' => $title . $extratext,
                        'besorolas_tip' => 'SZJ',
                        'besorolas' => $szj,
                        'mennyisegi_egy' => 'db',
                        'mennyiseg' => 1,
                        'afa_kulcs' => $tax,
                        'afa_nev' => $tax_code,
                        'brutto_egysegar' => $itemArr[orig_price] - $itemArr[discount_value] - $twentyseven
                        //'kedvezmeny' => $itemArr[discount_percent],
                        //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                    );
                    
                    if ($twentyseven > 0) {
                        echo "ELLENORIZD DUPLAN!!!! <hr/>";
                        $tetelek->tetel[] = array(
                            //	'cikkszam' => 'pg2344/12c',
                            'megnev' => "Kiegészítő felárak az alábbi sorszám alapján: $itemArr[offer_id]",
                            'mennyisegi_egy' => 'db',
                            'mennyiseg' => 1,
                            'afa_kulcs' => 27,
                            'afa_nev' => "27%",
                            'brutto_egysegar' => $twentyseven
                            //'kedvezmeny' => $itemArr[discount_percent],
                            //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                        );
                    }
                } else {
                    $dval = $itemArr[orig_price] - $itemArr[discount_value];
                    
                    if ($dval == 0) {
                        $tetelek->tetel[] = array(
                            //	'cikkszam' => 'pg2344/12c',
                            'megnev' => $title,
                            'besorolas_tip' => 'SZJ',
                            'besorolas' => $szj,
                            'mennyisegi_egy' => 'db',
                            'mennyiseg' => 1,
                            'afa_kulcs' => $tax,
                            'afa_nev' => $tax_code,
                            'brutto_egysegar' => $itemArr[orig_price],
                            'kedvezmeny' => 0
                            //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                        );
                        $tetelek->tetel[] = array(
                            //	'cikkszam' => 'pg2344/12c',
                            'megnev' => "Engedmény",
                            'besorolas_tip' => 'SZJ',
                            'besorolas' => $szj,
                            'mennyisegi_egy' => 'db',
                            'mennyiseg' => -1,
                            'afa_kulcs' => $tax,
                            'afa_nev' => $tax_code,
                            'brutto_egysegar' => $itemArr[orig_price],
                            'kedvezmeny' => 0
                            //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                        );
                    } else {
                        $tetelek->tetel[] = array(
                            //	'cikkszam' => 'pg2344/12c',
                            'megnev' => $title . $extratext,
                            'besorolas_tip' => 'SZJ',
                            'besorolas' => $szj,
                            'mennyisegi_egy' => 'db',
                            'mennyiseg' => 1,
                            'afa_kulcs' => $tax,
                            'afa_nev' => $tax_code,
                            'brutto_egysegar' => $itemArr[orig_price] - $itemArr[discount_value]
                            //'kedvezmeny' => $itemArr[discount_percent],
                            //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                        );
                    }
                    
                }
                
                
                if ($itemArr[post_fee] > 0) {
                    echo "Postakoltseg!!!!<hr/>";
                    $postage          = 445;
                    $tetelek->tetel[] = array(
                        //	'cikkszam' => 'pg2344/12c',
                        'megnev' => "Postaköltség",
                        'mennyisegi_egy' => 'db',
                        'mennyiseg' => 1,
                        'afa_kulcs' => "AHK",
                        'afa_nev' => "0%",
                        'brutto_egysegar' => $postage
                        //'kedvezmeny' => $itemArr[discount_percent],
                        //			'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
                    );
                    
                }
                
                
                $subtotal = $subtotal + ($itemArr[orig_price] - $itemArr[discount_value]) + $postage;
            }
            
            // - Mit szeretne?
            $szla_tipus        = 'N'; // Normál számla készítése
            // 'DIJ-'.'TR-'
            $SzlaKeszitRequest = array(
                'params' => array(
                    'api_key' => API_KEY,
                    'biz_tip' => 'E' . $szla_tipus . 'S', // Normál / Normál / Számla
                    'gen_pdf' => 1, // 0 - ne készítsen, 1 - készítsen PDF-et
                    'prefix' => $ppfix . '' . $prefix . 'M', // -> fej->vevo->tomb !	DIJ-	V
                    'kerekitesi_mod' => 'vegosszesen',
                    'sajat_azon' => '3984753847539847937984987'
                ),
                'data' => array(
                    'szla_xml' => array(
                        'stilus' => array(
                            'megjelenes' => 'modern',
                            'tipus' => 'normal', // normal, ablakos, csekkes, egyszeru,
                            'nyelv' => 1, // 1 - magyar, 2 - magyar/angol, 3 - magyar/német
                            'van_kiallito' => '1',
                            'van_atvevo' => '1' //,
                        ),
                        'fej' => array(
                            'vevo' => array(
                                'nev' => $partnerArr['name'],
                                'adoszam' => "$partnerArr[tax_no]",
                                'cim' => array(
                                    'irsz' => $partnerArr['zip'],
                                    'varos' => $partnerArr['city'],
                                    'utca' => $partnerArr['address'],
                                    'orszag' => '1'
                                ),
                                'lev_cim' => array(
                                    'irsz' => $partnerArr['zip'],
                                    'varos' => $partnerArr['city'],
                                    'utca' => $partnerArr['address'],
                                    'orszag' => '1'
                                ),
                                'email' => "$partnerArr[email]",
                                //	'cejegyzekszam' => 'cégjegyzékszám',
                                //	'euadoszam' => 'HU12345678',
                                'megjegy' => ''
                            ),
                            'tomb' => $ppfix . '' . $prefix . 'M' . date("Y"), // DIJ-	V		DIJ-
                            'kelt' => $kelt,
                            'telj' => $telj,
                            'fiz_hat' => $fiz_hat,
                            'fiz_mod' => $method, // 1 - Készpénz, 2 - Átutalás, 3 - Bankkártya, 4 - Hitel, 5 - Utánvét, 6 - Paypal,
                            'info' => array(
                                'fejlec_info' => '',
                                'fejlec_info2' => '',
                                'lablec_info' => $lablec,
                                'lablec_info2' => ''
                            ),
                            'tipus' => $szla_tipus, // Normál / Sztornó / Másolat
                            'fizetve' => $kelt, //'2010-05-25',
                            'deviza' => $deviza //,		// 'EUR'
                            //	'netto_ossz' => round($subtotal/1.18,2),
                            //	'brutto_ossz' => round($subtotal,2)
                        ),
                        'tetelek' => $tetelek
                    )
                )
            );
            
            
            
            //debug($SzlaKeszitRequest); die();
            //prevent false invoice request & foreign invoice creation
            /*if(empty($SzlaKeszitRequest)) {
            ("Vááááááááááááááááá"); 
            }*/
            /*print '<pre>'; 
            print_r($SzlaKeszitRequest);
            print '<pre>';
            print '<pre>';
            print_r($company);
            print '<pre>';
            print '<pre>';
            print_r($prefix);
            print '<pre>';*/
            //();
            $result = '';
            if (!empty($partnerArr['name'])) {
                $result = $client->SzlaKeszit($SzlaKeszitRequest);
                //debug($result);
            }
            
            if ($result == 'NULL' || $result == NULL) {
                $result = null;
            }
            $hiba = false;
            
            /*if(empty($zip) || empty($city) || empty($address)) {
            $hiba = true;
            $error = $SzlaKeszitRequest;
            //var_dump($error); ();
            $result[] = "Cím hiba!";
            }*/
            
            //echo "<pre>";
            //debug($result);
            //die();
            
            //	;
        }
        catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        /*} catch (SoapFault $fault) {
        //	echo "SOAP exception: ".$fault->getMessage()."<br/>";
        $hiba= $fault->getMessage();
        
        debug($hiba); ();
        }	// try*/
        
        if ($hiba || is_null($result) || is_soap_fault($result)) {
            if (isset($result)) {
                echo "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})<br/>";
            } // if (isset($result)) 
            echo "<tr class='red'><td colspan='30'>Hiba: " . var_dump($result) . "</td></tr>";
            //(); 
            // OK.
        } else {
            //	echo "Státusz:              <b>".$result->status.' </b><br>';
            if (is_object($result->messages)) {
                foreach ($result->messages as $messages) {
                    
                    foreach ($messages as $message) {
                        //var_dump($message->uzenet);
                        if (isset($message->uzenet)) {
                            //				echo "             (".$message->kod.") <b>".$message->uzenet.'</b>'.
                            //					' (mező: '.$message->mezo.', adat: '.$message->adat.')<br>';
                        }
                        
                    }
                } // foreach ($result['messages'] as $uzenet)
                
            } // if (is_array())
            if ($result->status == 'Ok' || isset($result->szla_resp)) {
                
                writelog($result->szla_resp->sorszam . " invoice created for item $itemArr[offer_id]");
                
                if (isset($result->szla_resp->pdf)) {
                    
                    if ($month < 10)
                        $zeroMonth = "0" . $month;
                    else
                        $zeroMonth = $month;
                    
                    
                    $filename = "../invoices/vatera/" . str_replace("/", "_", $result->szla_resp->sorszam) . ".pdf";
                    $fh = fopen($filename, 'w') or print("can't open file");
                    fwrite($fh, base64_decode($result->szla_resp->pdf));
                    fclose($fh);
                    $mysql->query("UPDATE customers SET user_invoice_number = '" . $result->szla_resp->sorszam . "', user_invoice_date = NOW() WHERE paid = 1 AND customers.cid = $customer");
                    
                    
                    if (substr($itemArr[offer_id], 0, 3) == 'CZO') {
                        include("../inc/languages/ro.lang.php");
                    } else if (substr($itemArr[offer_id], 0, 3) == 'TVO') {
                        include("../inc/languages/sk.lang.php");
                    } else {
                        include("../inc/languages/hu.lang.php");
                    }
                    //email
                    $random_hash = md5(date('r', time()));
                    //set email headers
                    $headers     = "Return-path: <$lang[adminemail]>" . "\r\n";
                    $headers .= "Reply-to: <$lang[adminemail]>" . "\r\n";
                    $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"\r\n";
                    // $headers .= "From: vtl@indulhatunk.hu <vtl@indulhatunk.hu>"."\r\n";
                    
                    $from = '=?utf-8?B?' . base64_encode($lang[adminname]) . '?=';
                    
                    $headers .= "From: $from <$lang[adminemail]>\r\n";
                    
                    
                    
                    $headers .= "X-Priority: 3\r\n";
                    $headers .= "X-Mailer: INDULHATUNK-MAILER\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "\r\n\r\n";
                    
                    $attachment = chunk_split(base64_encode(file_get_contents("../invoices/vatera/" . str_replace("/", "_", $result->szla_resp->sorszam) . ".pdf")));
                    $prebody    = "--PHP-mixed-{$random_hash}\r\n";
                    $prebody .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
                    $prebody .= "Content-Transfer-Encoding: 7bit\r\n";
                    $prebody .= "\r\n";
                    // $prebody .="Indulhatunk.hu számla";
                    
                    
                    $sb   = $lang['invoice_letter_subject'] . "" . $result->szla_resp->sorszam;
                    $hash = md5($partnerArr[email] . $sb . time());
                    
                    
                    $cnt = $lang['invoice_letter_content'];
                    
                    $content = str_replace("###BODY###", $cnt, $outletmailNew);
                    $content = str_replace("###SUBJECT###", $sb, $content);
                    $content = str_replace("###NAME###", $partnerArr[name], $content);
                    
                    $content = str_replace("###DEAR###", $lang[dear], $content);
                    $content = str_replace("###THANKS###", $lang[thanks], $content);
                    $content = str_replace("###SIGNATURE###", $lang[signature], $content);
                    $content = str_replace("###SZALLASURL###", $lang[szallasurl], $content);
                    
                    $content = str_replace("###TOPIMAGE###", $lang[countrycodelong], $content);
                    $content = str_replace("###CONTACTUS###", $lang["#CONTACTUS#"], $content);
                    $content = str_replace("###LETTERAPP###", $lang["#DOWNLOADAPP#"], $content);
                    $content = str_replace("###LETTERFOOTER###", $lang[letterfooter], $content);
                    
                    $prebody .= $content;
                    
                    
                    
                    $prebody .= "";
                    $prebody .= "\r\n";
                    $prebody .= "--PHP-mixed-{$random_hash}\r\n";
                    $prebody .= "Content-Type: application/pdf; name=\"" . $result->szla_resp->sorszam . ".pdf\"\r\n";
                    $prebody .= "Content-Transfer-Encoding: base64\r\n";
                    $prebody .= "Content-Disposition: attachment; filename=\"" . $result->szla_resp->sorszam . ".pdf\"\r\n\r\n";
                    $prebody .= "{$attachment}\r\n\r\n";
                    $prebody .= "--PHP-mixed-{$random_hash}--\r\n";
                    
                    
                    $subject = '=?utf-8?B?' . base64_encode($lang['invoice_letter_subject'] . "" . $result->szla_resp->sorszam) . '?=';
                    
                    $paid_date = explode(" ", $itemArr[paid_date]);
                    $paid_date = str_replace("-", '', $paid_date);
                    $limit     = date("Ymd") - 3;
                    if ($paid_date > $limit)
                        @mail($partnerArr[email], $subject, $prebody, $headers);
                    
                    
                    $domain = explode("@", $partnerArr[email]);
                    $domain = $domain[1];
                    
                    
                    
                    /*	$emailinsert = array();
                    
                    $emailinsert[from_email] = "vtl@indulhatunk.hu";
                    $emailinsert[from_name] = "Szállás Outlet";
                    $emailinsert[email] = $partnerArr[email];
                    $emailinsert[subject] = $sb;
                    $emailinsert[body] = $content;
                    $emailinsert[hash] = $hash;
                    $emailinsert[added] = 'NOW()';
                    $emailinsert[domain] = $domain;
                    
                    //$mysql->query("SET names 'utf8'");
                    $mysql->query_insert("emails_sent",$emailinsert);
                    
                    */
                    //email end
                    
                    //invoice request log
                    $invoice_log                 = array();
                    $invoice_log[added]          = 'NOW()';
                    $invoice_log[invoice_number] = $result->szla_resp->sorszam;
                    $invoice_log[request]        = serialize($SzlaKeszitRequest);
                    $invoice_log[type]           = 'user';
                    $invoice_log[telj]           = $telj;
                    $invoice_log[company]        = $company;
                    $mysql->query_insert("log_invoice", $invoice_log);
                    
                    if ($result->szla_resp->brutto_ossz == 0) {
                        //alert mail
                        sendEmail("Nulla értékű számla", "Nulla értékű számla: " . $result->szla_resp->sorszam, "it@indulhatunk.hu", "");
                    }
                    // print_r($result->szla_resp);
                    
                    writelog($result->szla_resp->sorszam . " invoice saved  for item $itemArr[offer_id]");
                } else {
                }
                
                
            } // if ($result->status=='Ok')
        } // if (is_soap_fault($result)) 
    } //end of partner query
    
    //echo "<a href=\"../customers.php\">Vissza a vásárlók oldalra</a>";
}
// ==============================================================================================================
?>