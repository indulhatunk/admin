<?php
/*
header('Content-type: text/html; charset=utf-8');
include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();


$sure = $_GET[sure];

if($sure <> 1) {
	head();
	echo message("<b>Biztosan ki szeretne nyomtatni a heti HotelOutlet számlat? <a href=\"?sure=1\">Igen!</a> | <a href=\"../customers.php\">Nem!</a></b>");
	foot();
	die;
}


if($CURUSER["userclass"] <> 255) {
	header("location: customers.php");
}
*/



function create_invoice($partner = '', $items = '',$needtopay = 0,$company = 'hoteloutlet')
{

	//die('itt');
	global $mysql;
	
	
	if($company == 'szallasoutlet') { $prefix= 'JT-'; }
	//weekly special generate invoice
	elseif($company == 'indusz') { $prefix= 'JT-'; }
	//weekly special generate invoice
	elseif($company == 'professional') { $prefix= 'PR-'; }
	else { $prefix= 'HO-'; }
	//echo ('Futtatás időpontja: <b>'.date('Y.m.d. H:i:s').'</b><br>');
	
	$kelt		= 	date('Y-m-d');
	
	if($partner[fiz_mod] == 2) //atutalas eseten +11 nap
		$fiz_hat = date('Y-m-d', mktime( 0, 0, 0, /* hó */ substr($kelt, 5, 2), /* nap */ substr($kelt, 8, 2), /* év */ substr($kelt, 0, 4)) +11*24*60*60);
	else
		$fiz_hat	= 	date('Y-m-d');
	$telj		= 	date('Y-m-d');
	
    require_once('soap.inc.php');
    //print 'itt';
    //print_r($company);
    $client = createSOAPClient(trim($company));// die();
		
	if(class_exists('TSzlaTetelek') != true)
	{
	
		class TSzlaTetelek {
			public $tetel;
		}
	}
	
	try {
	
	
	foreach($items as $item)
	{
	$tetelek->tetel[] =array(
		//	'cikkszam' => 'pg2344/12c',
			'megnev' => $item[megnev],
		//	'besorolas_tip' => 'VTSZ',
		//	'besorolas' => '64.20.12',
			'mennyisegi_egy' => 'db',
			'mennyiseg' => 1,
			'afa_kulcs' => 27,
			'afa_nev' => '27%',
			'netto_egysegar' => $item[netto_egysegar],
			'kedvezmeny' => 0,
		//	'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
		);
		$subtotal = $subtotal + $item[netto_egysegar];
		$itemscount++;

	}
	
	if($needtopay == 1)
		$cmt = "A SZÁMLA PÉNZÜGYI TELJESÍTÉST IGÉNYEL!";
	else
		$cmt = "A SZÁMLA PÉNZÜGYI TELJESÍTÉST NEM IGÉNYEL!";
	
	$szla_tipus= 'N';	// Normál számla készítése

	$SzlaKeszitRequest = array(
		'params' => array(
			'api_key' => API_KEY,
			'biz_tip' => 'E'.$szla_tipus.'S',	// Normál / Normál / Számla
			'gen_pdf' => 1,			// 0 - ne készítsen, 1 - készítsen PDF-et
			'prefix' => $prefix.'M',		// -> fej->vevo->tomb !	DIJ-	V
			'kerekitesi_mod' => 'vegosszesen',
			'sajat_azon' => '3984753847539847937984987'
		),
		'data' => array(
			'szla_xml' => array(
				'stilus' => array(
					'megjelenes' => 'modern',
					'tipus' => 'normal',
					'nyelv' => 1,
					'van_kiallito' => '1',
					'van_atvevo' => '1'//,
				),
				'fej' => array(
					'vevo' => array(
						'nev' => $partner[nev],
						'adoszam' => $partner[adoszam],
						'cim' => array( 'irsz' => $partner[irsz], 'varos'=> $partner[varos], 'utca'=> $partner[utca], 'orszag'=>'1'),
						'lev_cim' => array( 'irsz' => $partner[irsz], 'varos'=> $partner[varos], 'utca'=> $partner[utca], 'orszag'=>'1'),
						'email' => $partnerA[email],
						'megjegy' => $partner[megjegy]
					),
					'tomb' => $prefix.'M' . date("Y"),
					'kelt' => $kelt,
					'telj' => $telj,
					'fiz_hat' => $fiz_hat,
					'fiz_mod' => '2',		// 1 - Készpénz, 2 - Átutalás, 3 - Bankkártya, 4 - Hitel, 5 - Utánvét, 6 - Paypal, 11 - utalvany
					'info' => array(
						'fejlec_info' => '',
						'fejlec_info2' => '',
						'lablec_info' => "$cmt - info@indulhatunk.info",
						'lablec_info2' => ''
					),	
					'tipus' => $szla_tipus,
					'fizetve' => '', 
					'deviza' => $deviza,
				),
				'tetelek' =>  $tetelek
			)
		)
	);

	echo "Számla adatok:<br/>";
	echo "$partner[nev]<br/>";
	echo formatPrice($subtotal*1.27,2)." / $itemscount db<hr/>";
	/*echo "<pre>";
	print_r( $SzlaKeszitRequest ); 
	echo "</pre>";*/

	//die();
	
    $result = $client->SzlaKeszit($SzlaKeszitRequest);
    /*echo "<pre>";
     print_r($result);
     echo "</pre>"; die();/**/
	//return 99999;
	$hiba= false;
	
} catch (SoapFault $fault) {
	echo "SOAP exception: ".$fault->getMessage()."<br/>";
	$hiba= $fault->getMessage();
	
}	
// Hibás?
if ($hiba || is_null($result) || is_soap_fault($result)) {
	if (isset($result)) {
		echo "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})<br/>";
	}	// if (isset($result))
	echo "Hiba: ".$hiba."<br/>";
// OK.
} else {
	echo "Státusz:              <b>".$result->status.' </b><br>';
	if (is_object($result->messages)) {
		foreach ($result->messages as $messages) {
			
			foreach ($messages as $message) {
				//var_dump($message->uzenet);
				if (isset($message->uzenet)) {
					echo "             (".$message->kod.") <b>".$message->uzenet.'</b>'.
						' (mező: '.$message->mezo.', adat: '.$message->adat.')<br>';
				}
				
			}
		}	// foreach ($result['messages'] as $uzenet)
		
	}	// if (is_array())
	if ($result->status=='Ok' || isset($result->szla_resp)) {
	///	echo "Számla:<br>";
		echo "- sorszám:                     <b>".$result->szla_resp->sorszam.'</b><br>';
		echo "- nettó (Ft) összesen:              <b>".$result->szla_resp->netto_ossz.' Ft</b><br>';
		echo "- bruttó (Ft) összesen:             <b>".$result->szla_resp->brutto_ossz.' Ft</b><br>';
		
		writelog($result->szla_resp->sorszam." invoice created");
	
		if (isset($result->szla_resp->pdf)) {
			$filename = "invoices/vatera/".str_replace("/","_",$result->szla_resp->sorszam).".pdf";
			$fh = fopen($filename, 'w') or print("can't open file");
			fwrite($fh, base64_decode($result->szla_resp->pdf));
			fclose($fh);
		
			if($company == 'indusz')
				$cmp = 'szallasoutlet';
			else
				$cmp = $company;
				
			//invoice request log
		 	$invoice_log = array();
		 	$invoice_log[added] = 'NOW()';
		 	$invoice_log[invoice_number] = $result->szla_resp->sorszam;
		 	$invoice_log[request] = serialize($SzlaKeszitRequest);
		 	$invoice_log[type] = 'partner_vtl';
		 	$invoice_log[company] = $cmp;
		 	$invoice_log[telj] = $telj;
		 	$mysql->query_insert("log_invoice",$invoice_log);
		 
		 	if($result->szla_resp->brutto_ossz == 0)
		 	{
		 		//alert mail
		 		sendEmail("Nulla értékű számla","Nulla értékű számla: ".$result->szla_resp->sorszam,"it@indulhatunk.hu","");
		 	}
		 	
		 	
			writelog($result->szla_resp->sorszam." invoice saved");
			
			return $result->szla_resp->sorszam;
		} 	
					
	}

}
}
?>