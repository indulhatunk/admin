<?
/*
 * index.php
 *
 * the main login page
 *
 */

/* bootstrap file */
include ("../inc/init.inc.php");
userlogin();
error_reporting(E_ALL);

if ($CURUSER[userclass] < 50 && $CURUSER[username] != 'mindenkupon.hu')
    header("location: index.php");

head();

$storno = $_GET[storno];
$sure = $_GET[sure];
if ($storno != '' && $sure != 1) {
    
    if ($CURUSER[userclass] < 50 && $_GET[cid] < 0)
        die("Érvénytelen!");
    
    if ($_GET[recreate] == 1) {
        $comment = 'újra ki szeretné állítani a';
    } else {
        $comment = 'sztornózni szeretné a';
    }
    echo "<h2>Biztosan $comment <b><font color='red'>$storno</font></b> sorszámú számlát?</h1><br/>  <a href=\"?storno=$storno&sure=1&cid=$_GET[cid]&recreate=$_GET[recreate]\" class='button green'>$lang[yes]</a> <a href=\"list_invoice.php\" class='button red'>$lang[no]</a><div class='cleaner'></div>";
    foot();
    die();
}

$data = mysql_fetch_assoc($mysql->query("SELECT * FROM log_invoice WHERE invoice_number = '$storno'"));

if (class_exists('TSzlaTetelek') != true) {

    class TSzlaTetelek
    {

        public $tetel;
    }
}

// $data[company] = "professional";
 //echo "$data[company]<hr/>"; die();
$tetelek = new TSzlaTetelek();

require_once ('soap.inc.php');
$client = createSOAPClient(trim($data[company]));

$kelt = date("Y-m-d"); // 4+1+2+1+2
$fiz_hat = date('Y-m-d', mktime(0, 0, 0, /* hó */ substr($kelt, 5, 2), /* nap */ substr($kelt, 8, 2), /* év */ substr($kelt, 0, 4)) + 11 * 24 * 60 * 60);

// SOAP call:
try {
    
    $request = unserialize($data[request]);
    
    echo "<pre>";
    
    $request[params][biz_tip] = 'ESS';
    $request[data][szla_xml][fej][hivatkozas] = $storno;
    $request[data][szla_xml][fej][kelt] = $kelt;
    $request[data][szla_xml][fej][tomb] = str_replace("2016", "2017", str_replace("2015", "2016", str_replace("2014", "2015", str_replace("2013", "2014", $request[data][szla_xml][fej][tomb]))));
    $request[data][szla_xml][fej][telj] = $kelt;
    $request[data][szla_xml][fej][fiz_hat] = $fiz_hat;
    $request[data][szla_xml][fej][tipus] = "S";
    unset($request[data][szla_xml][fej][brutto_ossz]);
    
    // print_r($request);
    
    // die;
    
    // print_r($request);
    
    // $request[data][szla_xml][fej][fiz_hat] = $kelt;
    
    // echo "<pre>";
    // print_r($request);
    print_r($request);
    
    $result = $client->SzlaKeszit($request);
    
    $hiba = false;
    
    print_r($result);
    // echo "<pre>";
    die();
    ;
} catch (SoapFault $fault) {
    echo "SOAP exception: " . $fault->getMessage() . "<br/>";
    $hiba = $fault->getMessage();
} // try
  
// var_dump($result);

if ($hiba || is_null($result) || is_soap_fault($result)) {
    if (isset($result)) {
        echo "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})<br/>";
    } // if (isset($result))
    echo "Hiba: " . $hiba . "<br/>";
    // OK.
} else {
    // echo "Státusz: <b>".$result->status.' </b><br>';
    
    // print_r($result);
    if (is_object($result->messages)) {
        foreach ($result->messages as $messages) {
            
            foreach ($messages as $message) {
                // var_dump($message->uzenet);
                if (isset($message->uzenet)) {
                    // echo " (".$message->kod.") <b>".$message->uzenet.'</b>'.
                    // ' (mező: '.$message->mezo.', adat: '.$message->adat.')<br>';
                }
            }
        } // foreach ($result['messages'] as $uzenet)
    } // if (is_array())
    if ($result->status == 'Ok' || isset($result->szla_resp)) {
        
        echo message("A számla elkészült!");
        writelog($result->szla_resp->sorszam . " invoice created for item $itemArr[offer_id]");
        
        if (isset($result->szla_resp->pdf)) {
            
            if ($month < 10)
                $zeroMonth = "0" . $month;
            else
                $zeroMonth = $month;
            $filename = "/var/www/lighttpd/admin.indulhatunk.info/invoices/vatera/" . str_replace("/", "_", $result->szla_resp->sorszam) . ".pdf";
            $fh = fopen($filename, 'w') or print("can't open file");
            fwrite($fh, base64_decode($result->szla_resp->pdf));
            fclose($fh);
            
            // invoice request log
            $invoice_log = array();
            $invoice_log[added] = 'NOW()';
            $invoice_log[invoice_number] = $result->szla_resp->sorszam;
            $invoice_log[request] = serialize($request);
            $invoice_log[type] = 'partner_vtl_storno';
            $invoice_log[company] = $data[company];
            $invoice_log[telj] = $kelt;
            $mysql->query_insert("log_invoice", $invoice_log);
            
            writelog($result->szla_resp->sorszam . " storno invoice saved  for invoice $storno");
            
            if ($_GET[cid] > 0) {
                $customer = mysql_fetch_assoc($mysql->query("SELECT cid,offer_id FROM customers WHERE cid = $_GET[cid]"));
                
                if ($customer[offer_id] != '') {
                    writelog("$CURUSER[username] created storno invoice for ($customer[offer_id]) /" . $result->szla_resp->sorszam);
                    
                    $dt[user_storno_invoice_number] = $result->szla_resp->sorszam;
                    $dt[user_storno_invoice_date] = date("Y-m-d");
                    
                    if ($_GET[recreate] == 1) {
                        $dt[user_invoice_number] = '';
                        $dt[user_invoice_date] = '';
                        $dt[user_invoice_started] = '';
                    }
                    
                    $mysql->query_update("customers", $dt, "cid=$customer[cid]");
                }
            }
        } else {}
    } // if ($result->status=='Ok')
} // if (is_soap_fault($result))

?>