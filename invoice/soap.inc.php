<?php
error_reporting(E_ALL);
use_soap_error_handler(true);
ini_set("soap.wsdl_cache_enabled", "0");

function createSOAPClient($comapny = 'professional')
{
    define('WSDL_FILE', 'https://szamlazz.konnyen.hu/motor/szmotor.wsdl');
    $motorconf = array(
        'szallasoutlet' => array(
            'user' => '2126-motor',
            'passwd' => 'motorpass',
            'API_KEY' => 'MF3K0-dA'
        ),
        'indusz' => array(
            'user' => '2297-motor',
            'passwd' => 'motorpass',
            'API_KEY' => 'Z36R1bK2'
        ),
        'professional' => array(
            'user' => '2695-motor',
            'passwd' => 'motorpass',
            'API_KEY' => 'SwWDfA1m'
        ),
        'indulhatunk' => array(
            'user' => '493-motor',
            'passwd' => 'motorpass',
            'API_KEY' => '_OjXkza0'
        ),
        'mkmedia' => array(
            'user' => '2639-motor',
            'passwd' => 'motorpass',
            'API_KEY' => '+8NnjKvq'
        ),
        'horizonttravel' => array(
            'user' => '2629-motor',
            'passwd' => 'motorpass',
            'API_KEY' => '4JMQnP79'
        ),
        'hoteloutlet' => array(
            'user' => '1449-motor',
            'passwd' => 'motorpass',
            'API_KEY' => 'D9-2ydLs'
        )
    );
    
    if (! array_key_exists($comapny,$motorconf)) {
        $comapny = 'professional';
    }
    
    define('API_KEY', $motorconf[$comapny]['API_KEY']);
    
    $context = stream_context_create([
        'ssl' => [
            // set some SSL/TLS specific options
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        ]
    ]);
    
    $client = new SoapClient(WSDL_FILE, array(
        'login' => $motorconf[$comapny]['user'],
        'password' => $motorconf[$comapny]['passwd'],
        'trace' => 1,
        'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
        'exceptions' => true,
        'stream_context' => $context
    ));
    
    //print_r($comapny);
    
    return $client;
}
?>