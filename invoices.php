<?
/*
 * invoices.php 
 *
 * the invoice list page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");


userlogin();

if((int)$_GET[year] > 0)
	$extrayear = "AND year(invoice_date) = $_GET[year]";
else
	$extrayear = '';
	
if($CURUSER[userclass] <> 255) 
{
	$pid = " pid = '$CURUSER[pid]' $extrayear";
}
else
{
	$pid = ' cid > 0';
}

head("Számlák, elszámolások");

?>

<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href="invoices.php" class="<? if($_GET[year] == '') echo "current"?>">Minden tétel</a></li>
						
						<? for($i = date("Y"); $i > 2010; $i--)
						{
							if($_GET[year] == $i)
								$class = 'current';
							else
								$class = '';
							echo "<li><a href='invoices.php?year=$i' class='$class'>$i. év</a></li>";
						}
						?>
						
					</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<style>
	.hiddentable { display:none;}
</style>
<script>
$(document).ready(function() {

	$('.changecompany').click(function(){	
		
		var company;
		company = $(this).attr("company");
				
		$("."+company).show();
		
		$(".buttons").hide();
		return false;
	});

});

</script>
<div class='buttons'>
	<a href='#' class='changecompany' company='szallasoutlet' style='display:block; width:28%; padding:20px ; text-align:center; background-color:#E56E94;float:left;color:white;' >SzállásOutlet Kft. számlái</a>
	<?php /* <a href='#' class='changecompany' company='hoteloutlet' style='display:block; width:28%; padding:20px ;text-align:center; background-color:#C3FDB8;float:left;color:black;'>Hotel Outlet Kft. számlái</a> */ ?>
	<a href='#' class='changecompany' company='professional' style='display:block; width:28%; padding:20px ;text-align:center; background-color:#C3FDB8;float:left;color:black;'>Professional Outlet Kft. számlái</a>

	<? if($CURUSER[without_vat] == 1) { ?>
	<a href='#' class='changecompany' company='indusz' style='display:block; width:28%; padding:20px ;text-align:center; background-color:#9E7BFF;float:left;color:black;'>Indulhatunk U. Kft. számlái</a>
	<? } ?>
	<div class='cleaner'></div>
</div>		
<?
mysql_query("SET NAMES 'utf8'"); 

	$companies = array('szallasoutlet' => "SzállásOutlet Kft. által kiállított számlák",'hoteloutlet' => "Hotel Outlet Kft. által kiállított számlák",'indulhatunk' => "Indulhatunk.hu Kft által kiállított számlák", 'indusz' => "Indulhatunk utazásszervező Kft. által kiállított számlák",'indulhatunk' => "Indulhatunk.hu Kft által kiállított számlák", 'professional' => "Professional Outlet Kft által kiállított számlák");
//	else
//	$companies = array('hoteloutlet' => "Hotel Outlet Kft. által kiállított számlák",'indulhatunk' => "Indulhatunk.hu Kft által kiállított számlák");

$type = $_GET[type];

if($_GET[missing] == 1)
{
	echo message("Ha számláját nem találja, kérjük vegye fel a kapcsolatot velünk a tamas.forro@indulhatunk.hu e-mail címen");
}

//vatera 
if($type == "vtl" || $type == '')
{

	
$total_income = 0;
$total_yield = 0;
$total_must_pay = 0;


$c[szallasoutlet] = "<b>SzállásOutlet Kft.</b><br/><br/>
Cím: 1056 Budapest, Váci utca 9.<br/>
Adószám: 23686355-2-41<br/>
Bank: Erste Bank 11600006-00000000-51899097";


$c[hoteloutlet] = "<b>Hotel Outlet Kft.</b><br/><br/>
Cím: 1056 Budapest, Váci utca 9.<br/>
Adószám: 23417555-2-41<br/>
Bank: Erste Bank 11600006-00000000-49170300";

$c[indusz] = "<b>Indulhatunk Utazásszervező Kft.</b><br/><br/>
Cím: 1056 Budapest, Váci utca 9.<br/>
Adószám: 24856850-2-41<br/>
Bank: Erste Bank 11600006-00000000-65875957";
foreach($companies as $company => $cname)
{

	$statQuery = $mysql->query("SELECT * FROM customers
	 WHERE invoice_created = 1 AND company_invoice = '$company' AND $pid AND invoice_number <> '' GROUP BY invoice_number ORDER BY invoice_date DESC");


	 if($CURUSER[without_vat] == 0 && $company == 'indusz')
	 {
	 	$cmpname = 'szallasoutlet';
	 	$display = 0;
	 }
	 else
	 {
	 	$cmpname = $company;
	 	$display = 1;
	 }

echo "<div class='hiddentable $cmpname'>";


if($display == 1)
{
		echo message("<div style='color:red;margin:0 0 10px 0;text-align:left;font-size:13px'>FIGYELEM: A számlát kérjük az alábbi cégnévre állítsák ki!</div>
		<div style='text-align:left;'>$c[$company]</div>");
		
		echo message("FIGYELEM: Amennyiben nem találja az aktuális elszámolást kérjük görgessen a lista legaljára!");
		echo "<h3>$cname</h3>";
}
echo "<table>";

if($display == 1)
{

echo "<tr class='header'>";
	echo "<td>Cég</td>";
	echo "<td>Számlaszám</td>";
	echo "<td width='65'>Dátum</td>";
	echo "<td width='65'>Hét</td>";
	if($CURUSER[userclass] > 10)
		echo "<td>Partner neve</td>";
	echo "<td>Bevétel</td>";
	echo "<td>Jutalék</td>";
	echo "<td>Számla</td>";
	echo "<td>Elszámolás</td>";
	echo "<td>Állapot</td>";
	echo "<td>Szerződés</td>";
	echo "<td>Utalható</td>";
	echo "<td width='65'>Utalva</td>";

echo "</tr>";

}
//echo $dateArr[invoice_date];

	 
	 $z = 1;
	while($arr = mysql_fetch_assoc($statQuery)) {
		
		$partnerQuery = $mysql->query("SELECT * FROM partners WHERE pid = '$arr[pid]'");
		$partnerArr = mysql_fetch_assoc($partnerQuery);
		
		$sumQuery = $mysql->query("SELECT sum(orig_price) AS sumPrice FROM customers WHERE invoice_number = '$arr[invoice_number]' AND payment <> 5 AND payment <> 6 AND customers.inactive = 0");
		$sumArr = mysql_fetch_assoc($sumQuery);
	
		$sumTotal = $mysql->query("SELECT sum(orig_price) AS sumPrice FROM customers WHERE invoice_number = '$arr[invoice_number]' AND  customers.inactive = 0");
		$sumTotalArr = mysql_fetch_assoc($sumTotal);
		
		
		
		$realIncome = number_format($sumTotalArr[sumPrice], 0, ',', ' '). " Ft";
		if(str_replace("-","",$arr[invoice_date]) > 20110401)
	 	{
	 		if(str_replace("-","",$arr[invoice_date]) > 20120101)
	 		{
	 			$royalty = number_format($sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*1.27, 0, ',', ' '). " Ft";
				$income = $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*1.27;
	 		}
	 		else
	 		{
	 			$royalty = number_format($sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*1.25, 0, ',', ' '). " Ft";
				$income = $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*1.25;
			}
		}
		else
		{
			$royalty = number_format($sumTotalArr[sumPrice]*($partnerArr['yield']/100)*1.25, 0, ',', ' '). " Ft";
			$income = $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr['yield']/100)*1.25;

		}
		$income = number_format($income, 0, ',', ' '). " Ft";
		
		
	$total_income = $total_income + $sumTotalArr[sumPrice];
	$total_yield = $total_yield + $sumTotalArr[sumPrice]*($partnerArr['yield']/100)*1.25;
	$total_must_pay = $total_must_pay + $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr['yield']/100)*1.25;
	
	
	
	if($z%2 == 0)
		$class = 'grey';
	else
		$class = '';
		
	if($arr[invoice_date] > '2011-04-05')
		{
		
			$idate = str_replace("-",'',$arr[invoice_date]);
		
			if($arr[company_invoice] == 'szallasoutlet' || $idate > '20131013')
				$letter = "<b><a href='info/show_accounting_szallasoutlet.php?invoice_number=$arr[invoice_number]&pid=$arr[pid]' rel='facebox'>Elszámolás &raquo;</a></b>";
			else
				$letter = "<b><a href='info/show_accounting.php?invoice_number=$arr[invoice_number]&pid=$arr[pid]' rel='facebox'>Elszámolás &raquo;</a></b>";

			if($arr[invoice_cleared] <> '0000-00-00 00:00:00')
				$invoice = '<img src="/images/check1.png" width="25"/>';
			else
			{
				$invoice = '<img src="/images/cross1.png" width="25" alt="Kérjük a számlát a lehető leghamarabb juttassa el hozzánk!" title="Kérjük a számlát a lehető leghamarabb juttassa el hozzánk!"/>';
				$class = 'red';
			}
		}
		else
		{
			$letter = '';
			$invoice = '';
		}
	
	echo "<tr class='$class'>";
	
		if($arr[company_invoice] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		elseif($arr[company_invoice] == 'szallasoutlet')
			$logo = 'szo_logo.png';

		else
			$logo = 'ologo_small.png';
			
		echo "<td align='center'><img src='/images/$logo'/></td>";


		echo "<td><b>$arr[invoice_number]</b></td>";
		
		
		
	
		echo "<td align='center'><b>$arr[invoice_date]</b></td>";
		
		
	

		
		$wk = date("W",strtotime($arr[invoice_date]))-1;
		
		if($wk == 0)
			$wk = 52;
		echo "<td align='center'>".$wk.". hét</td>";
		
		if($CURUSER[userclass] > 10)
			echo "<td>$partnerArr[company_name] <br/> $partnerArr[hotel_name]</td>";
			
		if($arr[company_invoice] == 'indusz' && $display == 1)
		{
			echo "<td align='right' colspan='3'></td>";
		}
		else
		{
		
		echo "<td align='right'>$realIncome</td>";
		echo "<td align='right'>$royalty</td>";
		echo "<td align='center'><b><a href=' https://admin.indulhatunk.hu/invoices/dl/".md5($arr[invoice_number]."#FWf#CEFA#SDF343sf#")."'>Letöltés &raquo;</a></b></td>";
		}
		
		echo "<td align='center'>$letter</td>";




	if($arr[invoice_arrived_status] == 1)
		$status = 'díjbekérő';
	elseif($arr[invoice_arrived_status] == 2)
		$status = 'bemutatott';
		elseif($arr[invoice_arrived_status] == 3)
		$status = 'előlegszámla';
	elseif($arr[invoice_arrived_status] == 4)
		$status = 'végszámla';
	else
		$status = 'elintézendő';				
							
		echo "<td align='center'>$status</td>";


		$checkcontract = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE company = '$company' and partner_id = $partnerArr[partner_id] and type = 'outlet' LIMIT 1"));
		
		if($checkcontract[id] == '')
			echo "<td class='red' width='20' align='center'><a href=''><img src='/images/cross1.png' width='25'/></a></td>";
		else
			echo "<td align='center'><img src='/images/check1.png' width='25'/></td>";
			
			

		echo "<td align='center'>$invoice</td>";

	$transfer = explode(" ",$arr[transfer_sent]);
		$transfer = $transfer[0];
		
		if($transfer <> '0000-00-00')
			echo "<td align='center'>$transfer</td>";
		else
			echo "<td width='77'></td>";
			
		//echo "<td>$income</td>"; // https://admin.indulhatunk.hu/invoices/dl/".md5($arr[invoice_number]."#FWf#CEFA#SDF343sf#")."
	echo "</tr>";
		//echo "<hr/>";
		
		$z++;
	}
echo "</table></div>";
//}

}
//vatera end
//lmb

}

/*
*/
if($type == "lmb")
{


foreach($companies as $company => $cname)
{


if($company == 'indulhatunk')
	$extraselect = "AND invoice_date < '2011-12-01 00:00:00'";
else
	$extraselect = "AND invoice_date >= '2011-12-01 00:00:00'";


echo "<h3>$cname</h3>";

echo "<table>";
echo "<tr class='header'>";
	echo "<td colspan='2'>Számlaszám</td>";
	echo "<td>Partner neve</td>";
	echo "<td>Bevétel</td>";
	echo "<td>Jutalék</td>";
	echo "<td>Számlák</td>";
	echo "<td>Kifizetve?</td>";
echo "</tr>";

if($CURUSER[userclass] == 255) 
	$pid = "pid > 0";
else
	$pid = "pid = '$CURUSER[pid]'";

	$qr = $mysql->query("select * from partners where $pid");
	

	while($partner = mysql_fetch_assoc($qr))
	{
	$pid = $partner[coredb_id];
	
	$statQuery = $mysql->query("SELECT * FROM lmb
	 WHERE invoice_created = 1 AND partner_id = $pid $extraselect  GROUP BY invoice_number DESC");
	 
	 $z = 1;
	while($arr = mysql_fetch_assoc($statQuery)) {
		
		
			$year = date("Y",strtotime($arr[invoice_date]));
			
			if($year >= 2012)
				$tax = 1.27;
			else
				$tax = 1.25;
							
		$partnerQuery = $mysql->query("SELECT * FROM partners WHERE coredb_id = $arr[partner_id]");
		$partnerArr = mysql_fetch_assoc($partnerQuery);
		
		$sumQuery = $mysql->query("SELECT sum(total_price) AS sumPrice FROM lmb WHERE invoice_number = '$arr[invoice_number]'");
		$sumArr = mysql_fetch_assoc($sumQuery);
	
		$sumTotal = $mysql->query("SELECT sum(total_price) AS sumPrice FROM lmb WHERE invoice_number = '$arr[invoice_number]'");
		$sumTotalArr = mysql_fetch_assoc($sumTotal);
		
		
		
		$realIncome = number_format($sumTotalArr[sumPrice], 0, ',', ' '). " Ft";
		$royalty = number_format($sumTotalArr[sumPrice]*($partnerArr['yield']/100)*$tax, 0, ',', ' '). " Ft";
		
		
		$income = $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr['yield']/100)*$tax;
		
		$income = number_format($income, 0, ',', ' '). " Ft";
		
		
	$total_income = $total_income + $sumTotalArr[sumPrice];
	$total_yield = $total_yield + $sumTotalArr[sumPrice]*($partnerArr['yield']/100)*$tax;
	$total_must_pay = $total_must_pay + $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr['yield']/100)*$tax;
	
	
	if($z%2 == 0)
		$class = 'grey';
	else
		$class = '';
		
		
		if($arr[invoice_date] > '2011-04-05')
		{
			if($arr[invoice_cleared] <> '0000-00-00 00:00:00')
				$invoice = '<img src="/images/check1.png" width="25"/>';
			else
			{
				$invoice = '<img src="/images/cross1.png" width="25" alt="!" title=""/>';
				$class = 'red';
			}
		}
		else
		{
			$letter = '';
			$invoice = '';
		}
		
		$invoice_date = explode(' ',$arr[invoice_date]);
		$invoice_date = str_replace("-",'',$invoice_date[0]);
			
		
	echo "<tr class='$class'>";
	
	
		if($invoice_date < '20111201')
			$logo = 'ilogo_small.png';
		else
			$logo = 'ologo_small.png';
			
		echo "<td><img src='/images/$logo'/></td>";

		echo "<td><a href=\"#\" class=\"bold\">$arr[invoice_number]</a></td>";
		echo "<td>$partnerArr[company_name] $partnerArr[hotel_name]</td>";
		echo "<td>$realIncome</td>";
		echo "<td>$royalty</td>";
		echo "<td><b><a href=' https://admin.indulhatunk.hu/invoices/dl/".md5($arr[invoice_number]."#FWf#CEFA#SDF343sf#")."'>Számla letöltése</a></b></td>";
		
		echo "<td align='center'>$invoice</td>";
				
			
		//echo "<td>$income</td>"; // https://admin.indulhatunk.hu/invoices/dl/".md5($arr[invoice_number]."#FWf#CEFA#SDF343sf#")."
	echo "</tr>";
		//echo "<hr/>";
		$z++;
	}
	}
	echo "</table>";
}
}
/**/

echo "</div></div>";
foot();

?>
</body>
