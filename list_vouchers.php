<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

head("Havi barter kimutatás");
?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
			<li><a href='/own_vouchers.php'>Saját eladás</a></li>
			<li><a href='#' class='current'>Kimutatás</a></li>
			<li><a href='/list_mkmedia.php'>Kimutatás MK Média</a></li>

			
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


				
<?
if($_GET[page] == 'full')
	$filter = "AND date >= '2010-01-01' ";
elseif($_GET[month] > 0 && $_GET[year] > 0)
{
	$month = $_GET[month]-1;
	$monthp = $_GET[month];
	
	$year = $_GET[year];
	if($month == 0)
	{
		$yearp = $year - 1;
		$month = 12;
	}
	else
		$yearp = $year;
	
	
	
	$filter = "AND date >= '$yearp-$month-10 00:00:01' AND date <= '$year-$monthp-10 00:00:00'";
}
else
{
	$month = date("m")-1;
	$monthp = $month+1;
	$filter = "AND date >= '2012-04-10 00:00:00' AND date <= '2012-05-10 00:00:00'";
}
$query = $mysql->query("SELECT * FROM `own_vouchers` WHERE id > 0 $filter ORDER BY date ASC");

for($i=1;$i<=12;$i++)
	$mlist[] = $i;
	
	
if($_GET[justnow] == 1)
	$years = array("2015");
else
	$years = array("2010","2012","2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020");
	
foreach($years as $year)
{
	foreach($mlist as $m)
	{
		$prevmonth = $m - 1;
		$y = $year;
		
		if($prevmonth == 0)
		{
			$prevmonth = 12;
			$y = $year - 1;
			
		}
			
		$total = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers WHERE date >= '$y-$prevmonth-10 00:00:01' AND date <= '$year-$m-10 00:00:00'"));
		
		$totalpaid = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers WHERE paid_date >= '$y-$prevmonth-10 00:00:01' AND paid_date <= '$year-$m-10 00:00:00'"));

		
		
		$totalagi = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers INNER JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE date >= '$y-$prevmonth-10 00:00:01' AND contractor = 'Major Ágnes' AND date <= '$year-$m-10 00:00:00'"));

$totaleva = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers INNER JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE date >= '$y-$prevmonth-10 00:00:01' AND contractor = 'Németh Éva' AND date <= '$year-$m-10 00:00:00'"));




		if($totalagi[totalprice] > 0)
		{
			$teva = (int)$totaleva[totalprice];
			$tagi = (int)$totalagi[totalprice];
			$users[]= " ['$year. $mnt', $tagi, $teva]";
		}
	
		
			
		$mnt = $months[$m];
		if($total[totalprice] > 0)
			$dates[]= " ['$year. $mnt', $total[totalprice], $totalpaid[realprice]]";
		//	echo "$year-$m $total[totalprice] $total[realprice]<hr/>";
			
	
	}
}

$days = implode(',',$dates);
$user = implode(',',$users);

if($CURUSER[userclass] >= 255) { 
?>


   <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Hónap', 'Eladott', 'Fizetett'],
          <?=$days?>
        ]);

        var options = {
          title: 'Belföldi eladások',
          'width':870,
           'height':250,
           legend: {},
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div" style="width: 870px; height: 250px;"></div>
        
   
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Hónap', 'Major Ági', 'Németh Éva'],
          <?=$user?>
        ]);

        var options = {
          title: 'Belföldi eladások',
          'width':870,
           'height':250,
           legend: {},
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div2" style="width: 870px; height: 250px;"></div>

 <? } ?>      
        
<center>
<form method='GET'>
	<select name='year'>
		<option value='2018'>2018</option>
		<option value='2017'>2017</option>
		<option value='2016'>2016</option>
		<option value='2015'>2015</option>

		<option value='2014'>2014</option>
		<option value='2013' selected>2013</option>
		<option value='2012'>2012</option>
		<option value='2011'>2011</option>
		<option value='2010'>2010</option>
	</select>
		<select name='month'>
			<option value='1'>január</option>
			<option value='2'>február</option>
			<option value='3'>március</option>
			<option value='4'>április</option>
			<option value='5'>május</option>
			<option value='6'>június</option>
			<option value='7'>július</option>
			<option value='8'>augusztus</option>
			<option value='9'>szeptember</option>
			<option value='10'>október</option>
			<option value='11'>november</option>
			<option value='12'>december</option>
	</select>
	<input type='submit' value='Mutasd'/>
</form>
</center>
<hr/>
<?
echo "<table>";

echo "<tr class='header'>";
		echo "<td>Dátum</td>";
		echo "<td>Partner ID</td>";
		echo "<td>Szerződés ID</td>";
		echo "<td>Cégnév</td>";
		echo "<td>Hotel név</td>";	
		echo "<td>Mennyiség</td>";	
		echo "<td>Kötötte</td>";
		
		echo "<td>Névérték</td>";
		echo "<td>Eladási ár</td>";

	echo "</tr>";
	
$i=1;
while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partner_id = $arr[partner_id]"));
	
	$contracts = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE id = $arr[contract_id]"));
	
	if($contracts[contractor] <> 'Forró Tamás')
	{
	
	if($contracts[contractor] == '')	
		$class = 'red';
	else
		$class = '';
	echo "<tr class='$class'>";
		echo "<td align='center' width='70'>$arr[date]</td>";
		echo "<td align='center'>$arr[partner_id]</td>";
		echo "<td align='center'>$arr[contract_id]</td>";
		echo "<td>$partner[company_name]</td>";
		echo "<td>$partner[hotel_name]</td>";	
		echo "<td align='center'>$arr[quantity2] $arr[quantity]</td>";	
		echo "<td>$contracts[contractor]</td>";
		echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
		echo "<td align='right'>".formatPrice($arr[price])."</td>";
	echo "</tr>";
	
	$totalSum = $totalSum + $arr[orig_price];
	
	$income = $income + $arr[price];

	$i++;
	}
}
$i = $i-1;

echo "<tr class='header'>";
	echo "<td colspan='7'>Összesen ($i db)</td>";
	echo "<td align='right'><b>".formatPrice($totalSum,0,1)."</b></td>";
	echo "<td align='right'><b>".formatPrice($income,0,1)."</b></td>";
echo "</tr>";
echo "</table></div></div>";
foot();
?>