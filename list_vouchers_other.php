<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

head("Havi barter kimutatás");
?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
			<li><a href='/own_vouchers.php'>Saját eladás</a></li>
			<li><a href='/list_vouchers.php'>Kimutatás</a></li>
			<li><a href='/list_mkmedia.php'>Kimutatás MK Média</a></li>
			<li><a href='/list_vouchers_other.php' class='current'>Kimutatás NEM MK Média</a></li>

			
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


				
<?
if($_GET[page] == 'full')
	$filter = "AND own_vouchers.paid_date >= '2010-01-01' ";
elseif($_GET[month] > 0 && $_GET[year] > 0)
{
	$month = $_GET[month];
	$monthp = $_GET[month];
	
	$year = $_GET[year];
	if($month == 0)
	{
		$yearp = $year - 1;
		$month = 12;
	}
	else
		$yearp = $year;
	
	
	
	$filter = "AND own_vouchers.paid_date >= '$year-$month-01 00:00:01' AND own_vouchers.paid_date <= '$year-$month-31 00:00:00'";
}
else
{
	$month = date("m")-1;
	$monthp = $month+1;
	$filter = "AND own_vouchers.paid_date >= '2012-04-10 00:00:00' AND own_vouchers.paid_date <= '2012-05-10 00:00:00'";
}
$query = $mysql->query("SELECT own_vouchers.* FROM `own_vouchers` LEFT JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE contracts.company <> 'mkmedia' AND own_vouchers.id > 0 $filter ORDER BY own_vouchers.date ASC");

for($i=1;$i<=12;$i++)
	$mlist[] = $i;
	
	
if($_GET[justnow] == 1)
	$years = array("2015");
else
	$years = array("2010","2012","2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020");
	
foreach($years as $year)
{
	foreach($mlist as $m)
	{
		$prevmonth = $m - 1;
		$y = $year;
		
		if($prevmonth == 0)
		{
			$prevmonth = 12;
			$y = $year - 1;
			
		}
			
		$total = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers  LEFT JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE contracts.company <> 'mkmedia' AND  own_vouchers.date >= '$y-$prevmonth-10 00:00:01' AND own_vouchers.date <= '$year-$m-10 00:00:00'"));
		
		$totalpaid = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers  LEFT JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE contracts.company <> 'mkmedia' AND  own_vouchers.paid_date >= '$y-$prevmonth-10 00:00:01' AND own_vouchers.paid_date <= '$year-$m-10 00:00:00'"));

		
		
		$totalagi = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers INNER JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE contracts.company <> 'mkmedia' AND  own_vouchers.date >= '$y-$prevmonth-10 00:00:01' AND contractor = 'Major Ágnes' AND date <= '$year-$m-10 00:00:00'"));

$totaleva = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as totalprice, sum(price) as realprice  FROM own_vouchers INNER JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE contracts.company <> 'mkmedia' AND  date >= '$y-$prevmonth-10 00:00:01' AND contractor = 'Németh Éva' AND date <= '$year-$m-10 00:00:00'"));




		if($totalagi[totalprice] > 0)
		{
			$teva = (int)$totaleva[totalprice];
			$tagi = (int)$totalagi[totalprice];
			$users[]= " ['$year. $mnt', $tagi, $teva]";
		}
	
		
			
		$mnt = $months[$m];
		if($total[totalprice] > 0)
			$dates[]= " ['$year. $mnt', $total[totalprice], $totalpaid[realprice]]";
		//	echo "$year-$m $total[totalprice] $total[realprice]<hr/>";
			
	
	}
}

$days = implode(',',$dates);
$user = implode(',',$users);

if($CURUSER[userclass] >= 255) { 
?>


   <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Hónap', 'Eladott', 'Fizetett'],
          <?=$days?>
        ]);

        var options = {
          title: 'Belföldi eladások',
          'width':870,
           'height':250,
           legend: {},
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div" style="width: 870px; height: 250px;"></div>
        
   
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Hónap', 'Major Ági', 'Németh Éva'],
          <?=$user?>
        ]);

        var options = {
          title: 'Belföldi eladások',
          'width':870,
           'height':250,
           legend: {},
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div2" style="width: 870px; height: 250px;"></div>

 <? } ?>      
        
<center>
<form method='GET'>
	<select name='year'>
		<option value='2018'>2018</option>
		<option value='2017'>2017</option>
		<option value='2016' selected>2016</option>
		<option value='2015'>2015</option>

		<option value='2014'>2014</option>
		<option value='2013'>2013</option>
		<option value='2012'>2012</option>
		<option value='2011'>2011</option>
		<option value='2010'>2010</option>
	</select>
		<select name='month'>
			<option value='1' <? if($_GET[month] == 1) echo "selected"?>>január</option>
			<option value='2' <? if($_GET[month] == 2) echo "selected"?>>február</option>
			<option value='3' <? if($_GET[month] == 3) echo "selected"?>>március</option>
			<option value='4' <? if($_GET[month] == 4) echo "selected"?>>április</option>
			<option value='5' <? if($_GET[month] == 5) echo "selected"?>>május</option>
			<option value='6' <? if($_GET[month] == 6) echo "selected"?>>június</option>
			<option value='7' <? if($_GET[month] == 7) echo "selected"?>>július</option>
			<option value='8' <? if($_GET[month] == 8) echo "selected"?>>augusztus</option>
			<option value='9' <? if($_GET[month] == 9) echo "selected"?>>szeptember</option>
			<option value='10' <? if($_GET[month] == 10) echo "selected"?>>október</option>
			<option value='11' <? if($_GET[month] == 11) echo "selected"?>>november</option>
			<option value='12' <? if($_GET[month] == 12) echo "selected"?>>december</option>
	</select>
	<input type='submit' value='Mutasd'/>
</form>
</center>
<hr/>
<?
echo "<table>";

echo "<tr class='header'>";
		echo "<td>Eladás Dátuma</td>";
		echo "<td>Szerződés Dátuma</td>";
		echo "<td>Partner ID</td>";
		echo "<td>Szerződés ID</td>";
		echo "<td>Cégnév</td>";
		echo "<td>Hotel név</td>";	
		echo "<td>Mennyiség</td>";	
		echo "<td>Kötötte</td>";
		
		echo "<td>Névérték</td>";
		echo "<td>Eladási ár</td>";

	echo "</tr>";
	
$i=1;
while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partner_id = $arr[partner_id]"));
	
	$contracts = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE id = $arr[contract_id]"));
	
	if($contracts[contractor] <> 'Forró Tamás')
	{
	
	if($arr[paid_date] <> '0000-00-00 00:00:00')	
		$class = 'green';
	else
		$class = '';
		
		$paid = explode(" ", $arr[paid_date]);
	echo "<tr class='$class'>";
		echo "<td align='center' width='70'>$paid[0]</td>";
		echo "<td align='center' width='70'>$arr[date]</td>";

		echo "<td align='center'>$arr[partner_id]</td>";
		echo "<td align='center'>$arr[contract_id]</td>";
		echo "<td>$partner[company_name]</td>";
		echo "<td>$partner[hotel_name]</td>";	
		echo "<td align='center'>$arr[quantity2] $arr[quantity]</td>";	
		echo "<td>$contracts[contractor]</td>";
		echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
		echo "<td align='right'>".formatPrice($arr[price])."</td>";
	echo "</tr>";
	
	$totalSum = $totalSum + $arr[orig_price];
	
	$income = $income + $arr[price];

	$i++;
	}
}
$i = $i-1;

echo "<tr class='header'>";
	echo "<td colspan='8'>Összesen ($i db)</td>";
	echo "<td align='right'><b>".formatPrice($totalSum,0,1)."</b></td>";
	echo "<td align='right'><b>".formatPrice($income,0,1)."</b></td>";
echo "</tr>";
echo "</table><br/><br/>";

$query = $mysql->query("SELECT own_vouchers.* FROM `own_vouchers` LEFT JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE contracts.company <> 'mkmedia' AND own_vouchers.id > 0 AND paid_date = '0000-00-00 00:00:00' ORDER BY own_vouchers.date ASC");
//nicnsenek eladva
/*
echo "<h2>Szerződéshez tartozó voucherek</h2>";

$totalSum = 0;
$income = 0;
echo "<table>";

echo "<tr class='header'>";
		echo "<td>Eladás Dátuma</td>";
		echo "<td>Szerződés Dátuma</td>";
		echo "<td>Partner ID</td>";
		echo "<td>Szerződés ID</td>";
		echo "<td>Cégnév</td>";
		echo "<td>Hotel név</td>";	
		echo "<td>Mennyiség</td>";	
		echo "<td>Kötötte</td>";
		
		echo "<td>Névérték</td>";
		echo "<td>Eladási ár</td>";

	echo "</tr>";
	
$i=1;
while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partner_id = $arr[partner_id]"));
	
	$contracts = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE id = $arr[contract_id]"));
	
	if($contracts[contractor] <> 'Forró Tamás')
	{
	
	if($arr[paid_date] <> '0000-00-00 00:00:00')	
		$class = 'green';
	else
		$class = '';
		
				$paid = explode(" ", $arr[paid_date]);


	echo "<tr class='$class'>";
		echo "<td align='center' width='70'>$paid[0]</td>";
		echo "<td align='center' width='70'>$arr[date]</td>";

		echo "<td align='center'>$arr[partner_id]</td>";
		echo "<td align='center'>$arr[contract_id]</td>";
		echo "<td>$partner[company_name]</td>";
		echo "<td>$partner[hotel_name]</td>";	
		echo "<td align='center'>$arr[quantity2] $arr[quantity]</td>";	
		echo "<td>$contracts[contractor]</td>";
		echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
		echo "<td align='right'>".formatPrice($arr[price])."</td>";
	echo "</tr>";
	
	$totalSum = $totalSum + $arr[orig_price];
	
	$income = $income + $arr[price];

	$i++;
	}
}
$i = $i-1;

echo "<tr class='header'>";
	echo "<td colspan='8'>Összesen ($i db)</td>";
	echo "<td align='right'><b>".formatPrice($totalSum,0,1)."</b></td>";
	echo "<td align='right'><b>".formatPrice($income,0,1)."</b></td>";
echo "</tr>";
echo "</table><br/><br/>";
*/
$totalSum = 0;
$income = 0;

$query = $mysql->query("SELECT own_vouchers.* FROM `own_vouchers` LEFT JOIN contracts ON contracts.id = own_vouchers.contract_id WHERE contracts.company <> 'mkmedia' AND own_vouchers.id > 0 ANd paid <> 1 ORDER BY own_vouchers.date ASC");

echo "<h2>Raktár</h2>";

echo "<table>";

echo "<tr class='header'>";
		echo "<td>Eladás Dátuma</td>";
		echo "<td>Szerződés Dátuma</td>";
		echo "<td>Partner ID</td>";
		echo "<td>Szerződés ID</td>";
		echo "<td>Cégnév</td>";
		echo "<td>Hotel név</td>";	
		echo "<td>Mennyiség</td>";	
		echo "<td>Kötötte</td>";
		
		echo "<td>Névérték</td>";
		echo "<td>Eladási ár</td>";

	echo "</tr>";
	
$i=1;
while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partner_id = $arr[partner_id]"));
	
	$contracts = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE id = $arr[contract_id]"));
	
	if($contracts[contractor] <> 'Forró Tamás')
	{
	
	if($arr[paid] == 1)	
		$class = 'green';
	else
		$class = '';
		
	$paid = explode(" ", $arr[paid_date]);

	echo "<tr class='$class'>";
		echo "<td align='center' width='70'>$paid[0]</td>";
		echo "<td align='center' width='70'>$contracts[added]</td>"; 

		echo "<td align='center'>$arr[partner_id]</td>";
		echo "<td align='center'>$arr[contract_id]</td>";
		echo "<td>$partner[company_name]</td>";
		echo "<td>$partner[hotel_name]</td>";	
		echo "<td align='center'>$arr[quantity2] $arr[quantity]</td>";	
		echo "<td>$contracts[contractor]</td>";
		echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
		echo "<td align='right'>".formatPrice($arr[price])."</td>";
	echo "</tr>";
	
	$totalSum = $totalSum + $arr[orig_price];
	
	$income = $income + $arr[price];

	$i++;
	}
}
$i = $i-1;

echo "<tr class='header'>";
	echo "<td colspan='8'>Összesen ($i db)</td>";
	echo "<td align='right'><b>".formatPrice($totalSum,0,1)."</b></td>";
	echo "<td align='right'><b>".formatPrice($income,0,1)."</b></td>";
echo "</tr>";
echo "</table><br/><br/>";


echo "</div></div>";
foot();
?>