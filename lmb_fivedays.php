<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

userlogin();

head();

echo "<h2>LMB értesítők</h2>";


$sure = $_GET[sure];
$send = $_GET[send];

if($sure <> 1 && $send == 1) {
	echo message("<b>Biztosan ki szeretné küldeni a havi értesítőt? <a href=\"?sure=1&send=1\">Igen!</a> | <a href=\"../customers.php\">Nem!</a></b>");
	die;
}

	echo "<a href=\"?send=1\" class='button green'>LMB Értesítők kiküldése</a><div class='cleaner'></div>";
	
	echo "<a href=\"?showletter=1\" class='button green'>Leveleket mutasd</a><div class='cleaner'></div>";
echo "<hr/>";


$headers = "From: penzugy@indulhatunk.hu\r\n" .
"Reply-To: penzugy@indulhatunk.hu\r\n" .
"Content-type: text/html; charset=utf-8\r\n".
"X-Mailer: PHP/" . phpversion();	
	
$subject = "Mostutazz.hu elszámolás";

$i = 1;
$curMonth = date('n');
if($curMonth == 1)
	$curMonth = 13;

echo "<table>";



$query = $mysql->query("SELECT lmb.partner_id,lmb.partner_name,partners.email,partners.pid, partners.hotel_name FROM lmb INNER JOIN partners ON lmb.partner_id = partners.coredb_id WHERE  lmb.can_go = 1 AND lmb.arrived = 0 AND lmb.invoice_created = 0 AND MONTH(lmb.from_date) < $curMonth GROUP BY lmb.partner_id"); //AND MONTH(lmb.from_date) <> 1 
while($arr = mysql_fetch_assoc($query))
{
	$totalPrice = 0;
	$tableRows = '';
	$body = '';
	
	$items = mysql_query("SELECT * FROM lmb WHERE  can_go = 1 AND arrived = 0 AND partner_id = $arr[partner_id] AND lmb.invoice_created = 0 AND MONTH(lmb.from_date) < $curMonth");
	while($itemArr = mysql_fetch_assoc($items))
	{
		
		$offerPrice =  number_format($itemArr[total_price], 0, ',', ' '). " Ft";
		$offerDate = explode(" ",$itemArr[date]);
		$offerDate = $offerDate[0];
		$tableRows.= "<tr>
						<td>$itemArr[realID]</td>
						<td>$offerDate</td>
						<td>$itemArr[name]</td>
						<td>$itemArr[accomodation_name]</td>
						<td>$itemArr[from_date]</td>
						<td>$itemArr[to_date]</td>
						<td align='right'><b>$offerPrice</b></td>
					</tr>";
		$totalPrice = $totalPrice + $itemArr[total_price];
	}


if($totalPrice > 5)
{
	
$tPrice =  number_format($totalPrice, 0, ',', ' '). " Ft";
	
$body  =
<<<EOF

Kedves Partnerünk! <br/><br/>
Nyilvántartásunk alapján az alábbi foglalásokat közvetítettük az elmúlt időszakban.<br/>
Kérem, 5 napon belül levelünket visszaigazolni szíveskedjen, hogy az elektronikus jutalékszámlánkat<br/>
elkészíthessük, melyet minden hónap 10-e után tölthetnek le az adminisztrációs felületünkről.<br/><br/>

<b>LastMinuteBelföld:</b><br/><br/>
<table cellspacing='1' cellpadding='4' border='1'>
	<tr>
		<td><b>Foglalás száma</b></td>
		<td><b>Dátum</b></td>
		<td><b>Név</b></td>
		<td><b>Ajánlat neve</b></td>
		<td><b>Érkezés</b></td>
		<td><b>Távozás</b></td>
		<td><b>Érték</b></td>
	</tr>
	$tableRows
	<tr>
		<td colspan='6'><b>Összesen:</b></td>
		<td><b>$tPrice</b></td>
	<tr>
</table>
<br/><br/>


Üdvözlettel:<br/>
Finta Orsolya

EOF;

if($send == 1 && $sure == 1)
{

	$message = array();
	$message[pid] = $arr[pid];
		$message[added_by] = $CURUSER[username];
		$message[added] = 'NOW()';
		$message[message] = $body;
		$message[message_email] = $arr[email];
		$mysql->query_insert("messages",$message);
		
		
	mail($arr[email],$subject, $body,iconv('utf-8','ISO-8859-2',$headers));
	mail("info@indulhatunk.hu",$subject, $body,iconv('utf-8','ISO-8859-2',$headers));
	mail("it@indulhatunk.hu",$subject, $body,iconv('utf-8','ISO-8859-2',$headers));
	mysql_query("update lmb set notification_date = now() where partner_id = $arr[partner_id] AND can_go = 1 AND arrived = 0");
	echo "elküldve<hr/>";
}
}
	if($_GET[showletter] == 1)
		echo "<tr><td align='center'>$i</td><td>$arr[hotel_name]</td><td>$arr[email]</td><td>$body</td></tr>";
	else
		echo "<tr><td align='center'>$i</td><td>$arr[hotel_name]</td><td>$arr[email]</td><td></td></tr>";

	
	$i++;	
}

echo "</table>"
?>