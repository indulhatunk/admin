<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();



$h = 2;

$send = $_GET[send];
$sure = $_GET[sure];

$mysql->query("UPDATE partners SET lmb_due = 0");

if($CURUSER["userclass"] < 90) {
	header("location: lmb.php");
}
$cleared = $_GET[cleared];

if($cleared == 1)
{
	$clr = "lmb.invoice_cleared <> '0000-00-00 00:00:00' AND";
	$cls = 'grey';
	$hide = 'style="display:none;"';
}
else
{
	$clr = "lmb.invoice_cleared = '0000-00-00 00:00:00' AND";
	$update = 1;
}
	$itemQuery = $mysql->query("SELECT month(from_date) as mnth, lmb.invoice_number,lmb.invoice_date,partners.coredb_id,partners.account_no,partners.hotel_name,partners.pid,partners.company_name,partners.tax_no,partners.phone,partners.email,partners.zip,partners.address,partners.city,partners.yield FROM lmb
		INNER JOIN partners ON partners.coredb_id = lmb.partner_id 
		WHERE 
			
			$clr
			lmb.from_date > '2011-04-01' AND 
			lmb.invoice_created = 1 		
			
		    GROUP BY lmb.invoice_number ORDER BY partners.company_name ASC");
		    

$header.= "<table border=1>";

	$header.= "<tr class='header'>";
	$header.= "<td colspan='2'>Cég neve</td>";
	$header.= "<td align='right'>Kiállítandó</td>";
	$header.= "<td align='right'>ÁFA</td>";
	$header.= "<td align='right'>Számlaszám</td>";
	$header.= "<td align='right'>Kiegyenlítve</td>";
	$header.= "</tr>";

$h = 1;
$totalrows = 0;
while($partnerArr = mysql_fetch_assoc($itemQuery)) {

	//update partner notification bar if invoice was not received
	if($update == 1)
	{
		$mysql->query("UPDATE partners SET lmb_due = 1 WHERE pid = $partnerArr[pid]");
	}
	//update partner status end
	
	
	$itemArr = mysql_fetch_assoc($mysql->query("SELECT sum(total_price) AS total_price, count(total_price) as total FROM lmb WHERE invoice_number = '$partnerArr[invoice_number]'"));
	$w = date("m",strtotime($partnerArr[invoice_date]))-1;
	
	if($w == 0)
		$w = 12;
		
	$date = str_replace("-",'',$partnerArr[invoice_date]);
	if($date < 20120120)
	{
		$tax = 1.25;
		$tax_name = "25%";
	}
	else
	{
		$tax = 1.27;
		$tax_name = "27%";
	}
	$yield = formatPrice(($itemArr[total_price]*($partnerArr['yield']/100))*$tax);
	$totalPrice = formatPrice($itemArr[total_price]);
	$hash = md5($partnerArr[invoice_number]."#FWf#CEFA#SDF343sf#");;
	
	$rows.= "<tr class='$cls'><td>".$w.". hónap</td>";
	$rows.= "<td>".$partnerArr[company_name]." $partnerArr[hotel_name]</td>";
	$rows.= "<td  align='right'>".($yield)."</td>";
	$rows.= "<td  align='right'>".($tax_name)."</td>";
	$rows.= "<td  align='right'><a href='/invoices/lmb/".str_replace("/","_",$partnerArr[invoice_number]).".pdf' target='_blank'>$partnerArr[invoice_number]</a></td>";
	$rows.= "<td  align='right'><input type='button' value='Kiegyenlítve' class='cleared_lmb' id='$partnerArr[invoice_number]' $hide/></td>";	
	$rows.= "</tr>";
	
	$total = $total+(($itemArr[total_price]*($partnerArr['yield']/100))*$tax);
/// $totalPrice Ft * $partnerArr['yield']% + $tax_name Áfa <br/><br/> Összesen
	$body  =
<<<EOF

Kedves Partnerünk! <br/><br/>

Az alábbiakban részletezzük az aktuális elszámolásunkat.<br/>

A már az előző levelünkben leegyeztetett forgalom:<br/><br/>
			<b>$itemArr[total] db</b> foglalás történt <b>$totalPrice</b> értékben.<br/><br/>
			A szerződésünk értelmében a számla végösszege: <b>$yield</b><br/><br/>

A $partnerArr[invoice_number] számú jutalékszámlát az alábbi linkre kattintva töltheti le: <a href="https://admin.indulhatunk.hu/invoices/dl/$hash">$partnerArr[invoice_number] sz. számla letöltése</a><br/><br/>

$yield</b>-ot kérjük átutalni szíveskedjen az alábbi bankszámlánkra.<br/><br/>
Hotel Outlet Kft.<br/>
Erste Bank<br/>
11600006-00000000-49170300<br/><br/>

Üdvözlettel:<br/>
Forró Tamás

EOF;

if($send == 1 && $sure == 1)
{
	$mth = date("n")-1;
	
	if($mth == 0)
		$mth = 12;
	
	if($partnerArr[mnth] == $mth)	
	{
	
		$message = array();
		$message[pid] = $partnerArr[pid];
		$message[added_by] = $CURUSER[username];
		$message[added] = 'NOW()';
		$message[message] = $body;
		$message[message_email] = $partnerArr[email];
		$mysql->query_insert("messages",$message);
		
		
		//echo "$mth. havi elszámolás a(z) $partnerArr[hotel_name] részére<br/><br/>$body<hr/>";
		sendEmail("$mth. havi elszámolás a(z) $partnerArr[hotel_name] részére",$body,$partnerArr[email],'Partnerünk','info@indulhatunk.hu', 'Forró Tamás' );
		sendEmail("$mth. havi elszámolás a(z) $partnerArr[hotel_name] részére",$body,"info@indulhatunk.hu",'Partnerünk','info@indulhatunk.hu', 'Forró Tamás' );
		echo "elküldve<hr/>";
	}
	
}


	if($_GET[showletter] == 1)
		$rows.= "<tr class='grey'><td colspan='10'>$body</td></tr>";
		

		
}

$footer.= "<tr class='header'>
	<td colspan='2' align='left'>Összesen</td>
	<td align='right'>".formatPrice($total)."</td>
	<td colspan='3'></td>
</tr>";
$footer.= "</table>";

head();


if($send == 1 && $sure <> 1)
{
		echo message("Biztosan ki szeretné küldeni a havi LMB leveleket? <a href='?showletter=1&send=1&sure=1'><b>Igen</b></a> | <a href='?showletter=1'><b>Nem</b></a>");
	foot();
	

	die;
}


?>
<h2>LMB pénzügyek</h2>

<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
					
		<li><a href="?show=all"  class="current">Kiállított számlák</a></li>
	<? if($CURUSER[userclass] == 255) {?>
		<li><a href="/lmb_fivedays.php">Havi értesítők (5-ig)</a></li>
		<li><a href="/invoice/create_invoice_lmb.php">Számlázás (10-én)</a></li>
	<? } ?>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<?

echo "<div style='float:left;padding:5px 0 0 10px;'><a href='?cleared=0' class='button red'><b>Kiegyenlítetlen</b></a> <a href='?cleared=1' class='button grey'><b>Kiegyenlített</b></a> <a href='?showletter=1' class='button blue'><b>Leveleket mutasd</b></a>  <a href='?send=1' class='button green'><b>Leveleket kiküld</b></a> </div><div class='cleaner'></div><hr/>";
	echo $header;
	echo $rows;
	echo $footer;
	
if($CURUSER[userclass] == 255)
{

$item = $mysql->query("SELECT * from lmb WHERE
			lmb.invoice_date > '2013-11-15' AND 
			lmb.invoice_created = 1 		
			
		    GROUP BY lmb.invoice_number ORDER BY partner_id ASC");
echo "<hr/><h2>Számlázási eltérések</h2><table>";		    
while($arr = mysql_fetch_assoc($item))	
{
	$partnerArr = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partners.coredb_id = $arr[partner_id]"));
	$itemArr = mysql_fetch_assoc($mysql->query("SELECT sum(total_price) AS total_price, count(total_price) as total FROM lmb WHERE invoice_number = '$arr[invoice_number]'"));
	
	$y = $itemArr[total_price]*($partnerArr['yield']/100)*$tax;
	$sum = $sum + $y;
	$yield = formatPrice($y);
	$totalPrice = formatPrice($itemArr[total_price]);
	
	
	$invoice = mysql_fetch_assoc($mysql->query("SELECT * FROM log_invoice WHERE invoice_number = '$arr[invoice_number]'"));

	$request = unserialize($invoice[request]);

	$inv = $request[data][szla_xml][fej][netto_ossz];
	
		//echo "<pre>";


	//print_R($invoice);
		
	//print_r($request);
	
	//die;
		
	echo "<tr>
		<td>$arr[invoice_date]</td>
		<td><a href='/invoices/lmb/".str_replace("/","_",$arr[invoice_number]).".pdf' target='_blank'>$arr[invoice_number]</a></td>
		<td>$partnerArr[hotel_name]</td>

		<td align='right'>$yield</td>
		<td align='right'>".formatPrice($inv*1.27)."</td>
		<td align='right'>".formatPrice($y-$inv*1.27)."</td>
					</tr>";
	
	
	$yplus = $yplus + $inv*1.27;
	$yp = $yp+($y-$inv*1.27);
	
}  

	echo "<tr>
		<td colspan='3'>Összesen</td>
		<td align='right'>".formatPrice($sum)."</td>
		<td align='right'>".formatPrice($yplus)."</td>
		<td align='right'>".formatPrice($yp)."</td>
		</tr>";  
echo "</table>";		    

}
?>
</div></div>
<?
foot();
?>