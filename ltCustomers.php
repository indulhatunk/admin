<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();



$payments = array(
	1 => 'Készpénz',
	2 => 'Átutalás',
	3 => 'Üdülési csekk',
	4 => 'Bankkártya',
);

$shipments = array(
	1 => 'Személyes',
	2 => 'Levél',
	3 => 'Utánvét',
);

$status = array(
	0 => 'Elintézendő',
	1 => 'Fizetett',
	2 => 'Számlázott',
	3 => 'Törölt',
);
if($_POST[id] > 0)
{
	$mysql->query("UPDATE licittravel_customers SET status = '$_POST[status]' WHERE id = '$_POST[id]'");
}




if($_GET[from_date] <> '')
	$from = "AND licittravel_customers.added >= '$_GET[from_date]'";

if($_GET[from_date] <> '')
	$to = "AND licittravel_customers.added <= '$_GET[to_date]'";
	
	
if($_GET[status] > 0 )
	$extraSelect = "AND licittravel_customers.status = $_GET[status]";
else
	$extraSelect = "AND licittravel_customers.status = 0";


if($_GET[fieldvalue] <> '' )
{
	if($_GET[field] == 'id')
		$extraSelect = "AND licittravel_offers.$_GET[field] = '$_GET[fieldvalue]'";
	else
		$extraSelect = "AND licittravel_offers.$_GET[field] LIKE '%$_GET[fieldvalue]%'";

}
if($_GET[partner_id] <> '')
	$extraid = "AND licittravel_offers.pid = '$_GET[partner_id]'";


if($_GET[name] <> '')
	$name = "AND licittravel_customers.name LIKE '%$_GET[name]%'";
	
if($_GET[email] <> '')
	$email = "AND licittravel_customers.email LIKE '%$_GET[email]%'";	

	$qr = $mysql->query("SELECT licittravel_customers.* FROM licittravel_customers INNER JOIN licittravel_offers ON licittravel_offers.id = licittravel_customers.offer_id WHERE licittravel_customers.id > 0 $extraSelect $from $to $innerid $extraid $oid $name $email ORDER BY id DESC");




if($_GET[export] == 1)
{
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename=felhasznalok-'.date("Y-m-d").'.xls');

	//$query = $mysql->query("SELECT licittravel_customers.name, licittravel_bids.name as b_name, licittravel_customers.*, licittravel_bids.*  FROM licittravel_customers INNER JOIN licittravel_bids ON licittravel_bids.email = licittravel_customers.email group by licittravel_bids.email ORDER BY licittravel_customers.name ASC");
	echo "<table border='1'>";
		echo "<tr style='text-align:center; font-weight:bold;'>";
			echo "<td style='background-color:#c4c4c4;'>ID</td>";
			echo "<td style='background-color:#c4c4c4;'>Eladás dátuma</td>";
			echo "<td style='background-color:#c4c4c4;'>Felhasználónév</td>";
			echo "<td style='background-color:#c4c4c4;'>Név</td>";
			echo "<td style='background-color:#c4c4c4;'>E-Mail</td>";
			echo "<td style='background-color:#c4c4c4;'>Irsz.</td>";
			echo "<td style='background-color:#c4c4c4;'>Város</td>";
			echo "<td style='background-color:#c4c4c4;'>Cím</td>";
			echo "<td style='background-color:#c4c4c4;'>Telefonszám</td>";
			echo "<td style='background-color:#c4c4c4;'>Utolsó licit</td>";
			echo "<td style='background-color:#c4c4c4;'>Partner</td>";
			
			echo "<td style='background-color:#c4c4c4;'>Tétel ID</td>";
			echo "<td style='background-color:#c4c4c4;'>Tételszám</td>";


			echo "<td style='background-color:#c4c4c4;'>Ajánlat</td>";
			echo "<td style='background-color:#c4c4c4;'>Ár</td>";

		echo "</tr>";
	while($arr = mysql_fetch_assoc($qr))
	{
		$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM licittravel_offers WHERE id = $arr[offer_id] LIMIT 1"));
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $offer[pid]"));
		
		echo "<tr>";
			echo "<td>$arr[id]</td>";
			echo "<td>$arr[added]</td>";
			echo "<td>$arr[username]</td>";
			echo "<td>$arr[name]</td>";
			echo "<td>$arr[email]</td>";
			echo "<td>$arr[zip]</td>";
			echo "<td>$arr[city]</td>";
			echo "<td>$arr[address]</td>";
			echo "<td>$arr[phone]</td>";
			echo "<td>$arr[added]</td>";
			
			echo "<td>$partner[hotel_name]</td>";
			
			echo "<td>$arr[offer_id]</td>";
			echo "<td>$offer[inner_id]</td>";


			echo "<td>$offer[name]</td>";
			echo "<td>".formatprice($arr[price])."</td>";
		echo "</tr>";
	}
	echo "</table>";
	die;
}

head("LicitTravel vásárlók kezelése");

echo message($msg);


?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?status=0" class='<? if($_GET[status] == '' || $_GET[status] == 0) echo "current"?>'>Elintézendő vásárlók</a></li>
		<li><a href="?status=1" class='<? if($_GET[status] == '1') echo "current"?>'>Fizetett vásárlók</a></li>
		<li><a href="?status=2" class='<? if($_GET[status] == '2') echo "current"?>'>Számlázott vásárlók</a></li>
		<li><a href="?status=3" class='<? if($_GET[status] == '3') echo "current"?>'>Törölt vásárlók</a></li>
	</ul>
	<div class='clear'></div>
</div>
<div class='contentpadding'>

<form method='GET'>

<select name='partner_id' style='width:100%'>
		
		<option value=''>Partner</option>
	<?
		$query = $mysql->query("SELECT pid,hotel_name FROM partners WHERE userclass < 100 AND partner_type = 'licittravel' order by partners.hotel_name ASC");
		while($arr = mysql_fetch_assoc($query))
		{
			if($_GET[partner_id] == $arr[pid])
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='$arr[pid]' $selected>$arr[hotel_name] </a>";
		}
	?>
	</select>
	<hr/>
	
	<input type='text' name='email' id='lt_email' value='<?=$_GET[email]?>' style='width:140px'/>
	<input type='text' name='name' id='lt_name' value='<?=$_GET[name]?>'  style='width:140px'/>
	
	<input type='hidden' name='status' value='<?=$_GET[status]?>'/>

<input type='text' name='from_date'   class='dfilter' id="lt_from_date" value='<?=$_GET[from_date]?>' style='width:85px;'/> 
		<input type='text' name='to_date'   class='dfilter' id="lt_to_date"  value='<?=$_GET[to_date]?>' style='width:85px;'/>


<!--

	

	<select name='offer_id' style='width:300px;'>
		
		<option value=''>Ajánlat</option>
	<?
		$query = $mysql->query("SELECT * FROM licittravel_offers WHERE archive = 0 ORDER BY id ASC");
		while($arr = mysql_fetch_assoc($query))
		{
			if($_GET[offer_id] == $arr[id])
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='$arr[id]' $selected>$arr[name] ($arr[inner_id])</a>";
		}
	?>
	</select>
	
	<select name='inner_id'>
		
		<option value=''>Tételszám</option>
	<?
		$query = $mysql->query("SELECT * FROM licittravel_offers WHERE archive = 0 ORDER BY inner_id ASC");
		while($arr = mysql_fetch_assoc($query))
		{
			if($_GET[inner_id] == $arr[id])
				$selected = 'selected';
			else
				$selected = '';
				
			echo "<option value='$arr[id]' $selected>$arr[inner_id]</a>";
		}
	?>
	</select>
	
	
	<select name='id'>
		
		<option value=''>ID</option>
	<?
		$query = $mysql->query("SELECT * FROM licittravel_offers WHERE archive = 0 ORDER BY id ASC");
		while($arr = mysql_fetch_assoc($query))
		{
			if($_GET[id] == $arr[id])
				$selected = 'selected';
			else
				$selected = '';
			echo "<option value='$arr[id]' $selected>$arr[id]</a>";
		}
	?>
	</select>
	
-->


	<input type='text' name='fieldvalue' value='<?=$_GET[fieldvalue]?>' id='lt_content' style='width:225px'/>
	<select name='field'>
			<option value='name' <? if($_GET[field] == 'name' || $_GET[field] == '') echo "selected"?>>Ajánlat neve</option>
			<option value='id' <? if($_GET[field] == 'id') echo "selected"?>>Ajánlat ID</option>
			<option value='inner_id' <? if($_GET[field] == 'inner_id') echo "selected"?>>Tételszám</option>
	</select>
	

	<div style='text-align:center'>
	
		<input type='submit' value='Szűrés'/> <input type='button' value='Exportálás' onclick='document.location.href="?partner_id=<?=$_GET[partner_id]?>&email=<?=$_GET[email]?>&name=<?=$_GET[name]?>&status=<?=$_GET[status]?>&from_date=<?=$_GET[from_date]?>&to_date=<?=$_GET[to_date]?>&fieldvalue=<?=$_GET[fieldvalue]?>&field=<?=$_GET[field]?>&export=1"' />
	</div>
</form>

<!--<a href='?export=1' class='button blue'><img src='/images/excel.gif' valign='middle' width='11'/>&nbsp;&nbsp;Exportálás</a>-->
<div class='cleaner'></div>
<hr/>
<?



$query = $qr;

if(mysql_num_rows($query) == 0)
{
	echo message("Nincs találat")."</div></div>";
	foot();
	die;
}	


echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td width='20'>ID</td>";
	echo "<td width='20'>T.sz</td>";

	//echo "<td width='80'>Dátum</td>";
	echo "<td>Ajánlat</td>";
	echo "<td>Nicknév</td>";
	echo "<td>Név</td>";
	echo "<td>E-mail</td>";
	echo "<td>Telefonszám</td>";
	echo "<td>Cím</td>";
	//echo "<td>Fiz. mód</td>";
//	echo "<td>Száll. mód</td>";
	echo "<td>Összeg</td>";
	echo "<td>Licitek</td>";
	echo "<td>Állapot</td>";
echo "</tr>";


while($arr = mysql_fetch_assoc($query)) {

	$offer = mysql_fetch_assoc($mysql->query("SELECT inner_id, id,name FROM licittravel_offers offers WHERE id = $arr[offer_id]"));

if($arr[status] == 3)
	$class = 'red';
elseif($arr[status] == 2)
	$class = 'grey';
elseif($arr[status] == 1)
	$class = 'green';
else
	$class = '';
	echo "<tr class=\"$class\">";
		echo "<td align='center'>$arr[id]</td>";
		
		echo "<td align='center'>$offer[id]<br/>($offer[inner_id])</td>";
		//echo "<td>$arr[added]</td>";
		echo "<td><a href='ltOffers.php?editid=$offer[id]&add=1' target='_blank'><b>$offer[name]</b></a></td>";
		echo "<td>$arr[username]</td>";
		echo "<td>$arr[name]</td>";
		echo "<td>$arr[email]</td>";
		echo "<td>$arr[phone]</td>";
		echo "<td>$arr[zip] $arr[city] $arr[address]</td>";
		
		//echo "<td>".$payments[$arr[payment]]."</td>";
		//echo "<td>".$shipments[$arr[shipment]]."</td>";
		
		echo "<td align='right'>".formatPrice($arr[price],0,1)."</td>";
		
		echo "<td align='center'><a href='ltBids.php?offer_id=$offer[id]' alt='A tételhez tartozó licitek' title='A tételhez tartozó licitek'><img src='/images/arrow.png'/></a></td>";


		echo "<td>
		<form method='post'>
			<input type='hidden' name='id' value='$arr[id]'/>
			<select name='status' onchange='submit()'/>";
			
			
		foreach($status as $key => $name)
		{
			if($key == $arr[status])
				$selected = 'selected';
			else
				$selected = '';
				
			echo "<option value='$key' $selected>$name</option>";
		}
		echo "</select>
		</form>
		</td>";
	echo "</tr>";
	
}
echo "</table></div></div>";

foot();
?>