<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();


$name = $_POST[name];
$editID = (int)$_POST[id];


$_POST[from_date] = "$_POST[from_date_date] $_POST[from_date_time]";
$_POST[to_date] = "$_POST[to_date_date] $_POST[to_date_time]";

unset($_POST[from_date_date]);
unset($_POST[from_date_time]);
unset($_POST[to_date_date]);
unset($_POST[to_date_time]);



if($_GET[delete] > 0 && $_GET[sure] <> 1) {
	die("<h2>$lang[delete_item]</h2> <br/> <a href=\"?delete=$_GET[delete]&sure=1&returnto=".urlencode($_GET[returnto])."\" class='button green'>$lang[yes]!</a>  <a href=\"ltOffers.php\" class='button red'>$lang[no]!</a><div class='cleaner'></div>");
}
if($_GET[delete] > 0 && $_GET[sure] == 1) {
		
	$mysql->query("UPDATE licittravel_offers SET active = 6 WHERE id = $_GET[delete]");
	
	writelog("LICITTRAVEL: $CURUSER[username] deleted item #$_GET[delete]");
	header("Location: $_GET[returnto]");
	die;
}

$orderby = $_GET[orderby];

if($orderby == '' || $orderby == 'desc')
{
	$orderbySql = "licittravel_offers.id DESC";
	$orderbylink = 'asc';
}
elseif($orderby == 'descinner')
{
	$orderbySql = "licittravel_offers.inner_id DESC";
	$orderbylink2 = 'ascinner';
}
elseif($orderby == 'ascinner')
{
	$orderbySql = "licittravel_offers.inner_id ASC";
	$orderbylink2 = 'descinner';
}
else
{
	$orderbySql = "licittravel_offers.id ASC";
	$orderbylink = 'desc';
	$orderbylink2 = 'descinner';
}
if($_POST[hidden_min_price] == 'on')
	$_POST[hidden_min_price] = 1;
else
	$_POST[hidden_min_price] = 0;

	
if($name <> '' && $editID == '' && $_POST[pid] > 0)
{
	$returnto = $_POST[returnto];
	unset($_POST[returnto]);
	
	if($_POST[buy_it_now_stop] == 0)
		$_POST[buy_it_now_stop] = $_POST[buy_it_now];
		
		
	if($_POST[inner_id] <> '')
		$extend = "-".str_replace("-",'',strtolower(clean_url_licit($_POST[inner_id])));
	$_POST[url] = "".strtolower(clean_url_licit($_POST[meta_title]))."$extend";
	
	$_POST[archive] = 0;
	
	$mysql->query_insert("licittravel_offers",$_POST);
	

	$id = mysql_insert_id();
	
	writelog("LICITTRAVEL: $CURUSER[username] added item #$id");

	$msg = "Sikeresen beillesztette!";
	
	header("Location: $returnto");
}
if($name <> '' && $editID <> '' && $_POST[pid] > 0)
{
	$returnto = $_POST[returnto];
	unset($_POST[returnto]);
	unset($_POST[url_extend]);
	
	if($_POST[buy_it_now_stop] == 0)
		$_POST[buy_it_now_stop] = $_POST[buy_it_now];
		
	$_POST[url] = strtolower(clean_url_licit($_POST[url]));
	
	if( $_POST[bid_limit] == 0)
		 $_POST[bid_limit] = 1;
		 
	$_POST[autobid_steps] = ceil(($_POST[min_price]-$_POST[start_price])/ $_POST[bid_limit]);
	$mysql->query_update("licittravel_offers",$_POST,"id=$_POST[id]");
	$msg = "Sikeresen szerkesztette!";
	
	writelog("LICITTRAVEL: $CURUSER[username] edited item #$_POST[id]");


	$id = $_POST[id];
	
	header("Location: $returnto?msg=$msg");
}

if($_GET[auto_type] == 1)
{
	$extraseries =  " AND (auto_type = 1 OR auto_type = 2)";
}
elseif($_GET[auto_type] == 2)
{
	$extraseries =  " AND auto_type = 0";
}

if($_GET[active] <> '')
{
	$active = "AND active = '$_GET[active]'";
}
else
{
	$active = '';
}
if($_GET[offer_type] <> '')
{
	$extratype = "AND offer_type = '$_GET[offer_type]'";
}
if($_GET[fieldvalue] <> '')
{
	if($_GET[field] == 'id')
		$extraid = "AND licittravel_offers.id = '$_GET[fieldvalue]'";
	else
		$extraid = "AND licittravel_offers.$_GET[field] LIKE '%$_GET[fieldvalue]%'";
}
if($_GET[from_date] <> '')
{
	$extradate = "AND licittravel_offers.to_date >= '$_GET[from_date] 00:00:00' AND  licittravel_offers.to_date <= '$_GET[to_date] 00:00:00' ";
}
if($_GET[pid] > 0)
{
	$extrapid = "AND licittravel_offers.pid = '$_GET[pid]'";
}
/*if($_GET[name] <> '')
{
	$extraname = "AND licittravel_offers.name LIKE '%$_GET[name]%'";
}*/
head("LicitTravel ajánlatok kezelése");

mysql_query("SET NAMES 'utf8'"); 

echo "<h1><a href=\"http://admin.indulhatunk.info\">Vatera adminisztrációs felület</a> &raquo; Licittravel &raquo; Ajánlatok kezelése</h1>";

?>
<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,source,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,|,bullist,numlist,|,outdent,indent|,undo,redo,|,link,unlink,anchor,image,cleanup,|,forecolor,backcolor,fullscreen,preview",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|media,advhr,||,ltr,rtl,",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,
			entity_encoding : "raw",
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>

<?
$add = $_GET[add];
$translate = $_GET[translate];


if($add == 1)
{
	$show = '';
}
else
	$show = 'display:none';
	
	
if($translate == 1)
{
	$hidetranslate = 'display:none';
}
else
	$hidetranslate = '';
?>


<script>
$(document).ready(function() {

$("#infinity").click( function(){
		if( $(this).is(':checked') ) 
		{
			$("#buy_it_now_stop").val(999999);
			$("#min_price").val($("#buy_it_now").val())
		}
		else
			$("#buy_it_now_stop").val($("#buy_it_now").val());
   });



	$("#saveoffer").click(function(){
		
		
	

	var error = 0;
		
 		$("#pform input").removeClass("invalid");


		var partnerID = $("#partnerID").val()*1;
		var oname = $("#oname").val();
		var fdate = $("#fdate").val();
		var tdate = $("#tdate").val();
		var oprice = $("#oprice").val()*1;
		var blimit = $("#blimit").val()*1;
	    var title = $("#title").val();
		
		if(partnerID == 0)
		{
			error = 1;
  			$("#partnerID").addClass('invalid');
		} 
		if(oname == '')
		{
			error = 1;
  			$("#oname").addClass('invalid');
		} 
		if(fdate == '')
		{
			error = 1;
  			$("#fdate").addClass('invalid');
		} 
		if(tdate == '')
		{
			error = 1;
  			$("#tdate").addClass('invalid');
		} 
	/*	if(oprice == 0)
		{
			error = 1;
  			$("#oprice").addClass('invalid');
		} */
		if(blimit == 0)
		{
			error = 1;
  			$("#blimit").addClass('invalid');
		} 
		if(title == '')
		{
			error = 1;
  			$("#title").addClass('invalid');
		} 
		
		if(error == 1)
		{
			alert("Töltsön ki minden mezőt!");
			return false;
		}
		else
			return true;
	});
});

</script>

<style>


</style>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?add=1&returnto=<?=urlencode($_SERVER[REQUEST_URI])?>" class='<? if($_GET[add] == 1) echo "current";?>'>Új ajánlat</a></li>


		<li><a href="ltOffers.php" class='<? if($_GET[add] <> 1 && $_GET[archive] <> 1 && $_GET[auto_type] == 0) echo "current";?>'>Összes ajánlat</a></li>

		<li><a href="ltOffers.php?auto_type=1" class='<? if($_GET[auto_type] == 1) echo "current";?>'>Sorozat</a></li>
		<li><a href="ltOffers.php?auto_type=2" class='<? if($_GET[auto_type] == 2) echo "current";?>'>Csomag</a></li>

		<li><a href="ltOffers.php?archive=1" class='<? if($_GET[archive] == 1) echo "current";?>'>Archiv ajánlat</a></li>

		
		<!--<li><a href="?active=1" class='<? if(($_GET[active] == 1 || $_GET[active] == '') && $_GET[add] <> 1) echo "current";?>'>Aktív ajánlatok</a></li>
		<li><a href="?active=3" class='<? if($_GET[active] == 3) echo "current";?>'>Szerkesztés alatt</a></li>
		<li><a href="?active=4" class='<? if($_GET[active] == 4) echo "current";?>'>Lezárt sikeres</a></li>
		<li><a href="?active=5" class='<? if($_GET[active] == 5) echo "current";?>'>Lezárt sikertelen</a></li>
		<li><a href="?active=6" class='<? if($_GET[active] == 6) echo "current";?>'>Törölt</a></li> -->

	</ul>
	<div class='clear'></div>
</div>
<div class='contentpadding'>

<?


echo message($msg."$_GET[msg]");
 if($add == 1) { ?>
<form method="post" action="ltOffers.php" id='pform'>
<input type='hidden' name='returnto' value='<?=$_GET[returnto]?>'/>
<div style="">
<fieldset  style=";width:670px;background-color:#f9f9f9;<?=$show?>">
<legend>Ajánlat</legend>
<ul>
<?
$editid = (int)$_REQUEST[editid];
if($editid > 0 )
{
	$editQr = mysql_query("SELECT * FROM licittravel_offers WHERE id = $editid");
	$editarr = mysql_fetch_assoc($editQr);
	
	if($_GET[cop] <> 1)
		echo "<input type=\"hidden\" name=\"id\" value=\"$editarr[id]\"/>";
	else
	{
		$todayDate = date("Y-m-d H:i:s");
		$editarr[from_date] = $todayDate;
		$editarr[to_date] = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s", strtotime($todayDate)) . " +10 days"));
		$editarr[comment] = '';
		$editarr[current_bidder] = '';
		$editarr[current_bid_id] = '';
		$editarr[bid_next_price] = '';
		$editarr[close_date] = '';
		$editarr[id] = '';
		
	}
}

$partnerQuery = mysql_query("SELECT pid,hotel_name FROM partners WHERE userclass < 100  order by partners.hotel_name ASC");
// AND partner_type = 'licittravel'
?>
<?
	$fd = explode(" ",$editarr[from_date]);
	$from_date = $fd[0];
	$from_time = $fd[1];
	
	$td = explode(" ",$editarr[to_date]);
	$to_date = $td[0];
	$to_time = $td[1];
?>

	<table width='100%'>
	<? if($editarr[id] > 0) { ?>
	<tr>
		<td class='lablerow'>ID:</td>
		<td colspan='3'><input type="text" disabled value='<?=$editarr[id]?>'/></td>
	</tr>
	<? } ?>
	<tr>
		<td class='lablerow'>Partner:</td>
		<td colspan='3'><select name="pid" style="width:480px;" id="partnerID">
	<option value="0">Válasszon</option>
	<?
		while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
			if($editarr[pid] == $partnerArr[pid])
				$selected = "selected";
			else
				$selected = '';
			echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name] $partnerArr[pid] </option>\n";
		}
	?>
</select></td>
	</tr>
	<tr>
		<td class='lablerow'>Alpartner:</td>
		<td colspan='3'>
			<select name="sub_pid" style="width:480px;" id="partnerID">
				<option value="0">Válasszon</option>
				<?
					$partnerQuery = mysql_query("SELECT pid,hotel_name FROM partners WHERE userclass < 100   order by partners.hotel_name ASC");

					while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
						if($editarr[sub_pid] == $partnerArr[pid])
							$selected = "selected";
						else
							$selected = '';
							echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name] $partnerArr[pid] </option>\n";
							}
							?>
			</select>
		</td>
	</tr>
	
	<tr>
		<td class='lablerow'>Ajánlat neve:</td>
		<td colspan='3'><input type="text" class="long" name="name" value="<?=$editarr[name]?>" style='width:480px;' id='oname'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Rövid neve:</td>
		<td colspan='3'><input type="text" class="long" name="short_name" value="<?=$editarr[short_name]?>" style='width:480px;'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Fizetési mód:</td>
		<td colspan='3'><input type="text" name="payment_method" value="<?=$editarr[payment_method]?>" style='width:480px'/></td>
	</tr>
	<tr>
		<td colspan='4' class='header'>
			Érvényesség
		</td>
	</tr>
	<tr>
		<td class='lablerow'>Érvényesség kezdete:</td>
		<td><input type="text"  class="maskeddate" name="from_date_date" value="<?=$from_date?>"  style='width:80px;' id='fdate'/>&nbsp;<input type="text"  name="from_date_time" value="<?=$from_time?>"  style='width:80px;'/></td>
		<td class='lablerow'>Érvényesség vége:</td>
		<td><input type="text" name="to_date_date" class="maskeddate"  value="<?=$to_date?>"  style='width:80px;' id='tdate'/>&nbsp;<input type="text"  name="to_date_time" value="<?=$to_time?>"  style='width:80px;'/></td>

	</tr>
	<? if($editarr[close_date] <> '0000-00-00 00:00:00' && $editarr[close_date] <> '') { ?>
	<tr>
		<td class='lablerow'>Lezárás időpontja:</td>
		<td colspan='3'><input type="text"class="maskeddate" id="to_date_date" value="<?=$editarr[close_date]?>"  disabled/></td>
	</tr>
	<? } ?>
	
	<tr>
		<td colspan='4' class='header'>
			Árak
		</td>
	</tr>

	<tr>
		<td class='lablerow'>Listaár:</td>
		<td align='right'><input type="text" name="orig_price" class="numeric" value="<?=$editarr[orig_price]?>" id='oprice'/> Ft</td>
		<td class='lablerow'>Induló ár:</td>
		<td align='right'><input type="text" name="start_price" class="numeric" value="<?=$editarr[start_price]?>"/> Ft</td>

	</tr>
	<tr>
		<td class='lablerow'>Azonnali ár:</td>
		<td align='right'><input type="text" name="buy_it_now" id='buy_it_now' class="numeric" value="<?=$editarr[buy_it_now]?>"/> Ft</td>
		<td class='lablerow'>Azonnali ár stop:</td>
		<td align='right'><input type="text" name="buy_it_now_stop" id='buy_it_now_stop' class="numeric" value="<?=$editarr[buy_it_now_stop]?>"/> Ft
		
		<br/><input type="checkbox" id="infinity" <? if($editarr[buy_it_now_stop] == 999999) echo "checked"; ?>><span style='font-size:10px'>csak azonnali áron vehető meg<br/>(azonnali ár = minimum ár)</span>
		</td>

	</tr>
	<tr>
		<td class='lablerow'>Minimum ár:</td>
		<td align='right'><input type="text" name="min_price" id='min_price' class="numeric" value="<?=$editarr[min_price]?>" id='blimit'/> Ft<br/><input type="checkbox" name="hidden_min_price" <? if($editarr[hidden_min_price] == 1) echo "checked" ?>/>Rejtett </td>
		<td class='lablerow'>Licitlépcső:</td>
		<td align='right'><input type="text" name="bid_limit" class="numeric" value="<?=$editarr[bid_limit]?>"/> Ft</td>

	</tr>
	
	<tr>
		<td class='lablerow'>Aktív:</td>
		<td align='right'>
			<select name="active">
				<option value="3" <? if($editarr[active]==3 || $_GET[cop] == 1) echo"selected"; ?>>Szerkesztés alatt</option>
				<option value="1" <? if($editarr[active]==1 && $_GET[cop] <> 1) echo"selected";?>>Igen</option>
				<option value="4" <? if($editarr[active]==4 && $_GET[cop] <> 1) echo"selected";?>>Lezárt sikeres</option>
				<option value="5" <? if($editarr[active]==5 && $_GET[cop] <> 1) echo"selected";?>>Lezárt sikertelen</option>
				<option value="6" <? if($editarr[active]==6 && $_GET[cop] <> 1) echo"selected";?>>Törölt</option>
			</select>
			
		</td>
		<td class='lablerow'>Mennyiség:</td>
		<td align='right'>
		<? if($editarr[auto_type] == 1)
			$editarr[current_quantity] = $editarr[auto_quantity];
		?>
			<input type="text" name="quantity" style='width:20px!important' class="numeric formCalculate2" value="<?=$editarr[quantity]?>"/> db
			<input type="text"  class="numeric formCalculate2" style='width:20px!important' value="<?=$editarr[current_quantity]?>" disabled/> db *
			
			<input type="text"  name='repeat' class="numeric formCalculate2" style='width:20px!important' value="<?=$editarr[repeat]?>"/>

		</td>
	</tr>
	<tr>
		<td colspan='4' class='header'>
			Automata
		</td>
	</tr>

		<tr>
		<td class='lablerow'>Robi:</td>
		<td align='right'>
			<select name="auto_bid">
				<option value="0" <?if($editarr[auto_bid]==0)echo"selected";?>>nem</option>
				<option value="1" <?if($editarr[auto_bid]==1)echo"selected";?>>igen</option>
			</select>			
		</td>
		<td class='lablerow'>Auto típus:</td>
		<td align='right'>
			<select name="auto_type">
				<option value="0" <?if($editarr[auto_type]==0)echo"selected";?>>Általános tétel</option>
				<!--<option value="1" <?if($editarr[auto_type]==1)echo"selected";?>>Sorozat szülő</option>
				<option value="2" <?if($editarr[auto_type]==2)echo"selected";?>>Sorozat gyermek</option>
				<option value="3" <?if($editarr[auto_type]==3)echo"selected";?>>Csomag (Hamarosan)</option>-->
			</select>
		</td>

	</tr>

	
	
	
		<tr>
		<td class='lablerow'>Prioritás:</td>
		<td align='right'>
			<input type="text" name="priority" value="<?=$editarr[priority]?>"/>			
		</td>
		<td class='lablerow'>Típus:</td>
		<td align='right'>
			<select name="offer_type">
				<option value="szallas" <?if($editarr[offer_type]=="szallas")echo"selected";?>>szállás</option>
				<option value="utazas" <?if($editarr[offer_type]=="utazas")echo"selected";?>>utazás</option>
				<option value="szolgaltatas" <?if($editarr[offer_type]=="szolgaltatas")echo"selected";?>>szolgáltatás</option>
				<option value="ettermek" <?if($editarr[offer_type]=="ettermek")echo"selected";?>>étterem</option>
				<option value="arveres" <?if($editarr[offer_type]=="arveres")echo"selected";?>>last minute</option>
			</select>
		</td>

	</tr>
	
	<tr>
		<td class='lablerow'>Kiemelés:</td>
		<td align='right'>
		<select name="special">
			<option value="0" <?if($editarr[special]==0)echo"selected";?>>Nincs</option>
			<option value="1" <?if($editarr[special]==1)echo"selected";?>>Főoldali ajánlat</option>
			</select>			
		</td>
		<td class='lablerow'>Oldalsáv:</td>
		<td align='right'>
			<select name="sidebar">
				<option value="0" <?if($editarr[sidebar]==0)echo"selected";?>>Nem</option>
				<option value="1" <?if($editarr[sidebar]==1)echo"selected";?>>Igen</option>
			</select>
		</td>
	</tr>

	
	
	<tr>
		<td colspan='4' class='header'>
			Metaadatok
		</td>
	</tr>

	<tr>
		<td class='lablerow'>Meta title:</td>
		<td colspan='3'><input type="text" name="meta_title"  value="<?=$editarr[meta_title]?>" style='width:480px;' id='title'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Meta keyword:</td>
		<td colspan='3'><input type="text" name="meta_keyword" value="<?=$editarr[meta_keyword]?>" style='width:480px;'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Meta description:</td>
		<td colspan='3'><input type="text" name="meta_description" value="<?=$editarr[meta_description]?>" style='width:480px;'/></td>
	</tr>
<tr>
		<td class='lablerow'>URL:</td>
		<td colspan='3'><input type="text" name='url' value="<?=$editarr[url]?>" style='width:385px;'/><input type="text"  name='inner_id' value='<?=$editarr[inner_id]?>' style='width:80px;'/></td>
	</tr>

	
	
	
	




	<tr>
		<td class='lablerow'>Leírás:</td>
		<td colspan='3'><textarea name="description" rows="20" cols="20" style="width: 470px" class="tinymce"><?=$editarr[description]?></textarea></td>
		
	</tr>
	<tr>
		<td class='lablerow'>Rövid leírás:</td>
		<td colspan='3'><textarea name="short_description" rows="20" cols="20" style="width: 500px" class="tinymce"><?=$editarr[short_description]?></textarea></td>
		
	</tr>
		<tr>
		<td class='lablerow'>TagCloud:</td>
		<td colspan='3'><textarea name="tagcloud" rows="10" cols="20" style="width: 500px" class="tinymce"><?=$editarr[tagcloud]?></textarea></td>
		
	</tr>
	
	<tr>
		<td colspan='4' align='center'><input type="submit" value="Mentés"  id='saveoffer'/>&nbsp;&nbsp;&nbsp;<input type="button" value="Mégsem" class='backbutton' /> </td>
	</tr>
</table>


<ul>

<? if($editarr[current_bidder] <> '') { ?>


<li><label class="required">Jelenlegi licitáló</label><input type="text"  value="<?=$editarr[current_bidder]?>" disabled/></li>
<li><label class="required">Jelenlegi licit</label><input type="text"  class="numeric" value="<?=$editarr[current_price]?>" disabled/> Ft</li>
<li><label class="required">Következő licit</label><input type="text" class="numeric" value="<?=$editarr[bid_next_price]?>" disabled/> Ft</li>

<? } ?>

<? if($editarr[parent_id] > 0)
{
$parent = mysql_fetch_assoc($mysql->query("SELECT id, quantity, auto_quantity, name  FROM licittravel_offers WHERE id = $editarr[parent_id]"));
?>
<li><label>Szülő:</label>ID: <input type="text" class="numeric formCalculate2" value="<?=$parent[id]?>" disabled/> Link: <a href='?editid=<?=$editarr[parent_id]?>&add=1' target='_blank'><?=$parent[name]?></a></li>
<li><label>Szülő mennyiség</label><input type="text" class="numeric formCalculate2" value="<?=$parent[quantity]?>" disabled/> db</li>
<li><label>Szülő elkészült m.</label><input type="text" class="numeric formCalculate2" value="<?=$parent[auto_quantity]?>" disabled/> db</li>
<? } ?>

<? if($CURUSER[userclass] >= 255) { ?>
	<textarea><?=$editarr[comment]?></textarea>
<? } ?>




</fieldset>
<div class="cleaner"></div>
</div>
</form>
<?
foot();
die;
} 
?>
<form method='GET'>
	<div style='width:250px; float:left;'>
	<input type='hidden' name='archive' value='<?=$_GET[archive]?>'/>
	<input type='hidden' name='auto_type' value='<?=$_GET[auto_type]?>'/>

	<input type='hidden' name='searchsubmit' value='1'/>
		<input type='text' name='from_date'  class='dfilter' id="lt_from_date" value='<?=$_GET[from_date]?>' style='width:90px;'/>		<input type='text' name='to_date'  class='dfilter' id="lt_to_date"  value='<?=$_GET[to_date]?>' style='width:90px;'/>
	
	</div>
	<div style='float:left;'>
	<input type='text' name='fieldvalue' value='<?=$_GET[fieldvalue]?>' id='lt_content' style='width:245px'/>
	<select name='field'>
			<option value='id'>ID</option>
			<option value='name' <? if($_GET[field] == 'name') echo "selected"?>>Név</option>
			<option value='inner_id' <? if($_GET[field] == 'inner_id') echo "selected"?>>Tételszám</option>
	</select>
	
	<select name='offer_type'>
		<option value=''>Termék típusa</option>
		<option value='utazas' <? if($_GET[offer_type] == 'utazas') echo "selected"?>>Utazás</option>
		<option value='szallas' <? if($_GET[offer_type] == 'szallas') echo "selected"?>>Szállás</option>
		<option value='ettermek' <? if($_GET[offer_type] == 'ettermek') echo "selected"?>>Étterem</option>
		<option value='szolgaltatas' <? if($_GET[offer_type] == 'szolgaltatas') echo "selected"?>>Szolgáltatás</option>
		<option value='arveres' <? if($_GET[offer_type] == 'arveres') echo "selected"?>>Last Minute</option>
	</select>
	
	<select name='active'>
		<option value=''>Összes állapot</option>
		<option value='1' <? if($_GET[active] == '1') echo "selected"?>>Aktív</option>
		<option value='3' <? if($_GET[active] == '3') echo "selected"?>>Szerkesztés</option>
		<option value='4' <? if($_GET[active] == '4') echo "selected"?>>Sikeres</option>
		<option value='5' <? if($_GET[active] == '5') echo "selected"?>>Sikertelen</option>
		<option value='6' <? if($_GET[active] == '6') echo "selected"?>>Törölt</option>

	</select>
</div>
	<div class='cleaner'></div>
	<select name='pid' style='width:799px;margin:0 auto;'>
	<option value='0'>Partner</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(hotel_name) <> '' order by hotel_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[company_name] <> '')
			{
				if($_GET[pid] == $arr[pid])	
					$selected = 'selected'; 
				else
					$selected = '';
				echo "<option value='$arr[pid]' $selected>$arr[hotel_name]</option>";
			}
		}
	?>
</select>

<input type='submit' value='Szűrés'/>
</form>
<hr/>
<?
echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td width='20'><a href='?orderby=$orderbylink&active=$_GET[active]&archive=$_GET[archive]&searchsubmit=1&from_date=$_GET[from_date]&to_date=$_GET[to_date]&fieldvalue=$_GET[fieldvalue]&field=$_GET[field]&offer_type=$_GET[offer_type]&active=$_GET[active]&pid=$_GET[pid]'>ID</a></td>";
	echo "<td><a href='?orderby=$orderbylink2&active=$_GET[active]&archive=$_GET[archive]&searchsubmit=1&from_date=$_GET[from_date]&to_date=$_GET[to_date]&fieldvalue=$_GET[fieldvalue]&field=$_GET[field]&offer_type=$_GET[offer_type]&active=$_GET[active]&pid=$_GET[pid]'>TSZ.</a></td>";
	echo "<td>Partner</td>";
	echo "<td>Ajánlat neve</td>";
	echo "<td width='70'>Állapot</td>";
	echo "<td>Sorozat</td>";	
	
	echo "<td width='70'>Jelenlegi licit</td>";	
	echo "<td>Szerkeszt</td>";
echo "</tr>";


if($_GET[archive] == 1)
{
	$qr = "SELECT licittravel_offers.*,licittravel_offers.url as page_url  FROM licittravel_offers WHERE id > 0 AND description <> ''  AND archive = 1 $extraname $extratype $extradate $extrapid  $extraid  $active $extraseries GROUP BY page_url ORDER BY $orderbySql";
 //$extratype $extradate $extrapid  $extraid $extraname $active
}
elseif($_GET[searchsubmit] == 1)
{
	$qr = "SELECT licittravel_offers.*,partners.*,licittravel_offers.url as page_url  FROM licittravel_offers INNER JOIN partners ON partners.pid = licittravel_offers.pid WHERE id > 0 $extratype $extradate $extrapid $extraseries $extraid  $active ORDER BY $orderbySql LIMIT 200";

}
else
{
		$qr = "SELECT licittravel_offers.*,partners.*,licittravel_offers.url as page_url  FROM licittravel_offers INNER JOIN partners ON partners.pid = licittravel_offers.pid WHERE id > 0 $extratype $extradate $extrapid  $extraseries $extraid $extraname AND active = 1 ORDER BY $orderbySql LIMIT 200";

}

/*
elseif($_GET[active] == 3)
{
	$qr = "SELECT licittravel_offers.*,partners.*,licittravel_offers.url as page_url  FROM licittravel_offers INNER JOIN partners ON partners.pid = licittravel_offers.pid WHERE licittravel_offers.active = 3 $extratype $extradate $extrapid  $extraid $extraname ORDER BY $orderbySql LIMIT 200";
}
elseif($_GET[active] == 6)
{
	$qr = "SELECT licittravel_offers.*,partners.*,licittravel_offers.url as page_url  FROM licittravel_offers INNER JOIN partners ON partners.pid = licittravel_offers.pid WHERE licittravel_offers.active = 6 $extratype  $extradate $extrapid  $extraid $extraname ORDER BY $orderbySql LIMIT 200";
}

elseif($_GET[active] < 4)
	$qr = "SELECT licittravel_offers.*,partners.*,licittravel_offers.url as page_url  FROM licittravel_offers INNER JOIN partners ON partners.pid = licittravel_offers.pid WHERE active = 1  AND licittravel_offers.from_date <= NOW() AND licittravel_offers.to_date > NOW()  AND licittravel_offers.active <> 3 $extratype $extradate $extrapid   $extraid $extraname ORDER BY $orderbySql LIMIT 200"; //licittravel_offers.from_date <= NOW() AND licittravel_offers.to_date > NOW()
elseif($_GET[active] == 4 || $_GET[active] == 5)
	$qr = "SELECT licittravel_offers.*,partners.*,licittravel_offers.url as page_url FROM licittravel_offers INNER JOIN partners ON partners.pid = licittravel_offers.pid WHERE id > 0 AND licittravel_offers.active = $_GET[active]  $extratype  $extraid $extraname $extradate $extrapid   ORDER BY $orderbySql";
*/

$query = mysql_query($qr); 

while($arr = mysql_fetch_assoc($query)) {

	if($arr[sub_pid] > 0)
	{
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[sub_pid] LIMIT 1"));
		$extrapartner = "<br/> $partner[hotel_name]";
	}
	else
	{
		$extrapartner = "";
	}
	
	if($arr[active] == 4)
	{
		$class = 'green';
		$status = 'Lezárt, sikeres';
	}
	elseif($arr[active] == 5)
	{
		$class = 'blue';
		$status = 'Lezárt, sikertelen';
	}
	elseif($arr[active] == 3)
	{
		$class = 'orange';
		$status = 'Szerkesztés alatt';
	}
	elseif($arr[active] == 6)
	{
		$class = 'grey';
		$status = 'Törölt';
	}
	else
	{
		$class = '';
		$status = 'Aktív';
	}
	echo "<tr class=\"$class\">";
		echo "<td align='center'>$arr[id]</td>";

		if($arr[serial] > 0)
			$serial = "-$arr[serial]";
			
		echo "<td>$arr[inner_id]$serial</td>";
		echo "<td>$arr[hotel_name] $extrapartner</td>";
		echo "<td>$arr[name]<br/>$arr[to_date]</td>";
	
		echo "<td align='center'>$status</td>";
		
		if($arr[parent_id] > 0)
			$parent = "<a href='http://admin.indulhatunk.info/ltOffers.php?editid=$arr[parent_id]&add=1&returnto=%2FltOffers.php'>$arr[parent_id]</a>";
		elseif($arr[auto_type] == 1)
			$parent = 'szülő';
		else	
			$parent = '-';
		echo "<td align='center'>$parent</td>";

		echo "<td align='right'>".formatPrice($arr[current_price])."<br/>($arr[current_bidder])</td>";
		echo "<td width='50'>
			<a href=\"?editid=$arr[id]&add=1&returnto=".urlencode($_SERVER[REQUEST_URI])."\">[szerkeszt]</a><br/>
			<a href=\"?editid=$arr[id]&cop=1&add=1&returnto=".urlencode($_SERVER[REQUEST_URI])."\">[másol]</a><br/>
			<a href='ltOffers.php?delete=$arr[id]&returnto=".urlencode($_SERVER[REQUEST_URI])."' rel='facebox'>[töröl]</a></br>
			<a href=\"http://$arr[offer_type].licittravel.hu/$arr[page_url]/preview-$arr[id]\" target='_blank'>[link]</a><br/>
		
					<a href=\"https://www.facebook.com/sharer/sharer.php?u=http://$arr[offer_type].licittravel.hu/$arr[page_url]\" target='_blank'>[facebook]</a><br/>

				
			<a href='ltCustomers.php?fieldvalue=$arr[id]&field=id'>[vásárlók]</a><br/>
			<a href='ltBids.php?offer_id=$arr[id]'>[licitek]</a><br/><a href='ltImages.php?offer=$arr[id]'>[képtár]</a></td>";
			
	echo "</tr>";
	
}
echo "</table></div></div>";

foot();
?>