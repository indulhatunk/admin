<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

head('Felhasználók');


if($CURUSER[userclass] < 1)
	header("location:index.php");
	
$edit = $_POST[editid];
$editid = (int)$_GET[edit];

$data[title] = $_POST[title];
$data[body] = $_POST[body];



if($edit == "" && $data[body] <> ""  && $data[title] <> "") {
	$mysql->query_insert("licittravel_content",$data);
	$msg = "Sikeresen felvitte a tartalmat!";
}
elseif($edit > 0) {
	$mysql->query_update("licittravel_content",$data,"id=$edit");
	$msg = "Sikeresen szerkesztette a tartalmat!";
}



if($editid > 0) {
	$query = $mysql->query("SELECT * FROM licittravel_content WHERE id = $editid");
	$editarr = mysql_fetch_assoc($query);
}


mysql_query("SET NAMES 'utf8'"); 
$qr = "SELECT * FROM licittravel_content ORDER BY title ASC";
$query = mysql_query($qr); 


?>
<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			entity_encoding : "raw",
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>

<div class="partnerForm">
	<form method="POST">
		<input type="hidden" name="editid" value="<?=$editarr[id]?>">
	<fieldset>
		<legend>Tartalom kezelése</legend>
	<ul>
		<li><label>Cím</label><input type="text" name="title" value="<?=$editarr[title]?>"/></li>
		<li><label>Tartalom</label><textarea name='body' class='tinymce' style='height:500px'><?=$editarr[body]?></textarea></li>
		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</fieldset>
	</form>
</div>
<?
echo "<hr/>";
echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td></td>";
	echo "<td>Cím</td>";
echo "</tr>";


while($arr = mysql_fetch_assoc($query)) {

	echo "<tr class=\"\">";
		echo "<td width='25' align='center'> <a href=\"?edit=$arr[id]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='25'/></a></td>";
		echo "<td>$arr[title]</td>";
	echo "</tr>";
	
}
echo "</table>";
foot();
?>