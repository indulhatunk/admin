<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

head("Éttermek képeinek kezelése");

$db = new OCI8DB;
$dbconn = $db->dbconn();

$memcached = new Memcache;
$memcached->connect('127.0.0.1', 11211) or die ("Could not connect"); //connect to memcached server

$accquery = "SELECT * FROM CATER WHERE IMAGE_NUM > 0 ORDER BY NAME ASC";
$accdata = cachedSQL($accquery);



?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?add=1" class='current'>Éttermek képei</a></li>
		</ul>
	<div class='clear'></div>
</div>
<div class='contentpadding'>

<? if($_GET[showgallery] == 1)
{
	echo "<center>";
	for($i=1;$i<=$_GET[image_num];$i++)
	{
		if($i < 10)
			$z = "0".$i;
		else
			$z = $z;
		
		echo "<a href='http://static.indulhatunk.hu/uploaded_images/cater/$_GET[partner_id]/$_GET[package_id]/orig/$z.jpg' target='_blank'><img src='http://static.indulhatunk.hu/uploaded_images/cater/$_GET[partner_id]/$_GET[package_id]/282_170/$z.jpg' style='margin:1px; '/></a>";
	}
	echo "</center>";
}
else
{
?>
<table>
<tr class='header'>
	<td width='10'>#</td>
	<td>Étterem neve</td>
	<td width='35'>-</td>
	<td width='50'>Galéria</td>
</tr>
<?
	$i=1;
	foreach($accdata as $cater)
	{
		echo "<tr>";	
			echo "<td align='center'>$i</td>";
			echo "<td>$cater[NAME]</td>";
			echo "<td align='right'>$cater[IMAGE_NUM] db</td>";
			echo "<td align='right'><a href='?showgallery=1&partner_id=$cater[PARTNER_ID]&package_id=$cater[ID]&image_num=$cater[IMAGE_NUM]'>galéria &raquo;</a></td>";
		echo "</tr>";
		$i++;
	}
?>
</table>
<? } ?>

</div></div>
<?
foot();
?>