<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($_GET[from_date] <> '')
{
	$extradate = "AND to_date >= '$_GET[from_date] 00:00:00' AND to_date <= '$_GET[to_date] 00:00:00'"; 
}
if($_GET[pid] > 0)
{
	$extrapid = "AND pid = $_GET[pid]";
}


if($_GET[active] <> '')
{
	$active = implode(" OR active = ",$_GET[active]);
	
	$active = "AND (active = $active) ";
}
else
{
	$active = " AND active >= 4 AND active <= 5";
}
	

if($_GET[orderby] == 'to_date')
{
	$orderby = "ORDER BY to_date ASC";
}
elseif($_GET[orderby] == 'status')
{
	$orderby = "ORDER BY active ASC";
}
elseif($_GET[orderby] == 'pid')
{
	$orderby = "ORDER BY pid, sub_pid ASC";
}
else
{
	$orderby = "ORDER BY pid, from_date ASC";
}

mysql_query("SET NAMES 'utf8'"); 



if($_GET[export] <> 1)
{
	head("LicitTravel statisztika");


echo message($msg);

?>

<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?closed=0" class='<?  if($_GET[closed] == 0 || $_GET[closed] == '') echo "current";?>'>Nyitott ajánlatok</a></li>
		<li><a href="?closed=1" class='<? if($_GET[closed] == 1) echo "current";?>'>Lezárt ajánlatok</a></li>
	</ul>
	<div class='clear'></div>
</div>
<div class='contentpadding'>

<form method='GET' name="form2">

	<input type='hidden' name='closed' value='<?=$_GET[closed]?>'/>
		<input type='text' name='from_date'  class='dpick' id="lt_from_date" value='<?=$_GET[from_date]?>' style='width:90px;'/> 
		<input type='text' name='to_date'  class='dpick' id="lt_to_date"  value='<?=$_GET[to_date]?>' style='width:90px;'/>
	<div class='cleaner'></div>
		<select name='pid' style='width:522px;'>
	<option value='0'>Összes partner</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10  AND partner_type = 'licittravel' order by company_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[company_name] <> '')
			{
				if($_GET[pid] == $arr[pid])	
					$selected = 'selected'; 
				else
					$selected = '';
				echo "<option value='$arr[pid]' $selected>$arr[hotel_name] |  $arr[company_name]</option>";
			}
		}
	?>
</select>
<hr/>

<? if($_GET[closed] == 1) { ?>
<select name='orderby'>
	<option value='status'>Lezárás módja szerint</option>
	<option value='owner'>Tulajdonos szerint</option>
</select>

<?
if(empty($_GET[active]))
	$act = array();
else
	$act = $_GET[active];
	
 $act = implode(",",$act);
	if (strpos($act,'4') !== false) 
		$class4 = 'checked';
	if (strpos($act,'5') !== false) 
		$class5 = 'checked';

	
?>
<ul>
	<li><input type='checkbox' name='active[]' value='4' <?=$class4?>/>Sikeres ajánlatok</li>
	<li><input type='checkbox' name='active[]' value='5' <?=$class5?>/>Sikertelen ajánlatok</li>
	<li><input type='checkbox' name='fixed' value='99'/>Azonnali ajánlatok</li>
</ul>
<? } else { ?>
<select name='orderby'>
	<option value='to_date' <? if($_GET[orderby] == 'to_date') echo "selected"; ?>>Lejárat szerinti rendezés</option>
	<option value='owner' <? if($_GET[orderby] == 'owner') echo "selected"; ?>>Tulajdonos szerinti rendezés</option>
</select>
<? } ?>
<hr/>
<div style='text-align:center'>
	<input type='submit' value='Szűrés'/>
</div>
</form>

<div style='text-align:center;margin-top:-27px;margin-left:160px;margin-bottom:15px;'>
<form method='get' name="form2">
		<input type='hidden' name='closed' value='<?=$_GET[closed]?>'/>
		<input type='hidden' name='export' value='1'/>
		<input type='hidden' name='closed' value='<?=$_GET[closed]?>'/>
		<input type='hidden' name='from_date'value='<?=$_GET[from_date]?>'/> 
		<input type='hidden' name='to_date' value='<?=$_GET[to_date]?>'/>
		<input type='hidden' name='pid' value='<?=$_GET[pid]?>'/>

	<input type='submit' value='Exportálás'/>
</form>
</div>
<?
}
else
{
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename=felhasznalok-'.date("Y-m-d").'.xls');
}


if($_GET[closed] == 0)
{
	$query = $mysql->query("SELECT * FROM licittravel_offers WHERE active = 1 AND archive = 0 $extradate $extrapid $orderby ");
}

else
{
	$query = $mysql->query("SELECT * FROM licittravel_offers WHERE id > 0 AND archive = 0  $active $extradate $extrapid $orderby ");
}	
	if(mysql_num_rows($query) == 0)	
		echo message("Nincs találat");
	
	echo "<table>";
		echo "<tr class='header'>";
			echo "<td><b>-</b></td>";
			echo "<td><b>ID</b></td>";

			echo "<td><b>Tulajdonos</b></td>";
			echo "<td><b>Ajánlat</b></td>";

			echo "<td><b>Lezárás dátuma</b></td>";
			echo "<td><b>Induló ár</b></td>";
			echo "<td><b>Utolsó licit</b></td>";
			echo "<td><b>Nyertes neve</b></td>";
			echo "<td><b>Nyertes e-mail</b></td>";
			echo "<td colspan='2'>-</td>";

		echo "</tr>";
	
	while($arr = mysql_fetch_assoc($query))
	{
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid]"));
		
		$winner = mysql_fetch_assoc($mysql->query("SELECT * FROM licittravel_customers WHERE offer_id = $arr[id]"));

		if($arr[active] == 5)
		{
			$class = 'background-color:#E56E94;';
			$st = "sikertelen";
		}
		elseif($arr[active] == '')
		{
			$class = '';
			$st = 'sikeres';
		}
		else
		{
			$st = 'sikeres';
		}
		echo "<tr style='$class'>";
			echo "<td>$st</td>";
			echo "<td align='right'><a href='http://admin.indulhatunk.hu/ltOffers.php?editid=$arr[id]&add=1&returnto=".urlencode($_SERVER[REQUEST_URI])."' target='_blank'><b>$arr[id]</b><br/>$arr[inner_id]</td>";


			echo "<td>$partner[company_name] / $partner[hotel_name]</td>";
						
			echo "<td>$arr[name]</td>";

			echo "<td>$arr[to_date]</td>";
			


			echo "<td align='right'>".formatPrice($arr[start_price],0,1)."</td>";
			echo "<td align='right'>".formatPrice($arr[current_price],0,1)."</td>";
			echo "<td align='right'>$winner[name]</td>";
			echo "<td align='right'>$winner[email]</td>";

	if($winner[name] <> '')
				$w = 'sikeres';
			else
				$w = 'függő';
			echo "<td align='center'>$w</td>";
			
			if($_GET[export] == 1)
				echo "<td align='center'><a href='http://admin.indulhatunk.hu/ltBids.php?email=&name=&offer_id=$arr[id]'>licitek</a></td>";
			else
				echo "<td align='center'><a href='/ltBids.php?email=&name=&offer_id=$arr[id]'><img src='http://admin.indulhatunk.hu/images/arrow.png' title='A tétel licitjei' alt='A tétel licitjei'/></a></td>";
		

		echo "</tr>";
	}


echo "</table>";
if($_GET[export] <> 1)
{
	echo "</div></div>";
	foot();
}
?>