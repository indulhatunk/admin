<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();


/**** bitly ****/

if($CURUSER[userclass] < 20)
	header("location:index.php");
	
if($_GET[all] == 1)
	$limit = '';
else
	$limit = 'LIMIT 50';
	
head('Hírlevelek Kezelése');

?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?type=indulhatunk" class="<? if($_GET[type] == 'indulhatunk' || $_GET[type] == '') echo "current"?>">Indulhatunk.hu</a></li>
		<li><a href="?type=outlet" class="<? if($_GET[type] == 'outlet') echo "current"?>">Outlet</a></li>		
		<!--<li><a href="?type=rajongok" class="<? if($_GET[type] == 'rajongok') echo "current"?>">Rajongók</a></li>		
		<li><a href="?type=lmb" class="<? if($_GET[type] == 'lmb') echo "current"?>">LMB</a></li>
		<li><a href="?type=ucs" class="<? if($_GET[type] == 'ucs') echo "current"?>">Ücs</a></li>-->
		<li><a href="?type=gymb" class="<? if($_GET[type] == 'gymb') echo "current"?>">Gymb</a></li>
		<li><a href="?type=wellness" class="<? if($_GET[type] == 'wellness') echo "current"?>">Wellness</a></li>
		<li><a href="?type=szep" class="<? if($_GET[type] == 'szep') echo "current"?>">SZÉP</a></li>
		<li><a href="?type=foreign" class="<? if($_GET[type] == 'foreign') echo "current"?>">Pihenni</a></li>
		<!--<li><a href="?type=reseller" class="<? if($_GET[type] == 'reseller') echo "current"?>">Viszonteladó</a></li>-->
		<li><a href="/getLealkudtuk.php" target="_blank">Lealkudtuk lista</a></li>
		<li><a href="?type=licit" class="<? if($_GET[type] == 'licit') echo "current"?>">Licittravel</a></li>
		<li><a href="?type=mostutazz" class="<? if($_GET[type] == 'mostutazz') echo "current"?>">Mostutazz</a></li>
		<li><a href="/newsletter_templates.php">Template-k</a></li>
		<li><a href="/requests.php"><b>Ajánlatkérések</b></a></li>
		

		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<?

$eid = (int)$_GET[edit];

$editid = $_POST[id];

if($_POST[facebook] == '')
	$_POST[facebook] = 0;
if($_POST[special_newsletter] == '')
	$_POST[special_newsletter] = 0;
if($_POST[analytics] == '')
	$_POST[analytics] = 0;
if($_POST[ready] == '')
	$_POST[ready] = 0;
	
if($_POST[subject] <> '' && $editid == '')
{	
	$_POST[added] = 'NOW()';
	$mysql->query_insert("newsletter",$_POST);
	
	$id = mysql_insert_id();
	
	writelog("$CURUSER[username] created a new newsletter ($id)");
	$msg = 'Sikeres felvétel!';
}

if($_POST[subject] <> '' && $editid > 0)
{

	$mysql->query_update("newsletter",$_POST,"id=$_POST[id]");
	
	$id = $_POST[id];
	
	writelog("$CURUSER[username] edited a newsletter ($id)");
	$msg = 'Sikeres szerkesztés';
}
//upload image file


echo message($msg);

$editarr = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter WHERE id = $eid"));
?>

<? if($_GET[add] == 1) { ?>
<form method="post" action="newsletter.php?type=<?=$_GET[type]?>" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?=$editarr[id]?>"/>
	<table>
	<tr>
		<td class='lablerow'>Típus</td>
		<td>
			<select name='type'>
				<option value='indulhatunk' <? if($editarr[type] == 'indulhatunk' || $_GET[type] == 'indulhatunk') echo "selected"?>>Indulhatunk</option>
				<option value='outlet' <? if($editarr[type] == 'outlet'  || $_GET[type] == 'outlet') echo "selected"?>>Outlet</option>
				<option value='rajongok' <? if($editarr[type] == 'rajongok'  || $_GET[type] == 'rajongok') echo "selected"?>>Rajongók</option>
				<option value='lmb' <? if($editarr[type] == 'lmb'  || $_GET[type] == 'lmb') echo "selected"?>>LMB</option>
				<option value='gymb' <? if($editarr[type] == 'gymb'  || $_GET[type] == 'gymb') echo "selected"?>>GYMB</option>
				<option value='wellness' <? if($editarr[type] == 'wellness'  || $_GET[type] == 'wellness') echo "selected"?>>Wellness</option>
				<option value='szep' <? if($editarr[type] == 'szep'  || $_GET[type] == 'szep') echo "selected"?>>SZÉP</option>
				<option value='ucs' <? if($editarr[type] == 'ucs'  || $_GET[type] == 'ucs') echo "selected"?>>Ucs</option>
				<option value='foreign' <? if($editarr[type] == 'foreign'  || $_GET[type] == 'foreign') echo "selected"?>>Külfold</option>
				<option value='reseller' <? if($editarr[type] == 'reseller'  || $_GET[type] == 'reseller') echo "selected"?>>Viszonteladó</option>
				<option value='licit' <? if($editarr[type] == 'licit'  || $_GET[type] == 'licit') echo "selected"?>>Licittravel</option>
				<option value='mostutazz' <? if($editarr[type] == 'mostutazz'  || $_GET[type] == 'mostutazz') echo "selected"?>>MostUtazz</option>

			</select>
		</td>
	</tr>
	<tr>
		<td class='lablerow'>Pénznem</td>
		<td>
			<select name='currency'>
				<option value='' <? if($editarr[currency] == '') echo "selected"?>>Ft</option>
				<option value='RON' <? if($editarr[currency] == 'RON') echo "selected"?>>RON</option>
				<option value='EUR' <? if($editarr[currency] == 'EUR') echo "selected"?>>EUR</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class='lablerow'>Kész?</td>
		<td>
			<input type='checkbox' name='ready' value='1' <? if($editarr[ready] == 1) echo "checked"?>>
			<!--<select name='ready'>
				<option value='0' <? if($editarr[ready] == 0) echo "selected"?>>nem</option>
				<option value='1' <? if($editarr[ready] == 1) echo "selected"?>>igen</option>
			</select>-->
		</td>
	</tr>
	

	<tr>
		<td class='lablerow'>Facebook</td>
		<td>
			<input type='checkbox' name='facebook' value='1' <? if($editarr[facebook] == 1) echo "checked"?>>

			<!--<select name='facebook'>
				<option value='0' <? if($editarr[facebook] == 0) echo "selected"?>>nem</option>
				<option value='1' <? if($editarr[facebook] == 1) echo "selected"?>>igen</option>
			</select>-->
		</td>
	</tr>
	<tr>
		<td class='lablerow'>Analytics</td>
		<td>
			<input type='checkbox' name='analytics' value='1' checked>

			<!--<select name='facebook'>
				<option value='0' <? if($editarr[facebook] == 0) echo "selected"?>>nem</option>
				<option value='1' <? if($editarr[facebook] == 1) echo "selected"?>>igen</option>
			</select>-->
		</td>
	</tr>
	<tr>
		<td class='lablerow'>GA Source</td>
		<td><input type='text' name='analytics_prefix' value='<? if($editarr[analytics_prefix] == '') echo "newsletter_$_GET[type]"; else echo $editarr[analytics_prefix]?>'/></td>
	</tr>
		<tr>
		<td class='lablerow'>GA Source suff.</td>
		<td>
			<table border='0' cellspacing='0' cellpaddding='0'>
			<tr>
			
			<? for($i=0;$i<=10;$i++) { 
				$vname = "version".$i."_name";
			echo "<td class='lablerow' style='width:20px;'>V$i</td>
				<td><input type='text' name='$vname' value='$editarr[$vname]' style='width:14px;'/></td>";
			}
			?>
				
			
			</tr>
			
			
			</table>
			
		</td>
	</tr>
<tr>
		<td class='lablerow'>GA medium</td>
		<td><input type='text' name='analytics_medium' value='<?=$editarr[analytics_medium]?>'/></td>
	</tr>
	<tr>
		<td class='lablerow'>GA campaign</td>
		<td><input type='text' name='analytics_campaign' value='<?=$editarr[analytics_campaign]?>'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Dátum</td>
		<td><input type='text' class='dpick' name='newsletter_date' value='<?=$editarr[newsletter_date]?>'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Tárgy</td>
		<td><input type='text' name='subject' value='<?=$editarr[subject]?>' style='width:700px'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Alcím #1</td>
		<td><input type='text' name='subtitle_1' value='<?=$editarr[subtitle_1]?>' style='width:700px'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Alcím #2</td>
		<td><input type='text' name='subtitle_2' value='<?=$editarr[subtitle_2]?>' style='width:700px'/></td>
	</tr>
	<tr>
		<td class='lablerow'>Alcím #3</td>
		<td><input type='text' name='subtitle_3' value='<?=$editarr[subtitle_3]?>' style='width:700px'/></td>
	</tr>
	<td class='lablerow'>Speciális? (karácsony, húsvét, stb)</td>
		<td>
						<input type='checkbox' name='special_newsletter' value='1' <? if($editarr[special_newsletter] == 1) echo "checked"?>>
<!--
			<select name='special_newsletter'>
				<option value='0' <? if($editarr[special_newsletter] == 0) echo "selected"?>>nem</option>
				<option value='1' <? if($editarr[special_newsletter] == 1) echo "selected"?>>igen</option>
			</select>
		</td> -->
	</tr>
	<tr>
		<td class='lablerow'>HTML</td>
		<td>
		<div class='cleaner'></div>

<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',
			height:300,
			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,source,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,tablecontrols",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>
		<textarea name='description' style='height:300px'><?=$editarr[description]?></textarea></td>
	</tr>
	
		<tr>
		<td class='lablerow'>HTML 2</td>
		<td>
		<div class='cleaner'></div>

		<textarea name='short_description' class='tinymce'><?=$editarr[short_description]?></textarea></td>
	</tr>


	<tr>
		<td colspan='2' align='center'><input type="submit" value="Mentés" /></td></tr>	
	</table>	
</form>
<?
foot();
die;
}

$type = $_GET[type];

if($type == '')
	$type = 'indulhatunk';

echo "<table class=\"general\">";

echo "<tr><td colspan='6' align='center'><a  href='?add=1&type=$_GET[type]'><b>Új tétel</b></a></td></tr>";
$qr = "SELECT * FROM newsletter WHERE type = '$type'  ORDER BY id DESC $limit";
$query = mysql_query($qr); 



	echo "<tr class=\"header\">";
		echo "<td>#</td>";
		echo "<td>ID</td>";
		echo "<td>Típus</td>";
		echo "<td>Dátum</td>";
		echo "<td>Tárgy</td>";
		echo "<td></td>";

	echo "</tr>";
	echo "<form method='post'>";
	
while($arr = mysql_fetch_assoc($query)) {

	if($arr[ready] == 1)
		$class = 'lightgreen';
	else
		$class = '';
		
	echo "<tr class='$class'>";
	
	if($arr[facebook] == 1)
		$fb = "<img src='/images/fb.png' width='17' style='margin:3px 0 0 0'/>";
	else
		$fb = "";
		echo "<td width='20'><a href='?edit=$arr[id]&add=1&type=$arr[type]'><img src='/images/edit.png' width='20'/></a> $fb</td>";
		echo "<td width='20' align='center'>$arr[id]</td>";
		echo "<td width='50'>$arr[type]</td>";
		echo "<td width='80' align='center'>$arr[newsletter_date]</td>";
		echo "<td>$arr[subject]</td>";
		
	
		echo "<td width='60' align='center'><a href='newsletter_items.php?nid=$arr[id]&type=$arr[type]'>[tételek]</a><br/>
		<a href='newsletter_preview.php?id=$arr[id]&new=1&version=0' target='_blank'>[előnézet]</a><br/>
		<a href='newsletter_preview.php?id=$arr[id]&new=1&version=1' target='_blank'>[1.&nbsp;változat]</a><br/>
		<strike><a href='newsletter_preview.php?id=$arr[id]&new=1&version=2' target='_blank'>[2.&nbsp;változat]</a></strike><br/>
		<a href='newsletter_preview.php?id=$arr[id]&new=1&version=3' target='_blank'>[3.&nbsp;változat]</a><br/>
		<a href='newsletter_preview.php?id=$arr[id]&new=1&version=4' target='_blank'>[1/b.&nbsp;változat]</a><br/>
		<a href='newsletter_preview.php?id=$arr[id]&new=1&version=5' target='_blank'>[3/b.&nbsp;változat]</a><br/>

	
		
		$send</td>";
	echo "</tr>";
}

/*
<a href='newsletter_preview.php?id=$arr[id]&new=1&refactor=1' target='_blank'>[refactor]</a>

if($arr[type] == 'reseller')
		$send = "<a href='/reseller-send.php?id=$arr[id]' rel='facebox'>[kiküld]</a>";
	else
		$send = "<a href='/reseller-send.php?id=$arr[id]&test=1' rel='facebox'>[teszt]</a>";
	
	 <a href='newsletter_preview.php?id=$arr[id]&shorten=1&showstats=1' target='_blank'>[statisztika]</a> 	
*/
echo "</table>
<center><a href='?type=$_GET[type]&all=1'>Mindet mutat &raquo;</a></center>";

if($CURUSER[userclass] > 30) { 
?>

<br/><br/>
<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showTabs=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;hl=hu&amp;bgcolor=%23FFFFFF&amp;src=9gp2dbd75cinl4na04bpnc06ts%40group.calendar.google.com&amp;color=%232952A3&amp;ctz=Europe%2FBudapest" style=" border-width:0 " width="875" height="600" frameborder="0" scrolling="no"></iframe>
<?

}
echo "</div></div>";
foot();
?>