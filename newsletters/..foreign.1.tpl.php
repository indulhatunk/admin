<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type='text/css'>
		body, html { padding:0; margin:0; font-family:arial; color:#707170; font-size:12px;}
		a img { border:none; }
		a { color:black; }
	</style>
	<title>[@title]</title>
</head>
 <body bgcolor='#cecece'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#d3dee0' style='width: 100%;'>
     <tr>
        <td align='center'>
                    
	<div style='width:760px;margin:0 auto;text-align:left;'>
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white'>
	<tr>
		<td bgcolor='#d3dee0' height='20'><div style='text-align:center;'><a href='http://nl.lmb.hu/?id=[@id]' target='_blank' style='font-size:10px;color:#707170;text-decoration:none'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a></div></td>
	</tr>
	<tr>
		<td>
			<a href='http://www.pihenni.hu/?utm_source=newsletter_logo&utm_medium=[@nldate]' target='_blank'><img alt='' title='' src='http://www.pihenni.hu/images/nowheader.jpg' style='display:block;'/></a>
		</td>
	</tr>
	<tr>
	<td align='center'>
	<!-- content -->
	<div style='border-left:3px solid #aeaeae; border-right:3px solid #aeaeae;width:754px;'>
	

		<table width='754' cellpadding='0' cellspacing='0' border='0'>
		<tr>
			<td height='1'></td>
		</tr>
		<tr>
			<td height='35' width='150' valign='middle' bgcolor='#0e699a' align='center'><a target='_blank' href='http://www.pihenni.hu/buszos+utazasok+utak+buszos+kirandulasok+1?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_bus' style='font-size:13px;color:white; text-decoration:none;'>BUSZOS UTAZÁSOK</a></td>
			<td width='1'></td>
			<td height='35' width='150' valign='middle' bgcolor='#0e699a' align='center'><a target='_blank' href='http://www.pihenni.hu/repulos+utazasok+1?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_plane' style='font-size:13px;color:white; text-decoration:none;'>REPÜLŐS UTAZÁSOK</a></td>
			<td width='1'></td>
			<td height='35' width='150' valign='middle' bgcolor='#0e699a' align='center'><a target='_blank' href='http://www.pihenni.hu/egzotikus+utazasok+1?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_exotic' style='font-size:13px;color:white; text-decoration:none;'>EGZOTIKUS UTAK</a></td>
			<td width='1'></td>
			<td height='35' width='150' valign='middle' bgcolor='#0e699a' align='center'><a target='_blank' href='http://www.pihenni.hu/korutazasok+1?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_roundtrip' style='font-size:13px;color:white; text-decoration:none;'>KÖRUTAZÁSOK</a></td>
			<td width='1'></td>
				<td height='35' width='150' valign='middle' bgcolor='#0e699a' align='center'><a target='_blank' href='http://www.pihenni.hu/varoslatogatasok+1?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_city' style='font-size:13px;color:white; text-decoration:none;'>VÁROSLÁTOGATÁSOK</a></td>

		</tr>
		</table>
	
		<table cellpadding='0' cellspacing='0' border='0' width='700'>
					<tr>
						<td height='20'></td>
					</tr>
					
					<tr>
						<td style='font-size:30px; font-weight:bold;color:#106b9c; text-align:center;'>[@title]</td>
					</tr>
					<tr>
						<td height='15'></td>
					</tr>
					<tr>
						<td style='font-size:14px; font-weight:normal;color:#252525; text-align:center;'>	
							[@short_description]
						</td>
					</tr>
					<tr>
						<td height='15'></td>
					</tr>
					<tr>
						<td height='3' bgcolor='#cdcecd'></td>
					</tr>
					<tr>
						<td height='15'></td>
					</tr>
					<tr>
						<td>
							<!-- -->
							<table cellpadding='0' cellspacing='0' border='0' width='700' bgcolor='#e9f4f6'>
								<tr>
									<td width='395'><a href='[@offer1_url]'><img src='[@offer1_image]' width='395' height='375' alt='[@offer1_title]'/></a></td>
									<td width='20'></td>
									<td valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' bgcolor='#e9f4f6' width='265'>
											<tr>
												<td height='20'></td>
											</tr>
											<tr>
												<td height='40' valign='top'><div style='font-size:18px; color:white; font-weight:bold; background-color:#106b9c;padding:5px;display:inline'>[@offer1_city]</div></td>
											</tr>
											<tr>
												<td height='40' valign='top'><a href='[@offer1_url]' target='_blank' style='font-size:21px; color:#252525; text-decoration:none;font-weight:bold;'>[@offer1_title]</a></td>
											</tr>
											<tr>
												<td>[@offer1_short_description]</td>
											</tr>
											<tr>
												<td height='15'></td>
											</tr>
											<tr>
												<td>[@offer1_description]</td>
											</tr>								
											<tr>
												<td height='30'></td>
											</tr>
											<tr>
												<td align='center'><a href='[@offer1_url]'><img src='http://img.hotel-world.hu/images/more2.jpg' alt='Tovább az ajánlathoz'/></a></td>
											</tr>
										</table>
										
									</td>
								</tr>
							</table>
							<!-- -->
						</td>
					</tr>
					<!-- -->
					<tr>
						<td height='20'></td>
					</tr>
					<tr>
						<td>
							<table cellpadding='0' cellspacing='0' border='0' width='700'>
								<tr>
									<td><img src='http://www.pihenni.hu/images/plane.jpg' alt='Repülős utak'/></td>
									<td width='10'></td>
									<td style='font-size:30px; font-weight:normal;color:#3f3f3f;' valign='bottom' width='320' align='left'>REPÜLŐS UTAZÁS</td>
									
									<td width='10'></td>
									<td width='400'><div style='border-bottom:3px solid #cdcecd;height:23px;'></div></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height='10'></td>
					</tr>
					<tr>
						<td>
								<table cellpadding='0' cellspacing='0' border='0' width='700'>
								<tr>	
									<td width='215'  valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer2_url]'><img alt='[@offer2_title]' title='' src='[@offer2_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#fe5a00;padding:3px;display:inline'>[@offer2_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td><a target='_blank' href='[@offer2_url]' style='color:#;font-size:18px; text-decoration:none;'>[@offer2_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100' valign='top'>[@offer2_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer2_url]'><img src='http://img.hotel-world.hu/images/more4.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>
									<td width='35'></td>
										<td width='215'  valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer3_url]'><img alt='[@offer3_title]' title='' src='[@offer3_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#fe5a00;padding:3px;display:inline'>[@offer3_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td><a target='_blank' href='[@offer3_url]' style='color:#;font-size:18px; text-decoration:none;'>[@offer3_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100' valign='top'>[@offer3_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer3_url]'><img src='http://img.hotel-world.hu/images/more4.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>
									<td width='35'></td>
										<td width='215' valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer4_url]'><img alt='[@offer4_title]' title='' src='[@offer4_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#fe5a00;padding:3px;display:inline'>[@offer4_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td><a target='_blank' href='[@offer4_url]' style='color:#;font-size:18px; text-decoration:none;'>[@offer4_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100' valign='top'>[@offer4_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer4_url]'><img src='http://img.hotel-world.hu/images/more4.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>

								</tr>
								</table>
						</td>
					</tr> 
					<tr>
						<td height='20'></td>
					</tr>
								<!-- -->
					
					</table>
					
					<table cellpadding='0' cellspacing='0' border='0' width='754'  bgcolor='#84bc00'>
					<tr>
						<td align='center'>
						
						<table cellpadding='0' cellspacing='0' border='0' width='700'  bgcolor='#84bc00'>
						<tr>
						<td height='10'></td>
					</tr>
						<tr>
						<td>
							<table cellpadding='0' cellspacing='0' border='0' width='700'>
								<tr>
									<td><img src='http://www.pihenni.hu/images/lastminute.jpg' height='37' alt='Városlátogatások'/></td>
									<td width='10'></td>
									<td style='font-size:30px; font-weight:normal;color:white;' valign='bottom' width='220' align='left'>VÁROSLÁTOGATÁS</td>
									
									<td width='10'></td>
									<td width='498'><div style='border-bottom:3px solid white;height:23px;'></div></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height='10' bgcolor='#84bc00'></td>
					</tr>
					<tr>
						<td bgcolor='#84bc00'>
								<table cellpadding='0' cellspacing='0' border='0' width='700'>
								<tr>	
									<td width='215'  valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer8_url]'><img alt='[@offer8_title]' title='' src='[@offer8_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#106b9c;padding:3px;display:inline'>[@offer8_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td><a target='_blank' href='[@offer8_url]' style='color:white;font-size:18px; text-decoration:none;'>[@offer8_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100' style='color:white' valign='top'>[@offer8_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer8_url]'><img src='http://www.pihenni.hu/images/more5.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>
									<td width='35'></td>
										<td width='215'  valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer9_url]'><img alt='[@offer9_title]' title='' src='[@offer9_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#106b9c;padding:3px;display:inline'>[@offer9_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td><a target='_blank' href='[@offer9_url]' style='color:white;font-size:18px; text-decoration:none;'>[@offer9_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100'  style='color:white' valign='top'>[@offer9_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer9_url]'><img src='http://www.pihenni.hu/images/more5.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>
									<td width='35'></td>
										<td width='215' valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer10_url]'><img alt='[@offer10_title]' title='' src='[@offer10_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#106b9c;padding:3px;display:inline'>[@offer10_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td><a target='_blank' href='[@offer10_url]' style='color:white;font-size:18px; text-decoration:none;'>[@offer10_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100'  style='color:white' valign='top'>[@offer10_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer10_url]'><img src='http://www.pihenni.hu/images/more5.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>

								</tr>
								</table>
						</td>
					</tr> 
					<tr>
						<td height='10' bgcolor='#84bc00'></td>
					</tr>
					
					</table>
						</td>
					</tr>
					</table>
					<table cellpadding='0' cellspacing='0' border='0' width='700'>

							<!-- -->
					<tr>
						<td height='20'></td>
					</tr>
					<tr>
						<td>
							<table cellpadding='0' cellspacing='0' border='0' width='700'>
								<tr>
									<td><img src='http://www.pihenni.hu/images/busicon.jpg' alt='Buszos utak'/></td>
									<td width='10'></td>
									<td style='font-size:30px; font-weight:normal;color:#3f3f3f;' valign='bottom' width='320' align='left'>BUSZOS UTAZÁSOK</td>
									
									<td width='10'></td>
									<td width='400'><div style='border-bottom:3px solid #cdcecd;height:23px;'></div></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height='10'></td>
					</tr>
					<tr>
						<td>
								<table cellpadding='0' cellspacing='0' border='0' width='700'>
								<tr>	
									<td width='215'  valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer5_url]'><img alt='[@offer2_title]' title='' src='[@offer5_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#106b9c;padding:3px;display:inline'>[@offer5_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td  height='40'><a target='_blank' href='[@offer5_url]' style='color:#;font-size:18px; text-decoration:none;'>[@offer5_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100' valign='top'>[@offer5_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer5_url]'><img src='http://img.hotel-world.hu/images/more3.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>
									<td width='35'></td>
										<td width='215'  valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer6_url]'><img alt='[@offer6_title]' title='' src='[@offer6_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#106b9c;padding:3px;display:inline'>[@offer6_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td  height='40'><a target='_blank' href='[@offer6_url]' style='color:#;font-size:18px; text-decoration:none;'>[@offer6_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100' valign='top'>[@offer6_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer6_url]'><img src='http://img.hotel-world.hu/images/more3.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>
									<td width='35'></td>
										<td width='215' valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='215'>
										<tr>
										<td>
											<a target='_blank' href='[@offer7_url]'><img alt='[@offer7_title]' title='' src='[@offer7_image]' width='215' height='150'/></a>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
										<td>
											<div style='font-size:14px; color:white; font-weight:bold; background-color:#106b9c;padding:3px;display:inline'>[@offer7_city]</div>
										</td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
										<tr>
											<td height='40'><a target='_blank' href='[@offer7_url]' style='color:#;font-size:18px; text-decoration:none;'>[@offer7_title]</a></td>
										</tr>
										<tr>
											<td height='10'></td>
										</tr>
											<tr>
											<td height='100' valign='top'>[@offer7_description]</td>
										</tr>
										<tr>
											<td height='10' align='center'><a target='_blank' href='[@offer7_url]'><img src='http://img.hotel-world.hu/images/more3.jpg' alt=''/></a></td>
										</tr>
										<tr>
											<td></td>
										</tr>
										</table>
									</td>

								</tr>
								</table>
						</td>
					</tr> 
					
		</table>
		
	
		
							
							
						<table cellpadding='0' cellspacing='0' border='0' width='700'>
					<tr>
						<td height='20'></td>
					</tr>
					<!-- -->
					
					<tr>
						<td style='font-size:24px; ;color:#252525;' colspan='3'>
								További ajánlataink
				</td>
				</tr>


				</table>
				
				

	</div>
	<!-- content -->
	</td>
	
	</tr>
	<tr>
		<td>
			<img alt='' title='' src='http://www.pihenni.hu/images/nowsep.jpg' style='display:block;'/>
		</td>
	</tr>
	<tr>
		<td bgcolor='#e9f4f6'>
	
	[@description]
	
	

		</td>
	</tr>
			<tr>
		<td>
			<img alt='' title='' src='http://www.pihenni.hu/images/nowfooter.jpg' style='display:block;'/>
		</td>
	</tr>
	
	
	</table>
	</div>
	
	
	<!-- -->
	
		
	<div style='width:330px; margin-left:255px; margin-top:10px;'>
			
										<table width='330' border='0' cellspacing='0' cellpadding='0'>
			<tr>
			
			<td width='25'><img src='http://img.hotel-world.hu/images/fb.png' align='middle' width='20' alt='Látogass meg minket Facebookon'/></td>
			<td><a href='http://facebook.com/pihenni' target='_blank' style='text-decoration:none'>Csatlakozz oldalunkhoz a <b>Facebook</b>-on!</a></td>
			</tr></table>
			
	
	</div>
	

	<!-- -->
	
	<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#707170; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#707170;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:#707170;text-align:center;font-size:11px;padding:10px 0 0 0'>Minden jog fenntartva &copy;</div>
</td>
</tr>
	
</table>
</body></html>