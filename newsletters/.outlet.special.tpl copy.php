<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type="text/css">
		body, html { padding:0; margin:0; font-family:arial; color:#3c3d3e; font-size:12px;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
	</style>
	<title>Szállás Outlet napi ajánlat</title>
</head>
 <body bgcolor='#f2ebd9'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#f2ebd9' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:760px;margin:0 auto;'>
	
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white'>
	<tr>
		<td bgcolor='#f2ebd9'><div style='text-align:center;margin:10px'><a href='http://nl.lmb.hu/?id=[@id]' target='_blank' style='font-size:10px;color:#3c3d3e;'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a></div></td>
	</tr>
	
		<tr>
			<td height='159'><a href='http://szallasoutlet.hu/?utm_source=newsletter_logo'><img style='display:block' src='http://indulhatunk.com/images/daily-header.png' alt='Óriási kedvezmények belföldi és külföldi szállodákban: szallasoutlet.hu - vatera.hu - teszvesz.hu - lealkudtuk.hu' title='Óriási kedvezmények belföldi és külföldi szállodákban: szallasoutlet.hu - vatera.hu - teszvesz.hu - lealkudtuk.hu'/></a></td>
		</tr>
		
			<tr>
			<td style='padding:10px'>
				<table  width='740' cellpadding='5' cellspacing='0' border='0'>
					<tr bgcolor='#940c00' style='color:white;font-weight:normal; '>
						<td>Szálláshely</td>
						<td></td>
						<td>Kedv.</td>
						<td>Kedvezményes ár</td>
						<td align='center'>Megnézem</td>
					</tr>
					
					
					<tr bgcolor='#f2ebd9' style='color:white;font-weight:bold; '>
						<td><a href='[@offer2_url]' style='color:#494a4b;text-decoration:none;' target='_blank'>[@offer2_title]</a></td>
						<td align='left' style='color:#494a4b;font-weight:normal;'>[@offer2_city]</td>
						<td align='center'><span style='color:#940c00;'>[@offer2_percent]%</span></td>
						<td align='center' style='color:#494a4b;font-weight:normal;'><strike style='color:#494a4b;font-weight:normal'>[@offer2_origprice]</strike> helyett<br/><span style='color:#940c00;font-weight:bold;'>[@offer2_price]</span></td>
						<td align='center'><a href='[@offer2_url]' target='_blank'><img src='http://indulhatunk.com/images/outlets.png' alt='tovább az oldalra'/></a> <a href='[@offer2_url2]' target='_blank'><img src='http://indulhatunk.com/images/lealkudtuks.png' alt='Megnézem a Lealkudtuk.hu-n' title='Megnézem a Lealkudtuk.hu-n'/></a></td>
					</tr><tr bgcolor='#f8f5ec' style='color:white;font-weight:bold; '>
						<td><a href='[@offer3_url]' style='color:#494a4b;text-decoration:none;' target='_blank'>[@offer3_title]</a></td>
						<td align='left' style='color:#494a4b;font-weight:normal;'>[@offer3_city]</td>
						<td align='center'><span style='color:#940c00;'>[@offer3_percent]%</span></td>
						<td align='center' style='color:#494a4b;font-weight:normal;'><strike style='color:#494a4b;font-weight:normal'>[@offer3_origprice]</strike> helyett<br/><span style='color:#940c00;font-weight:bold;'>[@offer3_price]</span></td>
						<td align='center'><a href='[@offer3_url]' target='_blank'><img src='http://indulhatunk.com/images/outlets.png' alt='tovább az oldalra'/></a> <a href='[@offer3_url2]' target='_blank'><img src='http://indulhatunk.com/images/lealkudtuks.png' alt='Megnézem a Lealkudtuk.hu-n' title='Megnézem a Lealkudtuk.hu-n'/></a></td>
					</tr><tr bgcolor='#f2ebd9' style='color:white;font-weight:bold; '>
						<td><a href='[@offer4_url]' style='color:#494a4b;text-decoration:none;' target='_blank'>[@offer4_title]</a></td>
						<td align='left' style='color:#494a4b;font-weight:normal;'>[@offer4_city]</td>
						<td align='center'><span style='color:#940c00;'>[@offer4_percent]%</span></td>
						<td align='center' style='color:#494a4b;font-weight:normal;'><strike style='color:#494a4b;font-weight:normal'>[@offer4_origprice]</strike> helyett<br/><span style='color:#940c00;font-weight:bold;'>[@offer4_price]</span></td>
						<td align='center'><a href='[@offer4_url]' target='_blank'><img src='http://indulhatunk.com/images/outlets.png' alt='tovább az oldalra'/></a> <a href='[@offer4_url2]' target='_blank'><img src='http://indulhatunk.com/images/lealkudtuks.png' alt='Megnézem a Lealkudtuk.hu-n' title='Megnézem a Lealkudtuk.hu-n'/></a></td>
					</tr>
					

				</table>
			</td>
		</tr>


		<tr  bgcolor='#f2ebd9'>
			<td style='height:15px;'></td>
		</tr>
		<tr>
		<td style='text-align:center'>
		
			<div style='margin:20px 0 5px 0; text-align:center;font-size:16px;font-weight:bold;'>MAI AJÁNLATUNK:<br/> <span style='color:#940c00;font-weight:bold;font-size:24px;'>[@offer1_city]</span></div>
			
			

			<div style='text-align:center;margin:15px 0 0 0;'><a href='[@offer1_url]' title='[@offer1_title]'><img src='http://indulhatunk.com/images/pic.php?percent=[@offer1_percent]&amp;imgurl=[@offer1_image]&amp;big=1&amp;ext=.jpg' width='710' alt='[@offer1_title]' title='[@offer1_title]'/></a></div>
			
			<div style='color:#940c00;font-weight:bold;font-size:24px;text-transform:uppercase;border-bottom:1px solid #cbcbcb;width:710px;margin:20px auto; padding:0 0 20px 0;'>[@offer1_title]</div>
			<div style='width:710px; margin:0 auto;text-align:left;'>
			
			[@offer1_description]

			</div>
			
			
			<div style='margin:10px 20px 10px 20px;padding:10px;border-bottom:1px solid #cbcbcb;border-top:1px solid #cbcbcb;'><center><b>OTP, MKB és K&H SZÉP kártyát is elfogadunk!</b></center></div>
		
		
			
			<div style='text-align:center;padding:10px;'>
				Megnézem a <a href='[@offer1_url]' style='color:#940c00;text-decoration:underline;font-weight:bold;'>SzállásOutlet</a> és a <a href='[@offer1_url2]' style='color:#940c00;text-decoration:underline;font-weight:bold;'>Lealkudtuk</a> oldalán.
			</div>
		</td>
		
		</tr>
		
	
		<tr>
			<td><center><a href='[@offer1_url]'><img src='http://indulhatunk.com/images/daily-outlet.png' alt='Megnézem a SzállásOutleten'/></a><a href='[@offer1_url2]'><img src='http://indulhatunk.com/images/daily-lealkudtuk.png' alt='Megnézem a Lealkudtuk.hu-n'/></a></center></td>
		</tr>
	
		<tr>
			<td align='center'><div style='padding:10px 0 0 0;font-size:10px'>&nbsp;</div></td>
		</tr>
		
			<tr  bgcolor='#f2ebd9'>
			<td style='height:15px;'></td>
		</tr>

		
				<tr>
			<td style='padding:10px'>
				<table  width='740' cellpadding='5' cellspacing='0' border='0'>
					<tr bgcolor='#940c00' style='color:white;font-weight:normal; '>
						<td>Szálláshely</td>
						<td></td>
						<td>Kedvezmény</td>
						<td>Kedvezményes ár</td>
						<td align='center'>Megnézem</td>
					</tr>
					
					
					<tr bgcolor='#f2ebd9' style='color:white;font-weight:bold; '>
						<td><a href='[@offer5_url]' style='color:#494a4b;text-decoration:none;' target='_blank'>[@offer5_title]</a></td>
						<td align='left' style='color:#494a4b;font-weight:normal;'>[@offer5_city]</td>
						<td align='center'><span style='color:#940c00;'>[@offer5_percent]%</span></td>
						<td align='center' style='color:#494a4b;font-weight:normal;'><strike style='color:#494a4b;font-weight:normal'>[@offer5_origprice]</strike> helyett<br/><span style='color:#940c00;font-weight:bold;'>[@offer5_price]</span></td>
						<td align='center'><a href='[@offer5_url]' target='_blank'><img src='http://indulhatunk.com/images/outlets.png' alt='tovább az oldalra'/></a> <a href='[@offer5_url2]' target='_blank'><img src='http://indulhatunk.com/images/lealkudtuks.png' alt='Megnézem a Lealkudtuk.hu-n' title='Megnézem a Lealkudtuk.hu-n'/></a></td>
					</tr><tr bgcolor='#f8f5ec' style='color:white;font-weight:bold; '>
						<td><a href='[@offer6_url]' style='color:#494a4b;text-decoration:none;' target='_blank'>[@offer6_title]</a></td>
						<td align='left' style='color:#494a4b;font-weight:normal;'>[@offer6_city]</td>
						<td align='center'><span style='color:#940c00;'>[@offer6_percent]%</span></td>
						<td align='center' style='color:#494a4b;font-weight:normal;'><strike style='color:#494a4b;font-weight:normal'>[@offer6_origprice]</strike> helyett<br/><span style='color:#940c00;font-weight:bold;'>[@offer6_price]</span></td>
						<td align='center'><a href='[@offer6_url]' target='_blank'><img src='http://indulhatunk.com/images/outlets.png' alt='tovább az oldalra'/></a> <a href='[@offer6_url2]' target='_blank'><img src='http://indulhatunk.com/images/lealkudtuks.png' alt='Megnézem a Lealkudtuk.hu-n' title='Megnézem a Lealkudtuk.hu-n'/></a></td>
					</tr>
					
					<!--<tr bgcolor='#f2ebd9' style='color:white;font-weight:bold; '>
						<td><a href='[@offer7_url]' style='color:#494a4b;text-decoration:none;' target='_blank'>[@offer7_title]</a></td>
						<td align='left' style='color:#494a4b;font-weight:normal;'>[@offer7_city]</td>
						<td align='center'><span style='color:#940c00;'>[@offer7_percent]%</span></td>
						<td align='center' style='color:#494a4b;font-weight:normal;'><strike style='color:#494a4b;font-weight:normal'>[@offer7_origprice]</strike> helyett<br/><span style='color:#940c00;font-weight:bold;'>[@offer7_price]</span></td>
						<td align='center'><a href='[@offer7_url]' target='_blank'><img src='http://indulhatunk.com/images/outlets.png' alt='tovább az oldalra'/></a> <a href='[@offer7_url2]' target='_blank'><img src='http://indulhatunk.com/images/lealkudtuks.png' alt='Megnézem a Lealkudtuk.hu-n' title='Megnézem a Lealkudtuk.hu-n'/></a></td>
					</tr>-->
					

				</table>
			</td>
		</tr>




	<tr>
			<td>
			
			
			<div style='width:330px; margin-left:215px; margin-top:10px;'>
			
										<table width='330' border='0' cellspacing='0' cellpadding='0'>
			<tr>
			
			<td width='25'><img src='http://img.hotel-world.hu/images/fb.png' align='middle' alt='Csatlakozz hozzánk Facebook-on' width='20'/></td>
			<td><a href='http://facebook.com/szallas.Outlet' target='_blank' style='text-decoration:none'>Csatlakozz oldalunkhoz a <b>Facebook</b>-on, lájkolj minket!</a></td>
			</tr></table>
			
			</div></td>
		</tr>

		<tr>
			<td height='20'></td>
		</tr>
	
			<tr  bgcolor='#f2ebd9'>
			<td style='height:30px;'>
				
					<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#3c3d3e; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#3c3d3e;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:#3c3d3e;text-align:center;font-size:11px;padding:5px 0 0 0'>Minden jog fenntartva &copy; </div>
	[@track]



				
			</td>
		</tr>


	

	</table>
	</div>
	
	
</td>
</tr>
	
</table>


</body></html>