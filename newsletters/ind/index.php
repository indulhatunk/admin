<? 

header("Content-Type: text/html; charset=UTF-8");

$smallitem = "	<div style='width:340px;padding:0;background-color:#f1f5f6; margin:0 7px 0 -4px '>

	
						<div style='padding:10px '><a href='#' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;'>Mesés Shiraz Hotel****</a></div>

					<a href='#'><img src='images/smallimage.jpg' style='margin:0 0 0 -4px;'/></a>
					
					<div style='font-size:12px;padding:10px;height:44px;'>
						A Mesés Shiraz Hotel a legendás „keleti kényelem” élvezetét nyújtja, egyedülállóan hazánkban, Egerszalókon. 
					</div>
					
						<div style='padding:5px 10px 10px 0;  text-align:right;'>
							<b>6 nap 5 éjszaka, félpanzió:</b>
		 			
							<div style='font-size:16px;font-weight:bold;padding:5px 0 0 0;'><span style='font-size:30px;color:#79ab00'>96 000 Ft</span>-tól</div>

						</div>

	
					</div>";

$linkrow = "	<tr>
			<td><div style='border-bottom:1px solid #cccfd1;padding:5px 5px 5px 0;'><a href='#' style='text-decoration:none;color:#b9bcbe;'>Akciós szállások &raquo;</a></div></td>
			<td><div style='border-bottom:1px solid #cccfd1;padding:5px 5px 5px 0;margin:0 10px 0 10px'><a href='#' style='text-decoration:none;color:#b9bcbe;'>Akciós szállások &raquo;</a></div></td>
			<td><div style='border-bottom:1px solid #cccfd1;padding:5px 5px 5px 0;'><a href='#' style='text-decoration:none;color:#b9bcbe;'>Akciós szállások &raquo;</a></div></td>
		</tr>	";
		
		
$xsmallitem = "	<div style='width:161px;padding:0;background-color:#f1f5f6; margin:0 14px 0 0'>

	
						<div style='padding:10px '><a href='#' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;'>Mesés Shiraz Hotel****</a></div>

					<a href='#'><img src='images/xsmall.jpg' /></a>
					
					
						<div style='padding:5px 10px 10px 10px;  text-align:right;'>
							<b>6 nap 5 éjszaka, félpanzió:</b>
		 			
							<div style='font-size:14px;font-weight:bold;padding:5px 0 0 0;'><span style='font-size:22px;color:#79ab00'>96 000 Ft</span>-tól</div>

						</div>

					</div>";


	// message
	$content = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
	<style>
		body, html { padding:0; margin:0; font-family:arial; color:#2b2b2b; font-size:12px;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
	</style>
	<title>Indulhatunk.hu - A hét legjobb ajánlatai</title>
</head>
 <body bgcolor='#f2f5f7'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#f2f5f7' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:760px;margin:0 auto;'>
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white'>
		<tr>
			<td height='132'>
				<a href='http://indulhatunk.hu'><img style='display:block' src='images/header.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: szallasoutlet.hu - vatera.hu - teszvesz.hu - lealkudtuk.hu' 
					title='Óriási kedvezmények hazai szállodákban: szallasoutlet.hu - vatera.hu - teszvesz.hu - lealkudtuk.hu' width='760'/></a>
			</td>
		</tr>
		<tr>
		<td>
		                   
	
		<div style='width:700px; padding:10px 30px 10px 30px;'>
			<!-- -->
			 	<div style='background-color:#f1f5f6; padding:20px 0 20px 0;'>
			 	
			 	<div style='padding:0 0 0 20px;'>
		 			<a href='#' style='font-size:30px;color:#007bab;text-decoration:none; display:block;'>Mesés Shiraz Hotel</a>
		 			<div style='font-size:20px;color:#2b2b2b; font-weight:bold;'>Egerszalók</div>
		 		</div>
		 		
		 		<a href='#'><img src='images/bigimage.jpg' style='margin:10px 0 10px -4px'/></a>
		 		
		 		<div style='padding:10px 20px 0 20px;color:#2b2b2b;font-weight:bold;font-size:12px;'>
		 			A Mesés Shiraz Hotel a legendás „keleti kényelem” élvezetét nyújtja, egyedülállóan hazánkban, Egerszalókon. ÜNNEPI CSOMAG 4 éj!
		 			
		 			<div style='text-align:right;padding:10px 0 0 0; display:none;'>
		 			
		 			6 nap 5 éjszaka, félpanzió:
		 			
		 			<div style='font-size:18px;font-weight:bold;padding:10px 0 0 0;'><span style='font-size:30px;color:#79ab00'>96 000 Ft</span>-tól</div>
		 			</div>
		 		</div>
		 		

		</div>
		</td>
		
		</tr>
	
		<tr>
			<td>
			
			
			
			
			<div style='margin:0 0px 0 30px;'>
			<div style='background-color:#007bab;color:white;padding:7px; margin: 0 0 5px 0; width:687px; font-size:18px;'>A HÉT AJÁNLATAI:</div>
				<table>
					<tr>
						<td>
							$smallitem
						</td>
						<td>
							<div style='margin:0 0 0 15px'>$smallitem</div>
						</td>
					</tr>
				</table>	
			</div>
			
				<div style='height:10px'></div>
			</td>
		</tr>
		
		
			<tr>
			<td>
			<div style='margin:0 0px 0 30px;'>
			
				<table>
					<tr>
						<td>
							$xsmallitem
						</td>
						<td>
							$xsmallitem
						</td>
						<td>
							$xsmallitem
						</td>
						<td>
							$xsmallitem
						</td>
					</tr>
				</table>	
			</div>
			
				<div style='height:30px'></div>
			</td>
		</tr>

		
	</table>
	

	</div>
	
	
	<div style='margin:0 auto;width:760px;color:#b9bcbe;text-align:left;'>
	
	<div style='height:30px;font-size:11px;padding:10px 0 0 0;font-size:18px;font-style:italic'>Tekintsd meg további ajánlatainkat: </div>
	
	<table width='700' style='margin:0 auto;'>
		$linkrow
		$linkrow
		$linkrow
		$linkrow
		$linkrow
		$linkrow

	

	</table>
	</div>
	<div style='height:30px;color:#534741;text-align:center;font-size:11px;padding:10px 0 0 0'>Minden jog fenntartva &copy; 2012 indulhatunk.hu</div>
</td>
</tr>
	
</table>

<script type='text/javascript'>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19263238-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body><html>";

echo $content;
?>
