<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type='text/css'>
		body, html { padding:0; margin:0; font-family:arial; color:#2b2b2b; font-size:12px;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
	</style>
	<title>Indulhatunk.hu - A hét legjobb ajánlatai</title>
</head>
 <body bgcolor='#f2f5f7'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#f2f5f7' style='width: 100%;'>
     <tr>
        <td style='text-align:center;'>
                    
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white' style='text-align:left;margin:0 auto;'>
	<tr>
		<td style='text-align:center;padding:10px;' bgcolor='#f2f5f7' height='10'>
			<a href='http://nl.lmb.hu/?id=[@id]' target='_blank' style='font-size:10px;color:#959595; text-decoration:none;'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a>
		</td>
	</tr>
		<tr>
			<td height='130'>
				
			  <table cellspacing='0' cellpadding='0' border='0' width='760'>
				  <tr>
				  	<td width='420'><a href='http://www.indulhatunk.hu/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/indleft.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' 
					title='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' width='420' height='130'/></a></td>
				  	<td width='250' bgcolor='#1a9bcd' align='center'>
					  	<a href='[@offer8_url]' target='_blank' style='color:white; font-size:24px;font-weight:bold; text-decoration:none'>[@offer8_title]</a>
				  	</td>
				  	<td width='90'><img style='display:block' src='http://www.szallasoutlet.hu/images/indright.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' 
					title='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' width='90' height='130'/></td>
				  </tr>
			  </table>
			</td>
		</tr>
		
		<tr>
			<td valign='top'>
				<!-- -->
	
						<table width='700' style='margin:0px 0 0 30px;background-color:#f1f5f6;' border='0' cellspacing='0' cellpadding='0'>
							<tr>
								<td colspan='2' style="font-size:18px;color:white;font-weight:normal;" height='30' bgcolor='#007bab'><div style='padding:6px'>A HÉT AJÁNLATA</div></td>
							</tr>	
							<tr>
								<td height='247' valign='top'>
									<a href='[@offer1_url]' target='_blank'><img src='[@offer1_image1]' width='340' height='245' style='display:block;' alt='[@offer1_title]' /></a>
								</td>
								<td valign='top'>
								<table width='352' style='' border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td width='20'></td>
								<td height='206' valign='top'>
								<div style='height:10px'></div>
									<a href='[@offer1_url]' style='font-size:28px;color:#007bab;font-weight:bold;text-decoration:none; display:block;' target='_blank' >[@offer1_title]</a>
									<div style='font-size:20px;color:#2b2b2b; font-weight:bold;'>[@offer1_city]</div>
									
									<div style='height:15px;'></div>					
									<b>[@offer1_description]</b>
									
								</td>
								
								</tr>
								<tr>
									<td width='20'></td>
									<td bgcolor='#79ab00' align='center' height='40'>	
									<a href='[@offer1_url]' style='color:white;font-size:26px;font-weight:bold;text-decoration:none;'> MEGNÉZEM &raquo;</a>
</td>
								</tr>
								
								</table>					
								</td>
							</tr>
							
							
								
							<!--<tr>
								<td style='margin: 0 0 0 20px; font-size:18px;padding:10px 5px 5px 20px;'>
									<a href='[@offer1_url]' style='font-size:30px;color:#007bab;text-decoration:none; display:block;' target='_blank' >[@offer1_title]</a>
									<div style='font-size:20px;color:#2b2b2b; font-weight:bold;'>[@offer1_city]</div>
								</td>
							</tr>
							<tr>
								<td>
									<a href='[@offer1_url]'><img src='http://indulhatunk.com/images/newpic.php?indulhatunk=1&imgurl=[@offer1_image]' style='margin:10px 0 10px -4px' target='_blank' /></a>
								</td>
							</tr>
							<tr>
								<td style='padding:10px 20px 10px 20px;color:#2b2b2b;font-weight:bold;font-size:12px;'>
									[@offer1_description]									
								</td>
							</tr>-->

						</table>
		
				<!-- -->
				
				
			</td>
		</tr>
		<tr><td height='20'></td></tr>
	
		<tr>
			<td style='padding:0 0 0 30px'>
				<!-- -->
				<table width='700' border='0' cellspacing='0' cellpadding='0' >
				<tr>
					<td>
						<!-- -->
						<table width='340' border='0' cellspacing='0' cellpadding='0' bgcolor='#f1f5f6' >
							<tr>
								<td style="background-color:#007bab;color:white;margin: 0 0 0px 20px; font-size:18px;padding:5px 5px 5px 20px;" colspan='2'>[@offer2_city]</td>
							</tr>
							<tr>
								<td colspan='2'><a href='[@offer2_url]' target='_blank' ><img src='[@offer2_image1]' height='220' width='340' alt=''/></a></td>
							</tr>
							
							<tr>
								<td style='padding:10px;height:25px; ' colspan='2'><div style='height:25px'><a href='[@offer2_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;' target='_blank' >[@offer2_title]</a></div></td>
							</tr>
							
							<tr>
								<td style='font-size:12px;padding:10px;height:34px;' colspan='2'>
								
									[@offer2_description]
								</td>
							</tr>
							<tr>
								<td bgcolor='#007bab'>
								<div style='padding:10px; color:white;'>
									<b>[@offer2_subtitle]</b>
								</div>
								</td>
								<td bgcolor='#79ab00' align='center' width='180'>
										<a href='[@offer2_url]' style='color:white;font-size:18px;font-weight:bold;text-decoration:none;'>MEGNÉZEM &raquo;</a>
								</td>
							</tr>
						</table>
						<!-- -->						
					</td>
					<td width='20'></td>
					<td valign='top'>
						<!-- -->
							<table width='340' border='0' cellspacing='0' cellpadding='0' bgcolor='#f1f5f6' >
							<tr>
								<td style="background-color:#007bab;color:white;margin: 0 0 0px 20px; font-size:18px;padding:5px 5px 5px 20px;" colspan='2'>[@offer3_city]</td>
							</tr>
							<tr>
								<td colspan='2'><a href='[@offer3_url]' target='_blank' ><img src='[@offer3_image1]' height='220' width='340' alt=''/></a></td>
							</tr>
							
							<tr>
								<td style='padding:10px;height:25px; ' colspan='2'><div style='height:25px'><a href='[@offer3_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;' target='_blank' >[@offer3_title]</a></div></td>
							</tr>
							
							<tr>
								<td style='font-size:12px;padding:10px;height:34px;' colspan='2'>
								
									[@offer3_description]
								</td>
							</tr>
							<tr>
								<td bgcolor='#007bab'>
								<div style='padding:10px; color:white;'>
									<b>[@offer3_subtitle]</b>
								</div>
								</td>
								<td bgcolor='#79ab00' align='center' width='180'>
										<a href='[@offer3_url]' style='color:white;font-size:18px;font-weight:bold;text-decoration:none;'>MEGNÉZEM &raquo;</a>
								</td>
							</tr>
						</table>
						<!-- -->
					</td>
				</tr>
			</table>
			

				<!-- -->

			</td>
		</tr>
		
		<tr><td height='20'></td></tr>
		
		<tr>
			<td style='padding:0 0 0 30px'>

				<table width='700' border='0' cellspacing='0' cellpadding='0' >
					<tr>
						<td valign='top'>
							<!-- -->
							<table width='161' border='0' cellspacing='0' cellpadding='0' bgcolor='#f1f5f6' >
							<tr>
								<td style="background-color:#007bab;color:white;margin: 0 0 0px 20px; font-size:14px;padding:8px 5px 8px 0px; text-align:center;">[@offer4_city]</td>
							</tr>
							<tr>
								<td style='padding:10px;'>
								<div style='height:35px;'>
									<a href='[@offer4_url]' style='font-size:14px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;' target='_blank' >[@offer4_title]</a>
								</div>
								</td>
								
							</tr>
							<tr>
								<td><a href='[@offer4_url]' target='_blank' ><img src='http://indulhatunk.com/images/newpic.php?indulhatunk=1&imgurl=[@offer4_image]' style='margin:0 0 0 -4px;' alt=''/></a></td>
							</tr>
							<tr>
								<td>
								
								<div style='margin:5px 10px 10px 0;  text-align:right;font-size:12px;font-weight:normal;'>
								<b>[@offer4_subtitle]</b>
															</div>

									
								</td>
							</tr>
							
							<tr>
								<td bgcolor='#79ab00' align='center' height='31'>	
									<a href='[@offer4_url]' style='color:white;font-size:18px;font-weight:bold;text-decoration:none;'>MEGNÉZEM &raquo;</a>
								</td>
							</tr>
							</table>
							<!-- -->
						</td>
						<td width='20'></td>
						<td  valign='top'>
						
						<!-- -->
							<table width='161' border='0' cellspacing='0' cellpadding='0' bgcolor='#f1f5f6' >
							<tr>
								<td style="background-color:#007bab;color:white;margin: 0 0 0px 20px; font-size:14px;padding:8px 5px 8px 0px; text-align:center;">[@offer5_city]</td>
							</tr>
							<tr>
								<td style='padding:10px;'>
								<div style='height:35px;'>
									<a href='[@offer5_url]' style='font-size:14px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;' target='_blank' >[@offer5_title]</a>
								</div>
								</td>
							</tr>
							<tr>
								<td><a href='[@offer5_url]' target='_blank' ><img src='http://indulhatunk.com/images/newpic.php?indulhatunk=1&imgurl=[@offer5_image]' style='margin:0 0 0 -4px;' alt=''/></a></td>
							</tr>
							<tr>
								<td>
								
								<div style='margin:5px 10px 10px 0;  text-align:right;font-size:12px;font-weight:normal;'>
								<b>[@offer5_subtitle]</b>
															</div>

									
								</td>
							</tr>
							<tr>
								<td bgcolor='#79ab00' align='center' height='31'>	
									<a href='[@offer5_url]' style='color:white;font-size:18px;font-weight:bold;text-decoration:none;'>MEGNÉZEM &raquo;</a>
								</td>
							</tr>
							</table>
							<!-- -->
						</td>
						<td width='20'></td>
						<td  valign='top'>
							<!-- -->
							<table width='161' border='0' cellspacing='0' cellpadding='0' bgcolor='#f1f5f6' >
							<tr>
								<td style="background-color:#007bab;color:white;margin: 0 0 0px 20px; font-size:14px;padding:8px 5px 8px 0px; text-align:center;">[@offer6_city]</td>
							</tr>
							<tr>
								<td style='padding:10px;'>
								<div style='height:35px;'>
									<a href='[@offer6_url]' style='font-size:14px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;' target='_blank' >[@offer6_title]</a>
								</div>
								</td>
							</tr>
							<tr>
								<td><a href='[@offer6_url]' target='_blank' ><img src='http://indulhatunk.com/images/newpic.php?indulhatunk=1&imgurl=[@offer6_image]' style='margin:0 0 0 -4px;' alt=''/></a></td>
							</tr>
							<tr>
								<td>
								
								<div style='margin:5px 10px 10px 0;  text-align:right;font-size:12px;font-weight:normal;'>
								<b>[@offer6_subtitle]</b>
								</div>

									
								</td>
							</tr>
							<tr>
								<td bgcolor='#79ab00' align='center' height='31'>	
									<a href='[@offer6_url]' style='color:white;font-size:18px;font-weight:bold;text-decoration:none;'>MEGNÉZEM &raquo;</a>
								</td>
							</tr>
							</table>
							<!-- -->
						</td>
						<td  width='20'></td>
						<td  valign='top'>
							<!-- -->
							<table width='161' border='0' cellspacing='0' cellpadding='0' bgcolor='#f1f5f6' >
							<tr>
								<td style="background-color:#007bab;color:white;margin: 0 0 0px 20px; font-size:14px;padding:8px 5px 8px 0px; text-align:center;">[@offer7_city]</td>
							</tr>
							<tr>
								<td style='padding:10px;'>
								<div style='height:35px;'>
								<a href='[@offer7_url]' style='font-size:14px; text-transform:uppercase; text-decoration:none; color:#007bab; font-weight:bold;' target='_blank' >[@offer7_title]</a>
								</div>
								</td>
							</tr>
							<tr>
								<td><a href='[@offer7_url]' target='_blank' ><img src='http://indulhatunk.com/images/newpic.php?indulhatunk=1&imgurl=[@offer7_image]' style='margin:0 0 0 -4px;' alt=''/></a></td>
							</tr>
							<tr>
								<td>
								
								<div style='margin:5px 10px 10px 0;  text-align:right;font-size:12px;font-weight:normal;'>
								<b>[@offer7_subtitle]</b>
							</div>

									
								</td>
							</tr>
							
							<tr>
								<td bgcolor='#79ab00' align='center' height='31'>	
									<a href='[@offer7_url]' style='color:white;font-size:18px;font-weight:bold;text-decoration:none;'>MEGNÉZEM &raquo;</a>
								</td>
							</tr>
							
							</table>
							<!-- -->							
						</td>
						
					</tr>
					
				
				</table>
			
			
			</td>
		</tr>
       
		<tr>
			<td height='20'></td>
		</tr>
	</table>
	
        </td>
     </tr>
     
     <tr>
         	<td style='text-align:center;'>
     		<!-- -->
	 
	 <table width='760' cellpadding='0' cellspacing='0' border='0' style='text-align:left;margin:0 auto;'>
	<tr>
		<td style='height:15px'></td>
	</tr>
		
	<tr>
		<td style='height:20px;font-size:14px;font-size:18px;'>
			Tekintse meg további ajánlatainkat:

		</td>
	</tr>		
	<tr>

	<td style='margin:0 auto;width:760px;text-align:left;'>
	
		
		[@short_description]
</td>
		</tr>
		
	
	<tr>	
		<td align='center'><div style='background:url(http://img.hotel-world.hu/images/fb2.png) no-repeat; height:20px;width:280px;padding:4px 0 0 0;margin:0 auto;'><a href='http://facebook.com/indulhatunk.hu' target='_blank' style='text-decoration:none'>Csatlakozzon oldalunkhoz a <b>Facebook</b>-on!</a></div></td>
	</tr>

	 <tr>
		<td style='height:15px'></td>
	</tr>


	</table>




<!--

			<div style='width:330px; margin-left:215px; margin-top:10px;'>
			
										<table width='330' border='0' cellspacing='0' cellpadding='0'>
			<tr>
			
			<td width='25'><img src='http://img.hotel-world.hu/images/fb.png' align='middle' width='20' alt='Facebook'/></td>
			<td><a href='http://facebook.com/indulhatunk.hu' target='_blank' style='text-decoration:none'>Csatlakozzon oldalunkhoz a <b>Facebook</b>-on!</a></td>
			</tr></table>
			
			</div>
		-->
		
	<!-- <table width='700' style='margin:0 auto;font-size:12px;'>
	

	
	
		[@description22]
	</table> -->
	<div style='font-size:10px; text-align:center;margin:10px 0 0px 0;display:block;color:#acb5b8' id='uns'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' style='color:#acb5b8;' target="_blank">linkre</a> kattintva mondhatja le.</div>
	<div style='height:30px;color:#acb5b8;text-align:center;font-size:11px;margin:10px 0 0 0'>Minden jog fenntartva &copy; 2013 indulhatunk.hu</div>
	[@track]
 	</td>
	</tr>
	</table>     	
	<!-- -->
     	</td>
     </tr>
  </table>
</body></html>