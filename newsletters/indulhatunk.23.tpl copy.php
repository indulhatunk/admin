<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>[@title]</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='4' height='10'></td>
					</tr>
					<tr>
                    	<td width="15"></td>
						<td width="260" valign="top" height='40' align='left'><a target='_blank' href="http://www.indulhatunk.hu?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]&utm_campaign=btn_minden"><img  class="image_fix" alt="Az összes ajánlat megtekintése" title="Az összes ajánlat megtekintése" src='http://indulhatunk.hu/images/indulhatunk-logo.png' width='260' height='40'/></a></td>
						<td width="310" valign="middle" align='right' style='color: #007bab;'>[@subtitle_1]</td>
                        <td width="15"></td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='007bab'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:007BAB;font-weight:normal;'>
					<tr>
						<td colspan='4' height='9'></td>
					</tr>
					<tr>
                    	<td width="15"></td>
						<td width="185" valign="top" height='10' align='left' style='color:#FFFFFF; font-size:11px;'>[@date]</td>
						<td width="385" valign="middle" align='right' style='color:#FFFFFF; font-size:11px;'>
							<a href="http://www.indulhatunk.hu/akcios+ajanlatok.jsp?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_belfold" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">BELFÖLD</a>  |  <a href="http://www.indulhatunk.hu/utazas?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_kulfold" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">KÜLFÖLD</a>  |  <a href="http://www.indulhatunk.hu/repulojegy+london+repjegy.jsp?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_repulojegy" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">REPÜLŐJEGY</a>  |  <a href="http://www.indulhatunk.hu/legnepszerubb-uticelok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_legnepszerubb" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">NÉPSZERŰ ÚTI CÉLOK</a></td>
                            <td width="15"></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
					
			<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='570'>
								<tr>
									<td colspan='3' height='1'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<a href="[@offer1_url]" target ="_blank" title="[@offer1_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer1_title]" title="[@offer1_title]" src='[@offer1_image]' width='570' height='260'/></a>
									</td>
								</tr>
                                <tr>
									<td colspan='3' height='15'></td>
								</tr>
                                <tr>
									<td width='15'></td>
									<td width='555' colspan="2" style='font-size:18px;font-weight:bold; line-height:20px;'><span style='color:#007bab'>[@offer1_title]</span> <br/>
										
										<span style='color:#585858'>[@offer1_city]</span></td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td width='15'></td>
									<td colspan='2'>
										<table width="555" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td height='38' valign='middle' width='270' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:26px;line-height:26px; height:28px;color:#7db300;font-weight:bold;'>[@offer1_price]-tól</div>
											</td>
											
											<td width='285' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer1_url]" target ="_blank" title="[@offer1_title]" style="color: white; text-decoration: none;display:block;width:285px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
            <tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
								[@offer2_title]
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://www.szallasoutlet.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>
			
			
		
						
			<!-- START OF NL LINE -->
				<tr>
				<td valign='top'>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
                <tr bgcolor="#FFFFFF" height="0">
                <td colspan="2"></td></tr>
					<tr bgcolor="#FFFFFF">
					<td width="275" valign='top' >
                    <!-- START OF FRAMING TABLE -->
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='275' bgcolor='white' height='300'>
						<tr>
							<td colspan='2' height='15'></td>
						</tr>
						<tr>	
							<td width='0'></td>
							<td width="275" valign='top' >
								<!-- START OF INNER TABLE -->	
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='250'>
								<tr>
									<td width='10' height='1'></td>
									<td width='230' style='font-size:30px;color:#ec381a;font-weight:bold;'></td>
									<td width="10"></td>
								</tr>
								
								<tr>
									<td colspan='3'>
										<a href="[@offer3_url]" target ="_blank" title="[@offer3_title]" style="color: #454545; text-decoration: none;">
<img  class="image_fix" alt="[@offer3_title]" title="[@offer3_title]" src='[@offer3_image]' width='250' height='180'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
                                <tr>
									<td width='10'></td>
									<td width='240' colspan="2" style='font-size:16px;font-weight:bold; line-height:20px;'><span style='color:#007bab'>[@offer3_title]</span><br/>
										
								  <span style='color:#585858; font-size:12px;'>[@offer3_city]</span></td>
								</tr>
								<tr>
									<td colspan='3' height='8'></td>
								</tr>
                                
								<tr>
									<td></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='262' style='font-size:24px;color:#7db300;font-weight:bold;'>
												<div style='font-size:24px;line-height:22px; height:22px;color:#7db300;font-weight:bold;'>[@offer3_price]-tól</div>
											</td>
											
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- END OF INNER TABLE -->								
							</td>
							
						</tr>
						<tr>
							<td colspan='2' height='20'></td>
						</tr>
						
						
					</table>
					<!-- END OF FRAMING TABLE -->
				</td>
				
				
				<td width='310' valign='top'>
					<!-- -->

					<table cellpadding="0" cellspacing="0" border="0" align="center" width='310' bgcolor='white' height='300'>
					
					  <tr>
						  <td colspan='3' height='15'></td>
					  </tr>
                        <tr>
                          <td width='10'></td>
                          <td width="285" valign='top' ><table cellpadding="0" cellspacing="0" border="0" width="285">
                            <tr style='border-bottom:1px solid #e8e8e8;'>
                              <td width="175" valign='middle' style='font-size:14px; color:#585858;'><a href="[@offer4_url]" target="_blank" title="[@offer4_title]" style="font-size:14px; color:#585858;text-decoration:none;line-height:20px;"><b>[@offer4_title]</b></a>
                              <div style='padding:5px 0 0 0;'><a href="[@offer4_url]" target="_blank" title="[@offer4_title]" style="font-size:14px; color:#585858;text-decoration:none">[@offer4_city]</a></div></td>
                              <td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;'><a href="[@offer4_url]" target="_blank" title="[@offer4_title]" style="font-size:20px; color:#7db300;text-decoration:none"><b>[@offer4_price]</b></a></td>
                            </tr>
                          </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="285">
                              <tr style='border-bottom:1px solid #e8e8e8;'>
                                <td valign='middle' style='font-size:14px; color:#585858;'><a href="[@offer5_url]" target="_blank" title="[@offer5_title]" style="font-size:14px; color:#585858;text-decoration:none;line-height:20px;"><b>[@offer5_title]</b></a>
                                <div style='padding:5px 0 0 0;'><a href="[@offer5_url]" target="_blank" title="[@offer5_title]" style="font-size:14px; color:#585858;text-decoration:none">[@offer5_city]</a></div></td>
                                <td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;'><a href="[@offer5_url]" target="_blank" title="[@offer5_title]" style="font-size:20px; color:#7db300;text-decoration:none"><b>[@offer5_price]</b></a></td>
                              </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="285">
                              <tr style='border-bottom:1px solid #e8e8e8;'>
                                <td valign='middle' style='font-size:14px; color:#585858;'><a href="[@offer6_url]" target="_blank" title="[@offer6_title]" style="font-size:14px; color:#585858;text-decoration:none;line-height:20px;"><b>[@offer6_title]</b> </a>
                                  <div style='padding:5px 0 0 0;'><a href="[@offer6_url]" target="_blank" title="[@offer6_title]" style="font-size:14px; color:#585858;text-decoration:none">[@offer6_city]</a></div></td>
                                <td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;'><a href="[@offer6_url]" target="_blank" title="[@offer6_title]" style="font-size:20px; color:#7db300;text-decoration:none"><b>[@offer6_price]</b></a></td>
                              </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="285">
                              <tr style='border-bottom:1px solid #e8e8e8;'>
                                <td valign='middle' style='font-size:14px; color:#585858;'><a href="[@offer7_url]" target="_blank" title="[@offer7_title]" style="font-size:14px; color:#585858;text-decoration:none;line-height:20px;"><b>[@offer7_title]</b></a>
                                  <div style='padding:5px 0 0 0;'><a href="[@offer7_url]" target="_blank" title="[@offer7_title]" style="font-size:14px; color:#585858;text-decoration:none">[@offer7_city]</a></div></td>
                                <td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;'><a href="[@offer7_url]" target="_blank" title="[@offer7_title]" style="font-size:20px; color:#7db300;text-decoration:none"><b>[@offer7_price]</b></a></td>
                              </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="285">
                              <tr>
                                <td valign='middle' style='font-size:14px; color:#585858;'><a href="[@offer8_url]" target="_blank" title="[@offer8_title]" style="font-size:14px; color:#585858;text-decoration:none;line-height:20px;"><b>[@offer8_title]</b> </a>
                                  <div style='padding:5px 0 0 0;'><a href="[@offer8_url]" target="_blank" title="[@offer8_title]" style="font-size:14px; color:#585858;text-decoration:none">[@offer8_city]</a></div></td>
                                <td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;'><a href="[@offer8_url]" target="_blank" title="[@offer8_title]" style="font-size:20px; color:#7db300;text-decoration:none"><b>[@offer8_price]</b></a></td>
                              </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="285">
                            <tr><td height="10"></td>
                            </tr>
                              <tr>
                                <td width='285' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer2_url]" target ="_blank" title="[@offer2_title]" style="color: white; text-decoration: none;display:block;width:285px;height:25px;text-align:center;padding:13px 0 0 0;">[@offer2_subtitle]</a></td>
                              </tr>
                            </table>
                            <!-- -->
                            <!-- --></td>
                            
                          <td width='15'></td>
                        </tr>
                        <tr>
                          <td colspan='3' height='20'></td>
                        </tr>
<tr></tr>
					</table>
					<!-- -->
				</td>
				</tr>
                <tr bgcolor="#FFFFFF" height="0">
                <td colspan="2"></td></tr>
				</table>
				<!-- -->	
				</td>
				
			</tr>
            <tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
								[@offer8_title]
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://www.szallasoutlet.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
					<tr>
					<td width="185" valign='top'>
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='185' bgcolor='white' height='300'>
						<tr>
							<td width="185" colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td width="155" valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='155'>
								<tr>
									<td width='10' height='1'></td>
									<td width='145' style='font-size:30px;color:#ec381a;font-weight:bold;'></td>
									
								</tr>
								
								<tr>
									<td colspan='2'>
										<a href="[@offer10_url]" target ="_blank" title="[@offer10_title]" style="color: #454545; text-decoration: none;">
<img  class="image_fix" alt="[@offer10_title]" title="[@offer10_title]" src='[@offer10_image]' width='155' height='180'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
                                <tr>
									<td width='10'></td>
									<td width='145' style='font-size:14px;font-weight:bold; line-height:18px;'><span style='color:#007bab'>[@offer10_city]</span> <br/>
										
										<span style='color:#585858; font-size:11px;'>[@offer10_title]</span></td>
								</tr>
								<tr>
									<td colspan='2' height='8'></td>
								</tr>
                                
								<tr>
									<td></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='145' style='font-size:22px;color:#7db300;font-weight:bold;'>
												<div style='font-size:20px;line-height:22px; height:20px;color:#7db300;font-weight:bold;'>[@offer10_price]-tól</div>
											</td>
											
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					
				</td>
				
				<td width='22'></td>
                <td width="185" valign='top'>
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='185' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td width="155" valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='155'>
								<tr>
									<td width='10' height='1'></td>
									<td width='145' style='font-size:30px;color:#ec381a;font-weight:bold;'></td>
									
								</tr>
								
								<tr>
									<td colspan='2'>
										<a href="[@offer11_url]" target ="_blank" title="[@offer11_title]" style="color: #454545; text-decoration: none;">
<img  class="image_fix" alt="[@offer11_title]" title="[@offer11_title]" src='[@offer11_image]' width='155' height='180'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
                                <tr>
									<td width='10'></td>
									<td width='145' style='font-size:14px;font-weight:bold; line-height:18px;'><span style='color:#007bab'>[@offer11_city]</span> <br/>
										
										<span style='color:#585858; font-size:11px;'>[@offer11_title]</span></td>
								</tr>
								<tr>
									<td colspan='2' height='8'></td>
								</tr>
                                
								<tr>
									<td></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='145' style='font-size:22px;color:#7db300;font-weight:bold;'>
												<div style='font-size:20px;line-height:22px; height:20px;color:#7db300;font-weight:bold;'>[@offer11_price]-tól</div>
											</td>
											
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					
				</td>
				
				<td width='23'></td>
                <td width="185" valign='top'>
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='185' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td width="155" valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='155'>
								<tr>
									<td width='10' height='1'></td>
									<td width='145' style='font-size:30px;color:#ec381a;font-weight:bold;'></td>
									
								</tr>
								
								<tr>
									<td colspan='2'>
										<a href="[@offer12_url]" target ="_blank" title="[@offer12_title]" style="color: #454545; text-decoration: none;">
<img  class="image_fix" alt="[@offer12_title]" title="[@offer12_title]" src='[@offer12_image]' width='155' height='180'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
                                <tr>
									<td width='10'></td>
									<td width='145' style='font-size:14px;font-weight:bold; line-height:18px;'><span style='color:#007bab'>[@offer12_city]</span> <br/>
										
										<span style='color:#585858; font-size:11px;'>[@offer12_title]</span></td>
								</tr>
								<tr>
									<td colspan='2' height='8'></td>
								</tr>
                                
								<tr>
									<td></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='145' style='font-size:22px;color:#7db300;font-weight:bold;'>
												<div style='font-size:20px;line-height:22px; height:20px;color:#7db300;font-weight:bold;'>[@offer12_price]-tól</div>
											</td>
											
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					
				</td>
				
				</tr>
				</table>
				<!-- -->	
				</td>
				
			</tr>
            <tr>	
				<td height='15'></td>
			</tr>
						
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- --><!-- -->	
				</td>
				
			</tr>
			<!-- -->
			
			

			
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="[@offer9_url]" target ="_blank" title="[@offer9_title]" style="color:white; text-decoration: none;">[@offer9_subtitle]</a></td>
			</tr>	
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
								[@offer13_title]
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://www.szallasoutlet.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
					<tr>
					<td width="185" valign='top'>
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='185' bgcolor='white' height='300'>
						<tr>
							<td width="185" colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td width="155" valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='155'>
								<tr>
									<td width='10' height='1'></td>
									<td width='145' style='font-size:30px;color:#ec381a;font-weight:bold;'></td>
									
								</tr>
								
								<tr>
									<td colspan='2'>
										<a href="[@offer14_url]" target ="_blank" title="[@offer14_title]" style="color: #454545; text-decoration: none;">
<img  class="image_fix" alt="[@offer14_title]" title="[@offer14_title]" src='[@offer14_image]' width='155' height='180'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
                                <tr>
									<td width='10'></td>
									<td width='145' style='font-size:14px;font-weight:bold; line-height:18px;'><span style='color:#007bab'>[@offer14_city]</span> <br/>
										
										<span style='color:#585858; font-size:11px;'>[@offer14_title]</span></td>
								</tr>
								<tr>
									<td colspan='2' height='8'></td>
								</tr>
                                
								<tr>
									<td></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='145' style='font-size:22px;color:#7db300;font-weight:bold;'>
												<div style='font-size:20px;line-height:22px; height:20px;color:#7db300;font-weight:bold;'>[@offer14_price]-tól</div>
											</td>
											
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					
				</td>
				
				<td width='22'></td>
                <td width="185" valign='top'>
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='185' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td width="155" valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='155'>
								<tr>
									<td width='10' height='1'></td>
									<td width='145' style='font-size:30px;color:#ec381a;font-weight:bold;'></td>
									
								</tr>
								
								<tr>
									<td colspan='2'>
										<a href="[@offer15_url]" target ="_blank" title="[@offer15_title]" style="color: #454545; text-decoration: none;">
<img  class="image_fix" alt="[@offer15_title]" title="[@offer15_title]" src='[@offer15_image]' width='155' height='180'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
                                <tr>
									<td width='10'></td>
									<td width='145' style='font-size:14px;font-weight:bold; line-height:18px;'><span style='color:#007bab'>[@offer15_city]</span> <br/>
										
										<span style='color:#585858; font-size:11px;'>[@offer15_title]</span></td>
								</tr>
								<tr>
									<td colspan='2' height='8'></td>
								</tr>
                                
								<tr>
									<td></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='145' style='font-size:22px;color:#7db300;font-weight:bold;'>
												<div style='font-size:20px;line-height:22px; height:20px;color:#7db300;font-weight:bold;'>[@offer15_price]-tól</div>
											</td>
											
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					
				</td>
				
				<td width='23'></td>
                <td width="185" valign='top'>
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='185' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td width="155" valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='155'>
								<tr>
									<td width='10' height='1'></td>
									<td width='145' style='font-size:30px;color:#ec381a;font-weight:bold;'></td>
									
								</tr>
								
								<tr>
									<td colspan='2'>
										<a href="[@offer16_url]" target ="_blank" title="[@offer16_title]" style="color: #454545; text-decoration: none;">
<img  class="image_fix" alt="[@offer16_title]" title="[@offer16_title]" src='[@offer16_image]' width='155' height='180'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
                                <tr>
									<td width='10'></td>
									<td width='145' style='font-size:14px;font-weight:bold; line-height:18px;'><span style='color:#007bab'>[@offer16_city]</span> <br/>
										
										<span style='color:#585858; font-size:11px;'>[@offer16_title]</span></td>
								</tr>
								<tr>
									<td colspan='2' height='8'></td>
								</tr>
                                
								<tr>
									<td></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='145' style='font-size:22px;color:#7db300;font-weight:bold;'>
												<div style='font-size:20px;line-height:22px; height:20px;color:#7db300;font-weight:bold;'>[@offer16_price]-tól</div>
											</td>
											
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					
				</td>
				
				</tr>
				</table>
				<!-- -->	
				</td>
				
			</tr>
            <tr>	
				<td height='15'></td>
			</tr>
						
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- --><!-- -->	
				</td>
				
			</tr>
			<!-- -->
			
			

			
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="[@offer13_url]" target ="_blank" title="[@offer13_title]" style="color:white; text-decoration: none;">[@offer13_subtitle]</a></td>
			</tr>
			
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
								[@offer17_title]
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://www.szallasoutlet.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer18_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer18_url]" target="_blank" title="[@offer18_title]" style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer18_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer18_city]</div></a>
									</td>
									
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
                    <tr>
						<td width='20'></td>  
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer19_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer19_url]" target="_blank" title="[@offer19_title]"  style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer19_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer19_city]</div></a>
									</td>
									
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
                    <tr>
						<td width='20'></td>  
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer20_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer20_url]" target="_blank" title="[@offer20_title]"  style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer20_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer20_city]</div></a>
									</td>
									
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer21_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer21_url]" target="_blank" title="[@offer21_title]"  style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer21_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer21_city]</div></a>
									</td>
									
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer22_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer22_url]" target="_blank" title="[@offer22_title]" style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer22_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer22_city]</div></a>
									</td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>					
					<tr>
						<td colspan='3' height='10'></td>
					</tr>

				</table>
			</td>
			</tr>

	
		<tr>	
				<td height='15'>&nbsp;</td>
			</tr>	
			
					<tr>
			<td valign='top'>
			<!-- --></td>
			</tr>
			
			
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="[@offer17_url]" target ="_blank" title="[@offer17_title]" style="color:white; text-decoration: none;">[@offer17_subtitle]</a></td>
			</tr>
			<tr>	
				<td height='15'></td>
			</tr>
            <tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
                            
								<tr>
									<td width='230' valign="top">
										<img src='http://www.szallasoutlet.hu/images/staticmap.png' width='230' height='230'/>

									</td>
									<td width='20'>
									
									</td>
									<td width='310'>
										<table cellpadding="0" cellspacing="0" border="0" width='100%'>
											
											<tr>
												<td colspan='3' height='12'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>1067 Budapest, Teréz körút 27. </td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>1114 Budapest, Bartók Béla út 44. </td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>4400 Nyíregyháza, Kossuth tér 8.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>8000 Székesfehárvár, Kossuth utca 10.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iphone.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>+36 70 930 78 74</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/imail.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>info@indulhatunk.hu</td>
											</tr>
											
											<!-- <tr>
												<td colspan='3' height='25'></td>
											</tr>


											<tr>
												<td colspan='3' height='10' style='background-color:#494949;height:40px;text-align:center;' valign='middle'>
													<a href='http://www.indulhatunk.hu/elerhetosegeink?utm_source=[@ga_prefix]&utm_medium=[@nldate]&utm_campaign=btn_minden' target="_blank" style='font-size:16px;font-weight:bold;color:white;text-decoration:none;'>ELÉRHETŐSÉGEINK</a>
												</td>
											</tr> -->

											</table>
									</td>								
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>					
					<tr>
						<td colspan='3' height='20'></td>
					</tr>

				</table>
			</td>
			</tr>
            <tr>	
				<td height='15'></td>
			</tr>	
			<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
				
A hírlevélben feltüntetett árak, képek, és a szolgáltatások leírásai tájékoztató jellegűek és a figyelem felkeltésére szolgálnak. Az ajánlatok részletes leírása a linkekre kattintva honlapunkon érhető el. Levelünket a #EMAIL# címre küldtük, mivel korábban feliratkozott hírlevél szolgáltatásunkra. Amennyiben nem kíván több levelet kapni tőlünk, kérjük kattintson az alábbi linkre:<br/><a href="#URL#" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: underline;">Leiratkozás</a>

				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class="image_fix" alt="az indulhatunk.hu cégcsoport tagja" title="az indulhatunk.hu cégcsoport tagja" src='http://img.hotel-world.hu/images/corporate.png' width='240' height='48'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
		</table>
		</td>
	</tr>
</table>  
</body>
</html>