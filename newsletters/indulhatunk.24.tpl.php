<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>[@title]</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='4' height='10'></td>
					</tr>
					<tr>
                    	<td width="15"></td>
						<td width="260" valign="top" height='40' align='left'><a target='_blank' href="http://www.indulhatunk.hu?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]&utm_campaign=btn_minden"><img  class="image_fix" alt="Az összes ajánlat megtekintése" title="Az összes ajánlat megtekintése" src='http://indulhatunk.hu/images/indulhatunk-logo.png' width='260' height='40'/></a></td>
						<td width="310" valign="middle" align='right' style='color: #007bab;'>[@subtitle_1]</td>
                        <td width="15"></td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='007bab'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:007BAB;font-weight:normal;'>
					<tr>
						<td colspan='4' height='9'></td>
					</tr>
					<tr>
                    	<td width="15"></td>
						<td width="185" valign="top" height='10' align='left' style='color:#FFFFFF; font-size:11px;'>[@date]</td>
						<td width="385" valign="middle" align='right' style='color:#FFFFFF; font-size:11px;'>
							<a href="http://www.indulhatunk.hu/akcios+ajanlatok.jsp?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_belfold" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">BELFÖLD</a>  |  <a href="http://www.indulhatunk.hu/utazas?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_kulfold" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">KÜLFÖLD</a>  |  <a href="http://www.indulhatunk.hu/repulojegy+london+repjegy.jsp?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_repulojegy" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">REPÜLŐJEGY</a>  |  <a href="http://www.indulhatunk.hu/legnepszerubb-uticelok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_legnepszerubb" target ="_blank" title="" style="color: #FFFFFF; text-decoration: none;">NÉPSZERŰ ÚTI CÉLOK</a></td>
                            <td width="15"></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
				
             <tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
								LEGFRISSEBB AKCIÓS UTAZÁS AJÁNLATUNK</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://www.szallasoutlet.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>
					
			<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='570'>
								<tr>
									<td colspan='3' height='1'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<a href="[@offer1_url]" target ="_blank" title="[@offer1_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer1_title]" title="[@offer1_title]" src='[@offer1_image]' width='570' height='260'/></a>
									</td>
								</tr>
                                <tr>
									<td colspan='3' height='15'></td>
								</tr>
                                <tr>
									<td width='15'></td>
									<td width='555' colspan="2" style='font-size:18px;font-weight:bold; line-height:20px;'><span style='color:#007bab'>[@offer1_title]</span> <br/>
										
										<span style='color:#585858'>[@offer1_city]</span></td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td width='15'></td>
									<td colspan='2'>
										<table width="555" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td height='38' valign='middle' width='270' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:26px;line-height:26px; height:28px;color:#7db300;font-weight:bold;'>[@offer1_price]</div>
											</td>
											
											<td width='285' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer1_url]" target ="_blank" title="[@offer1_title]" style="color: white; text-decoration: none;display:block;width:285px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
             <tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
								LEGNAGYOBB KEDVEZMÉNNYEL ELÉRHETŐ AJÁNLATUNK</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://www.szallasoutlet.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>
					
			<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='570'>
								<tr>
									<td colspan='3' height='1'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<a href="[@offer2_url]" target ="_blank" title="[@offer2_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer2_title]" title="[@offer2_title]" src='[@offer2_image]' width='570' height='260'/></a>
									</td>
								</tr>
                                <tr>
									<td colspan='3' height='15'></td>
								</tr>
                                <tr>
									<td width='15'></td>
									<td width='555' colspan="2" style='font-size:18px;font-weight:bold; line-height:20px;'><span style='color:#007bab'>[@offer2_title]</span> <br/>
										
										<span style='color:#585858'>[@offer2_city]</span></td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td width='15'></td>
									<td colspan='2'>
										<table width="555" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td height='38' valign='middle' width='270' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:26px;line-height:26px; height:28px;color:#7db300;font-weight:bold;'>[@offer2_price]</div>
											</td>
											
											<td width='285' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer2_url]" target ="_blank" title="[@offer2_title]" style="color: white; text-decoration: none;display:block;width:285px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
             <tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
								LEGOLCSÓBB UTAZÁSI AJÁNLATUNK</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://www.szallasoutlet.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>
					
			<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						<tr>	
							<td width='15'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='570'>
								<tr>
									<td colspan='3' height='1'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<a href="[@offer3_url]" target ="_blank" title="[@offer3_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer3_title]" title="[@offer3_title]" src='[@offer3_image]' width='570' height='260'/></a>
									</td>
								</tr>
                                <tr>
									<td colspan='3' height='15'></td>
								</tr>
                                <tr>
									<td width='15'></td>
									<td width='555' colspan="2" style='font-size:18px;font-weight:bold; line-height:20px;'><span style='color:#007bab'>[@offer3_title]</span> <br/>
										
										<span style='color:#585858'>[@offer3_city]</span></td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td width='15'></td>
									<td colspan='2'>
										<table width="555" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td height='38' valign='middle' width='270' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:26px;line-height:26px; height:28px;color:#7db300;font-weight:bold;'>[@offer3_price]</div>
											</td>
											
											<td width='285' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer3_url]" target ="_blank" title="[@offer3_title]" style="color: white; text-decoration: none;display:block;width:285px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='15'></td>
						</tr>
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
          
           <tr>	
				<td height='15'></td>
			</tr>
			
			
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="http://www.indulhatunk.hu/utazas/akcios" target ="_blank" title="[@offer17_title]" style="color:white; text-decoration: none;">MEGNÉZEM AZ ÖSSZES AKCIÓS AJÁNLATOT</a></td>
			</tr>
			<tr>	
				<td height='15'></td>
			</tr>
            <tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
                            
								<tr>
									<td width='230' valign="top">
										<img src='http://www.szallasoutlet.hu/images/staticmap.png' width='230' height='230'/>

									</td>
									<td width='20'>
									
									</td>
									<td width='310'>
										<table cellpadding="0" cellspacing="0" border="0" width='100%'>
											
											<tr>
												<td colspan='3' height='12'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>1067 Budapest, Teréz körút 27. </td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>1114 Budapest, Bartók Béla út 44. </td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>4400 Nyíregyháza, Kossuth tér 8.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>8000 Székesfehárvár, Kossuth utca 10.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iphone.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>+36 70 930 78 74</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/imail.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>info@indulhatunk.hu</td>
											</tr>
											
											<!-- <tr>
												<td colspan='3' height='25'></td>
											</tr>


											<tr>
												<td colspan='3' height='10' style='background-color:#494949;height:40px;text-align:center;' valign='middle'>
													<a href='http://www.indulhatunk.hu/elerhetosegeink?utm_source=[@ga_prefix]&utm_medium=[@nldate]&utm_campaign=btn_minden' target="_blank" style='font-size:16px;font-weight:bold;color:white;text-decoration:none;'>ELÉRHETŐSÉGEINK</a>
												</td>
											</tr> -->

											</table>
									</td>								
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>					
					<tr>
						<td colspan='3' height='20'></td>
					</tr>

				</table>
			</td>
			</tr>
            <tr>	
				<td height='15'></td>
			</tr>	
			<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
				
A hírlevélben feltüntetett árak, képek, és a szolgáltatások leírásai tájékoztató jellegűek és a figyelem felkeltésére szolgálnak. Az ajánlatok részletes leírása a linkekre kattintva honlapunkon érhető el. Levelünket a #EMAIL# címre küldtük, mivel korábban feliratkozott hírlevél szolgáltatásunkra. Amennyiben nem kíván több levelet kapni tőlünk, kérjük kattintson az alábbi linkre:<br/><a href="#URL#" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: underline;">Leiratkozás</a>

				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class="image_fix" alt="az indulhatunk.hu cégcsoport tagja" title="az indulhatunk.hu cégcsoport tagja" src='http://img.hotel-world.hu/images/corporate.png' width='240' height='48'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
		</table>
		</td>
	</tr>
</table>  
</body>
</html>