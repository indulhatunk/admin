<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type='text/css'>
	
	*{
  margin: 0px;
  margin: 0px;
}

		body, html { margin:0; margin:0; font-family:arial; color:#2a0000; font-size:12px; text-align:center;}
		a img { border:none; }
		a { color:black; }
		a.white { color:#252525; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
		table { text-align:left;}
	</style>
	<title>Pünkösdi ajánlatok</title>
</head>
 <body bgcolor='#e0f2f5'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#e0f2f5' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:760px;margin:0 auto;'>
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white' align='center'>
		<tr>
			<td height='95'>
				<a href='http://www.indulhatunk.hu?utm_source=newsletter&utm_medium=child_extra&utm_campaign=logo'><img style='display:block' src='http://www.szallasoutlet.hu/images/child_header.jpg' 
					alt='' 
					title='' width='760'/></a>
			</td>
		</tr>
		<tr>
		<td>
			<div style='background-color:#e3e9f1;width:712px;margin:0 auto;'>
			
			<div style='background-color:#df0093;margin:20px 0 0 0;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;'>Tekintse meg kihagyhatatlan ajánlatainkat!</div></div>

			<div style='text-align:center;margin:0px 0 0 0; '>
				<a href='[@offer1_url]' title='[@offer1_title]'><img src='[@offer1_image]' alt='[@offer1_title]' title='[@offer1_title]' width='712' height='355'/></a>
			</div>
					<div style='text-align:left;background-color:#f1f1f1'>
									
					<div style='background-color:#f1f1f1; color:#252525;padding:10px;width:479px; float:left;margin:5px 0 0 0;'>
					
						<div style='font-size:20px;text-transform:uppercase; font-weight:bold;color:#00a8cc;'>[@offer1_title]</div>

					<div style='font-size:20px;text-transform:uppercase; font-weight:bold;color:#00a8cc;'>[@offer1_subtitle]</div>
		
		
							<div style='font-size:16px; font-weight:normal;margin:10px 0 10px 0;'>[@offer1_description]</div>



					</div>
				
					<div style='background-color:#df0093;color:white;font-size:14px;width:213px; float:left;text-align:center;'>
					
					<div style='margin:10px 10px 10px 10px	;'>
						<div style='margin:0 10px 10px 0;text-align:center;'>[@offer1_short_description]</div>
					</div>	
						<div><a href='[@offer1_url]' style='display:block;background-color:#00a8cc; color:white; text-decoration:none; text-align:center;padding:10px 0 10px 0; font-weight:bold;font-size:18px;'>Részletek &raquo;</a>
					</div>
					</div>
					<div style='clear:both;'></div>	
					</div>
					
							<!-- -->
							
				

			</div>

			<div style='margin:20px 25px 0 22px'>
			<table>
				<tr>
					<td width='340'>
						
						<!-- -->
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer2_city]</div></div>
						<a href='[@offer2_url]'><img src='[@offer2_image]' width='340' height='200' alt='[@offer2_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer2_subtitle]</div>
							
							<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer2_title]</div>


						</div>
						
												<div style='background-color:#f1f1f1;'>

						<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer7_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer2_description]</a>
							
							</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer2_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer2_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
							<div style='clear:both;'></div>
						</div>
						<!-- -->

						<!-- -->
						
					</td>
					<!-- -->
						<td width='369' align='right' valign='top'>
						
						<!-- -->
						<div style='width:340px;text-align:left;'>
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer3_city]</div></div>
						<a href='[@offer3_url]'><img src='[@offer3_image]' width='340' height='200'  alt='[@offer3_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer3_subtitle]</div>
							
							<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer3_title]</div>
						</div>
						
						<div style='background-color:#f1f1f1;'>

						<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer7_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer3_description]</a>
							
						</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer3_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer3_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
							<div style='clear:both;'></div>
						</div>
						<!-- -->
						</div>
				
					</td>

					<!-- -->
				</tr>
				<tr><td colspan='2'><div style='height:20px;'></div></td></tr>
				<tr>
					<td width='340'>
						
						<!-- -->
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer4_city]</div></div>
						<a href='[@offer4_url]'><img src='[@offer4_image]' width='340' height='200'  alt='[@offer4_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer4_subtitle]</div>
							
														<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer4_title]</div>

						</div>
						
												<div style='background-color:#f1f1f1;'>

						<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer7_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer4_description]</a>
							
							</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer4_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer4_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
							<div style='clear:both;'></div>
						</div>
						<!-- -->

					</td>
					<!-- -->
						<td width='369' align='right' valign='top'>
						
				
						<!-- -->
						<div style='width:340px;text-align:left;'>
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer5_city]</div></div>
						<a href='[@offer5_url]'><img src='[@offer5_image]' width='340' height='200'  alt='[@offer5_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer5_subtitle]</div>
													<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer5_title]</div>

						</div>
						
							<div style='background-color:#f1f1f1;'>
							
							<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer5_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer5_description]</a>
							
							</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer5_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer5_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
						
							<div style='clear:both;'></div>
						</div>
						</div>
						<!-- -->

						</td>
						
						<!-- -->

				</tr>
				
						<tr><td colspan='2'><div style='height:20px;'></div></td></tr>
				<tr>
					<td width='340'>
						
						<!-- -->
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer6_city]</div></div>
						<a href='[@offer6_url]'><img src='[@offer6_image]' width='340' height='200'  alt='[@offer6_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer6_subtitle]</div>
														<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer6_title]</div>

						</div>
						
													<div style='background-color:#f1f1f1;'>

						<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer6_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer6_description]</a>
							
							</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer6_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer6_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
							<div style='clear:both;'></div>
						</div>
						<!-- -->
						
					</td>
					<!-- -->
						<td width='369' align='right' valign='top'>
						
						<!-- -->
						<div style='width:340px;text-align:left;'>
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer7_city]</div></div>
						<a href='[@offer7_url]'><img src='[@offer7_image]' width='340' height='200'  alt='[@offer7_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer7_subtitle]</div>
													<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer7_title]</div>

						</div>
						
							<div style='background-color:#f1f1f1;'>
							
							<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer7_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer7_description]</a>
							
							</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer7_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer7_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
						
							<div style='clear:both;'></div>
						</div>
						</div>
						<!-- -->
						
					</td>

					<!-- -->
				</tr>
								<tr><td colspan='2'><div style='height:20px;'></div></td></tr>
				<tr>
					<td width='340'>
						
						<!-- -->
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer8_city]</div></div>
						<a href='[@offer8_url]'><img src='[@offer8_image]' width='340' height='200'  alt='[@offer8_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer8_subtitle]</div>
														<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer8_title]</div>

						</div>
						
													<div style='background-color:#f1f1f1;'>

						<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer8_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer8_description]</a>
							
							</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer8_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer8_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
							<div style='clear:both;'></div>
						</div>
						<!-- -->
						
					</td>
					<!-- -->
						<td width='369' align='right' valign='top'>
						
						<!-- -->
						<div style='width:340px;text-align:left;'>
						<div style='background-color:#df0093;'><div style='padding:10px;color:white;font-size:18px;font-weight:bold;text-align:center;'>[@offer9_city]</div></div>
						<a href='[@offer9_url]'><img src='[@offer9_image]' width='340' height='200'  alt='[@offer9_subtitle]'/></a>
						<div style='background-color:#f1f1f1;padding:10px;'>
							
							<div style='margin:10px 0 5px 0;color:#00a8cc;font-size:19px;text-transform:uppercase;font-weight:bold;height:25px;'>[@offer9_subtitle]</div>
													<div style='color:#00a8cc;font-size:16px;font-weight:normal;'>[@offer9_title]</div>

						</div>
						
							<div style='background-color:#f1f1f1;'>
							
							<div style='width:320px;padding:0 10px 10px 10px;'>
							
								<a href='[@offer7_url]' style='text-decoration:none; color:#252525;font-size:14px;'>[@offer9_description]</a>
							
							</div>
							
							
							
							<table width='340' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td bgcolor='#df0093'><div style='padding:10px;color:white;'>[@offer9_short_description]</div></td>
									<td bgcolor='#00a8cc' style='text-align:center;'  width='110'valign="middle"><a href='[@offer9_url]' style='color:white; text-decoration:none; text-align:center; font-weight:bold; font-size:16px;display:block;padding:10px 0 10px 0'>Részletek &raquo;</a></td>
									
								</tr>
							</table>
							
						
							<div style='clear:both;'></div>
						</div>
						</div>
						<!-- -->
						
					</td>

					<!-- -->
				</tr>
				
				
				
				
				
				
			</table>
			</div>
		</td>
		
		</tr>
		
			
				<tr>
			<td>
				<div style='margin:10px 35px 10px 35px;'>[@description]</div></td>
		</tr>
		
			<tr>
			<td>
			
			
			<div style='width:330px; margin-left:255px; margin-top:10px;'>
			
										<table width='330' border='0' cellspacing='0' cellpadding='0'>
			<tr>
			
			<td width='25'><img src='http://img.hotel-world.hu/images/fb.png' align='middle' width='20' alt='Látogass meg minket Facebookon'/></td>
			<td><a href='http://facebook.com/indulhatunk.hu' target='_blank' style='text-decoration:none'>Csatlakozzon oldalunkhoz a <b>Facebook</b>-on!</a></td>
			</tr></table>
			
			</div></td>
		</tr>
		
			<tr>
			<td align='center'><div style='margin:10px 0 0 0;font-size:10px'>&nbsp;</div></td>
		</tr>
		
		<tr>
			<td align='center'><img src='http://www.szallasoutlet.hu/images/rom_footer.jpg' alt='footer'/></td>
		</tr>
		
		
	</table>
	</div>
	
	<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#252525; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#252525;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:#252525;text-align:center;font-size:11px;margin:px 0 0 0'>Minden jog fenntartva &copy; 2013 |  indulhatunk.hu</div>
	[@track]
</td>
</tr>
</table>
</body>
</html>