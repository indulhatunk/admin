<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type='text/css'>
		body, html { padding:0; margin:0; font-family:arial; color:#2b2b2b; font-size:12px;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
	</style>
	<title>Indulhatunk.hu - A hét legjobb ajánlatai</title>
</head>
 <body bgcolor='#f2f5f7'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#f2f5f7' style='width: 100%;'>
     <tr>
        <td style='text-align:center;'>
                    
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white' style='text-align:left;margin:0 auto;'>
	<tr>
		<td style='text-align:center;padding:10px;' bgcolor='#f2f5f7' height='10'>
			<a href='http://nl.lmb.hu/?id=[@id]' target='_blank' style='font-size:10px;color:#959595; text-decoration:none;'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a>
		</td>
	</tr>
		<tr>
			<td height='112'>
				
			  <table cellspacing='0' cellpadding='0' border='0' width='760'>
				  <tr>
				  	<td width='505' height='112'><a href='http://www.indulhatunk.hu/?utm_source=[@ga_prefix]_logo' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/etop.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' 
					title='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' width='510' height='112'/></a></td>
				  	<td width='250' bgcolor='#1a9bcd' align='center'>
				  	
				  		  <table cellspacing='0' cellpadding='0' border='0' width='250'>
					  		  <tr>
					  		  	<td height='22'><img style='display:block' src='http://www.szallasoutlet.hu/images/etop1.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' 
					title='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' width='250' height='22'/></td>
					  		  </tr>
					  		  <tr>
					  		  	<td height='67' bgcolor='#006992' align='center'><a href='#' style='color:white; font-weight:bold; text-align:center;font-size:18px;text-decoration:none;'>Ön hova utazna legszívesebben?</a></td>
					  		  </tr>
							<tr>
					  		  	<td height='23'><img style='display:block' src='http://www.szallasoutlet.hu/images/etop2.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' 
					title='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' width='250' height='23'/></td>
					  		  </tr>

				  		  </table>
					  	
				  	</td>
				  </tr>
			  </table>
			</td>
		</tr>
		
		<tr>
			<td bgcolor='#00729e'>
				  <table cellspacing='0' cellpadding='0' border='0' width='760'>
				  	<tr>
				  		<td colspan='9' height='5'></td>
				  	</tr>
					  <tr>
					  	<td width='25'></td>
					  	<td width='170' bgcolor='#0084b1' align='center' height='25'><a href='http://www.xn--gyermekbart-ajnlatok-rxbe.hu/?utm_source=[@ga_prefix]_btn&utm_medium=[@nldate]' style='color:white; text-decoration:none;font-weight:bold;font-size:11px' target='_blank'>GYERMEKBARÁT</a></td>
					 	<td width='10'></td>
					  	<td width='170' bgcolor='#0084b1' align='center' height='25'><a href='http://www.szepkartyafelhasznalas.hu/?utm_source=[@ga_prefix]_btn&utm_medium=[@nldate]' style='color:white; text-decoration:none;font-weight:bold;font-size:11px' target='_blank'>SZÉP KÁRTYA</a></td>	
					  	<td width='10'></td>
					  	<td width='170' bgcolor='#0084b1' align='center' height='25'><a href='http://www.xn--wellness-ajnlatok-hpb.hu/?utm_source=[@ga_prefix]_btn&utm_medium=[@nldate]' style='color:white; text-decoration:none;font-weight:bold;font-size:11px' target='_blank'>WELLNESS</a></td>
					  	<td width='10'></td>
					  	<td width='170' bgcolor='#0084b1' align='center' height='25'><a href='http://www.indulhatunk.hu/?utm_source=[@ga_prefix]_btn&utm_medium=[@nldate]' style='color:white; text-decoration:none;font-weight:bold;font-size:11px' target='_blank'>INDULHATUNK?</a></td>
					  	<td width='25'></td>
					    </tr>
					<tr>
				  		<td colspan='9' height='5'></td>
				  	</tr>

				  </table>


				
			</td>
		</tr>
		<tr>
			<td height='15'></td>
		</tr>
		<tr>
			<td valign='top'>
			
			
			<table cellspacing='0' cellpadding='0' border='0' width='760'>
					<tr>
						<td width='40'></td>

						<td>
						
			Bizonyára Ön is észrevette, hogy a piacon elérhető utazási kínálat szinte végtelen. Éppen ezért szeretnénk segítséget nyújtani, hogy az elvárásainak legmegfelelőbb ajánlatokat kapja meg, így könnyítve meg Önnek, hogy a legjobbat tudja kiválasztani.<br/><br/>
Négy hetes periódusban küldött leveleinkben minden héten más-más ajánlattípusra hívjuk majd fel a figyelmét részletesen kiemelve azok előnyeit. Bízunk abban, hogy a tematikus oldalakra látogatva vagy akár a későbbiekben a hírlevélben ajánlott úticélok között megtalálja majd az Önnek megfelelő utazást.<br/><br/>

Az egyszerűbb keresés érdekében például összegyűjtöttük azokat a helyeket, amik kiemelten <a href='http://www.xn--gyermekbart-ajnlatok-rxbe.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank' style='text-decoration:none;font-weight:bold;color:#de3600'>gyermekbarát</a> ajánlatokkal várják a vendégeket, emellett akár <a href='http://www.xn--wellness-ajnlatok-hpb.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank' style='text-decoration:none;font-weight:bold;color:#de3600'>wellness</a> vagy <a href='http://www.szepkartyafelhasznalas.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank' style='text-decoration:none;font-weight:bold;color:#de3600'>Széchenyi kártyás</a> ajánlatok között is böngészhet, az árérzékeny <a href='http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank' style='text-decoration:none;font-weight:bold;color:#de3600'>Szállás Outlet</a> kínálatán vagy a <a href='http://www.pihenni.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank' style='text-decoration:none;font-weight:bold;color:#de3600'>külföldi</a> és <a href='http://www.indulhatunk.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank' style='text-decoration:none;font-weight:bold;color:#de3600'>belföldi</a> utakon túl. 
<br/><br/>
<b>Jó utazást!</b><br/><br/>

Indulhatunk.hu csapata
						</td>
						<td width='40'></td>
					</tr>
						<tr>
			<td height='15'></td>
		</tr>
			</table>




				 <table cellspacing='0' cellpadding='0' border='0' width='760'>
					<tr>
						<td width='40'></td>
						<td width='323' bgcolor='#f2f5f7' valign='top'>
							<!-- -->
							
						 <table cellspacing='0' cellpadding='0' border='0' width='322'>

							<tr>
							<td><a href='http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/o1.jpg' alt='' title='' width='323' height='310'/></a></td>
							</tr>
							<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;'>
									
									[@description]
							
									
								</td>
							</tr>
							<tr>
							<td><a href='http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/o2.jpg' alt='' title='' width='323' height='66'/></a></td>
							</tr>

							
						 </table>	
							
							</td>
						<td width='40'></td>
						<td width='323' bgcolor='#f2f5f7' valign='top'>
							<!-- -->
												 <table cellspacing='0' cellpadding='0' border='0' width='322'>

							<tr>
							<td><a href='http://www.pihenni.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/p1.jpg' alt='' title='' width='323' height='310'/></a></td>
							</tr>
							<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;'>
								
									[@short_description]
									
									
								</td>
							</tr>
							<tr>
							<td><a href='http://www.pihenni.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/p2.jpg' alt='' title='' width='323' height='66'/></a></td>
							</tr>

							
						 </table>	
							
							<!-- -->
							
						</td>						
						<td width='40'></td>
					</tr>	  
				
				</table>
			</td>
		</tr>
		
			<tr>
			<td height='30'></td>
		</tr>
		
		<tr>
			<td>
				 <table cellspacing='0' cellpadding='0' border='0' width='760'>

					 <tr>
					 	<td width='40'></td>
					 	<td><img style='display:block' src='http://www.szallasoutlet.hu/images/c1.jpg' alt='' title='' width='230' height='143'/></td>
					 
					 	<td width='390' bgcolor='#f2f5f7' style='border-top:1px solid #d9d9d9;border-bottom:1px solid #d9d9d9' height='141' valign='top'>
								 <table cellspacing='0' cellpadding='0' border='0' width='390'>
									 <tr>
										 <td height='21'></td>
									 </tr>
									  <tr>
										 <td height='46' bgcolor='#0089c6'><a href='[@offer1_url]' style='color:white;font-size:14px;text-decoration:none; font-weight:bold'>[@offer1_title]</a></td>
									 </tr>
									  <tr>
										 <td height='9'></td>
									 </tr>
									  <tr>
										 <td height='46'  bgcolor='#0089c6'><a href='[@offer2_url]' style='color:white;font-size:14px;text-decoration:none; font-weight:bold'>[@offer2_title]</a></td>
									 </tr>
									  <tr>
										 <td height='18'></td>
									 </tr>
								 </table>
						 	
					 	</td>
					 	<td><img style='display:block' src='http://www.szallasoutlet.hu/images/c3.jpg' alt='' title='' width='70' height='143'/></td>
					 	<td width='40'></td>
					 </tr>
				 </table>
				
			</td>
		</tr>
		
		
		<tr>
			<td height='20'></td>
		</tr>
		
		<!-- -->
				<tr>
			<td valign='top'>
				 <table cellspacing='0' cellpadding='0' border='0' width='760'>
					<tr>
						<td width='40'></td>
						<td width='323' bgcolor='#f2f5f7' valign='top'>
							<!-- -->
							
						 <table cellspacing='0' cellpadding='0' border='0' width='322'>

							<tr>
							<td><a href='http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/o3.jpg' alt='' title='' width='323' height='135'/></a></td>
							</tr>
							<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;' align='center'>
								
									 <table cellspacing='0' cellpadding='0' border='0' width='321'>
										 <tr>
										 	<td align='center'>
												<a href='[@offer3_url]' target='_blank'><img style='display:block' src='[@offer3_image]' alt='' title='' width='271' height='155'/></a>		
										 	</td>
										 </tr>
											
										<tr>
											<td height='10'></td>
										</tr>
									 
										<tr>
											<td align='center'>
												<a href='[@offer3_url]' style='text-decoration:none;font-size:16px;font-weight:bold;color:#de3600'>[@offer3_title]</a>
												<div style='color:#252525;font-size:14px;font-weight:bold;'>[@offer3_city]</div>
											</td>
										</tr>
		
										<tr>
											<td height='20'></td>
										</tr>
		
									 </table>
									
								</td>
							</tr>
							<tr>
							<td><a href='[@offer3_url]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/o4.jpg' alt='' title='' width='323' height='52'/></a></td>
							</tr>
							<tr>
							<td><img style='display:block' src='http://www.szallasoutlet.hu/images/sep1.jpg' alt='' title='' width='323' height='25'/></td>
							</tr>
								<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;' align='center'>
								
									 <table cellspacing='0' cellpadding='0' border='0' width='321'>
										 <tr>
										 	<td align='center'>
												<a href='[@offer4_url]' target='_blank'><img style='display:block' src='[@offer4_image]' alt='' title='' width='271' height='155'/></a>		
										 	</td>
										 </tr>
											
										<tr>
											<td height='10'></td>
										</tr>
									 
										<tr>
											<td align='center'>
												<a href='[@offer4_url]' style='text-decoration:none;font-size:16px;font-weight:bold;color:#de3600'>[@offer4_title]</a>
												<div style='color:#252525;font-size:14px;font-weight:bold;'>[@offer4_city]</div>
											</td>
										</tr>
		
										<tr>
											<td height='20'></td>
										</tr>
		
									 </table>
									
								</td>
							</tr>
							<tr>
							<td><a href='[@offer4_url]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/o4.jpg' alt='' title='' width='323' height='52'/></a></td>
							</tr>
							
							<tr>
							<td><img style='display:block' src='http://www.szallasoutlet.hu/images/sep1.jpg' alt='' title='' width='323' height='25'/></td>
							</tr>
							
							<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;' align='center'>
								
									 <table cellspacing='0' cellpadding='0' border='0' width='321'>
										 <tr>
										 	<td align='center'>
												<a href='[@offer5_url]' target='_blank'><img style='display:block' src='[@offer5_image]' alt='' title='' width='271' height='155'/></a>		
										 	</td>
										 </tr>
											
										<tr>
											<td height='10'></td>
										</tr>
									 
										<tr>
											<td align='center'>
												<a href='[@offer5_url]' style='text-decoration:none;font-size:16px;font-weight:bold;color:#de3600'>[@offer5_title]</a>
												<div style='color:#252525;font-size:14px;font-weight:bold;'>[@offer5_city]</div>
											</td>
										</tr>
		
										<tr>
											<td height='20'></td>
										</tr>
		
									 </table>
									
								</td>
							</tr>
							<tr>
							<td><a href='[@offer5_url]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/o4.jpg' alt='' title='' width='323' height='52'/></a></td>
							</tr>
							<tr>
							<td><img style='display:block' src='http://www.szallasoutlet.hu/images/nbt.jpg' alt='' title='' width='323' height='14'/></td>
							</tr>

							
						 </table>	
							
							</td>
						<td width='40'></td>
							<td width='323' bgcolor='#f2f5f7' valign='top'>
							<!-- -->
							
						 <table cellspacing='0' cellpadding='0' border='0' width='322'>

							<tr>
							<td><a href='http://www.pihenni.hu/?utm_source=[@ga_prefix]&utm_medium=[@nldate]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/p3.jpg' alt='' title='' width='323' height='135'/></a></td>
							</tr>
							<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;' align='center'>
								
									 <table cellspacing='0' cellpadding='0' border='0' width='321'>
										 <tr>
										 	<td align='center'>
												<a href='[@offer6_url]' target='_blank'><img style='display:block' src='[@offer6_image]' alt='' title='' width='271' height='155'/></a>		
										 	</td>
										 </tr>
											
										<tr>
											<td height='10'></td>
										</tr>
									 
										<tr>
											<td align='center'>
												<a href='[@offer6_url]' style='text-decoration:none;font-size:16px;font-weight:bold;color:#006bb0'>[@offer6_title]</a>
												<div style='color:#252525;font-size:14px;font-weight:bold;'>[@offer6_city]</div>
											</td>
										</tr>
		
										<tr>
											<td height='20'></td>
										</tr>
		
									 </table>
									
								</td>
							</tr>
							<tr>
							<td><a href='[@offer6_url]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/p4.jpg' alt='' title='' width='323' height='52'/></a></td>
							</tr>
							<tr>
							<td><img style='display:block' src='http://www.szallasoutlet.hu/images/sep1.jpg' alt='' title='' width='323' height='25'/></td>
							</tr>
								<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;' align='center'>
								
									 <table cellspacing='0' cellpadding='0' border='0' width='321'>
										 <tr>
										 	<td align='center'>
												<a href='[@offer7_url]' target='_blank'><img style='display:block' src='[@offer7_image]' alt='' title='' width='271' height='155'/></a>		
										 	</td>
										 </tr>
											
										<tr>
											<td height='10'></td>
										</tr>
									 
										<tr>
											<td align='center'>
												<a href='[@offer7_url]' style='text-decoration:none;font-size:16px;font-weight:bold;color:#006bb0'>[@offer7_title]</a>
												<div style='color:#252525;font-size:14px;font-weight:bold;'>[@offer7_city]</div>
											</td>
										</tr>
		
										<tr>
											<td height='20'></td>
										</tr>
		
									 </table>
									
								</td>
							</tr>
							<tr>
							<td><a href='[@offer7_url]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/p4.jpg' alt='' title='' width='323' height='52'/></a></td>
							</tr>
							
							<tr>
							<td><img style='display:block' src='http://www.szallasoutlet.hu/images/sep1.jpg' alt='' title='' width='323' height='25'/></td>
							</tr>
							
								<tr>
								<td  style='border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;' align='center'>
								
									 <table cellspacing='0' cellpadding='0' border='0' width='321'>
										 <tr>
										 	<td align='center'>
												<a href='[@offer8_url]' target='_blank'><img style='display:block' src='[@offer8_image]' alt='' title='' width='271' height='155'/></a>		
										 	</td>
										 </tr>
											
										<tr>
											<td height='10'></td>
										</tr>
									 
										<tr>
											<td align='center'>
												<a href='[@offer8_url]' style='text-decoration:none;font-size:16px;font-weight:bold;color:#006bb0'>[@offer8_title]</a>
												<div style='color:#252525;font-size:14px;font-weight:bold;'>[@offer8_city]</div>
											</td>
										</tr>
		
										<tr>
											<td height='20'></td>
										</tr>
		
									 </table>
									
								</td>
							</tr>
							<tr>
							<td><a href='[@offer8_url]' target='_blank'><img style='display:block' src='http://www.szallasoutlet.hu/images/p4.jpg' alt='' title='' width='323' height='52'/></a></td>
							</tr>
							<tr>
							<td><img style='display:block' src='http://www.szallasoutlet.hu/images/nbt.jpg' alt='' title='' width='323' height='14'/></td>
							</tr>

							
						 </table>	
							
							</td>
					
						<td width='40'></td>
					</tr>	  
				
				</table>
			</td>
		</tr>
		
		<!-- -->
		<tr>
			<td height='30'></td>
		</tr>
		
	</table>
	
        </td>
     </tr>
     
     <tr>
         	<td style='text-align:center;'>
     		<!-- -->
	 
	 <table width='760' cellpadding='0' cellspacing='0' border='0' style='text-align:left;margin:0 auto;'>
	<tr>
		<td style='height:15px'></td>
	</tr>
		
			
	
	<tr>	
		<td align='center'><div style='background:url(http://img.hotel-world.hu/images/fb2.png) no-repeat; height:20px;width:280px;padding:4px 0 0 0;margin:0 auto;'><a href='http://facebook.com/indulhatunk.hu' target='_blank' style='text-decoration:none'>Csatlakozzon oldalunkhoz a <b>Facebook</b>-on!</a></div></td>
	</tr>

	 <tr>
		<td style='height:15px'></td>
	</tr>


	</table>

	<div style='font-size:10px; text-align:center;margin:10px 0 0px 0;display:block;color:#acb5b8' id='uns'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' style='color:#acb5b8;' target="_blank">linkre</a> kattintva mondhatja le.</div>
	<div style='height:30px;color:#acb5b8;text-align:center;font-size:11px;margin:10px 0 0 0'>Minden jog fenntartva &copy; 2013 indulhatunk.hu</div>
	[@track]
 	</td>
	</tr>
	</table>     	
	<!-- -->
</body></html>