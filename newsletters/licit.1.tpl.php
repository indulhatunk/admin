<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type='text/css'>
		body, html { padding:0; margin:0; font-family:arial; color:#252525; font-size:12px;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
	</style>
	<title>Licittravel.hu ajánlat</title>
</head>
 <body bgcolor='#f3f0e9'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#f3f0e9' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:682px;margin:0 auto;'>
	<table width='682' cellpadding='0' cellspacing='0' border='0' bgcolor='white'>
	<tr>
		<td bgcolor='f3f0e9'><div style='text-align:center;margin:10px'><a href='http://nl.lmb.hu/?id=[@id]' target='_blank' style='font-size:10px;'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a></div></td>
	</tr>
		<tr>
			<td height='132'>
				<a href='http://licittravel.hu/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]'><img style='display:block' src='http://szallasoutlet.hu/images/licittop.jpg' 
					alt='Licitálj és megless!' 
					title='Licitálj és megless!' width='682'/></a>
			</td>
		</tr>
		<tr>
			<td>
				<div style='width:640px;border-right:1px solid #cfccc6; border-left:1px solid #cfccc6; padding:20px;'>
				
					<div style='padding:0px 0 20px 0'>[@description]</div>
					
					<table width='640' cellpadding='0' cellspacing='0' border='0' bgcolor='white'>
						<tr>
							<td width='330'>
								<!-- -->
								
								<table border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td width='330' height='60' valign='middle' style='color:white; width:280px;font-size:18px;background-color:#1f8ec4;height:40px; border-bottom:1px dashed white; font-weight:bold;padding:10px;text-align:center;'>
								
								[@offer1_title]
								</td>
								</tr>
								<tr>
								<td bgcolor='#f3f0e9' align='center' >
								
								
								<table border='0' cellspacing='0' cellpadding='0'  bgcolor='#f3f0e9'>
								<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								<tr>
									<td width='260' bgcolor='white' style='padding:10px;margin:10px;'>
										<a href='[@offer1_url]'><img src='[@offer1_image]' width='258' height='145' alt=''/></a>
											
										<div style='font-weight:bold;font-size:11px;padding:10px 0 5px 0'>[@offer1_city]</div>
										<div style='color:#1686bb; font-size:16px;font-weight:bold; height:40px'>[@offer1_subtitle]</div>
										<div style='font-size:11px;height:35px;'>[@offer1_description]</div>


										<div style='font-weight:bold;padding:10px 0 10px 0;'>Induló licitár: <span style='font-size:23px;color:#ff6701'>[@offer1_price]</span></div>
										
										<a href='[@offer1_url]'><img src='http://www.szallasoutlet.hu/images/licit_more.jpg' alt='Licitálok!'/></a>

									</td>
								</tr>
									<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								</table>
								
								
								</td>
								</tr>
								</table>
								<!-- -->
								
							</td>
							<td>
									<!-- -->
									<table border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td width='330' height='60' valign='middle' style='color:white; width:280px;font-size:18px;background-color:#1f8ec4;height:40px; border-bottom:1px dashed white; font-weight:bold;padding:10px;text-align:center;'>
								
								[@offer2_title]
								</td>
								</tr>
								<tr>
								<td bgcolor='#f3f0e9' align='center' >
								
								
								<table border='0' cellspacing='0' cellpadding='0'  bgcolor='#f3f0e9'>
								<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								<tr>
									<td width='260' bgcolor='white' style='padding:10px;margin:10px;'>
										<a href='[@offer2_url]'><img src='[@offer2_image]' width='258' height='145' alt=''/></a>
											
										<div style='font-weight:bold;font-size:11px;padding:10px 0 5px 0'>[@offer2_city]</div>
										<div style='color:#1686bb; font-size:16px;font-weight:bold; height:40px'>[@offer2_subtitle]</div>
										<div style='font-size:11px;height:35px;'>[@offer2_description]</div>


										<div style='font-weight:bold;padding:10px 0 10px 0;'>Induló licitár: <span style='font-size:23px;color:#ff6701'>[@offer2_price]</span></div>
										
										<a href='[@offer2_url]'><img src='http://www.szallasoutlet.hu/images/licit_more.jpg' alt='Licitálok!'/></a>

									</td>
								</tr>
									<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								</table>
								
								
								</td>
								</tr>
								</table>
								<!-- -->								
								<!-- -->
								
							</td>
						</tr>
						<tr>
							<td>	<!-- -->
									<table border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td width='330' height='60' valign='middle' style='color:white; width:280px;font-size:18px;background-color:#1f8ec4;height:40px; border-bottom:1px dashed white; font-weight:bold;padding:10px;text-align:center;'>
								
								[@offer3_title]
								</td>
								</tr>
								<tr>
								<td bgcolor='#f3f0e9' align='center' >
								
								
								<table border='0' cellspacing='0' cellpadding='0'  bgcolor='#f3f0e9'>
								<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								<tr>
									<td width='260' bgcolor='white' style='padding:10px;margin:10px;'>
										<a href='[@offer3_url]'><img src='[@offer3_image]' width='258' height='145' alt=''/></a>
											
										<div style='font-weight:bold;font-size:11px;padding:10px 0 5px 0'>[@offer3_city]</div>
										<div style='color:#1686bb; font-size:16px;font-weight:bold; height:40px'>[@offer3_subtitle]</div>
										<div style='font-size:11px;height:35px;'>[@offer3_description]</div>


										<div style='font-weight:bold;padding:10px 0 10px 0;'>Induló licitár: <span style='font-size:23px;color:#ff6701'>[@offer3_price]</span></div>
										
										<a href='[@offer3_url]'><img src='http://www.szallasoutlet.hu/images/licit_more.jpg' alt='Licitálok!'/></a>

									</td>
								</tr>
									<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								</table>
								
								
								</td>
								</tr>
								</table>
								<!-- -->
																<!-- --></td>
							<td>	<!-- -->
									<table border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td width='330' height='60' valign='middle' style='color:white; width:280px;font-size:18px;background-color:#1f8ec4;height:40px; border-bottom:1px dashed white; font-weight:bold;padding:10px;text-align:center;'>
								
								[@offer4_title]
								</td>
								</tr>
								<tr>
								<td bgcolor='#f3f0e9' align='center' >
								
								
								<table border='0' cellspacing='0' cellpadding='0'  bgcolor='#f3f0e9'>
								<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								<tr>
									<td width='260' bgcolor='white' style='padding:10px;margin:10px;'>
										<a href='[@offer4_url]'><img src='[@offer4_image]' width='258' height='145' alt=''/></a>
											
										<div style='font-weight:bold;font-size:11px;padding:10px 0 5px 0'>[@offer4_city]</div>
										<div style='color:#1686bb; font-size:16px;font-weight:bold; height:40px'>[@offer4_subtitle]</div>
										<div style='font-size:11px;height:35px;'>[@offer4_description]</div>


										<div style='font-weight:bold;padding:10px 0 10px 0;'>Induló licitár: <span style='font-size:23px;color:#ff6701'>[@offer4_price]</span></div>
										
										<a href='[@offer4_url]'><img src='http://www.szallasoutlet.hu/images/licit_more.jpg' alt='Licitálok!'/></a>

									</td>
								</tr>
									<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								</table>
								
								
								</td>
								</tr>
								</table>
								<!-- -->								
								<!-- --></td>
						</tr>
						<tr>
							<td>
									<!-- -->
									<table border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td width='330' height='60' valign='middle' style='color:white; width:280px;font-size:18px;background-color:#1f8ec4;height:40px; border-bottom:1px dashed white; font-weight:bold;padding:10px;text-align:center;'>
								
								[@offer5_title]
								</td>
								</tr>
								<tr>
								<td bgcolor='#f3f0e9' align='center' >
								
								
								<table border='0' cellspacing='0' cellpadding='0'  bgcolor='#f3f0e9'>
								<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								<tr>
									<td width='260' bgcolor='white' style='padding:10px;margin:10px;'>
										<a href='[@offer5_url]'><img src='[@offer5_image]' width='258' height='145' alt=''/></a>
											
										<div style='font-weight:bold;font-size:11px;padding:10px 0 5px 0'>[@offer5_city]</div>
										<div style='color:#1686bb; font-size:16px;font-weight:bold; height:40px'>[@offer5_subtitle]</div>
										<div style='font-size:11px;height:35px;'>[@offer5_description]</div>


										<div style='font-weight:bold;padding:10px 0 10px 0;'>Induló licitár: <span style='font-size:23px;color:#ff6701'>[@offer5_price]</span></div>
										
										<a href='[@offer5_url]'><img src='http://www.szallasoutlet.hu/images/licit_more.jpg' alt='Licitálok!'/></a>

									</td>
								</tr>
									<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								</table>
								
								
								</td>
								</tr>
								</table>
								<!-- -->								
								<!-- -->
							</td>
							<td>
									<!-- -->
										<table border='0' cellspacing='0' cellpadding='0'>
								<tr>
								<td width='330' height='60' valign='middle' style='color:white; width:280px;font-size:18px;background-color:#1f8ec4;height:40px; border-bottom:1px dashed white; font-weight:bold;padding:10px;text-align:center;'>
								
								[@offer6_title]
								</td>
								</tr>
								<tr>
								<td bgcolor='#f3f0e9' align='center' >
								
								
								<table border='0' cellspacing='0' cellpadding='0'  bgcolor='#f3f0e9'>
								<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								<tr>
									<td width='260' bgcolor='white' style='padding:10px;margin:10px;'>
										<a href='[@offer6_url]'><img src='[@offer6_image]' width='258' height='145' alt=''/></a>
											
										<div style='font-weight:bold;font-size:11px;padding:10px 0 5px 0'>[@offer6_city]</div>
										<div style='color:#1686bb; font-size:16px;font-weight:bold; height:40px'>[@offer6_subtitle]</div>
										<div style='font-size:11px;height:35px;'>[@offer6_description]</div>


										<div style='font-weight:bold;padding:10px 0 10px 0;'>Induló licitár: <span style='font-size:23px;color:#ff6701'>[@offer6_price]</span></div>
										
										<a href='[@offer6_url]'><img src='http://www.szallasoutlet.hu/images/licit_more.jpg' alt='Licitálok!'/></a>

									</td>
								</tr>
									<tr>
									<td height='10'>&nbsp;</td>
								</tr>
								</table>
								
								
								</td>
								</tr>
								</table>
								<!-- -->								
								<!-- -->
							</td>
						</tr>
					</table>
					
					<br/><br/>	Üdvözlettel: <br/><br/>
<b>A LicitTravel csapata</b>

				</div>
			</td>

		</tr>
		
		
		<tr>
			<td>
			<img src='http://szallasoutlet.hu/images/licitbottom.jpg' 
					alt='Licitálj és megless!' 
					title='Licitálj és megless!' width='682'/>
			</td>
		</tr>
		
		
	</table>
	</div>
	
	<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#bbb8b1; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#bbb8b1;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:#bbb8b1;text-align:center;font-size:11px;padding:10px 0 0 0'>Minden jog fenntartva &copy;2013 licittravel.hu</div>

</td>
</tr>
	
</table>
</body></html>