<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
	<style>
	
	*{
  margin: 0px;
  margin: 0px;
}

		body, html { margin:0; margin:0; font-family:arial; color:#2a0000; font-size:12px; text-align:center;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
		table { text-align:left;}
	</style>
	<title>Last Minute Szálloda ajánlat</title>
</head>
 <body bgcolor='#f1f1f1' topmargin="0">
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#f1f1f1' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:760px;margin:0 auto;'>
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white' align='center'>
		<tr>
			<td height='132'>
				<a href='http://lastminuteszalloda.hu/'><img style='display:block' src='http://www.szallasoutlet.hu/images/lmb-header.jpg' 
					alt='' 
					title='' width='760'/></a>
			</td>
		</tr>
		<tr>
		<td>
		
		
		<div style='background-color:#d5491f;color:white;'>
			<div style='text-align:center;padding:20px 10px 10px 10px;'>
				<a href='[@offer1_url]' title='[@offer1_title]'>
					<img src='[@offer1_image]' alt='[@offer1_title]' title='[@offer1_title]' style='border:5px solid white;'/>
				</a>
			</div>
			<div style='margin:10px 40px 0 40px'>
			
		
			
					<table>
				<tr>
					<td width='440'>
					
					<div style='width:439px;border-right:1px solid #d2d2d2;'>
						<div style='font-size:20px;margin:0 0 10px 0;'><b>[@offer1_city]</b></div>					
						
						<div><a href='[@offer1_url]' style='font-size:30px; text-transform:uppercase; text-decoration:none; color:white; font-weight:bold;'>[@offer1_title]</a></div>
					
						<div style='font-size:20px;margin:5px 0 0 0;'>[@offer1_subtitle]</div>
					
						<div style='font-size:18px;font-weight:bold;margin:20px 0 0 0;'>[@offer1_percent]% kedvezménnyel:</div>
						<div style='font-size:40px;color:white;font-weight:bold;'>[@offer1_price] <span style='font-size:20px'>[@offer1_short_description]</span></div>

					</div>
					</td>
					<td valign='top'>
					<div style='margin:0px 0 0 10px;'>

					<table border='0' width='220' cellspacing='0' cellpadding='0'>
						<tr>
							<td height='110' valign='top'>[@offer1_description]	</td>
						</tr>
						<tr>
							<td valign='top'>
								<div style='text-align:center;'>
									<a href='[@offer1_url]'><img src='http://szallasoutlet.hu/images/lmbmore.jpg'/></a>	
			
								</div>
							</td>
						</tr>

					</table>
					
				
					</div>
					
				
							
					</td>
				</tr>
			</table>
			
			<div style='height:10px 0 0 0;clear:both;'>&nbsp;</div>
			</div>
			
			</div>
			
			<div style='margin:10px 40px 0 40px'>
	
			
			<div style='height:10px; margin:10px 0 5px 0;border-top:1px solid #d2d2d2;'>&nbsp;</div>	
			<table width='680'>
				<tr>
					<td>
						<!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer2_url]'><img src='[@offer2_image]'/></a></div>
					
					<div style='margin:10px 0px 0;height:40px;'>
						<a href='[@offer2_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#0292ba; font-weight:bold;'>[@offer2_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer2_city]</b></div>					

					<div style='font-size:14px;margin:0 0 10px 0;color:#0292ba'><b>[@offer2_subtitle]</b></div>					


					<div style='font-size:14px;font-weight:bold;margin:5px 0 5px 0;'><span style='font-size:16px;color:#d5491f'>[@offer2_percent]%</span> kedvezménnyel:</div>
						<div style='font-size:18px;color:#0292ba;font-weight:bold;'>[@offer2_price] <span style='font-size:12px; color:black;'>[@offer2_short_description]</span></div>
					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer2_url]'><img src='http://szallasoutlet.hu/images/lmbmore2.jpg'/></a>
					</div>
					</div>
						<!-- -->
					</td>
					<td valign='top'>
						<!-- -->
						<div style='width:234px;border-right:1px solid #d2d2d2; border-left:1px solid #d2d2d2;margin:0 15px 0 15px'>
						
						<div style='margin:0 17px 0 17px;width:200px;'>
						
						
					<div style='height:117px;'><a href='[@offer3_url]'><img src='[@offer3_image]' /></a></div>
					
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer3_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#0292ba;font-weight:bold;'>[@offer3_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer3_city]</b></div>					

					<div style='font-size:14px;margin:0 0 10px 0;color:#0292ba'><b>[@offer3_subtitle]</b></div>					


					<div style='font-size:14px;font-weight:bold;margin:5px 0 5px 0;'><span style='font-size:16px;color:#d5491f'>[@offer3_percent]%</span> kedvezménnyel:</div>
						<div style='font-size:18px;color:#0292ba;font-weight:bold;'>[@offer3_price] <span style='font-size:12px; color:black;'>[@offer3_short_description]</span></div>
					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer3_url]'><img src='http://szallasoutlet.hu/images/lmbmore2.jpg'/></a>
					</div>
					</div>
					</div>
						<!-- -->
						
					</td>
					<td  valign='top'><!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer4_url]'><img src='[@offer4_image]'/></a></div>
					
					<div  style='margin:10px 0 0px 0;height:40px;'>	
						<a href='[@offer4_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#0292ba; font-weight:bold;'>[@offer4_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer4_city]</b></div>					

					<div style='font-size:14px;margin:0 0 10px 0;color:#0292ba'><b>[@offer4_subtitle]</b></div>					

					<div style='font-size:14px;font-weight:bold;margin:5px 0 5px 0;'><span style='font-size:16px;color:#d5491f'>[@offer4_percent]%</span> kedvezménnyel:</div>
						<div style='font-size:18px;color:#0292ba;font-weight:bold;'>[@offer4_price] <span style='font-size:12px; color:black;'>[@offer4_short_description]</span></div>
					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer4_url]'><img src='http://szallasoutlet.hu/images/lmbmore2.jpg'/></a>
					</div>
					</div>				
					<!-- --></td>
				</tr>
				<tr>
				<td colspan='3'>			<div style='height:10px; margin:10px 0 5px 0;border-top:1px solid #d2d2d2;'>&nbsp;</div>	
</td>
				</tr>
				<tr>
					<td  valign='top'>
						<!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer5_url]'><img src='[@offer5_image]'/></a></div>
					
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer5_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#0292ba; font-weight:bold;'>[@offer5_title]</a>
					</div>
					
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer5_city]</b></div>					

					<div style='font-size:14px;margin:0 0 10px 0;color:#0292ba'><b>[@offer5_subtitle]</b></div>					


					<div style='font-size:14px;font-weight:bold;margin:5px 0 5px 0;'><span style='font-size:16px;color:#d5491f'>[@offer5_percent]%</span> kedvezménnyel:</div>
						<div style='font-size:18px;color:#0292ba;font-weight:bold;'>[@offer5_price] <span style='font-size:12px; color:black;'>[@offer5_short_description]</span></div>
					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer5_url]'><img src='http://szallasoutlet.hu/images/lmbmore2.jpg'/></a>
					</div>
					</div>						<!-- -->
					</td>
					<td  valign='top'>
						<!-- -->
						<div style='width:234px;border-right:1px solid #d2d2d2; border-left:1px solid #d2d2d2; margin:0 15px 0 15px'>
						
												<div style=' margin:0 17px 0 17px;width:200px'>


					<div><a href='[@offer6_url]'><img src='[@offer6_image]'/></a></div>
					
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer6_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#0292ba; font-weight:bold;'>[@offer6_title]</a>
					</div>
											<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer6_city]</b></div>					

					<div style='font-size:14px;margin:0 0 10px 0;color:#0292ba'><b>[@offer6_subtitle]</b></div>					


					<div style='font-size:14px;font-weight:bold;margin:5px 0 5px 0;'><span style='font-size:16px;color:#d5491f'>[@offer6_percent]%</span> kedvezménnyel:</div>
						<div style='font-size:18px;color:#0292ba;font-weight:bold;'>[@offer6_price] <span style='font-size:12px; color:black;'>[@offer6_short_description]</span></div>
					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer6_url]'><img src='http://szallasoutlet.hu/images/lmbmore2.jpg'/></a>
					</div>
					</div>
					</div>
						<!-- -->
						
					</td>
					<td  valign='top'><!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer7_url]'><img src='[@offer7_image]'/></a></div>
					
								
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer7_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#0292ba; font-weight:bold;'>[@offer7_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer7_city]</b></div>					

					<div style='font-size:14px;margin:0 0 10px 0;color:#0292ba'><b>[@offer7_subtitle]</b></div>					


					<div style='font-size:14px;font-weight:bold;margin:5px 0 5px 0;'><span style='font-size:16px;color:#d5491f'>[@offer7_percent]%</span> kedvezménnyel:</div>
						<div style='font-size:18px;color:#0292ba;font-weight:bold;'>[@offer7_price] <span style='font-size:12px; color:black;'>[@offer7_short_description]</span></div>
					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer7_url]'><img src='http://szallasoutlet.hu/images/lmbmore2.jpg'/></a>
					</div>
					</div>						<!-- --></td>
				</tr>
				
			</table>
			
			</div>
		</td>
		
		</tr>
		
		
			<tr>
			<td>
			
			
			<div style='width:330px; margin-left:215px; margin-top:10px;'>
			
										<table width='330' border='0' cellspacing='0' cellpadding='0'>
			<tr>
			
			<td width='25'><img src='http://img.hotel-world.hu/images/fb.png' align='middle' width='20'/></td>
			<td><a href='http://www.facebook.com/last.minute.belfold' target='_blank' style='text-decoration:none'>Csatlakozz oldalunkhoz a <b>Facebook</b>-on, lájkolj minket!</a></td>
			</tr></table>
			
			</div></td>
		</tr>



		<tr>
			<td align='center'><div style='margin:10px 0 0 0;font-size:10px'>&nbsp;</div></td>
		</tr>
	</table>
	</div>
	
	<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#434343; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#434343;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:#434343;text-align:center;font-size:11px;margin:px 0 0 0'>Minden jog fenntartva &copy; 2012 szallasoutlet.hu</div>
	[@track]
</td>
</tr>
	
</table>

</body><html>