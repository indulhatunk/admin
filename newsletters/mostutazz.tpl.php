<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type='text/css'>
		body, html { padding:0; margin:0; font-family:arial; color:#707170; font-size:12px;}
		a img { border:none; }
		a { color:black; }
	</style>
	<title>[@title]</title>
</head>
 <body bgcolor='#eff4f6'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#eff4f6' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:760px;margin:0 auto;'>
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white'>
	<tr>
		<td bgcolor='#eff4f6' height='20'><div style='text-align:center;'><a href='http://nl.lmb.hu/?id=[@id]' target='_blank' style='font-size:10px;color:#707170;text-decoration:none'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a></div></td>
	</tr>
	<tr>
		<td>
			<a href='http://www.mostutazz.hu/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]' target='_blank'><img alt=''usemap="#imgmap2013523115240" title='' src='http://img.hotel-world.hu/images/muheader.jpg' style="display:block"/><map id="imgmap2013523115240" name="imgmap2013523115240"><area shape="rect" alt="" title="" coords="378,47,741,90" href="#" target="" /></map></a>
		</td>
	</tr>
	<tr>
	<td>
		<table width='760' cellpadding='0' cellspacing='0' border='0'>
		<tr>
			<td height='35' width='190' valign='middle' bgcolor='#2a78b0' align='center'><a target='_blank' href='http://www.mostutazz.hu/akcios-wellness-szallodak?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_belfold' style='font-size:15px;color:white; text-decoration:none;'>WELLNESS</a></td>
			<td height='35' width='190' valign='middle' bgcolor='#2a78b0' align='center'><a target='_blank' href='http://www.mostutazz.hu/akcios-csaladbarat-szallodak?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_vizpart' style='font-size:15px;color:white; text-decoration:none;'>CSALÁDBARÁT</a></td>
			<td height='35' width='190' valign='middle' bgcolor='#2a78b0' align='center'><a target='_blank' href='http://www.mostutazz.hu/szep-kartyat-elfogado-akcios-szallodak?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_wellness' style='font-size:15px;color:white; text-decoration:none;'>SZÉP KÁRTYA</a></td>
			<td height='35' width='190' valign='middle' bgcolor='#2a78b0' align='center'><a target='_blank' href='http://www.mostutazz.hu/akcios-kastelyszallok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_gyermekbarat' style='font-size:15px;color:white; text-decoration:none;'>KASTÉLYSZÁLLÓ</a></td>
		</tr>
		</table>
	</td>
	</tr>
	<tr>
	<td align='center'>
	<!-- content -->
	<div style='border-left:1px solid #dfe4e6; border-right:1px solid #dfe4e6;width:728px;padding:0 15px 0 15px'>
				<table cellpadding='0' cellspacing='0' border='0' width='728'>
					<tr>
						<td height='20' colspan='4'></td>
					</tr>
					<tr>
						<td width='487' valign='top'><a href='[@offer1_url]' target='_blank' ><img src='[@offer1_image]'/></a></td>
						<td width='20' valign='top' bgcolor='#eff4f6' ><img src='http://www.szallasoutlet.hu/images/mleft.jpg'/></td>
						<td width='210' valign='top'  bgcolor='#eff4f6' >
							<!-- -->
							<table cellpadding='0' cellspacing='0' border='0' width='210'>
							<tr><td colspan='2' height='10'></td></tr>

							<tr><td colspan='2' valign='top' height='65'><a href='[@offer1_url]' target='_blank' style='font-size:20px;font-weight:bold;color:#2a78b0;text-decoration:none'>[@offer1_title]</a><br/><div style='font-size:14px;font-weight:normal;color:#2a78b0;'>[@offer1_city]</div></td></tr>
							<tr><td colspan='2' height='57' valign='top'>[@offer1_subtitle]</td></tr>
							<tr>
								<td>
								 <div style='text-align:right;padding:0 15px 0 0;'>
									<div><strike>[@offer1_origprice]</strike> helyett</div>
									<div style='font-size:24px;color:#ed145b;font-weight:bold;'>[@offer1_price] <span style='font-size:18px;font-weight:normal'>/ fő</span></div></div>
								</td>
								<td bgcolor='#2a78b0' height='43' width='75' style='color:white;-text-align:center;font-size:20px;' align='center'>-[@offer1_percent]%</td>
							</tr>
							<tr><td colspan='2' height='30'></td></tr>
							<tr><td colspan='2'><a href='[@offer1_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext.png'/></a></td></tr>
							</table>
							<!-- -->
							
						</td>
						<td width='10' valign='top'><img src='http://www.szallasoutlet.hu/images/mright.jpg'/></td>

					</tr>
					<tr>
						<td height='30' colspan='4'></td>
					</tr>
					
					<tr>
						<td colspan='2' valign='top'>
							<table cellpadding='0' cellspacing='0' border='0' width='489'>
							<tr>
								<td width='179'><a href='http://www.mostutazz.hu/?utm_source=[@ga_prefix]_popular&utm_medium=[@nldate]'><img src='http://www.szallasoutlet.hu/images/mbtn1.jpg'/></a></td>
								<td><a href='http://www.mostutazz.hu/?utm_source=[@ga_prefix]_cities&utm_medium=[@nldate]'><img src='http://www.szallasoutlet.hu/images/mbtn2.jpg' width='178'/></a></td>

								<td><a href='http://www.mostutazz.hu/promocios-ajanlat/?utm_source=[@ga_prefix]_promotion&utm_medium=[@nldate]'><img src='http://www.szallasoutlet.hu/images/mbtn3.jpg' width='133'/></a></td>

							</tr>
						
							<tr>
							<td colspan='3'>
								<div style='width:457px;border-left:1px solid #c6ced1;border-right:1px solid #c6ced1;padding:15px;'>
								<table cellpadding='0' cellspacing='0' border='0' width='456'>
									<!-- -->
									
								<tr bgcolor='#eff4f6'>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft1.jpg'/></td>
									<td width='125'><a href='[@offer2_url]'><img src='[@offer2_image]'/></a></td>
									<td width='12'></td>
									<td width='185' valign='top'>
										<div style='padding:15px 0 0 0'>
										<a href='[@offer6_url]' style='color:#2a78b0;font-weight:normal;font-size:18px;text-decoration:none;'>[@offer2_title]</a>
										<div style='color:#2a78b0;font-size:12px;padding:5px 0 0 0;'>[@offer2_city]</div>
										<div style='padding:10px 0 0 0;font-weight:bold;'>[@offer2_subtitle]</div>
										</div>
									</td>
									<td width='114' valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='112'>
										<tr>
											<td bgcolor='2a78b0' style='color:white;font-weight:bold;font-size:24px;text-align:center;' height='55'>-[@offer2_percent]%</td>
										</tr>
										<tr>
											<td><img src='http://www.szallasoutlet.hu/images/mleft3.jpg'/></td>
										</tr>
										<tr>
											<td height='18'></td>
										</tr>
										<tr>
											<td><a href='[@offer2_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext3	.png' width='114'/></a></td>
										</tr>
										</table>
									</td>
									<td width='6'></td>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft2.jpg'/></td>
								</tr>	
								<tr>
									<td colspan='7' height='20'></td>
								</tr>
								
									<!-- -->
								
										<!-- -->
									
								<tr bgcolor='#eff4f6'>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft1.jpg'/></td>
									<td width='125'><a href='[@offer3_url]'><img src='[@offer3_image]'/></a></td>
									<td width='12'></td>
									<td width='185' valign='top'>
										<div style='padding:15px 0 0 0'>
										<a href='[@offer6_url]' style='color:#2a78b0;font-weight:normal;font-size:18px;text-decoration:none;'>[@offer3_title]</a>
										<div style='color:#2a78b0;font-size:12px;padding:5px 0 0 0;'>[@offer3_city]</div>
										<div style='padding:10px 0 0 0;font-weight:bold;'>[@offer3_subtitle]</div>
										</div>
									</td>
									<td width='114' valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='112'>
										<tr>
											<td bgcolor='2a78b0' style='color:white;font-weight:bold;font-size:24px;text-align:center;' height='55'>-[@offer3_percent]%</td>
										</tr>
										<tr>
											<td><img src='http://www.szallasoutlet.hu/images/mleft3.jpg'/></td>
										</tr>
										<tr>
											<td height='20'></td>
										</tr>
										<tr>
											<td><a href='[@offer3_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext3	.png' width='114'/></a></td>
										</tr>
										</table>
									</td>
									<td width='6'></td>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft2.jpg'/></td>
								</tr>	
								<tr>
									<td colspan='7' height='20'></td>
								</tr>
								
									<!-- -->
											<!-- -->
									
								<tr bgcolor='#eff4f6'>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft1.jpg'/></td>
									<td width='125'><a href='[@offer2_url]'><img src='[@offer4_image]'/></a></td>
									<td width='12'></td>
									<td width='185' valign='top'>
										<div style='padding:15px 0 0 0'>
										<a href='[@offer4_url]' style='color:#2a78b0;font-weight:normal;font-size:18px;text-decoration:none;'>[@offer4_title]</a>
										<div style='color:#2a78b0;font-size:12px;padding:5px 0 0 0;'>[@offer4_city]</div>
										<div style='padding:10px 0 0 0;font-weight:bold;'>[@offer4_subtitle]</div>
										</div>
									</td>
									<td width='114' valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='112'>
										<tr>
											<td bgcolor='2a78b0' style='color:white;font-weight:bold;font-size:24px;text-align:center;' height='55'>-[@offer4_percent]%</td>
										</tr>
										<tr>
											<td><img src='http://www.szallasoutlet.hu/images/mleft3.jpg'/></td>
										</tr>
										<tr>
											<td height='18'></td>
										</tr>
										<tr>
											<td><a href='[@offer4_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext3	.png' width='114'/></a></td>
										</tr>
										</table>
									</td>
									<td width='6'></td>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft2.jpg'/></td>
								</tr>	
								<tr>
									<td colspan='7' height='20'></td>
								</tr>
								
									<!-- -->
											<!-- -->
									
								<tr bgcolor='#eff4f6'>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft1.jpg'/></td>
									<td width='125'><a href='[@offer5_url]'><img src='[@offer5_image]'/></a></td>
									<td width='12'></td>
									<td width='185' valign='top'>
										<div style='padding:15px 0 0 0'>
										<a href='[@offer6_url]' style='color:#2a78b0;font-weight:normal;font-size:18px;text-decoration:none;'>[@offer5_title]</a>
										<div style='color:#2a78b0;font-size:12px;padding:5px 0 0 0;'>[@offer5_city]</div>
										<div style='padding:10px 0 0 0;font-weight:bold;'>[@offer5_subtitle]</div>
										</div>
									</td>
									<td width='114' valign='top'>
										<table cellpadding='0' cellspacing='0' border='0' width='112'>
										<tr>
											<td bgcolor='2a78b0' style='color:white;font-weight:bold;font-size:24px;text-align:center;' height='55'>-[@offer5_percent]%</td>
										</tr>
										<tr>
											<td><img src='http://www.szallasoutlet.hu/images/mleft3.jpg'/></td>
										</tr>
										<tr>
											<td height='18'></td>
										</tr>
										<tr>
											<td><a href='[@offer5_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext3	.png' width='114'/></a></td>
										</tr>
										</table>
									</td>
									<td width='6'></td>
									<td width='7'><img src='http://www.szallasoutlet.hu/images/mleft2.jpg'/></td>
								</tr>	
																
									<!-- -->
								</table>
								</div>
							</td>
							</tr>
							<tr>
							<td colspan='3'>
								<img src='http://www.szallasoutlet.hu/images/mbottom.jpg'/>
							</td>
							</tr>
							</table>
						</td>
						<td>
							<!-- -->
							<table cellpadding='0' cellspacing='0' border='0'>
							<!-- -->
								<tr>
									<td><a href='[@offer6_url]'><img src='[@offer6_image]'/></a></td>
									<td width='10'></td>
									<td valign='top'>
											<div style='height:30px;'><a style='color:#2a78b0;font-size:13px;text-decoration:none;' href='[@offer6_url]'>[@offer6_title]</a></div>
											<div style='padding:2px 0 9px 0;color:#ed145b;font-weight:bold;font-size:16px;text-align:right;'>[@offer6_price] <span style='font-size:14px;font-weight:normal'>/ fő</span></div>
											<a href='[@offer6_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext2.png' width='95' align='right'/></a></td>
								</tr>	
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
							<!-- -->
							<!-- -->
								<tr>
									<td><a href='[@offer7_url]'><img src='[@offer7_image]'/></a></td>
									<td width='10'></td>
									<td valign='top'>
											<div style='height:30px;'><a style='color:#2a78b0;font-size:13px;text-decoration:none;' href='[@offer7_url]'>[@offer7_title]</a></div>
											<div style='padding:2px 0 9px 0;color:#ed145b;font-weight:bold;font-size:16px;text-align:right;'>[@offer7_price] <span style='font-size:14px;font-weight:normal'>/ fő</span></div>
											<a href='[@offer7_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext2.png' width='95' align='right'/></a></td>
								</tr>	
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
							<!-- -->

<!-- -->
								<tr>
									<td><a href='[@offer8_url]'><img src='[@offer8_image]'/></a></td>
									<td width='10'></td>
									<td valign='top'>
											<div style='height:30px;'><a style='color:#2a78b0;font-size:13px;text-decoration:none;' href='[@offer8_url]'>[@offer8_title]</a></div>
											<div style='padding:2px 0 9px 0;color:#ed145b;font-weight:bold;font-size:16px;text-align:right;'>[@offer8_price] <span style='font-size:14px;font-weight:normal'>/ fő</span></div>
											<a href='[@offer8_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext2.png' width='95' align='right'/></a></td>
								</tr>	
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
							<!-- -->

<!-- -->
								<tr>
									<td><a href='[@offer9_url]'><img src='[@offer9_image]'/></a></td>
									<td width='10'></td>
									<td valign='top'>
											<div style='height:30px;'><a style='color:#2a78b0;font-size:13px;text-decoration:none;' href='[@offer9_url]'>[@offer9_title]</a></div>
											<div style='padding:2px 0 9px 0;color:#ed145b;font-weight:bold;font-size:16px;text-align:right;'>[@offer9_price] <span style='font-size:14px;font-weight:normal'>/ fő</span></div>
											<a href='[@offer9_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext2.png' width='95' align='right'/></a></td>
								</tr>	
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
							<!-- -->

<!-- -->
								<tr>
									<td><a href='[@offer10_url]'><img src='[@offer10_image]'/></a></td>
									<td width='10'></td>
									<td valign='top'>
											<div style='height:30px;'><a style='color:#2a78b0;font-size:13px;text-decoration:none;' href='[@offer10_url]'>[@offer10_title]</a></div>
											<div style='padding:2px 0 9px 0;color:#ed145b;font-weight:bold;font-size:16px;text-align:right;'>[@offer10_price] <span style='font-size:14px;font-weight:normal'>/ fő</span></div>
											<a href='[@offer10_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext2.png' width='95' align='right'/></a></td>
								</tr>	
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
							<!-- -->

<!-- -->
								<tr>
									<td><a href='[@offer11_url]'><img src='[@offer11_image]'/></a></td>
									<td width='10'></td>
									<td valign='top'>
											<div style='height:30px;'><a style='color:#2a78b0;font-size:13px;text-decoration:none;' href='[@offer11_url]'>[@offer6_title]</a></div>
											<div style='padding:2px 0 9px 0;color:#ed145b;font-weight:bold;font-size:16px;text-align:right;'>[@offer11_price] <span style='font-size:14px;font-weight:normal'>/ fő</span></div>
											<a href='[@offer11_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext2.png' width='95' align='right'/></a></td>
								</tr>	
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
							<!-- -->

<!-- -->
								<tr>
									<td><a href='[@offer12_url]'><img src='[@offer12_image]'/></a></td>
									<td width='10'></td>
									<td valign='top'>
											<div style='height:30px;'><a style='color:#2a78b0;font-size:13px;text-decoration:none;' href='[@offer12_url]'>[@offer12_title]</a></div>
											<div style='padding:2px 0 9px 0;color:#ed145b;font-weight:bold;font-size:16px;text-align:right;'>[@offer12_price] <span style='font-size:14px;font-weight:normal'>/ fő</span></div>
											<a href='[@offer12_url]' target='_blank'><img src='http://www.szallasoutlet.hu/images/mnext2.png' width='95' align='right'/></a></td>
								</tr>	
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
							<!-- -->

							</table>
							<!-- -->
						</td>
						<td></td>
					</tr>
					
						
					<!--<tr>
					<td colspan='5'>
						
						[@description]
						
					</td>
				</tr>-->
				</table>
				
							

	</div>
	<!-- content -->
	</td>
	</tr>
	<tr>
		<td>
			<img alt='' title='' src='http://img.hotel-world.hu/images/mufooter.jpg' style="display:block"/>
		</td>
	</tr>
	
	
	</table>
	</div>
	
	<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#362f2d; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#362f2d;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:#362f2d;text-align:center;font-size:11px;padding:10px 0 0 0'>Minden jog fenntartva &copy;</div>
	
			<div style='color:#362f2d;text-align:center;font-size:11px;margin:0px 0 20px 0'><img  class="image_fix" alt="az indulhatunk.hu cégcsoport tagja" title="az indulhatunk.hu cégcsoport tagja" src='http://img.hotel-world.hu/images/corporate.png' width='240' height='48'/></div>

</td>
</tr>
	
</table>
</body></html>