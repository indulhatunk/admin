<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>[@title]</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ec381a'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='40' align='left'><a target='_blank' href="http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]&utm_campaign=btn_minden"><img  class="image_fix" alt="Az összes ajánlat megtekintése" title="Az összes ajánlat megtekintése" src='http://img.hotel-world.hu/images/newlogo.jpg' width='230' height='40'/></a></td>
						<td width="200" valign="middle" align='right' style='color:white;'>[@subtitle_1]</td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ffffff'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='10' align='left' style='color:#ec381a; font-size:11px;'>SZÁLLÁS OUTLET ÉRTESÍTŐ</td>
						<td width="200" valign="middle" align='right' style='color:#ec381a; font-size:11px;'>
							<a href="http://www.szallasoutlet.hu/?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_belfold" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">BELFÖLD</a>  |  <a href="http://www.szallasoutlet.hu/vizpart-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_vizpart" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">VÍZPART</a>  |  <a href="http://www.szallasoutlet.hu/wellness-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_wellness" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">WELLNESS</a>  |  <a href="http://www.szallasoutlet.hu/gyermekbarat-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_gyermekbarat" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">GYERMEKBARÁT</a></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
					
			<tr>
				<td valign='top'>
				<!-- -->
						<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='10'></td>
									<td style='font-size:16px;font-weight:bold;line-height:18px' colspan='2'>
										<span style='color:#ed391b'>KEDVES %NAME%!</span> <br/>
										
										<span style='color:#585858;font-weight:normal;'>Észrevettük, hogy érdekelt téged az alábbi szállásajánlat:</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<a href='[@offer1_url]' target='_blank'><img  class="image_fix" alt="[@offer1_title]" title="[@offer1_title]" src='[@offer1_image]' width='560' height='240'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td width='10'></td>
											<td colspan='3' height='17' style='font-size:16px;font-weight:bold;color:#585858;'>[@offer1_subject_field]</td>
										</tr>
										<tr>
											<td colspan='4' height='15'></td>
										</tr>
										<tr>
											<td width='10'></td>
											<td colspan='3' height='80' style='font-size:15px;font-weight:normal;color:#585858;line-height:18px;' valign='top'>
												A szállás kupont most <b>[@offer1_subtitle] [@offer1_percent]%</b> kedvezménnyel vásárolhatod meg. De ne késlekedj mert a szabad helyek vészesen fogynak! Ha kérdésed <br/>lenne a foglalás menetével vagy a szállással kapcsolatban, kérünk, vedd fel velünk a kapcsolatot.
												
											</td>
										</tr>
										<tr>
											<td colspan='4' height='5'></td>
										</tr>
										<tr>
											<td width='255' colspan='2' height='38' bgcolor='3b3e40' valign='middle' style='backgrund-color:#3b3e40;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="mailto:vtl@indulhatunk.hu?subject=[@offer1_title_url]" target ="_blank" title="" style="color: white; text-decoration: none;">KÉRDÉSEM VAN</a></td>
											<td width='50'></td>
											<td width='255' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer1_url]" target ="_blank" title="" style="color: white; text-decoration: none;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
						
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#ed391b; font-weight:bold;' valign='middle' height='48' width='535'>
								MÁSOK ÍGY ÉRTÉKELTÉK A SZÁLLÁST
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/arrow.jpg' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#585858;font-weight:bold;'>[@review1_name]</td>
									<td valign='middle' style='font-size:14px; color:#585858;line-height:16px;'>
										<div style='padding:5px 0 5px 0;'>"[@review1_text]"</div>
									</td>
									<td width='10' valign='middle'></td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#585858;font-weight:bold;'>[@review2_name]</td>
									<td valign='middle' style='font-size:14px; color:#585858;line-height:16px;'>
										<div style='padding:5px 0 5px 0;'>"[@review2_text]"</div>
									</td>
									<td width='10' valign='middle'></td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#585858;font-weight:bold;'>[@review3_name]</td>
									<td valign='middle' style='font-size:14px; color:#585858;line-height:16px;'>
										<div style='padding:5px 0 5px 0;'>"[@review3_text]"</div>
									</td>
									<td width='10' valign='middle'></td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#585858;font-weight:bold;'>[@review4_name]</td>
									<td valign='middle' style='font-size:14px; color:#585858;line-height:16px;'>
										<div style='padding:5px 0 5px 0;'>"[@review4_text]"</div>
									</td>
									<td width='10' valign='middle'></td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style=''>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#585858;font-weight:bold;'>[@review5_name]</td>
									<td valign='middle' style='font-size:14px; color:#585858;line-height:16px;'>
										<div style='padding:5px 0 5px 0;'>"[@review5_text]"</div>
									</td>
									<td width='10' valign='middle'></td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>

					<tr>
						<td colspan='3' height='10'></td>
					</tr>

				</table>
			</td>
			</tr>
			<tr>	
				<td height='15'></td>
			</tr>
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="http://www.szallasoutlet.hu/minden-szallaskupon?utm_source=[@ga_prefix]&utm_medium=[@nldate]&utm_campaign=btn_followup" target ="_blank" title="Megnézem az összes ajánlatot" style="color:white; text-decoration: none;">MEGNÉZEM AZ ÖSSZES AJÁNLATOT</a></td>
			</tr>	
			<tr>	
				<td height='15'></td>
			</tr>	
			<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
				
A hírlevélben feltüntetett árak, képek, és a szolgáltatások leírásai tájékoztató jellegűek és a figyelem felkeltésére szolgálnak, így nem minősülnek konkrét ajánlattételnek. Az ajánlatok részletes leírása a linkekre kattintva honlapunkon érhető el.


				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class="image_fix" alt="Kiemelt partnereink" title="Kiemelt partnereink" src='http://img.hotel-world.hu/images/plogos.jpg' width='339' height='33'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
		</table>
		</td>
	</tr>
</table>  
</body>
</html>