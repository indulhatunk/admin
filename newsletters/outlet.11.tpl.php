<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>[@title]</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ec381a'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='40' align='left'><a target='_blank' href="http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]&utm_campaign=btn_minden"><img  class="image_fix" alt="Az összes ajánlat megtekintése" title="Az összes ajánlat megtekintése" src='http://img.hotel-world.hu/images/newlogo.jpg' width='230' height='40'/></a></td>
						<td width="200" valign="middle" align='right' style='color:white;'>[@subtitle_1]</td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ffffff'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='10' align='left' style='color:#ec381a; font-size:11px;'>SZÁLLÁS OUTLET HÍRLEVÉL - [@date]</td>
						<td width="200" valign="middle" align='right' style='color:#ec381a; font-size:11px;'>
							<a href="http://www.szallasoutlet.hu/?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_belfold" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">BELFÖLD</a>  |  <a href="http://www.szallasoutlet.hu/vizpart-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_vizpart" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">VÍZPART</a>  |  <a href="http://www.szallasoutlet.hu/wellness-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_wellness" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">WELLNESS</a>  |  <a href="http://www.szallasoutlet.hu/gyermekbarat-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_gyermekbarat" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">GYERMEKBARÁT</a></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
					
			<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>1</td>
									<td width='20'></td>
									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer1_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer1_city]</span> <br/>
										
										<span style='color:#585858'>[@offer1_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='15'></td>
								</tr>
								<tr>
									<td colspan='4'>
										<a href="[@offer1_url]" target ="_blank" title="[@offer1_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer1_title]" title="[@offer1_title]" src='[@offer1_image]' width='560' height='240'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='3'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer1_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer1_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer1_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer1_url]" target ="_blank" title="[@offer1_title]" style="color: white; text-decoration: none;display:block;width:200px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
			
			<tr>	
				<td height='15'></td>
			</tr>
			
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>2</td>
									<td width='20'></td>

									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer2_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer2_city]</span> <br/>
										
										<span style='color:#585858'>[@offer2_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='15'></td>
								</tr>
								<tr>
									<td colspan='4'>
										<a href="[@offer2_url]" target ="_blank" title="[@offer2_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer2_title]" title="[@offer2_title]" src='[@offer2_image]' width='560' height='240'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='3'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer2_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer2_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer2_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer2_url]" target ="_blank" title="[@offer1_title]" style="color: white; text-decoration: none;display:block;width:200px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
			
			<tr>	
				<td height='15'></td>
			</tr>
						
		
						
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>3</td>
									<td width='20'></td>

									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer3_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer3_city]</span> <br/>
										
										<span style='color:#585858'>[@offer3_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='15'></td>
								</tr>
								<tr>
									<td colspan='4'>
										<a href="[@offer3_url]" target ="_blank" title="[@offer3_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer3_title]" title="[@offer3_title]" src='[@offer3_image]' width='560' height='150'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='3'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer3_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer3_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer3_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer3_url]" target ="_blank" title="[@offer1_title]" style="color: white; text-decoration: none;display:block;width:200px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
						
			<!-- -->
			
			<tr>	
				<td height='15'></td>
			</tr>
						
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>4</td>
									<td width='20'></td>

									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer4_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer4_city]</span> <br/>
										
										<span style='color:#585858'>[@offer4_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='15'></td>
								</tr>
								<tr>
									<td colspan='4'>
										<a href="[@offer4_url]" target ="_blank" title="[@offer4_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer4_title]" title="[@offer4_title]" src='[@offer4_image]' width='560' height='150'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='3'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer4_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer4_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer4_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer4_url]" target ="_blank" title="[@offer1_title]" style="color: white; text-decoration: none;display:block;width:200px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
			
			<tr>	
				<td height='15'></td>
			</tr>
					
						
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>5</td>
									<td width='20'></td>

									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer5_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer5_city]</span> <br/>
										
										<span style='color:#585858'>[@offer5_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='15'></td>
								</tr>
								<tr>
									<td colspan='4'>
										<a href="[@offer5_url]" target ="_blank" title="[@offer5_title]" style="color: #454545; text-decoration: none;"><img  class="image_fix" alt="[@offer5_title]" title="[@offer5_title]" src='[@offer5_image]' width='560' height='150'/></a>
									</td>
								</tr>
								<tr>
									<td colspan='4' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='3'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer5_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer5_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer5_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'><a href="[@offer5_url]" target ="_blank" title="[@offer1_title]" style="color: white; text-decoration: none;display:block;width:200px;height:25px;text-align:center;padding:13px 0 0 0;">MEGNÉZEM</a></td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
			<tr>	
				<td height='15'></td>
			</tr>
					
							
			
			
			<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style=''>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>6</td>
									<td width='20'></td>
									<td width='55' style='font-size:24px;color:#ec381a;font-weight:bold;text-align:center;'>-[@offer6_percent]%</td>
									<td width='20'></td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer6_url]" target="_blank" title="[@offer6_title]" style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer6_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer6_city]</div></a>
									</td>
									<td width='120' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold; text-align:right;'>[@offer6_price]									</td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
								
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					
					
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style=''>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>7</td>
									<td width='20'></td>
									<td width='55' style='font-size:24px;color:#ec381a;font-weight:bold;text-align:center;'>-[@offer7_percent]%</td>
									<td width='20'></td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer7_url]" target="_blank" title="[@offer7_title]" style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer7_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer7_city]</div></a>
									</td>
									<td width='120' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold; text-align:right;'>[@offer7_price]									</td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
								
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
						
						<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style=''>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>8</td>
									<td width='20'></td>
									<td width='55' style='font-size:24px;color:#ec381a;font-weight:bold;text-align:center;'>-[@offer8_percent]%</td>
									<td width='20'></td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer8_url]" target="_blank" title="[@offer8_title]" style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer8_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer8_city]</div></a>
									</td>
									<td width='120' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold; text-align:right;'>[@offer8_price]									</td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
								
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					
						<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style=''>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>9</td>
									<td width='20'></td>
									<td width='55' style='font-size:24px;color:#ec381a;font-weight:bold;text-align:center;'>-[@offer9_percent]%</td>
									<td width='20'></td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer9_url]" target="_blank" title="[@offer9_title]" style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer9_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer9_city]</div></a>
									</td>
									<td width='120' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold; text-align:right;'>[@offer9_price]									</td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					
						<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style=''>
									<td width='40' style='background-color:#585858;text-align:center;color:white;font-size:24px;font-weight:bold;'>10</td>
									<td width='20'></td>
									<td width='55' style='font-size:24px;color:#ec381a;font-weight:bold;text-align:center;'>-[@offer10_percent]%</td>
									<td width='20'></td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<a href="[@offer10_url]" target="_blank" title="[@offer10_title]" style='font-size:14px; color:#585858;text-decoration:none'><b>[@offer10_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer10_city]</div></a>
									</td>
									<td width='120' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold; text-align:right;'>[@offer10_price]									</td>
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
								
					<tr>
						<td colspan='3' height='20'></td>
					</tr>				

				</table>
			</td>
			</tr>

	
					
			<tr>	
				<td height='15'></td>
			</tr>
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="http://www.szallasoutlet.hu/minden-szallaskupon?utm_source=[@ga_prefix]&utm_medium=[@nldate]&utm_campaign=btn_minden" target ="_blank" title="Megnézem az összes ajánlatot" style="color:white; text-decoration: none;">MEGNÉZEM AZ ÖSSZES AJÁNLATOT</a></td>
			</tr>	
			<tr>	
				<td height='15'></td>
			</tr>	
			<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
				
A hírlevélben feltüntetett árak, képek, és a szolgáltatások leírásai tájékoztató jellegűek és a figyelem felkeltésére szolgálnak. Az ajánlatok részletes leírása a linkekre kattintva honlapunkon érhető el. Levelünket a #EMAIL# címre küldtük, mivel korábban feliratkozott hírlevél szolgáltatásunkra. Amennyiben nem kíván több levelet kapni tőlünk, kérjük kattintson az alábbi linkre:<br/><a href="#URL#" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: underline;">Leiratkozás</a>

				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class="image_fix" alt="az indulhatunk.hu cégcsoport tagja" title="az indulhatunk.hu cégcsoport tagja" src='http://img.hotel-world.hu/images/corporate.png' width='240' height='48'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
		</table>
		</td>
	</tr>
</table>  
</body>
</html>