<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>[@title]</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ec381a'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='40' align='left'><a target='_blank' href="http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]&utm_campaign=btn_minden"><img  class="image_fix" alt="Az összes ajánlat megtekintése" title="Az összes ajánlat megtekintése" src='http://img.hotel-world.hu/images/newlogo.jpg' width='230' height='40'/></a></td>
						<td width="200" valign="middle" align='right' style='color:white;'>Már csak pár nap és megkezdődik a pihenés!</td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ffffff'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='10' align='left' style='color:#ec381a; font-size:11px;'>EMLÉKEZTETŐ</td>
						<td width="200" valign="middle" align='right' style='color:#ec381a; font-size:11px;'>
							<a href="http://www.szallasoutlet.hu/?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_belfold" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">BELFÖLD</a>  |  <a href="http://www.szallasoutlet.hu/vizpart-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_vizpart" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">VÍZPART</a>  |  <a href="http://www.szallasoutlet.hu/wellness-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_wellness" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">WELLNESS</a>  |  <a href="http://www.szallasoutlet.hu/gyermekbarat-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_gyermekbarat" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">GYERMEKBARÁT</a></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
					
			<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='10'></td>
									<td>
										<span style='font-size:16px;color:#ec381a;font-weight:bold;'>KEDVES %FULL NAME%!</span> <br/>
										
										<div style='padding:5px 0 0 0;color:#585858;font-weight:normal;font-size:16px'>Ugye nem felejtetted el? Pár nap és kezdetét veszi a pihenés!
</div>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<img  class="image_fix" alt="[@offer1_title]" title="[@offer1_title]" src='http://admin.indulhatunk.hu/vouchers/partners2/240163877.jpg' width='560' height='185'/>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='20'></td>
								</tr>
							<!--	<tr>
									<td width='10'></td>
									<td>
										<span style='color:#585858;font-weight:bold;font-size:16px'> - Debrecen</span>
									</td>
								</tr>
								
								<tr>
									<td cospan='2' height='20'></td>
								</tr>
								-->
								<tr>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0" align="center"  bgcolor='white'>
											<tr>
												<td width='15'></td>
												
												<td width='255'>
													  <table class="calendar-date" style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;border-spacing:0;border-radius:2px;border-collapse:collapse;text-align:center;width:100%">
<tbody><tr style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif">
<td style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;border: 1px solid #d3d3d3; padding-top: 15px; padding-bottom: 15px; font-size: 14px; color: #6f6f6f; border-top-left-radius: 2px; border-top-right-radius: 2px;">
     ÉRKEZÉS
    </td>
  </tr>
<tr style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif">
<td style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;background: #EEEEEE; border: 1px solid #d3d3d3; border-bottom: none; padding-top: 20px; color: #6f6f6f; font-size: 18px;">
     csütörtök, április
    </td>
  </tr>
<tr style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif">
<td style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;background: #EEEEEE; font-size: 55px; border: 1px solid #d3d3d3; border-top: none; font-weight: bold; line-height: 60px; padding-bottom: 20px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px;">
      11
    </td>
  </tr>
</tbody></table>
												</td>
												
												<td width='70' align='center' valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/rarr.jpg' width='12' height='15'/></td>
												
												<td width='255'>
													  <table class="calendar-date" style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;border-spacing:0;border-radius:2px;border-collapse:collapse;text-align:center;width:100%">
<tbody><tr style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif">
<td style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;border: 1px solid #d3d3d3; padding-top: 15px; padding-bottom: 15px; font-size: 14px; color: #6f6f6f; border-top-left-radius: 2px; border-top-right-radius: 2px;">
     TÁVOZÁS
    </td>
  </tr>
<tr style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif">
<td style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;background: #EEEEEE; border: 1px solid #d3d3d3; border-bottom: none; padding-top: 20px; color: #6f6f6f; font-size: 18px;">
     csütörtök, április
    </td>
  </tr>
<tr style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif">
<td style="margin:0;padding:0;font-family;Helvetica Neue;Helvetica; Helvetica, Arial, sans-serif;background: #EEEEEE; font-size: 55px; border: 1px solid #d3d3d3; border-top: none; font-weight: bold; line-height: 60px; padding-bottom: 20px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px;">
      11
    </td>
  </tr>
</tbody></table>
													
												</td>
												
												<td width='15'></td>
												
												
											
											</tr>
										</table>										
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='30'></td>
						</tr>
						
						
					</table>
				<!-- -->	
				</td>
				
			</tr>
		
		
		<!-- -->
	

				<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#ed391b; font-weight:bold;' valign='middle' height='48' width='535'>
								A HOTEL DIVINUS MEGKÖZELÍTÉSE:
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/arrow.jpg' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			
				<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr>
									<td width='270'>
										<img src='http://maps.googleapis.com/maps/api/staticmap?center=40.702147,-74.015794&zoom=13&size=270x185&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&sensor=false' width='270' height='185'/>

									</td>
									<td width='20'>
									
									</td>
									<td width='270'>
										<table cellpadding="0" cellspacing="0" border="0" width='100%'>
											
											<tr>
												<td colspan='3' height='12'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>4032 Debrecen,  Nagyerdei körút 1. </td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iphone.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>+36 52 510 900</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/imail.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:14px;color:#585858;line-height:16px;' valign='middle'>info@hoteldivinus.hu</td>
											</tr>
											
											<tr>
												<td colspan='3' height='25'></td>
											</tr>


											<tr>
												<td colspan='3' height='10' style='background-color:#494949;height:40px;text-align:center;' valign='middle'>
													<a href='http://www.google.com/maps/dir/1056%20Budapest%20Váci%20utca%209/1195%20Budapest%20Zrínyi%20utca%201/am=t/' target="_blank" style='font-size:16px;font-weight:bold;color:white;text-decoration:none;'>ÚTVONALTERVEZÉS</a>
												</td>
											</tr>

											</table>
									</td>								
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>					
					<tr>
						<td colspan='3' height='20'></td>
					</tr>

				</table>
			</td>
			</tr>
			
			
						
		<!-- -->
		
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#ed391b; font-weight:bold;' valign='middle' height='48' width='535'>
								HASZNOS TANÁCSOK:
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/arrow.jpg' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			
				<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr>
									<td>
										fdsfafsfsadsfsf
									</td>								
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>					
					<tr>
						<td colspan='3' height='10'></td>
					</tr>

				</table>
			</td>
			</tr>


		
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#ed391b; font-weight:bold;' valign='middle' height='48' width='535'>
								A SZÁLLODA AKUTÁLIS AJÁNLATAI:
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/arrow.jpg' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			
				<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr>
									<td>
										fdsfafsfsadsfsf
									</td>								
								</tr>
							</table>
							</td>
						<td width='20'></td> 
					</tr>					
					<tr>
						<td colspan='3' height='10'></td>
					</tr>

				</table>
			</td>
			</tr>





			<tr>	
				<td height='15'></td>
			</tr>
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='3B5998' height='38' align='center' valign='middle'><a href="http://www.szallasoutlet.hu/minden-szallaskupon?utm_source=[@ga_prefix]&utm_medium=[@nldate]&utm_campaign=btn_minden" target ="_blank" title="Megnézem az összes ajánlatot" style="color:white; text-decoration: none;">MEGOSZTOM A BARÁTAIMMAL!</a></td>
			</tr>	
			<tr>	
				<td height='15'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class="image_fix" alt="Kiemelt partnereink" title="Kiemelt partnereink" src='http://img.hotel-world.hu/images/plogos.jpg' width='339' height='33'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
			<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
					Minden jog fenntartva - szallasoutlet.hu &copy;2014		
				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
		</table>
		</td>
	</tr>
</table>  
</body>
</html>