<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Szifon.com WWDC14 LIVE - ÉLŐ KÖZVETÍTÉS</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #019dfc;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='019dfc'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
					<tr>
						<td width="321" valign="top" height='49' align='left'><a target='_blank' href="http://live.szifon.com/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]&utm_campaign=btn_minden"><img  class="image_fix" alt="Szifon.com WWDC14 LIVE - ÉLŐ KÖZVETÍTÉS" title="Szifon.com WWDC14 LIVE - ÉLŐ KÖZVETÍTÉS" src='http://live.szifon.com/images/szifon.png' width='321' height='49'/></a></td>
						<td valign="middle" align='right' style='color:white;'></td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ffffff'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='10' align='left' style='color:#019dfc; font-size:11px;'>WWDC14 LIVE - ÉLŐ KÖZVETÍTÉS</td>
						<td width="200" valign="middle" align='right' style='color:#019dfc; font-size:11px;'>
						</td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
					
			<tr>
				<td valign='top'>
				<!-- -->
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='1'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td colspan='1'>
										<img  class="image_fix" alt="" title="" src='http://szifon.com/wp-content/uploads/2014/05/wwdc14_cikkkozi@2x.png' width='560' height='80'/>
									</td>
								</tr>
								<tr>
									<td colspan='1' height='20'></td>
								</tr>
								<tr>
									<td>
										<!-- -->
<div style=" margin: 0 auto;">
<table style="" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td bgcolor="white" style='font-size:14px; line-height:20px;'><!-- -->
<center>
<span style='font-size:16px;color:#019dfc;font-weight:bold;'>Sziasztok Szifonosok!</span> <br/>
										
										<div style='padding:5px 0 0 0;color:#585858;font-weight:normal;font-size:16px'>Ugye nem felejtetted el? Ma WWDC14!
</div>
</center>


<div style="padding: 10px;">Ahogy azt már megszokhattátok, hétfő este is élőben közvetítjük a WWDC idei eseményét, melyen a programozás és a szoftver, tehát az iOS 8 és az OS X lesz a középpontban. Hardveres részről egyelőre nem lehet tudni, mi várható, de mivel a szlogen a szoftvert emeli ki, így új kevés az esély, hogy új eszközt is bemutat az Apple.</div>
<div style="padding: 10px;"><b>Korábban írtunk arról, hogy mire számítunk a WWDC-n, így ezeket érdemes elolvasni:</b></div>
<ul>
	<li><a href="http://szifon.com/2014/05/31/wwdc-2014-korkep-mire-szamithatunk-hetfon/" style='color:#019dfc;text-decoration:none'>WWDC 2014 körkép: mire számíthatunk hétfőn?</a></li>
	<li><a href="http://szifon.com/2014/05/30/wwdc-emlekezetes-pillanatok/" style='color:#019dfc;text-decoration:none'>WWDC – emlékezetes pillanatok</a></li>
	<li><a href="http://szifon.com/2014/05/29/wwdc14-szifon-hatterkepek-es-passbook-pass/" style='color:#019dfc;text-decoration:none'>WWDC14 – Szifon háttérképek és Passbook pass</a></li>
	<li><a href="http://szifon.com/2014/05/29/egyre-biztosabb-hogy-imac-frissites-is-erkezik-az-idei-wwdc-n/"  style='color:#019dfc;text-decoration:none'>Egyre biztosabb, hogy iMac frissítés is érkezik az idei WWDC-n</a></li>
	<li><a href="http://szifon.com/2014/05/28/felkerult-a-wwdc-diszites-a-moscone-centerben/"  style='color:#019dfc;text-decoration:none'>Felkerült a WWDC-díszítés a Moscone Centerben</a></li>
	<li><a href="http://szifon.com/2014/05/27/szifon-com-elo-kozvetites-wwdc-2014-ios-8-iwatch-itv-os-x/"  style='color:#019dfc;text-decoration:none'>Szifon.com élő közvetítés – WWDC 2014 (iOS 8, iWatch, iTV, OS X)</a></li>
	<li><a href="http://szifon.com/2014/04/03/junius-2-an-kezdodik-a-wwdc-2014/"  style='color:#019dfc;text-decoration:none'>Június 2-án kezdődik a WWDC 2014</a></li>
</ul>
<table style="margin: 0px auto; width: 420px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td height="10"></td>
</tr>
<tr>
<td style="font-size: 30px; font-weight: bold;text-align:center;" colspan="4">19:00 - <a style="color: black;" href="http://live.szifon.com" target="_blank">live.szifon.com »</a></td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td width="30"></td>
<td>
<a href="http://szifon.me/wwdc14event" target="_blank"><img class="alignnone  wp-image-42983" src="http://szifon.com/wp-content/uploads/2014/05/passbook-hero.png" alt="passbookszifon-1" width="40" /></a></td>
<td width="10"></td>
<td><b>Töltsd le a közvetítés Passbook-kártyáját!</b></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>									
										<!-- -->
									</td>
								</tr>
								
								<tr>
									<td cospan='1' height='20'></td>
								</tr>
								
						
																</table>										
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						
						<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='3B5998' height='38' align='center' valign='middle'><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flive.szifon.com" target ="_blank" title="Megnézem az összes ajánlatot" style="color:white; text-decoration: none;">MEGOSZTOM A BARÁTAIMMAL!</a></td>
			</tr>	
						<tr>
							<td colspan='3' height='15'></td>
						</tr>
					</table>
				<!-- -->	
				</td>
				
			</tr>
		
				<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
					Minden jog fenntartva - Szifon.com &copy;2014		
				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
		</table>
		</td>
	</tr>
</table>  
</body>
</html>