<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<style type='text/css'>
		body, html { padding:0; margin:0; font-family:arial; color:#707170; font-size:12px;}
		a img { border:none; }
		a { color:black; }
	</style>
	<title>[@title]</title>
</head>
 <body bgcolor='#cecece'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#cecece' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:760px;margin:0 auto;'>
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white'>
	<tr>
		<td bgcolor='#cecece' height='20'><div style='text-align:center;'><a href='http://nl.lmb.hu/?id=[@id]' target='_blank' style='font-size:10px;color:#707170;text-decoration:none'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a></div></td>
	</tr>
		<tr>
		<td width='760'>
			<table width='760' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td valign='top' width='405'><a href='http://www.szallasoutlet.hu/?utm_source=newsletter_3C_logo&utm_medium=[@nldate]' target='_blank'><img alt='' title='' src='http://img.hotel-world.hu/images/logo1.jpg' style="display:block"/></a>
					</td>
					<td valign='top' width='330'>
						<table width='330' cellpadding='0' cellspacing='0' border='0'>
							<tr>
								<td height='50' width='330'><img alt=''  title='' src='http://img.hotel-world.hu/images/logo2.jpg' style="display:block"/></td>
							</tr>
							<tr>
								<td height='68' width='330' bgcolor='#af2c01' align='center'>
									<a href='[@offer9_url]' style='color:white; font-size:20px; font-weight:bold; text-decoration:none;' target="_blank">
										[@offer9_title]<br/>
										[@offer9_city] - <span style='font-size:26px;'>[@offer9_price]</span>
									</a>
									</td>
							</tr>
							<tr>
								<td width='330' height='5'></td>
							</tr>
						</table>
						
					</td>
					<td width='25' height='123' valign='top'><img alt=''  title='' src='http://img.hotel-world.hu/images/logo3.jpg' style="display:block"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	<td>
		<table width='760' cellpadding='0' cellspacing='0' border='0'>
		<tr>
			<td height='35' width='190' valign='middle' bgcolor='#af2c01' align='center'><a target='_blank' href='http://www.szallasoutlet.hu/?utm_source=newsletter_3C&utm_medium=[@nldate]&utm_campaign=btn_belfold' style='font-size:15px;color:white; text-decoration:none;'>BELFÖLD</a></td>
			<td height='35' width='190' valign='middle' bgcolor='#006dd6' align='center'><a target='_blank' href='http://www.szallasoutlet.hu/tags/vizpart/?utm_source=newsletter_3C&utm_medium=[@nldate]&utm_campaign=btn_vizpart' style='font-size:15px;color:white; text-decoration:none;'>VÍZPART</a></td>
			<td height='35' width='190' valign='middle' bgcolor='#72af00' align='center'><a target='_blank' href='http://www.szallasoutlet.hu/tags/wellness/?utm_source=newsletter_3C&utm_medium=[@nldate]&utm_campaign=btn_wellness' style='font-size:15px;color:white; text-decoration:none;'>WELLNESS</a></td>
			<td height='35' width='190' valign='middle' bgcolor='#ff5c00' align='center'><a target='_blank' href='http://www.szallasoutlet.hu/tags/gyermekbarat/?utm_source=newsletter_3C&utm_medium=[@nldate]&utm_campaign=btn_gyermekbarat' style='font-size:15px;color:white; text-decoration:none;'>GYERMEKBARÁT</a></td>
		</tr>
		</table>
	</td>
	</tr>
	<tr>
	<td align='center'>
	<!-- content -->
	<div style='border-left:3px solid #aeaeae; border-right:3px solid #aeaeae;width:754px;'>
				<table cellpadding='0' cellspacing='0' border='0' width='700'>
					<tr>
						<td height='20' colspan='4'></td>
					</tr>
					<tr>
					<td rowspan='3' width='330'>
						<a href='[@offer1_url]' target='_blank'><img alt='[@offer1_title]' title='' src='[@offer1_image3]' width='328' height='252'/></a>
					</td>
					<td width='25' rowspan='3'></td>
					<td colspan='2' width='325'>
				
						<div style='border-bottom:1px solid #cdcecd;width:325px;padding:25px 0 10px 0'>
							<div style='height:25px;'><span style='background-color:#af2c01; color:white; font-weight:normal; text-transform:uppercase;font-size:14px;padding:3px;'>[@offer1_city]</span></div>
							<div>
								<div style='font-size:20px; color:#000000;height:28px;'>[@offer1_title]</div>
							
							[@offer1_short_description]
							
							
								<div style='height:14px;'></div>
						<a target='_blank' href='[@offer1_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more7.jpg'/></a>
						
							</div>
						</div>
							
			
						
					</td>
					</tr>
					<tr>
						<td height='5' colspan='4'></td>
					</tr>
					<tr>
						<td valign='top'>
								<div style='color:#707170;font-size:11px;'>Outlet ár:</div>
						<div style='color:#af2c01;font-size:30px;font-weight:bold;'>[@offer1_price]</div>

						</td>
						<td align='right' valign='top'>
						<div style='height:14px;'></div>
						<a target='_blank' href='[@offer1_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more6.jpg'/></a>
</td>
					</tr>
				
					<tr>
						<td height='20' colspan='4'></td>
					</tr>
						<tr>
						<td colspan='4' height='3' bgcolor='#cdcecd'></td>
					</tr>
						<tr>
						<td height='20' colspan='4'></td>
					</tr>
					<tr>
					<td rowspan='3' width='330'>
						<a href='[@offer2_url]' target='_blank'><img alt='[@offer2_title]' title='' src='[@offer2_image3]' width='328' height='252'/></a>
					</td>
					<td width='25' rowspan='3'></td>
					<td colspan='2' width='325'>
				
						<div style='border-bottom:1px solid #cdcecd;width:325px;padding:25px 0 10px 0'>
							<div style='height:25px;'><span style='background-color:#af2c01; color:white; font-weight:normal; text-transform:uppercase;font-size:14px;padding:3px;'>[@offer2_city]</span></div>
							<div>
								<div style='font-size:20px; color:#000000;height:28px;'>[@offer2_title]</div>
							
							[@offer2_short_description]
							
							
								<div style='height:14px;'></div>
						<a target='_blank' href='[@offer2_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more7.jpg'/></a>
						
							</div>
						</div>
							
			
						
					</td>
					</tr>
					<tr>
						<td height='5' colspan='4'></td>
					</tr>
					<tr>
						<td valign='top'>
								<div style='color:#707170;font-size:11px;'>Outlet ár:</div>
						<div style='color:#af2c01;font-size:30px;font-weight:bold;'>[@offer2_price]</div>

						</td>
						<td align='right' valign='top'>
						<div style='height:14px;'></div>
						<a target='_blank' href='[@offer2_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more6.jpg'/></a>
</td>
					</tr>
				
					<tr>
						<td height='20' colspan='4'></td>
					</tr>	
					
				</table>
				
				<!-- middle -->
				
				<table cellpadding='0' cellspacing='0' border='0' width='754' bgcolor='#ebebeb'>
					<tr>
						<td width='493' valign='top'>
							<!---->
							<table cellpadding='0' cellspacing='0' border='0' width='493'>
								<tr bgcolor='#76b300'>
									<td width='31'><img alt='' title='' src='http://img.hotel-world.hu/images/opng.png'/></td>
									<td colspan='4' style='font-size:18px;color:white;font-weight:bold;' height='38'>&nbsp;&nbsp;BELFÖLDI AJÁNLATUNK</td>
								</tr>
								<tr>
									<td colspan='5' height='25'></td>
								</tr>
								<tr>
									<td width='31'></td>
									<td colspan='2'>
										<a href='[@offer3_url]' target='_blank'><img  alt='[@offer3_title]' title='' src='[@offer3_image2]' width='247' height='173'/></a>
									</td>
									<td width='20'></td>
									<td>
									
									<div style='height:25px;'><span style='background-color:#76b300; color:white; font-weight:normal; text-transform:uppercase;font-size:14px;padding:3px;'>[@offer3_city]</span></div>
									<div style='font-size:16px; color:#000000;height:50px;'>[@offer3_title]</div>
							
									<div style='color:#707170;font-size:11px;'>Outlet ár:</div>
						<div style='color:#76b300;font-size:30px;font-weight:bold;'>[@offer3_price]</div>
						<div style='height:10px;'></div>


		
									<a href='[@offer3_url]' target='_blank'><img alt='' title='' src='http://img.hotel-world.hu/images/more2.jpg'/></a></td>
								</tr>
								<tr>
									<td colspan='5' height='25'></td>
								</tr>
								
									<tr bgcolor='#0071da'>
									<td width='31'><img alt='' title='' src='http://img.hotel-world.hu/images/opng.png'/></td>
									<td colspan='4' style='font-size:18px;color:white;font-weight:bold;' height='38'>&nbsp;&nbsp;KEDVENC AJÁNLATUNK</td>
								</tr>
								<tr>
									<td colspan='5' height='25'></td>
								</tr>
								<tr>
									<td width='31'></td>
									<td colspan='2'>
										<a href='[@offer4_url]' target='_blank'><img  alt='[@offer4_title]' title='' src='[@offer4_image2]' width='247' height='173'/></a>
									</td>
									<td width='20'></td>
									<td>
									
									<div style='height:25px;'><span style='background-color:#0071da; color:white; font-weight:normal; text-transform:uppercase;font-size:14px;padding:3px;'>[@offer4_city]</span></div>
									<div style='font-size:16px; color:#000000;height:50px;'>[@offer4_title]</div>
							
									<div style='color:#707170;font-size:11px;'>Outlet ár:</div>
						<div style='color:#0071da;font-size:30px;font-weight:bold;'>[@offer4_price]</div>
						<div style='height:10px;'></div>


		
									<a href='[@offer4_url]' target='_blank'><img alt='' title='' src='http://img.hotel-world.hu/images/more3.jpg'/></a></td>
								</tr>
								<tr>
									<td colspan='5' height='25'></td>
								</tr>


							</table>
							<!---->
							
						</td>
						<td width='1' bgcolor='white'></td>
						<td width='260' valign='top'>
							<!-- -->
								<table cellpadding='0' cellspacing='0' border='0' width='260'>
								<tr bgcolor='#ff5c00'>
									<td width='31'><img alt='' title='' src='http://img.hotel-world.hu/images/opng.png'/></td>
									<td width='6'></td>
									<td style='font-size:18px;color:white;font-weight:bold;' height='38'>KÜLFÖLDI</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td colspan='3' align='center'>
										<a href='[@offer7_url]'><img alt='' title='' src='[@offer7_image2]'/></a>
										
									</td>
								</tr>
								<tr>
									<td colspan='3' align='center' style='font-size:20px;color:#252525;height:40px;'>
										[@offer7_title]
										
									</td>
								</tr>
								<tr>
									<td colspan='3' align='center'>
										<a href='[@offer7_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more4.jpg'/></a>
										
									</td>
								</tr>
								<tr>
									<td colspan='3' height='21'></td>
								</tr>
								<tr>
									<td colspan='3' align='center'>
										<a href='[@offer8_url]'><img alt='' title='' src='[@offer8_image2]' width='206' height='129'/></a>
										
									</td>
								</tr>
								<tr>
									<td colspan='3' align='center' style='font-size:20px;color:#252525;height:40px;'>
										[@offer8_title]
										
									</td>
								</tr>
								<tr>
									<td colspan='3' align='center'>
										<a href='[@offer8_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more4.jpg'/></a>
										
									</td>
								</tr>
							</table>
							<!-- -->
							
						</td>
					</tr>
				</table>
				<!-- middle -->
				
							<table cellpadding='0' cellspacing='0' border='0' width='700'>
					<tr>
						<td height='20' colspan='4'></td>
					</tr>
					<tr>
					<td rowspan='3' width='330'>
						<a href='[@offer5_url]' target='_blank'><img alt='[@offer5_title]' title='' src='[@offer5_image3]' width='328' height='252'/></a>
					</td>
					<td width='25' rowspan='3'></td>
					<td colspan='2' width='325'>
				
						<div style='border-bottom:1px solid #cdcecd;width:325px;padding:25px 0 10px 0'>
							<div style='height:25px;'><span style='background-color:#af2c01; color:white; font-weight:normal; text-transform:uppercase;font-size:14px;padding:3px;'>[@offer5_city]</span></div>
							<div>
								<div style='font-size:20px; color:#000000;height:28px;'>[@offer5_title]</div>
							
							[@offer5_short_description]
							
							
								<div style='height:14px;'></div>
						<a target='_blank' href='[@offer5_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more7.jpg'/></a>
						
							</div>
						</div>
							
			
						
					</td>
					</tr>
					<tr>
						<td height='5' colspan='4'></td>
					</tr>
					<tr>
						<td valign='top'>
								<div style='color:#707170;font-size:11px;'>Outlet ár:</div>
						<div style='color:#af2c01;font-size:30px;font-weight:bold;'>[@offer5_price]</div>

						</td>
						<td align='right' valign='top'>
						<div style='height:14px;'></div>
						<a target='_blank' href='[@offer5_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more6.jpg'/></a>
</td>
					</tr>
				
					<tr>
						<td height='20' colspan='4'></td>
					</tr>
						<tr>
						<td colspan='4' height='3' bgcolor='#cdcecd'></td>
					</tr>
						<tr>
						<td height='20' colspan='4'></td>
					</tr>
					<tr>
					<td rowspan='3' width='330'>
						<a href='[@offer6_url]' target='_blank'><img alt='[@offer6_title]' title='' src='[@offer6_image3]' width='328' height='252'/></a>
					</td>
					<td width='25' rowspan='3'></td>
					<td colspan='2' width='325'>
				
						<div style='border-bottom:1px solid #cdcecd;width:325px;padding:25px 0 10px 0'>
							<div style='height:25px;'><span style='background-color:#af2c01; color:white; font-weight:normal; text-transform:uppercase;font-size:14px;padding:3px;'>[@offer6_city]</span></div>
							<div>
								<div style='font-size:20px; color:#000000;height:28px;'>[@offer6_title]</div>
							
							[@offer6_short_description]
							
							
								<div style='height:14px;'></div>
						<a target='_blank' href='[@offer6_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more7.jpg'/></a>
						
							</div>
						</div>
							
			
						
					</td>
					</tr>
					<tr>
						<td height='5' colspan='4'></td>
					</tr>
					<tr>
						<td valign='top'>
								<div style='color:#707170;font-size:11px;'>Outlet ár:</div>
						<div style='color:#af2c01;font-size:30px;font-weight:bold;'>[@offer6_price]</div>

						</td>
						<td align='right' valign='top'>
						<div style='height:14px;'></div>
						<a target='_blank' href='[@offer6_url]'><img alt='' title='' src='http://img.hotel-world.hu/images/more6.jpg'/></a>
</td>
					</tr>
				
					<tr>
						<td height='20' colspan='4'></td>
					</tr>	
					<tr>
						<td style='font-size:24px; ;color:#252525;' colspan='4'>
								További ajánlataink
				</td>
				</tr>
				</table>
				
							

	</div>
	<!-- content -->
	</td>
	</tr>
	
		<tr>
		<td>
			<img alt='' title='' src='http://img.hotel-world.hu/images/nowsep.jpg' style="display:block"/>
		</td>
	</tr>
	<tr>
		<td bgcolor='#e0e1e0'>
		[@description]
		</td>
	</tr>
	<tr>
		<td>
			<img alt='' title='' src='http://img.hotel-world.hu/images/nowfooter.jpg' style="display:block"/>
		</td>
	</tr>
	
	
	</table>
	</div>
	
	<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#707170; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#707170;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:#707170;text-align:center;font-size:11px;padding:10px 0 0 0'>Minden jog fenntartva &copy;</div>
</td>
</tr>
	
</table>
</body></html>