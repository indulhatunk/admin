<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>[@title]</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ec381a'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='40' align='left'><a target='_blank' href="http://www.szallasoutlet.hu/?utm_source=[@ga_prefix]_logo&utm_medium=[@nldate]&utm_campaign=btn_minden"><img  class="image_fix" alt="Az összes ajánlat megtekintése" title="Az összes ajánlat megtekintése" src='http://img.hotel-world.hu/images/newlogo.jpg' width='230' height='40'/></a></td>
						<td width="200" valign="middle" align='right' style='color:white;'>[@subtitle_1]</td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ffffff'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='10' align='left' style='color:#ec381a; font-size:11px;'>SZÁLLÁS OUTLET HÍRLEVÉL - [@date]</td>
						<td width="200" valign="middle" align='right' style='color:#ec381a; font-size:11px;'>
							<a href="http://www.szallasoutlet.hu/?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_belfold" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">BELFÖLD</a>  |  <a href="http://www.szallasoutlet.hu/vizpart-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_vizpart" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">VÍZPART</a>  |  <a href="http://www.szallasoutlet.hu/wellness-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_wellness" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">WELLNESS</a>  |  <a href="http://www.szallasoutlet.hu/gyermekbarat-szallasok?utm_source=newsletter&utm_medium=[@nldate]&utm_campaign=btn_gyermekbarat" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">GYERMEKBARÁT</a></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
					
			<tr>
				<td valign='top'>
				<!-- -->
					<a href="[@offer1_url]" target ="_blank" title="[@offer1_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='10'></td>
									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer1_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer1_city]</span> <br/>
										
										<span style='color:#585858'>[@offer1_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="[@offer1_title]" title="[@offer1_title]" src='[@offer1_image]' width='560' height='240'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer1_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer1_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer1_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
				<!-- -->	
				</td>
				
			</tr>
			
			<tr>	
				<td height='15'></td>
			</tr>
			
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
					<a href="[@offer2_url]" target ="_blank" title="[@offer2_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='10'></td>
									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer2_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer2_city]</span> <br/>
										
										<span style='color:#585858'>[@offer2_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="[@offer2_title]" title="[@offer2_title]" src='[@offer2_image]' width='560' height='240'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer2_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer2_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer2_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
				<!-- -->	
				</td>
				
			</tr>
			
			<tr>	
				<td height='15'></td>
			</tr>
						
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
					<tr>
					<td valign='top'>
					<a href="[@offer3_url]" target ="_blank" title="[@offer3_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='360' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='320'>
								<tr>
									<td width='10' height='40'></td>
									<td width='110' style='font-size:30px;color:#ec381a;font-weight:bold;'>-[@offer3_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:14px'>
										<span style='color:#ed391b'>[@offer3_city]</span> <br/>
										
										<span style='color:#585858'>[@offer3_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="[@offer3_title]" title="[@offer3_title]" src='[@offer3_image]' width='325' height='150'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='130' style='font-size:24px;color:#7db300;font-weight:bold;'>
												<div style='font-size:24px;line-height:24px; height:24px;color:#7db300;font-weight:bold;'>[@offer3_price]</div>
												<div style='color:#666666;font-size:10px;font-weight:bold;'>[@offer3_subtitle]</div>
											</td>
											<td width='185' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
					
				</td>
				
				<td width='15'></td>
				<td width='225' valign='top'>
					<!-- -->
					<a href="[@offer7_url]" target ="_blank" title="[@offer7_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='225' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='185'>
								<tr>
									<td width='2' height='40'></td>
									<td style='font-size:16px;font-weight:bold;line-height:14px'>
										<span style='color:#ed391b'>[@offer7_city]</span> <br/>
										
										<span style='color:#585858'>[@offer7_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<img  class="image_fix" alt="[@offer7_title]" title="[@offer7_title]" src='[@offer7_image]' width='185' height='150'/>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='25'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
										<td width='185' height='38' bgcolor='3b3e40' valign='middle' style='backgrund-color:#3b3e40;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
					<!-- -->
				</td>
				</tr>
				</table>
				<!-- -->	
				</td>
				
			</tr>
			
				<tr>	
				<td height='15'></td>
			</tr>
						
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
					<tr>
					<td valign='top'>
					<a href="[@offer4_url]" target ="_blank" title="[@offer4_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='360' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='320'>
								<tr>
									<td width='10' height='40'></td>
									<td width='110' style='font-size:30px;color:#ec381a;font-weight:bold;'>-[@offer4_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:14px'>
										<span style='color:#ed391b'>[@offer4_city]</span> <br/>
										
										<span style='color:#585858'>[@offer4_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="[@offer4_title]" title="[@offer4_title]" src='[@offer4_image]' width='325' height='150'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='130' style='font-size:24px;color:#7db300;font-weight:bold;'>
												<div style='font-size:24px;line-height:24px; height:24px;color:#7db300;font-weight:bold;'>[@offer4_price]</div>
												<div style='color:#666666;font-size:10px;font-weight:bold;'>[@offer4_subtitle]</div>
											</td>
											<td width='185' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
					
				</td>
				
				<td width='15'></td>
				<td width='225' valign='top'>
					<!-- -->
					<a href="[@offer8_url]" target ="_blank" title="[@offer8_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='225' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='185'>
								<tr>
									<td width='2' height='40'></td>
									<td style='font-size:16px;font-weight:bold;line-height:14px'>
										<span style='color:#ed391b'>[@offer8_city]</span> <br/>
										
										<span style='color:#585858'>[@offer8_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<img  class="image_fix" alt="[@offer8_title]" title="[@offer8_title]" src='[@offer8_image]' width='185' height='150'/>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='25'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
										<td width='185' height='38' bgcolor='3b3e40' valign='middle' style='backgrund-color:#3b3e40;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
					<!-- -->
				</td>
				</tr>
				</table>
				<!-- -->	
				</td>
				
			</tr>
			
			<!-- -->
			
			<tr>	
				<td height='15'></td>
			</tr>
			
						<tr>
				<td valign='top'>
				<!-- -->
					<a href="[@offer5_url]" target ="_blank" title="[@offer5_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='10'></td>
									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer5_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer5_city]</span> <br/>
										
										<span style='color:#585858'>[@offer5_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="[@offer5_title]" title="[@offer5_title]" src='[@offer5_image]' width='560' height='240'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer5_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer5_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer5_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
				<!-- -->	
				</td>
				
			</tr>
			
				<tr>	
				<td height='15'></td>
			</tr>
			
						<tr>
				<td valign='top'>
				<!-- -->
					<a href="[@offer6_url]" target ="_blank" title="[@offer6_title]" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='10'></td>
									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-[@offer6_percent]%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>[@offer6_city]</span> <br/>
										
										<span style='color:#585858'>[@offer6_title]</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="[@offer6_title]" title="[@offer6_title]" src='[@offer6_image]' width='560' height='240'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>[@offer6_price]</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>[@offer6_subtitle]</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">[@offer6_origprice2]</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
				<!-- -->	
				</td>
				
			</tr>

			
			
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#ed391b; font-weight:bold;' valign='middle' height='48' width='535'>
								[@subtitle_2]
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/arrow.jpg' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						<a href="[@offer9_url]" target="_blank" title="[@offer9_title]">
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer9_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>[@offer9_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer9_city]</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="[@offer9_title]" title="[@offer9_title]" src='[@offer9_image]' width='111' height='33'/>
									</td>
								</tr>
							</table></a>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
							<a href="[@offer10_url]" target="_blank" title="[@offer10_title]"><table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer10_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>[@offer10_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer10_city]</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="[@offer10_title]" title="[@offer10_title]" src='[@offer10_image]' width='111' height='33'/>
									</td>
								</tr>
							</table></a>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
						<a href="[@offer11_url]" target="_blank" title="[@offer11_title]">
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer11_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>[@offer11_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer11_city]</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="[@offer11_title]" title="[@offer11_title]" src='[@offer11_image]' width='111' height='33'/>
									</td>
								</tr>
							</table></a>
							</td>
						<td width='20'></td> 
					</tr>					
					<tr>
						<td colspan='3' height='10'></td>
					</tr>

				</table>
			</td>
			</tr>

	
		<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#ed391b; font-weight:bold;' valign='middle' height='48' width='535'>
								[@subtitle_3]
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/arrow.jpg' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
					<tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>			
					<tr>
						<td width='20'></td>  
						<td>
							<a href="[@offer12_url]" target="_blank" title="[@offer12_title]"><table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer12_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>[@offer12_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer12_city]</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="[@offer12_title]" title="[@offer12_title]" src='[@offer12_image]' width='111' height='33'/>
									</td>
								</tr>
							</table></a>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
							<a href="[@offer13_url]" target ="_blank" title="[@offer13_title]">
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer13_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>[@offer13_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer13_city]</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="[@offer13_title]" title="[@offer13_title]" src='[@offer13_image]' width='111' height='33'/>
									</td>
								</tr>
							</table></a>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
							<a href="[@offer14_url]" target ="_blank" title="[@offer14_title]"><table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<tr>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>[@offer14_price]</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>[@offer14_title]</b>
										<div style='padding:5px 0 0 0;'>[@offer14_city]</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="[@offer14_title]" title="[@offer14_title]" src='[@offer14_image]' width='111' height='33'/>
									</td>
								</tr>
							</table></a>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>

				</table>
			</td>
			</tr>
			
			<tr>	
				<td height='15'></td>
			</tr>
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="http://www.szallasoutlet.hu/minden-szallaskupon?utm_source=[@ga_prefix]&utm_medium=[@nldate]&utm_campaign=btn_minden" target ="_blank" title="Megnézem az összes ajánlatot" style="color:white; text-decoration: none;">MEGNÉZEM AZ ÖSSZES AJÁNLATOT</a></td>
			</tr>	
			<tr>	
				<td height='15'></td>
			</tr>	
			<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
				
A hírlevélben feltüntetett árak, képek, és a szolgáltatások leírásai tájékoztató jellegűek és a figyelem felkeltésére szolgálnak. Az ajánlatok részletes leírása a linkekre kattintva honlapunkon érhető el. Levelünket a #EMAIL# címre küldtük, mivel korábban feliratkozott hírlevél szolgáltatásunkra. Amennyiben nem kíván több levelet kapni tőlünk, kérjük kattintson az alábbi linkre:<br/><a href="#URL#" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: underline;">Leiratkozás</a>

				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class="image_fix" alt="Kiemelt partnereink" title="Kiemelt partnereink" src='http://img.hotel-world.hu/images/plogos.jpg' width='339' height='33'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
		</table>
		</td>
	</tr>
</table>  
</body>
</html>