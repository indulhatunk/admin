<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
	<style>
	
	*{
  margin: 0px;
  margin: 0px;
}

		body, html { margin:0; margin:0; font-family:arial; color:#2a0000; font-size:12px; text-align:center;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
		table { text-align:left;}
	</style>
	<title>Szállás Outlet napi ajánlat</title>
</head>
 <body bgcolor='#680800' topmargin="0">
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#680800' style='width: 100%;'>
     <tr>
        <td>
                    
	<div style='width:760px;margin:0 auto;'>
	<table width='760' cellpadding='0' cellspacing='0' border='0' bgcolor='white' align='center'>
		<tr>
			<td height='132'>
				<a href='http://szallasoutlet.hu/?utm_source=newsletter_header'><img style='display:block' src='http://www.szallasoutlet.hu/images/outlet-special.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: szallasoutlet.hu - vatera.hu - teszvesz.hu - lealkudtuk.hu' 
					title='Óriási kedvezmények hazai szállodákban: szallasoutlet.hu - vatera.hu - teszvesz.hu - lealkudtuk.hu' width='760'/></a>
			</td>
		</tr>
		<tr>
		<td>
			<div style='text-align:center;margin:20px 0 0 0;'>
				<a href='[@offer1_url]' title='[@offer1_title]'>
					<img src='http://indulhatunk.com/images/newpic.php?percent=[@offer1_percent]&outletbig=1&imgurl=[@offer1_image]' alt='[@offer1_title]' title='[@offer1_title]'/>
				</a>
			</div>
			<div style='margin:10px 40px 0 40px'>
			<table>
				<tr>
					<td width='440'>
					
					<div style='width:439px;border-right:1px solid #d2d2d2;'>
						<div style='font-size:20px;margin:0 0 10px 0;'><b>[@offer1_city]</b></div>					
						
						<div><a href='[@offer1_url]' style='font-size:30px; text-transform:uppercase; text-decoration:none; color:#e25325; font-weight:bold;'>[@offer1_title]</a></div>
					
						<div style='font-size:20px;margin:5px 0 0 0;'>[@offer1_subtitle]</div>
					
						<div style='font-size:18px;font-weight:bold;margin:20px 0 0 0;'>Outlet ár:</div>
						<div style='font-size:40px;color:#e25325;font-weight:bold;'>[@offer1_price]</div>

					</div>
					</td>
					<td valign='top'>
					<div style='margin:0px 0 0 10px;'>

					<table border='0' width='220' cellspacing='0' cellpadding='0'>
						<tr>
							<td height='110' valign='top'>[@offer1_description]	</td>
						</tr>
						<tr>
							<td valign='top'>
								<div style='text-align:center;'>
									[@offer1_firstlink]
									<a href='[@offer1_url2]'><img src='http://indulhatunk.com/images/daily-lealkudtuk.png' width='220'/></a>	
			
								</div>
							</td>
						</tr>

					</table>
					
				
					</div>
					
				
							
					</td>
				</tr>
			</table>
			
			<div style='height:10px; margin:10px 0 5px 0;border-top:1px solid #d2d2d2;'>&nbsp;</div>	
			<table width='680'>
				<tr>
					<td>
						<!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer2_url]'><img src='http://indulhatunk.com/images/newpic.php?percent=[@offer2_percent]&outletsmall=1&imgurl=[@offer2_image]'/></a></div>
					
					<div style='margin:10px 0px 0; height:40px;'>
						<a href='[@offer2_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#e25325; font-weight:bold;'>[@offer2_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer2_city]</b></div>					

					<div style='font-size:11px;font-weight:bold;margin:5px 0 0 0;'>Outlet ár:</div>
						<div style='font-size:18px;color:#e25325;font-weight:bold;'>[@offer2_price]</div>
						
					<div style='font-size:11px;font-weight:bold;'>[@offer2_description]</div>

					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer2_url]'><img src='http://indulhatunk.com/images/outlets.png'/></a>
						<a href='[@offer2_url2]'><img src='http://indulhatunk.com/images/lealkudtuks.png'/></a>
					</div>
					</div>
						<!-- -->
					</td>
					<td>
						<!-- -->
						<div style='width:234px;border-right:1px solid #d2d2d2; border-left:1px solid #d2d2d2;margin:0 15px 0 15px'>
						
						<div style='margin:0 17px 0 17px;width:200px;'>
						
						
					<div style='height:117px;'><a href='[@offer3_url]'><img src='http://indulhatunk.com/images/newpic.php?percent=[@offer3_percent]&outletsmall=1&imgurl=[@offer3_image]' /></a></div>
					
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer3_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#e25325;font-weight:bold;'>[@offer3_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer3_city]</b></div>					

					<div style='font-size:11px;font-weight:bold;margin:5px 0 0 0;'>Outlet ár:</div>
						<div style='font-size:18px;color:#e25325;font-weight:bold;'>[@offer3_price]</div>
											<div style='font-size:11px;font-weight:bold;'>[@offer3_description]</div>

					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer3_url]'><img src='http://indulhatunk.com/images/outlets.png'/></a>
						<a href='[@offer3_url2]'><img src='http://indulhatunk.com/images/lealkudtuks.png'/></a>
					</div>
					</div>
					</div>
						<!-- -->
						
					</td>
					<td><!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer4_url]'><img src='http://indulhatunk.com/images/newpic.php?percent=[@offer4_percent]&outletsmall=1&imgurl=[@offer4_image]'/></a></div>
					
					<div  style='margin:10px 0 0px 0;height:40px;'>	
						<a href='[@offer4_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#e25325; font-weight:bold;'>[@offer4_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer4_city]</b></div>					

					<div style='font-size:11px;font-weight:bold;margin:5px 0 0 0;'>Outlet ár:</div>
						<div style='font-size:18px;color:#e25325;font-weight:bold;'>[@offer4_price]</div>
											<div style='font-size:11px;font-weight:bold;'>[@offer4_description]</div>

					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer4_url]'><img src='http://indulhatunk.com/images/outlets.png'/></a>
						<a href='[@offer4_url2]'><img src='http://indulhatunk.com/images/lealkudtuks.png'/></a>
					</div>
					</div>				
					<!-- --></td>
				</tr>
				<tr>
				<td colspan='3'>			<div style='height:10px; margin:10px 0 5px 0;border-top:1px solid #d2d2d2;'>&nbsp;</div>	
</td>
				</tr>
				<tr>
					<td>
						<!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer5_url]'><img src='http://indulhatunk.com/images/newpic.php?percent=[@offer5_percent]&outletsmall=1&imgurl=[@offer5_image]'/></a></div>
					
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer5_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#e25325; font-weight:bold;'>[@offer5_title]</a>
					</div>
					
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer5_city]</b></div>					

					<div style='font-size:11px;font-weight:bold;margin:5px 0 0 0;'>Outlet ár:</div>
						<div style='font-size:18px;color:#e25325;font-weight:bold;'>[@offer5_price]</div>
											<div style='font-size:11px;font-weight:bold;'>[@offer5_description]</div>

					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer5_url]'><img src='http://indulhatunk.com/images/outlets.png'/></a>
						<a href='[@offer5_url2]'><img src='http://indulhatunk.com/images/lealkudtuks.png'/></a>
					</div>
					</div>						<!-- -->
					</td>
					<td>
						<!-- -->
						<div style='width:234px;border-right:1px solid #d2d2d2; border-left:1px solid #d2d2d2; margin:0 15px 0 15px'>
						
												<div style=' margin:0 17px 0 17px;width:200px'>


					<div><a href='[@offer6_url]'><img src='http://indulhatunk.com/images/newpic.php?percent=[@offer6_percent]&outletsmall=1&imgurl=[@offer6_image]'/></a></div>
					
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer6_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#e25325; font-weight:bold;'>[@offer6_title]</a>
					</div>
											<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer6_city]</b></div>					

					<div style='font-size:11px;font-weight:bold;margin:5px 0 0 0;'>Outlet ár:</div>
						<div style='font-size:18px;color:#e25325;font-weight:bold;'>[@offer6_price]</div>
											<div style='font-size:11px;font-weight:bold;'>[@offer6_description]</div>

					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer6_url]'><img src='http://indulhatunk.com/images/outlets.png'/></a>
						<a href='[@offer6_url2]'><img src='http://indulhatunk.com/images/lealkudtuks.png'/></a>
					</div>
					</div>
					</div>
						<!-- -->
						
					</td>
					<td><!-- -->
					<div style='width:200px;'>
					<div><a href='[@offer7_url]'><img src='http://indulhatunk.com/images/newpic.php?percent=[@offer7_percent]&outletsmall=1&imgurl=[@offer7_image]'/></a></div>
					
								
					<div style='margin:10px 0 0px 0;height:40px;'>
						<a href='[@offer7_url]' style='font-size:16px; text-transform:uppercase; text-decoration:none; color:#e25325; font-weight:bold;'>[@offer7_title]</a>
					</div>
					<div style='font-size:12px;margin:0 0 10px 0;'><b>[@offer7_city]</b></div>		
			

					<div style='font-size:11px;font-weight:bold;margin:5px 0 0 0;'>Outlet ár:</div>
						<div style='font-size:18px;color:#e25325;font-weight:bold;'>[@offer7_price]</div>
						<div style='font-size:11px;font-weight:bold;'>[@offer7_description]</div>

					<div style='text-align:center;margin:10px 0 0 0 ;'>
						<a href='[@offer7_url]'><img src='http://indulhatunk.com/images/outlets.png'/></a>
						<a href='[@offer7_url2]'><img src='http://indulhatunk.com/images/lealkudtuks.png'/></a>
					</div>
					</div>						<!-- --></td>
				</tr>
				
			</table>
			
			</div>
		</td>
		
		</tr>
		
		<tr>
			<td align='center'><div style='margin:10px 0 0 0;font-size:10px'>&nbsp;</div></td>
		</tr>
	</table>
	</div>
	
	<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:white; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:white;'>linkre</a> kattintva mondhatja le.
</div>

	<div style='height:30px;color:white;text-align:center;font-size:11px;margin:px 0 0 0'>Minden jog fenntartva &copy; 2012 szallasoutlet.hu</div>
	
	[@track]
</td>
</tr>
	
</table>

</body><html>