<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();

if($CURUSER[userclass] < 50)
	header("location:index.php");	

$pid = (int)$_GET[pid];
$editid = (int)$_GET[id];


$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$pid' LIMIT 1"));


head("Alirodák &raquo; $partner[company_name] $partner[hotel_name]");


if($_POST[zip] <> '' && $_POST[city] <> ''  && $_POST[address] <> '' &&  $_POST[id] == '')
{
	$mysql->query_insert("partner_offices",$_POST);
	$msg = "Sikeresen hozzáadta az irodát";
}

if($_POST[zip] <> '' && $_POST[city] <> ''  && $_POST[address] <> '' &&  $_POST[id] > 0)
{
	
	$mysql->query_update("partner_offices",$_POST,"id=$_POST[id]");
	$msg = "Sikeresen frissítette az iroda adatait";
}


$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$pid' LIMIT 1"));



?>




<style>
	.daytime { width:50px !important; }
</style>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='partners.php'>Minden partner</a></li>
						<li><a href='partners.php?edit=<?=$pid?>'>Partner szerkesztése</a></li>
						<li><a href='?pid=<?=$pid?>' class='<? if($_GET[add] <> 1) echo "current"?>'>Alirodák szerkesztése</a></li>
						
						<? if($_GET[id] > 0) { ?>
							<li><a href='offices.php?pid=<?=$_GET[pid]?>&id=<?=$_GET[id]?>&add=1' class='<? if($_GET[add] == 1) echo "current"?>'>Iroda szerkesztése</a></li>
						<? } else { ?>
							<li><a href='offices.php?pid=<?=$_GET[pid]?>&add=1' class='<? if($_GET[add] == 1) echo "current"?>'>Új iroda hozzádaása</a></li>
						<? } ?>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>

<?=message($msg)?>

<? if($_GET[add] == 1) { ?>
<div class="partnerForm">
	<form method="POST" action="offices.php?pid=<?=$_GET[pid]?>">
		<input type="hidden" name="id" value="<?=$_GET[id]?>">
			<input type="hidden" name="pid" value="<?=$pid?>">
		<?
		if($editid > 0)
		{
			$editID = (int)$_GET[editid];
			$editarr = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = $editid LIMIT 1"));
		}
		
		?>
	<fieldset>
		<legend><?=$partner[company_name]?> <?=$partner[hotel_name]?> irodái</legend>
	<ul>
		<li><label>Cím:</label><input type="text" name="zip" value="<?=$editarr[zip]?>"   style='width:35px;' class='numeric'/><input type="text" name="city"  style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" name="address" class="small2" style='width:295px;' value="<?=$editarr[address]?>" /></li>
		<li><label>Cégnév:</label><input type="text" name="company_name" value="<?=$editarr[company_name]?>"/></li>
		<li><label>Kapcsolattartó:</label><input type="text" name="name" value="<?=$editarr[name]?>"/></li>
		<li><label>Telefonszám:</label><input type="text" name="phone" value="<?=$editarr[phone]?>"/></li>
		<li><label>Email:</label><input type="text" name="email" value="<?=$editarr[email]?>"/></li>
		
		<li><label>GPS:</label><input type="text" name="latitude" value="<?=$editarr[latitude]?>" class='daytime'/> <input type="text" name="longitude" value="<?=$editarr[longitude]?>" class='daytime'/></li>
		<li><label>Ziplist:</label><textarea name="request_zip" style='width:230px;height:30px;'><?=$editarr[request_zip]?></textarea></li>
		<li><label>Megjegyzés:</label><textarea name="comment" style='width:230px;height:30px;'><?=$editarr[comment]?></textarea></li>
		<li><label>Nyitvatartás:</label><input type='text' name="opening_hours" value='<?=$editarr[opening_hours]?>'/>		
		<hr/>
		<table style='width:400px;float:left;'>
					<tr>
						<td>Nyitvatartás:</td>
						<td><table>
							<tr>
								<td width='85'>hétfő</td>
								<td><input type='text' name='f1' class='daytime' value='<?=$editarr[f1]?>'/> - <input type='text' name='t1' class='daytime' value='<?=$editarr[t1]?>'/></td>
							</tr>
							<tr>
								<td>kedd</td>
								<td><input type='text' name='f2' class='daytime' value='<?=$editarr[f2]?>'/> - <input type='text' name='t2' class='daytime' value='<?=$editarr[t2]?>'/></td>
							</tr>
							<tr>
								<td>szerda</td>
								<td><input type='text' name='f3' class='daytime' value='<?=$editarr[f3]?>'/> - <input type='text' name='t3' class='daytime' value='<?=$editarr[t3]?>'/></td>
							</tr>
							<tr>
								<td>csütörtök</td>
								<td><input type='text' name='f4' class='daytime' value='<?=$editarr[f4]?>'/> - <input type='text' name='t4' class='daytime' value='<?=$editarr[t4]?>'/></td>
							</tr>
							<tr>
								<td>péntek</td>
								<td><input type='text' name='f5' class='daytime' value='<?=$editarr[f5]?>'/> - <input type='text' name='t5' class='daytime' value='<?=$editarr[t5]?>'/></td>
							</tr>
							<tr>
								<td>szombat</td>
								<td><input type='text' name='f6' class='daytime' value='<?=$editarr[f6]?>'/> - <input type='text' name='t6' class='daytime' value='<?=$editarr[t6]?>'/></td>
							</tr>
							<tr>
								<td>vasárnap</td>
								<td><input type='text' name='f7' class='daytime' value='<?=$editarr[f7]?>'/> - <input type='text' name='t7' class='daytime' value='<?=$editarr[t7]?>'/></td>
							</tr>
							
						</table></td>
					</tr>
				</table>
				
				</li>
		<li><label>Zárva:</label><select name='closed'>
			<option value='0' <?if($editarr[closed] == 0) echo "selected"?>>nem</option>
			<option value='1' <?if($editarr[closed] == 1) echo "selected"?>>igen</option>
		</select></li>

		<li><label>Irodakód:</label><input type="text" name="code" value="<?=$editarr[code]?>" class='numeric'/></li>
		<li><label>Aldomain:</label><input type="text" name="subdomain" value="<?=$editarr[subdomain]?>"/></li>
		<li><label>Web engedélyett:</label><select name='web_enabled'>
			<option value='0' <?if($editarr[web_enabled] == 0) echo "selected"?>>nem</option>
			<option value='1' <?if($editarr[web_enabled] == 1) echo "selected"?>>igen</option>
		</select></li>

		<?
			for($i=1;$i<=10;$i++) { 
				
				echo "<li><label>$i. dolgozó:</label><input type='text' name='agent_$i' value='".$editarr["agent_".$i]."'/></li>";
			}
		?>
		<li style='text-align:center;'><input type="submit" value="Mentés" /></li>
	</ul>
	</fieldset>
	</form>
</div>
</div></div>
<?
foot();
die();
}

echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td colspan='2'>-</td>";
	echo "<td>Cím</td>";
	echo "<td>Kapcsolattartó</td>";
	echo "<td>Telefonszám</td>";
	echo "<td>E-mail</td>";
	echo "<td>Irodakód</td>";
	echo "<td>Subdomain</td>";
echo "</tr>";


if($pid > 0)
	$query = $mysql->query("SELECT * FROM partner_offices WHERE pid = '$pid'");


	echo "<tr style='background-color:#e7e7e7'>";
	
		echo "<td align='center' colspan='2'>-</td>";
		echo "<td>$partner[zip] $partner[city] $partner[address]</td>";
		echo "<td>$partner[contact]</td>";
		echo "<td>$partner[contact_phone]</td>";
		echo "<td>$partner[email]</td>";
		echo "<td>Fő iroda</td>";
		echo "<td></td>";	 
 
	echo "</tr>";

$i = 1;
while($arr = mysql_fetch_assoc($query)) {


	echo "<tr>";
		echo "<td>$i<br/>$arr[id]</td>";
		echo "<td align='center' width='25'><a href='?pid=$arr[pid]&id=$arr[id]&add=1'><img src='/images/edit.png' width='20' alt='szerkeszt' title='szerkeszt'/></a></td>";
		echo "<td>$arr[zip] $arr[city]<br/>$arr[address]</td>";
		echo "<td>$arr[name]</td>";
		echo "<td>$arr[phone]</td>";
		echo "<td>$arr[email]</td>";
		echo "<td>$arr[code]</td>";	
		echo "<td align='center'><a href='http://$arr[subdomain].greenoutlet.hu' target='_blank'>$arr[subdomain]</a></td>";	 

	echo "</tr>";
	$i++;
	
}
echo "</table></div></div>";
foot();
?>