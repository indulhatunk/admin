<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

head("SzállásOutlet.hu napi eladási statisztika");

$weekdays = array(
'',
'hétfő',
'kedd',
'szerda',
'csütörtök',
'péntek',
'szombat',
'vasárnap',
);

if($_GET[paid] == 1)
	$extrapaid = "AND paid = 1";
?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="?all=1" class="<? if($_GET[paid] <> 1) echo "current";?>">Eladott tételek</a></li>
		<li><a href="?paid=1" class="<? if($_GET[paid] == 1) echo "current";?>">Fizetett tételek</a></li>
	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>
	
	<form method='get'>
	<div style='text-align:center'>	
	<input type='hidden' name='paid' value='<?=$_GET[paid]?>'/>
	<input type='text' name='from_date' class='maskeddate' value='<?=$_GET[from_date]?>' placeholder='kezdete'/>
	<input type='text' name='to_date' class='maskeddate' value='<?=$_GET[to_date]?>' placeholder='vége'/>
	<select name='mode'>
		<option value='1' <? if($_GET[mode] == 1) echo "selected"?>>napi</option>
		<option value='2' <? if($_GET[mode] == 2) echo "selected"?>>heti</option>
		<option value='3' <? if($_GET[mode] == 3) echo "selected"?>>havi</option>
		<option value='4' <? if($_GET[mode] == 4) echo "selected"?>>éves</option>

	</select>
		<input type='submit' value='szűrés'/>
	</div>
	<hr/>
	</form>



<?

	if($_GET[from_date] <> '')
	{
		if($_GET[to_date] == '')
			$_GET[to_date] = date("Y-m-d");
		$efrom = "AND added >= '$_GET[from_date]' AND added <= '$_GET[to_date]'";
		
	}
	
	if($_GET[mode] == 2)
		$md = 'YEAR(added), WEEK(added)';
	elseif($_GET[mode] == 3)
		$md = 'YEAR(added), MONTH(added)';
	elseif($_GET[mode] == 4)
		$md = 'YEAR(added)';
	else
		$md = 'YEAR(added), MONTH(added), DAY(added)';
		
		
	$query = $mysql->query("SELECT count(cid) AS cnt, added, sum(orig_price) as total FROM customers WHERE type = 4 $extrapaid $efrom GROUP BY $md ORDER BY added ASC");
	
	while($arr = mysql_fetch_assoc($query))
	{
		$day = explode(" ",$arr[added]);
		$day = $day[0];

		$weekday = date('N', strtotime($day));
		
		$year = date('Y', strtotime($day));
		$month = date('m', strtotime($day));
		$week = date('W', strtotime($day));


			$offer_id = mysql_fetch_assoc($mysql->query("SELECT * FROM newsletter_offers WHERE newsletter_date = '$day'"));
			
			if($offer_id[id] <> 0)
				$extraday = "AND offers_id = $offer_id[offer_id]";
			else
				$extraday = "AND cid < 0";
			
	if($_GET[mode] == 2)
	{
		$md2 = "AND YEAR(added) = $year AND week(added) = $week";
		$ttl = "$year. $week. hét";
	
	}
	elseif($_GET[mode] == 3)
	{
		$md2 = "AND YEAR(added) = $year AND month(added) = $month";
		$ttl = "$year. $month.";
	}
	elseif($_GET[mode] == 4)
	{
		$md2 = "AND YEAR(added) = $year";
		$ttl = "$year.";
	}
	else
	{
		$md2 = "AND added >= '$day 00:00:00' and added <= '$day 23:59:59' ORDER BY added DESC";
		$ttl =  $day;

	}
	
		//	$pt = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS cnt, sum(orig_price) as total FROM customers WHERE type = 4  and paid = 1  $md2"));


	$reseller = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS cnt, sum(orig_price) as total FROM customers WHERE type = 8  $extrapaid $md2"));
	
	$lealkudtuk = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS cnt, sum(orig_price) as total FROM customers WHERE type = 6 $extrapaid $md2"));
	
	
	$total = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS cnt, sum(orig_price) as total FROM customers WHERE cid > 0 $extrapaid $md2"));
	
	//	$paid = mysql_fetch_assoc($mysql->query("SELECT count(cid) AS cnt FROM customers WHERE type = 4 AND added like '%$day%' AND paid = 1"));
	if($weekday == 7)
	{
		$sunday = $sunday + $arr[total];
		$class = 'header';
	}
	else
		$class = '';
		
	$rows.="<tr class='$class'>";
		$rows.= "<td width='80'>$day</td>";
		$rows.= "<td>".$weekdays[$weekday]."</td>";
		$rows.= "<td align='right'>$arr[cnt]</td>";
		$rows.= "<td align='right'>$pt[cnt]</td>";
		
		$rows.= "<td align='right'>".round($pt[cnt]/$arr[cnt]*100)."%</td>";

		$rows.= "<td align='right'>".formatPrice($arr[total]+$pnt[total])."</td>";
		$rows.= "<td align='right'>".formatPrice(($arr[total]+$pnt[total])*0.145)."</td>";
	$rows.= "</tr>";
	

	$yield = $yield + ($arr[total]+$pnt[total])*0.145;
	$totalcnt = $totalcnt + $arr[cnt]+$pnt[cnt];
	$totalprice = $totalprice + $arr[total] + $pnt[total];
	
	$users[]= " ['$ttl', $arr[cnt], $reseller[cnt], $lealkudtuk[cnt], $total[cnt]]";

	$tt[]= " ['$ttl', $arr[total], $reseller[total], $lealkudtuk[total], $total[total]]";

	$op = round($arr[total]/$total[total]*100,2);
	$lp = round($lealkudtuk[total]/$total[total]*100,2);
	$rp = round($reseller[total]/$total[total]*100,2);
	
	$perc[]= " ['$ttl', $op, $rp, $lp, 100]";

	$rlist[] = $rows;
	}
	
	
	$user = implode(',',$users);
	$tt = implode(',',$tt);
	$perc = implode(',',$perc);

	?>
	  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Dátum', 'Outlet','Viszonteladó','Lealkudtuk','Total'],
          <?=$user?>
        ]);

        var options = {
          title: 'Outlet eladások db',
          'width':870,
           'height':250,
           legend: {},
           crosshair: { trigger: 'both' },
		   focusTarget: "category",
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div" style="width: 870px; height: 250px;"></div>
        
        	  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Dátum', 'Outlet','Viszonteladó','Lealkudtuk','Total'],
          <?=$tt?>
        ]);

        var options = {
          title: 'Outlet eladások érték',
          'width':870,
           'height':250,
           legend: {},
           crosshair: { trigger: 'both' },
           focusTarget: "category",
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div_total'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div_total" style="width: 870px; height: 250px;"></div>


       	  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Dátum', 'Outlet','Viszonteladó','Lealkudtuk','Total'],
          <?=$perc?>
        ]);

        var options = {
          title: 'Outlet eladások bevétel  %-os megoszlás',
          'width':870,
           'height':250,
           legend: {},
           crosshair: { trigger: 'both' },
           focusTarget: "category",
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div_total_perc'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div_total_perc" style="width: 870px; height: 250px;"></div>




	<?
		
		$rows = implode("",array_reverse($rlist));
die;	
	echo "<table>";
	echo "<tr class='header'>";
		echo "<td>Dátum</td>";
		echo "<td>Nap</td>";
		echo "<td  width='30'>Eladott</td>";
		//echo "<td>Partner db</td>";
		echo "<td class='grey'  width='30'>Fizetett</td>";
		
		echo "<td class='grey' width='30'>Arány</td>";

		echo "<td>Eladott érték</td>";
		//echo "<td class='grey'>Fizetett érték</td>";
		echo "<td class='grey'>Jutalék</td>";
	echo "</tr>";
	
	
	echo $rows;
	
	echo "<tr class='header'>";
	
		echo "<td colspan='3'>Összesen</td>";
		echo "<td align='right' colspan='2'>$totalcnt db</td>";
		echo "<td align='right'>".formatPrice($totalprice)."</td>";
		echo "<td align='right'>".formatPrice($yield)."</td>";
		/*echo "<td>Összesen</td>";
		echo "<td>Összesen</td>";
		echo "<td align='right'>$totalcnt</td>";
		echo "<td align='right>$totalprice</td>";*/
	echo "</tr>";
	

echo "</table></div></div>";
?>
  
<?
foot();
?>