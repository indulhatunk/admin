<?
// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: inline; filename=data.csv');

// create a file pointer connected to the output stream

include("inc/config.inc.php");
include("inc/mysql.class.php");
include("inc/functions.inc.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

$sql = "SELECT count(*) as num, city FROM travel_outlet.offers
join partners on partners.pid = offers.partner_id
where start_date < now() and end_date > now()
group by partners.city
order by num desc";

$output = fopen('php://output', 'w');
fputcsv($output, array('Num', 'City'));

$rows = mysql_query($sql);

// loop over the rows, outputting them
while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);

ob_start();
$csv = fopen("php://output", 'w');
fputcsv($csv, array_keys(reset($values)));
foreach ($values as $row) {
    fputcsv($csv, $row);
}
fclose($csv);
ob_get_clean();


?>
