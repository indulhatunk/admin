<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");


//include("invoice/create_invoice_own.php");

userlogin();

if($CURUSER[userclass] < 50)
	header("location:index.php");

$type = $_GET[type];



function distance($lat1, $lon1, $lat2 = '47.4719', $lon2='19.0503', $unit ='K') {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return round($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}



if($CURUSER[userclass] == 255 && $_GET[disable] > 0)
{
	$mysql->query("UPDATE own_vouchers SET disable = 1 WHERE id = $_GET[disable]");
}


$id = (int)$_POST[id];
$gid = (int)$_GET[id];
$current_year = date("Y");

$price = $_POST[orig_price];
if($id == '' && $price <> '')
{
	if($_POST[date] == '')
		$_POST[date] = 'NOW()';
		
	$mysql->query_insert("own_vouchers",$_POST);
}
elseif($id > 0)
{
	if($_POST[paid] == 1)
	{
	if($_POST[payment] == 2 || $_POST[payment] == 3)
	{
		$cashArr[comment] = $_POST[comment];
		$cashArr[value] = $_POST[price]+$_POST[postage]*1.27;
		$cashArr[voucher_id] = $_POST[id];
		$cashArr[added] = 'NOW()';
		$cashArr[username] = $CURUSER[username];
		$cashArr[company] = 'hoteloutlet';
	
		
		writelog("$CURUSER[username] set paid OWN: $_POST[comment]");
	
		$warrant_id = mysql_fetch_assoc($mysql->query("SELECT max(warrant_id) as max FROM checkout_vtl WHERE warrant_year = '$current_year' AND company = 'hoteloutlet'"));
		$cashArr[warrant_year] = date("Y");
		$cashArr[warrant_id] = $warrant_id[max]+1;

		$mysql->query_insert("checkout_vtl",$cashArr);
		
		header("location: ../vouchers/print_checkout.php?cid=$_POST[id]&type=&warrant=1");
	}
	$_POST[paid_date] = 'NOW()';
	}

	if($_POST[licittravel_date] == 'on')
		$_POST[licittravel_date] = date("Y-m-d");
	else
		$_POST[licittravel_date] = '';
		
	if($_POST[vatera_date] == 'on')
		$_POST[vatera_date] = date("Y-m-d");
	else
		$_POST[vatera_date] = '';
		
	//if($_POST[outlet_date] == 'on')
	//	$_POST[outlet_date] = date("Y-m-d");
	//else
	//	$_POST[outlet_date] = '';

		
	if($_POST[on_sale] == 'on')
		$_POST[on_sale]  = date("Y-m-d");
	else
		$_POST[on_sale]  = '';
		
	$_POST[price] = $_POST[price] - $_POST[joker];
	
	$_POST[sale_date] = 'NOW()';
	$mysql->query_update("own_vouchers",$_POST,"id=$id");
}
head("Saját eladás");
?>
<fieldset style="border:1px solid black;-moz-opacity:0.8;opacity:0.80;width:215px;height:24px;padding:5px;position:fixed;top:10px;right:10px;background-color:white;">
<form method="post" id="sform" action="own_vouchers.php">
<input type="text" name="search"  style='height:20px;width:150px;margin:1px 5px 0 0;padding:0;float:left;' value="<?=$cookie?>" /><a href="#"; style='display:block; background:none; color:black; border:1px solid black; height:10px;padding:5px; margin:1px 0 0 0;float:left;width:30px;float:left;' id='submit'>Ok</a>
</form>
</fieldset>


<script>
$(document).ready(function() {
});
</script>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					
					
<?
//echo "<h2><a href=\"http://admin.indulhatunk.info\">Adminisztrációs felület</a> &raquo; Saját voucherek kezelése</h2>";



	$partnerArr = mysql_fetch_assoc($mysql->query("SELECT partners.* FROM partners INNER JOIN contracts ON contracts.partner_id = partners.partner_id WHERE contracts.id = '$_GET[cid]'"));


if($_GET[cid] > 0)
{
	$addVoucher = "<li><a href=\"?add=1&cid=$_GET[cid]\">Új voucher felvitele</a></li>";
	
}
else
	$addVoucher = '';
	
echo $addVoucher;
?>
<li><a href="own_vouchers.php" class='<? if($_GET[type] == '') echo "current"?>'>Elintézendő</a></li>
<li><a href="own_vouchers.php?type=blue"  class='<? if($_GET[type] == 'blue' && $_GET[showstats] <> 1) echo "current"?>'>Raktár</a></li>

<li><a href="own_vouchers.php?type=blue&showstats=1"  class='<? if($_GET[type] == 'blue' && $_GET[showstats] == 1) echo "current"?>'>Raktár kimutatás</a></li>


<li><a href="own_vouchers.php?type=green"  class='<? if($_GET[type] == 'green') echo "current"?>'>Fizetett</a></li>
<li><a href="?type=grey"  class='<? if($_GET[type] == 'grey') echo "current"?>'>Számlázott</a></li>
<li><a href="export-novak.php">Novák export</a></li>


	<li><a href="list_vouchers.php">Kimutatás</a></li>
<? 
	
	/*
	echo "<div class='subMenu'>$addVoucher <a href=\"own_vouchers.php\" class='white'>Elintézendő</a> <a href=\"own_vouchers.php?type=blue\" class='blue'>Raktár</a> <a href=\"own_vouchers.php?type=green\" class='green '>Fizetett</a>  <a href=\"?type=grey\" class='grey'>Számlázott</a> <a href=\"export-novak.php\" class='grey'>Novák export</a> <a href=\"list_vouchers.php\" class='grey'>Kimutatás</a><div class='cleaner'></div></div>";
*/
?>
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<?

	$add = $_GET[add];
	if($add == 1)
		$hidden = '';
	else

		$hidden = 'display:none;';

if($CURUSER[userclass] == 255 || $CURUSER[username] == 'forroattila'  || $CURUSER[username] == 'paranyipetra'  || $CURUSER[username] == 'vargapeter' || $CURUSER[username] == 'szaboandras')
{
$curMonth = date('n');
$curYear = date('Y');

$whiteTotal = mysql_fetch_assoc($mysql->query("SELECT sum(price) as whiteSum FROM own_vouchers WHERE name <> '' AND paid = 0"));
$greenTotal = mysql_fetch_assoc($mysql->query("SELECT sum(price) as whiteSum, sum(orig_price) as totalSum  FROM own_vouchers WHERE MONTH(paid_date) = $curMonth AND YEAR(paid_date) = $curYear and paid = 1"));

$blueTotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers WHERE paid = 0 and name = '' and price = 0"));

$thisDay = date("j");

if($thisDay <= 10)
{
	$month = date("n")-2;
	$nextMonth = date("n")-1;
	$curMonth = date("n");
	
	//echo "2010-$month-11 00:00:00 - 2010-$nextMonth-10 00:00:00";
	//echo "<hr/>";
	$greyTotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers WHERE date > '2011-$month-11 00:00:00' AND date < '2011-$nextMonth-10 00:00:00'"));
	//echo "SELECT sum(orig_price) as whiteSum  FROM own_vouchers WHERE date > '2010-$month-11 00:00:00' AND date < '2010-$nextMonth-10 00:00:00";
	//echo "2010-$nextMonth-11 00:00:00 - 2010-$curMonth-10 00:00:00";
	//echo "<hr/>";
	
	$redTotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers WHERE date > '2011-$nextMonth-10 00:00:00' AND date < '2011-$curMonth-11 00:00:00'"));

	$majorTotal2 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE  date > '2011-$nextMonth-10 00:00:00' AND date < '2011-$curMonth-11 00:00:00' and contractor = 'Major Ágnes'"));
	$nemethTotal2 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE  date > '2011-$nextMonth-10 00:00:00' AND date < '2011-$curMonth-11 00:00:00' and contractor = 'Németh Éva' "));

}	
else
{
//	$month = date("n")-2;
	$nextMonth = date("n")-1;
	$curMonth = date("n");
	
	if($nextMonth == 0)
	{
		$nextMonth = 12;
		$year = date("y")-1;
	}
	else
	{
		$nextMonth = $nextMonth;
		$year = date("y");
	}
	//echo $nextMonth;
	$curMonth = date("n");

	//$greyTotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers WHERE date > '2010-$nextMonth-11 00:00:00' AND date <= '2011-$curMonth-10 00:00:00'"));
	//$redTotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers WHERE date > '2011-$curMonth-10 00:00:00' "));
	
	$majorTotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date >= '2010-12-11 00:00:00' AND date <= '2011-01-10 00:00:00' and contractor = 'Major Ágnes'"));
	$nemethTotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date >= '2010-12-11 00:00:00' AND date <= '2011-01-10 00:00:00' and contractor = 'Németh Éva' "));

	$majorTotal3 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date >= '2010-11-11 00:00:00' AND date <= '2010-12-10 00:00:00' and contractor = 'Major Ágnes'"));
	$nemethTotal3 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date >= '2010-11-11 00:00:00' AND date <= '2010-12-10 00:00:00' and contractor = 'Németh Éva' "));

	$majorTotal4 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date >= '2010-10-11 00:00:00' AND date <= '2010-11-10 00:00:00' and contractor = 'Major Ágnes'"));
	$nemethTotal4 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date >= '2010-10-11 00:00:00' AND date <= '2010-11-10 00:00:00' and contractor = 'Németh Éva' "));


	$majorTotal2 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date > '2011-12-10 00:00:00' and contractor = 'Major Ágnes'"));
	$nemethTotal2 = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as whiteSum  FROM own_vouchers inner join contracts on contracts.id = own_vouchers.contract_id WHERE date > '2011-12-10 00:00:00' and contractor = 'Németh Éva' "));

	//echo "SELECT sum(orig_price) as whiteSum  FROM own_vouchers WHERE date > '2010-$curMonth-10 00:00:00'";
}
//echo "d: $thisDay";
if($greenTotal[totalSum] == 0)
	$greenTotal[totalSum] = 1;

$percentAge = $greenTotal[whiteSum]/$greenTotal[totalSum]*100;

$realPrice = $blueTotal[whiteSum]*($percentAge/100);
?>
<div style="float:left" >
<h1>Összesített egyenleg</h1>
<table>
<tr class="white">
	<td>Elintézendő összes</td>
	<td><?=formatPrice($whiteTotal[whiteSum])?></td>


</tr>

<tr class="green">
	<td>Fizetett</td>
	<td align="right"><?=formatPrice($greenTotal[whiteSum])?></td>
	<td align="right"><?=round($percentAge)?>%</td>

</tr>

<tr class="blue">
	<td rowspan='2'>Raktár</td>
	<td align="right">Névérték</td>
	<td align="right"><?=formatPrice($blueTotal[whiteSum])?></td>
</tr>
<tr class="blue">
	<td align="right">~Reálérték</td>
	<td align="right"><?=formatPrice($realPrice)?></td>
</tr>
<!-- 
<tr class="grey">
	<td rowspan='2'>Marketing</td>
	<td align="right">Major</td>
	<td align="right"><?=formatPrice($majorTotal4[whiteSum])?></td>
	<td align="right" rowspan='2'><?=formatPrice($majorTotal4[whiteSum]+$nemethTotal4[whiteSum])?></td>
</tr>
<tr class="grey">
	<td align="right">Németh</td>
	<td align="right"><?=formatPrice($nemethTotal4[whiteSum])?></td>

</tr>

<tr class="grey">
	<td rowspan='2'>Marketing</td>
	<td align="right">Major</td>
	<td align="right"><?=formatPrice($majorTotal3[whiteSum])?></td>
	<td align="right" rowspan='2'><?=formatPrice($majorTotal3[whiteSum]+$nemethTotal3[whiteSum])?></td>
</tr>
<tr class="grey">
	<td align="right">Németh</td>
	<td align="right"><?=formatPrice($nemethTotal3[whiteSum])?></td>

</tr>



<tr class="grey">
	<td rowspan='2'>Marketing</td>
	<td align="right">Major</td>
	<td align="right"><?=formatPrice($majorTotal[whiteSum])?></td>
	<td align="right" rowspan='2'><?=formatPrice($majorTotal[whiteSum]+$nemethTotal[whiteSum])?></td>
</tr>
<tr class="grey">
	<td align="right">Németh</td>
	<td align="right"><?=formatPrice($nemethTotal[whiteSum])?></td>

</tr>


<tr class="red">
	<td rowspan='2'>Marketing </td>
	<td align="right">Major</td>
	<td align="right"><?=formatPrice($majorTotal2[whiteSum])?></td>
	<td align="right" rowspan='2'><?=formatPrice($majorTotal2[whiteSum]+$nemethTotal2[whiteSum])?></td>
</tr>
<tr class="red">
	<td align="right">Németh</td>
	<td align="right"><?=formatPrice($nemethTotal2[whiteSum])?></td>

</tr>

-->

</table>
</div>
<div class="cleaner"></div>
<?

if($_GET[showstats] == 1) { 
$query  = $mysql->query("SELECT partner_id, count(id) as cnt, sum(orig_price) as total FROM own_vouchers WHERE  price = 0 GROUP by partner_id ORDER BY cnt DESC");

echo "<br/><br/><table>";

	echo "<tr class='header'>";
		echo "<td>Hotel neve</td>";
		echo "<td>Mennyiség</td>";
		echo "<td>Outleten</td>";
		echo "<td>30 nap</td>";
		
		echo "<td>60 nap</td>";
		
		echo "<td>180 nap</td>";
		echo "<td>360 nap</td>";

		echo "<td>Névérték</td>";
		
	echo "</tr>";
	
$i = 1;
while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partner_id = $arr[partner_id] LIMIT 1"));
	
	if($i%2 == 0)
		$class = 'lgrey';
	else
		$class = '';
		
	echo "<tr class='$class'>";
		echo "<td>$partner[hotel_name]</td>";
		echo "<td align='right'>$arr[cnt] db</td>";
		
		$thirty = mysql_fetch_assoc($mysql->query("SELECT partner_id, count(id) as cnt, sum(orig_price) as total FROM own_vouchers WHERE  price = 0 AND partner_id = $arr[partner_id] and expiration_date < DATE_ADD(NOW(),INTERVAL 30 DAY) GROUP BY partner_id"));
			
		$sixty = mysql_fetch_assoc($mysql->query("SELECT partner_id, count(id) as cnt, sum(orig_price) as total FROM own_vouchers WHERE  price = 0 AND partner_id = $arr[partner_id] and expiration_date < DATE_ADD(NOW(),INTERVAL 60 DAY)  GROUP BY partner_id"));
		
		$ninety = mysql_fetch_assoc($mysql->query("SELECT partner_id, count(id) as cnt, sum(orig_price) as total FROM own_vouchers WHERE  price = 0 AND partner_id = $arr[partner_id] and expiration_date < DATE_ADD(NOW(),INTERVAL 180 DAY) GROUP BY partner_id"));
		
		$fyear = mysql_fetch_assoc($mysql->query("SELECT partner_id, count(id) as cnt, sum(orig_price) as total FROM own_vouchers WHERE  price = 0 AND partner_id = $arr[partner_id] and expiration_date < DATE_ADD(NOW(),INTERVAL 360 DAY) GROUP BY partner_id"));

	$otyear = mysql_fetch_assoc($mysql->query("SELECT partner_id, count(id) as cnt, sum(orig_price) as total FROM own_vouchers WHERE  price = 0 AND partner_id = $arr[partner_id] AND outlet_date <> '0000-00-00' GROUP BY partner_id"));
	
				 
		echo "<td align='right'>$otyear[cnt]</td>";

		echo "<td align='right'>$thirty[cnt]</td>";
		echo "<td align='right'>$sixty[cnt]</td>";
		echo "<td align='right'>$ninety[cnt]</td>";
		echo "<td align='right'>$fyear[cnt]</td>";

		echo "<td align='right'>".formatPrice($arr[total])."</td>";
		
	echo "</tr>";
	
	$total = $total+$arr[total];
	
		$i++;
		
}

	echo "<tr class='header'>";
		echo "<td colspan='7'>Összesen</td>";
		echo "<td align='right'>".formatPrice($total)."</td>";
		
	echo "</tr>";
echo "</table><div class='cleaner'></div>";


}

}?>



<div class="partnerForm">
	<form method="POST" action="own_vouchers.php?cid=<?=$_GET[cid]?>" style="<?=$hidden?>">
		<?if($gid <> "") {
		$qr = $mysql->query("SELECT * FROM own_vouchers WHERE id = $gid");
		$editarr = mysql_fetch_assoc($qr);
		?>
		<input type="hidden" name="id" value="<?=$editarr[id]?>">
		

		<?}
?>
	<fieldset>
		<legend>Új Voucher felvitele</legend>
	<ul>
		<li><label>Szerződés száma</label><b><?=$partnerArr[hotel_name]?> / <?=$_GET[cid]?>  <?=$editarr[bid_name]?></b>	<input type="hidden" name="partner_id" value="<?=$partnerArr[partner_id]?>"/><input type="hidden" name="contract_id" value="<?=$_GET[cid]?>"/></li>
		
		<? if($CURUSER[username] == 'iSuperG' || $CURUSER[username] == 'vargapeter' || $CURUSER[username] == 'orsolya')
			{
				?>
<!--
		<li><label>Fizetve?:</label>
		<select name="paid">
			<option value="0" <?if($editarr[paid]==0)echo"selected";?>>nem</option>
			<option value="1" <?if($editarr[paid]==1)echo"selected";?>>fizetve</option>
		</select>
		</li>
	<li><label>Fizetés Dátuma</label> <input type="text" name="paid_date" value="<?=$partnerArr[paid_date]?>"/></li>
-->


				<?
			}
		?>
	<!--	<li><label>Partner:</label>
			<select name="partner_id">
				<option value="" <?if($editarr[partner_id]=='')echo"selected";?>>Válasszon</option>
				<?
				/*	$qr = $mysql->query("SELECT * FROM partners ORDER BY hotel_name ASC");
					while($qrArr = mysql_fetch_assoc($qr))
					{
						if($editarr[partner_id]==$qrArr[partner_id])
							$selected = 'selected';
						else
							$selected = '';
						echo "<option value=\"$qrArr[partner_id]\" $selected>$qrArr[hotel_name] $qrArr[partner_id]</option>";
					}
				*/
				?>
			</select>
		</li>
		<li><label>Szerződés:</label>
			<select name="contract_id">
				<option value="" <?if($editarr[contract_id]=='')echo"selected";?>>Válasszon</option>
				<?
				/*	$qr = $mysql->query("SELECT contracts.*,partners.* FROM contracts INNER JOIN partners ON partners.partner_id = contracts.partner_id ORDER BY id ASC");
					while($qrArr = mysql_fetch_assoc($qr))
					{
						if($editarr[contract_id]==$qrArr[id])
							$selected = 'selected';
						else
							$selected = '';
						echo "<option value=\"$qrArr[id]\" $selected>$qrArr[id] $qrArr[hotel_name]</option>";
					}
				*/
				?>
			</select>
		</li>
		-->
		<li><label>Névérték:</label><input type="text" name="orig_price" value="<?=$editarr[orig_price]?><?=$formData[orig_price]?>" style='width:50px;' class='numeric'/> Ft</li>
			<li><label>Dátum:</label><input type="text" name="date" value="<?=$editarr[date]?>" style='width:80px;' class='dpick'  autocomplete='off'/></li>

		<li><label>Mennyiségi egység (éj)::</label><input type="text" name="quantity" value="<?=$editarr[quantity]?><?=$formData[orig_price]?>" style='width:50px;'/></li>
		<li><label>Mennyiség:</label><input type="text" name="quantity2" value="<?=$editarr[quantity2]?><?=$formData[orig_price]?>"  style='width:50px;' class='numeric'/></li>
		<li><label>Voucher sorszám:</label><input type="text" name="voucher_id" value="<?=$editarr[voucher_id]?><?=$formData[orig_price]?>"/></li>
		<li><label>Érvényesség:</label><input type="text" name="expiration_date" value="<?=$editarr[expiration_date]?><?=$formData[expiration_date]?>" class='dpick' style='width:90px'/></li>

		<?if($gid <> "") {?>
		<li><label>Licit sorszám:</label><input type="text" name="bid_id" value="<?=$editarr[bid_id]?><?=$formData[orig_price]?>"/></li>
		<li><label>Vatera:</label><input type="text" name="vatera_date" value="<?=$editarr[vatera_date]?><?=$formData[orig_price]?>"/></li>
		<li><label>Licittravel:</label><input type="text" name="licittravel_date" value="<?=$editarrlicittravel_date?><?=$formData[orig_price]?>"/></li>

		<li><label>Megjegyzés:</label><input type="text" name="comment" value="<?=$editarr[comment]?><?=$formData[orig_price]?>"/></li>
		<li><label><b>Vásárló adatai</b></label></li>
		<li><label>Vevő neve:</label><input type="text" name="name" value="<?=$editarr[name]?>"/></li>
		<li><label>Cím:</label><input type="text" name="zip"value="<?=$editarr[zip]?>" style='width:50px;' class='numeric'/><input type="text" name="city" class="small1" value="<?=$editarr[city]?>" style='width:130px;margin:0 1px 0 1px;' /><input type="text" name="address" class="small" style='width:290px;' value="<?=$editarr[address]?>" /></li>
		<li><label>Adószám:</label><input type="text" class="tax_no" name="tax_no" value="<?=$editarr[tax_no]?>"/></li>
		<li><label>Telefonszám:</label><input type="text" class="phone" name="phone" value="<?=$editarr[phone]?>"/></li>
		<li><label>E-mail cím:</label><input type="text" name="email" value="<?=$editarr[email]?>"/></li>
		<li><label>Eladási ár:</label><input type="text" name="price" id='price' value="<?=$editarr[price]+$editarr[joker]?><?=$formData[orig_price]?>" style='width:50px;' class='numeric'/> Ft 
		</li>
		<li><label>Joker kedvezmény:</label>
		
		<input type='hidden'  id='jokerhidden' name='joker' value='<?=$editarr[joker]?>'  style='width:50px;' /> 
		<input type='text'  id='jokervalue' value='<?=$editarr[joker]?>'  style='width:50px;' class='numeric' disabled/> 
			
		Ft <input type='checkbox' id="jokercheckbox" <? if($editarr[joker] > 0) echo "checked"?>/>Joker?</li>
		<li><label>Postaköltség:</label><input type="text" name="postage" value="<?=$editarr[postage]?><?=$formData[bid_name]?>" style='width:50px;' class='numeric'/> Ft</li>
		
		<li><label>Fizetés módja:</label>
			<select name="payment">
				<option value="1" <?if($editarr[payment]==1)echo"selected";?>>Átutalás</option>
				<option value="2" <?if($editarr[payment]==2)echo"selected";?>>Készpénz</option>
				<option value="3" <?if($editarr[payment]==3)echo"selected";?>>Utánvét</option>
				<option value="4" <?if($editarr[payment]==4)echo"selected";?>>OTP SZÉP</option>
				<option value="5" <?if($editarr[payment]==5)echo"selected";?>>K&H SZÉP</option>
				<option value="6" <?if($editarr[payment]==6)echo"selected";?>>MKB SZÉP</option>
				<option value="9" <?if($editarr[payment]==9)echo"selected";?>>Bankkártya</option>

			</select>
		</li>
		<li><label>Outlet ID:</label><input type="text" class="tax_no" name="outlet_offer_id" value="<?=$editarr[outlet_offer_id]?>"/></li>

	<?}?>
		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</fieldset>
	</form>
</div>

<br/><br/>
<?
if($hidden == '')
{
	echo foot();
	die;
}
echo "<table class=\"general\" width='100%'>";

echo "<tr class=\"header\">";
	echo "<td>-</td>";

	echo "<td width='65'>Dátum</td>";
	
	if($type <> 'blue')
	{
		echo "<td>Vevő neve</td>";
		echo "<td>Vevő címe</td>";
		echo "<td>Fizetés módja</td>";
		echo "<td>Megjegyzés</td>";
		echo "<td>Fizetve</td>";
		//echo "<td>Licit azonosító</td>";
	}
	echo "<td>Hotel neve</td>";
	
	if($type <> 'green' && $type <> '')
		echo "<td>Város</td>";
	//echo "<td>Licit</td>";
	
	echo "<td>Voucher sorszám</td>";
	
	if($type == 'blue')
		echo "<td><a href='?type=blue&orderby=expiration'>Érvényesség</a></td>";
	else
		echo "<td><a href='?orderby=expiration'>Érvényesség</a></td>";
		
	echo "<td>Eladási ár</td>";
	
	if($type <> 'green' && $type <> '')
		echo "<td>Névérték</td>";
		
	if($_GET[cid] > 0 || $_GET[pid] > 0)
		echo "<td>Névérték</td>";
	
	
	
	
	if($type == 'green' || $type == 'grey')
	{
		echo "<td>számláz</td>";
	}
	if($type == 'blue')
	{
		echo "<td>L</td>";
		echo "<td>V</td>";
		echo "<td>O</td>";
		echo "<td>✓</td>";
	}


echo "</tr>";

$cid = (int)$_GET[cid];
$pid = (int)$_GET[pid];



if($type == '') 
{
	$extraSelect = "AND paid = 0 AND name <> ''";
	$class = '';
	$orderBy = ' sale_date DESC';
}
elseif($type == 'blue') 
{
	$extraSelect = "AND price = 0";
	$orderBy = ' contract_id ASC';
	$class = 'blue';
}
elseif($type == 'green') 
{
	$extraSelect = "AND paid = 1 AND name <> '' and invoice_number = ''  AND year(paid_date) >= 2013  AND disable = 0";
	$class = 'green';
	$orderBy = ' date DESC';
}
elseif($type == 'grey') 
{
	$extraSelect = "AND paid = 1 AND name <> '' and invoice_number <> '' AND year(paid_date) >= 2013 AND disable = 0";
	$class = 'grey';
	$orderBy = ' date DESC';
}

if($_GET[orderby] == 'expiration')
{
	$orderBy = "expiration_date ASC";
}
//elseif($type == 'red') 
//{
//	$extraSelect = "AND price = 0 AND (vatera_date <> '' or licittravel_date <> '')";
//	$class = 'red';
//}

$s = $_POST[search];


if($cid > 0)
{
	//$where = "WHERE contract_id = $cid";
	$query = $mysql->query("SELECT * FROM own_vouchers WHERE contract_id = $cid ORDER BY date DESC");

}
elseif($pid > 0)
{
	$query = $mysql->query("SELECT * FROM own_vouchers WHERE partner_id = $pid ORDER BY date DESC");

}
else
{

	if($s <> '')
	{
		$filterSelect = "AND name LIKE '%$s%' OR email like '%$s%'"; //	echo "kereses $qr";
		$query = $mysql->query("SELECT * FROM own_vouchers WHERE id > 0  $filterSelect ORDER BY $orderBy");
	}
	else
	{
	$query = $mysql->query("SELECT * FROM own_vouchers WHERE id > 0  $extraSelect $filterSelect ORDER BY $orderBy");
	}

}	


while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners where partner_id = $arr[partner_id]"));

		echo "<form method=\"post\" action=\"own_vouchers.php?type=$type#$arr[id]\">";
		echo "<input type=\"hidden\" name=\"id\" value=\"$arr[id]\">";
	
	echo "<input type=\"hidden\" name=\"payment\" value=\"$arr[payment]\">";
	echo "<input type=\"hidden\" name=\"comment\" value=\"$arr[voucher_id] voucher fizetve $arr[name] által\">";
	echo "<input type=\"hidden\" name=\"price\" value=\"$arr[price]\">";
	echo "<input type=\"hidden\" name=\"voucher_id\" value=\"$arr[voucher_id]\">";
	echo "<input type=\"hidden\" name=\"postage\" value=\"$arr[postage]\">";
	if ($arr[on_sale] <> '' && $type == 'blue')
	{
		$class2 = 'red';
	}
	elseif ($arr[outlet_date] <> '0000-00-00' && $type == 'blue')
	{
	
		$class2 = 'purple';
	}
	elseif ($arr[on_sale] == '' && ($arr[vatera_date] <> ''))
	{
		$class2 = 'grey';
	}
	elseif ($arr[on_sale] == '' && ($arr[licittravel_date] <> ''))
	{
		$class2 = 'yellow';
	}
//	elseif ($arr[licittravel_date] <> '' && $type == 'blue')
//	{
//		$class2 = 'red';
//	}
	else
	{
		$class2 = $class;
	}
	
	
	if($_GET[type] == 'blue')
	{
				
		$contract = mysql_fetch_assoc($mysql->query("SELECT * FROM contracts WHERE id=  $arr[contract_id] LIMIT 1"));
		
		if($contract[company] == 'mkmedia' && $contract[inactive] == 1)
		{
			$suff = '<b>[INAKTÍV SZERZŐDÉS]</b> ';
			$inact = 1;
		}
		else
		{
			$suff = '';
			$inact = 0;
		}
	}
	echo "<tr class='$class2' id='row-$arr[id]'><a name='$arr[id]'></a>";
	echo "<td><a href=\"?id=$arr[id]&add=1&cid=$arr[contract_id]\"><img src='images/edit.png' width='20'/></a></td>";
	
	$paid = explode(" ",$arr[paid_date]);
	$sale = explode(" ",$arr[sale_date]);
	
	if($paid[0] == '0000-00-00')
		$paid[0] = "-";
	
	if($sale[0] == '0000-00-00')
		$sale[0] = "-";
	if($arr[0] == '0000-00-00')
		$arr[0] = "-";


		
		
	echo "<td style='width:80px;text-align:left;'>V: $arr[date]<br/>E: $sale[0]<br/>F: $paid[0]</td>";
	
	if($type <> 'blue')
	{
	echo "<td>$arr[name]<br/>$arr[phone] <a href='mailto:$arr[email]'>$arr[email]</a></td>";
	echo "<td>$arr[zip] $arr[city] <br/> $arr[address]</td>";

	if($arr[payment] == 1) 
		$payment = "Átutalás";
	elseif($arr[payment] == 2) 
		$payment = "Készpénz";
	elseif($arr[payment] == 3) 
		$payment = "Utánvét";
	elseif($arr[payment] == 4) 
		$payment = "OTP SZÉP";
	elseif($arr[payment] == 5) 
		$payment = "K&H SZÉP";
	elseif($arr[payment] == 6) 
		$payment = "MKB SZÉP";
	elseif($arr[payment] == 9) 
		$payment = "Bankkártya";
	
		
	echo "<td>$payment</td>";
	echo "<td>$arr[comment]</td>";
	if($arr[paid] == 1)
	{
		$disabled = "disabled";
	}
	else {
		$disabled = "";
	}


	echo "<td>";
	
	if($CURUSER[office_id] == 2499 || $CURUSER[office_id] == 0) { 

	echo "<select name=\"paid\" id=\"paid\" onchange=\"this.form.submit();\" $disabled>";
		echo "<option value=\"0\">Nem</option>";
		echo "<option value=\"1\"";
		if($arr[paid] == 1)
				echo "selected";
		echo ">Igen</option>";
		echo "<option value=\"2\"";
		if($arr[paid] == 2)
				echo "selected";
		echo ">Nem kérte</option>";
	echo "</select>";
	}
	echo "</td>";
	


	//echo "<td>$arr[bid_name]</td>";
	//echo "<td>$arr[bid_id]</td>";
	}
	echo "<td><a href='contracts.php?pid=$arr[partner_id]'>".$suff."$partner[hotel_name]<br/> $arr[contract_id]  / [ $arr[quantity2]  $arr[quantity] ]</a></td>";
	
	$dist = distance($partner[latitude],$partner[longitude]);
	if($type <> 'green' && $type <> '')
		echo "<td>$partner[city] ($dist km)</td>";
		
	$v = explode(" ",$arr[voucher_id]);
		
	$final = end($v);	
	$voucher = $v[0];
	
	if($final == $voucher)
		$final = '';

	echo "<td>$voucher <br/>$final</td>";
	
	if($arr[expiration_date] == '0000-00-00')
		$arr[expiration_date] = '';
	echo "<td align='center'>$arr[expiration_date]</td>";

	echo "<td>".formatPrice($arr[price],0,1)."</td>";
	if($type <> 'green' && $type <> '')
		echo "<td>".formatPrice($arr[orig_price])."</td>";
	
	if($_GET[cid] > 0 || $_GET[pid] > 0)
		echo "<td>".formatPrice($arr[orig_price])."</td>";



	


	
	if($arr[paid] == 1 && $arr[invoice_number] == '')
	{
		
		$invoice = "<a href=\"invoice/create_invoice_own.php?customer=$arr[id]\">[számláz]</a><br/><a href='/vouchers/print_checkout.php?cid=$arr[id]&type=own&warrant=1'>[bizonylat]</a>";
	
		if($CURUSER[userclass] == 255)
		{	
			$pdate = explode(" ",$arr[paid_date]);
			$pdate = str_replace("-",'',$pdate[0]);
			
			if($pdate > '20110101' && $arr[price] > 1000)
			{
				
				
			//	echo "$incount - $pdate / $arr[price]<hr/>";
			//	$incount++;
				
			//	create_user_invoice_own($arr[id]);
				//if($incount > 10)
				//	die;
			}
		}
	}
	elseif($arr[paid] == 1 && $arr[invoice_number] <> '')
	{
		$invoice = "<a target='_blank' href=\"invoices/own/".str_replace("/","_",$arr[invoice_number]).".pdf\">$arr[invoice_number]</a><br/><a href='/vouchers/print_checkout.php?cid=$arr[id]&type=own'>[bizonylat]</a>";
	}
	else
	{
		$invoice = '-';
	}

	if($type == 'green' || $type == 'grey')
	{
		if($CURUSER[userclass] == 255)
			$extra = "<br/><a href='?type=$type&disable=$arr[id]'>[kész]</a>";
		else
			$extra = '';
		echo "<td>$invoice".$extra."</td>";
	}
	if($arr[licittravel_date] <> '')
		$lCh = "checked";
	else
		$lCh = '';
		
	if($arr[vatera_date] <> '')
		$vCh = "checked";
	else
		$vCh = '';
		
	if($arr[on_sale] <> '')
		$oCh = "checked";
	else
		$oCh = '';
		
	if($arr[outlet_date] <> '0000-00-00')
		$otCh = "checked";
	else
		$otCh = '';
		
	if($type == 'blue')
	{
		
		if($inact <> 1)
		{
			echo "<td class='yellow'><input type='checkbox' name='licittravel_date' id='$arr[id]' class='licittravel_date' \" $lCh/></td>";
			echo "<td class='grey'><input type='checkbox' name='vatera_date'  id='$arr[id]' class='vatera_date'  $vCh/></td>";
			echo "<td class='purple'><input type='checkbox' name='outlet_date'  id='$arr[id]' class='outlet_date'  $otCh/></td>";
			echo "<td class='red'><input type='checkbox' name='on_sale'  id='$arr[id]' class='on_sale'  $oCh/></td>";
		}
		else
		{
			echo "<td colspan='4'></td>";
		}
	if($arr[outlet_date] == '0000-00-00')
		$arr[outlet_date] = '';
	echo "<td class='red'>L: $arr[licittravel_date]<br/> V: $arr[vatera_date] <br/>O: $arr[on_sale] <br/>OT: $arr[outlet_date]</td>";
	/*echo "<td class='yellow'><input type='checkbox' name='licittravel_date'  onchange=\"this.form.submit();\" $lCh/></td>";
	echo "<td class='grey'><input type='checkbox' name='vatera_date'  onchange=\"this.form.submit();\" $vCh/></td>";
	echo "<td class='red'><input type='checkbox' name='on_sale'  onchange=\"this.form.submit();\" $oCh/></td>";
	echo "<td class='red'>L: $arr[licittravel_date]<br/> V: $arr[vatera_date] <br/>O: $arr[on_sale]</td>";*/
	}

	echo "</tr>";
	
	echo "</form>";
}
echo "</table>";

echo "</div></div>";

foot();
?>