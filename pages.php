<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

	userlogin();
	
	if($CURUSER[userclass] < 5)
	header("location: index.php");
	
head("Témaoldalak kezelése");



?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='?all=1' class="<? if($_GET[add] <> 1) echo "current"?>">Témaoldalak szerkesztése</a></li>
						<li><a href='?add=1' class="<? if($_GET[add] == 1) echo "current"?>">Témaoldalak létrehozása</a></li>
					</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<? if($_GET[edit] > 0 || $_GET[add] == 1) { 
	
	
	
	$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM pages WHERE id = '$_GET[edit]' LIMIT 1"));
	?>
	
	<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
$().ready(function() {
	$("#day_num").change(function () {
		var daynum = $(this).val()*1-1;
		
		if(daynum < 0)
			daynum = 0;
			
		$("#realnight").html(daynum);
		
		return false;
    });
    
    $(".changepackage").keyup(function () {
    	var daynum = <?=$editdata[day_num]-1?>*1;
		var thisid = $(this).attr('id');
		var thisval = Math.round($(this).val()*1/daynum/2);
		$('#pernight-'+thisid).html(thisval);
		//alert(thisid);
	
    });
});

	$().ready(function() {
		
		<? if($_GET[html] == 'enable') { ?>
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect,outdent,indent,blockquote,|,undo,redo,|,bullist,numlist,|",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
			  width : "745",
 			  height: "300",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		
		<? } ?>
	});
</script>


	<form method='POST' action='pages.php' enctype="multipart/form-data">
		<input type='hidden' name='id' value='<?=$city[id]?>' />
		
			<fieldset id='customerForm'>
	<legend>Témaoldal szerkesztése</legend>
	<table width='100%'>
	
		<tr>
			<td class='lablerow'>Title:</td>
			<td><input type='text' name='title' value='<?=$city[title]?>' style='width:520px'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Keyword:</td>
			<td><input type='text' name='keyword' value='<?=$city[keyword]?>' style='width:520px'/></td>
		</tr>

<tr>
			<td class='lablerow'>Description:</td>
			<td><input type='text' name='description' value='<?=$city[description]?>' style='width:520px'/></td>
		</tr>

		<tr>
			<td class='lablerow'>URL:</td>
			<td><input type='text' name='url_name' value='<?=$city[url_name]?>' style='width:520px'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Főoldali box címe:</td>
			<td><input type='text' name='home_title' value='<?=$city[home_title]?>' style='width:520px'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Főoldali megjelenés:</td>
			<td><input type='text' name='from_date' value='<?=$city[from_date]?>' style='width:100px' class='dpick'/> - <input type='text' name='to_date' value='<?=$city[to_date]?>' style='width:100px' class='dpick'/>  <input type='text' name='priority' value='<?=$city[priority]?>' style='width:20px' placeholder='prior'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Single:</td>
			<td><select name='single'>
				<option value='0' <? if($city[single] == 0) echo "selected"?>>nem</option>
				<option value='1' <? if($city[single] == 1) echo "selected"?>>igen</option>
			</select></td>
		</tr>
		<tr>
			<td class='lablerow'>Template:</td>
			<td><select name='template'>
				<option value='' <? if($city[template] == '') echo "selected"?>>nem</option>
				<option value='typeform' <? if($city[template] == 'typeform') echo "selected"?>>Typeform</option>
			</select></td>
		</tr>
			<tr>
			<td class='lablerow'>Típus:</td>
			<td><select name='type'>
				<option value='accomodation' <? if($city[type] == 'accomodation') echo "selected"?>>Belföld</option>
				<option value='tour' <? if($city[type] == 'tour') echo "selected"?>>Külföld</option>
			</select></td>
		</tr>
		<tr>
			<td class='lablerow'>Tartalom:</td>
			<td><textarea name='body' class='tinymce'><?=$city[body]?></textarea></td>
		</tr>
		<tr>
			<td class='lablerow'>Tartalom #2:</td>
			<td><textarea name='body_long' class='tinymce'><?=$city[body_long]?></textarea></td>
		</tr>

		<?
		if(@fopen("/var/www/hosts/pihenni.hu/htdocs/static.pihenni/display/themes/$city[id].jpg",'r') == TRUE)
			$profile = "<a href='http://static.indulhatunk.hu/display/themes/$city[id].jpg' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='25'/></a>";
		else
			$profile = "<img src='http://admin.indulhatunk.info/images/cross1.png' width='25'/>";	
			
		if(@fopen("/var/www/hosts/pihenni.hu/htdocs/static.pihenni/display/themes/$city[id]-1.jpg",'r') == TRUE)
			$profile2 = "<a href='http://static.indulhatunk.hu/display/themes/$city[id]-1.jpg' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='25'/></a>";
		else
			$profile2 = "<img src='http://admin.indulhatunk.info/images/cross1.png' width='25'/>";	

		?>
		<tr>
			<td class='lablerow'>Kép:</td>
			<td><input type="file" name="topicphoto"/> <?=$profile?></td>
		</tr>
		<tr>
			<td class='lablerow'>Főoldali kép:</td>
			<td><input type="file" name="topicphoto2"/> <?=$profile2?></td>
		</tr>
		<tr>
			<td colspan='2'><input type='submit' value='Mehet'/></td>
		</tr>
	</table>
	</form>
	</fieldset>
	<?
	echo "</div></div>";
	foot();
	die;	
}



if($_POST[id] > 0 && $_POST[title] <> '')
{
	
	if($_FILES["topicphoto"]["name"] <> '' && $_POST[id] <> '')
	{
		move_uploaded_file($_FILES["topicphoto"]["tmp_name"],"/var/www/hosts/pihenni.hu/htdocs/static.pihenni/display/themes/$_POST[id].jpg");
	}
	
	if($_FILES["topicphoto2"]["name"] <> '' && $_POST[id] <> '')
	{
		move_uploaded_file($_FILES["topicphoto2"]["tmp_name"],"/var/www/hosts/pihenni.hu/htdocs/static.pihenni/display/themes/$_POST[id]-1.jpg");
	}

	$mysql_tour->query_update("pages",$_POST,"id=$_POST[id]");
	echo message("Sikeresen szerkesztette a témaoldalt!");
	writelog("$CURUSER[username] edited theme page $_POST[id]");
}
if((int)$_POST[id] == 0 && $_POST[title] <> '')
{
	$mysql_tour->query_insert("pages",$_POST);
	echo message("Sikeresen létrehozta a témaoldalt!");
	writelog("$CURUSER[username] created theme page $_POST[title]");
}	
?>

<table>

	<tr class='header'>
		<td>-</td>
		<td>ID</td>
		<td>Title</td>
		<td>Keyword</td>
		<td>Description</td>
		<td></td>

	</tr>
<?

	$query = $mysql_tour->query("SELECT * FROM pages ORDER BY title ASC");
	while($arr = mysql_fetch_assoc($query))
	{
	
		echo "	<tr class='$class'>
					<td width='20'><a href='?edit=$arr[id]'><img src='/images/edit.png' width='20'/></a></td>
					<td>$arr[id]</td>
					<td><a href='http://www.indulhatunk.hu/$arr[url_name]' target='_blank'><b>$arr[title]</b></td>
					<td>$arr[keyword]</td>
					<td>$arr[description]</td>
					<td><a href='/pages_items.php?id=$arr[id]'>[tételek]</a></td>
				</tr>";
		
	}
?>
</table>
</div></div>
<?
foot();
?>