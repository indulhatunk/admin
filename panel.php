<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER[userclass] <> 255)
	header("location: index.php");
	
head("Vezérlőpult");

?>
	<div class='content-box'>
<div class='content-box-header'>
	<h3>Vezérlőpult</h3>
</div>
<div class='contentpadding'>

<ul>
	<li><a href='/partnerstats.php'>Eladási/fizetési statisztikák (heti, havi, éves)</a></li>
		<li><a href='/creditstats.php'>Lealkudtuk bankkártyás statisztika (heti, havi, éves)</a></li>

	<li><a href='/weekly-transfer.php'>Utalások</a></li>
	<li><a href='/weekly-transfer.php?transfer=4'>Eltolt, SZÉP, ÜCS</a></li>

	<li><a href='/customers.php?statistics=1'>Általános forgalmi statisztika</a></li>
	<li><a href='/cweekly-fast.php?company=hoteloutlet'>Heti kiállított számlák</a></li>
	<li><a href='/outlet_stats.php'>Napi outlet statisztika</a></li>
	<li><a href='/barterstats.php'>Ajánlat eladási statisztika</a></li>
	<li><a href='/reseller-stats.php'>Viszonteladói statisztika</a></li>
	<li><a href='/campaign-stats.php'>Időszakos eladási toplista</a></li>
	<li><a href='/totalstats.php?deleted=1'>Törölt statisztika (.xls)</a></li>
	<li><a href='/totalstats.php'>Havi statisztika (.xls)</a></li>
	<li><a href='/totalstats.php?type=weekly'>Heti statisztika (.xls)</a></li>
	<li><a href='/totalstats.php?emails=1'>Törölt e-mail címek (.xls)</a></li>
	<li><a href='/finalized.php'>Véglegesítés, számokban</a></li>
	<li><a href='/campaign-stats.php'>Kampányonkénti statisztika</a></li>

	<li><a href='/statistics.php?type=total'>Összesített statisztika</a></li>

	<li><a href='/translate.php'>Fordítások</a></li>


</ul>


</div>

</div>
				<?

	echo "<div class='cleaner'></div>";
 foot(); ?>