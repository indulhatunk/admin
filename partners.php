<?
/*
 * partners.php 
 *
 * partners page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
userlogin();

if($CURUSER[userclass] < 15 && $CURUSER[username] <> 'mindenkupon.hu')
	header("location:index.php");


$counties =array(   
	"Bács-Kiskun",
	"Baranya",
	"Békés",
	"Borsod-Abaúj-Zemplén",
	"Budapest",
	"Csongrád",
	"Fejér",
	"Győr-Moson-Sopron",
	"Hajdú-Bihar",
	"Heves",
	"Jász-Nagykun-Szolnok",
	"Komárom-Esztergom",
	"Nógrád",
	"Pest",
	"Somogy",
	"Szabolcs-Szatmár-Bereg",
	"Tolna",
	"Vas",
	"Veszprém",
	"Zala",
);



if($_GET[createcoredb] == 1)
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $_GET[pid] LIMIT 1"));
	
	if($partner[userclass] == 3)
		$data[partnertype_id] = 1;
	else
		$data[partnertype_id] = 2;

	$data[name] = $partner[hotel_name];
	$data[email] = $partner[email];
	$data[code] = generateTourID();
	
	$id = $mysql_tour->query_insert("partner",$data);
	
	$mysql->query("update partners set coredb_id = '$id' WHERE pid = $partner[pid]");
	echo message("Kész");
	die;
}	
$edit = $_POST[editid];
$editid = (int)$_GET[edit];

if($editid > 0) {
	$query = $mysql->query("SELECT * FROM partners WHERE pid = $editid");
	$editarr = mysql_fetch_assoc($query);
}
elseif($editid == 0 && $_GET[add] == 1)
{
	$maxpartner = mysql_fetch_assoc($mysql->query("SELECT max(partner_id) as max FROM partners"));
	$editarr[partner_id] = $maxpartner["max"]+1;
}
$data[coredb_id] = $_POST[coredb_id];
$data[coredb_package] = $_POST[coredb_package];


$data[foreign_bank] = $_POST[foreign_bank];
$data[foreign_bank_address] = $_POST[foreign_bank_address];
$data[foreign_swift] = $_POST[foreign_swift];
$data[foreign_iban] = $_POST[foreign_iban];
$data[foreign_account_no] = $_POST[foreign_account_no];
$data[foreign_account_name] = $_POST[foreign_account_name];
$data[county] = $_POST[county];

$data[lmb_type] = $_POST[lmb_type];
$data[country] = $_POST[country];

$data[latitude] = $_POST[latitude];

$data[longitude] = $_POST[longitude];

$data[yield_qr] = $_POST[yield_qr];
$data[is_qr] = $_POST[is_qr];
$data[is_museum] = $_POST[is_museum];

$data[is_top] = $_POST[is_top];

$data[lmb_continous] = $_POST[lmb_continous];


$data[company_name] = $_POST[company_name];
$data[special_comment] = $_POST[special_comment];
$data[directions] = $_POST[directions];

$data[location] = $_POST[location];
$data[ro_location] = $_POST[ro_location];
$data[sk_location] = $_POST[sk_location];


$data[hotel_name] = $_POST[hotel_name];
$data[ro_hotel_name] = $_POST[ro_hotel_name];

$data[sk_hotel_name] = $_POST[sk_hotel_name];

$data[url] = clean_url2($_POST[hotel_name]);


$data[userclass] = $_POST[userclass];
$data[status] = $_POST[status];

$data[rajongok_yield] = $_POST[rajongok_yield];
$data[rajongok_type] = $_POST[rajongok_type];


$data[tax_no] = $_POST[tax_no];
$data[partner_id] = $_POST[partner_id];
$data[contact] = $_POST[contact];
$data[contact_phone] = $_POST[contact_phone];
$data[address] = $_POST[address];
$data[zip] = $_POST[zip];
$data[city] = $_POST[city];

$data[is_reliable] = $_POST[is_reliable];


$data[yield_reseller] = $_POST[yield_reseller];

$data[yield_reseller] = $_POST[yield_reseller];
$data[yield_reseller_base] = $_POST[yield_reseller_base];
$data[yield_reseller_extra] = $_POST[yield_reseller_extra];
$data[yield_reseller_promotion] = $_POST[yield_reseller_promotion];

$data[reseller_online] = $_POST[reseller_online];


$data[invoice_name] = $_POST[invoice_name];
$data[invoice_zip] = $_POST[invoice_zip];
$data[invoice_city] = $_POST[invoice_city];
$data[invoice_address] = $_POST[invoice_address];

$data[foreign_custom_invoice] = $_POST[foreign_custom_invoice];
$data[islira] = $_POST[islira];

$data[phone] = $_POST[phone];
$data[reception_phone] = $_POST[reception_phone];
$data[email] = $_POST[email];
$data[website] = $_POST[website];
$data[category] = $_POST[category];

$data[vat_category] = $_POST[vat_category];

$data[has_foreign] = $_POST[has_foreign];

if($CURUSER[userclass] == 255)
	$data[extra_condition] = $_POST[extra_condition];

$data[reseller_print_disabled] = $_POST[reseller_print_disabled];


$data['yield'] = $_POST['yield'];
$data[yield_vtl] = $_POST[yield_vtl];
$data[username] = $_POST[username];
$data[password] = $_POST[password];
$data[account_no] = $_POST[account_no];
$data[reception_email] = $_POST[reception_email];

$data[contact2_name] = $_POST[contact2_name];
$data[contact2_phone] = $_POST[contact2_phone];
$data[contact2_email] = $_POST[contact2_email];
$data[contact2_position] = $_POST[contact2_position];
$data[contact_position] = $_POST[contact_position];


$data[contact1_ind_name] = $_POST[contact1_ind_name];
$data[contact2_ind_name] = $_POST[contact2_ind_name];
$data[contact3_ind_name] = $_POST[contact3_ind_name];
$data[contact4_ind_name] = $_POST[contact4_ind_name];


$data[contact1_ind_phone] = $_POST[contact1_ind_phone];
$data[contact2_ind_phone] = $_POST[contact2_ind_phone];
$data[contact3_ind_phone] = $_POST[contact3_ind_phone];
$data[contact4_ind_phone] = $_POST[contact4_ind_phone];


$data[contact1_ind_email] = $_POST[contact1_ind_email];
$data[contact2_ind_email] = $_POST[contact2_ind_email];
$data[contact3_ind_email] = $_POST[contact3_ind_email];
$data[contact4_ind_email] = $_POST[contact4_ind_email];


$data[contact1_ind_position] = $_POST[contact1_ind_position];
$data[contact2_ind_position] = $_POST[contact2_ind_position];
$data[contact3_ind_position] = $_POST[contact3_ind_position];
$data[contact4_ind_position] = $_POST[contact4_ind_position];


$data[licit_name] = $_POST[licit_name];
$data[licit_phone] = $_POST[licit_phone];
$data[licit_email] = $_POST[licit_email];
$data[licit_notification] = $_POST[licit_notification];
$data[licit_account_no] = $_POST[licit_account_no];

$data[outlet_post_id] = $_POST[outlet_post_id];


$data[szep_otp] = $_POST[szep_otp];
$data[szep_mkb] = $_POST[szep_mkb];
$data[szep_kh] = $_POST[szep_kh];

$data[is_kata] = $_POST[is_kata];
$data[is_amex] = $_POST[is_amex];


$data[unique_partner] = $_POST[unique_partner];

if($CURUSER[userclass] == 255 || $CURUSER[username] == 'miklos')
{
	$data[pp_disable] = $_POST[pp_disable];
	$data[post_balance] = $_POST[post_balance];	
	$data[postpone_category] = $_POST[postpone_category];	

}


$data[vat_extra_service] = $_POST[vat_extra_service];
$data[mkeh] = $_POST[mkeh];

$data[without_vat] = $_POST[without_vat];


$data[calculated_arrest] = $_POST[calculated_arrest];
$data[calculated_multiplier] = $_POST[calculated_multiplier];

$data[roomtype1_name] = $_POST[roomtype1_name];
$data[roomtype2_name] = $_POST[roomtype2_name];
$data[roomtype3_name] = $_POST[roomtype3_name];


if($_FILES["tos"]["name"] <> '')
{
      if(move_uploaded_file($_FILES["tos"]["tmp_name"],"/var/www/lighttpd/admin.indulhatunk.info/documents/tour_tos/$edit.pdf"))
     	 $mysql->query("UPDATE partners SET tos = 1 WHERE pid = $edit");
      else
      	echo "error";
      	
      writelog("$CURUSER[username] uploaded a TOS for partner #$id");
}




if($CURUSER[username] == 'novak')
	$data[partner_type] = 'licittravel';
	
	
if($_POST[property_spa] == 'on')
	$data[property_spa] = 1;
else
	$data[property_spa] = 0;
	
if($_POST[property_exclusive] == 'on')
	$data[property_exclusive] = 1;
else
	$data[property_exclusive] = 0;
	
if($_POST[property_special_food] == 'on')
	$data[property_special_food] = 1;
else
	$data[property_special_food] = 0;
	
if($_POST[property_autumn] == 'on')
	$data[property_autumn] = 1;
else
	$data[property_autumn] = 0;
	
if($_POST[property_check] == 'on')
	$data[property_check] = 1;
else
	$data[property_check] = 0;

if($_POST[property_wellness] == 'on')
	$data[property_wellness] = 1;
else
	$data[property_wellness] = 0;
	
if($_POST[property_mountain] == 'on')
	$data[property_mountain] = 1;
else
	$data[property_mountain] = 0;
	
if($_POST[property_water] == 'on')
	$data[property_water] = 1;
else
	$data[property_water] = 0;
	
if($_POST[property_castle] == 'on')
	$data[property_castle] = 1;
else
	$data[property_castle] = 0;
	
if($_POST[property_child] == 'on')
	$data[property_child] = 1;
else
	$data[property_child] = 0;
	
if($_POST[property_animal] == 'on')
	$data[property_animal] = 1;
else
	$data[property_animal] = 0;
	
if($_POST[property_accessibility] == 'on')
	$data[property_accessibility] = 1;
else
	$data[property_accessibility] = 0;
	

if($_POST[property_balaton] == 'on')
	$data[property_balaton] = 1;
else
	$data[property_balaton] = 0;
	
$data[property_main] = $_POST[property_main];

if($edit == "" && $data[company_name] <> "") {
	$mysql->query_insert("partners",$data);
	
	$id = mysql_insert_id();	
	writelog("$CURUSER[username] inserted partner #$id");

	$msg = "Sikeresen felvitte a partnert!";
}
elseif($edit > 0) {
	$mysql->query_update("partners",$data,"pid=$edit");
	$msg = "Sikeresen szerkesztette a partnert!";
	
	writelog("$CURUSER[username] updated partner #$edit");

}
head("Partnerek kezelése");
$mysql->query("SET NAMES 'utf8'"); 


if($_GET[partnertype] > 0)
{
	$extratype = "AND userclass = '$_GET[partnertype]'";
}
elseif($_GET[type] == 'maybe')
{
	$extraPartner = 'AND last_contract <  DATE_SUB(CURDATE(), INTERVAL 365 DAY) AND last_contract >=  DATE_SUB(CURDATE(), INTERVAL 548 DAY) ';

}
elseif($_GET[type] == 'inactive')
{
	$extraPartner = 'AND last_contract <  DATE_SUB(CURDATE(), INTERVAL 548 DAY) ';

}
elseif($_GET[type] == 'licittravel')
{
	$extraPartner = "AND partner_type = 'licittravel' ";

}
else
{
	$extraPartner = 'AND last_contract >=  DATE_SUB(CURDATE(), INTERVAL 365 DAY)';
}


$searchquery = $_POST[search];

if($searchquery == '')
{
	$qr = "SELECT * FROM partners WHERE userclass < 100 $extraPartner $extratype ORDER BY partner_id ASC";
}
else
{
	$qr = "SELECT * FROM partners WHERE company_name LIKE '%$searchquery%' OR invoice_name LIKE '%$searchquery%' OR hotel_name LIKE '%$searchquery%' OR email LIKE '%$searchquery%'   ORDER BY partner_id ASC";
}
print $qr;
$query = $mysql->query($qr); 
?>
<fieldset style="border:1px solid black;-moz-opacity:0.8;opacity:0.80;width:215px;height:24px;padding:5px;position:fixed;top:10px;right:10px;background-color:white;">
<form method="post" id="sform" action="partners.php">
<input type="text" name="search"  style='height:20px;width:150px;margin:1px 5px 0 0;padding:0;float:left;' value="<?=$cookie?>" autofocus/><a href="#"; style='display:block; background:none; color:black; border:1px solid black; height:10px;padding:5px; margin:1px 0 0 0;float:left;width:30px;float:left;' id='submit'>Ok</a>
</form>
</fieldset>
<?


if($msg <> '')
	echo "<div class='notify'>$msg</div>";

$add = $_REQUEST[add];

if($add == 1 || $editid > 0)

{
?>


<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='partners.php'>Minden partner</a></li>
						<li><a href='#' class='current'>Partner szerkesztése</a></li>
						<li><a href='offices.php?pid=<?=$_GET[edit]?>'>Alirodák szerkesztése</a></li>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>
<style>

<? if($editarr[userclass] == 5) {
	echo ".hotelinfo { display:none; }
		.resellerinfo { display:block; }
	
	";
	}
	if($editarr[userclass] == 0) {
	echo ".hotelinfo { display:block; }
		.resellerinfo { display:none; }
	
	";
	}
?>
</style>

<div class="partnerForm" <?=$show?>>
	<form method="POST" action="partners.php" id='pform'  enctype="multipart/form-data">
		<input type="hidden" name="editid" value="<?=$editarr[pid]?>" id='editid'>
	<fieldset style='width:520 px;'>
		<legend>Alapadatok</legend>
		<ul>
		<li><label>Típus:</label><select name='userclass' id='partnertype'>
			<option value='0' <? if($editarr[userclass] == 0) echo "selected";?>>Hotel</option>
			<option value='5'  <? if($editarr[userclass] == 5) echo "selected";?>>Partneriroda</option>
			<option value='3'  <? if($editarr[userclass] == 3) echo "selected";?>>Utazási iroda</option>
		</select>
		</li>
		<li class='hotelinfo'><label>Kiemelt partner:</label>
			<select name="is_top">
				<option value="0" <?if($editarr[is_top]==0)echo"selected";?>>nem</option>
				<option value="1" <?if($editarr[is_top]==1)echo"selected";?>>igen</option>

			</select>
		</li>
		<ul>
		<li><label>Állapot:</label><select name='status' id='status'>
			<option value='1'  <? if($editarr[status] == 1) echo "selected";?>>Tárgyalás alatt</option>
			<option value='0' <? if($editarr[status] == 0) echo "selected";?>>Partner</option>

		</select>
		</li>
	<li><label>Külföldi ajánlat:</label><select name='has_foreign' id='partnertype'>
			<option value='0' <? if($editarr[has_foreign] == 0) echo "selected";?>>nincs</option>
			<option value='1' <? if($editarr[has_foreign] == 1) echo "selected";?>>van</option>
		</select>
		</li>
		<li class='hotelinfo'><label>CoreDB ID:</label><input type="text" name="coredb_id"  value="<?=$editarr[coredb_id]?>" style='width:80px;'/></li>
		<li class='hotelinfo'><label>CoreDB csomag:</label><input type="text" name="coredb_package"   value="<?=$editarr[coredb_package]?>" style='width:80px;'/></li>
		<li><label>Ügyfélszám:</label><input type="text" name="partner_id"  class='numeric'  value="<?=$editarr[partner_id]?>" style='width:80px;'/></li>
		<li><label>Cég neve:*</label><input type="text" name="company_name" value="<?=$editarr[company_name]?>" id='company_name'/></li>
		<li><label>Egység neve:</label><input type="text" name="hotel_name" value="<?=$editarr[hotel_name]?>" id='hotel_name'/></li>
		<li><label>Egység RO neve:</label><input type="text" name="ro_hotel_name" value="<?=$editarr[ro_hotel_name]?>" id='hotel_name2'/></li>
		<li><label>Egység SK neve:</label><input type="text" name="sk_hotel_name" value="<?=$editarr[sk_hotel_name]?>" id='hotel_name3'/></li>

		<li><label>Cím:</label>
		
<?
$countries = array(
'AF'=>'Afghanistan',
'AL'=>'Albania',
'DZ'=>'Algeria',
'AS'=>'American Samoa',
'AD'=>'Andorra',
'AO'=>'Angola',
'AI'=>'Anguilla',
'AQ'=>'Antarctica',
'AG'=>'Antigua And Barbuda',
'AR'=>'Argentina',
'AM'=>'Armenia',
'AW'=>'Aruba',
'AU'=>'Australia',
'AT'=>'Austria',
'AZ'=>'Azerbaijan',
'BS'=>'Bahamas',
'BH'=>'Bahrain',
'BD'=>'Bangladesh',
'BB'=>'Barbados',
'BY'=>'Belarus',
'BE'=>'Belgium',
'BZ'=>'Belize',
'BJ'=>'Benin',
'BM'=>'Bermuda',
'BT'=>'Bhutan',
'BO'=>'Bolivia',
'BA'=>'Bosnia And Herzegovina',
'BW'=>'Botswana',
'BV'=>'Bouvet Island',
'BR'=>'Brazil',
'IO'=>'British Indian Ocean Territory',
'BN'=>'Brunei',
'BG'=>'Bulgaria',
'BF'=>'Burkina Faso',
'BI'=>'Burundi',
'KH'=>'Cambodia',
'CM'=>'Cameroon',
'CA'=>'Canada',
'CV'=>'Cape Verde',
'KY'=>'Cayman Islands',
'CF'=>'Central African Republic',
'TD'=>'Chad',
'CL'=>'Chile',
'CN'=>'China',
'CX'=>'Christmas Island',
'CC'=>'Cocos (Keeling) Islands',
'CO'=>'Columbia',
'KM'=>'Comoros',
'CG'=>'Congo',
'CK'=>'Cook Islands',
'CR'=>'Costa Rica',
'CI'=>'Cote D\'Ivorie (Ivory Coast)',
'HR'=>'Croatia (Hrvatska)',
'CU'=>'Cuba',
'CY'=>'Cyprus',
'CZ'=>'Czech Republic',
'CD'=>'Democratic Republic Of Congo',
'DK'=>'Denmark',
'DJ'=>'Djibouti',
'DM'=>'Dominica',
'DO'=>'Dominican Republic',
'TP'=>'East Timor',
'EC'=>'Ecuador',
'EG'=>'Egypt',
'SV'=>'El Salvador',
'GQ'=>'Equatorial Guinea',
'ER'=>'Eritrea',
'EE'=>'Estonia',
'ET'=>'Ethiopia',
'FK'=>'Falkland Islands (Malvinas)',
'FO'=>'Faroe Islands',
'FJ'=>'Fiji',
'FI'=>'Finland',
'FR'=>'France',
'FX'=>'France, Metropolitan',
'GF'=>'French Guinea',
'PF'=>'French Polynesia',
'TF'=>'French Southern Territories',
'GA'=>'Gabon',
'GM'=>'Gambia',
'GE'=>'Georgia',
'DE'=>'Germany',
'GH'=>'Ghana',
'GI'=>'Gibraltar',
'GR'=>'Greece',
'GL'=>'Greenland',
'GD'=>'Grenada',
'GP'=>'Guadeloupe',
'GU'=>'Guam',
'GT'=>'Guatemala',
'GN'=>'Guinea',
'GW'=>'Guinea-Bissau',
'GY'=>'Guyana',
'HT'=>'Haiti',
'HM'=>'Heard And McDonald Islands',
'HN'=>'Honduras',
'HK'=>'Hong Kong',
'HU'=>'Magyarország',
'IS'=>'Iceland',
'IN'=>'India',
'ID'=>'Indonesia',
'IR'=>'Iran',
'IQ'=>'Iraq',
'IE'=>'Ireland',
'IL'=>'Israel',
'IT'=>'Italy',
'JM'=>'Jamaica',
'JP'=>'Japan',
'JO'=>'Jordan',
'KZ'=>'Kazakhstan',
'KE'=>'Kenya',
'KI'=>'Kiribati',
'KW'=>'Kuwait',
'KG'=>'Kyrgyzstan',
'LA'=>'Laos',
'LV'=>'Latvia',
'LB'=>'Lebanon',
'LS'=>'Lesotho',
'LR'=>'Liberia',
'LY'=>'Libya',
'LI'=>'Liechtenstein',
'LT'=>'Lithuania',
'LU'=>'Luxembourg',
'MO'=>'Macau',
'MK'=>'Macedonia',
'MG'=>'Madagascar',
'MW'=>'Malawi',
'MY'=>'Malaysia',
'MV'=>'Maldives',
'ML'=>'Mali',
'MT'=>'Malta',
'MH'=>'Marshall Islands',
'MQ'=>'Martinique',
'MR'=>'Mauritania',
'MU'=>'Mauritius',
'YT'=>'Mayotte',
'ME'=>'Serbia, Montenegro',
'MX'=>'Mexico',
'FM'=>'Micronesia',
'MD'=>'Moldova',
'MC'=>'Monaco',
'MN'=>'Mongolia',
'MS'=>'Montserrat',
'MA'=>'Morocco',
'MZ'=>'Mozambique',
'MM'=>'Myanmar (Burma)',
'NA'=>'Namibia',
'NR'=>'Nauru',
'NP'=>'Nepal',
'NL'=>'Netherlands',
'AN'=>'Netherlands Antilles',
'NC'=>'New Caledonia',
'NZ'=>'New Zealand',
'NI'=>'Nicaragua',
'NE'=>'Niger',
'NG'=>'Nigeria',
'NU'=>'Niue',
'NF'=>'Norfolk Island',
'KP'=>'North Korea',
'MP'=>'Northern Mariana Islands',
'NO'=>'Norway',
'OM'=>'Oman',
'PK'=>'Pakistan',
'PW'=>'Palau',
'PA'=>'Panama',
'PG'=>'Papua New Guinea',
'PY'=>'Paraguay',
'PE'=>'Peru',
'PH'=>'Philippines',
'PN'=>'Pitcairn',
'PL'=>'Poland',
'PT'=>'Portugal',
'PR'=>'Puerto Rico',
'QA'=>'Qatar',
'RE'=>'Reunion',
'RO'=>'Romania',
'RU'=>'Russia',
'RW'=>'Rwanda',
'SH'=>'Saint Helena',
'KN'=>'Saint Kitts And Nevis',
'LC'=>'Saint Lucia',
'PM'=>'Saint Pierre',
'VC'=>'Saint Vincent',
'SM'=>'San Marino',
'ST'=>'Sao Tome And Principe',
'SA'=>'Saudi Arabia',
'SN'=>'Senegal',
'SC'=>'Seychelles',
'SL'=>'Sierra Leone',
'SG'=>'Singapore',
'SK'=>'Slovak Republic',
'SI'=>'Slovenia',
'SB'=>'Solomon Islands',
'SO'=>'Somalia',
'ZA'=>'South Africa',
'GS'=>'South Georgia',
'KR'=>'South Korea',
'ES'=>'Spain',
'LK'=>'Sri Lanka',
'SD'=>'Sudan',
'SR'=>'Suriname',
'SJ'=>'Svalbard And Jan Mayen',
'SZ'=>'Swaziland',
'SE'=>'Sweden',
'CH'=>'Switzerland',
'SY'=>'Syria',
'TW'=>'Taiwan',
'TJ'=>'Tajikistan',
'TZ'=>'Tanzania',
'TH'=>'Thailand',
'TG'=>'Togo',
'TK'=>'Tokelau',
'TO'=>'Tonga',
'TT'=>'Trinidad And Tobago',
'TN'=>'Tunisia',
'TR'=>'Turkey',
'TM'=>'Turkmenistan',
'TC'=>'Turks And Caicos Islands',
'TV'=>'Tuvalu',
'UG'=>'Uganda',
'UA'=>'Ukraine',
'AE'=>'United Arab Emirates',
'UK'=>'United Kingdom',
'US'=>'United States',
'UM'=>'United States Minor Outlying Islands',
'UY'=>'Uruguay',
'UZ'=>'Uzbekistan',
'VU'=>'Vanuatu',
'VA'=>'Vatican City (Holy See)',
'VE'=>'Venezuela',
'VN'=>'Vietnam',
'VG'=>'Virgin Islands (British)',
'VI'=>'Virgin Islands (US)',
'WF'=>'Wallis And Futuna Islands',
'EH'=>'Western Sahara',
'WS'=>'Western Samoa',
'YE'=>'Yemen',
'YU'=>'Yugoslavia',
'ZM'=>'Zambia',
'ZW'=>'Zimbabwe'
);
		?>	
		
		<select name="country" style='width:120px'>
			<option value='hu'>Magyarország</option>
			<?
				foreach($countries as $code => $cname)
				{
					if(strtolower($code) == $editarr[country])
						$sel = 'selected';
					else
						$sel = '';
						
					echo "<option value='".strtolower($code)."' $sel>$cname</option>";
					
				}
			?>
		</select>	
		
		<select name="county" style='width:200px'>
			<option value=''>Kérem válasszon</option>
			<?
				foreach($counties as $cname)
				{
					if($cname == $editarr[county])
						$sel = 'selected';
					else
						$sel = '';
						
					echo "<option value='$cname' $sel>$cname</option>";
					
				}
			?>
		</select>	
		</li>
		<li>
		<label></label>
		<input type="text" name="zip" value="<?=$editarr[zip]?>" id='zip'   style='width:35px;' class='numeric'/>
		
		<input type="text" name="city" id='city' style='width:100px;margin:0 1px 0 1px' value="<?=$editarr[city]?>" /><input type="text" id='address' name="address" class="small2" style='width:202px;' value="<?=$editarr[address]?>" /></li>
		
		<li><label>Kategória:</label><select name='category' id=''>
			<option value='0'  <? if($editarr[category] == 0) echo "selected";?>>nincs</option>
			<option value='1'  <? if($editarr[category] == 1) echo "selected";?>>1*</option>
			<option value='2'  <? if($editarr[category] == 2) echo "selected";?>>2*</option>
			<option value='3'  <? if($editarr[category] == 3) echo "selected";?>>3*</option>
			<option value='4'  <? if($editarr[category] == 4) echo "selected";?>>4*</option>
			<option value='5'  <? if($editarr[category] == 5) echo "selected";?>>5*</option>

		</select>
		</li>
		<li><label>GPS</label>
		
		<input type="text" name="latitude" value="<?=$editarr[latitude]?>" id='latitude'   style='width:50px;' placeholder='Latitude'/>
		<input type="text" name="longitude" value="<?=$editarr[longitude]?>" id='longitude'   style='width:50px;' placeholder='Longitude'/>

</li>

		<li><label>Helymeghatározás:</label><input type="text" name="location" value="<?=$editarr[location]?>"/></li>
		<li><label>Helymeghatározás RO:</label><input type="text" name="ro_location" value="<?=$editarr[ro_location]?>"/></li>
		<li><label>Helymeghatározás SK:</label><input type="text" name="sk_location" value="<?=$editarr[sk_location]?>"/></li>
		
		<li><label>Központi telefonszám:</label><input type="text" name="phone" value="<?=$editarr[phone]?>" id='phone'/></li>
		
	<? if($CURUSER[userclass] == 255 || $CURUSER[username] == 'miklos' ) { ?>
		<li><label>Ellenőrzés tiltása:</label>
			<select name='pp_disable'>
				<option value='0' <? if($editarr[pp_disable] == 0) echo "selected";?>>nem</option>
				<option value='1' <? if($editarr[pp_disable] == 1) echo "selected";?>>igen</option>

			</select>
 		</li>
 		<li><label>Ellenőrzés kategória:</label>
			<select name='postpone_category'>
				<option value='6' <? if($editarr[postpone_category] == 6 || $editarr[postpone_category] == '') echo "selected";?>>5 hét</option>
				<option value='3' <? if($editarr[postpone_category] == 3) echo "selected";?>>3 hét</option>
			</select>
 		</li>
 		<li><label>Utólagos elszámolás:</label>
			<select name='post_balance'>
				<option value='0' <? if($editarr[post_balance] == 0) echo "selected";?>>nem</option>
				<option value='1' <? if($editarr[post_balance] == 1) echo "selected";?>>igen</option>

			</select>
 		</li>
 	<? } ?>
 		<li><label>Egyedi utsz. elsz.:</label>
			<select name='foreign_custom_invoice'>
				<option value='0' <? if($editarr[foreign_custom_invoice] == 0) echo "selected";?>>nem</option>
				<option value='1' <? if($editarr[foreign_custom_invoice] == 1) echo "selected";?>>igen</option>

			</select>
 		</li>
 		
 		<li><label>Líra szállás:</label>
			<select name='islira'>
				<option value='0' <? if($editarr[islira] == 0) echo "selected";?>>nem</option>
				<option value='1' <? if($editarr[islira] == 1) echo "selected";?>>igen</option>

			</select>
 		</li>
 		
		<li class='hotelinfo'><label>Recepció:</label><input type="text" name="reception_phone" value="<?=$editarr[reception_phone]?>"/></li>
		<li class='hotelinfo'><label>Recepció e-mail:</label><input type="text" name="reception_email" value="<?=$editarr[reception_email]?>"/></li>
		<li class='resellerinfo'><label>MKEH:</label><input type="text" name="mkeh" value="<?=$editarr[mkeh]?>"/></li>	

		<li><label>Weboldal:</label><input type="text" name="website" value="<?=$editarr[website]?>"/></li>	
		<li class='resellerinfo'><label>Utalványnyomtatás letiltva:</label><select name='reseller_print_disabled'>
			<option value='0' <? if($editarr[reseller_print_disabled] == 0) echo "selected";?>>Nem</option>
			<option value='1'  <? if($editarr[reseller_print_disabled] == 1) echo "selected";?>>Igen</option>
		</select></li>
		<li><label>Felhasználónév*:</label><input type="text" name="username" value="<?=$editarr[username]?>" id='username'/></li>
		<li><label>Jelszó*:</label><input type="text" name="password" value="<?=$editarr[password]?>" id='password'/></li>
		
		<? if($CURUSER[userclass] == 255) { ?>
			<li><label>Feltétel:</label><input type="text" name="extra_condition" value="<?=$editarr[extra_condition]?>" id='extra_condition'/></li>
		<? } ?>
		</ul>
	</fieldset>
	
		<fieldset style='width:520 px;'>
		<legend>Kapcsolattartó adatok</legend>
		<ul>
			<li><label>1. kapcsolattartó neve:</label><input type="text" name="contact" id='contact' value="<?=$editarr[contact]?>"/></li>
			<li><label>1. kapcsolattartó tel.:</label><input type="text" name="contact_phone" id='contact_phone' value="<?=$editarr[contact_phone]?>"/></li>
			<li><label>1. kapcsolattartó e-mail:</label><input type="text" name="email" id='email' value="<?=$editarr[email]?>"/></li>
			<li><label>Beosztása:</label><input type="text" name="contact_position" value="<?=$editarr[contact_position]?>"/></li>

		</ul>
		<hr/>
		<ul>
			<li><label>Pénzügy neve:</label><input type="text" name="contact2_name"  value="<?=$editarr[contact2_name]?>"/></li>
			<li><label>Pénzügy tel.:</label><input type="text" name="contact2_phone"  value="<?=$editarr[contact2_phone]?>"/></li>
			<li><label>Pénzügy e-mail:</label><input type="text" name="contact2_email" value="<?=$editarr[contact2_email]?>"/></li>
			<li><label>Beosztása:</label><input type="text" name="contact2_position"  value="<?=$editarr[contact2_position]?>"/></li>

		</ul>
		<hr/>
		<ul>
			<li><label>Licit kapcsolattartó neve:</label><input type="text" name="licit_name"  value="<?=$editarr[licit_name]?>"/></li>
			<li><label>Licit kapcsolattartó tel.:</label><input type="text" name="licit_phone"  value="<?=$editarr[licit_phone]?>"/></li>
			<li><label>Licit kapcsolattartó e-mail:</label><input type="text" name="licit_email" value="<?=$editarr[licit_email]?>"/></li>
			<li><label>Licit értesítő e-mail:</label><input type="text" name="licit_notification" value="<?=$editarr[licit_notification]?>"/></li>
			<li><label>Licit bankszámla:</label><input type="text" name="licit_account_no"  class="accountNo" value="<?=$editarr[licit_account_no]?>"/></li>

		</ul>
		
	
		</fieldset>

	<fieldset>
		<legend>Indulhatunk kapcsolattartó adatok</legend>
		<table style='width:700px'>
			<tr class='header'>
				<td>Név</td>
				<td>E-mail cím</td>
				<td>Telefonszám</td>
				<td>Pozíció, megjegyzés</td>
			<tr>
			
			<?
				for($h = 1; $h<=4;$h++)
				{
				
				
				echo "	<tr>
				<td><input type='text' name='contact".$h."_ind_name' value='".$editarr["contact".$h."_ind_name"]."' style='width:150px'/></td>
				<td><input type='text' name='contact".$h."_ind_email' value='".$editarr["contact".$h."_ind_email"]."' style='width:150px'/></td>
				<td><input type='text' name='contact".$h."_ind_phone' value='".$editarr["contact".$h."_ind_phone"]."' style='width:150px'/></td>
				<td><input type='text' name='contact".$h."_ind_position' value='".$editarr["contact".$h."_ind_position"]."' style='width:150px'/></td>
				</tr>";	
					
				}
			?>
		</table>
	</fieldset>


	<fieldset style='width:520 px;'>
		<legend>Banki adatok</legend>
		<ul>
		<li><label>Bankszámlaszám:</label><input type="text" name="account_no" id="account_no" class="accountNo" value="<?=$editarr[account_no]?>"/></li>
		
		<li class='foreignaccount'><a href='#' id="showbankinfo" style=' display:block;width:665px; background-color:#e7e7e7;padding:5px; color:black; font-weight:bold;border:1px solid #c4c4c4;text-align:center;'>Külföldi bankszámla adatok</a></li>
		<li class='foreignbankinfo'><label>Számlatulajdonos:</label><input type="text" name="foreign_account_name"  value="<?=$editarr[foreign_account_name]?>"/></li>
		<li class='foreignbankinfo'><label>Bank neve:</label><input type="text" name="foreign_bank"  value="<?=$editarr[foreign_bank]?>"/></li>
		<li class='foreignbankinfo'><label>Bank címe:</label><input type="text" name="foreign_bank_address"  value="<?=$editarr[foreign_bank_address]?>"/></li>
		<li class='foreignbankinfo'><label>Bank SWIFT:</label><input type="text" name="foreign_swift" value="<?=$editarr[foreign_swift]?>"/></li>
		<li class='foreignbankinfo'><label>Bank IBAN:</label><input type="text" name="foreign_iban" value="<?=$editarr[foreign_iban]?>"/></li>
		<li class='foreignbankinfo'><label>Külföldi számlaszám:</label><input type="text" name="foreign_account_no" value="<?=$editarr[foreign_account_no]?>"/></li>
		</ul>
	</fieldset>

	<fieldset style='width:520 px;'>
		<legend>Számlázási adatok, jutalék</legend>
		<ul>
		<li><label>Számlázási név:</label><input type="text" name="invoice_name" value="<?=$editarr[invoice_name]?>"/></li>
		<li><label>Számlázási cím:</label><input type="text" name="invoice_zip" value="<?=$editarr[invoice_zip]?>"  style='width:35px;' class='numeric'/><input type="text" name="invoice_city" style='width:140px;margin:0 1px 0 1px'  value="<?=$editarr[invoice_city]?>" /><input type="text" name="invoice_address" class="small2" style='width:295px;' value="<?=$editarr[invoice_address]?>" /></li>
		<li><label>Adószám:</label><input type="text" name="tax_no" value="<?=$editarr[tax_no]?>"/></li>
<li><label>KATA:</label>
			<select name='is_kata'>
				<option value='0' <? if($editarr[is_kata] == 0) echo "selected";?>>nem</option>
				<option value='1' <? if($editarr[is_kata] == 1) echo "selected";?>>igen</option>

			</select>
 		</li>
 		
 		<li><label>AMEX:</label>
			<select name='is_amex'>
				<option value='0' <? if($editarr[is_amex] == 0) echo "selected";?>>nem</option>
				<option value='1' <? if($editarr[is_amex] == 1) echo "selected";?>>igen</option>

			</select>
 		</li>
 		
		<li  class='hotelinfo'><label>LMB típus:</label>
			<select name='lmb_type'>
				<option value='21' <? if($editarr[lmb_type]==21) echo"selected";?>>21 nap</option>
				<option value='7' <? if($editarr[lmb_type]==7) echo"selected";?>>7 nap</option>
			</select>
		</li>
		<li  class='hotelinfo'><label>LMB számla:</label>
			<select name='lmb_continous'>
				<option value='0' <? if($editarr[lmb_continous]==0) echo"selected";?>>nem folyamatos teljesítés</option>
				<option value='1' <? if($editarr[lmb_continous]==1) echo"selected";?>>folyamatos teljesítés: teljesítés = fizetési határidő</option>
			</select>
		</li>

<li  class='hotelinfo'><label>Rajongók típus:</label>
	<select name='rajongok_type'>
		<option value='4' <? if($editarr[rajongok_type]==4) echo "selected"; ?>>a. maga tölti + adwords + banner  (4%)</option>
		<option value='3' <? if($editarr[rajongok_type]==3) echo "selected"; ?>>b. maga tölti + adwords (6%)</option>
		<option value='5' <? if($editarr[rajongok_type]==5) echo "selected"; ?>>c. maga tölti + banner (6%)</option>
		<option value='7' <? if($editarr[rajongok_type]==7) echo "selected"; ?>>d. nem tölti + adwords + banner (6%)</option>
		<option value='2' <? if($editarr[rajongok_type]==2) echo "selected"; ?>>e. maga tölti (8%)</option>
		<option value='6' <? if($editarr[rajongok_type]==6) echo "selected"; ?>>f. nem tölti + adwords (8%)</option>
		<option value='8' <? if($editarr[rajongok_type]==8) echo "selected"; ?>>g. nem tölti + banner (8%)</option>
		<option value='1' <? if($editarr[rajongok_type]==1) echo "selected"; ?>>h. alap (12%)</option>
	</select>
</li>
		<li  class='hotelinfo'><label>Jutalék Rajongók:</label><input type="text" name="rajongok_yield"  class='floatnumber'  autocomplete="off" value="<?=$editarr[rajongok_yield]?>"/></li>

		<li  class='hotelinfo'><label>Jutalék LMB:</label><input type="text" name="yield"  class='floatnumber'  autocomplete="off" value="<?=$editarr['yield']?>"/></li>
		<li  class='hotelinfo'><label>Jutalék VTL:</label><input type="text" name="yield_vtl" class='floatnumber'  autocomplete="off" id='yield_vtl' value="<?=$editarr[yield_vtl]?>"/></li>
	
	<li class='hotelinfo'><label>QR partner:</label>
			<select name="is_qr">
				<option value="0" <?if($editarr[is_qr]==0)echo"selected";?>>nem</option>
				<option value="1" <?if($editarr[is_qr]==1)echo"selected";?>>igen</option>

			</select>
		</li>
		
		<li class='hotelinfo'><label>Múzeum:</label>
			<select name="is_museum">
				<option value="0" <?if($editarr[is_museum]==0)echo"selected";?>>nem</option>
				<option value="1" <?if($editarr[is_museum]==1)echo"selected";?>>igen</option>

			</select>
		</li>
		
		<li class='hotelinfo'><label>Áfamentes:</label>
			<select name="without_vat">
				<option value="0" <?if($editarr[without_vat]==0)echo"selected";?>>nem</option>
				<option value="1" <?if($editarr[without_vat]==1)echo"selected";?>>igen</option>

			</select>
		</li>
		
		<li class='hotelinfo'><label>Régi menü:</label>
			<select name="is_reliable">
				<option value="0" <?if($editarr[is_reliable]==0)echo"selected";?>>nem</option>
				<option value="1" <?if($editarr[is_reliable]==1)echo"selected";?>>igen</option>

			</select>
		</li>

			<li  class='hotelinfo'><label>Jutalék QR kód:</label><input type="text" name="yield_qr" id='yield_qr'   autocomplete="off" class='floatnumber'  value="<?=$editarr[yield_qr]?>"/></li>

			
		
		<li  class='resellerinfo'><label>Jutalék alap:</label><input type="text" name="yield_reseller_base"  class='floatnumber'  autocomplete="off" id='yield_reseller_base' value="<?=$editarr[yield_reseller_base]?>"/></li>
		
		<li  class='reselle
		
		rinfo'><label>Web only:</label>
		<select name="reseller_online">
				<option value="0" <?if($editarr[reseller_online]==0)echo"selected";?>>nem</option>
				<option value="1" <?if($editarr[reseller_online]==1)echo"selected";?>>igen</option>
			</select>
			
			
			
		</li>
		


		
		
		
		<li  class='resellerinfo'><label>Jutalék promóció:</label><input type="text" name="yield_reseller_promotion"  class='floatnumber'  autocomplete="off" value="<?=$editarr[yield_reseller_promotion]?>"/></li>
		<li  class='resellerinfo'><label>(Jutalék felár inaktív):</label><input type="text" name="yield_reseller_extra"  class='floatnumber'  autocomplete="off" value="<?=$editarr[yield_reseller_extra]?>"/></li>
		
		<li class='hotelinfo'><label>Kalkulált árrés:</label><input type="text" name="calculated_arrest" value="<?=$editarr[calculated_arrest]?>"/></li>
		<li class='hotelinfo'><label>Kalk. árfolyamszorzó:</label><input type="text" name="calculated_multiplier" value="<?=$editarr[calculated_multiplier]?>"/></li>
		</ul>
	</fieldset>
	<fieldset style='width:520 px;'  class='hotelinfo'>
		<legend>Szálláshely egyéb adatok</legend>
		<ul>
			
		<li class='hotelinfo'><label>1. Szobatípus:</label><input type="text" name="roomtype1_name" value="<?=$editarr[roomtype1_name]?>"/></li>
		<li class='hotelinfo'><label>2. Szobatípus:</label><input type="text" name="roomtype2_name" value="<?=$editarr[roomtype2_name]?>"/></li>
		<li class='hotelinfo'><label>3. Szobatípus:</label><input type="text" name="roomtype3_name" value="<?=$editarr[roomtype3_name]?>"/></li>

		<li class='hotelinfo'><label>Fő típusa:</label>
			<select name="property_main">
				<option value="1" <?if($editarr[property_main]==1)echo"selected";?>>Üdülésicsekk</option>
				<option value="2" <?if($editarr[property_main]==2)echo"selected";?>>Wellness</option>
				<option value="3" <?if($editarr[property_main]==3)echo"selected";?>>Gyermekbarát</option>
				<option value="4" <?if($editarr[property_main]==4)echo"selected";?>>Hegyvidék</option>
				<option value="5" <?if($editarr[property_main]==5)echo"selected";?>>Vízpart</option>
				<option value="6" <?if($editarr[property_main]==6)echo"selected";?>>Kastélyszálló</option>
				<option value="7" <?if($editarr[property_main]==7)echo"selected";?>>Gyógyfürdő</option>
			</select>
		</li>
		<li class='hotelinfo'><label>Félpanzió ÁFA kategória:</label>
			<select name="vat_extra_service">
				<option value="0" <?if($editarr[vat_extra_service]==0)echo"selected";?>>18%</option>
				<option value="1" <?if($editarr[vat_extra_service]==1)echo"selected";?>>25%</option>
				<option value="2" <?if($editarr[vat_extra_service]==2)echo"selected";?>>27%</option>

			</select>
		</li>
		<li class='hotelinfo'><label>Utalásokból elrejt:</label>
			<select name="unique_partner" id="unique_partner">
				<option value="0" <?if($editarr[unique_partner]==0)echo"selected";?>>Nem</option>
				<option value="1" <?if($editarr[unique_partner]==1)echo"selected";?>>Igen</option>
			</select>
		</li>
		
		<li class='hotelinfo'><label>Utazási szerződés:</label>
			<input type='file' name='tos'/> (csak pdf!)
			<? if($editarr[tos] == 1) echo "<b><a href='http://admin.indulhatunk.hu/documents/tour_tos/$editarr[pid].pdf' target='_blank'><font color='green'>Feltöltve!</font></a></b>";?>
			
		</li>

		<li class='hotelinfo'><label>SZÉP kártya:</label><input type="checkbox" name="property_check" <?if($editarr[property_check]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Balaton:</label><input type="checkbox" name="property_balaton" <?if($editarr[property_balaton]==1)echo "checked";?>/></li>

		<li class='hotelinfo'><label>Wellness:</label><input type="checkbox" name="property_wellness" <?if($editarr[property_wellness]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Gyermekbarát:</label><input type="checkbox" name="property_child" <?if($editarr[property_child]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Őszi szünet:</label><input type="checkbox" name="property_autumn" <?if($editarr[property_autumn]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Hegyvidék:</label><input type="checkbox" name="property_mountain" <?if($editarr[property_mountain]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Vízpart:</label><input type="checkbox" name="property_water" <?if($editarr[property_water]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Kastélyszálló:</label><input type="checkbox" name="property_castle" <?if($editarr[property_castle]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Gyógyfürdő:</label><input type="checkbox" name="property_spa" <?if($editarr[property_spa]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Állatbarát:</label><input type="checkbox" name="property_animal" <?if($editarr[property_animal]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Akadálymentesített:</label><input type="checkbox" name="property_accessibility" <?if($editarr[property_accessibility]==1)echo "checked";?>/></li>

		<li class='hotelinfo'><label>Exkluzív:</label><input type="checkbox" name="property_exclusive" <?if($editarr[property_exclusive]==1)echo "checked";?>/></li>
		<li class='hotelinfo'><label>Speciális étel:</label><input type="checkbox" name="property_special_food" <?if($editarr[property_special_food]==1)echo "checked";?>/></li>


	<li  class='hotelinfo'><label>OTP SZÉP:</label>
	<select name='szep_otp'>
		<option value='0' <? if($editarr[szep_otp]==0) echo "selected"; ?>>nem</option>
		<option value='1' <? if($editarr[szep_otp]==1) echo "selected"; ?>>igen</option>
	</select>
	<li  class='hotelinfo'><label>MKB SZÉP:</label>
	<select name='szep_mkb'>
		<option value='0' <? if($editarr[szep_mkb]==0) echo "selected"; ?>>nem</option>
		<option value='1' <? if($editarr[szep_mkb]==1) echo "selected"; ?>>igen</option>
	</select>

<li  class='hotelinfo'><label>KH SZÉP:</label>
	<select name='szep_kh'>
		<option value='0' <? if($editarr[szep_kh]==0) echo "selected"; ?>>nem</option>
		<option value='1' <? if($editarr[szep_kh]==1) echo "selected"; ?>>igen</option>
	</select>

</li>
	<li class='hotelinfo'><label>NE HASZNÁLD:</label>
			<select name="vat_category">
				<option value="0" <?if($editarr[vat_category]==0)echo"selected";?>>18%</option>
				<option value="27" <?if($editarr[vat_category]==27)echo"selected";?>>27%</option>

			</select>
		</li>
	<li>
				<li class='hotelinfo'><label>Facebook post id:</label><input type="text" name="outlet_post_id" value="<?=$editarr[outlet_post_id]?>"/></li>

	</li>
	</ul>
	
		
		<ul>
			<li><label>Megjegyzés</label>
				<textarea name='special_comment' style='height:150px;'><?=$editarr[special_comment]?></textarea>
			</li>
			
			<li><label>Megközelítés</label>
				<textarea name='directions' style='height:150px;'><?=$editarr[directions]?></textarea>
			</li>
		</ul>

	</fieldset>
	
	<ul>
		<li style='text-align:center; list-style:none;'><input type="submit" value="Mentés" id='savepartner' /></li>
	</ul>
	</form>
</div>

</div></div>
<?
foot();
die;
}
?>

<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href="?add=1">Új partner felvitele</a></li> <!-- href must be unique and match the id of target div -->
						<li><a href="partners.php" class="default-tab current">Partnerek</a></li>
					
					<? if($CURUSER[username] <> 'novak') { ?>
						<li><a href="new_contracts.php">Folyamatban</a></li>					
						<li><a href="contracts.php?type=outlet">Outlet</a></li>
						<li><a href="contracts.php?type=lmb">LMB</a></li>
						<li><a href="contracts.php?type=rajongok">Rajongók</a></li>
						<li><a href="partners.php?type=maybe">Majdnem döglött akták</a></li>
						<li><a href="partners.php?type=inactive">Döglött akták</a></li>
					<? } ?>
						<li><a href="partners.php?type=licittravel" class='<? if($_GET[type] == 'licittravel') echo "current";?>'>LicitTravel</a></li>
						<li><a href="imap/contract_date.php" rel='facebox'>Frissítés</a></li>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>

<a href='?partnertype=0' class='button blue'>Hotelek</a>
<a href='?partnertype=5' class='button green'>Viszonteladók</a>
<a href='?partnertype=3' class='button grey'>Utazásszervezők</a>

<div class='cleaner'></div>
<?
echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td>-</td>";
	echo "<td>No.</td>";
	echo "<td>Cég neve</td>";
	echo "<td>Város</td>";
	echo "<td>Kapcsolattartó</td>";
	echo "<td>Telefonszám</td>";
	
	if($CURUSER[username] <> 'novak')
	{
		echo "<td>LMB</td>";
		echo "<td>VTL</td>";
		echo "<td>Info</td>";
	}
echo "</tr>";


while($arr = mysql_fetch_assoc($query)) {


$nowdate = strtotime(date("Y-m-d"));
$thendate = strtotime($arr[last_contract]); 
$datediff = ($nowdate - $thendate); 
$days = round($datediff / 86400);
	
	if($days >= 300)
		$class = 'red';
	elseif($days < 300 && $days >= 180)
		$class = 'blue';
	elseif($days < 180 && $days >= 80)
		$class = 'green';
	else
		$class = '';
		
	if($_GET[type] == 'maybe')
		$class = 'grey';
	if($_GET[type] == 'inactive')
		$class = 'red';
	echo "<tr class=\"$class\">";

		echo "<td width='20'><a href=\"?edit=$arr[pid]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='20'/></a></td>";
		//echo "<td>$arr[coredb_id]</td>";
		//echo "<td>$arr[coredb_package]</td>";
		if($arr[coredb_id] == 0)
			$coreDB = "0<br/>";
		else
			$coreDB = '';
			
		if($arr[userclass] == 5)
			$prefix = 'P-';
		elseif($arr[userclass] == 0)
			$prefix = 'H-';
			
		echo "<td align='center' class='nobr'>$arr[pid]<br/>".$prefix."$arr[partner_id]</td>";
		
		if($CURUSER[username] == 'novak')
			echo "<td><b>$arr[company_name] $arr[hotel_name]</b> </td>";
		else
			echo "<td><a href='info/partner.php?cid=$arr[pid]' rel='facebox'><b>$arr[company_name] $arr[hotel_name]</b></a> </td>";
	//	echo "<td>$arr[tax_no]</td>";
		echo "<td>$arr[city]</td>";
		echo "<td>$arr[contact]</td>";
	//	echo "<td>$arr[account_no]</td>";
	//	echo "<td>$arr[zip] $arr[city] $arr[address]</td>";
		echo "<td>$arr[phone]</td>";
//		echo "<td width='50'>$arr[email]</td>";

	if($CURUSER[username] <> 'novak') {
		echo "<td align='right'>".$arr['yield']."%</td>";
		echo "<td align='right'>$arr[yield_vtl]%</td>";
	
		
		
		if($arr[coredb_id] > 0)
		{
			$addpartner = '';
		}
		else
		{
			$addpartner = "<br/><a href='?createcoredb=1&pid=$arr[pid]' rel='facebox'>[coredb-be]</a>";
		}
		
		if($CURUSER[userclass] > 5 && $CURUSER[username] <> 'novak' && $arr[coredb_id] > 0)
		{
			$request= "<br/><a href='requests.php?pid=$arr[coredb_id]'>[lekérések]</a>";
			$request.= "<br/><a href='reviews.php?pid=$arr[coredb_id]'>[értékelések]</a>";

			
		}
		else
			$request = ''; 
			
		if($arr[userclass] ==  5)
			$partnerlink = "<br/><a href='offices.php?pid=$arr[pid]'>[alirodák]</a>";
			
		else
			$partnerlink = "<br/><a href='own_vouchers.php?pid=$arr[partner_id]'>[voucherek]</a><!--<br/><a href='editdiscount.php?pid=$arr[coredb_id]'>[kedvezmény%]</a>--><br/><a href='partnerinfo.php?pid=$arr[pid]'>[kimutatás]</a><br/><a href='calls.php?pid=$arr[pid]'>[hívások]</a><br/><a href='fullhouse.php?pid=$arr[pid]'>[teltház]</a>$request
				 ";
		echo "<td><a href='contracts.php?pid=$arr[partner_id]'>[szerződések]</a>$partnerlink $addpartner</td>";
	}
	echo "</tr>";
	
}
echo "</table>";
?>
</div>
</div>
<?
foot();
?>
<!--<script>
alert('Bocs Miki');
</script>-->