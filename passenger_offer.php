<?
	/*
	 * customers.php 
	 *
	 * customers_tour page
	 *
	*/
	
	/* bootstrap file */
	include("inc/tour.init.inc.php");
	userlogin();
	
	if ($CURUSER[userclass] < 50)
	{
		header("location: index.php");
	}
	
	$id = (int)$_GET[id];

	$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $id LIMIT 1"));

	
	if($_GET[delete] > 0 && $_GET[sure] <> 1)
	{
		echo message("Törölni szeretné a tételt? <a href='?delete=$_GET[delete]&sure=1&id=$_GET[id]'>igen</a>");
		die;
	}
	if($_GET[delete] > 0 && $_GET[sure] == 1)
	{
		$mysql->query("DELETE FROM customers_tour_offer_items WHERE id = $_GET[delete]");
	}
	if($_GET[checkprice] == 1)
	{
		
		$rooms = $mysql_tour->query("SELECT * FROM prices WHERE offer_id = $_GET[offer_id] AND nights = $_GET[nights] AND from_date = '$_GET[from_date]' ORDER BY room_type ASC");

		echo "<option value=''>Kérem válasszon</option>";
		
		while($arr = mysql_fetch_assoc($rooms))
		{
			$fields = array();
			foreach($arr as $key => $value)
			{
				$fields[] = "$key='$value'";
			}
			$fields = implode(" ",$fields);
			echo "<option value='$arr[id]' $fields>$arr[room_type]</option>";
		}
		die;
	}
	
	if($_GET[checkdates] == 1)
	{
		
		$rooms = $mysql_tour->query("SELECT * FROM prices WHERE offer_id = $_GET[offer_id] GROUP BY from_date,nights,departure ORDER BY from_date, nights,departure ASC");

		echo "<option value=''>Kérem válasszon</option>";
		
		while($arr = mysql_fetch_assoc($rooms))
		{
/*
			$fields = array();
			foreach($arr as $key => $value)
			{
				$fields[] = "$key='$value'";
			}
			$fields = implode(" ",$fields);
*/
			echo "<option value='$arr[id]' from_date='$arr[from_date]' nights='$arr[nights]' day_num='$arr[day_num]' to_date='$arr[to_date]'>$arr[from_date] $arr[nights] éj, $arr[departure]</option>";
		}
		die;
	}
	
	
	if($_GET[sendemail] == 1)
	{
		echo message("Elküldve az utasnak");
		$agent = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $offer[agent_id] LIMIT 1"));

		$offer[offer_letter] = $offer[offer_letter]."
<br/><br/>Üdvözlettel: <br/> <br/> <strong>$agent[company_name]</strong><br/>
Indulhatunk.hu Nemzetközi Utazási Irodák<br/><br/>
Közvetlen e-mail: <a
        href='mailto:$agent[email]'>$agent[email]</a>
<br/> Közvetlen Telefonszám: $agent[phone]<br/>";
		sendEmail($offer[offer_subject],$offer[offer_letter],$offer[email],$offer[name],$agent[email], $agent[company_name]." ✈ Indulhatunk.hu", $source = 'plain',$amazon = 0,$disablesignature = 0,array('passenger_offer',"passenger-$offer[id]"));
		
		die;	
	}
	$editarr[latitude] = '0.0000';
	$editarr[longitude] = '0.0000';
	
	if ($_POST[type] == 1 && $_POST[offer_id] > 0)
	{
		
		$cid = $_POST[offer_id];
		
		$data[offer_letter] = $_POST[offer_letter];
		$data[offer_subject] = $_POST[offer_subject];

		$mysql->query_update("customers_tour", $data, "id=$cid");
		
		$msg = "Sikeresen módosította a levelet!";
		writelog("$CURUSER[username] modified letter for $cid");
	}
	
	/****/
	
	$base = $_POST[base_price]*$_POST[adults]+$_POST[child_value]*$_POST[child_number];
	
	$persons = $_POST[adults]+$_POST[child_number];
	
	
	if($_POST[storno_insurance_base] == 0)
	{
		$storno = $base*($_POST[storno_insurance]/100);
		$_POST[storno_insurance_base] = $base;
	}
	else
		$storno = $_POST[storno_insurance_base]*($_POST[storno_insurance]/100);
		
		

	
	$_POST[total] =
				$base + //base price
				$storno + //storno insurance
				$_POST[insurance_value] + //general insurance
				
				//special fees
				 ($_POST[airport_fee] + $_POST[visa_fee] + $_POST[reservation_fee] + $_POST[kerosene_fee] + $_POST[service_fee] + $_POST[transfer_fee]  + $_POST[local_fee])*$persons +
				//extra fees
				($_POST[extra1_value] + $_POST[extra2_value] + $_POST[extra3_value] + $_POST[extra4_value] + $_POST[extra5_value] + $_POST[extra6_value]);
				
			/*	echo "$base + //base price
				$base*($_POST[storno_insurance]/100) + //storno insurance
				$_POST[insurance_value] + //general insurance
				
				//special fees
				 ($_POST[airport_fee] + $_POST[visa_fee] + $_POST[reservation_fee] + $_POST[kerosene_fee] + $_POST[service_fee] + $_POST[transfer_fee])*$persons +
				//extra fees
				($_POST[extra1_value] + $_POST[extra2_value] + $_POST[extra3_value] + $_POST[extra4_value] + $_POST[extra5_value] + $_POST[extra6_value])";
				*/
		
	
	$_POST[total] = round_five($_POST[total]);
	/****/
	
	if ($_POST[type] == 2 && $_POST[id] > 0)
	{
		unset($_POST[type]);
		$cid = $_POST[id];
		$data = $_POST;
		
		$mysql->query_update("customers_tour_offer_items", $data, "id=$cid");
		
		$msg = "Sikeresen módosította a levelet!";
		writelog("$CURUSER[username] modified item for $cid");
	}
	
	if ($_POST[type] == 2 && $_POST[id] == '')
	{
		unset($_POST[type]);
		$cid = $_POST[id];
		$data = $_POST;
		
		$data[added] = 'NOW()';
		$id = $mysql->query_insert("customers_tour_offer_items", $data);
		
		if($_POST[latitude] <> '0.0000' && $_POST[tour_id] > 0)
		{
			
			$mysql_tour->query("UPDATE tour SET latitude = '$_POST[latitude]', longitude = '$_POST[longitude]', manual_gps = 1 WHERE id = $_POST[tour_id] AND manual_gps = 0");

		}
		$msg = "Sikeresen létrehozta a tételt;";
		writelog("$CURUSER[username] created item for $cid");
		header("Location: /passenger_offer.php?add=1&id=$_POST[offer_id]&edit=$id");
		die;
	}
	
	
	
	if ($offer[id] == '')
	{
		header("location: passengers.php");
	}
	
	
	if($_POST[picid] > 0 && !empty($_FILES))
	{
	
	$pathTo = "images/tour_offers/$_POST[offer_id]/$_POST[id]";

		makedir("$pathTo");
		makedir("$pathTo/orig/");
		makedir("$pathTo/364_252/");
		

	$imagecount = 0;
	$gallery = 1;
	for($i = 1; $i<=5;$i++)
	{
		//if($gallery <= 9)
		//	$gallery = "0".$gallery;
			
		if($_FILES["file".$i]["name"] <> '' && $_FILES["file".$i]['size'] < 1* 1024 * 1024)
		{
			if(move_uploaded_file($_FILES["file".$i]["tmp_name"],"$pathTo/orig/0$gallery.jpg"))
			{
				
			
				Image("$pathTo/orig/0$gallery.jpg", '1.445:1', '364x',"$pathTo/364_252/0$gallery.jpg");
				$imagecount++;
			}


		}
		elseif($_FILES["file".$i]['size'] > 1* 1024 * 1024)
		{
			echo message("A ".$_FILES["file".$i]["name"]." kép mérete túl nagy! Max 1 MB!");
		}
		
		$gallery++;
	}
	
	
	if($imagecount > 0)
	{
		$imgnum = count(glob("images/tour_offers/$_POST[offer_id]/$_POST[id]/orig/*")); 

		//echo message("$imagecount db kép feltöltve!");
		$mysql->query("UPDATE customers_tour_offer_items SET image_num = $imgnum WHERE id = $_POST[id]");
	}

	}
	
	$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $id LIMIT 1"));


	head("Utas ajánlatkérés készítése");
?>
	<div class='content-box'>
		<div class='content-box-header'>
			<ul class="content-box-tabs">
				
				<li><a href="/passengers.php">Minden utas</a></li>
				<li><a href="/passengers.php?add=1&editid=<?=$offer[id]?>"><?=$offer[offer_id]?> adatlapja</a></li>
				<li><a href="/passenger_offer.php?id=<?=$offer[id]?>" <? if($_GET[edit] == '') { ?>class='current' <? } ?>>Ajánlat szerkesztése</a></li>
				<li><a	href='http://www.indulhatunk.hu/utazasi-ajanlat/<?= md5("DARTHVADER19282" . $offer[id]) ?>'	target='_blank'><b>Előnézet</b></a></li>
				<? if($_GET[edit] <> '') { ?>
 				<li><a href="/passenger_offer.php?id=<?=$offer[id]?>" class='current'>Ajánlat tételei</a></li>
 				<? } ?>

			</ul>
			<div class="clear"></div>
		</div>
		<div class='contentpadding'>
			
			<?= message($msg) ?>
			
			<script type='text/javascript' src='https://geoxml3.googlecode.com/svn/branches/polys/geoxml3.js'></script>
			<script type="text/javascript"
			        src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&ext=.js"></script>
			<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
			<script type="text/javascript">
				$().ready(function () {
					
					
					var a = $('#offer_name').autocomplete({
						serviceUrl: 'info/showtour.php',
						minChars: 2,
						delimiter: /(,|;)\s*/, // regex or character
						maxHeight: 400,
						width: 500,
						zIndex: 9999,
						deferRequestBy: 0, //miliseconds
						params: {country: 'Yes'}, //aditional parameters
						noCache: true, //default is false, set to true to disable caching
						// callback function:
						onSelect: function (value, data) {
							//console.log(value+" >"+data);	
							$("#board").val(value.data.board);
							$("#travel").val(value.data.travel);
							$("#tour_id").val(value.data.offer_id);
							$("#destination").val(value.data.destination);
							$("#inprice").val(value.data.inprice);
							$("#outprice").val(value.data.outprice);
						
							$("#lat").val(value.data.lat);
							$("#lng").val(value.data.lng);

							$("#partner_id").val(value.data.partner_id);
							$("#offerlink").html("<a href='" + value.data.page_url + "' target='_blank'>[link]</a>");
							$("#offer_name").val(value.data.offer_name);
							
							$("#imagenum").html(value.data.image_num + " db kép")
							
							//startdates
							$.get('passenger_offer.php?checkdates=1&offer_id=' + value.data.offer_id, function (data) {
								$("#startdates").html('');
								$("#startdates").append(data);	
								
								$("#startdates").show();
							});
							reloadMap();
						}
						// local autosugest options:
						//lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
					});
					
					$('textarea.tinymce').tinymce({
						// Location of TinyMCE script
						script_url: '../jscripts/tiny_mce/tiny_mce.js',
						
						// General options
						theme: "advanced",
						plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
						
						// Theme options
						theme_advanced_buttons1: "code,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
						theme_advanced_toolbar_location: "top",
						theme_advanced_toolbar_align: "left",
						theme_advanced_statusbar_location: "bottom",
						theme_advanced_resizing: true,
						extended_valid_elements: "iframe[class|src|frameborder=0|alt|title|width|height|align|name]",
						// Example content CSS (should be your site CSS)
// 						content_css: "css/content.css",
						width: 600,
						height: 400,
						force_br_newlines: true,
						force_p_newlines: false,
						
						// Drop lists for link/image/media/template dialogs
						template_external_list_url: "lists/template_list.js",
						external_link_list_url: "lists/link_list.js",
						external_image_list_url: "lists/image_list.js",
						media_external_list_url: "lists/media_list.js",
						
						// Replace values for the template plugin
						template_replace_values: {
							username: "Some User",
							staffid: "991234"
						}
					});
					
							$(".locate").click(function() {
									
							});
							
							$("#lookup").click(function () {
								var offer_id = $("#tour_id").val()*1;
								var from_date = $("#from_date_tour").val();
								var nights = $("#nights_tour").val()*1;
								var error = 0;
								if(offer_id == 0)
								{
									alert('Válasszon utazási ajánlatot');
									error = 1;
								}
								else if(from_date == '0000-00-00')
								{
									alert('Válasszon indulási időpontot');
									error = 1;
								}
								else if(nights == 0)
								{
									alert('Adja meg az utazás napjainak számát');
									error = 1;
								}
								else
								{
								  $.get('passenger_offer.php?checkprice=1&offer_id=' + offer_id + '&from_date=' + from_date + '&nights=' + nights, function (data) {
								  	$("#roomtypes").html('');

								  	$("#roomtypes").append(data);
								  	$("#roomselect").show();
								  	
								  });
								  }
            
            
								return false;
							});
							
							
							var geocoder;
							var map;
							var markersArray = [];
							var mapOptions = {
								center: new google.maps.LatLng($("#lng").val(), $("#lat").val()),
								zoom: 8,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							}
							var marker;
							
							function reloadMap()
							{
																
									var latlng = new google.maps.LatLng($("#lng").val(), $("#lat").val());
									map.panTo(latlng);
									map.setCenter(latlng);
									createMarker(latlng);
								
								
							}
							jQuery('#mapslink').on('input propertychange paste', function () {
								
								var currval = $(this).val();
								var elements = [];
								if (currval != '') {
									elements = currval.split('@');
									elements = elements[1].split(',');
									
									$("#lng").val(elements[0]);
									$("#lat").val(elements[1]);


									reloadMap();
								
								}
								
							});
							
							
							function createMarker(latLng) {
								if (!!marker && !!marker.setMap) {
									
									console.log('alert' + latLng);
									// marker.setMap(null);
									marker.setPosition(latLng);
								} else { // if marker doesn't exist, create it
									marker = new google.maps.Marker({
										map: map,
										position: latLng,
										draggable: true
									});
								}
								document.getElementById('lat').value = marker.getPosition().lat().toFixed(6);
								document.getElementById('lng').value = marker.getPosition().lng().toFixed(6);
								
								google.maps.event.addListener(marker, "dragend", function () {
									document.getElementById('lat').value = marker.getPosition().lat().toFixed(6);
									document.getElementById('lng').value = marker.getPosition().lng().toFixed(6);
								});
							}
							
							function initialize() {
								geocoder = new google.maps.Geocoder();
								map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
								//codeAddress();
								
								var latlng = new google.maps.LatLng($("#lng").val(), $("#lat").val());
								
								map.setCenter(latlng);
								createMarker(latlng);
								
								
								google.maps.event.addListener(map, 'click', function (event) {
									map.panTo(event.latLng);
									map.setCenter(event.latLng);
									createMarker(event.latLng);
								});
								
							}
							
							$("#startdates").change(function() {
								var selected = $(this).find('option:selected'); 

								$("#from_date_tour").val(selected.attr('from_date'));
								$("#to_date").val(selected.attr('to_date'));

								$("#nights_tour").val(selected.attr('nights'));
								$("#days").val(selected.attr('day_num'));

								 $.get('passenger_offer.php?checkprice=1&offer_id=' + $("#tour_id").val() + '&from_date=' + selected.attr('from_date') + '&nights=' + selected.attr('nights'), function (data) {
								  	$("#roomtypes").html('');

								  	$("#roomtypes").append(data);
								  	$("#roomselect").show();
								  	
								  });



							});
							$("#roomtypes").change(function(){
								var selected = $(this).find('option:selected'); 
								$("#to_date").val(selected.attr('to_date'));
								$("#board").val(selected.attr('board'));
								$("#days").val(selected.attr('day_num'));
								$("#base_price_name").val('Alapár');
								$("#price").val(selected.attr('price'));
								$("#bed_number").val(selected.attr('bed_number'));
								$("#airport_fee").val(selected.attr('airport_fee'));
								$("#kerosene_fee").val(selected.attr('kerosene_fee'));
								$("#transfer_fee").val(selected.attr('transfer_fee'));
								$("#local_fee").val(selected.attr('local_fee'));
								$("#reservation_fee").val(selected.attr('reservation_price'));
								$("#visa_fee").val(selected.attr('visa_fee'));
								$("#room_type").val(selected.attr('room_type'));
								$("#extra_bed").val(selected.attr('extra_bed'));
								$("#extra_price").val(selected.attr('child1_price'));

								$("#optional1_price").val(selected.attr('optional1_price'));
								$("#optional2_price").val(selected.attr('optional2_price'));
								$("#optional3_price").val(selected.attr('optional3_price'));
								$("#optional4_price").val(selected.attr('optional4_price'));
								$("#optional5_price").val(selected.attr('optional5_price'));
								$("#optional6_price").val(selected.attr('optional6_price'));
								
								$("#optional5_price").val(selected.attr('optional5_price'));

								

								$("#storno_insurance").val(selected.attr('storno_insurance'));

							});
							
							google.maps.event.addDomListener(window, 'load', initialize);
							
							
							
				});
			</script>
			
			<? if($_GET[add] <> 1) {
				
				if($offer[offer_letter] == '')
				{
					$url  = "http://www.indulhatunk.hu/utazasi-ajanlat/".md5("DARTHVADER19282" . $offer[id]);
					$missingletter = 1;
					$offer[offer_letter] = "
					<p><strong>Kedves $offer[name]!</strong></p>
<br/>Köszönettel megkaptuk az <a href='http://indulhatunk.hu/'>Indulhatunk.hu</a> részére beküldött érdeklődését!
<br/>
<span class='hideletter'>
<br/>Az Ön érdeklődésének megfelelően összeáll&iacute;tott ajánlatot az alábbi linkre kattintva tekintheti meg:
<br/><br/><strong><a href='$url' style='color:#1177a3;font-weight:bold;'>Ajánlat megtekintése &raquo;</a></strong><br/>
</span>
<br/>
Kérem jelezzen vissza mihamarabb, hogy a kiküldött ajánlat megfelel-e Önöknek. Amennyiben szeretnék kérni az ajánlatot, van mód 8 napon kívüli indulások esetében 24 órás opciós foglalásra, ehhez szükségünk lenne az úti okmány szerinti nevekre, születési dátumokra, a szerződő lakcímére és telefonszámára. Foglalását 30 napon kívüli indulás esetén 40% előleggel, 30 napon belüli indulás esetén a teljes összeg befizetésével véglegesíthetik. <br/><br/>

<b>FONTOS:</b> az árak a mai napra érvényesek, későbbi foglalás esetén változhatnak! <br/><br/>	

További ajánlatok az Indulhatunk.hu weboldalon érhetőek el. Amennyiben még egyéb lehetőségek is érdeklik, kérem jelezze nekem telefonon, vagy e-mailen.";

				}
				 ?>
			<form method='post'>
				
				<fieldset>
					<table>
						<input type='hidden' name='offer_id' value='<?= $offer[id] ?>'/>
						<input type='hidden' name='tour_id' value='<?= $offer[tour_id] ?>'/>

						<input type='hidden' name='type' value='1'/>
						<? if($offer[offer_open] <> '0000-00-00 00:00:00')
						{
							
						?>
							<tr>
							<td class='lablerow'>Megnyitotta</td>
							<td><?= $offer[offer_open] ?></td>
						</tr>
						<? } ?>
 						<tr>
							<td class='lablerow'>Utas neve</td>
							<td><?= $offer[name] ?></td>
						</tr>
						<tr>
							<td class='lablerow'>E-email címe</td>
							<td><?= $offer[email] ?></td>
						</tr>
						<tr>
							<td class='lablerow'>Telefonszáma</td>
							<td><?= $offer[phone] ?></td>
						</tr>
						<tr>
							<td class='lablerow'>Tárgy</td>
							<td><input type="text" name="offer_subject" id='offer_subject' value="<?if($offer[offer_subject] == '') echo "Utazási ajánlata elkészült"; else echo $offer[offer_subject]; ?>" style='width:95%'/>
							</td>
						</tr>
						<tr>
							<td class='lablerow'>Üzenet</td>
							<td><textarea name='offer_letter' class='tinymce'><?= $offer[offer_letter] ?></textarea>
							</td>
						</tr>
						<tr>
							<td colspan='2' align='center'><input type='submit' value='Mentés'/></td>
						</tr>
			</form>
			</table>
			</fieldset>
			</form>
			
			<? if($missingletter <> 1) { ?>
			
			
			<fieldset>
				<legend>Tételek</legend>
				<table class='sorted_table'>
					<tr>
						<td colspan='5' align='center'><a href='?add=1&id=<?= $offer[id] ?>'><b>új elem
									hozzáadása</b></a> | <a href='?sendemail=1&id=<?= $offer[id] ?>' rel='facebox'><b>E-mail küldése az utasnak</b></a></td>
					</tr>
					<?
						$subitems = $mysql->query("SELECT * FROM customers_tour_offer_items WHERE offer_id = $offer[id] ORDER BY hidden, priority ASC");
						while ($arr = mysql_fetch_assoc($subitems))
						{
							if($arr[liked] == 1)
								$class = 'green';
							else
								$class = '';
								
							if($arr[hidden] == 1)
								$hidden = 'rejtve';
							else
								$hidden = 'látszik';
								
							echo "
				<tr class='$class' id='$arr[id]'><td width='45'><a href='?add=1&id=$offer[id]&edit=$arr[id]'><img src='/images/edit.png' width='20'/></a> <a href='?delete=$arr[id]&id=$offer[id]' rel='facebox'><img src='/images/trash.png' width='20'/></a></td><td width='10'>$arr[priority]</td><td align='center' width='70'>$arr[added]</td><td>$arr[offer_name]</td><td align='center'>$hidden</td></tr>
			";
						}
					?>
				</table>
			</fieldset>
			
			<? } ?>
			<?
				}
				else
				{
					if ($_GET[edit] > 0)
						$editarr = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour_offer_items WHERE id = '$_GET[edit]'"))
					?>
					
					<script type="text/javascript">
						$().ready(function () {
							
				
						});
					
					</script>
				
				<?
					$mapurl = "https://maps.google.hu/maps?q=" . urlencode($editarr[offer_name]) . "&ie=UTF8&t=&z=17&iwloc=B";
					if($editarr[priority] == '')
					{
						$checkpriority = mysql_fetch_assoc($mysql->query("SELECT priority FROM customers_tour_offer_items WHERE offer_id = $offer[id] ORDER BY priority DESC LIMIT 1"));
						$editarr[priority] = $checkpriority[priority]+1;
					}
				?>
					<br/><br/>
					<form method='post'>
						<input type='hidden' name='type' value='2'/>
						<input type='hidden' name='id' value='<?= $editarr[id] ?>'/>
						<input type='hidden' name='offer_id' id='offer_id' value='<?= $offer[id] ?>'/>
						
						<fieldset>
							
							<table>
								<tr>
									<td colspan='4'><a href='?id=<?=$offer[id]?>'><b>&laquo; összes tételhez</b></a></td>
								</tr>
								<tr>
									<td class='lablerow'><input type='text' class='numeric' name='priority'
										       value='<?=$editarr[priority] ?>' style='width:30px;text-align:center'/> út neve*</td>
									<td colspan='3'>
										<input type='text' name='offer_name' id='offer_name' style='width:640px;'
										       value='<?= $editarr[offer_name] ?>'/> <span
											id='offerlink'><? if ($editarr[offer_url] <> '')
											{ ?><a href='<?= $editarr[offer_url] ?> ' target='_blank'><b>[link]</b>
												</a> <? } ?></span>
										<input type='hidden' id='tour_id' name='tour_id'
										       value='<?= $editarr[tour_id] ?>'/>
									</td>
								</tr>
								
								<tr>
									<td class='lablerow'>Indulás dátuma:
										<select id='startdates' style='display:none'>
										</select>
									</td>
									<td>
										
										<input type="text" name="from_date" id='from_date_tour' value="<?= $editarr[from_date] ?>"
									           style='width:110px;text-align:right;'
									           class='simpledate dpick offer_datepick'/> - <input type="text" name='to_date' id='to_date' value="<?= $editarr[to_date] ?>"
									           style='width:110px;text-align:right;' class='simpledate'/></td>
									<td class='lablerow'>Éjszakák száma:</td>
									<td><input type="text" name="nights" id='nights_tour' value="<?= $editarr[nights] ?>"
									           style='width:30px' class='numeric'/> éj <input type="text" name="days" id='days' value="<?= $editarr[days] ?>" style='width:30px'
									           class='numeric'/>  nap</td>
								
								
								</tr>
								
								<tr id='roomselect' style='display:none;width:100%'>
									<td colspan='4'>
										<select id='roomtypes'>
										</select>
									</td>
								</tr>


														
								<tr>
									<td class='lablerow'>Desztináció</td>
									<td colspan='3'>
										<input type='text' name='destination' id='destination' style='width:540px;'
										       value='<?= $editarr[destination] ?>'/> <a href='<?= $mapurl ?>'
										                                                 class='locate' target='_blank'>[maps]</a>
										
										<input type='text' placeholder='Ide másold be a google maps linket'
										       id='mapslink' style='width:640px'/>
										
										<input id="lat" type="hidden" placeholder="Lat" name='latitude'
										       value='<?= $editarr[longitude] ?>'/>
										<input id="lng" type="hidden" placeholder="Lng" name='longitude'
										       value='<?= $editarr[latitude] ?>'/>
										
										<div id="map-canvas" style='width:100%; height:200px;'></div>
									
									
									</td>
								</tr>
								<tr>
									<td class='lablerow'>Szobatípus</td>
									<td colspan='3'>
										<input type="text" name="room_type" id='room_type' style='width:100%;'
										       value="<?= $editarr[room_type] ?>"/>
									</td>
									</tr>
								<tr>
									<td class='lablerow'>Ellátás</td>
									<td>
										<input type="text" name="board" id='board' style='width:200px;'
										       value="<?= $editarr[board] ?>"/>
									</td>
									<td class='lablerow'>Utazás módja</td>
									<td>
										<input type="text" name="travel" id='travel' style='width:200px;'
										       value="<?= $editarr[travel] ?>"/>
									</td>
								</tr>
								
								
								<tr>
									<td class='lablerow'><input type="text" name="base_price_name" id="base_price_name"
									                            value="<?=$editarr[base_price_name] ?>"
									                            style='width:135px'/></td>
									<td><input type="text" name="base_price" id='price' value="<?= $editarr[base_price] ?>"
									           style='width:90px' class='numeric '/> Ft * <input type="text"
									                                                             name="adults"
									                                                             id='bed_number'
									                                                             value="<?= $editarr[adults] ?>"
									                                                             style='width:15px'
									                                                             class='numeric'/> fő
									</td>
									<td class='lablerow'>Pótágy:</td>
									<td><input type="text" name="child_value" value="<?= $editarr[child_value] ?>"
									           style='width:80px' class='numeric' id='extra_price'/> Ft * <input type="text"
									                                                            name="child_number"
									                                                            value="<?= $editarr[child_number] ?>"
									                                                            style='width:15px'
									                                                            class='numeric' id='extra_bed'/> fő
									</td>
								</tr>
								
								<tr>
									<td class='lablerow'>Reptéri illeték:</td>
									<td><input type="text" name="airport_fee" value="<?= $editarr[airport_fee] ?>"
									           style='width:110px' id='airport_fee' class='numeric'/> Ft/fő
									</td>
									
									<td class='lablerow'>Vízum:</td>
									<td><input type="text" name="visa_fee" value="<?= $editarr[visa_fee] ?>"
									           style='width:110px' id='visa_fee' class='numeric'/> Ft/fő
									</td>
								</tr>
								
								
								<tr>
									<td class='lablerow'>Foglalási díj:</td>
									<td><input type="text" name="reservation_fee" id='reservation_fee'
									           value="<?= $editarr[reservation_fee] ?>" style='width:110px'
									           class='numeric'/> Ft/fő
									</td>
									<td class='lablerow'>Kerozin díj:</td>
									<td><input type="text" name="kerosene_fee" id='kerosene_fee' value="<?= $editarr[kerosene_fee] ?>"
									           style='width:110px' class='numeric'/> Ft/fő
									</td>
								</tr>
								
								<tr>
									<td class='lablerow'>Szerviz díj:</td>
									<td><input type="text" name="service_fee" id='service_fee' value="<?= $editarr[service_fee] ?>"
									           style='width:110px' class='numeric'/> Ft/fő
									</td>
									<td class='lablerow'>Transzfer díj:</td>
									<td><input type="text" name="transfer_fee" id='transfer_fee' value="<?= $editarr[transfer_fee] ?>"
									           style='width:110px' class='numeric'/> Ft/fő
									</td>
								</tr>
								<tr>
									<td class='lablerow'>Üdülőhelyi díj:
									</td>
									<td><input type="text" name="local_fee" id='local_fee' value="<?= $editarr[local_fee] ?>"
									           style='width:110px;' class='numeric'/> Ft/fő
									</td>
									<td class='lablerow'></td>
									<td></td>
								</tr>
								
								<tr>
									<td class='lablerow'>Stornó biztosítás:
									</td>
									<td><input type="text" name="storno_insurance_base"
									           value="<?= $editarr[storno_insurance_base] ?>"
									           style='width:70px;text-align:right;' class='numeric'/> Ft <input
											type="text" name="storno_insurance"
											value="<?= $editarr[storno_insurance] ?>" id='storno_insurance'
											style='width:30px;text-align:right;'/> %-a
									</td>
									<td class='lablerow'><input type="text" name="insurance_name"
									                            value="<?= $editarr[insurance_name] ?>"
									                            style='width:80px'/> biztosítás:
									</td>
									<td><input type="text" name="insurance_value"
									           value="<?= $editarr[insurance_value] ?>" style='width:110px'
									           class='numeric'/> Ft
									</td>
								</tr>
								<tr>
									<td class='lablerow'><input type="text" name="extra1_name"
									                            value="<?= $editarr[extra1_name] ?>"
									                            style='width:135px'/></td>
									<td><input type="text" name="extra1_value" value="<?= $editarr[extra1_value] ?>"
									           style='width:110px' class='numeric'/> Ft
									</td>
									<td class='lablerow'><input type="text" name="extra2_name"
									                            value="<?= $editarr[extra2_name] ?>"
									                            style='width:135px'/></td>
									<td><input type="text" name="extra2_value" value="<?= $editarr[extra2_value] ?>"
									           style='width:110px' class='numeric'/> Ft
									</td>
								</tr>
								<tr>
									<td class='lablerow'><input type="text" name="extra3_name"
									                            value="<?= $editarr[extra3_name] ?>"
									                            style='width:135px'/></td>
									<td><input type="text" name="extra3_value" value="<?= $editarr[extra3_value] ?>"
									           style='width:110px' class='numeric'/> Ft
									</td>
									<td class='lablerow'><input type="text" name="extra4_name"
									                            value="<?= $editarr[extra4_name] ?>"
									                            style='width:135px'/></td>
									<td><input type="text" name="extra4_value" value="<?= $editarr[extra4_value] ?>"
									           style='width:110px' class='numeric'/> Ft
									</td>
								</tr>
								<tr>
									<td class='lablerow'><input type="text" name="extra5_name"
									                            value="<?= $editarr[extra5_name] ?>"
									                            style='width:135px'/></td>
									<td><input type="text" name="extra5_value" value="<?= $editarr[extra5_value] ?>"
									           style='width:110px' class='numeric'/> Ft
									</td>
									<td class='lablerow'><input type="text" name="extra6_name"
									                            value="<?= $editarr[extra6_name] ?>"
									                            style='width:135px'/></td>
									<td><input type="text" name="extra6_value" value="<?= $editarr[extra6_value] ?>"
									           style='width:110px' class='numeric'/> Ft
									</td>
								</tr>
								
								
								<tr>
									<td class='lablerow'>Az ár tartalmazza:</td>
									<td colspan='3'><textarea name='inprice'
									                          id='inprice'><?= $editarr[inprice] ?></textarea></td>
								</tr>
								<tr>
									<td class='lablerow'>Az ár nem tartalmazza:</td>
									<td colspan='3'><textarea name='outprice'
									                          id='outprice'><?= $editarr[outprice] ?></textarea></td>
								</tr>
								<tr>
									<td class='lablerow'>Promóció</td>
									<td colspan='3'>
										<select name='sale_category'>
											<option value=''>nincs</option>
											<option value='freeacc' <?if($editarr[sale_category] == 'freeacc') echo "selected"?>>ajándék belföld</option>
											<option value='tenp' <?if($editarr[sale_category] == '10p') echo "selected"?>>10% előleggel</option>
											<option value='twentyp' <?if($editarr[sale_category] == 'twentyp') echo "selected"?>>20% előleggel</option>
											<option value='excursion' <?if($editarr[sale_category] == 'excursion') echo "selected"?>>ajándék kirándulás</option>
											<option value='parking' <?if($editarr[sale_category] == 'parking') echo "selected"?>>ajándék parkolás</option>
											<option value='gift' <?if($editarr[sale_category] == 'gift') echo "selected"?>>ajándékcsomag</option>
											<option value='exchangerate' <?if($editarr[sale_category] == 'exchangerate') echo "selected"?>>árfolyamgarancia</option>
											<option value='taxfree' <?if($editarr[sale_category] == 'taxfree') echo "selected"?>>illetékmentes</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class='lablerow'>Elrejtés a felhasználó elöl</td>
									<td colspan='3'>
										<select name='hidden'>
											<option value='0'>nem</option>
											<option value='1' <?if($editarr[hidden] == 1) echo "selected"?>>igen</option>
										</select>
									</td>
								</tr>

								<tr>
									<td class='lablerow'>Megjegyzés:</td>
									<td colspan='3'><textarea name='comment' class='tinymce'><?= $editarr[comment] ?></textarea></td>
								</tr>
								<tr>
									<td class='lablerow' colspan='3'>Összesen:</td>
									<td class='lablerow' align='right'><?=formatPrice($editarr[total])?></td>
								</tr>
								<tr>
									<td colspan='4' align='center'><input type='submit' value='Tétel mentése'/></td>
								</tr>
							</table>
						</fieldset>
					</form>
					
					<? if($editarr[id] > 0) { ?>
					<br/><br/>
					<fieldset>
						<legend>Képek</legend>
					<form method='post' enctype="multipart/form-data">
						<input type='hidden' name='picid' value='1'/>
						<input type='hidden' name='id' value='<?= $editarr[id] ?>'/>
						<input type='hidden' name='offer_id' id='offer_id' value='<?= $offer[id] ?>'/>
							<table>
								<tr>
									<td colspan='2' id='imagenum'><? if($editarr[image_num] > 0) { echo "$editarr[image_num] db kép feltöltve"; } ?></td>
								</tr>
								<? for($i = 1; $i <= 5; $i++) { ?>
								<tr>
									<td class='lablerow'>
										<?
											if($i <= $editarr[image_num])	
											{
												echo "<img src='/images/tour_offers/$offer[id]/$editarr[id]/364_252/0$i.jpg' width='80'/>";
											}
										?>
									</td>
									<td colspan='3'>
										<input type='file' name='file<?=$i?>'/>
									</td>
								</tr>
								<? } ?>
								<tr>
									<td colspan='2'><input type='submit' value='Feltöltés'/></td>
								</tr>
							</table>
							
					</form>
					</fieldset>
					<? } ?>
				<?
				}
			?>
		
		</div>
	</div>
<?
	foot();

?>