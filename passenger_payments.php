<?
/*
 * customers.php 
 *
 * customers_tour page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();


if($CURUSER[userclass] < 50)
	header("location: index.php");


$id = (int)$_GET[id];

if($id == 0)
	header("location: passeners.php");
	
	
$passenger = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $id"));
$paidtotal = mysql_fetch_assoc($mysql->query("SELECT sum(value) as paid FROM customers_tour_payment WHERE customer_id = $id AND is_transfer = 0"));
$transfertotal = mysql_fetch_assoc($mysql->query("SELECT sum(value) as paid FROM customers_tour_payment WHERE customer_id = $id AND is_transfer = 1"));

if($_POST[company] == 'horizonttravel')
{
	$mailtemplate = 'horizonttravel';
	$sender = "Horizont Travel";
	$senderemail = "info@horizonttravel.hu";
}
else
{
	$mailtemplate = 'indulhatunk';
	$sender = "Indulhatunk.hu pénzügy";
	$senderemail = "penzugy@indulhatunk.hu";
}

$mysql->query("update customers_tour set total_paid = '$paidtotal[paid]', total_transfer = '$transfertotal[paid]' WHERE id = $id");

if($_POST[passenger_id] > 0 && $_POST[value] > 0)
{
	$seller = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $passenger[agent_id] LIMIT 1"));
	
	$data[added] = 'NOW()';
	
	$data[comment] = $_POST[comment];
	$data[customer_id] = $_POST[passenger_id];
	$data[value] = $_POST[value]*$_POST[plusminus];
	$data[payment] = $_POST[payment];
	$data[due_date] = $_POST[due_date];
	$data[is_transfer] = $_POST[is_transfer];
	$data[username] = $CURUSER[username];

	$listid = $mysql->query_insert("customers_tour_payment",$data);
	
	writelog("$CURUSER[username] added ".formatPrice($data[value])." payment for $data[customer_id]");
	
	if($data[is_transfer] == 0)
	{
		
		$body = "
			A mai napon befizetés érkezett a(z) <b>$passenger[offer_id] sz. utazáshoz</b> (<b>".formatPrice($data[value])."</b>).<br/>
			<a href='https://admin.indulhatunk.hu/passengers.php?add=1&editid=$passenger[id]'>Ellenőrizd a foglalást &raquo;</a>
			<br/><br/>
			Üdvözlettel:
			<br/><br/>
			$sender";
		
		if($seller[email] <> '')
		{
			sendEmail("Befizetés könyvelése - $passenger[offer_id]",$body,$seller[email],$passenger[name],$senderemail,$sender, $mailtemplate);
		}

	}
	if($data[is_transfer] == 0 && $data[payment] == 2)
	{
		
		$dt[comment] = "$_POST[offer_id] befizetés $_POST[comment]";
		$dt[company] = $_POST[company];
		
		$dt[username] = $CURUSER[username];
		$dt[office_id] = $CURUSER[office_id];
		$dt[voucher_id] = $_POST[offer_id];

		if($dt[username] == 'iSuperG')
			$dt[username] = 'barathneandrea';
			
		$dt[value] = $_POST[value];
	
		if($dt[value]*$_POST[plusminus] > 0)
			$dt[type] = 0;
		else
		{
			$dt[type] = 1;
			$dt[value] = $_POST[value]*$_POST[plusminus];
		}
			
		$dt[added] = 'NOW()';
		
		$dt[warrant_year] = date("Y");
	
		$warrant_id = mysql_fetch_assoc($mysql->query("SELECT max(warrant_id) as max FROM checkout_vtl WHERE company = '$dt[company]' AND warrant_year = '$dt[warrant_year]' AND type = $dt[type]"));
		
		$dt[warrant_id] = $warrant_id[max]+1;
	
	
		writelog("$CURUSER[username] added checkout_vtl item: $data[voucher_id]/$_POST[id]");
	
	
		$ckid = $mysql->query_insert("checkout_vtl",$dt);
	
		$mysql->query("UPDATE customers_tour_payment SET checkout_id = $ckid WHERE id = $listid");
			
	}
	
	if($data[payment] == 1 && $data[is_transfer] == 0 && $data[value] > 0)
	{
		
		$body = "
			A mai napon bankszámlánkra megérkezett a(z) <b>$passenger[offer_id] sz. utazás</b> részvételi díjára befizetett <b>".formatPrice($data[value])."</b>.<br/>
			A beérkezett összeget rögzítettük foglalásához. 
<br/><br/>
Üdvözlettel:
<br/><br/>
$seller[company_name]
";
		
		if($passenger[email] <> '')
			sendEmail("Befizetés könyvelése - $passenger[offer_id]",$body,$passenger[email],$passenger[name],$seller[email],$seller[company_name], $mailtemplate);
		
		
				
	}
	$msg = 'Sikeresen rögzítette a befizetést!';

	header("Location: passenger_payments.php?id=$passenger[id]");
	die;
}
head("$passenger[name] befizetései");
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
	
		<li><a href="passengers.php" >Utasok</a></li>
		<li><a href="/passengers.php?add=1&editid=<?=$id?>" ><?=$passenger[name]?> ajánlata</a></li>
		<li><a href="?id=<?=$id?>" class="current" >Pénzügyi tételek (<?=$passenger[name]?> - <?=$passenger[offer_id]?>)</a></li>
			
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<?

if($msg == '')
	$msg = "<font color='red'>FIGYELEM: A felvitt összeg automatikusan bekerül a pénztárba! Nem kell külön felvinni!<br/><br/>
	
	Az utashoz átutalást az Orsi tud rögzíteni!
	</font><br/>
	";
echo message($msg);


?>
<div style='width:45%;float:left'>
<form method='POST' action='?id=<?=$id?>'>
	<input type='hidden' name='passenger_id' value='<?=$id?>'/>
	<input type='hidden' name='offer_id' value='<?=$passenger[offer_id]?>'/>
	<input type='hidden' name='company' value='<?=$passenger[company_invoice]?>'/>

<h2>Befizetés rögzítése </h2>
	<table style='width:390px;margin:0 auto;'>
	<tr class='header'>
	<td colspan='2' align='center'>
	<?=$passenger[offer_id]?>
	</td>
	</tr>
	<!--	<tr>
			<td class='header' style='text-align:left;width:120px'>Dátum</td>
			<td> <input type='text' name='added' class='maskeddate' value='<?=date("Y-m-d")?>'/></td>
		</tr>-->
		<tr>
			<td class='header' style='text-align:left;width:120px'>Utalási határidő</td>
			<td> <input type='text' name='due_date' class='maskeddate' value=''/></td>
		</tr>
		<tr>
			<td class='header' style='text-align:left;width:120px'>Összeg</td>
			<td align='right'> <select name='plusminus'><option value='1'>+</option><option value='-1'>-</option></select> <input type='text' name='value' class='numeric'/> Ft <select name='payment'>
					<option value='2'>Készpénz</option>
					<? /*if($CURUSER[passengers_admin] == 1) { ?>
						<option value='1'>Átutalás</option>
						<option value='14'>Utalvány</option>
					<? } */?>
					<option value='1'>Átutalás</option>
					<option value='14'>Utalvány</option>
					<option value='9'>Bankkártya</option>
				</select>
</td>
		</tr>
		<tr>
			<td class='header' style='text-align:left;width:120px'>Megjegyzés</td>
			<td> <input type='text' name='comment' value=''/></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Befizet'/></td>
		</tr>
	</table>
</form>
</div>

<div style='width:45%;float:right'>

<? if($paidtotal[paid] > 0) { ?>
<form method='POST' action='?id=<?=$id?>'>
	<input type='hidden' name='passenger_id' value='<?=$id?>'/>
	<input type='hidden' name='is_transfer' value='1'/>
<h2>Partner felé történő kifizetés </h2>
	<table style='width:390px;margin:0 auto;'>
	<tr class='header'>
	<td colspan='2' align='center'>
	<?=$passenger[offer_id]?>
	</td>
	</tr>
		<tr>
			<td class='header' style='text-align:left;width:120px'>Dátum</td>
			<td> <input type='text' name='added' class='maskeddate' value='<?=date("Y-m-d")?>'/></td>
		</tr>
		<tr>
			<td class='header' style='text-align:left;width:120px'>Összeg</td>
			<td align='right'><select name='plusminus'><option value='1'>+</option><option value='-1'>-</option></select> <input type='text' name='value' class='numeric'/> Ft
			<select name='payment'>
				<? if($CURUSER[passengers_admin] == 1) { ?>
					<option value='1'>Átutalás</option>
					<option value='2'>Készpénz</option>
				<? } ?>

					<option value='9'>Bankkártya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class='header' style='text-align:left;width:120px'>Megjegyzés</td>
			<td> <input type='text' name='comment' value=''/></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Befizet'/></td>
		</tr>
	</table>
</form>


<? } ?>
</div>

<div class='cleaner'></div>
<div style='height:20px'></div>
	<h2>Részletezve: </h2>


<table style='width:100%;margin:0 auto;'>

<tr class='header'>
	<td width='90'>Dátum</td>
	<td width='90'>Felhasználó</td>
	<td width='500'>Megjegyzés</td>
	<td width='150'>Fizetés módja</td>
	<td>Összeg</td>
</tr>
<?
$query = $mysql->query("SELECT * FROM customers_tour_payment WHERE customer_id = $id ORDER BY is_transfer, added ASC");

while($arr = mysql_fetch_assoc($query))
{
	if($arr[payment] == 2)
		$payment = 'Készpénz';
	if($arr[payment] == 9)
		$payment = 'Bankkártya';
	if($arr[payment] == 1)
		$payment = 'Átutalás';
	if($arr[payment] == 14)
		$payment = 'Utalvány';
		
	if($arr[is_transfer] == 1)
	{
		$payment = "Partner felé ".strtolower($payment);
		$class = 'blue';
		$totaltransferred = $totaltransferred+$arr[value];
		
	}
	else
	{
		$class = '';
		$total = $total+$arr[value];
		
		if($arr[checkout_id] > 0)
		{
			$arr[comment] = "$arr[comment] - <a href='/vouchers/print_checkout.php?sure=1&cid=$passenger[offer_id]&id=$arr[checkout_id]&type='>[nyomtat]</a>";
		}
	}	
	
	
echo "<tr class='$class'>
	<td>$arr[added]</td>
	<td>$arr[username]</td>
	<td>$arr[comment]</td>
	<td align='center'>$payment</td>
	<td align='right'>".formatPrice($arr[value])."</td>
</tr>";


}

echo "<tr class='header'>
	<td colspan='5'>azaz:</td>
</tr>";
echo "<tr>
	<td colspan='4'>Befizetett összesen:</td>
	<td align='right'>".formatPrice($total)."</td>
</tr>";

if(($passenger[total]-$total) == 0)
	$class = 'green';
else
	$class = 'red';
	
if($passenger['yield'] == 0)
	$yieldclass = 'red';


echo "<tr>
	<td colspan='4'>Utas hátralék:</td>
	<td align='right'><font color='$class'><b>".formatPrice($passenger[total]-$total-$passenger[voucher_value],0,1)."</b></font></td>
</tr>";

echo "<tr class='header'>
	<td colspan='5'>Partner felé történt utalások</td>
</tr>";
echo "<tr>
	<td colspan='4'>Elutalt összesen:</td>
	<td align='right'>".formatPrice($totaltransferred,0,1)."</td>
</tr>";
echo "<tr>
	<td colspan='4'>Még utalandó (<font color='$yieldclass'>jutalék: ".formatPrice($passenger['yield'],0,1)."</font>):</td>
	<td align='right'>".formatPrice($passenger[final_total] - $passenger['yield'] - $totaltransferred,0,1)."</td>
</tr>";
?>


</table>
</div></div>
<?
foot();
?>