<?
/*
 * customers.php 
 *
 * customers_tour page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
userlogin();


if($CURUSER[userclass] < 50)
	header("location: index.php");

	
if($_GET[from_date] == '')
	$_GET[from_date] = date("Y-m-01");
	

if($_GET[to_date] == '')
	$_GET[to_date] = date("Y-m-d",strtotime("+1 day"));
	
	if($_POST[giveme] == 1)
	{
		$ck = mysql_fetch_assoc($mysql->query("SELECT id, offer_id from customers_tour WHERE agent_id = 0 AND inactive = 0 ORDER BY id ASC LIMIT 1"));
		
		if($ck[id] == '')
			die("Időközben valaki már elvitte ezt azt utast :(");
		$mysql->query("UPDATE partners SET last_offer_request = NOW() WHERE pid = $CURUSER[pid]");
		$mysql->query("UPDATE customers_tour SET agent_id =  $CURUSER[pid] WHERE id = $ck[id]");
		writelog("$CURUSER[username] requested IND_ID: $ck[offer_id]");
		header("location: /passengers.php?add=1&editid=$ck[id]");
		die;
	}

	if($CURUSER[passengers_admin] == 1)
		$eoffice = '';
//	elseif($CURUSER[office_id] == 2499)
//		$eoffice = " (partners.office_id = '$CURUSER[office_id]' OR customers_tour.agent_id = 0) AND ";
	else
		$eoffice = " partners.office_id = '$CURUSER[office_id]' AND ";



$delreasons = array(
	0 => "",
	1 => "egyéb, megjegyzésben részletezem",
	2 => "nincs hely",
	3 => "máshol foglalt",
	4 => "nem utazik",
	5 => "egyénileg utazik",
	6 => "hiba az árban",
	7 => "nem reális kérés",
	8 => "nem reagált",
	9 => "azonos megrendelés", //nem hasznalatosI
	201 => "azonos megrendelés",
	200 => "emlékeztess később",
);
	

	
$status = (int)$_GET[status];


if($_GET[copy] == 1 && $_GET[sure] <> 1) {
	die("
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan másolja a tételt</h2> <br/><a href=\"?copy=1&id=$_GET[id]&sure=1\" class='button green'>Igen</a> 
	<a href=\"passengers.php\" class='button red closefacebox'>$lang[no]!</a><div class='cleaner'></div>");
}



if($_GET[copy] == 1 && $_GET[id] > 0 &&  $_GET[sure] == 1)
{
	$pass = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = '$_GET[id]' LIMIT 1"));
	
		$origid = $pass[offer_id];
		$pass[offer_id] = generateNumber('tour');
		$pass[added] = 'NOW()';
		$pass[status] = '';
		$pass[prepaid] = '';
		$pass['yield'] = '';
		$pass[position_number] = '';
		$pass[invoice_number] = '';
		$pass[invoice_date] = '';
		$pass[insurance_number] = '';
		$pass[own_storno_number] = '';
		$pass[due_date] = '';
		$pass[serial] = '';
		$pass[interspire] = '';
		$pass[voucher_id] = '';
		$pass[voucher_value] = '';
		$pass[donotwant] = '';
		$pass[inactive] = '';
		$pass[final_total] = '';
		$pass[confirmation_date] = '';
		$pass[inactive_date] = '';
		$pass[total_income] = '';
		$pass[total_income_date] = '';
		$pass[notification_count] = '';
		$pass[notification_date] = '';
		$pass[partner_deposit] = '';
		$pass[has_file] = '';
		$pass[file_date] = '';
		$pass[checked] = '';
		$pass[last_checked_date] = '';
		$pass[warning] = '';
		$pass[partner_deposit_due] = '';
		$pass[partner_deposit_sent] = '';
		$pass[comment] = '';

		

		unset($pass[id]);

		writelog("$CURUSER[username] copied tour $origid > $pass[offer_id]");
		
		$id = $mysql->query_insert("customers_tour",$pass);
		
		header("location: passengers.php?add=1&editid=$id");
		die;
}


//search 
$s = trim($_POST[search]);

if(strlen($s) < 3)
	$s = '';
	
if($s <> '')
	setcookie ("search", $s, time() + 3600);


if($_POST[addevent] == 1)
{
	$_POST[added] = 'NOW()';
	$_POST[agent_id] = $CURUSER[pid];
	unset($_POST[addevent]);
	$_POST[due_date] = "$_POST[due_date] $_POST[due_time]";
	unset($_POST[due_time]);
	
	$mysql->query_insert("customers_tour_notifications",$_POST);
	header("location: /passengers.php?add=1&editid=$_POST[customer_id]#upload");
	print_r($_POST);
	die;	
	
}
elseif($_POST[id] > 0 && $_POST[upload] == 1)
{
		$postid = (int)$_POST[id];
 		if($_FILES["file"]["name"] <> ''  && $_FILES["file"]['size'] < 2* 1024 * 1024)
		{	
			
			$folder = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/passengers/$postid";
			
			$ext = end(explode(".",$_FILES["file"]["name"]));
			
			$filename = "$_POST[category]-".date("ymdhis").".$ext";
			
			$msg = "A file sikeresen feltöltve";
			makedir($folder);
     
	      if(move_uploaded_file($_FILES["file"]["tmp_name"],"$folder/$filename"))
		  {
			  writelog("$CURUSER[username] uploaded $_POST[category] IND_ID: $_POST[offer_id]");
			   $mysql->query("UPDATE customers_tour SET has_file = 1, file_date = NOW() WHERE id = $postid");
		  }
	      else
	      	$msg.= "<font color='red'>HIBA: A file feltöltése sikertelen!</font>";
	      	
		  }
		  elseif($_FILES["file"]["name"] <> '')
		  {
	      		$msg.= "<font color='red'>HIBA: A file mérete túl nagy, max 2MB!</font>";
	      }
		  
		  header("location: /passengers.php?add=1&editid=$postid#upload");
		  die;

		 
}
elseif($_POST[id] > 0)
{
	$check = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $_POST[id] LIMIT 1"));

	$data = array();
	
	$data[status] = $_POST[status];
	$data[paid] = $_POST[paid];
	$data[last_status_change] = 'NOW()';
	if($data[status] > 3 && $check[confirmation_date] == '0000-00-00 00:00:00')
	{	
		//	debug($check);
		$data[confirmation_date] = 'NOW()';
	}	
	$mysql->query_update("customers_tour",$data,"id=$_POST[id]");
	writelog("$CURUSER[username] updated status = $_POST[status]  IND_ID: $check[offer_id]");
	
	if($data[status] == 4)
	{
		if($check[tour_category] == 'package' || $check[tour_category] == 'hotel'  || $check[tour_category] == 'hotel_hu' )
		{
			
			$agent = mysql_fetch_assoc($mysql->query("SELECT * from partners WHERE pid = $check[agent_id] LIMIT 1"));
		
			$body = "
	Ezúton tájékoztatom, hogy a(z) $check[offer_id] számú utazásuk visszaigazolásra került, így érvénybe lépett Utazási Szerződésük.<br/><br/>
	
	Indulás előtt néhány nappal fogjuk küldeni Önnek e-mailen az utazásukhoz szükséges dokumentumokat. Amennyiben személyesen szeretnék megkapni, kérem mielőbb jelezze nekem.<br/><br/>
	
	
	
	Kellemes utazást kívánok!<br/><br/>
	
			Üdvözlettel:<br/><br/>
			
			<b>$agent[company_name]</b><br/><br/>
			Utazási szakértő | Indulhatunk.hu Nemzetközi Utazási Irodák<br/><br/>
			
			Közvetlen e-mail: $agent[email]<br/>
			Közvetlen telefonszám: $agent[phone]";
			if($check[email] <> '')
				sendEmail("Indulhatunk.hu visszaigazolás - $check[offer_id]",$body,$check[email],$check[name],$agent[email],$agent[company_name], 'indulhatunk');
			
			$sms = "Kedves Utasunk!\nUtazásuk visszaigazolásra került. Indulás előtt néhány nappal küldjük az utazáshoz szükséges dokumentumokat. Üdvözlettel: Indulhatunk.hu";
			sendsms($sms,$check[phone]);
	
		}
				
	}

//	die;
}
$cookie = $_COOKIE[tour_search];
$editid = $_GET[editid];
$delete = $_GET[delete];
$sure = $_GET[sure];
$editIDPost = $_POST[editid];


if($_POST[own_storno] == 'on')
	$_POST[own_storno] = 1;
else
	$_POST[own_storno] = 0;
	
//delete, copy
if($delete > 0 && $sure <> 1) {
	die("
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>$lang[delete_item]</h2> <br/>

			<a href=\"?delete=$delete&sure=1&donotwant=2\" class='button green'>nincs hely</a>
				<a href=\"?delete=$delete&sure=1&donotwant=3\" class='button green'>máshol foglalt</a>
				<a href=\"?delete=$delete&sure=1&donotwant=4\" class='button green'>nem utazik</a>
				<a href=\"?delete=$delete&sure=1&donotwant=5\" class='button green'>egyénileg utazik</a>
				<a href=\"?delete=$delete&sure=1&donotwant=6\" class='button green'>hiba az árban</a>
				<a href=\"?delete=$delete&sure=1&donotwant=7\" class='button green'>nem reális kérés</a>
				<a href=\"?delete=$delete&sure=1&donotwant=8\" class='button green'>nem reagált</a>
				<a href=\"?delete=$delete&sure=1&donotwant=1\" class='button green'>egyéb, megjegyzésben részletezem</a>        
              	<a href=\"?delete=$delete&sure=1&donotwant=201\" class='button green'>azonos megrendelés</a>        
               	<a href=\"?delete=$delete&sure=1&donotwant=200\" class='button green'>emlékeztess később</a>        

                    
		<!--<a href=\"?delete=$delete&sure=1\" class='button green'>Törlés!</a>-->  <a href=\"customers.php\" class='button red closefacebox'>$lang[no]!</a><div class='cleaner'></div>");
}

if($delete > 0 && $sure == 1) {
	
	$check = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $delete LIMIT 1"));


	
	if($_GET[donotwant] > 0)
		$donotwant = ", donotwant = $_GET[donotwant]";
		
	$mysql->query("update customers_tour SET inactive = 1, inactive_date = NOW() $donotwant where id = $delete $extraSelect") or die(mysql_error());
	writelog("$CURUSER[username] deleted IND_ID: $check[offer_id]");
	head($lang[manage_customers]);
	echo message("<a href='customers.php'>$lang[deleted]</a> <br/> <a href='passengers.php' class='button grey' style='margin:10px 0 0 115px;'>Vissza!</a><div class='cleaner'></div>");
	foot();
	die();
}


if($_POST[name] <> "") {

	unset($_POST[editid]);
	
	$base = $_POST[base_price]*$_POST[adults]+$_POST[child_value]*$_POST[child_number];
	
	$persons = $_POST[adults]+$_POST[child_number];
	
	
	if($_POST[storno_insurance_base] == 0)
	{
		$storno = $base*($_POST[storno_insurance]/100);
		$_POST[storno_insurance_base] = $base;
	}
	else
		$storno = $_POST[storno_insurance_base]*($_POST[storno_insurance]/100);
		
		
	if($_POST[own_storno] == 1)
		$_POST[insurance_yield_value] = round(($_POST[insurance_value]*0.31+$storno*0.2));
	else
		$_POST[insurance_yield_value] = round($_POST[insurance_value]*0.31);

	
	$_POST[total] =
				$base + //base price
				$storno + //storno insurance
				$_POST[insurance_value] + //general insurance
				
				//special fees
				 ($_POST[airport_fee] + $_POST[visa_fee] + $_POST[reservation_fee] + $_POST[kerosene_fee] + $_POST[service_fee] + $_POST[transfer_fee]  + $_POST[local_fee])*$persons +
				//extra fees
				($_POST[extra1_value] + $_POST[extra2_value] + $_POST[extra3_value] + $_POST[extra4_value] + $_POST[extra5_value] + $_POST[extra6_value]);
				
			/*	echo "$base + //base price
				$base*($_POST[storno_insurance]/100) + //storno insurance
				$_POST[insurance_value] + //general insurance
				
				//special fees
				 ($_POST[airport_fee] + $_POST[visa_fee] + $_POST[reservation_fee] + $_POST[kerosene_fee] + $_POST[service_fee] + $_POST[transfer_fee])*$persons +
				//extra fees
				($_POST[extra1_value] + $_POST[extra2_value] + $_POST[extra3_value] + $_POST[extra4_value] + $_POST[extra5_value] + $_POST[extra6_value])";
				*/
		
	
	$_POST[total] = round_five($_POST[total]);
	
	if($_POST[from_date] == '' || $_POST[from_date] == '0000-00-00')
		$_POST[to_date] = date("Y-m-d",strtotime(date("Y-m-d", strtotime($_POST[from_date])) . " +$_POST[nights] day"));
	
	if($_POST[is_option] <> 1)
		$_POST[is_option] = 0;
		
	if($editIDPost > 0)
	{
		$_POST[serial] = $_POST[serial]+1;
			
		
		$check = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $editIDPost LIMIT 1"));
		
		if($check[total] <> $_POST[total] && $check[checked] == 1)
		{
			$_POST[warning] = 1;
			writelog("$CURUSER[username] edited price after check $check[total] > $_POST[total] IND_ID: $_POST[offer_id]");
			sendEmail("indulhatunk.hu ellenőrzés utáni ár módosítás - $_POST[offer_id] $CURUSER[username] által","A fenti ajánlat árát módosították","orsolya.finta@indulhatunk.hu","Ügyintéző");

		}
		
		if($_POST[comment] <> '')
		{
			$comm = date("Y-m-d H:i")." - $CURUSER[username] - $_POST[comment]";
			$_POST[comment] = "$comm\n$check[comment]";
		}
		else
			unset($_POST[comment]);
			
	  	$mysql->query_update("customers_tour",$_POST,"id=$editIDPost");
		$msg = "Sikeresen szerkesztette az utast!";
		writelog("$CURUSER[username] edited passenger IND_ID: $_POST[offer_id]");

	  

	}
	else
	{
		
		$_POST[offer_id] = generateNumber('tour');
		$_POST[added] = 'NOW()';
		//$_POST[source] = 'personal';

		$mysql->query_insert("customers_tour",$_POST);
		$msg = "Sikeresen létrehozta az utast!";
		
		
			//sendEmail("indulhatunk.hu utazási ajánlatkérés - $data[offer_id]",$body,"indulhatunk@indulhatunk.hu",$data[name]);
			//sendEmail("indulhatunk.hu utazási ajánlatkérés - $_POST[offer_id]",$body,"utak@indulhatunk.hu",$data[name]);

	//	sendEmail("indulhatunk.hu utazási vásárló felvitele - $_POST[offer_id]",$_POST[name],"mail@isuperg.com",$_POST[name]);
		
		
		writelog("$CURUSER[username] added passenger IND_ID: $_POST[offer_id]");
		header("location:passengers.php");
	}

}

if($editid >0) {
	$customerQuery = $mysql->query("SELECT * FROM customers_tour WHERE id = $editid $extraSelect");
	$editarr = mysql_fetch_assoc($customerQuery);
}

if($editarr[id] > 0)
		head("$editarr[name] - $editarr[offer_id]");
else
	head("Utasok kezelése - <a href='/events.php'>[Teendők]</a>");
	

$notification = mysql_fetch_assoc($mysql->query("SELECT * FROM information WHERE id = 19 LIMIT 1"));

if($notification[body] <> '')
	echo "<div class='redmessage'><a href='#'>".nl2br(strip_tags($notification[body]))."</a></div>";

?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
	
		<li><a href="?add=1">Új</a></li>
		<li><a href="?status=0" class="<? if($status == 0 && $_GET[showpaid] == 0 && $_GET[mybookings] <> 1) echo 'current'?>" >Ajánlat</a></li>
		<li><a href="?status=2" class="<? if($status == 2 && $_GET[mybookings] <> 1) echo 'current'?>">Megrendelő</a></li>
		<li><a href="?status=3" class="<? if($status == 3 && $_GET[mybookings] <> 1) echo 'current'?>">VIG vár</a></li>
		<li><a href="?status=4" class="<? if($status == 4 && $_GET[mybookings] <> 1) echo 'current'?>">VIG</a></li>
		<li><a href="?status=5" class="<? if($status == 5 && $_GET[mybookings] <> 1) echo 'current'?>">Voucher</a></li>
		<li><a href="?showpaid=1" class="<? if($_GET[showpaid] == 1) echo 'current'?>">Befizetett</a></li>
		<li><a href="bankdispatch.php">Bank</a></li>
		<? if($CURUSER[passengers_admin] == 1) { ?>
			<li><a href="?inactive=1">Törölt</a></li>
			<li><a href="passengers_check.php">Check</a></li>
			<li><a href="passengers_transfer.php">Utalás</a></li>
			<li><a href="tour_discount.php">Utalv.</a></li>
			<li><a href="touroffers.php">Utak</a></li>
		<? } ?>
		<? if($CURUSER[userclass] >= 255 || $CURUSER[username] == 'csongor' || $CURUSER[username] == 'matyas') { ?>
			<li><a href="?showstats=1" class="<? if($_GET[showstats] == 1) echo 'current'?>">Stat</a></li>
			
		<? } ?>
			<li><a href="?mybookings=1" class="<? if($_GET[mybookings] == 1) echo 'current'?>"><b>Foglalásaim</b></a></li>

		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<div class='clear'></div>

<fieldset class='searchform'>
<form method="post" id="sform" action="passengers.php">
<input type="text" name="search"  value="<?=$cookie?>" placeholder='Keresés...'/><a href="#" class='searchbutton' id='submit'>Ok</a>
<? 
if($_POST[showinactive] == 'on')
{
	$showchecked = "checked";
}
 ?>

<?
	if($CURUSER[passengers_admin] == 1)
	{
		echo "<input type='text' name='fdate' class='dpick'  value='$_POST[fdate]' style='width:90px;' placeholder='Indulás -tól'/><input type='text' name='tdate' class='dpick'  value='$_POST[tdate]' style='width:90px;'  placeholder='Indulás -ig'/>";
	}	
?>
<br/>
<div style='margin:0px 0 0 0'>
	<label for="showinactive"><input type='checkbox' name='showinactive' id="showinactive" <?=$showchecked?>/> Töröltek között </label>
</div>
</form>
</fieldset>
<?


if($msg <> '')
	echo message("$msg");
	
if($_GET[add] == 1) {

	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE coredb_id = '$editarr[partner_id]'"));
	$string = generateNumber('tour');
?>

<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		
		function deposit_recalculate()
		{
			var percent = $("#partner_deposit_percent").val()*1;
			var final_total = $("#final_total").val()*1;
			var partner_deposit = $("#partner_deposit").val()*1;

			if(percent > 100)
				$("#partner_deposit_percent").val(100);
				
			if(partner_deposit > final_total)
				$("#partner_deposit").val(final_total);
				
			
			if(final_total > 0)
			{
				if(percent == 0)
					$("#partner_deposit_percent").val(Math.round(partner_deposit/final_total*100));
				else
					$("#partner_deposit").val(Math.round(final_total*percent/100));
			}
			else
			{
				//alert('Töltse ki a visszaigazolt összeget');
			}
			
		}
		//pdeppercent
		$("#partner_deposit_percent").on('input', function() {
			
			deposit_recalculate();
		});
		
		$("#partner_deposit").on('input', function() {
			$("#partner_deposit_percent").val('');
			deposit_recalculate();
		});
		
		deposit_recalculate();

		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
				plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,|,undo,redo,|,bullist,numlist,|",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
extended_valid_elements: "iframe[class|src|frameborder=0|alt|title|width|height|align|name]",
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		


	 $('#psubmit').click(function()
 	 {
	 	 var error = 0;
	 	 if($("#pname").val() == '')
	 	 {
		 	 error = 1;
		 	 $("#pname").addClass('invalid');
	 	 }
	 	 
	 	 if($("#offer_name").val() == '')
	 	 {
		 	 error = 1;
		 	 $("#offer_name").addClass('invalid');
	 	 }
	 	 
	 	 if(error == 0)
	 	 	return true;
	 	 else
	 	 {
		 	 alert("A név és az új neve mező kitöltése kötelező!");
		 	 return false;
	 	 }
	});
	
	});
</script>


<div>

	<script>
	$(document).ready(function() {

	<? if($editarr[id] > 0) { ?>

	$('#logdata').load('/log.php?showpassenger=<?=$editarr[offer_id]?>&id=<?=$editarr[id]?>');
	<? } ?>
	
	var a = $('#offer_name').autocomplete({
	serviceUrl:'info/showtour.php',
	minChars:2,
	delimiter: /(,|;)\s*/, // regex or character
	maxHeight:400,
	width:500,
	zIndex: 9999,
	deferRequestBy: 0, //miliseconds
	params: { country:'Yes' }, //aditional parameters
	noCache: true, //default is false, set to true to disable caching
	// callback function:
	onSelect: function(value, data){ 
		//console.log(value+" >"+data);	
		$("#board").val(value.data.board);
		$("#travel").val(value.data.travel);
		$("#package_id").val(value.data.id);
		$("#destination").val(value.data.destination);
		$("#inprice").val(value.data.inprice);
		$("#outprice").val(value.data.outprice);
		
		$("#partner_id").val(value.data.partner_id);
		$("#offerlink").html("<a href='"+value.data.page_url+"' target='_blank'>[link]</a>");
		$("#offer_name").val(value.data.offer_name);
	},
	// local autosugest options:
	//lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
});


var a = $('#customeremail').autocomplete({
	serviceUrl:'info/showpassenger.php',
	minChars:5,
	delimiter: /(,|;)\s*/, // regex or character
	maxHeight:400,
	width:500,
	zIndex: 9999,
	deferRequestBy: 0, //miliseconds
	params: {  }, //aditional parameters
	noCache: true, //default is false, set to true to disable caching
	// callback function:
	onSelect: function(value, data){ 
		//console.log(value+" >"+data);	
		$("#pname").val(value.data.name);
		$("#pzip").val(value.data.zip);
		$("#pcity").val(value.data.city);
		$("#paddress").val(value.data.address);
		$("#customerphone").val(value.data.phone);
		$("#customeremail").val(value.data.email);
		
		if(value.data.invoice_name != '')
		{
			$("#invdata").show();
			$("#invoice_name").val(value.data.invoice_name);
			$("#invoice_zip").val(value.data.invoice_zip);
			$("#invoice_city").val(value.data.invoice_city);
			$("#invoice_address").val(value.data.invoice_address);


		}
	},
	// local autosugest options:
	//lookup: ['January', 'February', 'March', 'April', 'May'] //local lookup values
});





	});
	
	
	</script>

	
	
		<?if($editarr[offer_id] =="") {
			$code = $string;
		}	
		else {
			
			$code = "$editarr[offer_id]/$editarr[serial]";
		}?>
		
		
	<form method="POST"  enctype="multipart/form-data">
	
	
			<input type="hidden" name="editid" value="<?=$editarr[id]?>">
			<input type="hidden" name="offer_id" id='offer_id' value="<?=$editarr[offer_id]?>">
			<input type="hidden" name="package_id" id='package_id' value="<?=$editarr[package_id]?>">
			<input type="hidden" name="serial" value="<?=$editarr[serial]?>">

	
		
		
	<fieldset style='width:855px'>
	<legend><?=$code?> <? if($editarr[id] > 0) { ?>- <a href='/passenger_offer.php?id=<?=$editarr[id]?>'>[ajánlat készítése]</a>  - <a href='/vouchers/print_order.php?id=<?=$editarr[id]?>&print=1'>[utazási szerződés]</a> <a href='/vouchers/print_order.php?id=<?=$editarr[id]?>&print=1&order=1'>[partnermegrendelő]</a> <a href='/passenger_payments.php?id=<?=$editarr[id]?>'>[befizetések]</a> <a href='/vouchers/print_order.php?id=<?=$editarr[id]?>&print=1&delete=1'>[lemondás]</a> <a href='/customers/terminal?tour=1&passenger=<?=$editarr[id]?>&hash=<?=md5($editarr[id])?>'>[fizetés]</a>
	<a href='/info/notify-passenger.php?id=<?=$editarr[id]?>' rel='facebox'>[hátralékértesítő]</a>
	<?	if($editarr[status] >= 4) { ?>
		<a href='/vouchers/print_ticket.php?id=<?=$editarr[id]?>&print=1'>[voucher]</a>
	<? } ?>
	<? } ?></legend>
	<table width='100%'>
		<?
		
		$statuses = array("Ajánlat",'Lemondás','Megrendelőre vár','Visszaigazolásra vár','Visszaigazolva','Voucher kiküldve');

	if($editarr[inactive] == 1)
		$color = 'red';
	elseif($editarr[status] == 1)
		$color= 'lorange';
	elseif($editarr[status] == 2)
		$color= 'blue';
	elseif($editarr[status] == 3)
		$color= 'dpink';
	elseif($editarr[status] == 4)
		$color= 'green';
	elseif($editarr[status] == 5)
		$color= 'orange';
	else
		$color = '';





		?>
		
			
			<?
			
				$isduplicate = $mysql->query("SELECT * FROM customers_tour WHERE email = '$editarr[email]' AND email <> '' ORDER BY id DESC");
				
				if(mysql_num_rows($isduplicate) > 1)
				{
					$g = 1;
					while($c = mysql_fetch_assoc($isduplicate))
					{
						$ag = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $c[agent_id] limit 1"));
						$dt = explode(" ",$c[added]);
						
						if($c[id] == $editarr[id])
							$cvalue = '►';
						else
							$cvalue = '';
							
						if($c[inactive] == 1)
							$cstatus = 'red';
						else
							$cstatus = '';
							
						$prevlist.= "<tr class='$cstatus'><td width='10' align='center'>$cvalue</td><td width='20'><img src='/images/icons/$c[source].png' width='20' alt='$c[source]' title='$c[source]'/></td><td width='10' align='center'>$g</td><td width='80' align='center'>$dt[0]</td><td width='90'>$ag[username]</td><td>$c[offer_name]</td><td align='right'>".formatPrice($c[total])."</td><td>".$statuses[$c[status]]."</td><td align='center'><a href='/passengers.php?add=1&editid=$c[id]' target='_blank'>&raquo;</a></td></tr>";
						
						$g++;
						
					}
					echo "<tr>";
						echo "<td colspan='4'>
						<div style='height:100px;overflow:scroll;'><table>$prevlist</table></div>
						</td>";
					echo "</tr>";
				}
					
			?>
			<tr>
				<td colspan='4' align='center' class='<?=$color?>' style='text-transform:uppercase;font-weight:bold;'>
				<? if($editarr[inactive] == 1)  {
					
					$deltime = mysql_fetch_assoc($mysql->query("SELECT * FROM log WHERE event like '%deleted IND_ID: $editarr[offer_id]%' LIMIT 1"));
					
					$event = explode(" ",$deltime[event]);	
					echo "TÖRÖLT: ".$delreasons[$editarr[donotwant]]." - ($deltime[date] $event[0])<br/>";
				} ?>
				
				
		<?=$statuses[$editarr[status]]?> <? if($editarr[added] <> '') echo "(beérkezett: $editarr[added] - ".time_ago($editarr[added]).")";?>
	

		</td>
			</tr>
			
			<tr>
				<td class='lablerow'>Értékesítő cég:</td>
				<td>
					<select name='company_invoice'>
					<option value='szallasoutlet' <? if($editarr[company_invoice] == 'szallasoutlet' || $editarr[company_invoice] == '') echo "selected";?>>Szállás Outlet Kft.</option>
					<? if($editarr[company_invoice] == 'hoteloutlet') { ?>
						 <option value='hoteloutlet' <? if($editarr[company_invoice] == 'hoteloutlet' ) echo "selected";?>>Hotel Outlet Kft.</option>
					 <? } ?>
					</select>
				</td>
				
				<td class='lablerow'>Értékesítő:</td>
				<td>
					<select name='agent_id'>
					<option value='0'>Válasszon</option>
					<?
			$partnerQuery = $mysql->query("SELECT * FROM partners WHERE passengers = 1 ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($editarr[agent_id] == $partnerArr[pid])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
				</td>


				</tr>			
				<tr>
				
				<td class='lablerow'>Kategória:</td>
				<td>
					<select name='tour_category'>
					<option value='package' <? if($editarr[tour_category] == 'package') echo 'selected'?>>csomag</option>
					<option value='insurance' <? if($editarr[tour_category] == 'insurance') echo 'selected'?>>biztosítás</option>
					<option value='plane' <? if($editarr[tour_category] == 'plane') echo 'selected'?>>repülőjegy</option>
					<option value='hotel' <? if($editarr[tour_category] == 'hotel') echo 'selected'?>>csak szállás</option>
					<option value='hotel_hu' <? if($editarr[tour_category] == 'hotel_hu') echo 'selected'?>>csak szállás belföld</option>
					<option value='transfer_foreign' <? if($editarr[tour_category] == 'transfer_foreign') echo 'selected'?>>transzfer külföld</option>
					<option value='transfer_local' <? if($editarr[tour_category] == 'transfer_local') echo 'selected'?>>transzfer belföld</option>
					<option value='parking' <? if($editarr[tour_category] == 'parking') echo 'selected'?>>reptéri parkolás</option>
					<option value='car' <? if($editarr[tour_category] == 'car') echo 'selected'?>>autóbérlés</option>
					<option value='voucher' <? if($editarr[tour_category] == 'voucher') echo 'selected'?>>utalvány</option>
					<option value='visa' <? if($editarr[tour_category] == 'visa') echo 'selected'?>>vízum</option>
					<option value='other' <? if($editarr[tour_category] == 'other') echo 'selected'?>>egyéb</option>
					</select>
				</td>

			
					<td class='lablerow'>Honnan érkezett:</td>
				<td>
					<select name='source'>
					<option value='web' <? if($editarr[source] == 'web') echo 'selected'?>>weboldal</option>
					<option value='phone' <? if($editarr[source] == 'phone') echo 'selected'?>>telefon</option>
					<option value='chat' <? if($editarr[source] == 'chat') echo 'selected'?>>chat</option>
					<option value='email' <? if($editarr[source] == 'email') echo 'selected'?>>email</option>
					<option value='question' <? if($editarr[source] == 'question') echo 'selected'?>>kérdés</option>
					<option value='personal' <? if($editarr[source] == 'personal') echo 'selected'?>>személyes</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td class='lablerow'>Utazásszervező</td>
				<td  colspan='3'>
				
				
			<select name="partner_id" id='partner_id' style="width:655px">
			<option value="0">Válasszon</option>
	<?
			$partnerQuery = $mysql->query("SELECT * FROM partners WHERE userclass = 3 AND contact <> 'Modok Angéla'  AND coredb_id > 0 ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($editarr[partner_id] == $partnerArr[coredb_id])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[coredb_id]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
			</select>
			</td>
			</tr>
			<tr>
				<td class='lablerow'>Út neve*</td>
				<td  colspan='3'>
					<input type='text' name='offer_name' id='offer_name' style='width:640px;' value='<?=$editarr[offer_name]?>'/> <span id='offerlink'><? if($editarr[offer_url] <> '') { ?><a href='<?=$editarr[offer_url]?> ' target='_blank'><b>[link]</b></a> <? } ?></span>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Desztináció</td>
				<td  colspan='3'>
					<input type='text' name='destination' id='destination' style='width:640px;' value='<?=$editarr[destination]?>'/>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Ellátás</td>
				<td>
					<input type="text" name="board" id='board' style='width:200px;'value="<?=$editarr[board]?>"/>
				</td>
					<td class='lablerow'>Utazás módja</td>
				<td>
					<input type="text" name="travel" id='travel' style='width:200px;' value="<?=$editarr[travel]?>"/>
				</td>
			</tr>		
			<tr>
				<td class='lablerow'>E-mail cím*:</td>
				<td><input type="text" name="email" value="<?=$editarr[email]?>" id='customeremail'/></td>
				<td class='lablerow'>Telefonszám*:</td>
				<td><input type="text" name="phone" value="<?=$editarr[phone]?>" id='customerphone'/></td>
			
			</tr>
			<tr>
				<td class='lablerow'>Foglalási név, cím:*</td>
				<td  colspan='3'><input type="text" name="name" id='pname' value="<?=$editarr[name]?>" placeholder='Név'/><input type="text" name="zip" value="<?=$editarr[zip]?>" style='width:35px;' class='numeric'  placeholder='irsz.' id='pzip'/><input type="text" name="city" id='pcity' style='width:140px;margin:0 1px 0 1px' value="<?=$editarr[city]?>"  placeholder='város' /><input type="text" name="address" id='paddress' class="small" style='width:255px;' value="<?=$editarr[address]?>"  placeholder='cím'/></td>
			</tr>
		
		
		<tr id='invoicebutton' ><td colspan='4' <? if($editarr[invoice_name] <> '') echo "style='display:none'"?>><div style='text-align:center; padding:5px; '><a href='#' id='showinvdata'><b>Eltérő számlázási adatok</b></a></div></td></tr>
		
		<tr class='invdata' <? if($editarr[invoice_name] == '') echo "style='display:none'"?>><td class='lablerow'>Számlázási név, cím:</td>
				<td  colspan='3'><input type="text" name="invoice_name"  id="invoice_name" value="<?=$editarr[invoice_name]?>"/><input type="text" id='invoice_zip' name="invoice_zip" value="<?=$editarr[invoice_zip]?>" style='width:35px;'/><input type="text" style='width:140px;margin:0 1px 0 1px' class="small1"  id='invoice_city' value="<?=$editarr[invoice_city]?>" name="invoice_city"/><input type="text" id='invoice_address' name="invoice_address" class="small" style='width:255px;' value="<?=$editarr[invoice_address]?>" /></td>
			</tr>
		
		
	
	<tr>
		<td class='lablerow' colspan='4' style='padding:10px 10px 10px 5px;'>
			Utasok adatai:
		</td>
	</tr>
	
	<? for($i=1;$i<=10;$i++) { ?>
	
		<tr>
			<td class='lablerow'><?=$i?>. utas adatai:</td>
			<td><input type="text" placeholder='utas neve' name="passenger<?=$i?>_name" value="<?=$editarr["passenger".$i."_name"]?>" style='width:245px'/></td>
			<td><input type="text" name="passenger<?=$i?>_birth" value="<? if($editarr["passenger".$i."_birth"] <> '0000-00-00') echo $editarr["passenger".$i."_birth"];?>" style='width:155px;text-align:center;' class='simpledate'/></td>
			<td><input type="text" placeholder='útiokmány száma' name="passenger<?=$i?>_passport" value="<?=$editarr["passenger".$i."_passport"]?>"  style='width:155px'/></td>		
		</tr>
	<? } ?>
	
	<tr>
		<td class='lablerow' colspan='4' style='padding:10px 10px 10px 5px;'>
			 Árkalkuláció:
		</td>
	</tr>
	
	
		<tr>
			<td  class='lablerow'>Éjszakák száma:</td>
			<td><input type="text" name="nights" value="<?=$editarr[nights]?>"  style='width:110px' class='numeric'/> éj</td>		
			<td class='lablerow'>Napok száma:</td>
			<td><input type="text" name="days" value="<?=$editarr[days]?>" style='width:110px' class='numeric'/> nap</td>
		</tr>
		<tr>
			<td class='lablerow'>Indulás dátuma:</td>
			<td><input type="text" name="from_date" value="<?=$editarr[from_date]?>" style='width:110px;text-align:right;' class='simpledate'/></td>
			<td  class='lablerow'>Hazaérkezés dátuma:</td>
			<td><input type="text" name='to_date' value="<?=$editarr[to_date]?>" style='width:110px;text-align:right;'  class='simpledate'/></td>		
		</tr>
		
		<tr>
		<td class='lablerow'><input type="text" name="base_price_name" value="<?=$editarr[base_price_name]?>" style='width:135px'/></td>
			<td><input type="text" name="base_price" value="<?=$editarr[base_price]?>" style='width:90px' class='numeric '/> Ft * <input type="text" name="adults" value="<?=$editarr[adults]?>" style='width:15px' class='numeric'/> fő</td>
			<td  class='lablerow'>Pótágy:</td>
			<td><input type="text" name="child_value" value="<?=$editarr[child_value]?>"  style='width:80px' class='numeric'/> Ft * <input type="text" name="child_number" value="<?=$editarr[child_number]?>" style='width:15px' class='numeric'/> fő</td>		
		</tr>
		
		<tr>
		<td  class='lablerow'>Reptéri illeték:</td>
			<td><input type="text" name="airport_fee" value="<?=$editarr[airport_fee]?>"  style='width:110px' class='numeric'/> Ft/fő</td>		
		
			<td class='lablerow'>Vízum:</td>
			<td><input type="text" name="visa_fee" value="<?=$editarr[visa_fee]?>" style='width:110px' class='numeric'/> Ft/fő</td>
			</tr>
	
		
		<tr>
			<td class='lablerow'>Foglalási díj:</td>
			<td><input type="text" name="reservation_fee" value="<?=$editarr[reservation_fee]?>" style='width:110px' class='numeric'/> Ft/fő</td>
			<td  class='lablerow'>Kerozin díj:</td>
			<td><input type="text" name="kerosene_fee" value="<?=$editarr[kerosene_fee]?>"  style='width:110px' class='numeric'/> Ft/fő</td>		
		</tr>
		
		<tr>
			<td class='lablerow'>Szerviz díj:</td>
			<td><input type="text" name="service_fee" value="<?=$editarr[service_fee]?>" style='width:110px' class='numeric'/> Ft/fő</td>
			<td  class='lablerow'>Transzfer díj:</td>
			<td><input type="text" name="transfer_fee" value="<?=$editarr[transfer_fee]?>"  style='width:110px' class='numeric'/> Ft/fő</td>		
		</tr>
<tr>
			<td class='lablerow'>Üdülőhelyi díj:
			</td>
			<td><input type="text" name="local_fee" value="<?=$editarr[local_fee]?>" style='width:110px;' class='numeric'/> Ft/fő</td>
			<td class='lablerow'></td>
			<td></td>		
		</tr>
		
		<tr>
			<td class='lablerow'>Stornó biztosítás:
				<br/><input type='checkbox' name='own_storno' <? if($editarr[own_storno] == 1) echo "checked"; ?> /> saját
			</td>
			<td><input type="text" name="storno_insurance_base" value="<?=$editarr[storno_insurance_base]?>" style='width:70px;text-align:right;' class='numeric'/> Ft <input type="text" name="storno_insurance" value="<?=$editarr[storno_insurance]?>" style='width:30px;text-align:right;'/> %-a</td>
			<td class='lablerow'><input type="text" name="insurance_name" value="<?=$editarr[insurance_name]?>"  style='width:80px'/> biztosítás:</td>
			<td><input type="text" name="insurance_value" value="<?=$editarr[insurance_value]?>"  style='width:110px' class='numeric'/> Ft</td>		
		</tr>
		<tr>
			<td class='lablerow'>Storno kötvényszám:
			</td>
			<td><input type="text" name="own_storno_number" value="<?=$editarr[own_storno_number]?>" style='width:110px;'/> </td>
			<td class='lablerow'>Biztosítás kötvényszám:</td>
			<td><input type="text" name="insurance_number" value="<?=$editarr[insurance_number]?>"  style='width:110px' /></td>		
		</tr>
		
		<tr>
			<td class='lablerow'><input type="text" name="extra1_name" value="<?=$editarr[extra1_name]?>" style='width:135px'/></td>
			<td><input type="text" name="extra1_value" value="<?=$editarr[extra1_value]?>" style='width:110px' class='numeric'/> Ft</td>
			<td class='lablerow'><input type="text" name="extra2_name" value="<?=$editarr[extra2_name]?>" style='width:135px'/></td>
			<td><input type="text" name="extra2_value" value="<?=$editarr[extra2_value]?>" style='width:110px' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td class='lablerow'><input type="text" name="extra3_name" value="<?=$editarr[extra3_name]?>" style='width:135px'/></td>
			<td><input type="text" name="extra3_value" value="<?=$editarr[extra3_value]?>" style='width:110px' class='numeric'/> Ft</td>
			<td class='lablerow'><input type="text" name="extra4_name" value="<?=$editarr[extra4_name]?>" style='width:135px'/></td>
			<td><input type="text" name="extra4_value" value="<?=$editarr[extra4_value]?>"  style='width:110px' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td class='lablerow'><input type="text" name="extra5_name" value="<?=$editarr[extra5_name]?>" style='width:135px'/></td>
			<td><input type="text" name="extra5_value" value="<?=$editarr[extra5_value]?>" style='width:110px' class='numeric'/> Ft</td>
			<td class='lablerow'><input type="text" name="extra6_name" value="<?=$editarr[extra6_name]?>" style='width:135px'/></td>
			<td><input type="text" name="extra6_value" value="<?=$editarr[extra6_value]?>" style='width:110px' class='numeric'/> Ft</td>
		</tr>

		
		<tr>
			<td class='lablerow'>Az ár tartalmazza:</td>
			<td colspan='3'><textarea name='inprice' id='inprice'><?=$editarr[inprice]?></textarea></td>
		</tr>
		<tr>
			<td  class='lablerow'>Az ár nem tartalmazza:</td>
			<td colspan='3'><textarea name='outprice' id='outprice'><?=$editarr[outprice]?></textarea></td>
		</tr>
		<tr>
			<td  class='lablerow'>Egyéb megjegyzés:</td>
			<td colspan='3'><textarea name='order_comment' style='height:50px'><?=$editarr[order_comment]?></textarea></td>
		</tr>
		<tr>
			<td  class='lablerow'>Részvételi megjegyzés:</td>
			<td colspan='3'><textarea name='voucher_comment' class="tinymce"><?=$editarr[voucher_comment]?></textarea></td>
		</tr>

		<tr>
			<td class='lablerow' colspan='3'>Összesen:</td>
			<td class='lablerow' align='right'> <?=formatPrice($editarr[total])?>
			<input type='hidden' id='totalvalue' value='<?=$editarr[total]?>'/>
			</td>
		</tr>
		<tr>
			<td class='lablerow' colspan='3'>Előleg (<input type='checkbox' value='1' name='is_option' <? if($editarr[is_option] == 1) echo "checked";?>/>opciózva):</td>
			<td class='lablerow' align='right'><input type="text" id="prepaid" class='numeric' style='width:50px'/> Ft = <input type="text" name="prepaid" value="<?=$editarr[prepaid]?>" style='width:30px' class='numeric' id='ppvalue'/> % 
	</td>
		</tr>
			<tr>
			<td class='lablerow' colspan='2'>Utalványszám:</td>
			<td class='lablerow' align='right'><input type="text" name="voucher_id" value="<?=$editarr[voucher_id]?>" id='coupon_code' style='width:150px' placeholder='Az utalvány sorszáma'/> </td>
			<td class='lablerow' align='right'><input type="hidden" name="voucher_value" id='voucher_val' value="<?=$editarr[voucher_value]?>" style='width:60px' class='numeric'/><a href='#' class='chk'>ellenőrzés &raquo;</a><span id='voucher_value'><?=formatPrice($editarr[voucher_value])?></span> <? if($CURUSER[passengers_admin] == 1) { ?><br/><a href='#' id='delcoupon'>kód törlése &raquo;</a><? } ?><br/><a href='/tour_discount.php' target='_blank'>illetékmentes kód &raquo;</a></td>
		</tr>

		<tr>
			<td class='lablerow blue' colspan='3'>Visszaigazolt összeg:</td>
			<td class='lablerow blue' align='right'><input type="text" name="final_total" value="<?=$editarr[final_total]?>" style='width:60px' class='numeric' id='final_total'/> Ft</td>
		</tr>
		<tr>
			<td class='lablerow blue' colspan='3'>Visszaigazolt jutalék:</td>
			<td class='lablerow blue' align='right'><input type="text" name="yield" value="<?=$editarr['yield']?>" style='width:60px' class='numeric'/> Ft</td>
		</tr>
			<tr>
			<td class='lablerow blue' colspan='3'>Partner felé utalandó előleg <input type="text" style='width:25px' class='numeric' id='partner_deposit_percent'/>%: <span class='pdeppercent'></span></td>
			<td class='lablerow blue' align='right'> <input type="text" name="partner_deposit" value="<?=$editarr[partner_deposit]?>" style='width:60px' class='numeric' id='partner_deposit'/> Ft  <input type="text" name="partner_deposit_due" value="<? if($editarr["partner_deposit_due"] <> '0000-00-00') echo $editarr["partner_deposit_due"];?>" style='width:80px;text-align:center;' class='dpick' placeholder='utalási határidő'/></td>
		</tr>
		<tr>
			<td class='lablerow blue' colspan='3'>Biztosítás jutaléka: (automatán számolódik)</td>
			<td class='lablerow blue' align='right'><input type="text" name="insurance_yield_value" value="<?=$editarr[insurance_yield_value]?>" style='width:60px' class='numeric'/> Ft</td>
		</tr>
		
		<tr>
			<td  class='lablerow purple'>Privát megjegyzés:</td>
			<td colspan='3' class='purple'><textarea type='text' name='comment' style='height:20px;margin-bottom:2px;'></textarea>
			<textarea disabled style='height:40px;'><?=$editarr[comment]?></textarea></td>
		</tr>
		
		
	
	

		<tr>
			<td class='lablerow'>Hivatkozás:</td>
			<td class='lablerow' colspan='3'><input type="text" name="position_number" value="<?=$editarr[position_number]?>"/></td>
		</tr>
		<? if($editarr[invoice_number] <> '') { ?>
		<tr>
			<td class='lablerow'>Számla száma:</td>
			<td class='lablerow' colspan='3'><?=$editarr[invoice_number]?></td>
		</tr>
		<tr>
			<td class='lablerow'>Számla dátuma:</td>
			<td class='lablerow' colspan='3'><?=$editarr[invoice_date]?></td>
		</tr>
		<? } ?>
		<tr>
			<td class='lablerow'>Fizetési határidő:</td>
			<td class='lablerow' colspan='3'><input type="text" name="due_date" value="<?=$editarr[due_date]?>" class='dpick'/></td>
		</tr>
		
		<? if($CURUSER[passengers_admin] == 1) { ?>
		<tr>
			<td class='lablerow'>Törölt:</td>
			<td class='lablerow' colspan='3'>
				
				<select name='inactive'>
					<option value='0' <? if($editarr[inactive] == '0' ) echo "selected";?>>nem</option>
					<option value='1' <? if($editarr[inactive] == '1') echo "selected";?>>igen</option>
				</select>	<?=$delreasons[$editarr[donotwant]]?>
			</td>
		</tr>
		<? } ?>
		<tr>
			<td class='lablerow'>Emlékeztető tiltása:</td>
			<td class='lablerow' colspan='3'>
				
				<select name='disable_user_notification'>
					<option value='0' <? if($editarr[disable_user_notification] == '0' ) echo "selected";?>>nem</option>
					<option value='1' <? if($editarr[disable_user_notification] == '1') echo "selected";?>>igen</option>
				</select>
			</td>
		</tr>
			<tr>
				<td colspan='4' align='center'>
					<input type='submit' id='psubmit' value='Mentés'/>
				</td>
			</tr>
			
	</table>
	</form>
	
		
				<? if($editarr[id] > 0) { ?>
		<a name='upload' id='upload'></a>		
		<form method='post'   enctype="multipart/form-data">
		<input type='hidden' name='id' value='<?=$editarr[id]?>'/>
		<input type='hidden' name='offer_id' value='<?=$editarr[offer_id]?>'/>
		<input type='hidden' name='upload' value='1'/>
		<div style='height:30px'></div>
		<table>

		<tr>
			<td class='lablerow'>File feltöltése:</td>
			<td class=''>
				<input type='file' name='file'/>
			</td>
			<td><select name='category'>
				<option value='visszaigazolas'>visszaigazolás</option>
				<option value='szamla'>számla</option>
				<option value='kerveny'>kérvény</option>
				<option value='egyeb'>egyéb</option>
			</select>
			</td>
			<td><input type='submit' value='Feltöltés'/></td>
		</tr>
		
		<?
$dh  = @opendir("/var/www/hosts/pihenni.hu/htdocs/static.pihenni/passengers/$editarr[id]");
$files = array();
while (false !== ($filename = @readdir($dh))) {
   if($filename <> '.' && $filename <> '..')
    $files[] = "<li><a href='http://static.indulhatunk.hu/passengers/$editarr[id]/$filename' target='_blank'>".$filename."</a></li>";
}
	if(!empty($files))
	{
		$list = implode("",$files);
		echo "<tr>";
			echo "<td colspan='4'><ul>$list</ul></td>";
		echo "</tr>";
		
	}


		?>
		</table>
		</form>
		
		
		<div style='height:30px'></div>
		<table>
		<tr class='header'>
		<td colspan='4'>
			Emlékeztetők
		</td>
		</tr>
		<tr>
		<td colspan='4'>
		<form method='POST'>
			<input type='hidden' name='addevent' value='1'/>
			<input type='hidden' name='customer_id' value='<?=$editarr[id]?>'/>
			<input type='text' name='due_date' class='maskeddate' placeholder='Dátum'/>
			<select name='due_time'>
			<? 
				for($g = 8;$g<=24;$g++)
				{
					echo "<option value='$g:00'>$g:00</option>";
				}	
			?>
			</select>
			<input type='text' name='comment' style='width:500px;' placeholder='Megjegyzés'/>
			<input type='submit' value='Mentés'/>
		</form>
		</td>
		
		</tr>
		<?
			$checkevents = $mysql->query("SELECT * FROM customers_tour_notifications WHERE customer_id = $editarr[id] ORDER BY due_date DESC");
			while($due = mysql_Fetch_assoc($checkevents))
			{
				$agname = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $due[agent_id] LIMIT 1"));
				
				if($due[status] == 1)
				{
					$class = 'green';
					$ename = "(elvégezte: $due[resolved_by] @ $due[resolved_date])";
				}
				else
				{
					$class = '';
					$ename = '';
				}
				$events.="
					<tr class='$class'>
						<td>$due[due_date]</td>
						<td>$agname[username]</td>
						<td>$due[comment] $ename</td>
					</tr>
				";
				
			}
			
			if($events <> '')
			{
				echo "<tr class='header'>
						<td width='90'>Dátum</td>
						<td width='90'>Tulajdonos</td>
						<td>Megjegyzés</td>
					</tr>";
					
				echo $events;
				
				
			}
		?>
		</table>



		<div style='height:30px'></div>
		
		<? } ?>




	<? if($CURUSER[userclass] == 255 || $CURUSER[username] == 'csongor' || $CURUSER[username] == 'matyas')
		{
		?>
		
	<table>
		<tr>
			<td id='logdata' colspan='4'>
			
				
			
			</td>
		</tr>
		</table>
		
		
		

		<?	
		}	
		?>
	</table>


	</fieldset>

</div>

</div></div>
<?
foot();
die;
}


if($CURUSER[userclass] == 255 && $_GET[showusers] == 1)
{


$users = $mysql->query("SELECT agent_id FROM customers_tour WHERE company_invoice = 'szallasoutlet' GROUP BY agent_id ORDER BY agent_id ASC");
echo "<table>";

echo "<tr class='header'>";
		echo "<td></td>";
		echo "<td colspan='2'>web</td>";
		echo "<td colspan='2'>telefon</td>";
		echo "<td colspan='2'>email</td>";
		echo "<td colspan='2'>személyes</td>";
		echo "<td colspan='2'>Kérdés</td>";

		echo "<td colspan='3'>Összes</td>";
		echo "<td colspan='4'>Befizetett</td>";
		echo "<td></td>";

	echo "</tr>";

	echo "<tr class='header'>";
		echo "<td>Értékesítő</td>";
		echo "<td align='center'>db</td>";
		echo "<td align='center'>Ft</td>";
		echo "<td align='center'>db</td>";
		echo "<td align='center'>Ft</td>";
		echo "<td align='center'>db</td>";
		echo "<td align='center'>Ft</td>";
		echo "<td align='center'>db</td>";
		echo "<td align='center'>Ft</td>";
			echo "<td align='center'>db</td>";
		echo "<td align='center'>Ft</td>";


		echo "<td align='center'>db</td>";
		echo "<td align='center'>Ft</td>";
		echo "<td align='center'>átl.</td>";

		echo "<td align='center'>db</td>";
		echo "<td align='center'>Ft</td>";
		echo "<td align='center'>átl.</td>";
		echo "<td align='center'>jut.</td>";
		echo "<td align='center'>megv.</td>";
	//	echo "<td align='right'>".formatPrice($tp[total]/$tp[cnt],0,1)."</td>";
	//	echo "<td align='right'>".round($tp[cnt]/$total[cnt]*100)."%</td>";
	echo "</tr>";
	
while($arr = mysql_fetch_assoc($users))
{
	$agent = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[agent_id] LIMIT 1"));
	
	$total = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, count(id) as cnt FROM customers_tour WHERE company_invoice = 'szallasoutlet' AND agent_id = $arr[agent_id]"));
	
	$tp = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, count(id) as cnt, sum(yield) as yield FROM customers_tour WHERE company_invoice = 'szallasoutlet' AND agent_id = $arr[agent_id] AND status >= 3 AND inactive = 0"));
	
	$web = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, count(id) as cnt FROM customers_tour WHERE company_invoice = 'szallasoutlet' AND agent_id = $arr[agent_id] AND source = 'web' AND inactive = 0"));
	
	$phone = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, count(id) as cnt FROM customers_tour WHERE company_invoice = 'szallasoutlet' AND agent_id = $arr[agent_id] AND source = 'phone' AND inactive = 0"));

$email = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, count(id) as cnt FROM customers_tour WHERE company_invoice = 'szallasoutlet' AND agent_id = $arr[agent_id] AND source = 'email' AND inactive = 0"));


$personal = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, count(id) as cnt FROM customers_tour WHERE company_invoice = 'szallasoutlet' AND agent_id = $arr[agent_id] AND source = 'personal' AND inactive = 0"));

$question = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, count(id) as cnt FROM customers_tour WHERE company_invoice = 'szallasoutlet' AND agent_id = $arr[agent_id] AND source = 'question' AND inactive = 0"));

	$nm = explode(" ",$agent[company_name]);
		$nm = end($nm);

	echo "<tr>";
		echo "<td>$nm</td>";
		echo "<td align='right'>$web[cnt]</td>";
		echo "<td align='right'>".formatPrice($web[total],0,1)."</td>";
		echo "<td align='right' class='lightgrey'>$phone[cnt]</td>";
		echo "<td align='right' class='lightgrey rightborder'>".formatPrice($phone[total],0,1)."</td>";
		echo "<td align='right'>$email[cnt]</td>";
		echo "<td align='right' class='rightborder'>".formatPrice($email[total],0,1)."</td>";
		echo "<td align='right' class='lightgrey'>$personal[cnt]</td>";
		echo "<td align='right' class='lightgrey rightborder'>".formatPrice($personal[total],0,1)."</td>";
			echo "<td align='right' class='lightgrey'>$question[cnt]</td>";
		echo "<td align='right' class='lightgrey rightborder'>".formatPrice($question[total],0,1)."</td>";
		echo "<td align='right' class='rightborder'>$total[cnt]</td>";
		echo "<td align='right'>".formatPrice($total[total],0,1)."</td>";
		echo "<td align='right' class='rightborder'>".@formatPrice($total[total]/$total[cnt],0,1)."</td>";
		echo "<td align='right' class='lightgrey'>$tp[cnt]</td>";
		echo "<td align='right' class='lightgrey'>".formatPrice($tp[total],0,1)."</td>";
		echo "<td align='right' class='lightgrey rightborder'>".@formatPrice($tp[total]/$tp[cnt],0,1)."</td>";
		echo "<td align='right'>".formatPrice($tp['yield'],0,1)."</td>";
		echo "<td align='right'>".@round($tp[cnt]/$total[cnt]*100)."%</td>";

	echo "</tr>";
	
	$totc = $totc+$tp[cnt];
	$totp = $totp+$tp[total];
	$ty = $ty + $tp['yield'];
}



echo "<tr class='header'>";
		echo "<td colspan='12'></td>";
		echo "<td align='right'>$totc</td>";
		echo "<td align='right'>".formatPrice($totp,0,1)."</td>";
		echo "<td align='right'>".formatPrice($ty,0,1)."</td>";
		echo "<td align='right'>".formatPrice($totp/$totc,0,1)."</td>";
		echo "<td></td>";

	echo "</tr>";
	
echo "</table><hr/>";
/***/
$sum = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total FROM customers_tour_payment WHERE is_transfer = 0 AND added > '2014-07-04'"));
$total = mysql_fetch_assoc($mysql->query("SELECT sum(total) as total, sum(yield) as yield FROM customers_tour WHERE status >= 3 AND company_invoice = 'szallasoutlet'"));

$totalcount = mysql_fetch_assoc($mysql->query("SELECT count(id) as total FROM customers_tour WHERE status >= 3 AND company_invoice = 'szallasoutlet'"));

$tin = mysql_fetch_assoc($mysql->query("SELECT count(id) as total FROM customers_tour WHERE company_invoice = 'szallasoutlet'"));

$percent = $sum[total]/ $total[total]*100;

$pc = $totalcount[total]/ $tin[total]*100;

?>
<table style='width:300px;margin:0 auto;'>
	<tr>
		<td class='lablerow'>Beérkezett db</td>
		<td align='right'><?=$tin[total]?> db</td>
	</tr>
	<tr>
		<td class='lablerow'>Eladott db (<?=round($pc)?>%)</td>
		<td align='right'> <?=$totalcount[total]?> db</td>
	</tr>
	<tr>
		<td class='lablerow'>Eladott összeg</td>
		<td align='right'><?=formatPrice($total[total])?></td>
	</tr>
	
	<tr>
		<td class='lablerow'>Beérkezett összeg (<?=round($percent)?>%)</td>
		<td align='right'><?=formatPrice($sum[total])?></td>
	</tr>
	<tr>
		<td class='lablerow'>Realizált jutalék</td>
		<td align='right'><?=formatPrice($total['yield'])?></td>
	</tr>

</table>

<?
	$query = $mysql->query("SELECT sum(value) as total, added FROM customers_tour_payment WHERE is_transfer = 0  AND added > '2014-07-04' GROUP BY YEAR(added), MONTH(added)  ORDER BY added ASC");
	
	$cid = array();
	
	while($arr = mysql_fetch_assoc($query))
	{
		$tyield = 0;
		$m = explode("-",$arr[added]);
		$month = $m[1];
		
		$year = $m[0];

		$yield = $mysql->query("SELECT customer_id FROM customers_tour_payment WHERE added like '%$year-$month%'  GROUP by customer_id ORDER BY added DESC");
		
		while($y = mysql_fetch_assoc($yield))
		{
			
			if(!in_array($y[customer_id],$cid))			
			{
				$info = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour WHERE id = $y[customer_id] AND company_invoice = 'szallasoutlet' LIMIT 1"));
				$tyield = $tyield + $info['yield'];
			
				$cid[] = $y[customer_id];
			}
		}

	//	$weekday = date('N', strtotime($day));
		
		$dates[] = "['$year-$month',".$arr[total].",$tyield]";
	if($weekday == 7)
	{
		$sunday = $sunday + $arr[total];
		$class = 'header';
	}
	else
		$class = '';
		
		
/*
	echo "<tr class='$class'>";
		echo "<td width='80'>$day</td>";
		echo "<td>".$weekdays[$weekday]."</td>";
		echo "<td align='right'>$arr[cnt] db</td>";
	echo "</tr>";
*/
		$totalcnt = $totalcnt + $arr[cnt]+$pnt[cnt];
	}
	
/*	echo "<tr class='header'>";
	
		echo "<td colspan='2' align='left'>Összesen</td>";
		echo "<td align='right'>$totalcnt db</td>";
	echo "</tr>";
	
	echo "</table><br/><br/>";
	*/
	$days = implode(",",$dates)
?>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Hónap', 'Bevétel','Jutalék'],
          <?=$days?>
        ]);

        var options = {
          title: 'Belföldi eladások',
          'width':870,
           'height':250,
           legend: {},
           fontSize: 9
          //hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };
        
 
  
  

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    
        <div id="chart_div" style="width: 870px; height: 250px;"></div>
        





<hr/>
<?
}



if($_GET[showstats] == 1)
{
	?>
	<script src='//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js'></script>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css"/>
	<script>
		
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "currency-pre": function (a) {
        a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
        return parseFloat(a);
    },
    "currency-asc": function (a, b) {
        return a - b;
    },
    "currency-desc": function (a, b) {
        return b - a;
    }
});



		$(document).ready(function() {
			$('.sortable').DataTable({
				
				"aoColumns": [
				null,
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				null,
				null,
				{ "sType": "currency" }
		],
		
				"order": [[ 5, "desc" ]],
				bFilter: false, bInfo: false, "bPaginate": false,
		} );
		
		$('.sortablename').DataTable({
				"order": [[ 0, "asc" ]],
					"aoColumns": [
				null,
				null,
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				//{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
		],

				bFilter: false, bInfo: false, "bPaginate": false,
		} );
		
		$('.sortablename2').DataTable({
				"order": [[ 0, "asc" ]],
					"aoColumns": [
				null,
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
			],
			bFilter: false, bInfo: false, "bPaginate": false,
		} );
		
	} );	
	</script>
	<?
	$statuses = array("Ajánlat",'Lemondás','Megrendelőre vár','Visszaigazolásra vár','Visszaigazolva','Voucher kiküldve');
	
//ID	Útazás megnevezése	Utazásszervező	Foglalás dátuma	Indulás dátuma	Utasok száma	Kapcsolattartó	Szerződés összeg	Visszaigazolt összeg	Visszaigazolt jutalék	Anomália (Szerződés összeg - Visszaigazolt összeg)	Utas által befizetett összeg	Hártalék	Biztosítás összege	Biztosítás jutalék	Jutalék szla kiállítva	Hivatkozás	Állapot															



if($CURUSER[username] <> 'angela')
	$clist = "AND company_invoice = 'szallasoutlet'";
	
	
if($_GET[from_date] <> '')
	$extrafrom = "AND confirmation_date >= '$_GET[from_date] 00:00:00'";

if($_GET[to_date] <> '')
	$extrato = "AND confirmation_date <= '$_GET[to_date] 23:59:59'";

	
		$query = $mysql->query("SELECT * FROM customers_tour WHERE company_invoice = 'szallasoutlet' $extrato $extrafrom AND status >= 4 AND inactive = 0 ORDER BY from_date ASC");
?>

<a href='?showstats=1' style='display:block; width:100%; padding:10px; background-color:#4E9258; opacity:0.7; text-transform:uppercase; z-index:999; text-align:center; color:white; position:fixed; top:0; left: 0;'>Ehavi statisztika frissítése &raquo;</a>
<form method='get' method='get'>
	<input type='hidden' name='showstats' value='1'/>

	Kezdete<input type='text' name='from_date' class='dpick' value='<?=$_GET[from_date]?>'/> Vége<input type='text' class='dpick' name='to_date'  value='<?=$_GET[to_date]?>'/>
	<input type='checkbox' value='1' name='showitems' <? if($_GET[showitems] == 1) echo "checked"?>/> tételes lista
	<input type='submit' value='mutasd &raquo'/>
	
</form>
<hr/>
<?
	
	if($_GET[showitems] <> 1)
	{
		$hidetable = 'display:none';
	}
echo "<style>
.hidden { display:none } 
</style><table class=\"general\" width='100%' style='$hidetable'>";

	echo "<tr class='header'>";
			echo "<td width='70' align='center' colspan='2'>ID</td>";
			echo "<td>Név</td>";
			echo "<td>Út neve</td>";
			echo "<td>Utazásszervező</td>";
			echo "<td>Foglalás dátuma</td>";
			echo "<td>Indulás dátuma</td>";
			echo "<td>Utasok száma</td>";
			echo "<td colspan='2'>Kapcsolattartó</td>";
			echo "<td>Szerződés összege</td>";
			echo "<td>Reptéri illeték</td>";
			echo "<td>Visszaigazolt összeg</td>";
			echo "<td>Visszaigazolt jutalék</td>";
			echo "<td>Eltérés</td>";
			echo "<td>Befizetett</td>";
			echo "<td>Hátralék</td>";
			echo "<td>Biztosítás</td>";
			echo "<td>Bizt. jut.k</td>";
			echo "<td>Kötv.</td>";
			echo "<td>Voucher</td>";
			echo "<td>Jutalékszámla</td>";
			echo "<td>Hivatkozás</td>";
			echo "<td>Állapot</td>";
			echo "<td>Haszon</td>";
			echo "<td>Eltérés</td>";

		echo "</tr>";
		
	$i = 1;
	while($arr = mysql_fetch_assoc($query))
	{
	
		//
		
		
	
		$partner = mysql_fetch_assoc($mysql->query("SELECT hotel_name FROM partners WHERE coredb_id = $arr[partner_id]"));
				
			$wh = mysql_fetch_assoc($mysql->query("SELECT company_name, office_id FROM partners WHERE pid = $arr[agent_id]"));
			$who = end(explode(" ",$wh[company_name]));
			
		$paid = mysql_fetch_assoc($mysql->query("SELECT sum(value) as tpaid FROM customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 0"));


		if($arr[inactive] == 1 || $arr[status] == 1)
			$class = 'hidden';
		else
			$class = '';
			
		if($arr[invoice_number] <> '')
			$inv = '<img src="/images/check1.png" width="15" alt="Számlázva"  title="Számlázva"/>';
		else
			$inv = '<font color="red"><b>!!!</b></font>';
		
		if(($arr[total]-$arr[total_paid]) <> 0)
			$left = formatPrice($arr[total]-$arr[total_paid]);
		else
			$left = '';
			
		
		if(($arr[total]-$arr[total_transfer]-$arr['yield']-$arr[insurance_value]) <> 0)
			$tleft = formatPrice($arr[total]-$arr[total_transfer]-$arr['yield']-$arr[insurance_value],0,1);
		else
			$tleft = '';
			
		if($arr[own_storno] == 1 && $arr[own_storno_number] == '')
			$ownstorno = '<font color="red"><b>!!!</b></font>';
		elseif($arr[own_storno] == 1 && $arr[own_storno_number] <> '')
			$ownstorno = '<img src="/images/check1.png" width="15" alt="Számlázva"  title="Számlázva"/>';
		else
			$ownstorno = '';
			
		if($arr[insurance_value] > 0 && $arr[insurance_number] == '')
			$insurance =  '<font color="red"><b>!!!</b></font>';
		elseif($arr[insurance_value] > 0 && $arr[insurance_number] <> '')
			$insurance = '<img src="/images/check1.png" width="15" alt="Számlázva"  title="Számlázva"/>';
		else
			$insurance = '';
		
			
		$added = explode(" ",$arr[added]);
		$from_date = explode(" ",$arr[from_date]);
		
		$pcount = 0;
		
		for($h = 1;$h<=10;$h++)
		{
			
			if($arr["passenger".$h."_name"] <> '')
			{	
				$pcount++;
			}
			
		}
		
		
				
		if($arr[own_storno] == 1)
			$stornovalue = $arr[storno_insurance];
		else
			$stornovalue = 0;
			
			
		if($pcount == 0 && $arr[inactive] == 0 && $arr[status] >= 2)
			$pclass = 'red';
		else
			$pclass = '';
			
		if($from_date[0] == '0000-00-00')
			$from_date[0] = '';
			
		if(($arr[total]-$arr[final_total]-$arr[insurance_value]-$stornovalue) == 0 && $arr[status] >= 2)
			$extraclass = '';
		elseif(($arr[total]-$arr[final_total]-$arr[insurance_value]-$stornovalue) > 0)
			$extraclass = 'green';
		elseif($arr[status] >= 2 && $arr[inactive] == 0)
			$extraclass = 'red';
		else
			$extraclass = '';
			
		if($arr[status] >= 3 && $arr['yield'] == 0)
			$yclass = 'red';
		else
			$yclass = '';
			
	
		if($arr[inactive] == 1 || $arr[status] < 3)
		{
			
		}
		else
		{
		
			if($arr[confirmation_date] == '0000-00-00 00:00:00')
			{
				$ck = mysql_fetch_assoc($mysql->query("SELECT * FROM log WHERE event like '%updated status = 4 @ passenger-$arr[id]%' OR event like '%updated status = 5 @ passenger-$arr[id]%' ORDER BY date ASC LIMIT 1"));
	
			if($ck[date] <> '')
				echo "UPDATE customers SET confirmation_date = '$ck[date]' WHERE id = $arr[id]; #$ck[event] <hr/>";
			}
		/*
		1.a teljes csapat eredményéből 2% lesz köztetek szétosztva – ezzel kompenzálva a vándorló utasokat. 
2.irodai szinten az eredményből 1,5% lesz az irodában dolgozók között szétosztva – hiszen sok esetben egymásnak is segítetek. 
3.saját eladások eredményéből 2,5% kap minden egyes kolléga – ezzel ösztönözve, megbecsülve az egyéni teljesítményt. 
*/
		$office = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = '$wh[office_id]'"));

		
		if($arr[voucher_value] == 0)
			$arr[voucher_value] = '';
		echo "<tr class='$class'>";
			echo "<td>$i</td>";
			echo "<td width='70'><a href='/passengers.php?add=1&editid=$arr[id]'><b>".str_replace("IND-2012/",'',$arr[offer_id])."</b></a></td>";
			echo "<td>$arr[name]</td>";
			echo "<td>$arr[offer_name]</td>";
			echo "<td>$partner[hotel_name]</td>";
			echo "<td>$added[0]</td>";
			echo "<td>$from_date[0]</td>";
			echo "<td class='$pclass' align='right'>$pcount</td>";
			echo "<td align='right'>$who</td>";
			echo "<td>$office[address]</td>";
			echo "<td align='right'>".formatPrice($arr[total],0,1)."</td>";
			echo "<td align='right'>".formatPrice($arr[airport_fee],0,1)."</td>";
			echo "<td align='right'>".formatPrice($arr[final_total],0,1)."</td>";
			echo "<td class='$yclass' align='right'>".formatPrice($arr['yield'],0,1)."</td>";
			echo "<td class='$extraclass' align='right'>".formatPrice($arr[total]-$arr[final_total]-$arr[insurance_value]-$stornovalue)."</td>";
			echo "<td align='right'>".formatPrice($paid[tpaid],0,1)."</td>";
			echo "<td align='right'>".formatPrice($arr[total]-$paid[tpaid],0,1)."</td>";
			echo "<td align='right'>".formatPrice($arr[insurance_value],0,1)."</td>";
			echo "<td align='right'>".formatPrice($arr[insurance_yield_value],0,1)."</td>";
			echo "<td align='right'>$arr[insurance_number]<br/>$arr[own_storno_number]<br/></td>";					
			echo "<td align='right'>".formatPrice($arr[voucher_value],0,1)."</td>";
			echo "<td >$arr[invoice_number]</td>";
			echo "<td>$arr[position_number]</td>";
			echo "<td>".$statuses[$arr[status]]."</td>";
			
			$ptotal = $ptotal + $pcount;
			$tot = $tot + $arr[total];
			$totcontract = $totcontract + $arr[final_total];
			$totalpaid = $totalpaid + $paid[tpaid];
			$totalyield = $totalyield + $arr['yield'];
			$totalleft = $totalleft + ($arr[total]-$paid[tpaid]);
			$totaldiff = $totaldiff + ($arr[total]-$arr[final_total]-$arr[insurance_value]-$stornovalue);

			$totalinsurance = $totalinsurance + $arr[insurance_value];
			$insuranceyield = $insuranceyield + $arr[insurance_yield_value];

			$totaldiscount = $totaldiscount + $arr[voucher_value];
			
			
			 
			$itemtotal = $arr['yield'] + ($arr[total]-$arr[final_total]-$arr[insurance_value]-$stornovalue) + $arr[insurance_yield_value];
			
			$subtotal = $subtotal + $itemtotal;
			
			$aglist[$arr[agent_id]] = $aglist[$arr[agent_id]] + $itemtotal;
			$aglisttotal[$arr[agent_id]] = $aglisttotal[$arr[agent_id]] + $arr[total];

			
			if($wh[office_id] == 2499)
				$hqoffice[$arr[agent_id]] = $arr[agent_id];
				
				
			$oflist[$office[id]] = $oflist[$office[id]] + $itemtotal;

			$offincome[$office[id]] = $offincome[$office[id]] + $arr[total];

			echo "<td align='right'>".formatPrice($itemtotal,0,1)."</td>";
			
			$diff = round($itemtotal) - $arr[total_income];
			
			if($diff <> 0)
			{
				echo "<td align='right' class='red'>".formatPrice($diff,0,1)." </td>";
			}
			else
			{
				echo "<td align='right'></td>";
			}
				
			$partners[$arr[partner_id]][count]++;
			$partners[$arr[partner_id]][passengers] = $partners[$arr[partner_id]][passengers]+$pcount;
			$partners[$arr[partner_id]]['yield'] = $partners[$arr[partner_id]]['yield']+$arr['yield'];
			$partners[$arr[partner_id]][total] = $partners[$arr[partner_id]][total]+$arr[final_total];
			$partners[$arr[partner_id]][total_income] = $partners[$arr[partner_id]][total_income]+$itemtotal;


			if($_POST[close_month] == 1)
			{
			
				$mysql->query("UPDATE customers_tour  SET total_income = $itemtotal, total_income_date = NOW() WHERE id = $arr[id] AND total_income_date = '0000-00-00 00:00:00' AND total_income = 0");
				writelog("$CURUSER[username] set tour total_income");
				
			}
		/*	echo "<td>$arr[from_date]</td>";
			echo "<td width='70'><a href='/passengers.php?add=1&editid=$arr[id]'><b>".str_replace("IND-2012/",'',$arr[offer_id])."</b></a></td>";
			echo "<td><a href='#' alt='' title=''><b>$arr[name]</b></a><br/><span style='font-size:10px;'>$partner[hotel_name] / $arr[destination]</span></td>";
			echo "<td align='right'><a href='/passenger_payments.php?id=$arr[id]' target='_blank'><b>".formatPrice($arr[total],0,1)."</b></a></td>";
			echo "<td align='right'>".formatPrice($arr['yield'],0,1)."</td>";
			echo "<td align='right'>$left</td>";
			echo "<td align='right'>$tleft</td>";
			
			echo "<td align='center'>$insurance</td>";
			echo "<td align='center'>$ownstorno</td>";
			echo "<td align='center'>$inv</td>";


			echo "<td align='center'>".$statuses[$arr[status]]."<br/>$who</td>";*/
		echo "</tr>";
		
		$i++;
		}
	}
	
	echo "
	<tr class='header'>
	<td colspan='7'>Összesen</td>
	<td align='right'>$ptotal</td>
	<td align='right'></td>
	<td align='right'></td>
	<td align='right'>".formatPrice($tot,0,1)."</td>
	<td align='right'>".formatPrice($totcontract,0,1)."</td>
	<td align='right'>".formatPrice($totalyield,0,1)."</td>
	<td align='right'>".formatPrice($totaldiff,0,1)."</td>
	<td align='right'>".formatPrice($totalpaid,0,1)."</td>
	<td align='right'>".formatPrice($totalleft,0,1)."</td>
	<td align='right'>".formatPrice($totalinsurance,0,1)."</td>
	<td align='right'>".formatPrice($insuranceyield,0,1)."</td>
	<td></td>
	<td align='right'>".formatPrice($totaldiscount,0,1)."</td>
	<td align='right' colspan='3'></td>
	<td align='right'>".formatPrice($subtotal,0,1)."</td>

	</tr>";
	
	
echo "</table>";


echo "<hr/><table class='sortable'>";

echo "<thead><tr class='header'>";
		echo "<td>Partner</td>";
		echo "<td>Beérkezett</td>";
		echo "<td>Befizetett</td>";
		echo "<td>%</td>";
		echo "<td>Bef. fő</td>";
		echo "<td>Érték</td>";
		echo "<td>Jutalék</td>";
		echo "<td>Jut.%</td>";

		echo "<td>Nyereség</td>";

	echo "</tr></thead>";
	
foreach($partners as $partner => $details)
{
	$pdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE coredb_id = $partner"));
	
	$total = mysql_fetch_assoc($mysql->query("SELECT count(id) as cnt FROM customers_tour WHERE company_invoice = 'szallasoutlet' $extrato $extrafrom  AND inactive = 0 AND partner_id = $partner"));

	$ratio = round(($details[count]/$total[cnt])*100);
	
	if($details[total] > 0)
		$yield = round(($details['yield']/$details[total])*100);

	echo "<tr>";
		echo "<td><a href='?showstats=1&showpartner=$pdata[pid]&from_date=$_GET[from_date]&to_date=$_GET[to_date]'><b>".substr($pdata[hotel_name],0,20)."</b></a></td>";
		echo "<td align='right'>$total[cnt] db</td>";
		echo "<td align='right'>$details[count] db</td>";
		echo "<td align='right'>$ratio%</td>";
		echo "<td align='right'>$details[passengers] fő</td>";
		echo "<td align='right'>".formatPrice($details[total])."</td>";
		echo "<td align='right'>".formatPrice($details['yield'])."</td>";
		echo "<td align='right'>$yield%</td>";
		echo "<td align='right'>".formatPrice($details[total_income])."</td>";

	echo "</tr>";
	
	if($pdata[pid] == $_GET[showpartner])
	{
		echo "<tr><td colspan='9'><table>";

		$clist = $mysql->query("SELECT final_total,offer_id,destination FROM customers_tour WHERE company_invoice = 'szallasoutlet' $extrato $extrafrom  AND inactive = 0 AND partner_id = $partner ORDER BY destination ASC");

		while($a = mysql_fetch_assoc($clist))
		{
			
			echo "<tr><td width='10' align='center'>&raquo;</td><td>$a[destination]</td><td>$a[offer_id]</td><td align='right'>".formatPrice($a[final_total])."</td></tr>";
		}
		
		echo "</table></td></tr>";
	}
}

echo "</table>";

echo "<hr/><table class='sortablename'>";
	echo "<thead><tr class='header'><td>Név</td><td>Forgalom</td><td align='right'>Bruttó</td><td align='right'>Nettó</td><td align='right'>Iroda 3,5%</td><td align='right'>Saját 2.5%</td><td align='right'>Jutalék</td></tr></thead>";

$total = 0;
$totalyield = 0;

if(empty($hqoffice))
	$hqoffice = array();
	
foreach($aglist as $id => $value)
{
	$minus = count($hqoffice);
	$singleyield = round($subtotal/(count($aglist)-$minus)*0.02);
	
	$wh = mysql_fetch_assoc($mysql->query("SELECT company_name, office_id FROM partners WHERE pid = $id"));
	$wh2 = explode(" ",$wh[company_name]);
	$who = end($wh2);
	
	$who = $who." ".$wh2[0][0].".";
	
	$howmany = mysql_fetch_assoc($mysql->query("select count(pid) as cnt FROM partners WHERE office_id = '$wh[office_id]'"));
	$off = $oflist[$wh[office_id]];
	
	$curryield = ((($off/$howmany[cnt])/1.27)*0.035)+(($value/1.27)*0.025);
	
	if(in_array($id,$hqoffice))
		$class = 'orange';
	else
		$class = '';


	$ofdata = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = '$wh[office_id]'"));


	echo "<tr class='$class'>
		<td>$who<br/>($ofdata[city] $howmany[cnt] fő)</td>
		<td align='right'>".formatPrice($aglisttotal[$id])."<br/>".formatPrice($offincome[$wh[office_id]])."</td>
		<td align='right'>".formatPrice($value)."<br/>".formatPrice($oflist[$wh[office_id]])."</td>
		<td align='right'>".formatPrice($value/1.27)."<br/>".formatPrice($oflist[$wh[office_id]]/1.27)."</td>
		<td align='right'>".formatPrice((($off/$howmany[cnt])/1.27)*0.035)."</td>
		<td align='right'>".formatPrice(($value/1.27)*0.025)."</td>
		<td align='right' class='grey'>".formatPrice($curryield)."</td>

		
	</tr>";
	
	
	$total = $total + $value;
	$totalyield = $totalyield + $curryield;
}


	echo "<tfoot><tr class='header'><td>Összesen</td><td></td><td align='right'>".formatPrice($total)."</td><td align='right'>".formatPrice($total/1.27)."</td><td colspan='2'></td><td align='right' class='grey'>".formatPrice($totalyield)."</td></tr></tfoot>";

echo "</table>";

echo "<hr/><table class='sortablename2'>";
$total = 0;
	echo "<thead><tr class='header'><td>Név</td><td align='right'>Bevétel</td><td align='right'>Bruttó</td><td align='right'>Nettó</td></tr></thead>";

foreach($oflist as $id => $value)
{
	$who = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = '$id'"));
	echo "<tr><td>$who[city]</td><td align='right'>".formatPrice($offincome[$id])."</td><td align='right'>".formatPrice($value)."</td><td align='right'>".formatPrice($value/1.27)."</td></tr>";
		$total = $total + $value;
		$totalinc = $totalinc + $offincome[$id];
}
	echo "<tfoot><tr class='header'><td>Összesen</td><td align='right'>".formatPrice($totalinc)."</td><td align='right'>".formatPrice($total)."</td><td align='right'>".formatPrice($total/1.27)."</td></tr></tfoot>";

echo "</table>";



		
if($_GET[from_date] <> '' && $_GET[to_date] <> '')
{

echo "<form method='POST'>
	<input type='hidden' name='close_month' value='1'/>
	<input type='hidden' name='from_date' class='dpick' value='$_GET[from_date]'/><input type='hidden' class='dpick' name='to_date'  value='$_GET[to_date]'/>
	<input type='submit' value='Elszámolás készítése &raquo'/>
	
</form>";

}
	echo "</div></div>";
	echo foot();
die;
}



if($_GET[showpaid] == 1)
{
	$statuses = array("Ajánlat",'Lemondás','Megrendelőre vár','Visszaigazolásra vár','Visszaigazolva','<img src="/images/check1.png" width="15" alt="Voucher kiküldve"  title="Voucher kiküldve"/>');
	



if($CURUSER[username] <> 'angela')
	$clist = "AND company_invoice = 'szallasoutlet'";
	
	
		$query = $mysql->query("SELECT customers_tour.* FROM customers_tour LEFT join partners ON partners.pid = customers_tour.agent_id WHERE  $eoffice total_paid > 0 AND inactive = 0 $clist   $elink ");



//		$query = $mysql->query("SELECT * FROM customers_tour WHERE total_paid > 0 AND inactive = 0 $clist ORDER BY from_date ASC");

echo "<table class=\"general\" width='100%'>";

	echo "<tr class='header'>";
		echo "<td width='70'>Indulás</td>";
			echo "<td width='70' align='center'>ID</td>";
			echo "<td>Név</td>";
			echo "<td>Összeg</td>";
			echo "<td>Jutalék</td>";
			echo "<td>Hátralék</td>";
			echo "<td>Utalandó</td>";
			echo "<td>Bizt.</td>";
			echo "<td>St.</td>";
			echo "<td width='20'>Szla</td>";
			echo "<td>Állapot</td>";
		echo "</tr>";
	while($arr = mysql_fetch_assoc($query))
	{
		
		$partner = mysql_fetch_assoc($mysql->query("SELECT hotel_name FROM partners WHERE coredb_id = $arr[partner_id]"));
				
			$who = mysql_fetch_assoc($mysql->query("SELECT company_name, office_id FROM partners WHERE pid = $arr[agent_id]"));
			$who = end(explode(" ",$who[company_name]));
			
			
		if($arr[from_date] <= date("Y-m-d"))
			$class = 'blue';
		else
			$class = '';
		if($arr[invoice_number] <> '')
			$inv = '<img src="/images/check1.png" width="15" alt="Számlázva"  title="Számlázva"/>';
		else
			$inv = '<font color="red"><b>!!!</b></font>';
		
		if(($arr[total]-$arr[total_paid]) <> 0)
			$left = formatPrice($arr[total]-$arr[total_paid]);
		else
			$left = '';
			
		
		if(($arr[total]-$arr[total_transfer]-$arr['yield']-$arr[insurance_value]) <> 0)
			$tleft = formatPrice($arr[total]-$arr[total_transfer]-$arr['yield']-$arr[insurance_value],0,1);
		else
			$tleft = '';
			
		if($arr[own_storno] == 1 && $arr[own_storno_number] == '')
			$ownstorno = '<font color="red"><b>!!!</b></font>';
		elseif($arr[own_storno] == 1 && $arr[own_storno_number] <> '')
			$ownstorno = '<img src="/images/check1.png" width="15" alt="Számlázva"  title="Számlázva"/>';
		else
			$ownstorno = '';
			
		if($arr[insurance_value] > 0 && $arr[insurance_number] == '')
			$insurance =  '<font color="red"><b>!!!</b></font>';
		elseif($arr[insurance_value] > 0 && $arr[insurance_number] <> '')
			$insurance = '<img src="/images/check1.png" width="15" alt="Számlázva"  title="Számlázva"/>';
		else
			$insurance = '';
		
		if($left > 0)
			$leftlink = "<a href='/info/notify-passenger.php?id=$arr[id]' rel='facebox' alt='Fizetési emlékeztető küldése' title='Fizetési emlékeztető küldése'><b>$left &raquo;</b></a>";
		else
			$leftlink = '';
		echo "<tr class='$class'>";
			echo "<td>$arr[from_date]</td>";
			echo "<td width='70'><a href='/passengers.php?add=1&editid=$arr[id]'><b>".str_replace("IND-2012/",'',$arr[offer_id])."</b></a></td>";
			echo "<td><a href='#' alt='' title=''><b>$arr[name]</b></a><br/><span style='font-size:10px;'>$partner[hotel_name] / $arr[destination]</span></td>";
			echo "<td align='right'><a href='/passenger_payments.php?id=$arr[id]' target='_blank'><b>".formatPrice($arr[total],0,1)."</b></a></td>";
			echo "<td align='right'>".formatPrice($arr['yield'],0,1)."</td>";
			echo "<td align='right'>$leftlink</td>";
			echo "<td align='right'>$tleft</td>";
			
			echo "<td align='center'>$insurance</td>";
			echo "<td align='center'>$ownstorno</td>";
			echo "<td align='center'>$inv</td>";


			echo "<td align='center'>".$statuses[$arr[status]]."<br/>$who</td>";
		echo "</tr>";
	}
echo "</table>";

	echo "</div></div>";
	echo foot();
die;
}
//if searchquery is not empty

if($_GET[mybookings] == 1 && $CURUSER[passengers_admin] == 1)
{
	
	$sellers = $mysql->query("SELECT * FROM partners WHERE passengers = 1 ORDER By username ASC");
?>	
<div style='text-align:center'>
<form method='get'>
	<input type='hidden' name='mybookings' value='1'/>
	<select name='agent_id' onchange='submit()'>
		<option value='1'>értékesítő</option>
		<?
			while($s = mysql_Fetch_assoc($sellers))
			{	
				if($s[pid] == $_GET[agent_id])	
					$selected = 'selected';
				else
					$selected = '';
				echo "<option value='$s[pid]' $selected>$s[username]</option>";

			}
			?>
	</select>
</form>
<hr/>
</div>	
<?	
	
}
if($CURUSER[username] <> 'angela')
	$clist = "AND company_invoice = 'szallasoutlet'";

if($_POST[fdate] <> '' && $_POST[tdate] <> '')	
{
	
	if($s <> '')
		$searchextra = "(offer_id like '%$s%' OR name like '%$s%' OR invoice_name like '%$s%' OR comment like '%$s%' OR email like '%$s%' OR position_number LIKE '%$s%') AND";
	else
		$searchextra = '';

	$qr = "SELECT * FROM customers_tour WHERE id > 0 AND $showdeleted  $extraSelect  $searchextra  status > 2 AND from_date >= '$_POST[fdate]' AND from_date <= '$_POST[tdate]' ORDER BY added DESC";
}
elseif($s <> '')
{
/*	if($_POST[showinactive] == 'on')
	{
		$showdeleted = 'inactive = 1';
	}
	else
	{
		$showdeleted = 'inactive = 0';
	}
*/
	$showdeleted = 'id > 0';
	
	$qr = "SELECT * FROM customers_tour WHERE id > 0 AND $showdeleted  $extraSelect AND (offer_id like '%$s%' OR name like '%$s%' OR invoice_name like '%$s%' OR comment like '%$s%' OR email like '%$s%' OR position_number LIKE '%$s%') ORDER BY added DESC";
}
elseif($_GET[mybookings] == 1 || $_GET[agent_id] > 0)
{
	if($_GET[agent_id] > 0)
		$agent_id = $_GET[agent_id];
	else
		$agent_id = $CURUSER[pid];
		
	$qr = "SELECT * FROM customers_tour WHERE inactive = 0 AND agent_id = $agent_id  $clist ORDER BY status, added ASC";
}
else
{

	if($_GET[status] == "5")
		$extravoucher = "AND customers_tour.from_date > NOW()";
		
	if($_GET[status] >= 3 && $CURUSER[passengers_admin] == 1)
	{
		$ord = "customers_tour.last_status_change DESC";
	}
	elseif($_GET[status] > 3)
		$ord = "customers_tour.from_date ASC";
	else
		$ord = "customers_tour.added DESC";
	
	if($_GET[inactive] == 1)
		$elink = "customers_tour.inactive = 1 ORDER BY customers_tour.id DESC LIMIT 200";	 
	else
		$elink = " customers_tour.inactive = 0 AND customers_tour.status = $status  $extraSelect  $extravoucher $clist ORDER BY $ord ";

		
	$qr = "SELECT customers_tour.*, partners.company_name FROM customers_tour LEFT join partners ON partners.pid = customers_tour.agent_id WHERE  $eoffice $elink ";
	
	//echo $qr;
}	

	$query = $mysql->query($qr);


if(mysql_num_rows($query) <= 0)
{
	echo "<div class='redmessage'>$lang[no_results]</div>";
	foot();
	die;
}	


	if($_POST[giveme] == 1)
	{
		echo message("Sikeresen kért egy utast!");
	}
	
	$ck = mysql_fetch_assoc($mysql->query("SELECT count(id) as cnt from customers_tour WHERE agent_id = 0 AND inactive = 0"));
	
	if(strtotime($CURUSER[last_offer_request]) < strtotime("-10 minutes") && $ck[cnt] > 0) {
		echo "<div style='text-align:center;'><form method='post'>
			<input type='hidden' name='giveme' value='1'/>
			<input type='submit' value='Kérek egy utast ($ck[cnt] db van függőben) &raquo;'/>
		</form></div><hr/>";
	}
	else
	{
			$now = time();
			$left = round(((strtotime($CURUSER[last_offer_request]))-$now)/60/60);
						
		
				
			if($left < 0)
				$left = 0;
			echo message("Új utas kéréséhez várj még néhány percet, vagy jelezd Matyinak ($ck[cnt] db függőben)");
	}


echo "<table class=\"general\" width='100%'>";
echo "<tr class=\"header\">";

	echo "<td width='20' colspan='3'>-</td>";
	echo "<td width='150'>$lang[customer_name]</td>";
	echo "<td>$lang[partner]</td>";
	echo "<td>Utazás</td>";
	//echo "<td>Fizetés</td>";
	echo "<td width='60'>Fizetve/hátralék</td>";
	echo "<td width='60'>Állapot</td>";

	//echo "<td width='60'>Eszközök</td>";


echo "</tr>";




$z = 1;
while($arr = mysql_fetch_assoc($query)) {
	
	//$rowdata = '';
	
	$partner = mysql_fetch_assoc($mysql->query("SELECT foreign_custom_invoice, hotel_name, address FROM partners WHERE coredb_id = $arr[partner_id]"));
	
	$paid = mysql_fetch_assoc($mysql->query("SELECT SUM(value) AS paid FROM customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 0"));

	if($arr[payment] == 1) 
		$payment = $lang[transfer];
	elseif($arr[payment] == 2) 
		$payment = $lang[cash];
	elseif($arr[payment] == 5) 
		$payment = $lang[t_check];
	elseif($arr[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($arr[payment] == 10) 
		$payment = "OTP SZÉP kártya";
	elseif($arr[payment] == 11) 
		$payment = "MKB SZÉP kártya";
	elseif($arr[payment] == 12) 
		$payment = "K&H SZÉP kártya";
	
		
		
		//	$who = mysql_fetch_assoc($mysql->query("SELECT company_name FROM partners WHERE pid = $arr[agent_id]"));
		$who = end(explode(" ",$arr[company_name]));
		
	
	if($who == '' && $_GET[mybookings] <> 1)
		$color = 'pink';
	elseif($arr[inactive] == 1)
		$color= 'red';
	elseif($arr[status] == 1)
		$color= 'lorange';
	elseif($arr[status] == 2)
		$color= 'blue';
	elseif($arr[status] == 3)
		$color= 'dpink';
	elseif($arr[status] == 4)
		$color= 'green';
	elseif($arr[status] == 5)
		$color= 'orange';
	else
		$color = '';
		
		

			if($CURUSER[pid] == $arr[agent_id])
				$cpls = 'red';
			else
				$cpls = '';
				
			if($arr[inactive] == 1)
			{
				$delbtn = '';
				$dreason = "<br/><b>[".$delreasons[$arr[donotwant]]."]</b>";
			}
			else
			{
				$delbtn = "	<a href=\"?delete=$arr[id]&editid=$arr[cid]&year=$year&month=$month\" id=\"$arr[cid]\" rel='facebox iframe'><b><img src='images/trash.png' alt='töröl' title='töröl' width='20'/></b></a>";
				$dreason = "";

			}
				
	$rowdata.= "<tr class='$color'><form method='POST'>";
		$rowdata.= "<td width='20' align='center'><img src='/images/icons/$arr[source].png' width='20' alt='$arr[source]' title='$arr[source]'/><br/>$z.</td><td width='20'><input type='hidden' name='id' value='$arr[id]'/><a href=\"?add=1&editid=$arr[id]&year=$year&month=$month\" id=\"$arr[cid]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='20'/></b></a>
		
		<a href=\"?copy=1&id=$arr[id]\" id=\"$arr[cid]\" rel='facebox'><img src='images/copy.png' alt='másol' title='másol' width='20'/></b></a>


			$delbtn</td>";

		
		
		$persons = 0;
		for($pcount = 1; $pcount <= 10; $pcount++)
		{
			if($arr["passenger".$pcount."_name"] <> '')
				$persons++;
		}
		
		$totalperons = $totalperons + $persons;

		$rowdata.= "<td>".str_replace(array("IND-2012/","IND-2013/","IND-2014/",'PH-2014/','PH-2013/','IND-2015/'),array('','','','','',''),$arr[offer_id])."<br/><span class='$cpls'>$who</span><br/>$persons fő</td>";
		$rowdata.= "<td width='150'>$arr[name] $dreason<br/><span style='font-size:10px;'>($arr[added])</span></td>";
		$rowdata.= "<td><a href='$arr[offer_url]' target='_blank'><b>$partner[hotel_name]</b><br/>$arr[offer_name] <br/><span style='font-size:11px'>($arr[destination])</span></a></td>";
		
		
		if (strtotime($arr[from_date]) <= time())
		{
			$rowdata.= "<td align='center' width='80'><a href='#' title='Elment utas!'>$arr[from_date]<br/>$arr[to_date]</td>";
		}
		elseif (strtotime($arr[from_date]) <= strtotime('+7 days'))
		{
			
			$rowdata.= "<td align='center' width='80'><div style='background-color:#E56E94;'><a href='#' title='1 héten belüli indulás!'>$arr[from_date]<br/>$arr[to_date]</a></td>";
		
		}
		else
			$rowdata.= "<td align='center' width='80'>$arr[from_date]<br/>$arr[to_date]</td>";

		//echo "<td align='center'>$payment</td>";
		
		
		$left = $arr[total]-$paid[paid]-$arr[voucher_value]; 


		if($arr[invoice_number] <> '' )
			$yield ="<hr/><b><a href='/invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf' target='_blank' >jutalék &raquo;</a></b>";
		elseif($partner[foreign_custom_invoice] == 1) //disable creating invoice for certain passengers
			$yield = '';
		elseif($arr['yield'] > 0 && $arr[invoice_number] == '' && $left <= 0)
		{
			if($partner[address] == '' || $partner[company_name] == '')
				$yield = "<hr/><b><a href='/partners.php?edit=$partner[pid]' alt='Partner adatait kitölteni!' title='Partner adatait kitölteni!'><font color='red'>!!! ADAT !!!</font></a></b>";
			else
				$yield ="<hr/>".formatPrice($arr['yield'])."<input type='button' value='Számláz' onclick=\"jQuery.facebox({ ajax: '/invoice/create_invoice_yield_tour.php?id=$arr[id]' });\"/>";
		}
		else
		{
			$yield = '';
		}
		
		if($arr[tour_category] <> 'package' && $arr[tour_category] <> 'insurance' && $arr[status] >= 4)
		{
			if($left <= 0 && $arr[user_invoice_number] == '')
			{
				$yield ="<hr/>".formatPrice($arr['yield'])."<input type='button' value='Utasszámla' onclick=\"jQuery.facebox({ ajax: '/invoice/create_invoice_customer_tour.php?id=$arr[id]' });\"/>";
			}
			elseif($arr[user_invoice_number] <> '')
			{
			$yield ="<hr/><b><a href='/invoices/vatera/".str_replace("/","_",$arr[user_invoice_number]).".pdf' target='_blank' >utasszámla &raquo;</a></b>";

			}
		}
		
		if(($arr[total]-$paid[paid]-$arr[voucher_value]) <= 0)
{
	$cls = 'green';
	$statuses = array("Ajánlat",'Lemondás','Megrendelőre vár','Visszaigazolásra vár','Visszaigazolva','Voucher kiküldve');

}
else
{
	$cls = 'red';
	$statuses = array("Ajánlat",'Lemondás','Megrendelőre vár','Visszaigazolásra vár','Visszaigazolva');
}	


		$rowdata.= "<td width='90' align='right' >
			<a href='/passenger_payments.php?id=$arr[id]' target='_blank'>".formatPrice($paid[paid])."<br/>
			<font color='$cls'><b>".formatPrice($left)."</b></font></a>
			$yield
			
			</td>";
			
			if($arr[is_option] == 1)
				$cls = 'dpink';
			else
				$cls = '';
		$rowdata.= "<td width='60' align='center' class='$cls'>
	<select name='status' onchange='submit()'>";
		
		



			foreach($statuses as $key => $status)
			{
				if($arr[status] == $key)
					$selected = 'selected';
				else
					$selected = '';
				$rowdata.= "<option value='$key' $selected>$status</option>";
			}
	$rowdata.= "</select></td>";
		

		
	if($arr[status] >= 4)
		$ticket = "<a href='/vouchers/print_ticket.php?id=$arr[id]&print=1'>[voucher]</a>";
	else
		$ticket = '';
	
	$t = $t + $arr[total];
	$tleft = $tleft + $left;
	
	$leftvalue = $leftvalue + ($left);


	$t2 = $t2 + $arr[total]-$arr[voucher_value];
	$leftvalue2 = $leftvalue2 + ($left- $arr[voucher_value]);

	
	$rowdata.= "</form></tr>";
	
	$z++;
}

if($CURUSER[passengers_admin] == 1)
{
	
	echo "<tr class='header'>";
		echo "<td colspan='6' align='right' valign='top'>Fő:<br/>Összesen:<br/><br/>Hátralék:</td><td align='right'>$totalperons fő<br/>".formatPrice($t)."<br/>".formatPrice($t2)."<br/>".formatPrice($leftvalue)."<br/>".formatPrice($leftvalue2)."</td><td></td>";
	echo "</tr>";
}

echo $rowdata;

echo "</table>";

?>
</div></div>
<?
foot();
?>