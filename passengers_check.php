<?
/*
 * customers.php 
 *
 * customers_tour page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();


if($CURUSER[userclass] < 50)
	header("location: index.php");


if($_GET[company_invoice] == 'horizonttravel')
	$company_invoice = 'horizonttravel';
else
	$company_invoice = 'szallasoutlet';
	
if($_GET[agent_id] > 0)
		$eagent = "AND agent_id = $_GET[agent_id]";
		
if($_POST[id] > 0 && $_GET[checkout] <> 1)
{
	
	$mysql->query("UPDATE customers_tour SET checked = 1, last_checked_date = NOW() WHERE id = $_POST[id]");	
	writelog("$CURUSER[username] checked IND_ID: $_POST[offer_id]");
	$msg = "Sikeresen ellenőrizte az utast!";
}

if($_POST[id] > 0 && $_GET[checkout] == 1)
{
	
	$mysql->query("UPDATE customers_tour_payment SET checked = 1, checked_date = NOW() WHERE id = $_POST[id]");	
	writelog("$CURUSER[username] set payment checked IND_ID: $_POST[offer_id]");
	$msg = "Sikeresen ellenőrizte az utast!";
}

head("Utasok ellenőrzése");
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
	
		<li><a href="passengers.php">Minden utas</a></li>
			<li><a href="passengers_check.php"  class='<? if($_GET[checkout] <> 1) echo "current";?>'>Utasok ellenőrzése</a></li>
			<li><a href="passengers_check.php?checkout=1" class='<? if($_GET[checkout] == 1) echo "current";?>'>Pénzmozgások ellenőrzése</a></li>


		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<?=message($msg)?>


<? if($_GET[checkout] == 1 )
{

$payments = array(
	2 => "készpénz",
	9 => "bankkártya",
	1 => "átutalás",
);
?>


<fieldset>
	<form method='get'>
		<input type='hidden' name='checkout' value='<?=$_GET[checkout]?>'/>
	
	<select name='company_invoice'>
		<?
			
		echo "<option value='szallasoutlet'>Szállás Outlet Kft.</option>";
		
		if($company_invoice == 'horizonttravel')
			$selected = 'selected';
		echo "<option value='horizonttravel' $selected>Horizont Travel Kft.</option>";

		?>
					</select>
					
		
		<select name='agent_id'>
					<option value='0'>Válasszon</option>
					<?
			$partnerQuery = $mysql->query("SELECT 'company_name','pid' FROM partners WHERE passengers = 1 ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($_GET[agent_id] == $partnerArr[pid])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
	
	<input type='submit' value='szűrés'/>
	</form>
	<a href='?all=1&agent_id=<?=$_GET[agent_id]?>&company_invoice=<?=$_GET[company_invoice]?>'>Minden tételt mutat Hello</a>
</fieldset>
<hr/>

<?
echo "<table>";

$query = $mysql->query("SELECT customers_tour_payment.checked,customers_tour_payment.id as cid, customers_tour_payment.checkout_id, customers_tour.offer_id, customers_tour.id, customers_tour.agent_id,  customers_tour.name, customers_tour_payment.payment , customers_tour_payment.value, customers_tour_payment.added,customers_tour.name,customers_tour_payment.username, customers_tour_payment.is_transfer FROM customers_tour_payment INNER JOIN customers_tour ON customers_tour.id = customers_tour_payment.customer_id WHERE customers_tour.company_invoice = '$company_invoice' AND customers_tour_payment.payment = 9 ORDER BY customers_tour_payment.checked, customers_tour_payment.added ASC LIMIT 0,50"); // customers_tour_payment.added

$i = 1;
while($arr = mysql_fetch_assoc($query))
{

	if($arr[payment] == 0)
		$arr[payment] = 1;
		
	$agent = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[agent_id]"));
	$agent = end(explode(" ",$agent[company_name]));
	
	$added = explode(" ",$arr[added]);
	
	if($arr[is_transfer] == 1)
	{
		$class='blue';
		$arr[value] = $arr[value]*-1;
		$ptotalout[$arr[payment]] = $ptotalout[$arr[payment]]+$arr[value];
		
	}
	else
	{
		$class = '';
		$ptotalin[$arr[payment]] = $ptotalin[$arr[payment]]+$arr[value];
	}
		
	if($arr[checkout_id] > 0)
		$class = 'green';
	else
		$class = $class;
		

		
	if($arr[checked] == 1)
	{
		$cls = 'green';
		$cbtn = "<img src='/images/check1.png' width='20'/>";
	}
	else
	{
		$cls = '';
		$cbtn = "<input type='submit' value='ok'/>";
	}
	echo "<form method='post' action='/passengers_check.php?checkout=1'><input type='hidden' name='id' value='$arr[cid]'><input type='hidden' name='offer_id' value='$arr[offer_id]'><tr class='$class'>";
		echo "<td width='20'>$i</td>";
		echo "<td>$added[0]</td>";
		echo "<td><a href='/passengers.php?add=1&editid=$arr[id]' target='_blank'><b>$arr[offer_id]</b></a></td>";
		echo "<td>$arr[name]</td>";
		echo "<td>$agent</td>";
		echo "<td>$arr[username]</td>";
		echo "<td>".$payments[$arr[payment]]."</td>";
		echo "<td align='right'>".formatPrice($arr[value])."</td>";
		echo "<td align='center' width='20' class='$cls'>$cbtn</td>";
	echo "</tr></form>";
	
/*

	if($arr[payment] == 2 && $arr[is_transfer] == 0)
	{
		
		$ccode = end(explode("/",$arr[offer_id]));
//echo "SELECT * FROM checkout_vtl WHERE (replace(comment, ' ', '') like '%$arr[offer_id]%' OR replace(comment, ' ', '') like '%$ccode%') AND value = $arr[value] LIMIT 1 <hr/>";
		$checkout = $mysql->query("SELECT * FROM checkout_vtl WHERE (replace(comment, ' ', '') like '%$arr[offer_id]%' OR replace(comment, ' ', '') like '%$ccode%') AND value = $arr[value] ");
		while($a = mysql_fetch_assoc($checkout)) 
		{
			
			echo "UPDATE customers_tour_payment SET checkout_id = 1 WHERE id = $arr[cid] AND checkout_id = 0; #1<br/>";
			echo "<tr class='header'>";
				echo "<td colspan='7'>$a[comment]</td>";
				echo "<td align='right'>".formatPrice($a[value])."</td>";
			echo "</tr>";
		}
	}
	
	}
*/
	
		$i++;

	$total = $total + $arr[value];
	
	
	
}
echo "<tr class='header'>";
	echo "<td colspan='7'></td>";
	echo "<td align='right'>".formatPrice($total)."</td>";
echo "</tr>";
echo "</table><hr/>";

echo "<table>";

echo "<tr class='header'>";
		echo "<td>Fizetési mód</td>";
		echo "<td>Befizetett</td>";
		echo "<td>Kifizetett</td>";
		echo "<td>Egyenleg</td>";
		
	echo "</tr>";
	
foreach($payments as $key => $value)
{
	echo "<tr>";
		echo "<td>$value</td>";
		echo "<td align='right'>".formatPrice($ptotalin[$key])."</td>";
		echo "<td align='right'>".formatPrice($ptotalout[$key])."</td>";
		echo "<td align='right'>".formatPrice($ptotalin[$key]+$ptotalout[$key])."</td>";
		
	echo "</tr>";
}

echo "</table>";

}
else { ?>




<fieldset>
	<form method='get'>
		<input type='hidden' name='checkout' value='<?=$_GET[checkout]?>'/>
		<select name='company_invoice'>
		<?
			
		echo "<option value='szallasoutlet'>Szállás Outlet Kft.</option>";
		
		if($company_invoice == 'horizonttravel')
			$selected = 'selected';
		echo "<option value='horizonttravel' $selected>Horizont Travel Kft.</option>";

		?>
					</select>
		<select name='agent_id'>
					<option value='0'>Válasszon</option>
					<?
			$partnerQuery = $mysql->query("SELECT * FROM partners WHERE passengers = 1 ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($_GET[agent_id] == $partnerArr[pid])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
		<select name='company_pid'>
					<option value='0'>Válasszon</option>
					<?
			$partnerQuery = $mysql->query("SELECT `company_name`,`pid` FROM partners WHERE userclass = 3 GROUP BY `company_name` ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($_GET[company_pid] == $partnerArr[pid])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
	<input type='submit' value='szűrés'/>
	</form>
	<a href='?all=1&agent_id=<?=$_GET[agent_id]?>&company_invoice=<?=$_GET[company_invoice]?>'>Minden tételt mutat</a>
</fieldset>
<hr/>


<table>
<?

	
		
	if($_GET[all] == 1)
		$extra = '';
	else
		$extra = 'AND inactive = 0 AND status > 3';
		
	echo "<tr class='header'>";
		echo "<td colspan='3'>ID</td>";
		echo "<td>Létreh.</td>";
		echo "<td>Ind.</td>";
		echo "<td>Név</td>";
		echo "<td>Értékesítő</td>";
		echo "<td>Fizetendő</td>";
		echo "<td>Visszaigazolt</td>";
		echo "<td>Befizetett</td>";
		//echo "<td class='orange'>Készpénz</td>";
		echo "<td>Voucher</td>";
		echo "<td>Partnernek</td>";

		echo "<td>Jutalék</td>";
		echo "<td>File</td>";
		echo "<td>Ok</td>";

	echo "</tr>";

		
	$i = 1;
	$sql = "SELECT * FROM customers_tour ORDER BY last_checked_date, has_file, tour_category ASC";
	
	
	$query = $mysql->query("SELECT * FROM customers_tour WHERE company_invoice = '$company_invoice' $eagent $extra ORDER BY last_checked_date, has_file, tour_category ASC");
	
	//debug($sql); //die();
	//$query = $mysql->query($sql); 
	while($arr = mysql_fetch_assoc($query))
	{
		
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE coredb_id = $arr[partner_id] LIMIT 1"));

		$agent = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[agent_id] LIMIT 1"));
		$agent = end(explode(" ",$agent[company_name]));
		
		if($arr[inactive] == 1)
		{
			$deltext = '(törölt)';
			$class = 'grey';
		}
		elseif($arr[status] < 3)
		{
			$deltext = '';
			$class = 'blue';
		}	
		else
		{
			$deltext = '';
			$class = '';
		}
		
		$sql = "SELECT sum(value) as paid, count(id) as cnt FROM customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 0";
		//debug($sql);
		$paidtotal = mysql_fetch_assoc($mysql->query($sql));
    $transfertotal = mysql_fetch_assoc($mysql->query("SELECT sum(value) as paid FROM customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 1"));
	$cashtotal = mysql_fetch_assoc($mysql->query("SELECT sum(value) as paid, count(id) as cnt FROM customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 0 AND payment = 2"));

	$added = explode(" ",$arr[added]);
	$added = $added[0];
		
	if($arr[from_date] == '0000-00-00')	
		$arr[from_date] = '';
		
		echo "<form method='post'><input type='hidden' name='id' value='$arr[id]'><input type='hidden' name='offer_id' value='$arr[offer_id]'><tr class='$class'>";
			echo "<td>$i</td>";
			echo "<td width='20'><a href='/passengers.php?add=1&editid=$arr[id]' target='_blank'><img src='/images/edit.png' width='20'/></a></td>";
			echo "<td>$arr[tour_category]</td>";
			echo "<td>$added</td>";
			echo "<td>$arr[from_date]</td>";
			echo "<td>$arr[name] $deltext<br/><span class='small'>$arr[offer_id]</span></td>";
			echo "<td>$partner[company_name]<br/>$agent</td>";
			echo "<td align='right'>".formatPrice($arr[total])."</td>";
			echo "<td align='right'>".formatPrice($arr[final_total])."</td>";
			echo "<td align='right'><a href='/passenger_payments.php?id=$arr[id]' target='_blank'><b>".formatPrice($paidtotal[paid])."</b></td>";
		//	echo "<td align='right' class='orange'>".formatPrice($cashtotal[paid])."</td>";
			echo "<td align='right'>".formatPrice($arr[voucher_value])."</td>";
			echo "<td align='right'>".formatPrice($transfertotal[paid])."</td>";

			echo "<td align='right'>".formatPrice($arr['yield'])."</td>";
			
			if($arr[has_file] == 1)
			{
				$check = "<a href='/passengers.php?add=1&editid=$arr[id]#upload' target='_blank'><font color='red'>!!!</font></a>";
				$inp = "<input type='submit' value='OK'/>";
			}
			else
			{
				$check = '';
				$inp = '';
			}
			
			if($arr[checked] == 1)
			{
				$cls = 'green';
				$inp = "<img src='/images/check1.png' width='20'/>";
			}
			else
			$cls = '';
				
			echo "<td align='center'>$check</td>";
			echo "<td align='center' class='$cls'>$inp</td>";

		echo "</tr></form>";
		
		$totalcash = $totalcash + $cashtotal[paid];
		$i++;
		
	}
	
	/*
	echo "<tr class='$class'>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td align='right'></td>";
			echo "<td align='right'></td>";
			echo "<td align='right'></td>";
			//echo "<td align='right'>".formatPrice($totalcash)."</td>";
			echo "<td align='right'></td>";
			echo "<td align='right' colspan='3'></td>";

		echo "</tr>";
		*/
?>
</table>
</div>
<? } ?>

</div></div>
<?
foot();

?>