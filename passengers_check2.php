<?
/*
 * customers.php 
 *
 * customers_tour page
 *
*/
error_reporting(0);
ini_set('display_errors', 'off');

/* bootstrap file */
$noheader = 'no';
include("inc/init.inc.php");

userlogin();
if($CURUSER[userclass] < 50)
    header("location: index.php");


if($_GET[company_invoice] == 'horizonttravel')
    $company_invoice = 'horizonttravel';
else
    $company_invoice = 'szallasoutlet';

if($_GET[agent_id] > 0)
    $eagent = "AND agent_id = $_GET[agent_id]";

    if($_POST[id] > 0 && $_GET[checkout] <> 1)
    {

        $mysql->query("UPDATE customers_tour SET checked = 1, last_checked_date = NOW() WHERE id = $_POST[id]");
        writelog("$CURUSER[username] checked IND_ID: $_POST[offer_id]");
        $msg = "Sikeresen ellenőrizte az utast!";
    }

    if($_POST[id] > 0 && $_GET[checkout] == 1)
    {

        $mysql->query("UPDATE customers_tour_payment SET checked = 1, checked_date = NOW() WHERE id = $_POST[id]");
        writelog("$CURUSER[username] set payment checked IND_ID: $_POST[offer_id]");
        $msg = "Sikeresen ellenőrizte az utast!";
    }
if(empty($_GET[datefrom])) { $_GET[datefrom] = date('Y-m-d',strtotime(date('Y-01-01'))); }
if($_GET[all] == 1)
    $extra = '';
else
    $extra = 'AND customers_tour.inactive = 0 AND customers_tour.status > 3';
    debug($_GET[company_pid]);
if(!isset($_GET[company_pid]))
    $partner = '';
else {
    debug(implode(',',$_GET[company_pid]));
    if(is_array($_GET[company_pid])) {
        $partner = 'AND customers_tour.partner_id IN (' . implode(',',$_GET[company_pid]) . ') ';
    }
    else {
        $partner = 'AND customers_tour.partner_id = ' . $_GET[company_pid];
    }
}

$date = "";
if(!empty($_GET[datefrom]))
    $date  .= "AND (customers_tour.from_date >= '$_GET[datefrom]') ";
if(!empty($_GET[dateto]))
    $date  .= "AND (customers_tour.from_date <= '$_GET[dateto]') ";

if(empty($_GET[items]))
    $_GET[items] = 50;

$limit = "LIMIT 0,".$_GET[items];

if($_GET[items] == 'all' or $_GET['export'] == '1') { $limit = ""; }
            
$sql = "SELECT
customers_tour.id,
customers_tour.tour_category,
partners.company_name,
agent.company_name as agent,
customers_tour.name,
DATE(customers_tour.added) as added,
customers_tour.from_date,
customers_tour.last_checked_date,
customers_tour.offer_id,
customers_tour.total,
customers_tour.final_total,
customers_tour.voucher_value,
customers_tour.yield,
customers_tour.has_file,
customers_tour.checked,
customers_tour.inactive,
customers_tour.status,

SUM(IF(customers_tour_payment.is_transfer = '0', customers_tour_payment.value, 0)) as totalpaid,
SUM(IF(customers_tour_payment.is_transfer = '1', customers_tour_payment.value, 0)) as transfertotal,
SUM(IF(customers_tour_payment.is_transfer = '0' AND customers_tour_payment.payment = '2', customers_tour_payment.value, 0)) as cashtotalpaid
FROM
customers_tour
LEFT JOIN
partners ON customers_tour.partner_id = partners.coredb_id
LEFT JOIN
partners as agent ON agent.pid = customers_tour.agent_id
LEFT JOIN
customers_tour_payment  ON customers_tour.id = customers_tour_payment.customer_id
WHERE customers_tour.company_invoice = '$company_invoice'
$eagent
$extra
$partner
$date
GROUP BY customers_tour.id
ORDER BY last_checked_date , has_file , tour_category ASC
$limit
";
//debug($sql); die();

$query = $mysql->query($sql);
if($_GET['export'] == '1') {
    $path = realpath('lib/PHPExcel') . '/PHPExcel.php';
    include_once($path);
    $doc = new PHPExcel();
    $doc->setActiveSheetIndex(0);

    $rowCount = 1;

    while($row = mysql_fetch_array($query)){
        $result[] = array(
            $row[0], //id
            $row[1], //category
            $row[5], //added
            $row[6], //from
            $row[4] . ' ' . $row[8], //name
            $row[2] . ' ' . $row[3], //company, agent
            $row[9], //fizetendő total
            $row[10], //visszaigazolt final_total
            $row[17], //befizetett paidtotal
            $row[11], //voucher voucher
            $row[18], //partnerek 
            $row[12]  //jutalék yield
        );
    }

    $header = array(
        'id',
        'kategória',
        'Létreh.',	
        'Ind.',
        'Név',
        'Értékesítő',
        'Fizetendő',
        'Visszaigazolt',
        'Befizetett',
        'Voucher',
        'Partnernek',
        'Jutalék'
    );

    $doc->getActiveSheet()  ->setTitle("checklist")
    ->fromArray($header, null, 'A1')
    ->fromArray($result, null, 'A2');

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="checklist-'.date('Ymdhis').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}     




head("Utasok ellenőrzése",0,0,'',1);
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
	
		<li><a href="passengers.php">Minden utas</a></li>
			<li><a href="passengers_check.php"  class='<? if($_GET[checkout] <> 1) echo "current";?>'>Utasok ellenőrzése</a></li>
			<li><a href="passengers_check.php?checkout=1" class='<? if($_GET[checkout] == 1) echo "current";?>'>Pénzmozgások ellenőrzése</a></li>


		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<?=message($msg)?>


<? if($_GET[checkout] == 1 )
{

$payments = array(
	2 => "készpénz",
	9 => "bankkártya",
	1 => "átutalás",
);
?>


<fieldset>
	<form method='get'>
		<input type='hidden' name='checkout' value='<?=$_GET[checkout]?>'/>
	
	<select name='company_invoice'>
		<?
			
		echo "<option value='szallasoutlet'>Szállás Outlet Kft.</option>";
		
		if($company_invoice == 'horizonttravel')
			$selected = 'selected';
		echo "<option value='horizonttravel' $selected>Horizont Travel Kft.</option>";

		?>
					</select>
					
		
		<select name='agent_id'>
					<option value='0'>Válasszon</option>
					<?
			$partnerQuery = $mysql->query("SELECT 'company_name','pid' FROM partners WHERE passengers = 1 ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($_GET[agent_id] == $partnerArr[pid])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
	
	<input type='submit' value='szűrés'/>
	</form>
	<a href='?all=1&agent_id=<?=$_GET[agent_id]?>&company_invoice=<?=$_GET[company_invoice]?>'>Minden tételt mutat Hello</a>
</fieldset>
<hr/>

<?
echo "<table>";

$query = $mysql->query("SELECT customers_tour_payment.checked,customers_tour_payment.id as cid, customers_tour_payment.checkout_id, customers_tour.offer_id, customers_tour.id, customers_tour.agent_id,  customers_tour.name, customers_tour_payment.payment , customers_tour_payment.value, customers_tour_payment.added,customers_tour.name,customers_tour_payment.username, customers_tour_payment.is_transfer FROM customers_tour_payment INNER JOIN customers_tour ON customers_tour.id = customers_tour_payment.customer_id WHERE customers_tour.company_invoice = '$company_invoice' AND customers_tour_payment.payment = 9 ORDER BY customers_tour_payment.checked, customers_tour_payment.added ASC LIMIT 0,50"); // customers_tour_payment.added

$i = 1;
while($arr = mysql_fetch_assoc($query))
{

	if($arr[payment] == 0)
		$arr[payment] = 1;
		
	$agent = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[agent_id]"));
	$agent = end(explode(" ",$agent[company_name]));
	
	$added = explode(" ",$arr[added]);
	
	if($arr[is_transfer] == 1)
	{
		$class='blue';
		$arr[value] = $arr[value]*-1;
		$ptotalout[$arr[payment]] = $ptotalout[$arr[payment]]+$arr[value];
		
	}
	else
	{
		$class = '';
		$ptotalin[$arr[payment]] = $ptotalin[$arr[payment]]+$arr[value];
	}
		
	if($arr[checkout_id] > 0)
		$class = 'green';
	else
		$class = $class;
		

		
	if($arr[checked] == 1)
	{
		$cls = 'green';
		$cbtn = "<img src='/images/check1.png' width='20'/>";
	}
	else
	{
		$cls = '';
		$cbtn = "<input type='submit' value='ok'/>";
	}
	echo "<form method='post' action='/passengers_check.php?checkout=1#tr-$arr[cid]'><input type='hidden' name='id' value='$arr[cid]'><input type='hidden' name='offer_id' value='$arr[offer_id]'><tr class='$class' id='tr-$arr[cid]'>";
		echo "<td width='20'>$i</td>";
		echo "<td>$added[0]</td>";
		echo "<td><a href='/passengers.php?add=1&editid=$arr[id]' target='_blank'><b>$arr[offer_id]</b></a></td>";
		echo "<td>$arr[name]</td>";
		echo "<td>$agent</td>";
		echo "<td>$arr[username]</td>";
		echo "<td>".$payments[$arr[payment]]."</td>";
		echo "<td align='right'>".formatPrice($arr[value])."</td>";
		echo "<td align='center' width='20' class='$cls'>$cbtn</td>";
	echo "</tr></form>";
	
/*

	if($arr[payment] == 2 && $arr[is_transfer] == 0)
	{
		
		$ccode = end(explode("/",$arr[offer_id]));
//echo "SELECT * FROM checkout_vtl WHERE (replace(comment, ' ', '') like '%$arr[offer_id]%' OR replace(comment, ' ', '') like '%$ccode%') AND value = $arr[value] LIMIT 1 <hr/>";
		$checkout = $mysql->query("SELECT * FROM checkout_vtl WHERE (replace(comment, ' ', '') like '%$arr[offer_id]%' OR replace(comment, ' ', '') like '%$ccode%') AND value = $arr[value] ");
		while($a = mysql_fetch_assoc($checkout)) 
		{
			
			echo "UPDATE customers_tour_payment SET checkout_id = 1 WHERE id = $arr[cid] AND checkout_id = 0; #1<br/>";
			echo "<tr class='header'>";
				echo "<td colspan='7'>$a[comment]</td>";
				echo "<td align='right'>".formatPrice($a[value])."</td>";
			echo "</tr>";
		}
	}
	
	}
*/
	
		$i++;

	$total = $total + $arr[value];
	
	
	
}
echo "<tr class='header'>";
	echo "<td colspan='7'></td>";
	echo "<td align='right'>".formatPrice($total)."</td>";
echo "</tr>";
echo "</table><hr/>";

echo "<table>";

echo "<tr class='header'>";
		echo "<td>Fizetési mód</td>";
		echo "<td>Befizetett</td>";
		echo "<td>Kifizetett</td>";
		echo "<td>Egyenleg</td>";
		
	echo "</tr>";
	
foreach($payments as $key => $value)
{
	echo "<tr>";
		echo "<td>$value</td>";
		echo "<td align='right'>".formatPrice($ptotalin[$key])."</td>";
		echo "<td align='right'>".formatPrice($ptotalout[$key])."</td>";
		echo "<td align='right'>".formatPrice($ptotalin[$key]+$ptotalout[$key])."</td>";
		
	echo "</tr>";
}

echo "</table>";

}
else { ?>



<fieldset>
	<form method='get' id="checklist">
		<input type='hidden' name='checkout' value='<?=$_GET[checkout]?>'/>
		<select name='company_invoice'>
		<?
			
		echo "<option value='szallasoutlet'>Szállás Outlet Kft.</option>";
		
		if($company_invoice == 'horizonttravel')
			$selected = 'selected';
		echo "<option value='horizonttravel' $selected>Horizont Travel Kft.</option>";

		?>
					</select>
		<select name='agent_id'>
					<option value='0'>Válasszon</option>
					<?
			$partnerQuery = $mysql->query("SELECT * FROM partners WHERE passengers = 1 ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($_GET[agent_id] == $partnerArr[pid])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
		<select id="multiple_company_pid" multiple="multiple" name='company_pid[]'>
					<option value='0'>Válasszon</option>
					<?
			//$partnerQuery = $mysql->query("SELECT `company_name`,`coredb_id` FROM partners WHERE userclass = 3 GROUP BY `company_name` ORDER BY company_name ASC");
			$partnerQuery = $mysql->query("SELECT `company_name`,`coredb_id` FROM partners WHERE userclass = 3 ORDER BY company_name ASC");
				
			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if(in_array($partnerArr[coredb_id],$_GET[company_pid]))
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[coredb_id]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
	
	
	<input type='text' name='datefrom' class="datepicker"  placeholder="Indulás -tól" value="<?php echo $_GET[datefrom] ?>"/>
	<input type='text' name='dateto' class="datepicker" placeholder="Indulás -ig" value="<?php echo $_GET[dateto] ?>" />
	<select id="items" name='items'>
		<option value='50' <?php if($_GET[items] == '50') print 'seleceted' ?>>50</option>
		<option value='100' <?php if($_GET[items] == '100') print 'seleceted' ?>>100</option>
		<option value='1000' <?php if($_GET[items] == '1000') print 'seleceted' ?>>1000</option>
		<option value='all' <?php if($_GET[items] == 'all') print 'seleceted' ?>>Összes</option>
	</select>
	<input type='submit' value='szűrés'/>
	<input type="hidden" id="exportval" name="export" value="0">
	<input type='button' value='export' onclick="setExport()"/>
	<script>
       function setExport()
       {
          document.getElementById("exportval").value = 1;
          var form = document.getElementById("checklist");
          form.setAttribute("target", "_blank");
          form.submit();
          form.setAttribute("target", "");
       }
    </script>
	
	</form>
	<a href='?all=1&agent_id=<?=$_GET[agent_id]?>&company_invoice=<?=$_GET[company_invoice]?>'>Minden tételt mutat</a>
	</fieldset>
	<script type="text/javascript">
      $(document).ready(function() {
          $('#multiple_company_pid').multiselect({
        	  includeSelectAllOption: true,
        	  enableFiltering: true,
        	  'selectAll': false
              });
          $( ".datepicker" ).datepicker( {format: 'yyyy-mm-dd', language: 'hu'});
      });
	</script>

<hr/>


<table>
<?

	
		
	
		
	echo "<tr class='header'>";
		echo "<td colspan='3'>ID</td>";
		echo "<td>Létreh.</td>";
		echo "<td>Ind.</td>";
		echo "<td>Név</td>";
		echo "<td>Értékesítő</td>";
		echo "<td>Fizetendő</td>";
		echo "<td>Visszaigazolt</td>";
		echo "<td>Befizetett</td>";
		//echo "<td class='orange'>Készpénz</td>";
		echo "<td>Voucher</td>";
		echo "<td>Partnernek</td>";

		echo "<td>Jutalék</td>";
		echo "<td>File</td>";
		echo "<td>Ok</td>";

	echo "</tr>";

		
	$i = 1;
	
	        
	
	while($arr = mysql_fetch_assoc($query))
	{
		//ebug($arr);
		if($arr[inactive] == 1)
		{
		    $deltext = '(törölt)';
		    $class = 'grey';
		}
		elseif($arr[status] < 3)
		{
		    $deltext = '';
		    $class = 'blue';
		}
		else
		{
		    $deltext = '';
		    $class = '';
		}
		if($arr[from_date] == '0000-00-00')	{ $arr[from_date] = ''; }
		echo "<form method='post'><input type='hidden' name='id' value='$arr[id]'><input type='hidden' name='offer_id' value='$arr[offer_id]'><tr class='$class'>";
		echo "<td>$i</td>";
		echo "<td width='20'><a href='/passengers.php?add=1&editid=$arr[id]' target='_blank'><img src='/images/edit.png' width='20'/></a></td>";
		echo "<td>$arr[tour_category]</td>";
		echo "<td>$arr[added]</td>";
		echo "<td>$arr[from_date]</td>";
		echo "<td>$arr[name] $deltext<br/><span class='small'>$arr[offer_id]</span></td>";
		echo "<td>$arr[company_name]<br/>$arr[agent]</td>";
		echo "<td align='right'>".formatPrice($arr[total])."</td>";
		echo "<td align='right'>".formatPrice($arr[final_total])."</td>";
		echo "<td align='right'><a href='/passenger_payments.php?id=$arr[id]' target='_blank'><b>".formatPrice($arr[paidtotal])."</b></td>";
		//	echo "<td align='right' class='orange'>".formatPrice($cashtotal[paid])."</td>";
		echo "<td align='right'>".formatPrice($arr[voucher_value])."</td>";
		echo "<td align='right'>".formatPrice($arr[transfertotal])."</td>";
		
		echo "<td align='right'>".formatPrice($arr['yield'])."</td>";
		if($arr[has_file] == 1)
		{
		    $check = "<a href='/passengers.php?add=1&editid=$arr[id]#upload' target='_blank'><font color='red'>!!!</font></a>";
		    $inp = "<input type='submit' value='OK'/>";
		}
		else
		{
		    $check = '';
		    $inp = '';
		}
		if($arr[checked] == 1)
		{
		    $cls = 'green';
		    $inp = "<img src='/images/check1.png' width='20'/>";
		}
		else {
		    $cls = '';
		}
	    echo "<td align='center'>$check</td>";
	    echo "<td align='center' class='$cls'>$inp</td>";
	    echo "</tr></form>";
	    
	    $totalcash = $totalcash + $arr[cashtotalpaid];
	    $i++;
	}
		
		
	
?>
</table>
</div>
<? } ?>

</div></div>
<?
foot();

?>

