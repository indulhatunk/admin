<?
/*
 * customers.php 
 *
 * customers_tour page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
userlogin();

if($CURUSER[passengers_admin] <> 1)
	header("Location: /");
	
if($_GET[download] <> 1)
	head("Utasok ellenőrzése");



if($_GET[company_invoice] == 'horizonttravel')
	$company_invoice = 'horizonttravel';
else
	$company_invoice = 'szallasoutlet';
	
$content.="<div class='content-box'>
<div class='content-box-header'>
					<ul class='content-box-tabs'>
	
		<li><a href='passengers.php'>Minden utas</a></li>
			<li><a href='?all=1'  class='current'>Utalások</a></li>
			
			<li><a href='?download=1&from_date=$_GET[from_date]' >Letöltés</a></li>
			<li><a href='?settransfer=1' >Elutalva</a></li>
		</ul>
		<div class='clear'></div>
</div>
<div class='contentpadding'>";

if($_GET[settransfer] == 1)
{
	echo message("Sikeresen beállította az utalást!");
}

$chk = ($_GET[green] == 1) ? 'checked' : '';

if($_GET[company_invoice] == 'horizonttravel')
	$select2 = 'selected';
else
	$select1 = 'selected';
	
$content.="<fieldset><legend>Utalási nap kiválasztása</legend><form method='get'>
<select name='company_invoice'>
	<option value='szallasoutlet' $select1>Szállás Outlet</option>
	<option value='horizonttravel' $select2>Horizont Travel</option>
</select>
<input type='text' class='dpick' value='$_GET[from_date]' name='from_date' autocomplete='off'/>
<input type='checkbox' name='green' value='1' $chk/> Green
<input type='submit' value='Mutat &raquo;'/></form></fieldset><hr/>";
$content.="<table>";


	$content.="<tr class='header'>";
	$content.="<td colspan='9'>Előlegutalások</td>";
	$content.="</tr>";

	$content.="<tr class='header'>";
		$content.="<td colspan='4'>Utas</td>";
		$content.="<td>Előleg határidő</td>";
		$content.="<td>Utalandó</td>";
		$content.="<td>Előleg</td>";
		$content.="<td>Egyenleg</td>";
		$content.="<td>Utalva</td>";

	$content.="</tr>";
	
	
$h = 1;

if($_GET[from_date] <> '')
{
	$from = "= '$_GET[from_date]'";
	$from2 = " = DATE_ADD('$_GET[from_date]',INTERVAL 30 DAY)";
}
else
{
	$from = "<= NOW()";
	$from2 = "<= NOW() + INTERVAL 30 DAY";
}
	

if($_GET[green] == 1)
	$green = "AND partner_id = '1227356'";
else
	$green = "AND partner_id <> '1227356'";

$query = $mysql->query("SELECT * FROM customers_tour WHERE status >= 4 AND checked =1  AND company_invoice = '$company_invoice' AND (partner_deposit_due $from AND partner_deposit_due <>  '0000-00-00') AND partner_deposit_sent = '0000-00-00 00:00:00' AND partner_deposit <> final_total-yield AND tour_category <> 'insurance' $green");

while($arr = mysql_fetch_assoc($query))
{
	$income = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total from customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 0"));
	$outcome = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total from customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 1"));

	$partnerArr = mysql_fetch_assoc($mysql->query("SELECT * from partners WHERE coredb_id = $arr[partner_id]"));

	if($outcome[total] < $arr[partner_deposit])
	{
	
	if(($income[total]-$outcome[total]) < $arr[partner_deposit])
		$class = 'red';
	else
		$class = '';
		
	
	if(($income[total]-$outcome[total]) > $arr[partner_deposit] && $partnerArr[pid] <> 3699)
	{
			$totalinv = str_pad($arr[partner_deposit], 10, "0", STR_PAD_LEFT);
			$inner_id = str_pad($h, 6, "0", STR_PAD_LEFT); 
			$account_no = str_replace("-",'',trim($partnerArr[account_no]));
			$partner_id = str_pad(substr(trim($partnerArr[coredb_id]),0,22), 24, " ", STR_PAD_RIGHT); 
			$partner_name = str_pad(substr(removeSpecialChars(trim($partnerArr[company_name])),0,32), 35, " ", STR_PAD_RIGHT); 
			$partner_address = str_pad(substr(trim(removeSpecialChars($partnerArr[address])),0,32), 35, " ", STR_PAD_RIGHT); 
			$cmnt = str_pad(substr("$arr[name] - $arr[offer_id] - $arr[position_number]",0,65), 70, " ", STR_PAD_RIGHT); 
			
			$sum = $sum+round($arr[partner_deposit]);
			$transactions.= "02".$inner_id."00000000".$totalinv."".$account_no."".$partner_id."".$partner_name."".$partner_address."".$partner_name."".$cmnt."\r\n";
			$h++;		
		
		$trurl = "<a href='?settransfer=1&id=$arr[id]'>[elutalva]</a>";
	}
	else
		$trurl = "";
	
	$totransfer = $arr[partner_deposit];
	$totalbalance = $income[total]-$outcome[total];
	$content.="<tr class='$class'>";
		$content.="<td width='20'><input type='checkbox' name='tottransfer[]' value='$arr[id]'/></td>";
		$content.="<td width='20'>$h</td>";
		$content.="<td width='20'>$arr[offer_id]</td>";
		$content.="<td colspan='2'><a href='/passengers.php?add=1&editid=$arr[id]' target='_blank'>$partnerArr[company_name] <br/>$partnerArr[account_no]</a></td>";
		$content.="<td align='right'>$arr[partner_deposit_due]</td>";
		$content.="<td align='right'>".formatPrice($arr[final_total]-$arr['yield'])."</td>";
		$content.="<td align='right'>".formatPrice($arr[partner_deposit])."</td>";
		$content.="<td align='right'>".formatPrice($income[total]-$outcome[total])."</td>";
		$content.="<td align='right' class='$cls'>$trurl</td>";
	$content.="</tr>";
	
	}

}


	$content.="<tr><td colspan='9'></td></tr><tr class='header'>";
	$content.="<td colspan='9'>Hátralékutalások</td>";
	$content.="</tr>";
	
	
	$content.="<tr class='header'>";
		$content.="<td colspan='3'>Utas</td>";
		$content.="<td>Összes utalandó</td>";
		$content.="<td>Előleg fizetve</td>";
		$content.="<td></td>";
		$content.="<td>Utastól beérkezett</td>";
		$content.="<td>Partner hátralék</td>";
		$content.="<td>Utalás</td>";
	$content.="</tr>";
	
$query = $mysql->query("SELECT * FROM customers_tour WHERE status >= 4 AND inactive = 0  AND company_invoice = '$company_invoice' AND tour_category <> 'insurance' AND ((from_date $from2) ) AND checked =1 $green"); // OR partner_deposit = final_total-yield 
while($arr = mysql_fetch_assoc($query))
{
	$income = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total from customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 0"));
	$outcome = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total from customers_tour_payment WHERE customer_id = $arr[id] AND is_transfer = 1"));

	$partnerArr = mysql_fetch_assoc($mysql->query("SELECT * from partners WHERE coredb_id = $arr[partner_id]"));


		
	$totransfer = $arr[final_total]-$arr['yield'];
	
	$current_balance = $totransfer-$outcome[total];
	
	
	if($income[total]+$arr[voucher_value] < $arr[total])
		$class = 'red';
	else
		$class = '';
		
	if($current_balance <> 0)
	{
		
	if($current_balance < 0)
		$cls = 'purple';
	else
		$cls = '';
	
	
	if($income[total]+$arr[voucher_value] == $arr[total] && $current_balance > 0  && $partnerArr[pid] <> 3699)
	{
			$totalinv = str_pad(round($current_balance), 10, "0", STR_PAD_LEFT);
			$inner_id = str_pad($h, 6, "0", STR_PAD_LEFT); 
			$account_no = str_replace("-",'',trim($partnerArr[account_no]));
			$partner_id = str_pad(substr(trim($partnerArr[coredb_id]),0,22), 24, " ", STR_PAD_RIGHT); 
			$partner_name = str_pad(substr(removeSpecialChars(trim($partnerArr[company_name])),0,32), 35, " ", STR_PAD_RIGHT); 
			$partner_address = str_pad(substr(trim(removeSpecialChars($partnerArr[address])),0,32), 35, " ", STR_PAD_RIGHT); 
			$cmnt = str_pad(substr("$arr[name] - $arr[offer_id] - $arr[position_number]",0,65), 70, " ", STR_PAD_RIGHT); 
			
			$sum = $sum+round($current_balance);
			$transactions.= "02".$inner_id."00000000".$totalinv."".$account_no."".$partner_id."".$partner_name."".$partner_address."".$partner_name."".$cmnt."\r\n";
		
			$h++;		
			
			$trurl = "<a href='?settransfer=1&id=$arr[id]'>[elutalva]</a>";
	}
	else
		$trurl = "";
		

	
	$totalbalance = $income[total]-$outcome[total];
	$content.="<tr class='$class'>";
		$content.="<td width='20'><input type='checkbox' name='tottransfer[]' value='$arr[id]'/></td>";
		$content.="<td width='20'>$h</td>";
		$content.="<td width='20'>$arr[offer_id]</td>";
		$content.="<td><a href='/passengers.php?add=1&editid=$arr[id]' target='_blank'>$partnerArr[company_name] <br/>$partnerArr[account_no] - $arr[from_date]</a></td>";
		$content.="<td align='right'>".formatPrice($totransfer)."</td>";
		$content.="<td align='right'>".formatPrice($outcome[total])."</td>";
		$content.="<td align='right'>".formatPrice($arr[total])."</td>";
		$content.="<td align='right'>".formatPrice($income[total])."</td>";
		$content.="<td align='right' class='$cls'>".formatPrice($current_balance)."</td>";
		$content.="<td align='right' class='$cls'>$trurl</td>";
	$content.="</tr>";
	
	
		
		
	}
}



$content.="</table></div></div>";


/*** transfer footer *////

	if($company_invoice == 'szallasoutlet')
	{
		$cname = 'SzallasOutlet Kft';
		$cname_orig = 'SzállásOutlet Kft.';
		$account = "116000060000000051899097";
		$taxno = "23686355";
		$fname = "szo-";
	}
	else
	{
		$cname = 'Horizont Travel Kft';
		$cname_orig = 'Horizont Travel Kft.';
		$account = "116000060000000038695809";
		$taxno = "13567721";
		$fname = "hor-";
	}
	
	$date = date("Ymd");
	$today = date("Y-m-d");
	$code = "PEA";
	$from_company = str_pad("$cname", 35, " ", STR_PAD_RIGHT); 
	$comment_main = str_pad("$today napi elszamolas", 70, " ", STR_PAD_RIGHT); 
	$head.= "01ATUTAL0A$taxno    ".$date."0002".$account."".$date."".$code."".$from_company."".$comment_main."\r\n";



	
	
	$totalrows = $h-1;
	$count = str_pad($totalrows, 6, "0", STR_PAD_LEFT); 
	$totaltransfer = str_pad($sum, 16, "0", STR_PAD_LEFT); 
	$foot= "03".$count."".$totaltransfer;

	$transfer =  $head;
	$transfer.= $transactions;
	$transfer.= $foot."\r\n";
/****/




if($_GET[download] <> 1)
{
	
	echo $content;
	foot();
}


if($_GET[download] == 1)
{

	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment;filename=tour-".date("Ymd-His").".121");
	header('Pragma: no-cache');
	header('Expires: 0');
	
	echo $transfer;
	
}
?>