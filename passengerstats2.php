<?
include("inc/tour.init.inc.php");

userlogin();

if($CURUSER[userclass] < 50)
	header("Location: index.php");
	
head("Külföldi statisztika statisztika");


$weekdays = array(
'',
'hétfő',
'kedd',
'szerda',
'csütörtök',
'péntek',
'szombat',
'vasárnap',
);


$delreasons = array(
	0 => "",
	1 => "egyéb, megjegyzésben részletezem",
	2 => "nincs hely",
	3 => "máshol foglalt",
	4 => "nem utazik",
	5 => "egyénileg utazik",
	6 => "hiba az árban",
	7 => "nem reális kérés",
	8 => "nem reagált",
	9 => "azonos megrendelés", //nem hasznalatosI
	201 => "azonos megrendelés",
	200 => "emlékeztess később",
);

$ag = array();

?>


<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='?all=1' class='current'>Ajánlatkérés / foglalás statisztika</a></li>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>

	<form method='get'>
	<div style='text-align:center'>	

	<input type='text' name='from_date' class='maskeddate' value='<?=$_GET[from_date]?>' placeholder='kezdete'/>
	<input type='text' name='to_date' class='maskeddate' value='<?=$_GET[to_date]?>' placeholder='vége'/>

	<select name='agent_id'>
			<option value=''>Iroda / értékesítő</option>
			<? 
			$query = $mysql->query("SELECT * FROM partner_offices WHERE pid = 3404 ORDER BY city ASC");
			while($a = mysql_fetch_assoc($query))
			{
				if ($_GET[agent_id] == "office-$a[id]")
					$sel = 'selected';
				else
					$sel = '';
					
				echo "<option value='office-$a[id]' $sel>$a[city]</option>";

				$qr = $mysql->query("SELECT pid, username FROM partners WHERE passengers = 1 AND office_id = $a[id] ORDER BY username ASC");
				while($arr = mysql_fetch_assoc($qr))
				{
					if($_GET[agent_id] == $arr[pid])
						$selected = 'selected';
					else
						$selected = '';
					echo "<option value='$arr[pid]' $selected>&nbsp;&nbsp;&nbsp;$arr[username]</option>";
				}
			}
			?>
		</select>
		<input type='checkbox' value='1' name='showdata'/> listát mutat
		<input type='submit' value='szűrés'/>
	</div>
	<hr/>
	</form>
	<style>
	<? if($_GET[showdata] <> 1) { ?>
		.hidetable { display:none; }
	<? } ?>
	</style>
	<table class='hidetable'>
<?
if (strpos($_GET[agent_id],'office-') !== false) {
		$oid = explode("-",$_GET[agent_id]);
		$qr = $mysql->query("SELECT pid, username FROM partners WHERE passengers = 1 AND office_id = $oid[1] ORDER BY username ASC");
		while($arr = mysql_fetch_assoc($qr))
		{
			$agents[] = $arr[pid];
		}
		
		$agents = implode(" OR agent_id =", $agents);
		
		}
		else
			$agents = $_GET[agent_id];
			

if($agents == '')
	$agents = '';
else
	$agents = "AND (agent_id = $agents)";
	
$passengers = $mysql->query("SELECT * FROM customers_tour WHERE added >= '$_GET[from_date]' AND added <= '$_GET[to_date]' AND  donotwant <> 201 $agents");

$z = 1;
while($arr = mysql_fetch_assoc($passengers))
{
	$agent = mysql_fetch_assoc($mysql->query("SELECT username FROM partners WHERE pid = $arr[agent_id] LIMIT 1"));
	
	$pcount = 0;
	for($p = 1; $p <= 10; $p++)
	{
		if($arr["passenger".$p."_name"] <> '')
			$pcount++;
	}
	
	if($arr[inactive] == 1)
	{
		$class = 'grey';
		$inactive++;
		$inactiveval = $inactiveval + $arr[total] -$arr[voucher_value];
		
		$ag[$arr[agent_id]][inactive] = $ag[$arr[agent_id]][inactive]+ $arr[total] -$arr[voucher_value];
		$p1val = $p1val + $pcount;
		$ag[$arr[agent_id]][inactivec] = $ag[$arr[agent_id]][inactivec]+ $pcount;
		$ag[$arr[agent_id]][inactivecv]++;


	}
	elseif($arr[status] <= 2)
	{
		$class = 'blue';
		$inprogress++;
		$progr = $progr + $arr[total] -$arr[voucher_value];
		$p2val = $p2val + $pcount;
		$ag[$arr[agent_id]][inprog] = $ag[$arr[agent_id]][inprog]+ $arr[total] -$arr[voucher_value];
		$ag[$arr[agent_id]][inprogc] = $ag[$arr[agent_id]][inprogc]+ $pcount;
		$ag[$arr[agent_id]][inpr]++;
		
	}
	else
	{
		
		if($arr[source] == 'web' || $arr[source] == 'chat' || $arr[source] == 'question')
			$ag[$arr[agent_id]][online]++;
			
		$class = '';
		$total = $total + $arr[total]- -$arr[voucher_value];
		$p3val = $p3val + $pcount;
		$ag[$arr[agent_id]][total] = $ag[$arr[agent_id]][total]+ $arr[total] -$arr[voucher_value];
		$ag[$arr[agent_id]][totalc] = $ag[$arr[agent_id]][totalc]+ $pcount;
		$ag[$arr[agent_id]][totalpr]++;
	}
	
	if($arr[source] == 'web' || $arr[source] == 'chat' || $arr[source] == 'question')
			$ag[$arr[agent_id]][onlinereal]++;

	
	
	$row = "<tr class='$class'>";
		$row.= "<td width='20' align='center'>$z</td>";
		$row.= "<td width='20'><img src='/images/icons/$arr[source].png' width='20' alt='$arr[source]' title='$arr[source]'/></td>";
		$row.=  "<td width='70'><a href='passengers.php?add=1&editid=$arr[id]' target='_blank'>$arr[offer_id]</a></td>";
		$row.=  "<td width='50'>$agent[username]</td>";
		$row.=  "<td width='200'>$arr[offer_name] - <b>{$delreasons[$arr[donotwant]]}</b></td>";
		$row.=  "<td>$arr[name]</td>";
		$row.=  "<td align='right'>$pcount fő</td>";
		$row.=  "<td align='right'>".formatPrice($arr[total]-$arr[voucher_value])."</td>";
	$row.=  "</tr>";
	
	$table[$class][] = $row;
	
	$z++;
}

if(empty($table))
	$table = array();
foreach($table as $t)
	echo implode(" ",$t);
?>
</table>
<hr/>
<table  class='hidetable'>
	<tr class='header'><td colspan='30'>Idegen irodákban történő befizetések a hálózatban <?=$_GET[from_date]?> - <?=$_GET[to_date]?> között</td></tr>
<? 
	if($CURUSER[userclass] == 255)
	{
		$z = 1;
		$foreign = 1;
		$passengers = $mysql->query("SELECT * FROM customers_tour WHERE added >= '$_GET[from_date]' AND added <= '$_GET[to_date]'  AND status > 2 AND  donotwant <> 201 AND inactive = 0 ORDER BY id ASc");
		
		while($arr = mysql_fetch_assoc($passengers))
		{
			
			$agent = mysql_fetch_assoc($mysql->query("SELECT username,office_id FROM partners WHERE pid = $arr[agent_id] LIMIT 1"));
				
			$payment = mysql_fetch_assoc($mysql->query("SELECT * FROM customers_tour_payment WHERE is_transfer = 0 and customer_id = $arr[id] ORDER BY id ASc LIMIT 1"));
			
			$payer = mysql_fetch_assoc($mysql->query("SELECT pid,office_id,username FROM partners WHERE username = '$payment[username]' LIMIT 1"));

			if($agent[office_id] == 2499 && $payer[office_id] == 2499)
			{
				$online++;
			}
			elseif($agent[office_id] == 2499 && $payer[office_id] <> 2499)
				$delegated++;
			
			if($payment[payment] == 2)
				$ptext = 'Készpénz';
			if($payment[payment] == 9)
				$ptext = 'Bankkártya';
			if($payment[payment] == 1)
				$ptext = 'Átutalás';

			if($agent[username] <> $payer[username] && $payment[payment] <> 1 && $payer[username] <> '')
			{
				
				//if($arr[source] == 'chat' || $arr[source] == 'web')
					$foreignchat[$arr[agent_id]]++;
					
				$fcount[$payer[pid]]++;
				
				$office[$payer[office_id]]++;
				echo "
				<tr>
					<td>$foreign</td>
					<td><a href='passengers.php?add=1&editid=$arr[id]' target='_blank'>$arr[offer_id]</a></td>
					<td>$arr[name]</td>
					<td>$ptext</td>
					<td>$agent[username]</td>
					<td>$payment[username]</td>	
					<td align='right'>".formatPrice($arr[total])."</td>				
				</tr>";
				
				$foreign++;
			}
		
			$z++;	
		}

			
	}
	
?>
</table>
<hr/>
<table>
	<tr>
		<td class='lablerow'>Online maradt:</td>
		<td align='right'><?=$online?></td>
	</tr>
	<tr>
		<td class='lablerow'>Online, de irodában fizetett:</td>
		<td align='right'><?=$delegated?></td>
	</tr>
	<?
		if(empty($office))
			$office = array();
			
		arsort($office);
		
		foreach($office as $key => $value)
		{
			$o = mysql_fetch_assoc($mysql->query("SELECT city FROM partner_offices WHERE id = $key"));
			
			if($o[city] <> '')
			{
				echo "<tr>";
					echo "<td class='lablerow'>$o[city]</td>";
					echo "<td align='right'>$value</td>";
				echo "</tr>";
			}
		}
	?>
</table>
<hr/>

	<script src='//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js'></script>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css"/>
	<script>
		
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "currency-pre": function (a) {
        a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
        return parseFloat(a);
    },
    "currency-asc": function (a, b) {
        return a - b;
    },
    "currency-desc": function (a, b) {
        return b - a;
    }
});



		$(document).ready(function() {
			$('.sortable').DataTable({
				
				"aoColumns": [
				null,
				null,
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
				{ "sType": "currency" },
		],
		
				"order": [[ 0, "desc" ]],
				bFilter: false, bInfo: false, "bPaginate": false,
		} );
		
			
	} );	
	</script>



<table class='sortable'>
	<thead>
	<tr class='header'>
		<td>Seller</td>
		<td>Iroda</td>
		<td>db</td>
		<td>Törölt</td>
		<td>db</td>
		<td>Folyamatban</td>
		<td>db</td>
		<td>Befoglalt</td>
		<td>Idegen másé</td>
		<td>Online mind</td>
		<td>Online siker</td>
		<td>Idegen saját</td>

	</tr>
	</thead>
	<? 
		foreach($ag as $k => $v)
		{
					
			$agent = mysql_fetch_assoc($mysql->query("SELECT company_name,office_id FROM partners WHERE pid = '$k' LIMIT 1"));	
			$office = mysql_fetch_assoc($mysql->query("SELECT city FROM partner_offices WHERE id = '$agent[office_id]' LIMIT 1"));
			
		if($agent[company_name] <> '')
		{
			echo "<tr>
		<td>$agent[company_name]</td>
		<td>$office[city]</td>
		<td align='right'>$v[inactivecv]</td>
		<td align='right'>".formatPrice($v[inactive])."</td>
		<td align='right'>$v[inpr]</td>
		<td align='right'>".formatPrice($v[inprog])."</td>
		<td align='right'>$v[totalpr]</td>
		<td align='right'>".formatPrice($v[total])."</td>
		<td align='right'>{$fcount[$k]}</td>
		<td align='right'>$v[onlinereal]</td>		
		<td align='right'>$v[online]</td>
		<td align='right'>$foreignchat[$k]</td>

		</tr>";
		}
		}
		?>
	<!-- 	<td align='right'><?=$inactive?> db</td>
		<td align='right'><?=$p1val?> fő</td>
		<td align='right'><?=formatPrice($inactiveval)?></td>
	
	<tr>
		<td align='right'><?=$inprogress?> db</td>
		<td align='right'><?=$p2val?> fő</td>
		<td align='right'><?=formatPrice($progr)?></td>
	</tr>
	<tr>
		<td align='right'><?=$z-$inprogress-$inactive-1?> db</td>
		<td align='right'><?=$p3val?> fő</td>
		<td align='right'><?=formatPrice($total)?></td>
	</tr>
	-->
	
</table>
<hr/>


</div>
</div>

<?
foot();
?>