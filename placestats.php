<?
/*
 * customers.php 
 *
 * customers page
*/
error_reporting(E_ERROR | E_PARSE);

/* bootstrap file */
include("inc/init.inc.php");
userlogin();


head("Helyszíni fizetés kimutatás");
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					<li><a href='customers.php'>Vásárlók</a></li>
					<li><a href='placestats.php' class='<? if($_GET[showcustomer] <> 1 && $_GET[qr] <> 1) echo "current"?>'>Kintlévőség statisztika</a></li>
					<li><a href='?showcustomer=1' class='<? if($_GET[showcustomer] == 1) echo "current"?>'>Behajtás</a></li>
					<li><a href='?qr=1' class='<? if($_GET[qr] == 1) echo "current"?>'>QR</a></li>



		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<script>

$(document).ready(function() {
	$('.checkall').click(function(){	
		$("input[type=checkbox]").each( function() {
				$(this).attr("checked",status);
		});

		return false;
	});
	
	$('.callbutton').click(function(){	
		alert('aaaa');
		return false;
	});

});

</script>

<?

if($_GET[qr] == 1) {
	
	$query = $mysql->query("SELECT * FROM customers WHERE payment = 6 AND inactive = 0 AND is_qr = 1 AND customer_left = '0000-00-00 00:00:00' ORDER BY added ASC");
	
	$i = 1;
	echo "<table>";
	
	echo "<tr class='header'>";
			echo "<td>ID</td>";
			echo "<td>Hotel</td>";
			echo "<td>Utalványs</td>";
			echo "<td>Név</td>";
			echo "<td>Foglalt</td>";
			echo "<td>Érvényes</td>";
			echo "<td align='right'>Ár</td>";
		echo "</tr>";
		
	while($arr = mysql_fetch_assoc($query))
	{
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid]"));
		$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = $arr[offers_id]"));

		if($arr[customer_booked] == '0000-00-00')
			$arr[customer_booked] = '';
			
		$left = str_replace("-",'',$arr[customer_booked]);
		$today = date("Ymd");
		
		if($left < $today && $left <> '')
			$class = 'orange';
		elseif($arr[paid] == 1)
			$class = 'green';
		else
			$class = '';
		echo "<tr class='$class'>";
			echo "<td>$i</td>";
			echo "<td>$partner[hotel_name]</td>";
			echo "<td>$arr[offer_id]</td>";
			echo "<td>$arr[name]</td>";
			echo "<td align='center'>$arr[customer_booked]</td>";
			echo "<td align='center'>$offer[expiration_date]</td>";
			echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
		echo "</tr>";
		$i++;
	}
	echo "</table>";
}
elseif($_GET[showcustomer] <> 1) { ?>
<fieldset id='filter'>
<legend>Szűrés</legend>
<form method='GET'>


<input type='checkbox' name='payment[]' value="2" <? if(in_array(2, $_GET[payment])) echo "checked";?>/>Készpénz
<input type='checkbox' name='payment[]'  value="1" <? if(in_array(1, $_GET[payment])) echo "checked";?>/>Átutalás
<input type='checkbox' name='payment[]' value="9" <? if(in_array(9, $_GET[payment])) echo "checked";?>/>Bankkártya
<input type='checkbox' name='payment[]' value="6" <? if(in_array(6, $_GET[payment])) echo "checked";?>/>Helyszínen
<input type='checkbox' name='payment[]' value="10" <? if(in_array(10, $_GET[payment])) echo "checked";?>/>OTP SZÉP kártya
<input type='checkbox' name='payment[]' value="11" <? if(in_array(11, $_GET[payment])) echo "checked";?>/>MKB SZÉP kártya
<input type='checkbox' name='payment[]' value="12" <? if(in_array(12, $_GET[payment])) echo "checked";?>/>K&H SZÉP kártya <a href='#' class='checkall'>(mind)</a>

<input type='submit' value='Szűrés'/> 				
</form>
</fieldset>
<br/><br/>
<table>
<tr class='header'>
	<td>Hotel neve</td>
	<td>E-mail</td>
	<td width='50'>db</td>
	<td>Érték</td>

</tr>
<?

if($_GET[payment] <> '')
{
	$payment = "AND (payment = ".implode(" OR payment = ",$_GET[payment]).")";
	$pm = "&payment[]=".implode("&payment[]=",$_GET[payment]);

}
else
{
	$payment = '';
	
}


	$query = $mysql->query("select count(cid) as cnt, sum(orig_price) as total, pid from customers where   inactive = 0 AND paid = 0 $payment GROUP by pid order by cnt DESC");
	
	while($arr = mysql_fetch_assoc($query))
	{
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] order by added"));
	echo "<tr>
			<td><a href='?pid=$arr[pid]$pm'><b>$partner[hotel_name]</b></a></td>
			<td><a href='mailto:$partner[email]'><b>$partner[email]</b></a></td>
			<td align='right'>$arr[cnt] db</td>
			<td align='right'>".formatPrice($arr[total])."</td>

		</tr>";
		
		if($arr[pid] == $_GET[pid])
		{
		
			echo "<tr><td colspan='4'><table>";
			
			$items = $mysql->query("select * FROM customers where inactive = 0 AND paid = 0 AND pid = $arr[pid] $payment order by added DESC,name ASC");
			while($item = mysql_fetch_assoc($items))
			{
				$pitems = '';
				
			$unpaid = mysql_fetch_assoc($mysql->query("select count(cid) as cnt FROM customers where inactive = 0 AND pid = $arr[pid] AND email = '$item[email]'  AND cid <> $item[cid]  order by added DESC LIMIT 1"));
			
			$paid = mysql_fetch_assoc($mysql->query("select count(cid) as cnt FROM customers where inactive = 0 AND pid = $arr[pid] AND email = '$item[email]' AND paid = 1 AND cid <> $item[cid]  order by added DESC LIMIT 1"));


	if($item[postpone] == 1)
		$payment = $lang[t_check];
	elseif($item[payment] == 1  || $item[checkpaper_id] > 0) 
		$payment = $lang[transfer];
	elseif($item[payment] == 2) 
		$payment = $lang[cash];
	elseif($item[payment] == 3) 
		$payment = $lang[postpaid];
	elseif($item[payment] == 4) 
		$payment = $lang[delivery];
	elseif($item[payment] == 5) 
		$payment = $lang[t_check];
	elseif($item[payment] == 6) 
		$payment = $lang[place];
	elseif($item[payment] == 7) 
		$payment = $lang[online];
	elseif($item[payment] == 8) 
		$payment = $lang[facebook];
	elseif($item[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($item[payment] == 10) 
		$payment = "OTP SZÉP";
	elseif($item[payment] == 11) 
		$payment = "MKB SZÉP";
	elseif($item[payment] == 12) 
		$payment = "K&H SZÉP";



		if($paid[cnt] == 0)
			$paid[cnt] = '';
		else 
		{
				
				$plist = $mysql->query("select paid_date FROM customers where inactive = 0 AND pid = $arr[pid] AND email = '$item[email]' AND paid = 1 AND cid <> $item[cid] ");
				while($p = mysql_fetch_assoc($plist))
				{
					$pdata = explode(" ",$p[paid_date]);
					
					$pitems.="$pdata[0]<br/>";
					
					
				}
		}
		
		if($unpaid[cnt] == 0)
			$unpaid[cnt] = '';

			
			if($item[name] <> $prevname)
				echo "<tr class='lgrey'><td colspan='9' style='height:2px;padding:0;!important;font-size:1px;'></td></tr>";	
			
			$odata = mysql_fetch_assoc($mysql->query("SELECT expiration_date FROM offers WHERE id = $item[offers_id]"));
			if($item[call_comment] <> '')
				$cls = 'red';
			else
				$cls = '';
				
			
			if (strpos($item[offer_id],'TVO-') !== false) 
				$class = 'grey';
			elseif (strpos($item[offer_id],'CZO-') !== false) 
				$class = 'grey';
			else
				$class = '';
			
			$added = explode(" ",$item[added]);
			echo "<tr class='$class'>
				<td width='65' align='center'>$added[0]</td>
				<td width='50'>".str_replace("SZO-",'',$item[offer_id])."</td>
				<td width='160'><a href='mailto:$item[email]'>$item[name]</a></td>
				<td width='100'>$item[phone]</td>
				<td width='100'>$odata[expiration_date]</td>
				<td width='80'>$payment</td>
				<td width='100' align='right'>".formatprice($item[orig_price])."</td>
				
				<td width='10'>$unpaid[cnt]</td>
				<td width='65' align='center'>$pitems</td>
				
				<td width='65' align='center' class='$cls'><a href='info/callcomment.php?pid=$arr[pid]&cid=$item[cid]'  rel='facebox iframe'>tovább&nbsp;&raquo;</a></td>


				</tr>";
		
					
				$prevname = $item[name];
			}

			echo "</table></td></tr>";
		}
		
		$total = $total + $arr[cnt];
		$totalprice = $totalprice + $arr[total];

	}
	
	echo "
		<tr class='header'>
			<td colspan='2'>Összesen</td>
			<td align='right'>$total db</td>
			<td align='right'>".formatPrice($totalprice)."</td>
		</tr>";
?>


</table>
<? } else {
	
	?>
	<form method='get'>
		<input type='hidden' name='showcustomer' value='1'/>
		<center><select name='expired' onchange='submit()'>
			<? for($i=1;$i<=60;$i++)
				{	
					if($i == $_GET[expired])
						$selected = 'selected';
					elseif($_GET[expired] == '' && $i == 18)
						$selected = 'selected';
					else
						$selected = '';
					echo "<option value='$i' $selected>$i napja lejárt</option>";
				}
			?>
		</select>
		<hr/></center>
	</form>
	<?
	
	$days = (int)$_GET[expired];
	
	if($days == 0)
		$days = 18;
	echo "<table>	
		<tr class='header'>
			<td>#</td>
			<td>Vásárlás</td>
			<td>Határidő</td>
			<td>Utalvány</td>
			<td>Név</td>
			<td>Telefon</td>
			<td>Fiz. mód</td>
			<td>Összeg</td>
			<td>-</td>
			<td>Fizetettek</td>	
			<td colspan='2'>Tovább</td>		
		</tr>";

		$i = 1;
	$items = $mysql->query("select * FROM customers where inactive = 0 AND paid = 0 AND added <  date_sub(NOW(), INTERVAL $days DAY) AND added > '2013-08-20%' order by due_date DESC");
			while($item = mysql_fetch_assoc($items))
			{
				$pitems = '';
				
			$unpaid = mysql_fetch_assoc($mysql->query("select count(cid) as cnt FROM customers where inactive = 0 AND pid = '$item[pid]' AND email = '$item[email]'  AND cid <> $item[cid]  order by added DESC LIMIT 1"));
			
			$paid = mysql_fetch_assoc($mysql->query("select count(cid) as cnt FROM customers where inactive = 0 AND pid = '$item[pid]' AND email = '$item[email]' AND paid = 1 AND cid <> $item[cid]  order by added DESC LIMIT 1"));


	if($item[postpone] == 1)
		$payment = $lang[t_check];
	elseif($item[payment] == 1  || $item[checkpaper_id] > 0) 
		$payment = $lang[transfer];
	elseif($item[payment] == 2) 
		$payment = $lang[cash];
	elseif($item[payment] == 3) 
		$payment = $lang[postpaid];
	elseif($item[payment] == 4) 
		$payment = $lang[delivery];
	elseif($item[payment] == 5) 
		$payment = $lang[t_check];
	elseif($item[payment] == 6) 
		$payment = $lang[place];
	elseif($item[payment] == 7) 
		$payment = $lang[online];
	elseif($item[payment] == 8) 
		$payment = $lang[facebook];
	elseif($item[payment] == 9) 
		$payment = $lang[credit_card];
	elseif($item[payment] == 10) 
		$payment = "OTP SZÉP";
	elseif($item[payment] == 11) 
		$payment = "MKB SZÉP";
	elseif($item[payment] == 12) 
		$payment = "K&H SZÉP";



		if($paid[cnt] == 0)
			$paid[cnt] = '';
		else 
		{
				
				$plist = $mysql->query("select paid_date FROM customers where inactive = 0 AND pid = $item[pid] AND email = '$item[email]' AND paid = 1 AND cid <> $item[cid] ");
				while($p = mysql_fetch_assoc($plist))
				{
					$pdata = explode(" ",$p[paid_date]);
					
					$pitems.="$pdata[0]<br/>";
					
					
				}
		}
		
		if($unpaid[cnt] == 0)
			$unpaid[cnt] = '';

			
			if($item[name] <> $prevname)
				echo "<tr class='lgrey'><td colspan='12' style='height:2px;padding:0;!important;font-size:1px;'></td></tr>";	
			
			
			if($item[call_comment] <> '')
				$cls = 'red';
			else
				$cls = '';
			$added = explode(" ",$item[added]);
			
			$due = explode(" ",$item[due_date]);
			
			
			if (strpos($item[offer_id],'TVO-') !== false) 
				$class = 'grey';
			elseif (strpos($item[offer_id],'CZO-') !== false) 
				$class = 'grey';
			else
				$class = '';


			echo "<tr class='$class'>
				<td width='10' align='center'>$i</td>
				<td width='65' align='center'>$added[0]</td>
					<td width='65' align='center'>$due[0]</td>
				<td width='50'>".str_replace("SZO-",'',$item[offer_id])."</td>
				<td width='160'><a href='mailto:$item[email]'>$item[name]</a></td>
				<td width='100'>$item[phone]</td>
				<td width='80'>$payment</td>
				<td width='100' align='right'>".formatprice($item[orig_price])."</td>
				
				<td width='10'>$unpaid[cnt]</td>
				<td width='65' align='center'>$pitems</td>
				
				<td width='65' align='center' class='$cls'><a href='info/callcomment.php?pid=$arr[pid]&cid=$item[cid]'  rel='facebox iframe'>tovább&nbsp;&raquo;</a></td>

				<td width='65' align='center' class='$cls'><a href='info/show_customer_notification.php?cid=$item[cid]'  rel='facebox iframe'>e-mailek&nbsp;&raquo;</a></td>


				</tr>";
		
					
				$prevname = $item[name];
				
				$i++;
			}

			echo "</table>";
	
}  ?>
</div></div>
<?
foot();
?>