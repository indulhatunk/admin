<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

head("Portál kimutatás");
?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
			<li><a href='/own_vouchers.php'>Saját eladás</a></li>
			<li><a href='#' class='<? if($_GET[contracts] <> 1) echo "current"?>'>Szerződéskötési kimutatás</a></li>

			<li><a href='?contracts=1' class='<? if($_GET[contracts] == 1) echo "current"?>'>Szerződés kimutatás</a></li>

		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<form method='get'>
	<input type='hidden' value='<?=$_GET[contracts]?>' name='contracts' class='maskeddate'/> 
	<input type='text' value='<?=$_GET[from_date]?>' name='from_date' class='maskeddate'/> 
	<input type='text'  value='<?=$_GET[to_date]?>' name='to_date' class='maskeddate'/> 
	<input type='submit' value='Szűrés'/>
</form>
<hr/>
<?

if($_GET[contracts] == 1) 
{
echo "<table>";

echo "<tr class='header'>";
		echo "<td>ID</td>";
		echo "<td>Típus</td>";
		echo "<td>Hotel</td>";
		echo "<td>Dátum</td>";
		echo "<td>Darab</td>";
		echo "<td>Érték</td>";
		echo "<td>Tartalom</td>";
	echo "</tr>";

	
$query = $mysql->query("SELECT * FROM contracts WHERE added >= '$_GET[from_date]' AND added <= '$_GET[to_date]' AND type <> 'outlet' AND comment <> '' ORDER BY partner_id, added DESC");

$i = 1;
while($arr = mysql_fetch_assoc($query))
{

	
			$partner =  mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE partner_id = $arr[partner_id] LIMIT 1"));
			$vouchers =  mysql_fetch_assoc($mysql->query("SELECT count(id) as cnt, sum(orig_price) as total FROM own_vouchers WHERE partner_id = $arr[partner_id] AND contract_id = $arr[id]"));

/*	if($prevpartner <> $partner[pid] && $prevpartner <> '' )
	{
		
		echo "<tr class='header'>
			<td colspan='4' align='left'>$photel összesen</td>";
		echo "<td align='right'>$vcount</td>";
		echo "<td align='right'>".formatPrice($vtotal,0,1)."</td>";
		echo "<td></td></tr>";
		echo "<tr><td colspan='7' style='height:10px;font-size:11px;padding:0;'></td></tr>";
		$vcount = 0;
		$vtotal = 0;
	}
*/
	echo "<tr>";
		echo "<td>$i</td>";
		echo "<td>$arr[type]</td>";
		echo "<td>$partner[hotel_name]</td>";
		echo "<td width='80'>$arr[added]</td>";
		echo "<td align='right'>$vouchers[cnt]</td>";
		echo "<td align='right'>".formatPrice($vouchers[total],0,1)."</td>";
		echo "<td>".strip_tags($arr[comment])."</td>";

	echo "</tr>";
	
	$prevpartner = $partner[pid];
	$photel = $partner[hotel_name];

	$vtotal = $vtotal + $vouchers[total];
	$vcount = $vcount + $vouchers[cnt];
	$total = $total + $vouchers[cnt];
	$tvalue = $tvalue + $vouchers[total];
	
	$i++;
}	

	echo "<tr class='header'>
			<td colspan='4' align='left'>Összesen</td>";
		echo "<td align='right'>$total</td>";
		echo "<td align='right'>".formatPrice($tvalue,0,1)."</td>";
		echo "<td></td></tr>";
		
echo "</table>";



}
else
{
echo "<table>";

echo "<tr class='header'>";
		echo "<td>Típus</td>";
		echo "<td>DB.</td>";
		echo "<td>Voucher</td>";
		echo "<td>Érték</td>";
		echo "<td>Fizetett</td>";
		echo "<td>Raktár</td>";
		echo "<td>Rakt. ért.</td>";
	echo "</tr>";
	
$query = $mysql->query("SELECT * FROM contracts WHERE added >= '$_GET[from_date]' AND added <= '$_GET[to_date]' GROUP BY type ORDER BY type ASC");
while($arr = mysql_fetch_assoc($query))
{
	$contracts = $mysql->query("SELECT * FROM contracts WHERE  added >= '$_GET[from_date]' AND added <= '$_GET[to_date]' and type = '$arr[type]' ");
	
	$h = 1;
	$vouchers = 0;
	$vouchervalue = 0;
	$vouchersold = 0;
	
	$unpaid = 0;
	$unpaidvalue = 0;
	while($c = mysql_fetch_assoc($contracts))
	{
		$v =  mysql_fetch_assoc($mysql->query("SELECT count(id) as cnt, sum(orig_price) as total, sum(price) as paid FROM own_vouchers WHERE  contract_id = $c[id] and partner_id = $c[partner_id] "));
		
		$up =  mysql_fetch_assoc($mysql->query("SELECT count(id) as cnt, sum(orig_price) as total, sum(price) as paid FROM own_vouchers WHERE  contract_id = $c[id] and partner_id = $c[partner_id] AND paid = 0 AND price <> 1"));


		//echo " $vouchers + $v[cnt]<hr/>";
		$vouchers = $vouchers + $v[cnt];
		$vouchervalue = $vouchervalue + $v[total];
		$vouchersold = $vouchersold + $v[paid];
		
		$unpaid = $unpaid + $up[cnt];
		$unpaidvalue = $unpaidvalue + $up[total];
		
		$h++;	
	}
	
	echo "<tr>";
		echo "<td>$arr[type]</td>";
		echo "<td align='right'>$h</td>";
		echo "<td align='right'>$vouchers</td>";
		echo "<td align='right'>".formatprice($vouchervalue)."</td>";
		echo "<td align='right'>".formatPrice($vouchersold)."</td>";
		
		echo "<td align='right'>$unpaid</td>";
		echo "<td align='right'>".formatprice($unpaidvalue)."</td>";
	echo "</tr>";
}

echo "</table>";

}

echo "</div></div>";
foot();
?>