<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

	userlogin();
	
	if($CURUSER[userclass] < 3)
	header("location: index.php");
	
	
if($_POST[id] > 0 && $_POST[page_title] <> '')
{
	$_POST[last_edit] = 'NOW()';
	$mysql_tour->query_update("presentation",$_POST,"id=$_POST[id]");
//	echo message("Sikeresen szerkesztette a bemutatót!");
	writelog("$CURUSER[username] edited presentation page $_POST[id]");
	
	header("Location: presentation.php?edit=$_POST[id]");
	die;
}
if((int)$_POST[id] == 0 && $_POST[page_title] <> '')
{
	$_POST[create_date] = 'NOW()';
	$id = $mysql_tour->query_insert("presentation",$_POST);
//	echo message("Sikeresen létrehozta a bemutatót!");
	writelog("$CURUSER[username] created presentation page $_POST[title]");
	header("Location: presentation.php?edit=$id");
	die;
}	


$query = $mysql_tour->query("SELECT * FROM presentation WHERE page_url = '' ORDER BY id asc");

while($arr = mysql_fetch_assoc($query))
{
	$url = "http://video.indulhatunk.hu/".clean_url_indulhatunk($arr[page_title])."-$arr[id]";
	
	$mysql_tour->query("UPDATE presentation SET page_url = '$url' WHERE id = $arr[id]");
	
}
head("Bemutatók kezelése");


?>

<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
extended_valid_elements: "iframe[class|src|frameborder=0|alt|title|width|height|align|name]",
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>



<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='?all=1' class="<? if($_GET[add] <> 1) echo "current"?>">Bemutatók szerkesztése</a></li>
						<li><a href='?add=1' class="<? if($_GET[add] == 1) echo "current"?>">Új cikk</a></li>
					</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<? if($_GET[edit] > 0 || $_GET[add] == 1) { 
	
	$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM presentation WHERE id = '$_GET[edit]' LIMIT 1"));
	?>
	
	<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
$().ready(function() {
	$("#day_num").change(function () {
		var daynum = $(this).val()*1-1;
		
		if(daynum < 0)
			daynum = 0;
			
		$("#realnight").html(daynum);
		
		return false;
    });
    
    $(".changepackage").keyup(function () {
    	var daynum = <?=$editdata[day_num]-1?>*1;
		var thisid = $(this).attr('id');
		var thisval = Math.round($(this).val()*1/daynum/2);
		$('#pernight-'+thisid).html(thisval);
		//alert(thisid);
	
    });
});

	</script>



	<form method='POST' action='presentation.php'>
		<input type='hidden' name='id' value='<?=$city[id]?>' />
		
			<fieldset id='customerForm'>
	<legend>Bemutatók szerkesztése</legend>
	<table width='100%'>
	
	<tr>
				<td class='lablerow'>Ország:</td>
				<td>
					<select name='country_id'>
						<?
						$query = $mysql_tour->query("SELECT * FROM country ORDER BY name ASC");
						
						echo "<option value=''>Kérjük válasszon</option>";
						while($arr = mysql_fetch_assoc($query))
						{
							if($arr[id] == $city[country_id])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[id]' $select>$arr[name]</option>";
						}
						?>					
					</select>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Régió:</td>
				<td>
					<select name='region_id'>
						<?
						$query = $mysql_tour->query("SELECT * FROM region ORDER BY name ASC");
						
						echo "<option value=''>Kérjük válasszon</option>";

						while($arr = mysql_fetch_assoc($query))
						{
							if($arr[id] == $city[region_id])
								$select = 'selected';
							else
								$select = '';
								
							if($arr[aleph_id] > 0)
								$ok = "(jó)";
								else
								$ok = "(nem hasznald!)";
							echo "<option value='$arr[id]' $select>$arr[name] $ok</option>";
						}
						?>					
					</select>
				</td>
			</tr>
				<tr>
				<td class='lablerow'>Város:</td>
				<td>
					<select name='city_id'>
						<?
						$query = $mysql_tour->query("SELECT * FROM city ORDER BY name ASC");
						
						echo "<option value=''>Kérjük válasszon</option>";
						while($arr = mysql_fetch_assoc($query))
						{
							
								if($arr[aleph_id] > 0)
								$ok = "(jó)";
								else
								$ok = "(nem hasznald!)";
								
							if($arr[id] == $city[city_id])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[id]' $select>$arr[name] $ok</option>";
						}
						?>					
					</select>
				</td>
			</tr>

		<tr>
			<td class='lablerow'>Cím:</td>
			<td><input type='text' name='page_title' value='<?=$city[page_title]?>' style='width:520px'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Keywords:</td>
			<td><input type='text' name='page_keywords' value='<?=$city[page_keywords]?>' style='width:520px'/></td>
		</tr>

<tr>
			<td class='lablerow'>Description:</td>
			<td><input type='text' name='page_description' value='<?=$city[page_description]?>' style='width:520px'/></td>
		</tr>

		<tr>
			<td class='lablerow'>URL:</td>
			<td><input type='text' name='page_url' value='<?=$city[page_url]?>' style='width:520px'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Youtube URL:</td>
			<td><input type='text' name='youtube_url' value='<?=$city[youtube_url]?>' style='width:520px'/></td>
		</tr>
		<tr>
			<td colspan='2'><textarea name='description' class='tinymce'  rows="100" cols="20" ><?=$city[description]?></textarea></td>
		</tr>
		<tr>
			<td class='lablerow'>Publikálás:</td>
			<td><select name='published'>
				<option value='0' <? if($city[published] == 0) echo "selected";?>>nem</option>
				<option value='1' <? if($city[published] == 1) echo "selected";?>>igen</option>
			</select></td>
		</tr>
		<tr>
			<td colspan='2'><input type='submit' value='Mehet'/></td>
		</tr>
	</table>
	</form>
	</fieldset>
	<?
	echo "</div></div>";
	foot();
	die;	
}




?>

<table>

	<tr class='header'>
		<td>-</td>
		<td>ID</td>
		<td>Ország</td>		
		<td>Régió</td>
		<td>Város</td>
		<td>Név</td>

	</tr>
<?

	$query = $mysql_tour->query("SELECT * FROM presentation WHERE country_id <> 125 ORDER BY country_id ASC");
	while($arr = mysql_fetch_assoc($query))
	{
	
		$country = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM country WHERE id = '$arr[country_id]' LIMIT 1"));
		$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = '$arr[city_id]' LIMIT 1"));
		$region = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM region WHERE id = '$arr[region_id]' LIMIT 1"));

		echo "	<tr class='$class'>
					<td width='20'><a href='?edit=$arr[id]'><img src='/images/edit.png' width='20'/></a></td>
					<td>$arr[id]</td>
					<td>$country[name]</td>
					<td>$region[name]</td>
					<td>$city[name]</td>
					<td>$arr[page_title]</td>
				</tr>";
		
	}
?>
</table>
</div></div>
<?
foot();
?>