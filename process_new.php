<? 

ob_start();


class OCI8DB {

function dbconn() {
	$conn = oci_connect('coredb', 'coredb', 'static.pihenni.hu/xe','AL32UTF8');
	if (!$conn) {
		trigger_error("Could not connect to database", E_USER_ERROR);
		return false;
	}
	return $conn;
}

}

$db = new OCI8DB;
$dbconn = $db->dbconn();

function parseQuery($dbconn,$query) {
	$st = oci_parse($dbconn, $query);
	oci_execute($st, OCI_DEFAULT);
	return $st;
}

function num_files($partnerid,$offerid,$type) 
{ 
	$dir = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/$type/$partnerid/$offerid/orig/";
	//get all image files with a .jpg extension.
	$images = glob($dir . "*.jpg");
	//print each file name
	$z=0;
	foreach($images as $image)
	{
		$size = filesize($image);
		
		if($size > 0)
		{
			$z++;
		}	
	//	echo "$image >>> $size\n\n";
		
	}

	return $z;
    //return count(glob("/var/www/hosts/pihenni.hu/htdocs/static.pihenni/$type/$partnerid/$offerid/orig/*")); 
}
function dirList ($directory) 
{
    $results = array();
    $handler = opendir($directory);
	    while ($file = readdir($handler)) {

        if ($file != '.' && $file != '..')
            $results[] = $file;
    }
    closedir($handler);
    return $results;

}
function Image($image, $crop = null, $size = null,$fileName = "") {
    $image = ImageCreateFromString(file_get_contents($image));

    if (is_resource($image) === true) {
        $x = 0;
        $y = 0;
        $width = imagesx($image);
        $height = imagesy($image);

        /*
        CROP (Aspect Ratio) Section
        */

        if (is_null($crop) === true) {
            $crop = array($width, $height);
        } else {
            $crop = array_filter(explode(':', $crop));

            if (empty($crop) === true) {
                    $crop = array($width, $height);
            } else {
                if ((empty($crop[0]) === true) || (is_numeric($crop[0]) === false)) {
                        $crop[0] = $crop[1];
                } else if ((empty($crop[1]) === true) || (is_numeric($crop[1]) === false)) {
                        $crop[1] = $crop[0];
                }
            }

            $ratio = array(0 => $width / $height, 1 => $crop[0] / $crop[1]);

            if ($ratio[0] > $ratio[1]) {
                $width = $height * $ratio[1];
                $x = (imagesx($image) - $width) / 2;
            }

            else if ($ratio[0] < $ratio[1]) {
                $height = $width / $ratio[1];
                $y = (imagesy($image) - $height) / 2;
            }

        }

        /*
        Resize Section
        */

        if (is_null($size) === true) {
            $size = array($width, $height);
        }

        else {
            $size = array_filter(explode('x', $size));

            if (empty($size) === true) {
                    $size = array(imagesx($image), imagesy($image));
            } else {
                if ((empty($size[0]) === true) || (is_numeric($size[0]) === false)) {
                        $size[0] = round($size[1] * $width / $height);
                } else if ((empty($size[1]) === true) || (is_numeric($size[1]) === false)) {
                        $size[1] = round($size[0] * $height / $width);
                }
            }
        }

       $result = ImageCreateTrueColor($size[0], $size[1]);

        if (is_resource($result) === true) {
            ImageSaveAlpha($result, true);
            ImageAlphaBlending($result, true);
            ImageFill($result, 0, 0, ImageColorAllocate($result, 255, 255, 255));
            ImageCopyResampled($result, $image, 0, 0, $x, $y, $size[0], $size[1], $width, $height);

            ImageInterlace($result, true);
            ImageJPEG($result, $fileName, 100);
        }
    }

    return false;
}

function makedir($dir, $mode = 0777) {
  if (is_dir($dir) || @mkdir($dir,$mode)) return TRUE;
  if (!makedir(dirname($dir),$mode)) return FALSE;
  return @mkdir($dir,$mode);
}
function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
       } 
     } 
     reset($objects); 
     rmdir($dir); 
 
   }
   echo ">>> dir deleted: $dir\n"; 
 } 




$pathFrom = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/000_partners_to_resize";
$pathTo = "/var/www/hosts/pihenni.hu/htdocs/static.pihenni/uploaded_images/accomodation";
$typeNew = "uploaded_images/accomodation";


$dirlist = @dirList("$pathFrom");


foreach($dirlist as $partnerID) {

	$accID = @dirList("$pathFrom/$partnerID");

	sort($accID);
	
	if(is_dir("$pathTo/$partnerID/$accID[0]") == TRUE) {
		rrmdir("$pathTo/$partnerID/$accID[0]");
	}
		makedir("$pathTo/$partnerID/$accID[0]/orig/");
		makedir("$pathTo/$partnerID/$accID[0]/79_79/");	//pihenni.hu
		makedir("$pathTo/$partnerID/$accID[0]/90_90/");	//pihenni.hu
		makedir("$pathTo/$partnerID/$accID[0]/137_119/");	//pihenni.hu
		makedir("$pathTo/$partnerID/$accID[0]/137_137/");	//pihenni.hu
		makedir("$pathTo/$partnerID/$accID[0]/169_172/");	//pihenni.hu
		makedir("$pathTo/$partnerID/$accID[0]/171_99/");	//pihenni.hu
		makedir("$pathTo/$partnerID/$accID[0]/177_124/");	//pihenni.hu
		makedir("$pathTo/$partnerID/$accID[0]/297_222/");
		makedir("$pathTo/$partnerID/$accID[0]/188_75/");
		makedir("$pathTo/$partnerID/$accID[0]/115_77/");

		makedir("$pathTo/$partnerID/$accID[0]/364_252/");
		makedir("$pathTo/$partnerID/$accID[0]/73_54/");
			
	
		makedir("$pathTo/$partnerID/$accID[0]/134_181/");
		makedir("$pathTo/$partnerID/$accID[0]/140_85/");
		makedir("$pathTo/$partnerID/$accID[0]/189_90/");
		makedir("$pathTo/$partnerID/$accID[0]/280_172/");
		
		
		makedir("$pathTo/$partnerID/$accID[0]/299_174/");
		makedir("$pathTo/$partnerID/$accID[0]/115_77/");
		makedir("$pathTo/$partnerID/$accID[0]/298_72/");
		makedir("$pathTo/$partnerID/$accID[0]/226_82/");
		



	echo "required dirs created\n";

	$newImageArray = @dirList("$pathFrom/$partnerID/$accID[0]/orig");
	
	sort($newImageArray);
	
	echo $partnerID." $accID[0]\n";

	$good_image_names = array();
	
	foreach($newImageArray as $key => $array) {
		$extension = end(explode(".", $array));
		$filename = reset(explode(".", $array));
		
		if(strlen($filename) == 1) {
			$addLeadingZero = "0$filename.$extension";
			rename("$pathFrom/$partnerID/$accID[0]/orig/$filename.$extension", "$pathFrom/$partnerID/$accID[0]/orig/$addLeadingZero");
			echo "images renamed";
		}
		else {
			$addLeadingZero = "$filename.$extension";
			$origImageName = explode(".",$addLeadingZero);
			//echo "FROM $pathFrom/$partnerID/$accID[0]/orig/$filename.$extension >>>> $pathFrom/$partnerID/$accID[0]/orig/$origImageName[0].jpg\n";
			//die;
			//rename("$pathFrom/$partnerID/$accID[0]/orig/$filename.$extension", "$pathFrom/$partnerID/$accID[0]/orig/$addLeadingZero.$extension");
		}
		if($extension <> "db")
			$good_image_names[] = $addLeadingZero; 
	
	}
	sort($good_image_names);
	
	
	foreach($good_image_names as $key => $array) {		
		$newFileName = $key+1;
		if($newFileName<10)
			$newFileName = "0$newFileName";
		else
			$newFileNAme = $newFileName;
			
		copy("$pathFrom/$partnerID/$accID[0]/orig/$array", "$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg");
		echo "$pathFrom/$partnerID/$accID[0]/orig/$array >$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg\n";
		
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.43:1', '177x',"$pathTo/$partnerID/$accID[0]/177_124/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.73:1', '171x',"$pathTo/$partnerID/$accID[0]/171_99/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:1.015', '169x',"$pathTo/$partnerID/$accID[0]/169_172/$newFileName.jpg");
    Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:1', '137x',"$pathTo/$partnerID/$accID[0]/137_137/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:1', '90x',"$pathTo/$partnerID/$accID[0]/90_90/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:0.67', '115x',"$pathTo/$partnerID/$accID[0]/115_77/$newFileName.jpg");

	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:1', '79x',"$pathTo/$partnerID/$accID[0]/79_79/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.15:1', '137x',"$pathTo/$partnerID/$accID[0]/137_119/$newFileName.jpg");
 	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.335:1', '297x',"$pathTo/$partnerID/$accID[0]/297_222/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '2.5:1', '188x',"$pathTo/$partnerID/$accID[0]/188_75/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.445:1', '364x',"$pathTo/$partnerID/$accID[0]/364_252/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.4:1', '73x',"$pathTo/$partnerID/$accID[0]/73_54/$newFileName.jpg");
	// '1.4:1', '73x'
	
	
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '0.739:1', '134x',"$pathTo/$partnerID/$accID[0]/134_181/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.65:1', '140x',"$pathTo/$partnerID/$accID[0]/140_85/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '2.1:1', '189x',"$pathTo/$partnerID/$accID[0]/189_90/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1.63:1', '280x',"$pathTo/$partnerID/$accID[0]/280_172/$newFileName.jpg");

		
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:0.65', '299x',"$pathTo/$partnerID/$accID[0]/299_174/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:0.67', '115x',"$pathTo/$partnerID/$accID[0]/115_77/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:0.24', '298x',"$pathTo/$partnerID/$accID[0]/298_72/$newFileName.jpg");
	Image("$pathTo/$partnerID/$accID[0]/orig/$newFileName.jpg", '1:0.365', '226x',"$pathTo/$partnerID/$accID[0]/226_82/$newFileName.jpg");



			echo " >>>>>> regi: $array - uj: $newFileName.jpg\n";
			
	usleep(3000);
	}
	
	//do actual data
	
	$realImageNum = num_files($partnerID,$accID[0],$typeNew);


	$query  = "UPDATE ACCOMODATION SET IMAGE_NUM = $realImageNum,IMAGE_GEN = '1' WHERE ID = $accID[0]" ;
	$stmt = oci_parse($dbconn, $query);
	oci_execute($stmt, OCI_DEFAULT);
	$r = oci_commit($dbconn);
		if (!$r) 
		{
	    	$e = oci_error($conn);
	    	echo "ERROR: $e\n";
		}
		else 
		{
			echo "UPDATED IMAGE NUM: $realImageNum | $partner_name\n";
		}
	$i++;
	
	
	ob_flush();
	flush();
		
	//sleep
	usleep(3000);
	
	echo "<br/>torolni: $pathFrom/$partnerID<hr/>";
	//rrmdir("$pathFrom/$partnerID");		
}

echo "done";


ob_end_flush();


die;
?>