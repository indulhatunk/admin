<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();



if($_GET[invoice] > 0 && $_GET[sure] <> 1 && $_GET[customer] > 0) {
	die("
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan megérkezett a számla?</h2> <br/> <a href=\"?invoice=1&customer=$_GET[customer]&sure=1\" class='button green'>$lang[yes]!</a>  <a href=\"customers.php\" class='button red closefacebox'>$lang[no]!</a><div class='cleaner'></div>");
}
if($_GET[invoice] > 0 && $_GET[sure] == 1 && $_GET[customer] > 0) {
	
	$cid = (int)$_GET[customer];
	
	$mysql->query("UPDATE customers SET greenoutlet_invoice_arrived_date = NOW() WHERE cid = $cid");
	writelog("$CURUSER[username] set greenoutlet invoice arrived $_GET[customer]");
}

if($_GET[transfer] > 0 && $_GET[sure] <> 1 && $_GET[customer] > 0) {
	die("
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan megérkezett a számla?</h2> <br/> <a href=\"?transfer=1&customer=$_GET[customer]&sure=1\" class='button green'>$lang[yes]!</a>  <a href=\"customers.php\" class='button red closefacebox'>$lang[no]!</a><div class='cleaner'></div>");
}
if($_GET[transfer] > 0 && $_GET[sure] == 1 && $_GET[customer] > 0) {
	
	$cid = (int)$_GET[customer];
	
	$mysql->query("UPDATE customers SET greenoutlet_cleared_date = NOW() WHERE cid = $cid");
	writelog("$CURUSER[username] set greenoutlet invoice transferred $_GET[customer]");
}



$yr = (int)$_POST[year]; 
$inv = (int)$_POST[reseller_invoice];


if($CURUSER[userclass] < 50 && $CURUSER[username] <> 'greenoutlet')
	header("location: index.php");

head('Viszonteladói statisztika');

echo message($msg);
?>

<script>

$(document).ready(function() {


  $('.reseller_invoice').change(function() {
  	var invoice_id = $(this).attr("invoice_id");
  	var year = $(this).attr("year");

  	if (confirm('Biztosan megérkezett a '+year+'/'+ invoice_id+' sz. számla?')) {
    	$("#form-"+year+"-"+invoice_id).submit();
    } else {
   		return false;
   	}
  });
  
    $('.reseller_transfer').change(function() {
  	var invoice_id = $(this).attr("invoice_id");
  	var year = $(this).attr("year");

  	if (confirm('Biztosan megérkezett a '+year+'/'+ invoice_id+' sz. ellenértéke?')) {
    	$("#form-"+year+"-"+invoice_id).submit();
    } else {
   		return false;
   	}
  });

  
 }); 
 
</script>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href='?showinvoices=1' class='<? if($_GET[all] <> 1) echo "current";?>'>Elszámolások</a></li>
		<!--		<li><a href='?all=1' class='<? if($_GET[all] == 1) echo "current"?>'>Viszonteladói statisztika</a></li>
		<li><a href='?send_reseller=1' rel='facebox'>Elszámolás elküldése</a></li>-->


	</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<?

 if($_GET[all] == 1)
{ 
?>
<table>
<tr class='header'>
	<td>Partner</td>
	<td width='80'>1. eladás</td>
	<td>Eladott</td>
	<td>Fizetett</td>
	<td>Fizetett összeg</td>
	<td></td>
</tr>
<?

	$partners = $mysql->query("SELECT * FROM partners WHERE userclass = 5 ORDER BY company_name ASC");
	while($arr = mysql_fetch_assoc($partners))
	{
		$paid = mysql_fetch_assoc($mysql->query("SELECT count(cid) as cnt, sum(orig_price) as tot FROM customers WHERE reseller_id = $arr[pid] AND paid = 1 AND inactive = 0"));
		$total = mysql_fetch_assoc($mysql->query("SELECT count(cid) as cnt FROM customers WHERE reseller_id = $arr[pid] AND inactive = 0"));
		
		$first = mysql_fetch_assoc($mysql->query("SELECT added FROM customers WHERE reseller_id = $arr[pid] AND inactive = 0 ORDER BY cid ASC LIMIT 1"));
		$first = explode(" ",$first[added]);
		
		if($first == '0000-00-00')
			$first = '';
			
		$tt = $total[cnt] + $tt;
		$pp = $paid[cnt] + $pp;
		$tot = $paid[tot] + $tot;
		if($_GET[reseller] == $arr[pid])
			$class = 'grey';
		else
			$class = '';
		
		if($paid[cnt] > 0)
			$info = "<a href='info/show_accounting_partner.php?pid=$arr[pid]' rel='facebox'>[elszámolás]</a>";
		else
			$info = '';
		echo "<tr class='$class'>";
			echo "<td>$arr[company_name]</td>";
			echo "<td align='center'>$first[0]</td>";
			echo "<td align='right'>$total[cnt] db</td>";
			echo "<td align='right'>$paid[cnt] db</td>";
				echo "<td align='right'>".formatPrice($paid[tot])."</td>";
			echo "<td><a href='?reseller=$arr[pid]&all=1'>[részletek]</a></td>";
			//echo "<td>$info</td>";
		echo "</tr>";
		
		
		
		if($_GET[reseller] == $arr[pid])
		{
			echo "<tr class='grey'><td colspan='6'><table bgcolor='white'>";
			$i=1;
			$customers = $mysql->query("SELECT orig_price,pid,sub_pid,offer_id, name,paid FROM customers WHERE reseller_id = $arr[pid] AND inactive = 0 ORDER BY paid, added DESC");
			
		
			while($o = mysql_fetch_assoc($customers) )
			{
			
				if($o[sub_pid] > 0)
					$pid = $o[sub_pid];
				else
					$pid = $o[pid];
				
					$p = mysql_fetch_assoc($mysql->query("SELECT hotel_name FROM partners WHERE pid = '$pid' LIMIT 1"));



				if($o[paid] == 1)
					$cls = 'green';
				else
					$cls = 'white';
			echo "
			<tr class='$cls'>
				<td>$i. $p[hotel_name]</td>
				<td>$o[offer_id]</td>
				<td>$o[name]</td>	
				<td>".formatPrice($o[orig_price])."</td>	
			</tr>
			";
				$i++;
			}
			echo "</table></td></tr>";
		}
		
	}
		echo "<tr class='grey'>";
			echo "<td colspan='2'>Összesen</td>";
			echo "<td align='right'>$tt db</td>";
			echo "<td align='right'>$pp db</td>";
			echo "<td align='right'>".formatPrice($tot)."</td>";
			echo "<td colspan='1'></td>";
		echo "</tr>";
?>
</table>
<? } else { 
	
	/*	echo "
	<div style='text-align:center;padding:0 0 10px 0;'><select name='reseller_office'>
		<option value=''>Kérem válasszon partnerirodát</option>
	</select></div>
	";*/
	
	echo "<table>";
	
	echo "<tr class='header'>";
			echo "<td>Iroda</td>";
			echo "<td>Utalványszám</td>";
			echo "<td>Név</td>";
			echo "<td>Értékesítő</td>";
			
			echo "<td>%</td>";
			echo "<td>Fizetett</td>";
			
			echo "<td>Jut.</td>";
			echo "<td>Utalandó.</td>";
			echo "<td colspan='2'></td>";
		echo "</tr>";


	

	//$query = $mysql->query("SELECT * FROM customers WHERE reseller_invoice <> '' GROUP BY year(reseller_invoice_date), reseller_invoice ORDER BY reseller_invoice ASC");
	$query = $mysql->query("SELECT * FROM customers INNER JOIN partners ON partners.pid = customers.reseller_id  WHERE reseller_invoice <> '' AND reseller_id = 3557 GROUP BY reseller_office ORDER BY reseller_invoice_date ASC");

	$reseller = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '3557' LIMIT 1"));


	while($arr = mysql_fetch_assoc($query))
	{
		//echo "$arr[reseller_invoice_date] $arr[reseller_invoice] $arr[reseller_id]<hr/>";
		
		$dt = explode(" ",$arr[reseller_invoice_date]);
		$dt = str_replace("-",'',$dt[0]);
		
		
		if($dt < '20130707')
			$field = "actual_price";
		else
			$field = "orig_price";
			
			
		$year = explode("-",$arr[reseller_invoice_date]);
		$year = $year[0];

			

	
			$off = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = '$arr[reseller_office]' LIMIT 1"));

			echo "<tr class='header'>";
				echo "<td colspan='10'>$off[name]</td>"; //$year/".str_pad($arr[reseller_invoice], 5, "0", STR_PAD_LEFT)." 
			echo "</tr>";	

			//echo "<tr><td><table>";
				$ck = $mysql->query("SELECT * FROM customers WHERE reseller_invoice <> '' AND reseller_id = 3557 AND reseller_office = '$arr[reseller_office]'  ORDER BY reseller_invoice_date ASC");
				
				$yield = '';
				$total = '';
				
				while($a = mysql_fetch_assoc($ck))
				{
					$a[orig_price] = $a[orig_price]-$a[discount_value];
					
					if($a[discount_code] <> '')	
					{
						$field2 = "yield_reseller_promotion";
					
					}
					else
						$field2 = "yield_reseller_base";

					$yld = $a[$field]*($reseller[$field2]/100);

					
					$add = explode(" ",$a[added]);
					
					
					if($a[greenoutlet_invoice_arrived_date] <> '0000-00-00 00:00:00' && $a[greenoutlet_cleared_date] == '0000-00-00 00:00:00')
					{
						$class='red';
					}
					elseif($a[greenoutlet_invoice_arrived_date] <> '0000-00-00 00:00:00' && $a[greenoutlet_cleared_date] <> '0000-00-00 00:00:00')
					{
						$class='grey';
					}
					else
						$class = '';	
					echo "<tr class='$class'>";
						echo "<td width='85' align='center'>$add[0]</td>";
						echo "<td>$a[name]</td>";
						echo "<td width='85' align='center'>$a[offer_id]</td>";
					//	echo "<td>$a[name]</td>";
						echo "<td>$a[reseller_agent]</td>";
						
						echo "<td align='center'>".$reseller[$field2]."%</td>";
						echo "<td align='right'>".formatPrice($a[orig_price])."</td>";
						echo "<td align='right'>".formatPrice($yld)."</td>";
						echo "<td align='right'>".formatPrice($a[orig_price]-$yld)."</td>";
						
						if($a[greenoutlet_invoice_arrived_date] <> '0000-00-00 00:00:00')
						{
							$cd = explode(" ",$a[greenoutlet_invoice_arrived_date]);
							echo "<td align='center'>$cd[0]</td>";
						}
						else
							echo "<td align='center'><a href='?invoice=1&customer=$a[cid]' rel='facebox'>[számla itt]</a></td>";
					
						
						if($a[greenoutlet_cleared_date] <> '0000-00-00 00:00:00')
						{
							$cd = explode(" ",$a[greenoutlet_cleared_date]);
							echo "<td align='center'>$cd[0]</td>";
						}
						elseif($a[greenoutlet_invoice_arrived_date] <> '0000-00-00 00:00:00')
							echo "<td align='center'><a href='?transfer=1&customer=$a[cid]' rel='facebox'>[utalás itt]</a></td>";
						else
							echo "<td align='center' width='80'>-</td>";

					echo "</tr>";
					
					$yield = $yield + $yld;
					$total = $total + $a[orig_price];
				}
			
			echo "<tr class='header'>";
				echo "<td colspan='5'>Összesen</td>"; 
				echo "<td align='right'>".formatPrice($total)."</td>";
				echo "<td align='right'>".formatPrice($yield)."</td>";
				echo "<td align='right'>".formatPrice($total-$yield)."</td>";  
				echo "<td colspan='2'></td>";
			echo "</tr>
			<tr>";
				echo "<td colspan='10' style='height:3px'></td>"; //$year/".str_pad($arr[reseller_invoice], 5, "0", STR_PAD_LEFT)." 
			echo "</tr>";
		//	echo "</table></td></tr>";
			/*	echo "<form method='post' id='form-$year-$arr[reseller_invoice]'>
			<input type='hidden' name='year' value='$year'/>
			<input type='hidden' name='reseller_invoice' value='$arr[reseller_invoice]'/><tr class='$cls'>
			<input type='hidden' name='invoice_state' value='$arr[reseller_invoice_arrived_date]'/><tr class='$cls'>
			";

			echo "<td align='center'>$year/".str_pad($arr[reseller_invoice], 5, "0", STR_PAD_LEFT)."</td>";
			echo "<td>$partner[hotel_name]<br/>$partner[company_name]</td>";
			echo "<td align='right'>".formatPrice($total[total])."</td>";
			echo "<td align='right' class='$warn'>".formatPrice($yield)."</td>";
			
			
			if($arr[reseller_invoice_arrived_date] <> '0000-00-00 00:00:00')
				$invoice_arrived = 'selected';
			else	
				$invoice_arrived = '';
				
			if($arr[reseller_cleared_date] <> '0000-00-00 00:00:00')
				$transfer_arrived = 'selected';
			else	
				$transfer_arrived = '';
		
			echo "<td align='center'><select name='invoice' class='reseller_invoice' invoice_id='$arr[reseller_invoice]' year='$year'>
				<option value='0'>számlára vár</option>
				<option value='1' $invoice_arrived>számla itt</option>
			</select></td>";
			echo "<td align='center'><select name='transfer' class='reseller_transfer' invoice_id='$arr[reseller_invoice]' year='$year'>
				<option value='0'>kiegyenlítésre vár</option>
				<option value='1' $transfer_arrived>utalva ($arr[reseller_cleared_date])</option>
			</select></td>";
			
			echo "<td align='center'><a href='info/show_accounting_partner.php?pid=$arr[reseller_id]&year=$year&invoice_number=$arr[reseller_invoice]' rel='facebox'>[elszámolás]</a></td>";
		echo "</tr></form>";*/
		
		

	}
	echo "</table>";
	
} ?>
</div></div>
<? foot(); ?>