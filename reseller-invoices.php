<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();



head('Elszámolások, utalások listája');
?>

<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href='#' class='current'>Összes elszámolás</a></li>
	</ul>
<div class="clear"></div>
</div>
<div class='contentpadding'>
<?

$query = $mysql->query("SELECT * FROM customers WHERE reseller_id = '$CURUSER[pid]' AND reseller_invoice <> '' GROUP BY year(reseller_invoice_date), reseller_invoice ORDER BY reseller_invoice_date ASC");


if($CURUSER[yield_reseller_base] == 0)	
{	
	echo message("Az Ön jutalékát még nem állították be. Kérjük vegye fel a kapcsolatot az ügyintézőnkkel!");
	foot();
	die;
}
if(mysql_num_rows($query) == 0)
	echo "<div class='redmessage'>Még nem készült heti elszámolás</div>";
else
{
	echo "<table border=1>";
	echo "<tr class='header'>";
	echo "<td>Azonosító</td>";
	echo "<td>Dátum</td>";
	echo "<td>Fiz. hat.</td>";
	//echo "<td>Bevétel</td>";
	//	echo "<td>Jutalék</td>";
	echo "<td colspan='2' align='center'></td>";
	echo "</tr>";
}

while($arr = mysql_fetch_assoc($query))
{

	if($arr[reseller_cleared] == 1)
	{
		$invoice = '<img src="/images/check1.png" width="25" alt="Az utalás megérkezett! Köszönjük!" title="Az utalása megérkezett! Köszönjük!"/>';
		$class = '';
	}
	else
	{
		$invoice = '<img src="/images/cross1.png" width="25" alt="Kérjük, hogy az utalást mihamarabb indítsa el felénk!" title="Kérjük, hogy az utalást mihamarabb indítsa el felénk!"/>';
		$class = 'red';
	}
	
	$tot = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as ototal, sum(actual_price) as atotal FROM customers WHERE reseller_invoice = '$arr[reseller_invoice]' AND reseller_id = '$CURUSER[pid]'"));
	
	
	$total = $tot[ototal];
	$yield = $tot[atotal]*($CURUSER[yield_reseller_base]/100);
	
	$transfer = $total - $yield;
	
	
	$year = explode("-",$arr[reseller_invoice_date]);
$year = $year[0];

$invoice_number = "$year/".str_pad($arr[reseller_invoice], 5, "0", STR_PAD_LEFT);	



	echo "<tr class='$class'>";
	echo "<td width='50' align='center'>RE$invoice_number</td>";
	echo "<td width='120' align='center'>".date("Y.m.d.",strtotime($arr[reseller_invoice_date]))."</td>";
	echo "<td width='120' align='center'>". date("Y.m.d.",strtotime(date("Y-m-d", strtotime(date($arr[reseller_invoice_date]))) . " +8 day"))."</td>";
	//echo "<td align='right'>".formatPrice($total)."</td>";
	//echo "<td align='right'>".formatPrice($yield)."</td>";
	echo "<td width='20' align='center'><a href='/info/show_accounting_partner.php?pid=$CURUSER[pid]&year=$year&invoice_number=$arr[reseller_invoice]' rel='facebox'><img src='/images/getinfo.png' width='25'/></a></td>";
	echo "<td width='25'>$invoice</td>";		
			
	echo "</tr>";

}


echo "</table>";


?>
</div></div>
<?
foot(); ?>