<?
/*
 * reviews.php 
 *
 * the review page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");

userlogin();

$reported = (int)$_GET[reported];
if($reported > 0)
{
	$repdata[report] = 1;
	$mysql->query_update("reviews",$repdata,"id=$reported");
	die;
}

if($CURUSER[userclass] < 50)
{
	$pid = $CURUSER[pid];
	$hotelquery = "AND partner_id = $CURUSER[pid]";
}
else
{
	$pid = (int)$_GET[pid];
	$cpid = mysql_fetch_assoc($mysql->query("SELECT pid FROM partners WHERE coredb_id = $pid ORDER BY pid DESC LIMIT 1"));
	
	if($cpid[pid] == '')
	{
		head("Nincs értékelés");
		echo message("Nincs értékelés");
		foot();
		die;
		
	}
	$hotelquery = "AND partner_id = $cpid[pid]";
}

if($_GET[highlight] > 0)
{
	if($_GET[highlighted] == 0)
		$mysql->query("UPDATE reviews SET highlight = 1 WHERE id = '$_GET[highlight]'");
	else
		$mysql->query("UPDATE reviews SET highlight = 0 WHERE id = '$_GET[highlight]'");
}
head('Értékelések');


?>

<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
	<? if($CURUSER[userclass] > 90) { ?>
				<li><a href='accomodation.php'>Szálláshelyek</a></li>
		<? } ?>

		<li><a href="?report=0" class="<? if($_GET[report] == 0 || $_GET[report] == '') echo "current";?>">Értékelések</a></li>
		<li><a href="?report=1"  class="<? if($_GET[report] == 1) echo  "current";?>">Jelentett értékelések</a></li>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<?
if($CURUSER[userclass] > 50) { 
$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM accomodation WHERE id > 0 AND partner_id = $pid AND package = 0 LIMIT 1"));




if($partner[id] > 0) { ?>
	<fieldset>
		<legend>Eszköztár</legend>
		<table style='text-align:center;'>
			<tr>
				<td colspan='5' align='center' style='font-weight:bold;'><?=$partner[name]?> szobatípusai</td>
			</tr>
			<tr>
				<td><a href='accomodation.php?id=<?=$partner[partner_id]?>&add=1'>[partner szerkesztése]</a></td>
				<td><a href='editoffers.php?pid=<?=$partner[partner_id]?>'>[csomagok]</a></td>
				<td><a href='rooms.php?pid=<?=$partner[partner_id]?>'>[szobatípusok]</a></td>
				<td><a href='partner-domain.php?pid=<?=$partner[partner_id]?>'>[domainek]</a></td>
				<td  class='header'><a href='reviews.php?pid=<?=$partner[partner_id]?>'>[értékelések]</a></td>
			</tr>
		</table>
	</fieldset>
	<hr/>
<? } }


if($_GET[activate] > 0)
{
	echo message("Sikeresen aktiválta!");
	
	$mysql->query("UPDATE reviews SET report = 0 WHERE id = '$_GET[activate]'");
}

//echo " <a href=\"reviews.php\"  class=\"white button\">Összes hozzászólás</a> ";
//echo " <a href=\"reviews.php?report=1\"  class=\"red button\">Jelentett hozzászólások</a> <div class='cleaner'></div>";



if($_GET[report] == 1)
{
	$report = 'AND report = 1';
	//$hotelquery = '';
}
else
{

	/*if($pid > 0)
		$hotelquery = "AND partner_id = $cpid";
	else
	{
		$hotelquery = "AND added > DATE_SUB(CURDATE(), INTERVAL 7 DAY)";
	}*/
}

$query = $mysql->query("SELECT * FROM `reviews` WHERE added <> '0000-00-00 00:00:00' $report $hotelquery ORDER BY id DESC");


if(!empty($_POST[reviews]))
{

	foreach($_POST[reviews] as $review)
	{
		$mysql->query("DELETE FROM reviews WHERE id = '$review'");
	}
}


if($CURUSER[hotel_name] == '')
{
?>
<form method='get' action=''>
<select name='hotel' onchange='submit()'  style='width:430px;'>
	<option value='0'>Hotel neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(hotel_name) <> '' order by hotel_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[hotel_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[hotel_name]</option>";
		}
	?>
</select>
<hr/>
</form><?
}
echo "<h1>$partner[hotel_name]</h1>";

echo "<table>";

echo "<form method='post'>";
echo "<tr class='header'>";
		echo "<td>Dátum</td>";
		echo "<td>Partner</td>";

		echo "<td>Tisztaság</td>";
		echo "<td>Szoba</td>";
		echo "<td>Ár/érték</td>";
		echo "<td>Étel</td>";	
		echo "<td>Kiszolgálás</td>";	
		echo "<td>Vélemény</td>";
		
		echo "<td>Jelent</td>";
		if($CURUSER[userclass] > 10)
			echo "<td>Töröl</td>";
	echo "</tr>";
	
$i=1;


while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM `partners` WHERE pid = $arr[partner_id] LIMIT 1"));


	
if($arr[report] == 1)
{
	$class = 'red';
	$dbutton = 'disabled';
}
elseif($arr[highlight] == 1)
{
	$class = 'blue';
	//$dbutton = 'disabled';
}
else
{
	$dbutton = '';
	$class = '';
}	
		echo "<tr class='$class' id='row-$arr[id]'>";
		echo "<td width='125'>$arr[added]</td>";
		echo "<td width='200'>$partner[hotel_name]</td>";
		echo "<td align='center'>$arr[cleanliness]</td>";
		echo "<td align='center'>$arr[room]</td>";
		echo "<td align='center'>$arr[pricevalue]</td>";
		echo "<td align='center'>$arr[food]</td>";	
		echo "<td align='center'>$arr[service]</td>";	
		echo "<td>$arr[comment]</td>";
		
		echo "<td><a href='?activate=$arr[id]&report=1'>[visszaállít]</a></td>";
		
		echo "<td><a href='?pid=$pid&highlight=$arr[id]&highlighted=$arr[highlight]'>[kiemel]</a></td>";

		echo "<td align='center'><input type='button' class='report' value='Jelentem!' id='$arr[id]' $dbutton/></td>";

		
		if($CURUSER[userclass] >  10)
			echo "<td align='center'><input type='checkbox' name='reviews[]' value='$arr[id]'/></td>";
		
		
	echo "</tr>";
	
	$clean = $clean + $arr[cleanliness];
	$room = $room + $arr[room];
	$pricevalue = $pricevalue + $arr[pricevalue];
	$food = $food + $arr[food];
	$service = $service + $arr[service];
	
	$i++;
}
$i = $i-1;

if($i==0)
	$i = 1;


echo "<tr class='header'>";
		echo "<td colspan='2'>-</td>";
		echo "<td>".round($clean/$i,2)."</td>";
		echo "<td>".round($room/$i,2)."</td>";
		echo "<td>".round($pricevalue/$i,2)."</td>";
		echo "<td>".round($food/$i,2)."</td>";	
		echo "<td>".round($service/$i,2)."</td>";	
		echo "<td>-</td>";
		echo "<td>-</td>";
		
		if($CURUSER[userclass] > 10)
			echo "<td><input type='submit' value='töröl'/></td>";
echo "</tr>";

echo "</form>";
echo "</table></div></div>";
foot();
?>