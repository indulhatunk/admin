<?
include('../../../inc/tour.init.inc.php');
head();
?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js" charset="UTF-8"></script>
<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/smoothness/jquery-ui.min.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../js/js/css/jquery.ui.plupload.css" media="screen" />



<script type="text/javascript" src="../js/plupload.full.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="../js/js/jquery.ui.plupload.min.js" charset="UTF-8"></script>


<div id="uploader">
	<p>Árvíztűrő tükörfúrógép</p>
</div>

<script type="text/javascript">
// Initialize the widget when the DOM is ready
$(function() {


plupload.addI18n({"Stop Upload":"Feltöltés leállítása","Upload URL might be wrong or doesn't exist.":"A feltöltő URL hibás vagy nem létezik.","tb":"","Size":"Méret","Close":"Bezárás","Init error.":"Init hiba.","Add files to the upload queue and click the start button.":"A fájlok feltöltési sorhoz való hozzáadása után az Indítás gombra kell kattintani.","Filename":"Fájlnév","Image format either wrong or not supported.":"Rossz vagy nem támogatott képformátum.","Status":"Állapot","HTTP Error.":"HTTP-hiba.","Start Upload":"Feltöltés indítása","mb":"","kb":"","Duplicate file error.":"Duplikáltfájl-hiba.","File size error.":"Hibás fájlméret.","N/A":"Nem elérhető","gb":"","Error: Invalid file extension:":"Hiba: érvénytelen fájlkiterjesztés:","Select files":"Fájlok kiválasztása","%s already present in the queue.":"%s már szerepel a listában.","File: %s":"Fájl: %s","b":"b","Uploaded %d/%d files":"Feltöltött fájlok: %d/%d","Upload element accepts only %d file(s) at a time. Extra files were stripped.":"A feltöltés egyszerre csak %d fájlt fogad el, a többi fájl nem lesz feltöltve.","%d files queued":"%d fájl sorbaállítva","File: %s, size: %d, max file size: %d":"Fájl: %s, méret: %d, legnagyobb fájlméret: %d","Drag files here.":"Ide lehet húzni a fájlokat.","Runtime ran out of available memory.":"Futásidőben elfogyott a rendelkezésre álló memória.","File count error.":"A fájlok számával kapcsolatos hiba.","File extension error.":"Hibás fájlkiterjesztés.","Error: File too large:":"Hiba: a fájl túl nagy:","Add Files":"Fájlok hozzáadása"});

		
	$("#uploader").plupload({
		// General settings
		runtimes : 'html5',
		url : "dump.php",
		max_file_count: 2,
		// Maximum file size
		max_file_size : '2mb',

		chunk_size: '1mb',

		// Resize images on clientside if we can
		/*resize : {
			width : 200, 
			height : 200, 
			quality : 90,
			crop: true // crop to exact dimensions
		},
		*/
		// Specify what files to browse for
		filters : [
			{title : "Image files", extensions : "jpg,png,jpeg,JPG,JPEG,PNG"},
		//	{title : "Zip files", extensions : "zip,avi"}
		],

		// Rename files by clicking on their titles
		rename: false,
		
		// Sort files
		sortable: true,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,

		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},

		// Flash settings
		//flash_swf_url : '/plupload/js/Moxie.swf',
	
		// Silverlight settings
		//silverlight_xap_url : '/plupload/js/Moxie.xap',
		    // Post init events, bound after the internal events
        init : {
             UploadComplete: function(up, files) {
              
               $("#uploader").hide();
            },
 
         
            Error: function(up, args) {
                // Called when error occurs
                console.log('[Error] ', args);
            }
        }
  
	});
});
</script>
