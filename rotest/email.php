<?
header('Content-type: text/html; charset=utf-8');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>[@subject]</title>
	<style type="text/css">
		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" bgcolor='e8e8e8'>
	<tr>
		<td valign="top" align='center'> 
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ec381a'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='40' align='left'><a href="http://img.hotel-world.hu"><img  class="image_fix" alt="Az összes ajánlat megtekintése" title="Az összes ajánlat megtekintése" src='http://img.hotel-world.hu/images/newlogo.jpg' width='230' height='40'/></a></td>
						<td width="200" valign="middle" align='right' style='color:white;'>A legjobb szállások Neked válogatva!</td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='ffffff'>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
					<tr>
						<td width="200" valign="top" height='10' align='left' style='color:#ec381a; font-size:11px;'>SZÁLLÁS OUTLET HÍRLEVÉL - 2014-04-12</td>
						<td width="200" valign="middle" align='right' style='color:#ec381a; font-size:11px;'>
							<a href="http://img.hotel-world.hu" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">BELFÖLD</a>  |  <a href="http://img.hotel-world.hu" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">VÍZPART</a>  |  <a href="http://img.hotel-world.hu" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">WELLNESS</a>  |  <a href="http://img.hotel-world.hu" target ="_blank" title="" style="color: #ec381a; text-decoration: none;">GYERMEKBARÁT</a></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
			
			<? for($i=1;$i<3;$i++) {?>
			
			<tr>
				<td valign='top'>
				<!-- -->
					<a href="http://img.hotel-world.huu" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top'>
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='560'>
								<tr>
									<td width='10'></td>
									<td width='110' style='font-size:38px;color:#ec381a;font-weight:bold;'>-99%</td>
									<td style='font-size:16px;font-weight:bold;line-height:18px'>
										<span style='color:#ed391b'>OFFER08 CITY - ARIAL BOLD 16PT</span> <br/>
										
										<span style='color:#585858'>Offer08 title - Arial Bold 16pt</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="Partnereink" title="Partnereink" src='http://img.hotel-world.hu/images/im1.png' width='560' height='240'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='170' style='font-size:28px;font-weight:bold;color:#7db300'>
												<div style='font-size:28px;line-height:28px; height:28px;color:#7db300;font-weight:bold;'>55 900 Ft</div>
												<div style='color:#666666;font-size:11px;font-weight:bold;'>OUTLET ÁR</div>
											</td>
											<td height='38' valign='top' width='180' style='color:#bbbbbb; font-weight:normal;'>
												<div style="font-size:28px;line-height:28px; height:28px;">55 900 Ft</div>
												<div style='color:#bbbbbb;font-size:11px;'>EREDETI ÁR</div>
											</td>
											<td width='200' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
				<!-- -->	
				</td>
				
			</tr>
			
			<tr>	
				<td height='15'></td>
			</tr>
			
			<? } ?>
			
			<!-- -->
				<tr>
				<td valign='top'>
				<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600'>
					<tr>
					<td valign='top'>
					<a href="http://img.hotel-world.huu" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='360' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='320'>
								<tr>
									<td width='10' height='40'></td>
									<td width='110' style='font-size:30px;color:#ec381a;font-weight:bold;'>-99%</td>
									<td style='font-size:16px;font-weight:bold;line-height:14px'>
										<span style='color:#ed391b'>OFFER08 CITY</span> <br/>
										
										<span style='color:#585858'>Offer08 title</span>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='15'></td>
								</tr>
								<tr>
									<td colspan='3'>
										<img  class="image_fix" alt="Partnereink" title="Partnereink" src='http://img.hotel-world.hu/images/im1.png' width='325' height='150'/>
									</td>
								</tr>
								<tr>
									<td colspan='3' height='25'></td>
								</tr>
								<tr>
									<td width='10'></td>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td height='38' valign='middle' width='130' style='font-size:24px;color:#7db300;font-weight:bold;'>
												<div style='font-size:24px;line-height:24px; height:24px;color:#7db300;font-weight:bold;'>55 900 Ft</div>
												<div style='color:#666666;font-size:10px;font-weight:bold;'>OUTLET ÁR</div>
											</td>
											<td width='185' height='38' bgcolor='7db300' valign='middle' style='backgrund-color:#7db300;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
					
				</td>
				
				<td width='15'></td>
				<td width='225' valign='top'>
					<!-- -->
					<a href="http://img.hotel-world.huu" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: none;">
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='225' bgcolor='white' height='300'>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						<tr>	
							<td width='20'></td>
							<td valign='top' >
								<!-- -->
								<table cellpadding="0" cellspacing="0" border="0" align="center" width='185'>
								<tr>
									<td width='2' height='40'></td>
									<td style='font-size:16px;font-weight:bold;line-height:14px'>
										<span style='color:#ed391b'>OFFER08 CITY</span> <br/>
										
										<span style='color:#585858'>Offer08 title</span>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='15'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<img  class="image_fix" alt="Partnereink" title="Partnereink" src='http://img.hotel-world.hu/images/im1.png' width='185' height='150'/>
									</td>
								</tr>
								<tr>
									<td colspan='2' height='25'></td>
								</tr>
								<tr>
									<td colspan='2'>
										<table cellpadding="0" cellspacing="0" border="0">
										<tr>
										<td width='185' height='38' bgcolor='3b3e40' valign='middle' style='backgrund-color:#3b3e40;color:white;text-align:center;font-size:16px;font-weight:bold;'>MEGNÉZEM</td>
										</tr>
										</table>
									</td>
								</tr>
								
								</table>
								<!-- -->								
							</td>
							<td width='20'></td>
						</tr>
						<tr>
							<td colspan='3' height='20'></td>
						</tr>
						
						
					</table>
					</a>
					<!-- -->
				</td>
				</tr>
				</table>
				<!-- -->	
				</td>
				
			</tr>
			
			<!-- -->
			
			
			
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:16px;color:#ed391b; font-weight:bold;' valign='middle' height='48' width='535'>
								TOVÁBBI AJÁNLATAINK
							</td>
							<td valign='middle'><img  class="image_fix" alt="Nyíl" title="Nyíl" src='http://img.hotel-world.hu/images/arrow.jpg' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			<tr>
			<td valign='top'>
			<!-- -->
				<a href="http://img.hotel-world.huu" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: none;">
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<a href="http://img.hotel-world.hu"><tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>22 000 Ft</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>Offer10 Title</b>
										<div style='padding:5px 0 0 0;'>Offer10 city</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="Nyíl" title="Nyíl" src='http://img.hotel-world.hu/images/lealkudtuks.png' width='111' height='33'/>
										
									</td>
								</tr></a>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<a href="http://img.hotel-world.hu"><tr style='border-bottom:1px solid #e8e8e8;'>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>22 000 Ft</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>Offer10 Title</b>
										<div style='padding:5px 0 0 0;'>Offer10 city</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="Nyíl" title="Nyíl" src='http://img.hotel-world.hu/images/lealkudtuks.png' width='111' height='33'/>
										
									</td>
								</tr></a>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td width='20'></td>  
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
								<a href="http://img.hotel-world.hu"><tr>
									<td width='10'></td>
									<td height='50' width='110' valign='middle' style='font-size:20px;color:#7db300;font-weight:bold;'>22 000 Ft</td>
									<td valign='middle' style='font-size:14px; color:#585858;'>
										<b>Offer10 Title</b>
										<div style='padding:5px 0 0 0;'>Offer10 city</div>
									</td>
									<td width='120' valign='middle'>
										<img  class="image_fix" alt="Nyíl" title="Nyíl" src='http://img.hotel-world.hu/images/lealkudtuks.png' width='111' height='33'/>
										
									</td>
								</tr></a>
							</table>
							</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td colspan='3' height='10'></td>
					</tr>

				</table>
				</a>
			</td>
			</tr>

	
			<tr>	
				<td height='15'></td>
			</tr>
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href="http://img.hotel-world.hu" target ="_blank" title="Leiratkozás" style="color:white; text-decoration: none;">MEGNÉZEM AZ ÖSSZES AJÁNLATOT</a></td>
			</tr>	
			<tr>	
				<td height='15'></td>
			</tr>	
			<tr>
				<td align='center' style='color:#454545;font-size:11px;line-height:14px;'>
				
A hírlevélben feltüntetett árak, képek, és a szolgáltatások leírásai tájékoztató jellegűek és a figyelem felkeltésére szolgálnak. Az ajánlatok részletes leírása a linkekre kattintva honlapunkon érhető el. Levelünket a krisztian.nagy@indulhatunk.hu címre küldtük, mive korábban feliratkozott hírlevél szolgáltatásunkra. Amennyiben nem kíván több levelet kapni tőlünk, kérjük kattintson az alábbi linkre:<br/><a href="http://img.hotel-world.hu" target ="_blank" title="Leiratkozás" style="color: #454545; text-decoration: underline;">Leiratkozás</a>

				</td>
			</tr>
			<tr>	
				<td height='10'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class="image_fix" alt="Partnereink" title="Partnereink" src='http://img.hotel-world.hu/images/plogos.jpg' width='339' height='33'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
		</table>
		<!-- End example table -->

		</td>
	</tr>
</table>  
</body>
</html>