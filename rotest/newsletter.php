<?
header('Content-Type: text/html; charset=utf-8');
	?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>

<head>
	<style  type='text/css'>
		body, html { padding:0; margin:0; font-family:arial; color:#2b2b2b; font-size:12px;}
		a img { border:none; }
		a { color:black; }
		a.white { color:white; }
		.cleaner { clear:both; }
		.title { font-size:24px; }
	</style>
	<title>Nyerjen wellness hétvégét!</title>
</head>
 <body bgcolor='#680800'>
  <table cellspacing='0' cellpadding='0' border='0' bgcolor='#680800' style='width: 100%;'>
     <tr>
        <td style='text-align:center;'>
                    
	<table width='758' cellpadding='0' cellspacing='0' border='0' bgcolor='white' style='text-align:left;margin:0 auto;'>
	<tr>
		<td style='text-align:center;padding:5px;'>
			<a href='http://admin.indulhatunk.hu/evker/newsletter.php' target='_blank' style='font-size:10px;'>Amennyiben az e-mail nem jelenik meg hibátlanul, klikkeljen ide a webes verzióért &raquo;</a>
		</td>
	</tr>
		<tr>
			<td height='132'>
	

				<a href='http://www.lmb.hu/evker2013' target='_blank'><img style='display:block' src='http://img.hotel-world.hu/images/outlet-header.jpg' 
					alt='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' 
					title='Óriási kedvezmények hazai szállodákban: indulhatunk.hu' width='758'/></a>
			</td>
		</tr>
		
		<tr>
			<td>
			
	
	
	<div style='padding:20px 0 20px 0px'>
	
			
	<table border='0' cellspacing='0' cellpadding='0' width=''>
			
	<tr>
			
	<td valign='top'>
		<h1>Kedves $c[name]!</h1>
		<div style='padding:0px 20px 10px 20px;'>
					<div style='font-size:18px;padding:0 0 0 0px;text-align:center;'>Köszönjük, hogy partnerünket a (<b>$partner[hotel_name]</b>) választotta pihenése helyszínéül.<br/><br/>
	
							Számunkra kiemelten  fontos az Ön véleménye.</div>
	
				<div style='font-size:20px;padding:10px 0 0px 0;text-align:center;'>Ha elégedett volt szolgáltatásunkkal, kérjük szavazzon ránk az év internetes kereskedője szavazáson az alábbi linkre kattintva:</div>
				</div>
			</td>
		</tr>
	</table>

		<div style='text-align:center; font-size:25px; font-weight:bold; color:#ff3c1b;padding:20px 0 20px 0'><a href='http://www.lmb.hu/evker2013' style='text-decoration:underline;color:#ff3c1b;' target='_blank'>SZAVAZOK &raquo;</a></div>
		
	</div>
	
	<div style='background-color:#2a0000;padding:10px'>
		<table cellspacing='0' cellpadding='0' border='0'>
		<tr>
		
		<td>
		<div style='padding:20px; font-size:22px; font-weight:bold;color:white;text-align:center;'>Sőt mi több! <br/><br/>
		Vásárlóink között, <br/>egy 2 éj 2 fő részére szóló wellnesshétvégét sorsolunk ki az<br/><br/> egri 
IMOLA HOTEL PLATÁN****-ba.</div> </td>
		<td width='268'>
				<a href='http://www.lmb.hu/imola' target='_blank'><img src='http://img.hotel-world.hu/images/imola.png'  alt='Szavazzon az év internetes kereskedőjére!'  title='Szavazzon az év internetes kereskedőjére!'/></a>
		</td>
		</tr>
	</table>
	</div>
	<div style='color:#ff3c1b; font-size:23px;padding:10px'>A szavazók között továbbá kisorsolásra kerül*:</div>
	
		<a href='http://lmb.hu/evker2013' target='_blank'><img src='http://img.hotel-world.hu/images/gift.jpg' alt='Szavazzon az év internetes kereskedőjére!'  title='Szavazzon az év internetes kereskedőjére!'/></a>


	<div style='padding:20px 0 20px 0;text-align:center;'>
			<b>Üdvözlettel:</b><br/>
			az indulhatunk csapata
		</div>
		
		
		<div style='padding:0 20px 0 20px;font-size:9px'>- A wellnesshétvége nyertese április 18-án kerül kihirdetésre az alábbi oldalon: <a href='http://facebook.com/indulhatunk.hu' target='_blank'>facebook.com/indulhatunk.hu</a><br/>
		- a további nyereményeket az 'Év internetes kereskedője' c. szavazás szervezője sorsolja ki.</div>
		
	
				</td>
		</tr>
		<tr>
			<td height='10'></td>
		</tr>
	</table>
	
        </td>
     </tr>
     
     <tr>
         	<td style='text-align:center;'>
     		<!-- -->
	 
	<div style='font-size:10px; text-align:center;margin:10px 0 0px 0;display:block;color:white;' id='uns'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='http://nl.lmb.hu/unsubscribe.php?email=#EMAIL#' target='_blank' style='color:white;'>linkre</a> kattintva mondhatja le.</div>
	<div style='height:30px;;text-align:center;font-size:11px;margin:10px 0 0 0;color:white;'>Minden jog fenntartva &copy; 2013</div>
	
 
     	</td>
     </tr>
  </table>
		
</body></html>