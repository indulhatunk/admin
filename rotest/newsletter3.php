<?
header('Content-Type: text/html; charset=utf-8');
	?>

<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'> 
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
	<title>Nyerj egy hétvégét a Bambara Hotel****premium-ba</title>
	<style type='text/css'>
		#outlook a {padding:0;} /* Force Outlook to provide a 'view in browser' menu link. */
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; } 
		.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */  
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { /*line-height: 100%;*/}
		#backgroundTable {margin:0; padding:0; width:100% !important; /*line-height: 100% !important;*/}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
		a img {border:none;} 
		.image_fix {display:block;}
		p {margin: 1em 0;}
		h1, h2, h3, h4, h5, h6 {color: black !important;}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		color: red !important; 
		}

		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		color: purple !important;
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #ec381a;}
		
			body { margin:0 auto; background-color: #f0f0f0; font-family:Tahoma; font-size:12px; color:#363636; }
	
		
	h1 { font-size: 20px; text-align: center; color: #0079b3; margin:0; padding:10px 0 20px 0; }
	
	h2 { font-size: 14px; text-align: left; color: #004b6f; margin:0; padding:0 0 10px 0; border-bottom:1px solid #ededed; margin:0 0 10px 0;}
	

	a img { border:none; }
	
	a {text-decoration:none; font-weight:bold; color:#0079b3}


	</style>
</head>
<body bgcolor='e8e8e8' style='font-family:Arial;font-size:12px;'>
<table cellpadding='0' cellspacing='0' border='0' id='backgroundTable' bgcolor='e8e8e8'>
	<tr>
		<td valign='top' align='center'> 
		<!-- -->
		<table cellpadding='0' cellspacing='0' border='0' align='center' width='100%' bgcolor='e8e8e8'>
			<tr>
				<td>
				<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' style='color:white;font-weight:normal;'>
					<tr>
						<td colspan='4' height='10'></td>
					</tr>
					<tr>
                    	<td width='15'></td>
						<td width='260' valign='top' height='40' align='left'><a target='_blank' href='http://www.indulhatunk.hu?utm_source=transaction_email_logo&utm_medium=email&utm_campaign=btn_minden'><img  class='image_fix' alt='Az összes ajánlat megtekintése' title='Az összes ajánlat megtekintése' src='http://indulhatunk.hu/images/indulhatunk-logo.png' width='260' height='40'/></a></td>
						<td width='310' valign='middle' align='right' style='color: #007bab;'>Karácsonyi nyeremányjáték</td>
                        <td width='15'></td>
					</tr>
					<tr>
						<td colspan='2' height='10'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
			<table cellpadding='0' cellspacing='0' border='0' align='center' width='100%' bgcolor='007bab'>
			<tr>
				<td>
				<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' style='color:007BAB;font-weight:normal;'>
					<tr>
						<td colspan='4' height='9'></td>
					</tr>
					<tr>
                    	<td width='15'></td>
						<td width='185' valign='top' height='10' align='left' style='color:#FFFFFF; font-size:11px;'>Az összes utazási ajánlat egy helyen</td>
						<td width='385' valign='middle' align='right' style='color:#FFFFFF; font-size:11px;'>
							<a href='http://www.indulhatunk.hu/akcios+ajanlatok.jsp?utm_source=newsletter&utm_medium=2015-12-09&utm_campaign=btn_belfold' target ='_blank' title='' style='color: #FFFFFF; text-decoration: none;'>BELFÖLD</a>  |  <a href='http://www.indulhatunk.hu/utazas?utm_source=newsletter&utm_medium=2015-12-09&utm_campaign=btn_kulfold' target ='_blank' title='' style='color: #FFFFFF; text-decoration: none;'>KÜLFÖLD</a>  |  <a href='http://www.indulhatunk.hu/repulojegy+london+repjegy.jsp?utm_source=newsletter&utm_medium=2015-12-09&utm_campaign=btn_repulojegy' target ='_blank' title='' style='color: #FFFFFF; text-decoration: none;'>REPÜLŐJEGY</a>  |  <a href='http://www.indulhatunk.hu/legnepszerubb-uticelok?utm_source=newsletter&utm_medium=2015-12-09&utm_campaign=btn_legnepszerubb' target ='_blank' title='' style='color: #FFFFFF; text-decoration: none;'>NÉPSZERŰ ÚTI CÉLOK</a></td>
                            <td width='15'></td>
					</tr>
					<tr>
						<td colspan='2' height='9'></td>
					</tr>
				</table>

				</td>
			</tr>
		</table>
		<!-- -->
		<table cellpadding='0' cellspacing='0' border='0' align='center' width='600'>
			<tr>	
				<td height='15'></td>
			</tr>	
					
			<tr>
				<td valign='top'>
				
					<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' bgcolor='white'>
					<tr>
						<td colspan='3' height='10'></td>
				</tr>
				<tr>
					<td width='15'></td>
					<td>
							
							
				<!-- -->
			
			
						
	<table border='0' cellspacing='0' cellpadding='0' width='560'>
			
	<tr>
			
	<td valign='top'>
	
	
<h1>Kedves Barátom!</h1>


		<div style='padding:0px 20px 10px 20px;'>
					<div style='font-size:15px;padding:0 0 0 0px;text-align:center;'>Most játszottam az Indulhatunk.hu nyereményjátékán ahol értékes nyereményeket sorsolnak ki.
<br/><br/>
	
	
				<div style='font-size:15px;padding:10px 0 0px 0;text-align:center;'>Gyere és csatlakozz a játékhoz Te is, így növelve a nyerési esélyeket.

					
<br/><br/>

<div style='text-align:left;'>
Ehhez nincs más dolgod, mint kattints a <a href='http://www.lmb.hu/jatek' style='text-decoration:none;color:#ff3c1b;' target='_blank'>NYERNI SZERETNÉK</a> gombra!<br/><br/>


<b>Többszörözd meg MOST nyerési esélyeidet:</b><br/><br/>
1. Küldd tovább ezt a levelet ismerőseidnek e-mailen<br/>
2. Hívd meg barátaidat az Indulhatunk.hu Karácsonyi nyereményjátékára.   <br/><br/> 

</div>

Játékra fel!



</div>
				</div>
			</td>
		</tr>
	</table>

		<div style='text-align:center; font-size:25px; font-weight:bold; color:#ff3c1b;padding:20px 0 20px 0'><a href='http://www.lmb.hu/jatek' style='text-decoration:underline;color:#ff3c1b;' target='_blank'>NYERNI SZERETNÉK &raquo;</a></div>
		
	</div>
	
	<div style='background-color:#007bab;padding:10px'>
		<table cellspacing='0' cellpadding='0' border='0'>
		<tr>
		
		<td>
		<div style='padding:20px; font-size:17px; font-weight:bold;color:white;text-align:center;'>FŐNYEREMÉNY: <br/><br/>
		 <br/>egy 2 éj 2 fő részére szóló wellness hétvége az<br/><br/>ÉV SZÁLLODÁJÁBA, a 
Bambara Hotel****premium<br/> szállodába.</div> </td>
		<td width='268'>
				<a href='http://www.lmb.hu/jatek' target='_blank'><img src='http://www.indulhatunk.hu/images/bambara.jpg'  alt='Nyerjen wellness hétvégét!'  title='Nyerjen wellness hétvégét!'/></a>
		</td>
		</tr>
	</table>
	</div>
	<div style='color:#ff3c1b; font-size:23px;padding:10px'>A résztvevők között továbbá kisorsolásra kerül:</div>
	
		<a href='http://www.lmb.hu/jatek' target='_blank'><img src='http://indulhatunk.hu/images/win.jpg' alt='További nyereményeink'  title='További nyereményeink' width='560'/></a>


	<div style='padding:20px 0 20px 0;text-align:right;'>
				</div>
		
		
		<div style='padding:0 20px 0 20px;font-size:9px'>-a nyeremények január 5-én kerülnek kihirdetésre az alábbi oldalon: <a href='http://facebook.com/indulhatunk.hu' target='_blank'>facebook.com/indulhatunk.hu</a>.<br/>
	</div>
	



			<!-- -->
				</td>
				<td width='15'></td>
				</tr>
				<tr>
					<td colspan='3' height='15'></td>
				</tr>
				</table>
		
				
								<!-- -->	
				</td>
				<td width='15'></td>
			</tr>
			<tr>	
				<td height='15'> </td>
			</tr>	
			
					<tr>
			<td valign='top'>
			<!-- --></td>
			</tr>
			
			
			<tr>
				<td style='font-size:16px;font-weight:bold;' bgcolor='7db300' height='38' align='center' valign='middle'><a href='http://www.indulhatunk.hu/?utm_source=tr_mail&utm_medium=email&utm_campaign=btn_all' target ='_blank' title='Megnézem a külföldi ajánlatokat' style='color:white; text-decoration: none;'>MEGNÉZEM AZ ÖSSZES AJÁNLATOT</a></td>
			</tr>	
		<tr>	
				<td height='15'> </td>
			</tr>		
			<tr>	
				<td>
					
					<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='e8e8e8'>
						<tr>
							<td width='30'></td>
							<td style='font-size:14px;color:#007bab; font-weight:bold;' valign='middle' height='48' width='535'>
						Indulhatunk.hu Nemzetközi Utazási Irodák 10 helyen országszerte
							</td>
							<td valign='middle'><img  class="image_fix" alt="" title="" src='http://img.hotel-world.hu/images/blue-arrow.png' width='15' height='12'/></td>
						</tr>
					</table>
				</td>
			</tr>	
			
			<tr>

            <tr>
			<td valign='top'>
			<!-- -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width='100%' bgcolor='white'>
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					<tr>
						<td colspan='3' align='center' style='color:#585858;font-size:13px;'><a href='http://www.indulhatunk.hu/elerhetosegeink?utm_source=outlet_opening' target='_blank'><b>Irodáink ünnepi nyitvatartásának megtekintése »</b></a></td>
					</tr>	
					<tr>
						<td colspan='3' height='20'></td>
					</tr>


					<tr>
						<td width='20'></td>  
						<td>
						
						<!-- -->
						
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
                            
								<tr>
									<td width='260' valign="top">
	<table cellpadding="0" cellspacing="0" border="0" width='100%'>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>1024 Budapest, Margit körút 39.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>1029 Budapest, Máriaremetei út 117.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>1056 Budapest, Váci utca 9.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>1067 Budapest, Teréz körút 27.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>1114 Budapest, Bartók Béla út 44.</td>
											</tr>
											</table>

									</td>
									<td width='20'>
									
									</td>
									<td width='260'>
										<table cellpadding="0" cellspacing="0" border="0" width='100%'>
											
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>4400 Nyíregyháza, Kossuth tér 8.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>8000 Székesfehérvár, Kossuth utca 10.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>9021 Győr, Kisfaludy utca 16.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
                                            <tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>9700 Szombathely, Kőszegi u. 11-17.</td>
											</tr>
											<tr>
												<td colspan='3' height='10'></td>
											</tr>
											<tr>
												<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iplace.png' width='20' height='25'/></td>
												<td width='10'></td>
												<td style='font-size:13px;color:#585858;line-height:16px;' valign='middle'>6720 Szeged, Kígyó utca 1.</td>
											</tr>
											</table>
									</td>								
								</tr>
							</table>


						<!-- -->
						
													</td>
						<td width='20'></td> 
					</tr>
					<tr>
						<td colspan='3' height='20'></td>
					</tr>		
					<tr>
						<td colspan='3' align='center' style='color:#585858;font-size:13px;'>Vagy keressen minket <b>a hét minden napján 8:00-22:00 óráig hívható</b> Ügyfélszolgálatunkon:</td>
					</tr>	
					<tr>
						<td colspan='3' height='20'></td>
					</tr>
					<tr>
						<td colspan='3'>
							<table cellpadding="0" cellspacing="0" border="0" width='100%'>
							<tr>
								<td width='220'></td>
								<td valign='middle' width='20'><img src='http://img.hotel-world.hu/images/iphone.png' width='20' height='25'/></td>
								<td width='10'></td>
								<td style='font-size:20px;color:#585858;font-weight:bold; line-height:16px;' valign='middle'>+36 70 9307874</td>
							
							</tr>
							</table>		
						</td>
					</tr>			
					<tr>
						<td colspan='3' height='20'></td>
					</tr>

				</table>
			</td>
			</tr>	
				<tr>	
				<td height='20'>
			
				<div style='font-size:10px; text-align:center;margin:10px 0 10px 0;color:#454545; display:block;'>A levelet (#EMAIL#) címre küldjük , amennyiben a továbbiakban nem kíván élni szolgáltatásunkkal, erre a <a href='#URL#' target="_blank" style='color:#454545;'>linkre</a> kattintva mondhatja le.
</div>



					<div style='text-align:center;padding:5px; font-size:11px;'>Köszönjük, hogy minket választott - indulhatunk.hu ©2014</div>
					</td>
				</tr>	



            <tr>	
				<td height='5'></td>
			</tr>	
			<tr>	
				<td align='center'><img  class='image_fix' alt='az indulhatunk.hu cégcsoport tagja' title='az indulhatunk.hu cégcsoport tagja' src='http://img.hotel-world.hu/images/corporate.png' width='240' height='48'/></td>
			</tr>
			<tr>	
				<td height='20'></td>
			</tr>	
						
		</table>
		</td>
	</tr>
</table>  
</body>
</html>