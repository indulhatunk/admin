<?php
// ==============================================================================================================
/**
 * @name		Számlamotor - DIJBEKÉRŐ, SZÁMLA tesztprogram, lekérdez az előző napi MNB devizaárfolyamot!
 *				Számlamotor verzió: V2.4 2010.06.14.
 *				FIGYELEM! Ha az MNB honlapja nem elérhető hibaüzenetet küldhet!
 *
 * Változások /  Changelog:
 *	2010.06.14.	Számlamotor verzió: V2.4 2010.06.14.: bruttó egységár megadásának lehetősége
 *	2010.05.27.	Számlamotor verzió: V2.4 2010.05.27.
 *				Új motordemo/szmotor_demo.wsdl fájl
 *				Teszt árfolyam, ha nincs elérhető MNB árfolyam.
 *				Számla: többsoros megjelenítés
 *				Prefix, csak_elonezet, van_kiallito, kiallito, van_atvevo, atvevo
 *				Több hibaüzenet: kod, adat, mezo,
 *				Megjelenítés formázása
 *	2010.04.20.	Eredeti változat: PDF megjelenítése
 *				xml2array(): XML adatokból tömböt (array) készít
 *				NapiMNBArfolyamLetoltes(): napi MNB árfolyam letöltése
 * 
 * @author		Brainwave Kft.
 * @copyright	Brainwave Kft.
 * @filesource	teszt.php
 * @version		1.2
 */
// ==============================================================================================================
header('Content-type: text/html; charset=utf-8');
// ==============================================================================================================
/**
 * xml2array(): XML adatokból tömböt (array) készít
 *
 * @param string $contents			XML adatok
 * @param string $get_attributes	attribútumokkal is? (1= igen)
 * @return array					tömb
 */
function xml2array($contents, $get_attributes=1) { 
    if(!$contents) return array(); 

    if(!function_exists('xml_parser_create')) { 
        //print "'xml_parser_create()' function not found!"; 
        return array(); 
    } 
    //Get the XML parser of PHP - PHP must have this module for the parser to work
    // $contents => $xml_values 
    $parser = xml_parser_create(); 
    xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 ); 
    xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 ); 
    xml_parse_into_struct( $parser, $contents, $xml_values);
    xml_parser_free( $parser ); 

    if(!$xml_values) return;	//Hmm... 

    //Initializations 
    $xml_array = array(); 
    $parent = array();	// parents volt! 
    $opened_tags = array(); 
    $arr = array(); 

    $current = &$xml_array; 

    //Go through the tags. 
    foreach($xml_values as $data) { 
        unset($attributes,$value);//Remove existing values, or there will be trouble
        // A tömböt szétszedjük elemeire: $tag, $type, $level, $attributes 
        extract($data);//We could use the array by itself, but this cooler.

        $result = ''; 
        if($get_attributes) {//The second argument of the function decides this. 
            $result = array(); 
            if(isset($value)) $result['value'] = $value;  // original $result['value'] = $value; // $result = $value 

            //Set the attributes too. 
            if(isset($attributes)) { 
                foreach($attributes as $attr => $val) {
                	 
                    if($get_attributes == 1) $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr' 
                    /**  :TODO: should we change the key name to '_attr'? Someone may use the tagname 'attr'. Same goes for 'value' too */ 
                } 
            } 
        } elseif(isset($value)) { 
            $result = $value; 
        } 

        //See tag status and do the needed. 
        if($type == "open") {//The starting of the tag '<tag>' 
            $parent[$level-1] = &$current; 

            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag 
                $current[$tag] = $result; 
                $current = &$current[$tag]; 

            } else { //There was another element with the same tag name 
                if(isset($current[$tag][0])) { 
                    array_push($current[$tag], $result); 
                } else { 
                    $current[$tag] = array($current[$tag],$result); 
                } 
                $last = count($current[$tag]) - 1; 
                $current = &$current[$tag][$last]; 
            } 

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />' 
            //See if the key is already taken. 
            if(!isset($current[$tag])) { //New Key 
                $current[$tag] = $result; 

            } else { //If taken, put all things inside a list(array) 
                if((is_array($current[$tag]) and $get_attributes == 0)//If it is already an array... 
                        or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) { 
                    array_push($current[$tag],$result); // ...push the new element into that array. 
                } else { //If it is not an array... 
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value 
                } 
            } 

        } elseif($type == 'close') { //End of tag '</tag>' 
            $current = &$parent[$level-1]; 
        } 
    } 

    return($xml_array); 
}
// --------------------------------------------------------------------------------------------------------------
/**
 * NapiMNBArfolyamLetoltes(): napi MNB árfolyam letöltése
 *
 * @return unknown
 */
function NapiMNBArfolyamLetoltes() {
	$arfolyamok = false;

	$bdy = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	$bdy.= "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">";
	$bdy.= "<soap:Body>";
	$bdy.= "<GetCurrentExchangeRates xmlns=\"http://www.mnb.hu/webservices/\" />";
	$bdy.= "</soap:Body>";
	$bdy.= "</soap:Envelope>\r\n";
	
	$req = "POST /arfolyamok.asmx HTTP/1.1\r\n";
	$req.= "Host: www.mnb.hu\r\n";
	$req.= "Connection: Close\r\n";
	$req.= "Content-Type: text/xml; charset=utf-8\r\n";
	$req.= "Content-Length: ".strlen($bdy)."\r\n";
	$req.= "SOAPAction: \"http://www.mnb.hu/webservices/GetCurrentExchangeRates\"\r\n\r\n";
	
	$fs = fsockopen("www.mnb.hu", 80);
	fwrite($fs, $req.$bdy);
	$s = '';
	while (!feof($fs))
	{
	    $s .= fgets($fs);
	}
	fclose($fs);
	// Levesszük a fejlécet...
	$s = strstr($s, '<?xml');
	
	$data1 = xml2array($s);
	
	// Lekérdezés adatainak kiszedése...
	$s = $data1['soap:Envelope']['soap:Body']['GetCurrentExchangeRatesResponse']['GetCurrentExchangeRatesResult']['value'];
	$data2 = xml2array($s);
	$arfolyamok = array('datum'=> $data2['MNBCurrentExchangeRates']['Day']['attr']['date'], 'arfolyamok'=>$data2['MNBCurrentExchangeRates']['Day']['Rate'] );
	return $arfolyamok; 
}
// --------------------------------------------------------------------------------------------------------------
/**
 * MNBArfolyamLetoltes():
 *
 * @param unknown_type $currency
 * @param unknown_type $datum
 * @return unknown
 */
function MNBArfolyamLetoltes($currency, $datum=false) {
	$arfolyamok = false;
	
	// Ha van megadva dátum, akkor azt kéri le, egyébként meg az aktuálisat!
	if ($datum) {
		$bdy = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		$bdy.= "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">";
		$bdy.= "<soap:Body>";
		$bdy.= "<GetExchangeRates xmlns=\"http://www.mnb.hu/webservices/\">";
			$bdy.= "<startDate>".$datum."</startDate>";
			$bdy.= "<endDate>".$datum."</endDate>";
			$bdy.= "<currencyNames>".$currency."</currencyNames>";
		$bdy.= "</GetExchangeRates>";
		$bdy.= "</soap:Body>";
		$bdy.= "</soap:Envelope>\r\n";
		
		$req = "POST /arfolyamok.asmx HTTP/1.1\r\n";
		$req.= "Host: www.mnb.hu\r\n";
		$req.= "Connection: Close\r\n";
		$req.= "Content-Type: text/xml; charset=utf-8\r\n";
		$req.= "Content-Length: ".strlen($bdy)."\r\n";
		$req.= "SOAPAction: \"http://www.mnb.hu/webservices/GetExchangeRates\"\r\n\r\n";	// GetCurrentExchangeRates
		
		$fs = fsockopen("www.mnb.hu", 80);
		fwrite($fs, $req.$bdy);
		$s = '';
		while (!feof($fs))
		{
		    $s .= fgets($fs);
		}
		fclose($fs);
		// Levesszük a fejlécet...
		$s = strstr($s, '<?xml');
		
		$data1 = xml2array($s);
		
		// Lekérdezés adatainak kiszedése...
		$s = $data1['soap:Envelope']['soap:Body']['GetExchangeRatesResponse']['GetExchangeRatesResult']['value'];
		$data2 = xml2array($s);
		// Ha van erre a napra letölthető, akkor beállítja....
		if (isset($data2['MNBExchangeRates']['Day'])) {
			$arfolyamok = array('datum'=> $data2['MNBExchangeRates']['Day']['attr']['date'], 'arfolyamok'=>$data2['MNBExchangeRates']['Day']['Rate'] );
		// Egyébként üres.
		} else {
			$arfolyamok = false;
		}
	// 
	} else {
		$arfolyamok= NapiMNBArfolyamLetoltes();
	}
	return $arfolyamok; 
}
// --------------------------------------------------------------------------------------------------------------
// Hibamegjelenítés bekapcsolása
error_reporting(E_ALL);
//error_reporting(0); 
//ini_set("display_errors", 0); // kikapcsolás!

echo ('<html>
<head>
<title>SZÁMLAMOTOR TESZT - DÍJBEKÉRŐ / SZÁMLA</title>
</head>
<body>');

echo ('<b>FIGYELEM!</b> Ha az MNB honlapja nem elérhető hibaüzenetet küldhet!<br>');

echo ('Futtatás időpontja: <b>'.date('Y.m.d. H:i:s').'</b><br>');
echo ('<br>');

echo ('IP cím: <b>'.$_SERVER['SERVER_ADDR'].'</b><br>');
echo ('Szerver: <b>'.$_SERVER['SERVER_NAME'].'</b><br>');

define( 'USER',		'mail@isuperg.com');
define( 'PASSWORD', '31-Kq7PR');
define( 'API_KEY', 'DJrxmTK_');
// DEMO
define( 'WSDL_FILE', 'https://szamlazz.konnyen.hu/motordemo/szmotor_demo.wsdl');

// enable error reporting
use_soap_error_handler(true);

ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache

// Előző nap
$kelt= date('Y-m-d');	// 4+1+2+1+2
// - 1 nap
$kelt= date('Y-m-d', mktime( 0, 0, 0, /* hó */ substr($kelt, 5, 2), /* nap */ substr($kelt, 8, 2), /* év */ substr($kelt, 0, 4)) -1*24*60*60);
// - 7 nap
$telj= date('Y-m-d', mktime( 0, 0, 0, /* hó */ substr($kelt, 5, 2), /* nap */ substr($kelt, 8, 2), /* év */ substr($kelt, 0, 4)) -7*24*60*60);
// + 7 nap
$fiz_hat= date('Y-m-d', mktime( 0, 0, 0, /* hó */ substr($kelt, 5, 2), /* nap */ substr($kelt, 8, 2), /* év */ substr($kelt, 0, 4)) +7*24*60*60);;
echo('Kelt      : <b>'.$kelt.'</b><br>');
echo('Teljesítés: <b>'.$telj.'</b><br>');
echo('Fiz.hat.i.: <b>'.$fiz_hat.'</b><br>');
// - Deviza
$deviza= 'RON';
// Napi árfolyam letöltése:
$arfolyamok= MNBArfolyamLetoltes($deviza, $kelt);
if ($arfolyamok['datum']== $kelt
	&& $arfolyamok['arfolyamok']['attr']['curr']==$deviza) {
	$arfolyam= str_replace(',', '.', $arfolyamok['arfolyamok']['value'])/$arfolyamok['arfolyamok']['attr']['unit']; 
} else {
	$arfolyam= 0;
}
// Tesztelésre: (ha nincs arra a napra árfolyam, akkor csinálunk!)
if ($arfolyam==0) {
	$arfolyam= rand(100,10000)/100;
}	// if
echo('<b>'.$deviza.'</b> MNB árfolyama <b>'.$kelt.'</b>-én: <b>'.$arfolyam.'</b><br>');
// - Forint
// $deviza= 'HUF';
// $arfolyam= 0;

print('Felhasználó: <b>'.USER.'</b>, jelszó: <b>'.PASSWORD.'</b><br>');
print('WSDL fájl: <b>'.(WSDL_FILE).'</b><br>');
print('<br>');
// SOAP hívás:
try {

	$client = new SoapClient(WSDL_FILE,
					array('login'=> USER, 'password'=> PASSWORD, 'trace' => 1,
							'features'=> SOAP_SINGLE_ELEMENT_ARRAYS,
							'exceptions' => true));
	
	$client_functions = $client->__getFunctions(); // lekerdezi a rendelkezesre allo szolgaltatasokat
	
	// Elérhető funkciók (függvények) kiiratása:
	echo "Elérhető funkciók (függvények):<br/>";
	var_dump($client_functions);
	
	echo "<br/>";
	
	// Új számla készítése:	-------------------------------------------
	
	class TSzlaTetelek {
		public $tetel;
	}
	
	$tetelek = new TSzlaTetelek();
	
	$tetelek->tetel[] =array(
			'cikkszam' => 'pg2344/12c',
			'megnev' => 'Tuti termék|akár több sorba is lehet!',
			'besorolas_tip' => 'VTSZ',
			'besorolas' => '64.20.12',
			'mennyisegi_egy' => 'db',
			'mennyiseg' => 1,
			'afa_kulcs' => 24,
			'afa_nev' => '24%',
			//'netto_egysegar' => 2000,
			// Melyik egységárat szeretné megadni? Csak egyet adhat meg!
			'brutto_egysegar' => 6000,
			//'kedvezmeny' => 10,
			//'megjegyzes' => 'Termék neve: ez itt egy termék|Típusa: TUTI1|Egyéb:'			
		);
	
	$tetelek->tetel[] =array(
			'cikkszam' => 'pg2343/13f',
			'megnev' => 'Mégtutibb termék|akár több sorba is lehet!',
			'besorolas_tip' => 'VTSZ',
			'besorolas' => '64.45',
			'mennyisegi_egy' => 'db',
			'mennyiseg' => 2,
			'afa_kulcs' => 24,
			'afa_nev' => '24%',
			//'netto_egysegar' => 5000,
			// Melyik egységárat szeretné megadni? Csak egyet adhat meg!
			'brutto_egysegar' => 5000,
			///'kedvezmeny' => 10,
			'megjegyzes' => 'Termék neve: ez itt egy újabb termék|Típusa: TUTI2|Egyéb:'				
		);
		
	// - Mit szeretne?
	$szla_tipus= 'N';	// Normál számla készítése
	//$szla_tipus= 'M';	// (Hiteles) Másolat
	//$szla_tipus= 'S';	// Sztornó
	// - Prefix beállítása: 	díjbekérő prefix 
	$prefix= '';			// 'DIJ-'.'TR-'
	$SzlaKeszitRequest = array(
		'params' => array(
			'api_key' => API_KEY,
			'biz_tip' => 'N'.$szla_tipus.'S',	// Normál / Normál / Számla
			//'biz_tip' => 'NND',	// Elektronikus / Normál / Díjbekérő
			'gen_pdf' => 1,			// 0 - ne készítsen, 1 - készítsen PDF-et
			'prefix' => $prefix.'MV',		// -> fej->vevo->tomb !	DIJ-	V
			'kerekitesi_mod' => 'soronkent',
			//	'kerekitesi_mod' => 'vegosszesen',
			'sajat_azon' => '3984753847539847937984987'
			//	'sajat_azon' => basename(__FILE__)		// Küldjük el a fájl nevét!
		),
		'data' => array(
			'szla_xml' => array(
				'stilus' => array(
					'megjelenes' => 'modern',
					'tipus' => 'normal',		// normal, ablakos, csekkes, egyszeru,
					'nyelv' => 8,				// 1 - magyar, 2 - magyar/angol, 3 - magyar/német
					//'csak_elonezet' => 1	// 1 - csak előnézet, más - elkészítése
										
					'van_kiallito' => '1',
					//'kiallito' => 'Tóth József',
					
					'van_atvevo' => '1'//,
					//'atvevo' => 'Szabó Sándor'		
				),
				'fej' => array(
					'vevo' => array(
						'nev' => 'Gibsz Jakab Bt.',
						'adoszam' => '12345678-1-12',
						'cim' => array( 'irsz' => '1022', 'varos'=> 'Budapest', 'utca'=>'Fehér utca 12.', 'orszag'=>'1'),
						'lev_cim' => array( 'irsz' => '1022', 'varos'=> 'Budapest', 'utca'=>'Fehér utca 12.', 'orszag'=>'1'),
						'email' => 'jakab@gibsz.hu',
						'cejegyzekszam' => 'cégjegyzékszám',
						'euadoszam' => 'HU12345678',
						'megjegy' => 'Szerződött partnerünk|akár két sorban is'
					),
					'tomb' => 'MV2013',			// DIJ-	V		DIJ-
					'kelt' => $kelt,
					'telj' => $telj,
					'fiz_hat' => $fiz_hat,
					'fiz_mod' => '1',		// 1 - Készpénz, 2 - Átutalás, 3 - Bankkártya, 4 - Hitel, 5 - Utánvét, 6 - Paypal,
					'info' => array(
						'fejlec_info' => 'Kérdéseivel forduljon hozzánk! megjegyzes, szamlan megjelenik',
						'fejlec_info2' => 'Telefonszám: 1/234-5678, fejlec 2. sora',
						'lablec_info' => 'megjegyzes, szamlan megjelenik',
						'lablec_info2' => 'lablec 2. sora'
					),	
					'tipus' => $szla_tipus,			// Normál / Sztornó / Másolat
					'fizetve' => $kelt, 	//'2010-05-25',
					'deviza' => "RON",		// 'EUR'
					'arfolyam' => 74,	// 234.3
					//'hivatkozas' => $prefix.'M'.'2010'.'/'.'000044' //,
					// Akár díjbekérőt is megadhat!
					//'hivatkozas' => 'DIJ-'.$prefix.'M'.'2010'.'/'.'000014' //,	.'V'	'DIJ-'.	
					 'van_afa' => 1,
					// Ellenőrzés miatt:
					//'netto_ossz' => '-1342',
					//'brutto_ossz' => '-1342'
				),
				'tetelek' =>  $tetelek
			)
		)
	);

	echo "<br/>";
	
	echo "Számla adatok:<br/>";
	var_dump( $SzlaKeszitRequest );
	
	$result = $client->SzlaKeszit($SzlaKeszitRequest);
	$hiba= false;
	
} catch (SoapFault $fault) {
	echo "SOAP exception: ".$fault->getMessage()."<br/>";
	$hiba= $fault->getMessage();
	
}	// try

echo "<br/>";

// Hibás?
if ($hiba || is_null($result) || is_soap_fault($result)) {
	if (isset($result)) {
		echo "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})<br/>";
	}	// if (isset($result))
	echo "Hiba: ".$hiba."<br/>";
// OK.
} else {
	echo "Visszakapott adatok:<br/>";
	echo "Saját azonosító:      <b>".$result->sajat_azon.'</b><br>';
	echo "Tranzakció azonosító (több sorban!): <b>".chunk_split($result->tr_id,40,'<br>').'</b><br>';
	echo "Státusz:              <b>".$result->status.'</b><br>';
	echo "Üzenetek:             ".'<br>';
	if (is_object($result->messages)) {
		foreach ($result->messages as $messages) {
			
			foreach ($messages as $message) {
				//var_dump($message->uzenet);
				if (isset($message->uzenet)) {
					echo "             (".$message->kod.") <b>".$message->uzenet.'</b>'.
						' (mező: '.$message->mezo.', adat: '.$message->adat.')<br>';
				}
				
			}
		}	// foreach ($result['messages'] as $uzenet)
		
	}	// if (is_array())
	if ($result->status=='Ok' || isset($result->szla_resp)) {
		echo "Számla:<br>";
		echo "- sorszám:                     <b>".$result->szla_resp->sorszam.'</b><br>';
		echo "- nettó (Ft) összesen:              <b>".$result->szla_resp->netto_ossz.' Ft</b><br>';
		echo "- bruttó (Ft) összesen:             <b>".$result->szla_resp->brutto_ossz.' Ft</b><br>';
		if (isset($result->szla_resp->netto_ossz_d)) {
			echo "- nettó (deviza) összesen:              <b>".$result->szla_resp->netto_ossz_d.' '.$deviza.'</b><br>';
			echo "- bruttó (deviza) összesen:             <b>".$result->szla_resp->brutto_ossz_d.' '.$deviza.'</b><br>';
		}
		if (isset($result->szla_resp->pdf)) {
			//echo "- PDF:                         <b>".base64_decode($result->szla_resp->pdf).'</b><br>';
			echo "- PDF:".'<br>';
			echo "<form name='megnez' method='post' action='pdf.php' target='_blank'>
				<input type='hidden' name='pdf' value='".$result->szla_resp->pdf."'>
				<input type='submit' value='PDF megtekintése'>
				</form>";
		} else {
			echo "- PDF:                         <b>NEM TARTALMAZ PDF FÁJLT!</b><br>";
		}
					
	}	// if ($result->status=='Ok')
}	// if (is_soap_fault($result)) 

echo "<table style='width: 1000px; height: 1000px;' border=1><tr height=20>";	//  height: 1550px;
echo "<td style='width: 500px;'>Küldött</td><td style='width: 500px;'>Fogadott</td>";
echo "</tr><tr height=200>";

echo "<td><textarea style='width: 500px; height: 100%;'>".$client->__getLastRequestHeaders()."</textarea></td>";

echo "<td><textarea style='width: 500px; height: 100%;'>".$client->__getLastResponseHeaders()."</textarea></td>";

echo "</tr><tr>";

echo "<td><textarea style='width: 500px; height: 100%;'>".$client->__getLastRequest()."</textarea></td>";

echo "<td><textarea style='width: 500px; height: 100%;'>".$client->__getLastResponse()."</textarea></td>";

echo "</tr>";
echo "</table>";
echo ('</body></html>');
// ==============================================================================================================
?>