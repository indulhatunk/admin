<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");

//check if user is logged in or not
userlogin();

function removespecial($szoveg) {
        $mit = array("Á","É","Í","Ó","Ö","Ö","Ú","Ü","Ü","Ô","O","U","Ü","á","é","í","ó","ö","ö","ú","ü","ü","ő","o","u","ü");
        $mire = array("A","E","I","O","O","O","U","U","U","O","O","U","U","a","e","i","o","o","o","u","u","u","o","o","u","u");
        $szoveg = str_replace($mit, $mire, $szoveg);
        return $szoveg;
}


$db = new OCI8DB;
$dbconn = $db->dbconn();


if(!empty($_POST))
	$mysql->query("UPDATE partners SET disable_group_customers = '$_POST[disable_group_customers]' WHERE pid = $CURUSER[pid]");



if(!empty($_POST) && $_POST[newpass] <> '' &&  $_POST[newpass_again] <> '' && $_POST[currpass] <> '')
{
	validatePost($_POST);
	
	
	if($CURUSER[password] <> $_POST[currpass])
		$msg = ("A jelenlegi jelszó nem megfelelő!");
	elseif($_POST[newpass] <> $_POST[newpass_again])
		$msg = ("A jelszó megerősítése nem megfellő!");
	elseif($_POST[newpass] == $_POST[newpass_again] && $CURUSER[password] == $_POST[currpass])
	{
		$mysql->query("UPDATE partners SET password = '$_POST[newpass]' WHERE pid = '$CURUSER[pid]'");
		$password = md5($_POST[newpass]);
		setcookie("password", $password, time()+360000);
		$msg = ("A jelszó sikeresen megváltoztatva!");
	}

}

head("Beállítások");


echo message($msg);

?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href="settings.php" class='<? if(empty($_GET)) echo "current"?>'>Szerződéses adatok</a></li> <!-- href must be unique and match the id of target div -->
					<? if($CURUSER[coredb_id] > 0) {?>
						<li><a href="?showdetails=1" class="<? if($_GET[showdetails] == 1) echo "current"?>">Szállás információk, gyermekkedvezmények</a></li>
						<li><a href="?showproperties=1"  class="<? if($_GET[showproperties] == 1) echo "current"?>">Szolgáltatások kategóriák</a></li>
					<? } ?>
						<li><a href="?changepass=1" class="<? if($_GET[changepass] == 1) echo "current"?>">Jelszó módosítása
						</a></li>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>

<?
if(!empty($_POST) && $CURUSER[coredb_id] > 0 && $_GET[showdetails] == 1)
{	
	$mysql_tour->query_update("partner",$_POST,"id=$CURUSER[coredb_id]");
	
	/*$mysql_tour-query("UPDATE partner SET 
		CHILD_1 = '$_POST[CHILD_1]',
		CHILD_2 = '$_POST[CHILD_2]',
		CHILD_3 = '$_POST[CHILD_3]',
		CHILD_1_DISCOUNT = '$_POST[CHILD_1_DISCOUNT]',
		CHILD_2_DISCOUNT = '$_POST[CHILD_2_DISCOUNT]',
		CHILD_3_DISCOUNT = '$_POST[CHILD_3_DISCOUNT]',
		
	 DISCLAIMER = '$_POST[DISCLAIMER]', ADVANCE = '$_POST[ADVANCE]', EMAIL = '$_POST[EMAIL]', PHONE = '$_POST[PHONE]', WEBADDRESS = '$_POST[WEBADDRESS]', VIDEO_URL = '$_POST[VIDEO_URL]'  WHERE ID = '$CURUSER[coredb_id]'");
	//oci_bind_by_name($stid, ":disclaimer", $_POST[DISCLAIMER]); , DISCLAIMER = :disclaimer
	//oci_bind_by_name($stid, ":advance", $_POST[advance]);
//	oci_execute($stid, OCI_DEFAULT);
//	oci_commit($dbconn);
	*/
	writelog("$CURUSER[username] updated settings");
	echo message("Sikeresen szerkesztette az adatait!");
}

if(empty($_GET)) {
?>
	<fieldset class='settingsForm'>
		<legend>Általános információk</legend>
		<table>
		<? if($CURUSER[hotel_name] <> '') { ?>
			<tr><td class='lablerow'>Hotel neve:</td><td><?=$CURUSER[hotel_name]?></td></tr>
		<? } ?>
		<? if($CURUSER[contact] <> '') {?>
			<tr><td class='lablerow'>Kapcsolattartó:</td><td><?=$CURUSER[contact]?></td></tr>
		<? } ?>
		<? if($CURUSER[phone] <> '') {?>
			<tr><td class='lablerow'>Telefonszám:</td><td> <?=$CURUSER[phone]?> </td></tr>
		<? } ?>
		<? if($CURUSER[zip] <> '') { ?>
			<tr><td class='lablerow'>Cím:</td><td> <?=$CURUSER[zip]?>  <?=$CURUSER[city]?>  <?=$CURUSER[address]?></td></tr>
		<? } ?>
			<tr><td class='lablerow'>E-mail cím:</td><td><?=$CURUSER[email]?></td></tr>
		<? if($CURUSER[account_no] <> '') {?>
				<tr><td class='lablerow'>Bankszámlaszám:</td><td><?=$CURUSER[account_no]?></td></tr>
		<? } ?>
		<? if($CURUSER[tax_no] <> '') {?>
				<tr><td class='lablerow'>Adószám:</td><td><?=$CURUSER[tax_no]?></td></tr>
		<? } ?>
		
		
			
<?
	
	if($CURUSER[userclass] > 20)
{
	
	/*$mysql_tour_mail = new Database("localhost", DB_USER, DB_PASS, "vexim");
	$mysql_tour_mail->connect();
	
	//$mysql_tour_mail->query("SET names 'latin1'");
	
	if($_POST[on_vacation] == 'on')
		$_POST[on_vacation] = 1;
	else
		$_POST[on_vacation] = 0;
	if($_POST[on_vacation] == 1 || $_POST[vacation] <> '')
	{
		$_POST[vacation] = removespecial($_POST[vacation]);
		
		$mysql_tour_mail->query("UPDATE users SET vacation = '".trim($_POST[vacation])."', on_vacation = $_POST[on_vacation] WHERE username = '$CURUSER[email]'");
		
	}
	elseif($_POST[autoresponder] == 1)
	{
		$mysql_tour_mail->query("UPDATE users SET vacation = '".trim($_POST[vacation])."', on_vacation = 0 WHERE username = '$CURUSER[email]'");
	}
	
	$user = mysql_fetch_assoc($mysql_tour_mail->query("SELECT * FROM users WHERE username = '$CURUSER[email]' LIMIT 1"));
	
	if($user[user_id] > 0)
	{
	
		if($user[on_vacation] == 1)
			$checked = 'checked';
		else
			$checked = '';
		echo "<form method='post'><input type='hidden' name='autoresponder' value='1'/>	<tr><td class='lablerow'>Vakáció:</td><td><input type='checkbox' name='on_vacation' $checked/></td></tr>";
		echo "	<tr><td class='lablerow'>Üzenet:</td><td><textarea name='vacation' style='width:500px;height:200px'>$user[vacation]</textarea></td></tr>
		<tr><td class='lablerow' align='center' colspan='2'><input type='submit' value='mentés'/></td></tr>
		</form>";
	}*/
	
} 
?>


		</table>	
	</fieldset>
	
	<? if($CURUSER[invoice_name] <> '') {?>
	<fieldset class='settingsForm'>
		<legend>Számlázási információk</legend>
		<? if($CURUSER[invoice_name] <> '')
		{
			$invoice_name = $CURUSER[invoice_name];
			$invoice_address = "$CURUSER[invoice_zip] $CURUSER[invoice_city] $CURUSER[invoice_address]";
		}
		else
		{
			$invoice_name = $CURUSER[company_name];
			$invoice_address = "$CURUSER[zip] $CURUSER[city] $CURUSER[address]";
		}
		?>
		<table>
			<tr><td class='lablerow'>Számlázási név:</td><td><?=$invoice_name?></td></tr>
			<tr><td class='lablerow'>Számlázási cím:</td><td> <?=$invoice_address?> </td></tr>
		</table>	
	</fieldset>
	<div style='font-size:11px;text-align:center;'>
		Amennyiben a fenti adatokat módosítani szeretné, kérjük jelezze a <a href='mailto:tamas.forro@indulhatunk.hu'>tamas.forro@indulhatunk.hu</a> e-mail címen.
	</div>
	
v
	<? } ?>
<?
}
if($_GET[showdetails] == 1) {
//partner properties 
if($CURUSER[coredb_id] > 0)
{
$partnerdata = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = '$CURUSER[coredb_id]'"));
?>
	<form method='post' action='settings.php?showdetails=1'>
	<fieldset class='settingsForm'>
		<legend>Szállás tulajdonságok</legend>
		<ul>
			<li><label>Foglalási E-mail cím:</label><input type="text" name="email" value="<?=$partnerdata[email]?>"/></li>
			<li><label>Telefonszám:</label><input type="text" name="phone" value="<?=$partnerdata[phone]?>"/></li>
			<li><label>Weboldal:</label><input type="text" name="webaddress" value="<?=$partnerdata[webaddress]?>"/></li>
			<li><label>Youtube URL:</label><input type="text" name="video_url" value="<?=$partnerdata[video_url]?>"/></li>
			<li><label>Lemondási feltételek</label><textarea name="disclaimer" style='height:150px;'><?=$partnerdata[disclaimer]?></textarea></li>
			<li><label>Előlegigény:</label><textarea name="advance" style='height:150px;' ><?=$partnerdata[advance]?></textarea></li>
			
			
			<li>
			<label>Gyermekkedvezmények:</label><br/><br/>
					<table style='width:280px;margin:10px 0 0 120px;'>
						<tr>
							<td><input type="text" name="child1" value="<?=$partnerdata[child1]?>" class='numeric child1value' style='width:30px;text-align:center;'/> éves kor alatt</td><td><input type="text" name="child1_discount" value="<?=$partnerdata[child1_discount]?>"  class='numeric' style='width:30px;'/>%</td>
						</tr>
						<tr>
							<td><input type='text' style='width:30px;text-align:center;' value='<?=$partnerdata[child1]?>' class='child1valuereal' disabled/> és <input type="text" name="child2" value="<?=$partnerdata[child2]?>" class='numeric child2value' style='width:30px;text-align:center;'/> éves kor között</td><td><input type="text" name="child2_discount" value="<?=$partnerdata[child2_discount]?>"  class='numeric' style='width:30px;text-align:center;'/>%</td>
						</tr>
						<tr>
							<td><input type='text' style='width:30px;text-align:center;' value='<?=$partnerdata[child2]?>' class='child2valuereal' disabled/> és <input type="text" name="child3" value="<?=$partnerdata[child3]?>" class='numeric child3value' style='width:30px;text-align:center;'/> éves kor között</td><td><input type="text" name="child3_discount" value="<?=$partnerdata[child3_discount]?>"  class='numeric child3percent' style='width:30px;text-align:center;'/>%</td>
						</tr>
						<tr class='grey'>
							<td><input type='text' style='width:30px;text-align:center;' value='<?=$partnerdata[child3]?>' class='child3valuereal' disabled/> éves kor felett</td><td><input type='text' style='width:30px;text-align:center;' value='0' class='child3percentreal' disabled/>%</td>
						</tr>
					</table>
			
			
			</li>
			<li><input type="submit" value="Mentés" /></li>
		</ul>	
	</fieldset>
	</form>	
<?
}
}

//partner properties end 
if($CURUSER[coredb_id] > 0 && $_GET[showproperties] == 1)
{

if($_POST[OFFER_ID] > 0)
{
	if($CURUSER[coredb_package] > 0)
	{
		$delprop = oci_parse($dbconn,"DELETE FROM OFFER_PROPERTY WHERE OFFER_ID = '$CURUSER[coredb_package]'");
		oci_execute($delprop);
		oci_commit($dbconn);
	
	}
	if(empty($_POST[offer_property]))
		$_POST[offer_property] = array();

	foreach($_POST[offer_property] as $property)
	{
		$stid = oci_parse($dbconn,"INSERT INTO OFFER_PROPERTY (
 			ID,
 			OFFER_ID, 
 			PROPERTY_ID
 	 		) 
 		VALUES
 		(
 			PRICES_SEQ.nextval, 
 			'$CURUSER[coredb_package]', 
 			'$property'	
 		)");
 		

 		 	
 		writelog("$CURUSER[username] added price for offer #$_POST[OFFER_ID]");
			oci_execute($stid);		
		oci_commit($dbconn);
	}
		
}

?>
	<form method='post' action='settings.php?showproperties=1'>
	<input type='hidden' name='OFFER_ID' value='<?=$CURUSER[coredb_package]?>'/>

<ul class="tabbed">
	<li><a href="#tab1">Általános szolgáltatások</a></li>
    <li><a href="#tab2">Szálláshely <br/> típusa</a></li>
    <li><a href="#tab3">Wellness szolgáltatások</a></li>
    <li><a href="#tab4">Szabadidős szolgáltatások</a></li>
	<li><a href="#tab5">Gyermekbarát szolgáltatások</a></li>
</ul>
<div class='cleaner'></div>
	<fieldset class='settingsForm'>
		<legend>Szolgáltatások</legend>
		
		<form method='post'>
		

<div class='cleaner'></div>
<div class="tab_container">

			<?
				
				$categories = array(69884633, 8, 11, 12, 13);
				$i = 1;
				foreach($categories as $category)
				{
					if($i > 1)
						$hidden = 'hidden';
						
					echo "<div id='tab$i' class='tab_content $hidden'><table>";
					$pname = getOracleSQL("SELECT * FROM FIELD_CATEGORY_CORE_NAME WHERE FIELD_CATEGORY_ID = $category AND DOMAIN_ID = 2");
					$pname = $pname[0];
					echo "<tr class='header'><td colspan='2'>$pname[NAME]</td></tr>";
					
						$pp = getOracleSQL("SELECT * FROM FIELD_PROPERTY_CORE WHERE CATEGORY_ID = $category");
						
						foreach($pp as $properties)
						{	
						//	debug($properties);
							$property = getOracleSQL("SELECT * FROM FIELD_PROPERTY_CORE_NAME WHERE FIELD_PROPERTY_ID = $properties[ID] AND DOMAIN_ID = 2");
							
					//		echo "<tr><td>SELECT * FROM OFFER_PROPERTY WHERE PROPERTY_ID = $properties[ID] AND OFFER_ID = '$CURUSER[coredb_package]</td></tr>";
							
							$ischecked = getOracleSQL("SELECT * FROM OFFER_PROPERTY WHERE PROPERTY_ID = $properties[ID] AND OFFER_ID = '$CURUSER[coredb_package]'");
							$ischecked = $ischecked[0];
							
							if($ischecked[OFFER_ID] <> 0)
								$checked = 'checked';
							else
								$checked = '';
								
							if($property[0][NAME] <> '')
							{
								echo "<tr class='hovertd'>
									<td>".strtolower($property[0][NAME])."</td>
									<td align='center'><input type='checkbox' value='$properties[ID]' name='offer_property[]' $checked/></td>
							
								</tr>";
							}
						}
					echo "</table></div>";
					$i++;
				}
								?>
			</div>			
	</fieldset>
	<center><input type="submit" value="Mentés" /></center>
	</form>	
<?
}

if($_GET[changepass] == 1) {
?>
<form method='post' action='settings.php?changepass=1'>
	<input type='hidden' name='sent' value='1'/>
	<fieldset class='settingsForm'>
		<legend>Jelszó módosítás</legend>
		<ul>
			<li><label>Új jelszó:</label><input type="password" name="newpass" value=""/></li>
			<li><label>Új jelszó ismét:</label><input type="password" name="newpass_again" value=""/></li>
			<li><label>Jelenlegi jelszó:</label><input type="password" name="currpass" value=""/></li>
		<? if($CURUSER[userclass] >= 5) { ?>
			<li><label>Vásárlók csoportosítása:</label>
				<select name='disable_group_customers'>
					<option value='0'>Nem</option>
					<option value='1' <? if($CURUSER[disable_group_customers] == 1) echo "selected"; ?>>Igen</option>
					
				</select>	
			</li>
		 <? } ?>
			<li><input type="submit" value="Mentés" /></li>
		</ul>	
	</fieldset>
	</form>	
	
<? } ?>
</div></div>
<?
foot();



//foot();
?>