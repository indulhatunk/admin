<?
/*
 * customers.php
 *
 * customers_tour page
 *
*/

/* bootstrap file */
$noheader = 'no';
include("inc/init.inc.php");

userlogin();
if($CURUSER[userclass] < 50)
    header("location: index.php");

if(empty($_GET[datefrom])) { $_GET[datefrom] = date('Y-m-d',strtotime(date('Y-01-01'))); }

if(!isset($_GET[company_pid]))
    $partner = '';
else {
    debug(implode(',',$_GET[company_pid]));
    if(is_array($_GET[company_pid])) {
        $partner = 'AND customers.company_invoice IN ("' . implode('","',$_GET[company_pid]) . '") ';
    }
    else {
        $partner = 'AND customers.company_invoice LIKE "' . $_GET[company_pid] . '"';
    }
}

$date = "";
if(!empty($_GET[datefrom]))
    $date  .= "AND (customers.added >= '$_GET[datefrom]') ";
if(!empty($_GET[dateto]))
    $date  .= "AND (customers.added <= '$_GET[dateto]') ";

if(empty($_GET[items]))
    $_GET[items] = 50;

$limit = "LIMIT 0,".$_GET[items];

if($_GET[all] == 1 or $_GET['export'] == '1') { $limit = ""; }

$sql = "SELECT
customers.cid,
customers.pid,
customers.name,
partners.company_name,
customers.added,
customers.company_invoice,
customers.invoice_number,
customers.paid,
customers.szep_paid,
customers.added,
customers.inactive
FROM
customers
LEFT JOIN
partners ON customers.pid = partners.pid
WHERE customers.invoice_number != ''
AND   customers.paid = 1
AND   customers.szep_successful = 1
$date
$partner
ORDER BY customers.added DESC
$limit
";
//debug($sql); die();

$query = $mysql->query($sql);
if($_GET['export'] == '1') {
    $path = realpath('lib/PHPExcel') . '/PHPExcel.php';
    include_once($path);
    $doc = new PHPExcel();
    $doc->setActiveSheetIndex(0);

    $rowCount = 1;

    while($row = mysql_fetch_array($query)){
        $result[] = array(
            $row[6], //szamlaszam
            $row[2], //vasarlo
            $row[3], //partner
            $row[5], //partner cég
            $row[4] //létrehozva
        );
    }

    $header = array(
        'Számlaszám',
        'Vásárló',
        'Partner',
        'Parnter cég',
        'Létrehozva'
    );

    $doc->getActiveSheet()  ->setTitle("checklist")
    ->fromArray($header, null, 'A1')
    ->fromArray($result, null, 'A2');

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="checklist-'.date('Ymdhis').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}




head("Utasok ellenőrzése",0,0,'',1);
?>
<div class='content-box'>
<div class='contentpadding'>

<?=message($msg)?>

<fieldset>
	<form method='get' id="checklist">
		<input type='hidden' name='checkout' value='<?=$_GET[checkout]?>'/>
		<select id="multiple_company_pid" multiple="multiple" name='company_pid[]'>
					<option value='0'>Válasszon</option>
					<?
			//$partnerQuery = $mysql->query("SELECT `company_name`,`coredb_id` FROM partners WHERE userclass = 3 GROUP BY `company_name` ORDER BY company_name ASC");
			//$partnerQuery = $mysql->query("SELECT `company_name`,`coredb_id` FROM partners WHERE userclass = 3 ORDER BY company_name ASC");
        $partnerQuery = $mysql->query("SELECT
        customers.company_invoice
        FROM
        customers
        WHERE customers.invoice_number != ''
        AND   customers.paid = 1
        AND   customers.szep_successful = 1
        GROUP BY customers.company_invoice");


			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if(in_array($partnerArr[company_invoice],$_GET[company_pid]))
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[company_invoice]\" $selected>$partnerArr[company_invoice] </option>\n";
			}
		?>
					</select>


	<input type='text' name='datefrom' class="datepicker"  placeholder="Indulás -tól" value="<?php echo $_GET[datefrom] ?>"/>
	<input type='text' name='dateto' class="datepicker" placeholder="Indulás -ig" value="<?php echo $_GET[dateto] ?>" />
	<select id="items" name='items'>
		<option value='50' <?php if($_GET[items] == '50') print 'seleceted' ?>>50</option>
		<option value='100' <?php if($_GET[items] == '100') print 'seleceted' ?>>100</option>
		<option value='1000' <?php if($_GET[items] == '1000') print 'seleceted' ?>>1000</option>
		<option value='all' <?php if($_GET[items] == 'all') print 'seleceted' ?>>Összes</option>
	</select>
	<input type='submit' value='szűrés'/>
	<input type="hidden" id="exportval" name="export" value="0">
	<input type='button' value='export' onclick="setExport()"/>
	<script>
       function setExport()
       {
          document.getElementById("exportval").value = 1;
          var form = document.getElementById("checklist");
          form.setAttribute("target", "_blank");
          form.submit();
          form.setAttribute("target", "");
          document.getElementById("exportval").value = 0;
       }
    </script>

	</form>
	<a href='?all=1&company_invoice=<?=$_GET[company_invoice]?>'>Minden tételt mutat</a>
	</fieldset>
	<script type="text/javascript">
      $(document).ready(function() {
          $('#multiple_company_pid').multiselect({
        	  includeSelectAllOption: true,
        	  enableFiltering: true,
        	  'selectAll': false
              });
          $( ".datepicker" ).datepicker( {format: 'yyyy-mm-dd', language: 'hu'});
      });
	</script>

<hr/>


<table>
<?





	echo "<tr class='header' style='text-align: left;'>";
		//echo "<td colspan='3'>ID</td>";
    echo "<td>No.</td>";
    echo "<td>Számlaszám</td>";
    echo "<td>Vásárló</td>";
    echo "<td>Partner</td>";
    echo "<td>Partner cég</td>";
    echo "<td>Létreh.</td>";



		// echo "<td>Fizetendő</td>";
		// echo "<td>Visszaigazolt</td>";
		// echo "<td>Befizetett</td>";
		//echo "<td class='orange'>Készpénz</td>";
		// echo "<td>Voucher</td>";
		// echo "<td>Partnernek</td>";

		// echo "<td>Jutalék</td>";
		// echo "<td>File</td>";
		// echo "<td>Ok</td>";

	echo "</tr>";


	$i = 1;



	while($arr = mysql_fetch_assoc($query))
	{
		//ebug($arr);
		if($arr[inactive] == 1)
		{
		    $deltext = '(törölt)';
		    $class = 'grey';
		}
    /*
    elseif($arr[status] < 3)
		{
		    $deltext = '';
		    $class = 'blue';
		}
    */
		else
		{
		    $deltext = '';
		    $class = '';
		}
		if($arr[from_date] == '0000-00-00')	{ $arr[from_date] = ''; }
		echo "<form method='post'><input type='hidden' name='id' value='$arr[id]'><input type='hidden' name='offer_id' value='$arr[offer_id]'><tr class='$class'>";
		echo "<td>$i</td>";
		//echo "<td width='20'><a href='/passengers.php?add=1&editid=$arr[id]' target='_blank'><img src='./images/edit.png' width='20'/></a></td>";
    echo "<td>$arr[invoice_number]</td>";
    echo "<td>$arr[company_name] $deltext<br/><span class='small'>$arr[offer_id]</span></td>";
    echo "<td>$arr[name] $deltext<br/><span class='small'>$arr[offer_id]</span></td>";
    echo "<td>$arr[company_invoice]</td>";




    echo "<td>$arr[added]</td>";


		//echo "<td>$arr[company_name]<br/>$arr[agent]</td>";
		//echo "<td align='right'>".formatPrice($arr[total])."</td>";
		//echo "<td align='right'>".formatPrice($arr[final_total])."</td>";
		//echo "<td align='right'><a href='/passenger_payments.php?id=$arr[id]' target='_blank'><b>".formatPrice($arr[paidtotal])."</b></td>";
		//	echo "<td align='right' class='orange'>".formatPrice($cashtotal[paid])."</td>";
		//echo "<td align='right'>".formatPrice($arr[voucher_value])."</td>";
		//echo "<td align='right'>".formatPrice($arr[transfertotal])."</td>";

		//echo "<td align='right'>".formatPrice($arr["yield"])."</td>";
		if($arr[has_file] == 1)
		{
		    $check = "<a href='/passengers.php?add=1&editid=$arr[id]#upload' target='_blank'><font color='red'>!!!</font></a>";
		    $inp = "<input type='submit' value='OK'/>";
		}
		else
		{
		    $check = '';
		    $inp = '';
		}
		if($arr[checked] == 1)
		{
		    $cls = 'green';
		    $inp = "<img src='/images/check1.png' width='20'/>";
		}
		else {
		    $cls = '';
		}
	    //echo "<td align='center'>$check</td>";
	    //echo "<td align='center' class='$cls'>$inp</td>";
	    echo "</tr></form>";

	    $totalcash = $totalcash + $arr[cashtotalpaid];
	    $i++;
	}



?>
</table>
</div>


</div></div>
<?
foot();

?>
