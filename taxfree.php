<?
include("inc/tour.init.inc.php");

userlogin();

if($CURUSER[userclass] < 50)
	header("Location: index.php");
	
head("Külföldi statisztika statisztika");

function getPartner($pid = 0, $db = '')
{
	global $mysql;
	
	if($db == 'coredb')
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE coredb_id = $pid LIMIT 1"));
	else
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $pid LIMIT 1"));

	return $partner;
}
?>


<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='?all=1' class='current'>Ajánlatkérés / foglalás statisztika</a></li>
					</ul>
					<div class="clear"></div>
</div>
<div class='contentpadding'>

	<table>
<?
	
	
	$z = 0;
	$query = $mysql->query("SELECT * FROM customers_tour where voucher_id > 0 AND status > 2 AND added >= '2015-03-01' AND added <= '2015-06-01' AND voucher_value > 0 AND partner_id <> '267001292' AND partner_id <> '267001287' AND partner_id <> '1227291' AND inactive = 0 ORDER BY id ASC");
	while($arr = mysql_Fetch_assoc($query))
	{
		
		$partner = getPartner($arr[partner_id],'coredb');
		
		$destination = explode(",", $arr[destination]);
		$destination = trim($destination[0]);
		
		if($destination == 'Észak-Ciprus')
			$destination = 'Ciprus';
			
		$p = 0;
		for($i = 1; $i <= 10; $i++)
		{
			if($arr["passenger".$i."_name"] <> '')
				$p++;
		}
		
		
		$list[$arr[partner_id]][$destination][final_total] += $arr[final_total];
		$list[$arr[partner_id]][$destination][people] += $p;
		$list[$arr[partner_id]][$destination][count]++;
		$list[$arr[partner_id]][$destination][voucher_value] += $arr[voucher_value];
		$list[$arr[partner_id]][$destination]['yield'] += $arr['yield'];

		echo  "
			<tr class='hidden'>
				<td>$z</td>
					<td>$arr[added]</td>
				<td>$arr[name]</td>
				<td>$partner[hotel_name]</td>
				<td>$arr[destination]</td>
				<td>$arr[total]</td>
				<td>$p</td>
			</tr>
		";
		
		$z++;
	}
	
	
	foreach($list as $key => $value)
	{
		
		$partner = getPartner($key,'coredb');

		echo "<tr>";
			echo "<td>$partner[hotel_name]</td>";
			echo "<td><table>";
			
				$ptotal = 0;
				$vtotal = 0;
				$ctotal = 0;
				$ttotal = 0;

					echo "<tr class='header'><td>Desztináció</td><td>db</td><td>Fő</td><td>Összérték</td><td>Ebből illeték</td><td>Jutalék</td></tr>";

				foreach($value as $k => $v)
				{
					echo "<tr><td>$k</td><td align='right'>".$v[count]." db</td><td align='right'>".$v[people]." fő</td><td align='right'>".formatPrice($v[final_total])."</td><td align='right'>".formatPrice($v[voucher_value])."</td><td align='right'>".formatPrice($v['yield'])."</td></tr>";
						
					$vtotal += $v[final_total];
					$ptotal += $v[people];
					$ctotal += $v[count];
					$ttotal += $v[voucher_value];
					$ytotal += $v['yield'];
				}
				echo "<tr class='header'><td>Összesen</td><td align='right'>".$ctotal." db</td><td align='right'>".$ptotal." fő</td><td align='right'>".formatPrice($vtotal)."</td><td align='right'>".formatPrice($ttotal)."</td><td align='right'>".formatPrice($ytotal)."</td></tr>";
			echo "</table></td>";
			
		echo "</td>";
	}
	
?>
</table>
<hr/>
<h2>Green Travel nem Taxfree 2015. január 1-től</h2>
<table>
<?
	$list = array();
	
			$query = $mysql->query("SELECT * FROM customers_tour where status > 2 AND partner_id = 1227356 AND added > '2015-01-01' AND voucher_id = 0 AND inactive = 0 ORDER BY id ASC");
		while($arr = mysql_Fetch_assoc($query))
		{
				$destination = explode(",", $arr[destination]);
		$destination = trim($destination[0]);
		
		if($destination == 'Észak-Ciprus')
			$destination = 'Ciprus';
			
		$p = 0;
		for($i = 1; $i <= 10; $i++)
		{
			if($arr["passenger".$i."_name"] <> '')
				$p++;
		}
		
		
		$list[$arr[partner_id]][$destination][final_total] += $arr[final_total];
		$list[$arr[partner_id]][$destination][people] += $p;
		$list[$arr[partner_id]][$destination][count]++;
		$list[$arr[partner_id]][$destination][voucher_value] += $arr[voucher_value];;
		$list[$arr[partner_id]][$destination]['yield'] += $arr['yield'];;

		}
		
		
			foreach($list as $key => $value)
	{
		
		$partner = getPartner($key,'coredb');

		echo "<tr>";
			echo "<td>$partner[hotel_name]</td>";
			echo "<td><table>";
			
				$ptotal = 0;
				$vtotal = 0;
				$ctotal = 0;
				$ttotal = 0;

					echo "<tr class='header'><td>Desztináció</td><td>db</td><td>Fő</td><td>Összérték</td></tr>";

				foreach($value as $k => $v)
				{
					echo "<tr><td>$k</td><td align='right'>".$v[count]." db</td><td align='right'>".$v[people]." fő</td><td align='right'>".formatPrice($v[final_total])."</td><td align='right'>".formatPrice($v['yield'])."</td></tr>";
						
					$vtotal += $v[final_total];
					$ptotal += $v[people];
					$ctotal += $v[count];
					$ttotal += $v[voucher_value];
					$ytotal += $v['yield'];

				}
				echo "<tr class='header'><td>Összesen</td><td align='right'>".$ctotal." db</td><td align='right'>".$ptotal." fő</td><td align='right'>".formatPrice($vtotal)."</td><td align='right'>".formatPrice($ytotal)."</td></tr>";
			echo "</table></td>";
			
		echo "</td>";
	}
	

		
		
		
		
?>
</table>

<hr/>
<h2>Green Travel Nyíregy 2015. január 1-től</h2>
<table>
<?
	$vtotal = 0;
	$ptotal = 0;
	$ctotal = 0;
	$ttotal = 0;
	$ytotal = 0;

	
	$list = array();
	
			$query = $mysql->query("SELECT * FROM customers_tour where status > 2 AND partner_id = 1227356 AND added > '2015-01-01'  AND agent_id = 3694 AND inactive = 0 ORDER BY id ASC");
		while($arr = mysql_Fetch_assoc($query))
		{
				$destination = explode(",", $arr[destination]);
		$destination = trim($destination[0]);
		
		if($destination == 'Észak-Ciprus')
			$destination = 'Ciprus';
			
		$p = 0;
		for($i = 1; $i <= 10; $i++)
		{
			if($arr["passenger".$i."_name"] <> '')
				$p++;
		}
		
		
		$list[$arr[partner_id]][$destination][final_total] += $arr[final_total];
		$list[$arr[partner_id]][$destination][people] += $p;
		$list[$arr[partner_id]][$destination][count]++;
		$list[$arr[partner_id]][$destination][voucher_value] += $arr[voucher_value];;
		$list[$arr[partner_id]][$destination]['yield'] += $arr['yield'];;

		}
		
		
			foreach($list as $key => $value)
	{
		
		$partner = getPartner($key,'coredb');

		echo "<tr>";
			echo "<td>$partner[hotel_name]</td>";
			echo "<td><table>";
			
				$ptotal = 0;
				$vtotal = 0;
				$ctotal = 0;
				$ttotal = 0;

					echo "<tr class='header'><td>Desztináció</td><td>db</td><td>Fő</td><td>Összérték</td><td>Jutalék</td></tr>";

				foreach($value as $k => $v)
				{
					echo "<tr><td>$k</td><td align='right'>".$v[count]." db</td><td align='right'>".$v[people]." fő</td><td align='right'>".formatPrice($v[final_total])."</td><td align='right'>".formatPrice($v['yield'])."</td></tr>";
						
					$vtotal += $v[final_total];
					$ptotal += $v[people];
					$ctotal += $v[count];
					$ttotal += $v[voucher_value];
					$ytotal += $v['yield'];
				}
				echo "<tr class='header'><td>Összesen</td><td align='right'>".$ctotal." db</td><td align='right'>".$ptotal." fő</td><td align='right'>".formatPrice($vtotal)."</td><td align='right'>".formatPrice($ytotal)."</td></tr>";
			echo "</table></td>";
			
		echo "</td>";
	}
	

		
		
		
		
?>
</table>

<hr/>
<h2>Green Travel Ajka 2015. január 1-től</h2>
<table>
<?
	$vtotal = 0;
	$ptotal = 0;
	$ctotal = 0;
	$ttotal = 0;
	$ytotal = 0;

	
	$list = array();
	
			$query = $mysql->query("SELECT * FROM customers_tour where status > 2 AND partner_id = 1227356 AND added > '2015-01-01'  AND agent_id = 3859 AND inactive = 0 ORDER BY id ASC");
		while($arr = mysql_Fetch_assoc($query))
		{
				$destination = explode(",", $arr[destination]);
		$destination = trim($destination[0]);
		
		if($destination == 'Észak-Ciprus')
			$destination = 'Ciprus';
			
		$p = 0;
		for($i = 1; $i <= 10; $i++)
		{
			if($arr["passenger".$i."_name"] <> '')
				$p++;
		}
		
		
		$list[$arr[partner_id]][$destination][final_total] += $arr[final_total];
		$list[$arr[partner_id]][$destination][people] += $p;
		$list[$arr[partner_id]][$destination][count]++;
		$list[$arr[partner_id]][$destination][voucher_value] += $arr[voucher_value];;
		$list[$arr[partner_id]][$destination]['yield'] += $arr['yield'];;

		}
		
		
			foreach($list as $key => $value)
	{
		
		$partner = getPartner($key,'coredb');

		echo "<tr>";
			echo "<td>$partner[hotel_name]</td>";
			echo "<td><table>";
			
				$ptotal = 0;
				$vtotal = 0;
				$ctotal = 0;
				$ttotal = 0;

					echo "<tr class='header'><td>Desztináció</td><td>db</td><td>Fő</td><td>Összérték</td><td>Jutalék</td></tr>";

				foreach($value as $k => $v)
				{
					echo "<tr><td>$k</td><td align='right'>".$v[count]." db</td><td align='right'>".$v[people]." fő</td><td align='right'>".formatPrice($v[final_total])."</td><td align='right'>".formatPrice($v['yield'])."</td></tr>";
						
					$vtotal += $v[final_total];
					$ptotal += $v[people];
					$ctotal += $v[count];
					$ttotal += $v[voucher_value];
					$ytotal += $v['yield'];
				}
				echo "<tr class='header'><td>Összesen</td><td align='right'>".$ctotal." db</td><td align='right'>".$ptotal." fő</td><td align='right'>".formatPrice($vtotal)."</td><td align='right'>".formatPrice($ytotal)."</td></tr>";
			echo "</table></td>";
			
		echo "</td>";
	}
	

		
		
		
		
?>
</table>


</div>
</div>

<?
foot();
?>