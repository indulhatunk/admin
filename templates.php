<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER[userclass] < 50)
	header("location: index.php");
	
$partnerID = (int)$_POST[partnerID];
$tid = (int)$_POST[tid];

if($partnerID > 0 && $tid == '')
{

	$data[partnerID] = $partnerID;
	$data[body] = $_POST[body];
	$data[ro_body] = $_POST[ro_body];
	$mysql->query_insert("templates",$data);
	
	$id = mysql_insert_id();
	
	$msg = "Sikeresen beillesztette!";
	writelog("$CURUSER[username] inserted a template #$id");
}
if($partnerID > 0 && $tid <> '')
{

	$data[partnerID] = $partnerID;
	$data[body] = $_POST[body];
	$data[ro_body] = $_POST[ro_body];
	$mysql->query_update("templates",$data,"tid=$tid");
	$msg = "Sikeresen szerkesztette!";
	
		writelog("$CURUSER[username] edited a template #$tid");

}

head("Templatek kezelése");

?>
<div class='content-box'>
<div class='content-box-header'>
		<ul class="content-box-tabs">
			<li><a href='?add=1' class='<? if($_GET[add] == 1) echo "current"?>'>Új template</a></li>
			
			<li><a href='?add=0' class='<? if($_GET[add] <> 1) echo "current"?>'>Template-ek kezelése</a></li>
		</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
				
<?

echo $msg;


if($_GET[add] == 1) { 
?>

					
<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			force_br_newlines : true,
			force_p_newlines : false,
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>

<form method="post" action="templates.php">
<fieldset class="news">
<legend>Template</legend>
<ul>
<?
$editid = (int)$_REQUEST[editid];
if($editid > 0)
{
	$editQr = mysql_query("SELECT * FROM templates WHERE tid = $editid");
	$editarr = mysql_fetch_assoc($editQr);
	echo "<input type=\"hidden\" name=\"tid\" value=\"$editarr[tid]\"/>";
}
?>
<li><label>Hotel</label><select name="partnerID" style="width:250px;">
	<?
		$partnerQuery = mysql_query("SELECT * FROM partners where userclass < 100 AND status = 0 ORDER BY hotel_name ASC");
		while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
			if($editarr[partnerID] == $partnerArr[pid])
				$selected = "selected";
			else
				$selected = '';
			echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[hotel_name]</option>\n";
		}
	?>
</select></li>
<li>
<textarea name="body" rows="100" cols="20" style="width: 70%" class="tinymce"><?=$editarr[body]?></textarea>
</li>

<li><input type="submit" value="Mentés" /></li>
</ul>	
</fieldset>


</form>
<?

} 
else { 
echo "<hr/>";

echo "<table class=\"general\">";

echo "<tr class=\"header\">";
	echo "<td colspan='2'>ID</td>";
	echo "<td>Hotel</td>";
	echo "<td>Előnezet</td>";
	echo "<td>Szerkeszt</td>";
echo "</tr>";


$qr = "SELECT hotel_name, tid, partnerID FROM templates INNER JOIN partners ON partners.pid = templates.partnerID ORDER BY partners.hotel_name";
$query = mysql_query($qr); 

$i = 1;
while($arr = mysql_fetch_assoc($query)) {

	echo "<tr class=\"\">";
		echo "<td align='center' width='20'>$i.</td>";
		echo "<td align='center' width='20'>$arr[tid]</td>";
		echo "<td>$arr[hotel_name]</td>";
		echo "<td  align='center'><a target=\"_blank\" href=\"preview.php?tid=$arr[partnerID]&preview=1\">[előnezet]</a></td>";
		echo "<td  align='center'><a href=\"?editid=$arr[tid]&add=1\">[szerkeszt]</a></td>";
	echo "</tr>";
	$i++;
	
}
echo "</table></div>";

}

echo "</div></div>";

foot();

?>