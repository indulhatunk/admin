<?php
/*
 * torokexport.php
 * Author: gergely.solymosi@gmail.com
 *
 */

/* bootstrap file */
$noheader = 'no';
include("inc/init.inc.php");

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }
    array_multisort($sort_col, $dir, $arr);
}
    
userlogin();
if($CURUSER[userclass] < 50)
    header("location: index.php");

$header = array(
		  'offer_id' 	    => "Utalvány azonosító",
          'total'           => "Utalvány összeg",
          'cofferd_id' 	    => "Repjegy azonosító",
          'airplane_fee'    => "Repjegy ár",
          'airport_fee'     => "Reptéri illeték",
          'seaview'         => "Tengerre néző szoba felár", 
		  'num' 		    => "Sorszám",
		  'passenger_name'  => "Utas neve",
		  'passenger_birth' => "Utas szül.idő",
          'email' 	        => "Email",
          'phone' 	        => "Telefon",
          'name' 	        => "Név",
          'zip' 	        => "IRSZ",
          'city' 	        => "Város",
          'address' 	    => "Cím",
          'offer_name' 	    => "Szálloda",
          'from_date'       => "Indulási időpont",
          'to_date'         => 'Hazaérkezés dátuma',
          'destination' 	=> "repülőjegy ország",
          'added' 	        => "vigvár dátum szállás",
          'cadded' 	        => "vigvár dátum repjegy",
          'comment'         => 'Megjegyzés',
          'agent'           => 'Ügyintéző'
        );
$search  = array(',','!','*','.','/','%!%',";","(",")", " ", "&", "á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ö", "Ö", "ő", "Ő", "ú", "Ú", "ü", "Ü", "ű", "Ű");
$replace = array("","","","","","","","","-", "-", "a", "A", "e", "E", "i", "I", "o", "O", "o", "O", "o", "O", "u", "U", "u", "U", "u", "u");

$sql = "SELECT offer_id, name, offer_name, from_date, customers_tour.* FROM travel_outlet.customers_tour left join partners on partners.coredb_id = customers_tour.partner_id where customers_tour.status = 3  and from_date > '2017-04-01' and customers_tour.partner_id in(267001293,267001616) order by name, from_date";
$sql = "SELECT     customers_tour.offer_id,    customers_tour.name,customers_tour.email,customers_tour.phone,    customers_tour.offer_name,    customers_tour.from_date,	customers_tour.zip, customers_tour.city, customers_tour.address, customers_tour.passenger1_name,    customers_tour.passenger2_name,    customers_tour.passenger3_name,    customers_tour.passenger4_name,	customers_tour.passenger5_name,    customers_tour.passenger6_name,    customers_tour.passenger7_name,    customers_tour.passenger8_name,	customers_tour.passenger9_name,    customers_tour.passenger10_name,	customers_tour.passenger1_birth,    customers_tour.passenger2_birth,    customers_tour.passenger3_birth,    customers_tour.passenger4_birth,	customers_tour.passenger5_birth,    customers_tour.passenger6_birth,    customers_tour.passenger7_birth,    customers_tour.passenger8_birth,	customers_tour.passenger9_birth,    customers_tour.passenger10_birth,    customers_tour.extra1_name,    customers_tour.extra2_name,    customers_tour.extra3_name,  customers_tour.added as added,   c1.offer_id AS cofferd_id,    c1.destination,	c1.added as cadded FROM customers_tour        LEFT JOIN    partners ON partners.coredb_id = customers_tour.partner_id        LEFT JOIN    customers_tour AS c1 ON c1.name = customers_tour.name        AND c1.from_date = customers_tour.from_date WHERE customers_tour.status = 3 AND customers_tour.from_date > '2017-04-01' AND customers_tour.partner_id = 267001616 AND customers_tour.tour_category = 'voucher'        AND c1.tour_category = 'plane'        AND c1.partner_id = 267001293 ORDER BY customers_tour.name , customers_tour.from_date";
$sql = "SELECT 
    customers_tour.offer_id,
    customers_tour.name,
    customers_tour.email,
    customers_tour.phone,
    customers_tour.offer_name,
    customers_tour.from_date,
    customers_tour.to_date,
    customers_tour.zip,
    customers_tour.city,
    customers_tour.address,
    customers_tour.comment,
    customers_tour.passenger1_name,
    customers_tour.passenger2_name,
    customers_tour.passenger3_name,
    customers_tour.passenger4_name,
    customers_tour.passenger5_name,
    customers_tour.passenger6_name,
    customers_tour.passenger7_name,
    customers_tour.passenger8_name,
    customers_tour.passenger9_name,
    customers_tour.passenger10_name,
    customers_tour.passenger1_birth,
    customers_tour.passenger2_birth,
    customers_tour.passenger3_birth,
    customers_tour.passenger4_birth,
    customers_tour.passenger5_birth,
    customers_tour.passenger6_birth,
    customers_tour.passenger7_birth,
    customers_tour.passenger8_birth,
    customers_tour.passenger9_birth,
    customers_tour.passenger10_birth,
    customers_tour.extra1_value,
    customers_tour.extra2_value,
    customers_tour.extra3_value,
    customers_tour.extra1_name,
    customers_tour.extra2_name,
    customers_tour.extra3_name,
    customers_tour.child_value,
    customers_tour.child_number,
    c1.airport_fee,
    c1.adults,
    customers_tour.total,
    c1.base_price as airplane_fee,
    c1.extra1_value as airextra1,
    c1.extra2_value as airextra2,
    c1.extra3_value as airextra3,    
    c1.child_value as airchild_value,
    customers_tour.added AS added,
    c1.offer_id AS cofferd_id,
    c1.destination,
    c1.added AS cadded,
    agent.company_name as agent
FROM
    customers_tour
        LEFT JOIN
    partners ON partners.coredb_id = customers_tour.partner_id
       LEFT JOIN
    partners as agent ON agent.pid = customers_tour.agent_id
       LEFT JOIN
    customers_tour AS c1 ON c1.name = customers_tour.name
        AND c1.from_date = customers_tour.from_date

WHERE
    customers_tour.status = 3
    AND customers_tour.inactive = 0 
    AND c1.status = 3
        AND customers_tour.from_date > '2017-04-01'
        AND c1.from_date > '2017-04-01'
        AND customers_tour.partner_id = 267001616
        AND customers_tour.tour_category = 'voucher'
        AND c1.tour_category IN ('plane','green_air')
        AND c1.partner_id = 267001293
        #AND customers_tour.added > '2017-01-03'
GROUP BY customers_tour.offer_id
ORDER BY customers_tour.added,customers_tour.name , customers_tour.from_date";

$query = $mysql->query($sql);
$result = array();
$y = 0;
while($row = mysql_fetch_assoc($query)){
    $seaview = 0;
    $persons = 0;
    $order = array();
    for($k=1;$k<11;$k++) {
        if(!empty($row['passenger'.$k.'_name'])) {
            $persons++; 
            $date1 = new DateTime($row['passenger'.$k.'_birth']);
            $date2 = new DateTime('-2 year');
            if ($date_now > $date2)
            if($date2 < $date1) {
                $persons--;
            }
            if(!empty($row['passenger'.$k.'_birth'])) {
                $order[$k] =  new DateTime($row['passenger'.$k.'_birth']);
            }
        }
    }
    
    asort($order);
    
    $persons -= $row['child_number'];
        
    $c = 0;
    foreach($order as $i=>$item) {  
      if(!empty($row['passenger'.$i.'_name'])) {
        $c++;
	    $result[$y] = array(
		  'offer_id' 	    => $row['offer_id'],
          'total'           => '',
          'cofferd_id' 	    => $row['cofferd_id'],
          'airplane_fee'    => $row['airplane_fee'],
          'airport_fee'     => $row['airport_fee'],
          'seaview'         => $seaview,
		  'num' 		    => $c,
		  'passenger_name'  => $row['passenger'.$i.'_name'],
		  'passenger_birth' => $row['passenger'.$i.'_birth'],
          'email' 	        => $row['email'],
          'phone' 	        => $row['phone'],
          'name' 	        => $row['name'],
          'zip' 	        => $row['zip'],
          'city' 	        => $row['city'],
          'address' 	    => $row['address'],
          'offer_name' 	    => $row['offer_name'],
          'from_date'       => $row['from_date'],
	      'to_date'         => $row['to_date'],
          'destination' 	=> $row['destination'],
          'added' 	        => $row['added'],
          'cadded' 	        => $row['cadded'],
	      'comment'         => $row['comment'],
	      'agent'           => $row['agent'],
        );
        if($c == 1) {
            $result[$y]['total'] = $row['total'];
        }
        if($c == $row['adults']+1) {
            if($row['airextra1'] > 30000) {
                $row['airextra1'] -= $row['airport_fee'];
            }
            $result[$y]['airplane_fee'] = $row['airextra1'];
            if(empty($result[$y]['airplane_fee'])) {
                $result[$y]['airplane_fee'] = $row['airchild_value'];
            }
        }
        if($c == $row['adults']+2) {
            if($row['airextra3']-$row['airport_fee'] > 25000) {
                $row['airextra3'] -= $row['airport_fee'];
            }
            $result[$y]['airplane_fee'] = $row['airextra3'];
        }
        if($c == $row['adults']+3) {
            $result[$y]['airplane_fee'] = 0;
        }
        if($c == $row['adults']+4) {
            $result[$y]['airplane_fee'] = 0;
        }
        if($c == $row['adults']+5) {
            $result[$y]['airplane_fee'] = 0;
        }
        if($c == $row['adults']+6) {
            $result[$y]['airplane_fee'] = 0;
        }
        if (preg_match('/Tengerre/',$row['extra1_name']))    {    $seaview = $row['extra1_value'];    }
        else if (preg_match('/Tengerre/',$row['extra2_name']))    {    $seaview = $row['extra2_value'];    }
        else if (preg_match('/Tengerre/',$row['extra3_name']))    {    $seaview = $row['extra3_value'];    }
        
        if($seaview>0) {
            if($persons % 2 == 1) {
                $persons--;
            }
            $result[$y]['seaview'] = $seaview/$persons;
        }

	  }
	  //die();
	  $y++;
    }
    
}
//die();


$path = realpath('lib/PHPExcel') . '/PHPExcel.php';
include_once($path);
$doc = new PHPExcel();
$doc->setActiveSheetIndex(0);

$doc->getActiveSheet()  ->setTitle("list")
->fromArray($header, null, 'A1')
->fromArray($result, null, 'A2');

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="checklist-'.date('Ymdhis').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
$objWriter->save('php://output');
exit;

?>