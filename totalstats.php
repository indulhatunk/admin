<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();


header('Content-type: application/ms-excel');
header('Content-Disposition: attachment; filename=report.xls');


/*?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href='?all=1' class='<? if($_GET[deleted] <> 1) echo "current";?>'>Statisztika</a></li>
		<li><a href='?deleted=1' class='<? if($_GET[deleted] == 1) echo "current"; ?>'>Törölt megoszlás</a></li>
	</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<?*/

function generateTable($groupcategory = '', $criteria = '', $title = '',$type = 'sum', $percentage = '',$comparedata = '',$return = 0,$sumfield = 'added')
{
	global $mysql;
	
$years = array(2010, 2011, 2012,2013,2014,2015);
$months = array("","január","február","március","április","május","június","július","augusztus","szeptember","október","november","december");

$data = array();


	echo "<table style='width:250px;float:left; margin:0 20px 20px 0 ' border='1'>";
			
			if($percentage <> '')
				$ecolcount = 1;
			else
				$ecolcount = 0; 
			$colcount = count($years) + 1 + $ecolcount;
			
			
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:left;'><td colspan='$colcount' style='background-color:#c4c4c4;font-weight:bold;text-align:center;'>$title</td></tr>";
		
			echo "<tr style='background-color:#c4c4c4;font-weight:bold;text-align:center;'>";
			
			for($i=0; $i < count($years); $i++)
			{
				if($i == 0)
					echo "<td width='70'>-</td>";
					
				echo "<td>".$years[$i]."</td>";
				
			}
			
			if($percentage <> '')
				echo "<td>%</td>";
			echo "</tr>";
			
			
			if($groupcategory == 'weekly')
				$maxrows = 52;
			else
				$maxrows = 12;
			for($h = 1; $h <= $maxrows; $h++)
			{
				
				echo "<tr>";
				for($i=0; $i < count($years); $i++)
				{
				
					if($type == 'sum')
						$groupby = ' count(cid) as tcount';
					elseif($type == 'vatera' )
						$groupby = '  sum(actual_price) as tcount';
					else	
						$groupby = '  sum(orig_price) as tcount';
						
					if($groupcategory == 'weekly')
						$deleted = mysql_fetch_assoc($mysql->query("SELECT $groupby FROM customers WHERE cid > 0 AND  year($sumfield) = '".$years[$i]."' AND week($sumfield,3) = '".$h."' $criteria"));
					else
						$deleted = mysql_fetch_assoc($mysql->query("SELECT $groupby FROM customers WHERE cid > 0 AND type = 4 AND  year($sumfield) = '".$years[$i]."' AND month($sumfield) = '".$h."' $criteria"));

					if($i == 0)
					{
						if($groupcategory == 'weekly')
							echo "<td  style='background-color:#c4c4c4;font-weight:bold;text-align:left;' style='background-color:#c4c4c4;font-weight:bold;text-align:left;'>".$h.". hét</td>";
						else
							echo "<td  style='background-color:#c4c4c4;font-weight:bold;text-align:left;' style='background-color:#c4c4c4;font-weight:bold;text-align:left;' >".$months[$h]."</td>";

					}
						if($deleted[tcount] == 0)
							$deleted[tcount] = '';
							
						if($type == 'sum')
							echo "<td align='right'>$deleted[tcount]</td>";
						elseif($deleted[tcount] == 0)
							echo "<td align='right'></td>";
						else
							echo "<td align='right' style='mso-number-format:Currency;'>".formatPrice($deleted[tcount],3,0,'Ft')."</td>";
							
						
						$index = $years[$i].$h;
						
						$data[$index] = $deleted[tcount];
						
						$yearcount = count($years);
						
						if($i == ($yearcount -1) && $percentage <> '')
						{
							$prevdata = $years[$i]-1;
							$previndex = $prevdata.$h;
							
							if($percentage == 'plus')
								$pc = number_format(($data[$index]-$data[$previndex])/$data[$previndex]*100,2,',','');
							elseif($percentage == 'minus' && $comparedata[$index] <> '')
								$pc = number_format(100-$data[$index]/$comparedata[$index]*100,2,',','');
							else
								$pc = 100;
							echo "<td align='right'>$pc&nbsp%</td>";
						}

					}
				echo "</tr>";

			}
		echo "</table>";
		
		if($return == 1)
			return $data;
}


/* beginning of deleted section */

	if($_GET[deleted] == 1)
	{
	
	$criteria = array(" AND inactive = 1" => "Törölt (db)", " AND inactive = 1 AND checked = 1" => "Ezek közül kitöltött (db)", " AND inactive = 1 AND voucher = 1" => "Ezek közül letöltött (db)");
		
	foreach($criteria as $crit => $title)
		{
			
			generateTable($type,$crit,$title);
		}
			/*
			
		echo "<table style='width:250px;float:left; margin:0 20px 0 0 '>";
			
			$colcount = count($years) + 1;
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:left;'><td colspan='$colcount'>$title</td></tr>";
		
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:left;'>";
			
			for($i=0; $i < count($years); $i++)
			{
				if($i == 0)
					echo "<td width='50'>-</td>";
					
				echo "<td>".$years[$i]."</td>";
				
			}
			echo "</tr>";

			for($h = 1; $h <= 12; $h++)
			{
				
				echo "<tr>";
				for($i=0; $i < count($years); $i++)
				{
				
					$deleted = mysql_fetch_assoc($mysql->query("SELECT count(cid) as tcount FROM customers WHERE inactive = 1 AND year(added) = '".$years[$i]."' AND month(added) = '".$h."' $crit"));
				
					if($i == 0)
						echo "<td  style='background-color:#c4c4c4;font-weight:bold;text-align:left;' style='text-align:left;' >".$months[$h]."</td>";
						
						if($deleted[tcount] == 0)
							$deleted[tcount] = '';
						echo "<td align='right'>$deleted[tcount]</td>";
				
					}
				echo "</tr>";

			}
		echo "</table>";
		
		}
		*/
		echo "<div class='cleaner'></div>";
	}
	elseif($_GET[emails] == 1)
	{
		?>
		<ul>
			<li><a href='#kitlet'>Letöltött + kitöltött</a></li>
			<li><a href='#kit'>CSAK kitöltött</a></li>
			<li><a href='#zero'>NEM letöltött, nem kitöltött</a></li>


		</ul>
		<?
		
		echo "<table>";
			echo "<tr   style='background-color:#c4c4c4;font-weight:bold;text-align:center;'><td colspan='8'><a name='kitlet'></a>Visszamondottak partnerek alapján 2012-ben</td></tr>";
		
		$query = $mysql->query("SELECT count(cid) as cnt, pid FROM customers WHERE paid = 1 and inactive = 0 AND year(added) = 2012 GROUP by pid ORDER BY cnt DESC ");
		
		
		echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:center;'>";
				echo "<td width='20'></td>";
				echo "<td>Hotel neve</td>";
				echo "<td>Eladott</td>";
				echo "<td>Törölt</td>";
				echo "<td>%</td>";

			echo "</tr>";

		$i = 1;
		while($arr = mysql_fetch_assoc($query))
		{
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));

			$inactive = mysql_fetch_assoc($mysql->query("SELECT count(cid) as cnt FROM customers WHERE pid = $arr[pid] AND inactive = 1 AND year(added) = 2012"));

			$percent = round(($inactive[cnt]/($arr[cnt]+$inactive[cnt]))*100,2);
			echo "<tr>";
				echo "<td width='20'>$i.</td>";
				echo "<td>$partner[hotel_name]</td>";
				echo "<td align='right'>$arr[cnt] db</td>";
				echo "<td align='right'>$inactive[cnt] db</td>";
				echo "<td align='right'>".str_replace(".",",",$percent)." %</td>";
			echo "</tr>";
			
			$i++;
		}
		
		echo "</table>";
			echo "<br/><br/>";
			
				
		echo "<table>";
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:center;'><td colspan='8'><a name='kitlet'></a>Visszamondottak: letöltött + kitöltött</td></tr>";
		
		$query = $mysql->query("SELECT * FROM customers WHERE inactive = 1 AND voucher = 1 AND checked = 1 GROUP by trim(email) ORDER BY inactive_date DESC ");
		
		
		echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:center;'>";
				echo "<td width='20'></td>";
				echo "<td>Eladva</td>";
				echo "<td>Törölve</td>";
				echo "<td>E-mail</td>";
				echo "<td>ID</td>";
				echo "<td>Név</td>";
				echo "<td>Partner</td>";
			echo "</tr>";



		$i = 1;
		while($arr = mysql_fetch_assoc($query))
		{
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));
			if($arr[sub_pid] > 0)
				$spartner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[sub_pid] LIMIT 1"));

			echo "<tr>";
				echo "<td width='20'>$i.</td>";
				echo "<td>$arr[added]</td>";
				echo "<td>$arr[inactive_date]</td>";
				echo "<td width='250'>$arr[email]</td>";
				echo "<td>$arr[offer_id]</td>";
				echo "<td width='250'>$arr[name]</td>";
				echo "<td>$partner[hotel_name] $spartner[hotel_name]</td>";
			
			$i++;
		}
		
		echo "</table>";
			echo "<br/><br/>";
		echo "<table>";
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:center;'><td colspan='8'><a name='kit'></a>Visszamondottak: csak kitöltött</td></tr>";
			
				$query = $mysql->query("SELECT * FROM customers WHERE inactive = 1 AND voucher = 0 AND checked = 1 GROUP by trim(email) ORDER BY inactive_date DESC ");
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:left;'>";
				echo "<td width='20'></td>";
				echo "<td>Eladva</td>";
				echo "<td>Törölve</td>";
				echo "<td>E-mail</td>";
				echo "<td>ID</td>";
				echo "<td>Név</td>";
				echo "<td>Partner</td>";
			echo "</tr>";
			
		$i = 1;
		while($arr = mysql_fetch_assoc($query))
		{
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));
			
			if($arr[sub_pid] > 0)
				$spartner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[sub_pid] LIMIT 1"));

			echo "<tr>";
				echo "<td width='20'>$i.</td>";
				echo "<td>$arr[added]</td>";
				echo "<td>$arr[inactive_date]</td>";
				echo "<td width='250'>$arr[email]</td>";
				echo "<td>$arr[offer_id]</td>";
				echo "<td width='250'>$arr[name]</td>";
				echo "<td>$partner[hotel_name] $spartner[hotel_name]</td>";
			echo "</tr>";
			
			$i++;
		}
		
			
		echo "</table>";
			echo "<br/><br/>";
		echo "<table>";
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:left;'><td colspan='8'><a name='zero'></a>Visszamondottak: NEM letöltött NEM kitöltött</td></tr>";
		$query = $mysql->query("SELECT * FROM customers WHERE inactive = 1 AND voucher = 0 AND checked = 0 GROUP by trim(email) ORDER BY inactive_date DESC ");
		
			echo "<tr  style='background-color:#c4c4c4;font-weight:bold;text-align:left;'>";
				echo "<td width='20'></td>";
				echo "<td>Eladva</td>";
				echo "<td>Törölve</td>";
				echo "<td>E-mail</td>";
				echo "<td>ID</td>";
				echo "<td>Név</td>";
				echo "<td>Partner</td>";
			echo "</tr>";
			
		$i = 1;
		while($arr = mysql_fetch_assoc($query))
		{
			$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));
			if($arr[sub_pid] > 0)
				$spartner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[sub_pid] LIMIT 1"));

			echo "<tr>";
				echo "<td width='20'>$i.</td>";
				echo "<td>$arr[added]</td>";
				echo "<td>$arr[inactive_date]</td>";
				echo "<td width='250'>$arr[email]</td>";
				echo "<td>$arr[offer_id]</td>";
				echo "<td width='250'>$arr[name]</td>";
				echo "<td>$partner[hotel_name] $spartner[hotel_name]</td>";
			$i++;
		}
		echo "</table>";
	}
	else
	{
	
	$type = $_GET[type];
	
	if($type == '')	
		$type = 'monthly';
	else
		$type = 'weekly';

echo "<style>
	body, html { font-family:arial }
    table { border-collapse: collapse; }
    table table td, th { padding:3px; border: 1px solid #000000; vertical-align: baseline; }
    
    .header { text-align:center; background-color:#e7e7e7; font-weight:bold;}
</style>";
	

echo "<table>";

echo "<tr>";

		 echo "<td>";
		 	generateTable($type,'','Eladott darab','sum');
		 echo "</td>\n";
		 
		
 		  echo "<td>";
 		  	generateTable($type,'','Eladott Ft','total','plus');
 		  echo "</td>\n";
	
		  echo "<td>";
		  	generateTable($type,'AND inactive = 1','Törölt darab','sum');
		  echo "</td>\n";
		 
 		  echo "<td>";
 		  	generateTable($type,'AND inactive = 1','Törölt Ft', 'total','plus');
 		  echo "</td>\n";
 		 
 		   echo "<td>";
 		   		generateTable($type,'AND inactive = 0 AND paid = 1','Fizetett darab','sum','',0,'paid_date');
 		   echo "</td>\n";
		 
 		  echo "<td>";
 		  	generateTable($type,'AND inactive = 0 AND paid = 1','Fizetett Ft','total','plus','',0,'paid_date');
 		  echo "</td>\n";
 		 
 		 
 
echo "</tr><tr>";


 		  echo "<td>";

 		  $comparedata = generateTable($type,'  AND (type = 1 OR type = 2 OR type = 6) ','Eladott VTL ','vatera');


 echo "</td>\n";
		 
 		  echo "<td>";
 		   generateTable($type,'AND inactive = 0 AND paid = 1  AND (type = 1 OR type = 2 OR type = 6) ','Fizetett VTL ','vatera','minus',$comparedata,1,'paid_date');
 echo "</td>\n";
		 
 		  echo "<td>";


 		   generateTable($type,'AND inactive = 0 AND paid = 1  AND (type = 1 OR type = 2 OR type = 6) AND payment = 5','Fizetett ÜCS ','vatera','',0,'paid_date');
 echo "</td>\n";
		 
 		  echo "<td>";
 		   generateTable($type,'AND inactive = 0 AND paid = 1  AND (type = 1 OR type = 2 OR type = 6) AND (payment = 10 OR payment = 11 OR payment = 12) ','Fizetett SZÉP ','vatera','',0,'paid_date');
 echo "</td>\n";
		 


		 echo "</tr></table>";
		 
	
	}
	/* end of deleted section */
?>
</div>
</div>
<? 
?>