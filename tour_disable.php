<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");

userlogin();

if($CURUSER[passengers_admin] <> 1)
	header("location: index.php");
		
head("Utazás letiltások kezelése");

if($_GET[delete] > 0)
{
	$msg = "Sikeresen törölte az árletiltást";
	$mysql_tour->query("DELETE FROM tour_disable WHERE id = '$_GET[delete]'");	
}

if(!empty($_POST[delete]))
{
	foreach($_POST[delete] as $del)
	{
		$del = (int)$del;
		$mysql_tour->query("DELETE FROM tour_disable WHERE id = '$del'");	

	}
	$msg = "Sikeresen törölte a letiltásokat";

}
if($_POST[id] > 0)
{

	if($_POST[offer_id] > 0)
	{	
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $_POST[offer_id] LIMIT 1"));
		
		if($offer[id] > 0)
		{
			$_POST[partner_id] = $offer[partner_id];
			$_POST[region_id] = $offer[region_id];
			$_POST[country_id] = $offer[country_id];
			$_POST[travel] = 0;
		}
	}
		
	$mysql_tour->query_update("tour_disable",$_POST,"id=$_POST[id]");
}
if(!empty($_POST) && $_POST[id] == '')
{

	if($_POST[offer_id] > 0)
	{	
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $_POST[offer_id] LIMIT 1"));
		
		if($offer[id] > 0)
		{
			$_POST[partner_id] = $offer[partner_id];
			$_POST[region_id] = $offer[region_id];
			$_POST[country_id] = $offer[country_id];
			$_POST[travel] = 0;
		}
		else
		{
			die("Érvénytelen ajánlat!");
		}
	}
	if($_POST[partner_id] == '')
		$msg = "A dátum, a partner és az éjszakák száma kötelező";
	else
	{
		if(empty($_POST[region_id]) && empty($_POST[country_id]))
		{
			$msg = "Üres kérés!";
		}
		else
		{
			$mysql_tour->query_insert("tour_disable", $_POST);
		}
		
	}
	
	$msg = 'Sikeres szerkesztés';
}
?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					
					<li><a href='?all=1' class='current'>Utazási árletiltástok</a></li>

					</ul>


	<div class="clear"></div>
</div>
<div class='contentpadding'>


<?


echo message($msg);

if($_GET[edit] > 0)
{
	$editarr = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour_disable WHERE id = '$_GET[edit]' LIMIT 1"));
}
?>
<div class="partnerForm">

	

	<form method="POST">
	<input type='hidden' name='id' value='<?=$editarr[id]?>'/>
	<fieldset class="news">
		<legend>Lezárt időpontok</legend>
	<ul>		
		<fieldset>
		<legend>Hotel stop sales</legend>
		<li><label>Hotel:</label>
			<input type='text' name='offer_id' value='<?=$editarr[offer_id]?>' placeholder='Hotel azonosítója' class='numeric'/></li>
		
		<li>
		</fieldset>
		
		<fieldset>
			<legend>Vagy utazás</legend>
			
				<li><label>Partner:</label>
			<select name='partner_id'>
					<?
					$query = $mysql_tour->query("SELECT * FROM partner WHERE partnertype_id = 1 ORDER BY name ASC");
					
					while($arr = mysql_fetch_assoc($query))
					{
						if($arr[id] == $editarr[partner_id])
							$select = 'selected';
						else
							$select = '';
						echo "<option value='$arr[id]' $select>$arr[name]</option>";
					}
					?>					
				</select>	
		</li>


		<li>
			<label>Ország</label>
			<select name='country_id'>
						<option value='0'>Kérem válasszon</option>
						<?
						$query = $mysql_tour->query("SELECT * FROM country WHERE offercount > 0 ORDER BY name ASC");
						
						while($arr = mysql_fetch_assoc($query))
						{
							if($arr[id] == $editarr[country_id])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[id]' $select>$arr[name]</option>";
						}
						?>					
					</select>
		</li>
		<li><label>Régió</label>
				<select name='region_id'>
						<option value='0'>Kérem válasszon</option>
						<?
						$query = $mysql_tour->query("SELECT * FROM region ORDER BY name ASC");
						
						while($arr = mysql_fetch_assoc($query))
						{
							if($arr[id] == $editarr[region_id])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[id]' $select>$arr[name]</option>";
						}
						?>					
					</select>
		</li>
		<li><label>Típus:</label>
			<select name='travel'>
				<option value="0">Utazás módja?</option>
				<option value="1" <? if($editarr[travel] == 1) echo "selected"?>>Busz</option>
				<option value="2"  <? if($editarr[travel] == 2) echo "selected"?>>Hajó</option>
				<option value="780"  <? if($editarr[travel] == 780) echo "selected"?>>Repülő</option>
				<option value="69884550"  <? if($editarr[travel] == 69884550) echo "selected"?>>Egyéni</option>
			</select>
		</li>
		</fieldset>
		<fieldset>
		<legend>Dátum adatok</legend>
		<li><label>Dátum:</label><input type='text' name='from_date' class='maskeddate' value='<?=$editarr[from_date]?>'/> </li>
		<li><label>Éjek száma:</label><input type='text' name='nights' class='numeric' value='<?=$editarr[nights]?>'/> </li>
		</fieldset>
				<li><input type='submit' value='Mentés'/></li>
	</ul>
	</fieldset>
	</form>

</div>
<?

echo "<hr/>";
$query = $mysql_tour->query("SELECT * FROM  tour_disable ORDER by partner_id, id ASC");

echo "<form method='post'><table style='width:100%'>";
	echo "<tr><td colspan='20' class='header'><a href='?update=1'>Frissítés &raquo;</a></td></tr>";
	echo "<tr class='header'>
		<td colspan='2'>-</td>
		<td>Partner</td>
		<td>Dátum</td>
		<td>Éj</td>
		<td>Ország</td>
		<td>Régió</td>
		<td>Típus</td>
		<td>Ajánlat neve</td>
		</tr>";
		
while($arr = mysql_fetch_assoc($query))
{
	$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $arr[partner_id] LIMIT 1"));
		$offer  = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $arr[offer_id] LIMIT 1"));


	$region = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM region WHERE id = $arr[region_id] LIMIT 1"));

	$country = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM country WHERE id = $arr[country_id] LIMIT 1"));

	$property = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM field_property_core_name WHERE field_property_id  = $arr[travel] LIMIT 1"));

	if($arr[from_date] == '0000-00-00')
		$arr[from_date] = '';

	echo "<tr>
		<td><input type='checkbox' name='delete[]' value='$arr[id]'/></td>
		<td width='45'>
			<a href='?edit=$arr[id]'><img src='/images/edit.png' width='20'/></a><a href='?delete=$arr[id]'><img src='/images/trash.png' width='20'/></a>
		</td>
		<td>$partner[name]</td>
		<td>$arr[from_date]</td>
		<td>$arr[nights]</td>
		<td>$country[name]</td>
		<td>$region[name]</td>
		<td>$property[name]</td>
		<td><a href='$offer[page_url]' target='_blank'><b>$offer[name]</b></a></td>
		</tr>";
}
echo "<tr><td colspan='9'><input type='submit' value='Kijelöltek törlése'/></td></tr></table></form>";

echo "</div></div>";

function update_tour_disable()
{
	global $mysql_tour;
	
	$mysql_tour->query("DELETE FROM `tour_disable` WHERE from_date <> '0000-00-00' and from_date < NOW()");
	
	echo "<b>Desztináció letiltás időpontra:</b><br/>";
	$qr = $mysql_tour->query("SELECT * FROM tour_disable WHERE from_date >= NOW() AND offer_id = 0 ORDER BY id aSC");
	while($arr = mysql_fetch_assoc($qr))
	{
		$etravel = '';
		$eregion = '';
		$ecountry = '';
		
		$i = 1;
		if($arr[travel] > 0)
			$etravel = "AND offer_property.property_id = $arr[travel]";
			
		if($arr[region_id] > 0)
			$eregion = "AND region_id  = $arr[region_id]";
			
		if($arr[country_id] > 0)
			$ecountry = "AND country_id  = $arr[country_id]";

		if($arr[country_id] == 0 && $arr[region_id] == 0 && $arr[travel] == 0)
			die("Hiányos adatok");
				
		$query = $mysql_tour->query("SELECT tour.id, tour.name FROM tour LEFT JOIN offer_property ON offer_property.offer_id = tour.id WHERE tour.id > 0 AND tour.aleph_ok = 1 AND partner_id = $arr[partner_id]  $etravel $eregion  $ecountry GROUP BY tour.id ");
		
		while($a = mysql_fetch_assoc($query))
		{
			echo "\t- $arr[from_date]: $a[name]<br/>";
			$mysql_tour->query("DELETE FROM prices WHERE offer_id = $a[id] AND from_date = '$arr[from_date]' AND nights = $arr[nights] AND accomodation = 0");
			
			$changed[$a[id]] = 1;
		}		
	}
	
	echo "<b>Desztináció letiltás általános:</b><br/>";
	
	$qr = $mysql_tour->query("SELECT * FROM tour_disable WHERE from_date = '0000-00-00' AND offer_id = 0 ORDER BY id aSC");
	while($arr = mysql_fetch_assoc($qr))
	{
		$etravel = '';
		$eregion = '';
		$ecountry = '';
		
		$i = 1;
		if($arr[travel] > 0)
			$etravel = "AND offer_property.property_id = $arr[travel]";
			
		if($arr[region_id] > 0)
			$eregion = "AND region_id  = $arr[region_id]";
			
		if($arr[country_id] > 0)
			$ecountry = "AND country_id  = $arr[country_id]";

			
		if($arr[country_id] == 0 && $arr[region_id] == 0 && $arr[travel] == 0)
			die("Hiányos adatok");
				
		$query = $mysql_tour->query("SELECT tour.id, tour.name FROM tour LEFT JOIN offer_property ON offer_property.offer_id = tour.id WHERE tour.id > 0 AND tour.aleph_ok = 1 AND partner_id = $arr[partner_id] $etravel $eregion  $ecountry GROUP BY tour.id ");
		
		while($a = mysql_fetch_assoc($query))
		{
			
			echo "\t- $a[name]<br/>";

			$mysql_tour->query("DELETE FROM prices WHERE offer_id = $a[id] AND accomodation = 0");
			
			$changed[$a[id]] = 1;
		//	updatourprice($a[id]);
			
			//echo "$i $a[name]<br/>";
			//$i++;		
		}		
	}
	
	echo "<b>Ajánlat letiltás dátum nélkül:</b><br/>";
	$qr = $mysql_tour->query("SELECT * FROM tour_disable WHERE from_date = '0000-00-00' AND offer_id > 0 ORDER BY id aSC");
	while($arr = mysql_fetch_assoc($qr))
	{
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT name FROM tour WHERE id = $arr[offer_id]"));
		echo "\t- $offer[name]<br/>";

		$mysql_tour->query("DELETE FROM prices WHERE offer_id = $arr[offer_id] AND accomodation = 0");
			$changed[$arr[offer_id]] = 1;
	//	updatourprice($arr[offer_id]);
	}	
	
	echo "<b>Ajánlat letiltás dátummal:</b><br/>";
	
		$qr = $mysql_tour->query("SELECT * FROM tour_disable WHERE from_date <> '0000-00-00' AND offer_id > 0 ORDER BY id aSC");
	while($arr = mysql_fetch_assoc($qr))
	{
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT name FROM tour WHERE id = $arr[offer_id]"));
		echo "\t- $arr[from_date] $arr[nights] éj: $offer[name]<br/>";

		if($arr[nights] <> '')
			$nights = "AND nights = $arr[nights]";
		else
			$nights = '';
			
			$mysql_tour->query("DELETE FROM prices WHERE offer_id = $arr[offer_id] AND accomodation = 0 AND from_date = '$arr[from_date]' $nights");
			#$mysql_tour->query("DELETE FROM prices WHERE offer_id = $arr[offer_id] AND accomodation = 0");
			$changed[$arr[offer_id]] = 1;
	//	updatourprice($arr[offer_id]);
	}	
	


	
	foreach($changed as $key => $val)
		updatourprice($key);

}

if($_GET[update] == 1)
	update_tour_disable();


foot();
?>