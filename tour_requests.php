<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

userlogin();

header("Content-type: text/html; charset=utf-8");
head("Indulhatunk.hu portálcsoport ajánlatkérések");


if($_POST[id] > 0 && $_POST[id] <> '')
{
		
	if($_POST[agent_id] == 0)
		$_POST[agent_id] = $CURUSER[pid];
	
	$_POST[last_update] = 'NOW()';
	$mysql_tour->query_update("tour_requests",$_POST,"id=$_POST[id]");
	$msg = 'Sikeresen frissítette az érdeklődést!';
	
	writelog("$CURUSER[username] updated tour request $_POST[id]");
	
}

if((int)$_POST[id] == 0 && $_POST[name] <> '')
{
	unset($_POST[id]);
	
	$_POST[added] = 'NOW()';
	if($_POST[agent_id] == 0)
		$_POST[agent_id] = $CURUSER[pid];
		
	$mysql_tour->query_insert("tour_requests",$_POST);
	$msg = 'Sikeresen létrehozta az érdeklődést!';
	
	writelog("$CURUSER[username] updated tour request $_POST[id]");
	
}


$oc = $mysql_tour->query("SELECT * FROM tour_requests ORDER BY id DESC LIMIT 100");
?>

<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
			<li><a href='?add=1' class='<? if($_GET[add] == 1) echo "current"?>'>Új felvitele</a></li>

			<li><a href='?all=1' class='<? if($_GET[add] <> 1) echo "current"?>'>Külfölföldi érdeklődések</a></li>
	</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<?
echo message($msg);

if($_GET[add] == 1)
{
	if($_GET[edit] > 0)
		$editarr = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour_requests WHERE id = $_GET[edit] LIMIT 1"));
?>
	<form method='POST' action='tour_requests.php'>	
	<input type='hidden' name='id' value='<?=$editarr[id]?>'/>
	<fieldset style='width:855px'>
	<legend>Érdeklődés adatai</legend>
	<table width='100%'>
			<tr>
				<td class='lablerow'>Értékesítő:</td>
				<td>
					<select name='agent_id'>
					<option value='0'>Válasszon</option>
					<?
			$partnerQuery = $mysql->query("SELECT * FROM partners WHERE passengers = 1 ORDER BY company_name ASC");

			while($partnerArr = mysql_fetch_assoc($partnerQuery)) {
				if($editarr[agent_id] == $partnerArr[pid])
					$selected = "selected";
				else
					$selected = '';
				echo "<option value=\"$partnerArr[pid]\" $selected>$partnerArr[company_name] </option>\n";
			}
		?>
					</select>
				</td>
				
				<td class='lablerow'>Honnan érkezett:</td>
				<td>
					<select name='source'>
					<option value='web' <? if($editarr[source] == 'web') echo 'selected'?>>weboldal</option>
					<option value='phone' <? if($editarr[source] == 'phone') echo 'selected'?>>telefon</option>
					<option value='email' <? if($editarr[source] == 'email') echo 'selected'?>>email</option>
					<option value='personal' <? if($editarr[source] == 'personal') echo 'selected'?>>személyes</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Út neve</td>
				<td  colspan='3'>
					<input type='text' name='offer_name' style='width:640px;' value='<?=$editarr[offer_name]?>'/><a href='<?=$editarr[offer_url]?> ' target='_blank'><b>[link]</b></a> <!-- (<?=$editarr[package_id]?>)-->
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Desztináció</td>
				<td  colspan='3'>
					<input type='text' name='whereto' style='width:640px;' value='<?=$editarr[whereto]?>'/>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Név:*</td>
				<td  colspan='3'>
					<input type="text" name="name" value="<?=$editarr[name]?>"  style='width:640px;'/>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Telefonszám*:</td>
				<td  colspan='3'><input type="text" name="phone" value="<?=$editarr[phone]?>" id='customerphone'  style='width:640px;'/></td>
			</tr>
		<tr>
				<td class='lablerow'>E-mail cím*:</td>
				<td  colspan='3'><input type="text" name="email" value="<?=$editarr[email]?>" id='customeremail'  style='width:640px;'/></td>
			</tr>
			<tr>
				<td class='lablerow'>Desztináció:</td>
				<td><input type="text" name="whereto" value="<?=$editarr[whereto]?>"/></td>
				<td class='lablerow'>Időpont:</td>
				<td><input type="text" name="from_date" value="<?=$editarr[from_date]?>"/></td>
			</tr>
			<tr>
				<td class='lablerow'>Megjegyés:</td>
				<td  colspan='3'><textarea name="comment"><?=$editarr[comment]?></textarea></td>
			</tr>

		<tr>
			<td class='lablerow'>Ár-tól</td>
			<td><input type='text' name='price_from'/> Ft</td>
			<td class='lablerow'>Ár-ig</td>
			<td><input type='text' name='price_to'/> Ft</td>
		</tr>
		<tr>
			<td colspan='4' class='lablerow' align='center'><input type='submit' value='Mentés'/></td>
		</tr>

	</table>
	</fieldset>
	</form>
<?
}
else
{

echo "<table>
<tr class=\"rowHead\">
	<td width='10' colspan='2'>-</td>
	<td width='200'>Név</td>
	<td width='200'>Ajánlat</td>
	<td width='10'>Típus</td>
	<td width='90'>Megjegyzés</td>
	<td>Telefonszám</td>
	<td width='90'>Állapot</td>
	<td width='90'>Agent</td>
</tr>";

$i = 1;

while($offerArr = mysql_fetch_assoc($oc)) {

/*	$fdate =  explode(" ",$offerArr[added]);
	$fdate = $fdate[0];
	
	$accomodation = mysql_fetch_assoc($mysql_tour->query("SELECT price FROM prices WHERE offer_id = $offerArr[offer_id] ORDER BY price ASC LIMIT 1"));
	
	$acc = mysql_fetch_assoc($mysql_tour->query("SELECT name,package FROM accomodation WHERE id = $offerArr[offer_id]  LIMIT 1"));

	if($acc[package] == 0)
		$acc = '';
		$date = explode(" ",$offerArr[added]);
	

	if($offerArr[status] == 1)
		$class = 'blue';
	elseif($offerArr[status] == 2)
		$class = 'green';
	elseif($offerArr[status] == 3)
		$class = 'grey';
	elseif($offerArr[status] == 4)
		$class = 'orange';
	else
		$class = '';
	
	if($CURUSER[userclass] < 5)
		echo "<td align='center' width='80'>$date[0]</td>";
	else
		echo "<td align='center'>$i</td>";
	
	echo "<td align='center'>$offerArr[source]</td>";
	
	if($CURUSER[userclass] > 5)
	{
		$partner = "<b>$offerArr[p_name]</b><br/>";
	}
	else
	{
		$partner = '';
	}
	
	if($acc[name] == '')
		$acc[name] = $offerArr[p_name];
	*/
	
	$status[question] = "kérdés";
	$status[callback] = "visszahívás";
	$status[request] = "érdeklődés";

	$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $offerArr[offer_id] LIMIT 1"));
	
	$agent = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $offerArr[agent_id] LIMIT 1"));


	if($agent[company_name] == '')
		$agent[company_name] = 'web';
		
	echo "<tr class='$class'>";
	
	echo "
		<td><a href='?edit=$offerArr[id]&add=1'><img src='/images/edit.png' width='20'/></a></td>
		<td>$offerArr[added]</td>
		<td width='170'><a href='mailto:$offerArr[email]'>$offerArr[name]</a></td>
		<td width='170'><a href='$offer[page_url]' target='_blank'>$offerArr[offer_name]</a></td>
		<td>".$status[$offerArr[type]]."</td>
		<td>$offerArr[comment] $offerArr[whereto]</td>
		<td>$offerArr[phone]</td>
		<td></td>
		<td>$agent[username]</td>
</tr>";
	
	$i++;
}

echo "</table>";
}

echo "</div></div>";
foot();
?>