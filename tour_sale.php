<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

	userlogin();
	
	if($CURUSER[userclass] < 50)
	header("location: index.php");
	
$id = (int)$_GET[id];


if($id > 0 && $_GET[delete] > 0 && $_GET[sure] <> 1)
{
	die("
	
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan törölni szeretné a <b><font color='red'>$_GET[delete]</font></b> sorszámú árat?</h1><br/>  <a href=\"?id=$id&delete=$_GET[delete]&sure=1\" class='button green'>$lang[yes]</a> <a href=\"tourprices.php\" class='button red closefacebox'>$lang[no]</a><div class='cleaner'></div>");
	
	
}
head("Utazási akciók");


	


?>

<script>
$(document).ready(function(){
    $('.back').click(function(){
        parent.history.back();
        return false;
    });
});
</script>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='touroffers.php'>Minden utazás</a></li>
						<li><a href='#' class='current'>Utazási akciók</a></li>

						</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<? if($_GET[id] > 0) { 

 		$tour = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $id LIMIT 1"));
		$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $tour[partner_id] LIMIT 1"));
		$tour = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $id LIMIT 1"));
		
			$property = $mysql_tour->query("SELECT property_id FROM offer_property WHERE offer_id = $id");
			
			$prop = '';
			while($off = mysql_fetch_assoc($property))
			{
				$p = mysql_fetch_assoc($mysql_tour->query("SELECT name FROM field_property_core_name WHERE field_property_id = '$off[property_id]'"));
				$prop.= " ".$p[name];
			}
					
?>

	<table style='width:100%;margin:0 auto;'>
		<tr class='header'>
			<td colspan='2'>Az út adatai</td>
		</tr>
			<tr>
				<td class='' width='150'>Utazásszervező</td>	
				<td><?=$partner[name]?></td>				
			</tr>
			<tr>
				<td class=''>Út neve</td>	
				<td><?=$tour[name]?></td>				
			</tr>
			<tr>
				<td class=''>Útazás módja</td>	
				<td><?=$prop?></td>				
			</tr>
		</table>
<hr/>

<?
if($_POST[id] > 0)
{
	
	unset($_POST[sent]);

	echo message("Sikeresen szerkesztette a tételt!");
	
	$mysql_tour->query_update("tour_sale",$_POST,"id=$_POST[id] AND offer_id = $_POST[offer_id]");
	writelog("$CURUSER[username] edited sale for offer #$_POST[offer_id]");
		
}
elseif($_POST[sent] == 1 && $_POST[offer_id] > 0 && $_POST[id] == 0)
{
	unset($_POST[sent]);
	
	$_POST[added] = 'NOW()';
	$mysql_tour->query_insert("tour_sale",$_POST);
	echo message("Sikeresen létrehozta a tételt!");
	writelog("$CURUSER[username] inserted sale for offer #$_POST[offer_id]");

}

if($id > 0 && $_GET[delete] > 0 && $_GET[sure] == 1)
{
	echo message("Sikeresen törölte az árat!");
	
	$mysql_tour->query("DELETE from tour_sale WHERE id=$_GET[delete] AND offer_id= $id");
	writelog("$CURUSER[username] deleted sale for offer #$id");

}

$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour_sale WHERE id = '$_GET[edit]' AND offer_id = '$id'"));

?>
<fieldset>
	<legend>Akció szerkesztése</legend>

<form method='post' action='/tour_sale.php?id=<?=$id?>'>
	<input type='hidden' name='offer_id' value='<?=$id?>'/>
	
	<? if($_GET[edit] > 0) { ?>
		<input type='hidden' name='id' value='<?=$_GET[edit]?>'/>
	<? } ?>
	<input type='hidden' name='sent' value='1'/>
	
	<table width='100%'>
	
		<tr>
			<td class='lablerow'>Érvényesség kezdete:</td>
			<td><input type='text' name='from_date' value='<?=$offer[from_date]?>' class='dpick'/></td>
			<td class='lablerow'>Érvényesség vége:</td>
			<td><input type='text' name='to_date' value='<?=$offer[to_date]?>' class='dpick'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Akció megnevezése:</td>
			<td colspan='3'><input type='text' name='name' value='<?=$offer[name]?>' style='width:490px;'/> </td>
		</tr>
		<tr>
			<td class='lablerow'>Akció leírása:</td>
			<td colspan='3'><textarea name='description'><?=$offer[description]?></textarea></td>
		</tr>
		<tr>
			<td class='lablerow'>Alapár</td>
			<td><input type='text' name='orig_price' value='<?=$offer[orig_price]?>' class='numeric'/> Ft</td>
			<td class='lablerow'>Kedvezményes ár:</td>
			<td><input type='text' name='price' value='<?=$offer[price]?>' class='numeric'/> Ft</td>
		</tr>		
		<tr>
			<td colspan='4' align='center'>
				<input type='submit' value='Mentés'/>
			</td>
		</tr>

	
		
	</table>

</form>
</fieldset>
<br/><br/>
<table>

	<tr class='header'>
		<td>-</td>
		<td>-tól</td>
		<td>-ig</td>
		<td>Név</td>
		<td>Leírás</td>
		<td>Kat. ár.</td>
		<td>Kedv.</td>
	</tr>
<?
	if($id > 0 )
	{
		$offers = $mysql_tour->query("SELECT * FROM tour_sale WHERE offer_id = $id ORDER BY from_date, price ASC");
		
		while($arr = mysql_fetch_assoc($offers))
		{
		
			
			echo "	<tr>
						<td align='center' width='20'><a href='?id=$id&edit=$arr[id]'><img src='/images/edit.png' width='20'/></a>
						<a href='?id=$id&delete=$arr[id]' rel='facebox'><img src='/images/trash.png' width='20'/></a></td>
						<td>".formatDate($arr[from_date])."</td>
						<td>".formatDate($arr[to_date])."</td>
						<td>$arr[name]</td>
						<td>$arr[description]</td>
						<td align='right'>".formatprice($arr[description])."</td>
						<td align='right'>".$arr[percent]."%</td>

					</tr>";
		}
	}
?>

</table>

<? } else { ?>

<table>
	<tr class='header'>
		<td></td>
		<td>Utazás neve</td>
		<td>Ország</td>
		<td>Város</td>
		<td>Megnevezés</td>
		<td>Kezdete</td>
		<td>Vége</td>
		<td>Eredeti ár</td>
		<td>Új ár</td>
	</tr>
	
	<?
	$offers = $mysql_tour->query("SELECT * FROM tour_sale ORDER BY from_date ASC");
	while($arr = mysql_fetch_assoc($offers))
	{	
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $arr[offer_id]"));
		$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = $offer[city_id]"));
		$country = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM country WHERE id = $offer[country_id]"));

		echo "<tr>";
			echo "<td width='15'><a href='?id=$offer[id]&edit=$arr[id]'><img src='/images/edit.png' width='20'/></a></td>";
			echo "<td><a href='http://www.pihenni.hu/".clean_url($offer[name])."+$offer[id]' target='_blank'><b>$offer[name]</b></a></td>";
			echo "<td>$country[name]</td>";
			echo "<td>$city[name]</td>";
			echo "<td>$arr[name]</td>";
			echo "<td align='center'>$arr[from_date]</td>";
			echo "<td align='center'>$arr[to_date]</td>";
			echo "<td align='right'>".formatPrice($arr[orig_price])."</td>";
			echo "<td align='right'>".formatPrice($arr[price])."</td>";
			
		echo "</tr>";
	}
	?>
</table>
<? } ?>
</div></div>
<?
foot();
?>