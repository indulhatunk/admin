<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
	
head("Utazási ajánlatok");

?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='passengers.php' class="">Utasok</a></li>
						<li><a href='?all=1' class="<? if($_GET[disabled] <> 1 && $_GET[fixprice] <> 1) echo "current"?>">Minden utazás</a></li>
						<li><a href='?disabled=1' class="<? if($_GET[disabled] == 1) echo "current"?>">Letiltások</a></li>
						<li><a href='?fixprice=1' class="<? if($_GET[fixprice] == 1) echo "current"?>">Ármódosítások</a></li>
						<li><a href='/tour_sale.php' class="">Akciók</a></li>
						</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>
<table>
<? 
echo "<tr class='header'>";
			echo "<td>ID</td>";
			echo "<td>Partner</td>";
			echo "<td>Ajánlat</td>";
			echo "<td>Min ár</td>";
			echo "<td>Max ár</td>";
			echo "<td></td>";
		echo "</tr>";
		
$query = $mysql_tour->query("SELECT * FROM tour WHERE aleph_ok = 1 AND min_price > 1500 ORDER BY partner_id, min_price ASC");

	$i = 1;
	while($arr = mysql_fetch_assoc($query))
	{
		
		$checkprice = mysql_fetch_assoc($mysql_tour->query("SELECT avg(price) as price FROM prices WHERE offer_id = $arr[id] AND from_date > NOW() LIMIT 1"));
		
		$hf = $checkprice[price]*0.4;
		
		if($arr[min_price] < $hf)
		{
		
		$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $arr[partner_id] LIMIT 1"));
		echo "<tr>";
			echo "<td>$i</td>";
			echo "<td>$partner[name]</td>";
			echo "<td>$arr[name]</td>";
			echo "<td align='right'>".formatPrice($arr[min_price])."</td>";
			echo "<td align='right'>".formatPrice($checkprice[price])."</td>";
			echo "<td align='right'><a href='$arr[page_url]' target='_blank'>tovább &raquo;</a></td>";
			
		echo "</tr>";
		$i++;
		}
		
	}
?>
</table>
</div></div>
<?
foot();
?>