<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not


	userlogin();
	
	
	if($CURUSER[userclass] < 50)
	header("location: index.php");
	
head("Utazási ajánlatok", 1);
?>


<div class='content-box'>

<div class='contentpadding'>

<table>
	<?php 
		
		echo "<tr class='header'>";
				echo "<td>ID</td>";
				
				echo "<td>Partner</td>";
				echo "<td>Ország</td>";
				echo "<td>Régió</td>";
				echo "<td>Város</td>";
				echo "<td>Utazás</td>";
				echo "<td>Utolsó frissítés</td>";
				echo "<td>Aktív?</td>";
			echo "</tr>";
			
		
		$country = $mysql_tour->query("SELECT * FROM country ORDER BY name ASC");
		while($c = mysql_fetch_assoc($country))
		{
			$countries[$c[id]] = $c;
		}
		
		$region = $mysql_tour->query("SELECT * FROM region ORDER BY name ASC");
		while($r = mysql_fetch_assoc($region))
		{
			$regions[$r[id]] = $r;
		}
		
		$city = $mysql_tour->query("SELECT * FROM city ORDER BY name ASC");
		while($c = mysql_fetch_assoc($city))
		{
			$cities[$c[id]] = $c;
		}
		
		$partner = $mysql_tour->query("SELECT * FROM partner ORDER BY name ASC");
		while($c = mysql_fetch_assoc($partner))
		{
			$partners[$c[id]] = $c;
		}
		
		
		$orderby = 'name';
		
		if($_GET[active] == 1)
			$extra .= " AND aleph_ok = 1 ";
		
		if($_GET[partner_id] <> '')
		{
			$extra .= " AND partner_id = $_GET[partner_id] ";
			$orderby = "partner_id";
		}
			
		if($_GET[country_id] <> '')
		{
			$extra .= " AND country_id = $_GET[country_id] ";
			$orderby = "country_id";
		}
			
		if($_GET[region_id] <> '')
		{
			$extra .= " AND region_id = $_GET[region_id] ";
			$orderby = "region_id";

		}	
		if($_GET[city_id] <> '')
		{
			$extra .= " AND city_id = $_GET[city_id] ";
			$orderby = "city_id";

		}
		
		$query = $mysql_tour->query("SELECT name, country_id, region_id, city_id, aleph_ok, last_update, partner_id, page_url FROM tour  WHERE id > 0 $extra ORDER BY $orderby ASC");
		
		$i = 1;
		while($arr = mysql_fetch_assoc($query))
		{
			
			echo "<tr>";
				echo "<td>$i</td>";
				
				echo "<td><a href='?partner_id=$arr[partner_id]'>".$partners[$arr[partner_id]][name]."</a></td>";
				
				if($arr[country_id] <> $cities[$arr[city_id]][country_id])
					$cnterror = 'red';
				else
					$cnterror = '';
					
				if($arr[region_id] <> $cities[$arr[city_id]][region_id])
					$regionerr = 'red';
				else
					$regionerr = '';
					
				echo "<td class='$cnterror'><a href='?country_id=$arr[country_id]'>".$countries[$arr[country_id]][name]." ($arr[country_id])</a></td>";
				echo "<td class='$regionerr'><a href='?region_id=$arr[region_id]'>".$regions[$arr[region_id]][name]." ($arr[region_id])</a></td>";
				echo "<td><a href='?city_id=$arr[city_id]'>".$cities[$arr[city_id]][name]." ($arr[city_id])</a></td>";
				echo "<td><a href='$arr[page_url]' target='_blank'>$arr[name] &raquo;</a></td>";
				echo "<td>$arr[last_update]</td>";
				echo "<td><a href='?active=$arr[aleph_ok]'>$arr[aleph_ok]</a></td>";
			echo "</tr>";
			
			$i++;
		}
		
	?>
</table>
</div></div>
<?
foot();
?>