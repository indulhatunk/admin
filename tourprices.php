<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

	userlogin();
	
	if($CURUSER[userclass] < 50)
	header("location: index.php");




$id = (int)$_GET[id];



if($id > 0 && $_GET[delete] > 0 && $_GET[sure] <> 1)
{
	die("
	
	<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script>
	
	<h2>Biztosan törölni szeretné a <b><font color='red'>$_GET[delete]</font></b> sorszámú árat?</h1><br/>  <a href=\"?id=$id&delete=$_GET[delete]&sure=1\" class='button green'>$lang[yes]</a> <a href=\"tourprices.php\" class='button red closefacebox'>$lang[no]</a><div class='cleaner'></div>");
	
	
}
head("Utazási ajánlatok");


	


?>

<script>
$(document).ready(function(){
    $('.back').click(function(){
        parent.history.back();
        return false;
    });
});
</script>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='passengers.php' class="">Utasok</a></li>
						<li><a href='touroffers.php'>Minden utazás</a></li>
						<li><a href='#' class='current'>Árak</a></li>

						</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<? 		$tour = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $id LIMIT 1"));
		$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $tour[partner_id] LIMIT 1"));
		$tour = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $id LIMIT 1"));
		
			$property = $mysql_tour->query("SELECT property_id FROM offer_property WHERE offer_id = $id");
			
			$prop = '';
			while($off = mysql_fetch_assoc($property))
			{
				$p = mysql_fetch_assoc($mysql_tour->query("SELECT name FROM field_property_core_name WHERE field_property_id = '$off[property_id]'"));
				$prop.= " ".$p[name];
			}
					
?>

	<table style='width:100%;margin:0 auto;'>
		<tr class='header'>
			<td colspan='2'>Az út adatai</td>
		</tr>
			<tr>
				<td class='' width='150'>Utazásszervező</td>	
				<td><?=$partner[name]?></td>				
			</tr>
			<tr>
				<td class=''>Út neve</td>	
				<td><?=$tour[name]?></td>				
			</tr>
			<tr>
				<td class=''>Útazás módja</td>	
				<td><?=$prop?></td>				
			</tr>
		</table>
<hr/>

<?
if($_POST[id] > 0)
{
	
	unset($_POST[sent]);

	echo message("Sikeresen szerkesztette a tételt!");
	
	$mysql_tour->query_update("prices",$_POST,"id=$_POST[id] AND offer_id = $_POST[offer_id] AND accomodation = 0");
	
	updatourprice($_POST[offer_id]);


	writelog("$CURUSER[username] edited price for offer #$_POST[offer_id]");
	
	
		
}
elseif($_POST[sent] == 1 && $_POST[offer_id] > 0 && $_POST[id] == 0)
{
	unset($_POST[sent]);
	
	$_POST[added] = 'NOW()';
	$mysql_tour->query_insert("prices",$_POST);
	
	updatourprice($_POST[offer_id]);

	echo message("Sikeresen létrehozta a tételt!");
	writelog("$CURUSER[username] inserted price for offer #$_POST[offer_id]");

}


if(!empty($_POST[delprice]))
{
	echo message("Sikeresen törölte az árat!");
	
	foreach($_POST[delprice] as $priceid)
	{
	$mysql_tour->query("DELETE from prices WHERE id= $priceid AND offer_id= $id AND accomodation = 0");
	}

	updatourprice($id);

}



if($id > 0 && $_GET[delete] > 0 && $_GET[sure] == 1)
{
	echo message("Sikeresen törölte az árat!");
	
	$mysql_tour->query("DELETE from prices WHERE id=$_GET[delete] AND offer_id= $id AND accomodation = 0");
	
	updatourprice($id);

	writelog("$CURUSER[username] deleted price for offer #$id");

}
 if($_GET[edit] > 0 || $_GET[add] == 1 || $_GET[copy] > 0)
{

	if($_GET[copy] > 0)
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE id = '$_GET[copy]'"));
	else
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM prices WHERE id = '$_GET[edit]'"));

?>
<fieldset>
	<legend>Ár szerkesztése</legend>

<form method='post' action='/tourprices.php?id=<?=$id?>'>
	<input type='hidden' name='offer_id' value='<?=$id?>'/>
	
	<? if($_GET[edit] > 0) { ?>
		<input type='hidden' name='id' value='<?=$_GET[edit]?>'/>
	<? } ?>
	<input type='hidden' name='sent' value='1'/>
	
	<table width='100%'>
	
		
		<? if($offer[id] > 0) { ?>
			<tr>
				<td class='lablerow'>Azonosító:</td>
				<td><?=$offer[id]?></td>
					<td class='lablerow'>Létrehozva:</td>
				<td><?=$offer[added]?></td>
			</tr>
		<? } ?>
		<tr>
			<td class='lablerow'>Érvényesség kezdete:</td>
			<td><input type='text' name='from_date' value='<?=$offer[from_date]?>' class='dpick'/></td>
			<td class='lablerow'>Érvényesség vége:</td>
			<td><input type='text' name='to_date' value='<?=$offer[to_date]?>' class='dpick'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Napok száma:</td>
			<td><input type='text' name='day_num' value='<?=$offer[day_num]?>' class='numeric'/> nap</td>
			<td class='lablerow'>Éjek száma:</td>
			<td><input type='text' name='nights' value='<?=$offer[nights]?>' class='numeric'/> éj </td>
		</tr>
		<tr>
			<td class='lablerow'></td>
			<td><!--<input type='text' name='bed_number' value='<?=$offer[bed_number]?>' class='numeric'/> db--></td>
			<td class='lablerow'>Ellátás:</td>
			<td><input type='text' name='board' value='<?=$offer[board]?>'/></td>
		</tr>
		<tr>
			<td class='lablerow'>Felnőttek száma:</td>
			<td><input type='text' name='bed_number' value='<?=$offer[bed_number]?>' class='numeric'/> fő</td>
			<td class='lablerow'>Gyermekek száma:</td>
			<td><input type='text' name='child_number' value='<?=$offer[child_number]?>' class='numeric'/> fő</td>
		</tr>		
		<tr>
			<td class='lablerow'>Alapár:</td>
			<td><input type='text' name='price' value='<?=$offer[price]?>' class='numeric'/> Ft</td>
			<td class='lablerow'>Katalógusár:</td>
			<td><input type='text' name='catalogue_price' value='<?=$offer[catalogue_price]?>' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td class='lablerow'>Ár típus:</td>
			<td><select name='akcios'>
				<option value='0'>katalógusár</option>
				<option value='1' <? if($offer[akcios] == 1) echo "selected"?>>akciós</option>
				<option value='2' <? if($offer[akcios] == 2) echo "selected"?>>last minute</option>
				<option value='3' <? if($offer[akcios] == 3) echo "selected"?>>first minute</option>
			</select></td>
			<td class='lablerow'>Reptéri illeték:</td>
			<td><input type='text' name='airport_fee' value='<?=$offer[airport_fee]?>' class='numeric'/> Ft</td>

		</tr>
		
		<tr>
			<td class='lablerow'>Kerozin:</td>
			<td><input type='text' name='kerosene_fee' value='<?=$offer[kerosene_fee]?>' class='numeric'/> Ft</td>
			<td class='lablerow'>Szervizdíj:</td>
			<td><input type='text' name='service_fee' value='<?=$offer[service_fee]?>' class='numeric'/> Ft</td>

		</tr>
	<tr>
			<td class='lablerow'>Transzfer:</td>
			<td><input type='text' name='transfer_fee' value='<?=$offer[transfer_fee]?>' class='numeric'/> Ft</td>
			<td class='lablerow'>Vízum:</td>
			<td><input type='text' name='visa_fee' value='<?=$offer[visa_fee]?>' class='numeric'/> Ft</td>

		</tr>
		
		<tr>
			<td class='lablerow'>Foglalási díj:</td>
			<td><input type='text' name='reservation_price' value='<?=$offer[reservation_price]?>' class='numeric'/> Ft</td>
			<td class='lablerow'>Sztornó:</td>
			<td><input type='text' name='storno_insurance' value='<?=$offer[storno_insurance]?>' style='width:80px'/>%</td>

		</tr>
		<tr>
			<td class='lablerow'>Felnőtt pótágy:</td>
			<td><input type='text' name='extra_bed_price' value='<?=$offer[extra_bed_price]?>' class='numeric'/> Ft</td>

			<td class='lablerow'>Üdülőhelyi díj:</td>
			<td><input type='text' name='local_fee' value='<?=$offer[local_fee]?>' class='numeric'/> Ft</td>
		</tr>
			<tr>
			<td class='lablerow'>1. gyermek kora:</td>
			<td><input type='text' name='child1_age' value='<?=$offer[child1_age]?>' class=''/></td>
			<td class='lablerow'>1. gyermek ára:</td>
			<td><input type='text' name='child1_price' value='<?=$offer[child1_price]?>' class='numeric'/> Ft</td>
		</tr>
			<tr>
			<td class='lablerow'>2. gyermek kora:</td>
			<td><input type='text' name='child2_age' value='<?=$offer[child2_age]?>' class=''/></td>
			<td class='lablerow'>2. gyermek ára:</td>
			<td><input type='text' name='child2_price' value='<?=$offer[child2_price]?>' class='numeric'/> Ft</td>
		</tr>
		<tr>
			<td class='lablerow'>3. gyermek kora:</td>
			<td><input type='text' name='child3_age' value='<?=$offer[child3_age]?>' class=''/></td>
			<td class='lablerow'>3. gyermek ára:</td>
			<td><input type='text' name='child3_price' value='<?=$offer[child3_price]?>' class='numeric'/> Ft</td>
		</tr>

			<tr>
			<td class='lablerow'><input type='text' name='optional1_name'  value='<?=$offer[optional1_name]?>' placeholder='Opcionális 1'/></td>
			<td><input type='text' name='optional1_value' value='<?=$offer[optional1_value]?>' class='numeric'/> Ft <?=$offer[optional1_type]?></td>
			<td class='lablerow'><input type='text' name='optional2_name'  value='<?=$offer[optional2_name]?>' placeholder='Opcionális 2'/></td>
			<td><input type='text' name='optional2_value' value='<?=$offer[optional2_value]?>' class='numeric'/> Ft <?=$offer[optional2_type]?></td>

		</tr>
		
				<tr>
			<td class='lablerow'><input type='text' name='optional3_name'  value='<?=$offer[optional3_name]?>' placeholder='Opcionális 3'/></td>
			<td><input type='text' name='optional3_value' value='<?=$offer[optional3_value]?>' class='numeric'/> Ft <?=$offer[optional3_type]?></td>
			<td class='lablerow'><input type='text' name='optional4_name'  value='<?=$offer[optional4_name]?>' placeholder='Opcionális 4'/></td>
			<td><input type='text' name='optional4_value' value='<?=$offer[optional4_value]?>' class='numeric'/> Ft <?=$offer[optional4_type]?></td>

		</tr>
		
				<tr>
			<td class='lablerow'><input type='text' name='optional5_name'  value='<?=$offer[optional5_name]?>' placeholder='Opcionális 5'/></td>
			<td><input type='text' name='optional5_value' value='<?=$offer[optional5_value]?>' class='numeric'/> Ft <?=$offer[optional5_type]?></td>
			<td class='lablerow'><input type='text' name='optional6_name'  value='<?=$offer[optional6_name]?>' placeholder='Opcionális 6'/></td>
			<td><input type='text' name='optional6_value' value='<?=$offer[optional6_value]?>' class='numeric'/> Ft <?=$offer[optional6_type]?></td>

		</tr>

		
		
		<tr>
			<td class='lablerow'>Szobatípus:</td>
			<td  colspan='3'><input type='text' name='room_type' value='<?=$offer[room_type]?>' style='width:500px'/></td>
		</tr>
		
		<tr>
			<td class='lablerow'>Indulás:</td>
			<td  colspan='3'>	
				
				<input type='text' name='departure' value='<?=$offer[departure]?>'/>
				
				<!--<select name='departure'>
						<option value='0'>Kérem válasszon</option>
						<?
						$query = $mysql_tour->query("SELECT * FROM city WHERE aleph_id > 0 ORDER BY name ASC");
						
						while($arr = mysql_fetch_assoc($query))
						{

							if($arr[aleph_id] == $offer[departure])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[aleph_id]' $select>$arr[name]</option>";
						}
						?>					
					</select>-->
					
					</td>
		</tr>
		<tr>
			<td class='lablerow'>Leírás:</td>
			<td  colspan='3'><input type='text' name='description' value='<?=$offer[description]?>' style='width:500px'/></td>
		</tr>
		<tr>
			<td colspan='4' align='center'>
				<input type='submit' value='Mentés'/>
			</td>
		</tr>

	
		
	</table>

</form>
</fieldset>
<?
}
else { 
?>
<table>
	
	<tr>
		<td colspan='15' align='center'><a href='?id=<?=$id?>&add=1'><b>Új ár hozzáadása</b></a></td>
	</tr>
	<tr class='header'>
		<td>-</td>
		<td>Kezdő időpont</td>
		<td>Időtartam</td>
		<td>Ágy</td>
		<td>Ár</td>
		<td>Kerozin</td>
		<td>Reptér</td>
		<td>Transzfer</td>
		<td>Szerviz</td>
		<td>Foglalás</td>
		<td>Vízum</td>

		<td>Storno</td>
		<td>Leírás</td>
		<td>Összesen</td>
		<td>Foglalás</td>
	</tr>
	
	<form method='post'>
<?
	if($id > 0 )
	{
		$offers = $mysql_tour->query("SELECT * FROM prices WHERE offer_id = $id ORDER BY from_date, price ASC");
		
	
		?>
	
		<?
		while($arr = mysql_fetch_assoc($offers))
		{
		
			$total = $arr[price]+$arr[kerosene_fee]+$arr[airport_fee]+$arr[transfer_fee]+$arr[service_fee]+$arr[reservation_fee];
			
			$storno = $total*$arr[storno_insurance]/100;
			
			$total = $total+$storno+$arr[visa_fee];
			
			
			$hash = md5($arr[description].$arr[from_date].$arr[to_date].$arr[offer_id].$arr[price]);
		
			$offerdates[$arr[from_date]."_".$arr[nights]] = 1;

			echo "	<tr>
						<td align='center' width='20'><a href='?id=$id&edit=$arr[id]'><img src='/images/edit.png' width='20'/></a>
						<a href='?id=$id&copy=$arr[id]'><img src='/images/copy.png' width='20'/></a>
						<a href='?id=$id&delete=$arr[id]' rel='facebox'><img src='/images/trash.png' width='20'/></a></td>
						<td>".formatDate($arr[from_date])."<br/>$arr[departure]</td>
						<td align='right'>$arr[day_num] nap<br/>$arr[nights] éj</td>
						<td align='right'>$arr[bed_number]</td>
						<td align='right'>".formatPrice($arr[price])."</td>
						<td align='right'>".formatPrice($arr[kerosene_fee])."</td>
						<td align='right'>".formatPrice($arr[airport_fee])."</td>
						<td align='right'>".formatPrice($arr[transfer_fee])."</td>
						<td align='right'>".formatPrice($arr[service_fee])."</td>
						<td align='right'>".formatPrice($arr[reservation_fee])."</td>
						<td align='right'>".formatPrice($arr[visa_fee])."</td>
						<td align='right'>".$arr[storno_insurance]."%</td>
						<td align='right'>$arr[description]</td>
						
						<td align='right'><b>".formatPrice($total*2)."</b></td>
						<!--<td align='right'><form method='get' action='/passengers.php?add=1&price=$arr[id]&offer_id=$id'><input type='submit' value='foglalás &raquo;'/></form>
						
					
						<a href='http://www.pihenni.hu/".clean_url(strtolower($tour[name]))."+$tour[id]#$hash' target='_blank'>Árhoz</a>-->
						
						<td align='center' width='20'><input type='checkbox' name='delprice[]' value='$arr[id]'/></td>
						</td>
					</tr>";
		}
	}


?>
<tr>
	<td colspan='14'></td>
	<td>
	<script>
	$(function () {
    $('.checkall').on('click', function () {
        $(this).closest('table').find(':checkbox').prop('checked', this.checked);
    });
});
	</script>
	<input type='checkbox' class='checkall'/>
	<input type='submit' value='Törlés'/></td>
</tr>
	</form>
</table>
<hr/>
<form method='POST'>
<input type='hidden' name='offer_id' value='<?=$tour[id]?>'>
<?

	if(!empty($_POST[disable_dates]))
	{
		
		$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = $_POST[offer_id] LIMIT 1"));
		

		foreach($_POST[disable_dates] as $o)
		{
			$data = array();

			$k = explode("_",$o);
			$data[partner_id] = $offer[partner_id];
			$data[offer_id] = $offer[id];
			$data[region_id] = $offer[region_id];
			$data[country_id] = $offer[country_id];
			$data[nights] = $k[1];
			$data[from_date] = $k[0];
			$data[travel] = 0;
			$mysql_tour->query_insert("tour_disable", $data);

		}

		echo message("Sikeresen törölted a dátumokat! <a href='http://admin.indulhatunk.hu/tour_disable.php?update=1' target='_blank'>Frissíts &raquo;</a>");
	}	

?>
<table style='width:250px;margin:0 auto;'>
<tr>
	<td colspan='2' class='header'/>Több időpont törlése</td>
</tr>
<? 
	foreach($offerdates as $key => $v)
	{
		$k = explode("_",$key);

		echo "<tr><td width='10'><input type='checkbox' name='disable_dates[]' value='$key'/></td><td>$k[0] / $k[1] éj</td></tr>";
	}
?>

<script>
	$(function () {
    $('.checkall2').on('click', function () {
        $(this).closest('table').find(':checkbox').prop('checked', this.checked);
    });
});
	</script>




<tr>
	<td colspan='2' align='center'>
			<input type='checkbox' class='checkall2'/>
		<input type='submit' value='A kiválasztott időpontok törlése'/></td>
</tr>
</table>
</form>
<? } ?>
</div></div>
<?
foot();
?>