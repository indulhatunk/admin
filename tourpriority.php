<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

	userlogin();
	
	if($CURUSER[userclass] < 50)
	header("location: index.php");
	
head("Utazási ajánlatok");

$pid = (int)$_GET[pid];
$category = (int)$_GET[category];
$country_id = (int)$_GET[country_id];
$name = $_GET[name];

$active = $_GET[aleph_ok];


if($_GET[update] == 1)
{
	$mysql_tour->query("UPDATE tour SET priority = 5000");


	$offers = $mysql_tour->query("SELECT * FROM priority ORDER BY priority ASC LIMIT 500");
	while($arr = mysql_fetch_assoc($offers))
	{
		
		if($arr[region_id] > 0)
			$eregion = "AND region_id = $arr[region_id]";
		else
			$eregion = '';
		$mysql_tour->query("UPDATE tour SET priority = $arr[priority] WHERE partner_id = $arr[partner_id] AND country_id = $arr[country_id] $eregion AND priority_special = 0");
			echo message("Sikeresen frissítette a prioritást!");

	}
}

?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='tourpriority.php' class="<? if($_GET[disabled] <> 1) echo "current"?>">Utazás prioritások</a></li>
						<li><a href='touroffers.php' class="<? if($_GET[disabled] == 1) echo "current"?>">Minden utazás</a></li>
						</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<?

if($_POST[id] > 0)
{
/*
	$mysql_tour->query("UPDATE tour SET own_id = $_POST[own_id] WHERE id = '$_POST[id]'");
	
	
*/
		
	$mysql_tour->query_update("priority",$_POST,"id=$_POST[id]");
	writelog("$CURUSER[username] updated priority item $_POST[id]");
	echo message("Sikeresen szerkesztette a prioritást!");
}
elseif($_POST[partner_id] > 0)
{

	
	$mysql_tour->query_insert("priority",$_POST);
	
	$newid = mysql_insert_id();
	writelog("$CURUSER[username] created priority for item $newid");
	
		
	
	echo message("Sikeresen létrehozta a prioritást!");
}

if($_GET[delete] > 0)
{
	$offer = $mysql_tour->query("DELETE FROM priority WHERE id = '$_GET[delete]'");
	echo message("Sikeresen törölte a prioritást!");

}

if($_GET[edit] > 0 || $_GET[add] == 1)
{
	$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM priority WHERE id = '$_GET[edit]' LIMIT 1"));

?>
<form method="post" action='tourpriority.php'>

<fieldset>
	<legend>Utazási prioritások kezelése</legend>
	
		<input type='hidden' name='id' value='<?=$offer[id]?>'/>

	<script>
	$(document).ready(function() {
		$( ".country_select" ).change(function() {
		
			$(".region_select option").each(function() {
				$(this).remove();
			});

		$(".region_select").append("<option value='0'>-</option>");

		});
	});
	
	
	</script>
	
	<table width='100%'>
	
		
		<? if($offer[id] > 0) { ?>
			<tr>
				<td class='lablerow'>Azonosító:</td>
				<td><?=$offer[id]?></td>
			</tr>
		<? } ?>
			<tr>
				<td class='lablerow'>Partner:</td>
				<td>
					<select name='partner_id'>
						<?
						$query = $mysql_tour->query("SELECT * FROM partner WHERE partnertype_id = 1 ORDER BY name ASC");
						
						while($arr = mysql_fetch_assoc($query))
						{
							if($arr[id] == $offer[partner_id])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[id]' $select>$arr[name]</option>";
						}
						?>					
					</select>
				</td>
			</tr>
			<tr>
				<td class='lablerow'>Ország:</td>
				<td>
					<select name='country_id' class='country_select'>
						<?
						$query = $mysql_tour->query("SELECT * FROM country ORDER BY name ASC");
						
						while($arr = mysql_fetch_assoc($query))
						{
							if($arr[id] == $offer[country_id])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[id]' $select>$arr[name]</option>";
						}
						?>					
					</select>
				</td>
			</tr>
			<? if($offer[country_id] > 0) { ?>
			<tr>
				<td class='lablerow'>Régió:</td>
				<td>
					<select name='region_id' class='region_select'>
						<option value='0'>Kérem válasszon</option>
						<?
						$query = $mysql_tour->query("SELECT * FROM region WHERE country_id = $offer[country_id] ORDER BY name ASC");
						
						while($arr = mysql_fetch_assoc($query))
						{
							if($arr[id] == $offer[country_id])
								$select = 'selected';
							else
								$select = '';
							echo "<option value='$arr[id]' $select>$arr[name]</option>";
						}
						?>					
					</select>
				</td>
			</tr>
			<? } ?>
			<tr>
				<td class='lablerow'>Prioritás</td>
				<td><input type='text' name='priority' value='<?=$offer[priority]?>'/></td>
			</tr>		
	</table>
	
	<input type='submit' value='Mentés'/>
</fieldset>

</form>
<?

foot();
die;
}
?>


<table>
<tr>
	<td colspan='8' align='center'><a href='?add=1'><b>Új prioritás</b></a> | <a href='?update=1'><b>Prioritások frissítése</b></a></td>
</tr>
	<tr class='header'>
		<td colspan='2'>-</td>
		<td>Partner</td>
		<td>Ország</td>
		<td>Régió</td>
		<td>Prioritás</td>
	</tr>
<?
	
				
		$offers = $mysql_tour->query("SELECT * FROM priority ORDER BY priority ASC LIMIT 500");
		
		
		$i = 1;
		while($arr = mysql_fetch_assoc($offers))
		{
			$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $arr[partner_id]"));
			
			$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = $arr[city_id]"));
			$country = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM country WHERE id = $arr[country_id]"));
			
			$region = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM region WHERE id = $arr[region_id]"));

			echo "<tr>
				<td align='center' width='20'>$i.</td>
					<td width='20'>
					<a href='?delete=$arr[id]'><img src='/images/trash.png' width='20'/></a>
						<a href='?edit=$arr[id]'><img src='/images/edit.png' width='20'/></a>
					</td>
					<td>$partner[name]</td>
					<td>$country[name]</td>
					<td>$region[name]</td>
					<td align='right'>$arr[priority]</td>
				</tr>";
	
			$i++;
		}
?>

</table>
</div></div>
<?
foot();
?>