<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

userlogin();

	
	if($CURUSER[userclass] < 10)
	header("location: index.php");
	
head("Utazási ajánlatok fordítása");


?>
<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='tourtranslate.php' class="current">Utazások fordítása</a></li>
					</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>

<?

if($_POST[id] > 0)
{
	
	$mysql_tour->query_update("tour",$_POST,"id=$_POST[id]");
	writelog("$CURUSER[username] translated tour item $_POST[id]");
	echo message("Sikeresen szerkesztette az utat!");
}

if($_GET[edit] > 0 || $_GET[add] == 1)
{
	$offer = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM tour WHERE id = '$_GET[edit]' LIMIT 1"));

?>
<form method="post" action='tourtranslate.php'>

<fieldset>
	<legend>Utazási ajánlatok fordítása</legend>
	
		<input type='hidden' name='id' value='<?=$offer[id]?>'/>

	
	
	<table width='100%'>
	
		
		<? if($offer[id] > 0) { ?>
			<tr>
				<td class='lablerow'>Azonosító:</td>
				<td><?=$offer[id]?></td>
			</tr>
		<? } ?>
			<tr>
				<td class='lablerow'>Ajánlat neve:</td>
				<td><?=$offer[name]?></td>
			</tr>

	
			<tr>
				<td class='lablerow'>Leírás:</td>
				<td>
				
				<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '../jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "code,save,source,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
extended_valid_elements: "iframe[class|src|frameborder=0|alt|title|width|height|align|name]",
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
			
			force_br_newlines : true,
			force_p_newlines : false,

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>


						<textarea  rows="15" cols="20" style="width: 480px" class="tinymce"  id="templateBody"><?=$offer[description]?></textarea>
						


				</td>
			</tr>
			<tr>
				<td class='lablerow'>Román leírás:</td>
				<td><textarea  rows="15" cols="20" style="width: 480px" class="tinymce"  id="templateBody" name='ro_description'><?=$offer[ro_description]?></textarea>
				</td>
			</tr>
				
	</table>
	
	<input type='submit' value='Mentés'/>
</fieldset>
<?

foot();
die;
}
 
?>

<table>
<tr>
</tr>
	<tr class='header'>
		<td colspan='2'>-</td>
		<td>Partner</td>
		<td>Ajánlat</td>
		<td>Ország</td>
		<td>Város</td>
	</tr>
<?

				
		$offers = $mysql_tour->query("SELECT tour.id, tour.* FROM tour 
		
		LEFT JOIN offer_property ON offer_property.offer_id = tour.id
		LEFT JOIN prices ON prices.offer_id = tour.id
		WHERE tour.id > 0 and ro_translate = 1 GROUP BY tour.id ORDER BY aleph_ok,name ASC LIMIT 500");
		
		
		$i = 1;
		while($arr = mysql_fetch_assoc($offers))
		{
			$partner = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM partner WHERE id = $arr[partner_id]"));
			
			$city = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM city WHERE id = $arr[city_id]"));
			$country = mysql_fetch_assoc($mysql_tour->query("SELECT * FROM country WHERE id = $arr[country_id]"));
			
			if($arr[ro_description] <> '')
				$class = 'blue';
			else
				$class = '';
				
			if($arr[lastminute] == 1)
				$lmcheckbox = 'selected';
			else
				$lmcheckbox = '';	
			echo "<tr class='$class'>
				<td align='center' width='20'>$i.</td>
					<td width='20' align='center'>
						<a href='?edit=$arr[id]'><img src='/images/edit.png' width='20'/></a>
						</td>
					<td>$partner[name]</td>
					<td><a href='$arr[page_url]' target='_blank'><b>$arr[name]</b></a></td>
					<td>$country[name]</td>
					<td>$city[name]</td>
				<!--	<td>
						<form method='GET'>
							<input type='hidden' name='offer_id' value='$arr[id]'/>
							<select name='lastminute' onchange='submit();'>
								<option value='0'>Nem</option>
								<option value='1' $lmcheckbox>Igen</option>
							</select>	
							
						</form>
						
					</td>	-->
				</tr>";
	
			$i++;
		}

?>

</table>
</div></div>
<?
foot();
?>