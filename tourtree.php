<?
/*
 * getoffers.php 
 *
 * the offers page
 *
*/

/* bootstrap file */
include("inc/tour.init.inc.php");
//check if user is logged in or not

userlogin();

	if($CURUSER[userclass] < 50)
	header("location: index.php");

if($_POST[lookup] == 1 )
{
?>
[
	{
		"text": "1. Pre Lunch (120 min)",
		"expanded": true,
		"classes": "important",
		"children":
		[
			{
				"text": "1.1 The State of the Powerdome (30 min)"
			},
		 	{
				"text": "1.2 The Future of jQuery (30 min)"
			},
		 	{
				"text": "1.2 jQuery UI - A step to richnessy (60 min)"
			}
		]
	},
	{
		"text": "2. Lunch  (60 min)"
	},
	{
		"text": "3. After Lunch  (120+ min)",
		"children":
		[
			{
				"text": "3.1 jQuery Calendar Success Story (20 min)"
			},
		 	{
				"text": "3.2 jQuery and Ruby Web Frameworks (20 min)"
			},
		 	{
				"text": "3.3 Hey, I Can Do That! (20 min)"
			},
		 	{
				"text": "3.4 Taconite and Form (20 min)"
			},
		 	{
				"text": "3.5 Server-side JavaScript with jQuery and AOLserver (20 min)"
			},
		 	{
				"text": "3.6 The Onion: How to add features without adding features (20 min)",
				"id": "36",
				"hasChildren": true
			},
		 	{
				"text": "3.7 Visualizations with JavaScript and Canvas (20 min)"
			},
		 	{
				"text": "3.8 ActiveDOM (20 min)"
			},
		 	{
				"text": "3.8 Growing jQuery (20 min)"
			}
		]
	}
]
<?
die;
}
head("Utazási ajánlatok");
?>

<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
						<li><a href='passengers.php' class="">Utasok</a></li>
						<li><a href='?all=1' class="<? if($_GET[disabled] <> 1 && $_GET[fixprice] <> 1) echo "current"?>">Minden utazás</a></li>
						<li><a href='?disabled=1' class="<? if($_GET[disabled] == 1) echo "current"?>">Letiltások</a></li>
						<li><a href='?fixprice=1' class="<? if($_GET[fixprice] == 1) echo "current"?>">Ármódosítások</a></li>
						<li><a href='tourpriority.php'>Prioritások</a></li>

						</ul>
		<div class="clear"></div>
</div>
<div class='contentpadding'>


<!-- -->
<style>
	.treeview, .treeview ul {
	padding: 0;
	margin: 0;
	list-style: none;
}

.treeview ul {
	background-color: white;
	margin-top: 4px;
}

.treeview .hitarea {
	background: url(images/treeview/treeview-default.gif) -64px -25px no-repeat;
	height: 16px;
	width: 16px;
	margin-left: -16px;
	float: left;
	cursor: pointer;
}
/* fix for IE6 */
* html .hitarea {
	display: inline;
	float:none;
}

.treeview li {
	margin: 0;
	padding: 3px 0pt 3px 16px;
}

.treeview a.selected {
	background-color: #eee;
}

#treecontrol { margin: 1em 0; display: none; }

.treeview .hover { color: red; cursor: pointer; }

.treeview li { background: url(images/treeview/treeview-default-line.gif) 0 0 no-repeat; }
.treeview li.collapsable, .treeview li.expandable { background-position: 0 -176px; }

.treeview .expandable-hitarea { background-position: -80px -3px; }

.treeview li.last { background-position: 0 -1766px }
.treeview li.lastCollapsable, .treeview li.lastExpandable { background-image: url(images/treeview/treeview-default.gif); }
.treeview li.lastCollapsable { background-position: 0 -111px }
.treeview li.lastExpandable { background-position: -32px -67px }

.treeview div.lastCollapsable-hitarea, .treeview div.lastExpandable-hitarea { background-position: 0; }

.treeview-red li { background-image: url(images/treeview/treeview-red-line.gif); }
.treeview-red .hitarea, .treeview-red li.lastCollapsable, .treeview-red li.lastExpandable { background-image: url(images/treeview/treeview-red.gif); }

.treeview-black li { background-image: url(images/treeview/treeview-black-line.gif); }
.treeview-black .hitarea, .treeview-black li.lastCollapsable, .treeview-black li.lastExpandable { background-image: url(images/treeview/treeview-black.gif); }

.treeview-gray li { background-image: url(images/treeview/treeview-gray-line.gif); }
.treeview-gray .hitarea, .treeview-gray li.lastCollapsable, .treeview-gray li.lastExpandable { background-image: url(images/treeview/treeview-gray.gif); }

.treeview-famfamfam li { background-image: url(images/treeview/treeview-famfamfam-line.gif); }
.treeview-famfamfam .hitarea, .treeview-famfamfam li.lastCollapsable, .treeview-famfamfam li.lastExpandable { background-image: url(images/treeview/treeview-famfamfam.gif); }

.treeview .placeholder {
	background: url(images/treeview/ajax-loader.gif) 0 0 no-repeat;
	height: 16px;
	width: 16px;
	display: block;
}

.filetree li { padding: 3px 0 2px 16px; }
.filetree span.folder, .filetree span.file { padding: 1px 0 1px 16px; display: block; }
.filetree span.folder { background: url(images/treeview/folder.gif) 0 0 no-repeat; }
.filetree li.expandable span.folder { background: url(images/treeview/folder-closed.gif) 0 0 no-repeat; }
.filetree span.file { background: url(images/treeview/file.gif) 0 0 no-repeat; }


	</style>
	<script src="/inc/js/jquery.treeview.js" type="text/javascript"></script>
	<script src="/inc/js/jquery.treeview.async.js" type="text/javascript"></script>

	<script type="text/javascript">

	$(document).ready(function(){
		$("#mixed").treeview({
			url: "tourtree.php",
			// add some additional, dynamic data and request with POST
			ajax: {
				data: {
					'lookup': 1,
					"additional": '2222',
				},
				type: "post"
			}
		});
	});
	</script>



	<h4>Mixed pre and lazy-loading</h4>

	<ul id="mixed">
		<li><span>Item 1</span>
			<ul>
				<li><span>Item 1.0</span>
					<ul>
						<li><span>Item 1.0.0</span></li>
					</ul>
				</li>
				<li><span>Item 1.1</span></li>
			</ul>
		</li>
		<li id="36" class="hasChildren">
			<span>Item 2</span>
			<ul>
				<li><span class="placeholder">&nbsp;</span></li>
			</ul>
		</li>
		<li>
			<span>Item 3</span>
		</li>
	</ul>
<!-- -->

</div></div>
<?
foot();
?>