<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

$positions = array(
    0 =>  "Egyéb",
    1 =>  "Értékesítő",   
    2 =>  "Szállás Outlet RO",
    3 =>  "SzállásOutlet RO ügyfélszolgálat",
    4 =>  "Szlovákia",
    5 =>  "Call-center",
    6 =>  "Értékesítési vezető",
    7 =>  "Indulhatunk belföld marketing",
    8 =>  "Irodákkal / Partnerekkel kapcsolattartás",
    9 =>  "IT",
    10 =>  "Outlet külföldi lekérések",
    11 =>  "Outlet mindenes",
    12 =>  "Pénzügy",
    13 =>  "SzállásOutlet megjelenések szerkesztése",
    14 =>  "Ügyvezető",
);

if($_GET[vcf] == 1)
{

header('Content-Type: text/x-vcard');  
header('Content-Disposition: inline; filename=mindenki.vcf'); 

$qr = $mysql->query("SELECT * FROM partners WHERE userclass >= 40 AND pid <> 897 $extras AND disabled = 0 ORDER BY office_id, position ASC");

while($arr = mysql_Fetch_assoc($qr))
{
	$fn = explode(" ",$arr[company_name]);
	$phone = str_replace(array(" ", '-'),array(),$arr[phone]);
		
	$office = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = '$arr[office_id]' LIMIT 1"));

if($phone <> '')
{
echo "BEGIN:VCARD
VERSION:3.0
PRODID:-//Apple Inc.//Mac OS X 10.10.2//EN
N:$fn[0];$fn[1];;;
FN:$arr[company_name]
ORG:SzállásOutlet Kft.
TITLE:$arr[position]
TEL;type=CELL;type=VOICE;type=pref:$phone
ADR;WORK:;;$office[address];$office[city];;$office[zip];Magyarország
EMAIL;PREF;INTERNET:$arr[email]
END:VCARD
";
}
}

die;
}




//check if user is logged in or not
userlogin();


//if($CURUSER[userclass] < 50)
//	header("location:index.php");
	
	
head('Kapcsolat');


if(($CURUSER[passengers_admin] == 1 || $CURUSER[username] == 'angela') && $_GET[reset] > 0) {

	$mysql->query("UPDATE partners SET last_offer_request = '' WHERE pid = $_GET[reset]");
	echo message("A felhasználó sikeresen feloldva!");
	writelog("$CURUSER[username] reset user #$_GET[reset]");
}


if($CURUSER[userclass] < 50)
	$extras = "AND position <> ''";
	
$edit = $_POST[editid];
$editid = (int)$_GET[edit];

if($editid > 0) {
	$query = $mysql->query("SELECT * FROM partners WHERE pid = $editid");
	$editarr = mysql_fetch_assoc($query);
}
$data[coredb_id] = $_POST[coredb_id];
$data[coredb_package] = $_POST[coredb_package];
$data[company_name] = $_POST[company_name];
$data[hotel_name] = $_POST[hotel_name];
$data[tax_no] = $_POST[tax_no];
$data[partner_id] = $_POST[partner_id];
$data[contact] = $_POST[contact];
$data[address] = $_POST[address];
$data[last_offer_request] = $_POST[last_offer_request];

$check = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$edit'"));

if($check[office_id] <> $_POST[office_id] && !empty($_POST))
{
	$oototal = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total FROM checkout_vtl WHERE username = '$check[username]' AND added > '2014-07-04'"));
	//echo formatPrice($oototal[total]);
	if($oototal[total] > 0)
	{
		echo message("A pénztárában van pénz! (".formatPrice($oototal[total]).")");
		die();
	}
}

if($CURUSER[userclass] == 255)
{
	 $data[disabled] = $_POST[disabled];
	 $data[employer] = $_POST[employer];
	 $data[dayoffs] = $_POST[dayoffs];
}

$data[zip] = $_POST[zip];
$data[city] = $_POST[city];
$data[phone] = $_POST[phone];
$data[reception_phone] = $_POST[reception_phone];
$data[reception_email] = $_POST[reception_email];
$data[email] = $_POST[email];
$data['yield'] = $_POST['yield'];
$data[username] = $_POST[username];
//$data[password] = $_POST[password];
$data[account_no] = $_POST[account_no];
$data[office_id] = $_POST[office_id];

$data[position] = $_POST[position];

if($_FILES["signature"]["name"] <> '')
{
	move_uploaded_file($_FILES["signature"]["tmp_name"],"/var/www/lighttpd/admin.indulhatunk.info/images/signature/$_POST[username].png");
}

if($_FILES["profile"]["name"] <> '')
{
	move_uploaded_file($_FILES["profile"]["tmp_name"],"/var/www/lighttpd/admin.indulhatunk.info/images/profile/$_POST[username].jpg");
}

$data[property_main] = $_POST[property_main];

if($edit == "" && $data[company_name] <> "") {
	$data[password] = 'indulhatunk12@';
	$data[userclass] = '90';
	$data[vatera] = '1';
	$data[outlet] = '1';
	$data[passengers] = '1';
	
	$mysql->query_insert("partners",$data);
	writelog("$CURUSER[username] successfully created user: $data[company_name]");
	$msg = "Sikeresen felvitte a partnert!";
}
elseif($edit > 0) {
	$mysql->query_update("partners",$data,"pid=$edit");
		writelog("$CURUSER[username] successfully edited user: $data[company_name]");
	$msg = "Sikeresen szerkesztette a partnert!";
}
mysql_query("SET NAMES 'utf8'"); 
$qr = "SELECT * FROM partners WHERE userclass >= 40 AND pid <> 897 $extras ORDER BY office_id, position ASC";
//print $qr;
$query = mysql_query($qr); 

echo "<h1><a href=\"http://admin.indulhatunk.info\">Vatera adminisztrációs felület</a> &raquo; Felhasználók kezelése</h1>";



if($CURUSER[passengers_admin] == 1)
{
	echo "<div class='subMenu'><a href=\"users.php?add=1\">Új felhasználó felvitele</a><div class='cleaner'></div></div>";
}
if($msg <> '')
	echo "<div class='notify'>$msg</div>";

$add = $_REQUEST[add];

if($add == 1 || $editid > 0)
 $show = "";
else
	$show = "style='display:none;'";

echo message("Figyelem! Hogyha egy usert átmozgatsz egy másik irodába, akkor a pénztárát nullázni kell először!");
?>

<div class="partnerForm" <?=$show?>>
	<form method="POST" action="users.php" enctype="multipart/form-data">
		<input type="hidden" name="editid" value="<?=$editarr[pid]?>">
	<fieldset>
		<legend>Partnerek kezelése</legend>
	<ul>
		<li><label>Név</label><input type="text" name="company_name" value="<?=$editarr[company_name]?>"/></li>
		<!--<li><label>Felhasználónév:</label><input type="text" name="username" value="<?=$editarr[username]?>"/></li>
		<li><label>Jelszó:</label><input type="text" name="password" value="<?=$editarr[password]?>"/></li>
		<li><label>Rang:</label><input type="text" name="userclass" value="<?if($editarr[userclass]=='') echo '100'; else echo $editarr[userclass]; ?>"/></li>-->
		<li><label>E-mail:</label><input type="text" name="email" value="<?=$editarr[email]?>"/></li>
		<li><label>Privát tel.:</label><input type="text" name="reception_phone" value="<?=$editarr[reception_phone]?>"/></li>
		<li><label>Gtalk:</label><input type="text" name="reception_email" value="<?=$editarr[reception_email]?>"/></li>
		<li><label>Céges tel.:</label><input type="text" name="phone" value="<?=$editarr[phone]?>"/></li>
		<li><label>Pozíció:</label><!-- <input type="text" name="position" value="<?=$editarr[position]?>"/>  -->
			<select name="position">
				<?php 
				    foreach($positions as $position) {
				        print "<option value='$position' ";
				        if($editarr[employer]==$position) echo"selected";
				        print ">$position</option>";
				    }
				?>
				
				<option value="hoteloutlet" <?if($editarr[employer]=='hoteloutlet')echo"selected";?>>Hotel Outlet</option>
			</select>
		
		</li>
		
		<li><label>Utolsó lekérés:</label><input type="text" name="last_offer_request" value="<?=$editarr[last_offer_request]?>"/></li>

		<? if($editid > 0) { ?>
			<input type="hidden" name="username" value="<?=$editarr[username]?>">
		<? } else { ?>
			<li><label>Felhasználónév:</label>	<input type="text" name="username" value="<?=$editarr[username]?>"></li>
		<? } ?>

<? if($CURUSER[userclass] == 255)
{
	if($_GET[add] == 1 && $_GET[edit] == '')
	{
		?>
		<input type='hidden' name='userclass' value='90'/>
		<input type='hidden' name='outlet' value='1'/>
		<input type='hidden' name='vatera' value='1'/>
		<input type='hidden' name='passengers' value='1'/>
		<input type='hidden' name='position' value='Értékesítő'/>

		<?
	}
	else
	{
?>
	<li><label>Kilépett:</label>
			<select name="disabled">
				<option value="1" <?if($editarr[disabled]==1)echo"selected";?>>Igen</option>
				<option value="0" <?if($editarr[disabled]==0)echo"selected";?>>Nem</option>
			</select>
		</li>

		<li><label>Vatera:</label>
			<select name="vatera">
				<option value="1" <?if($editarr[vatera]==1)echo"selected";?>>Igen</option>
				<option value="2" <?if($editarr[vatera]==0)echo"selected";?>>Nem</option>
			</select>
		</li>
		<li><label>LMB:</label>
			<select name="lmb">
				<option value="1" <?if($editarr[lmb]==1)echo"selected";?>>Igen</option>
				<option value="2" <?if($editarr[lmb]==0)echo"selected";?>>Nem</option>
			</select>
		</li>
		<li><label>Indulhatunk:</label>
			<select name="indulhatunk">
				<option value="1" <?if($editarr[indulhatunk]==1)echo"selected";?>>Igen</option>
				<option value="2" <?if($editarr[indulhatunk]==0)echo"selected";?>>Nem</option>
			</select>
		</li>
		<li><label>Outlet:</label>
			<select name="outlet">
				<option value="1" <?if($editarr[outlet]==1)echo"selected";?>>Igen</option>
				<option value="2" <?if($editarr[outlet]==0)echo"selected";?>>Nem</option>
			</select>
		</li>
		<li><label>Elszámolás:</label>
			<select name="accounting">
				<option value="1" <?if($editarr[accounting]==1)echo"selected";?>>Igen</option>
				<option value="2" <?if($editarr[accounting]==0)echo"selected";?>>Nem</option>
			</select>
		</li>
	<? } ?>
	
	<li><label>Munkáltató:</label>
			<select name="employer">
				<option value="indulhatunk" <?if($editarr[employer]=='indulhatunk')echo"selected";?>>Indulhatunk</option>
				<option value="hoteloutlet" <?if($editarr[employer]=='hoteloutlet')echo"selected";?>>Hotel Outlet</option>
			</select>
		</li>
	<? } ?>
	
		<li><label>Iroda:</label>
			<select name="office_id">
				<option value="0" <?if($editarr[office_id]== 0)echo"selected";?>>Kérem válasszon</option>
			<? $offices = $mysql->query("SELECT * FROM partner_offices WHERE pid = 3404 ORDER BY id ASC");
				while($a = mysql_fetch_assoc($offices)) { 
					
					if($editarr[office_id] == $a[id])
						$sel = 'selected';
					else
						$sel = '';
					echo "<option value='$a[id]' $sel>$a[city] $a[address]</option>";	
					
				}
			?>
			</select>
		</li>
		<li><label>Szabadságok:</label><input type="text" name="dayoffs" value="<?=$editarr[dayoffs]?>"/></li>
		<li>
			<label>Pénztáregyenleg</label>
			<?
				
				$oototal = mysql_fetch_assoc($mysql->query("SELECT sum(value) as total FROM checkout_vtl WHERE username = '$editarr[username]' AND added > '2014-07-04'"));
				echo formatPrice($oototal[total]);
				
					if(@fopen("images/signature/$editarr[username].png",'r') == TRUE)
			$logo = "<a href='/images/signature/$editarr[username].png' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='25'/></a>";
		else
			$logo = "<img src='http://admin.indulhatunk.info/images/cross1.png' width='25'/>";
		
		if(@fopen("images/profile/$editarr[username].jpg",'r') == TRUE)
			$profile = "<a href='/images/profile/$editarr[username].jpg' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='25'/></a>";
		else
			$profile = "<img src='http://admin.indulhatunk.info/images/cross1.png' width='25'/>";



			?>
			
		</li>
		<li><label>Aláírás (199x124):</label><input type="file" name="signature"/> <?=$logo?></li>
		<li><label>Kép (180x230):</label><input type="file" name="profile"/> <?=$profile?></li>

		<li><input type="submit" value="Mentés" /></li>
	</ul>
	</fieldset>
	</form>
</div>
<?
echo "<hr/>";
echo "<table class=\"general\">";
echo "<tr class='header'><td colspan='30'><a href='?vcf=1'>Névjegyek letöltése &raquo;</a></td></tr>";
echo "<tr class=\"header\">";

if($CURUSER[passengers_admin] == 1)
{
		echo "<td>-</td>";
}

if($CURUSER[userclass] == 255)
{
	echo "<td>No.</td>";
}
	echo "<td>Neve</td>";
	echo "<td>Beosztás</td>";
	echo "<td>Iroda</td>";
if($CURUSER[passengers_admin] == 1)
{
	echo "<td colspan='2'>Képek</td>";
}
if($CURUSER[userclass] == 255)
{
	/*echo "<td>Vatera</td>";
	echo "<td>LMB</td>";
	echo "<td>Indulhatunk</td>";
	echo "<td>Outlet</td>";
	echo "<td>Elszámolás</td>";*/
	echo "<td>Felhasználónév</td>";
	echo "<td>Jelszó</td>";
}	
	echo "<td>E-mail</td>";
	echo "<td>GTalk</td>";
	echo "<td>Céges tel.</td>";
	
if($CURUSER[userclass] > 50)
	echo "<td>Privát tel.</td>";
	
if($CURUSER[passengers_admin] == 1 || $CURUSER[username] == 'angela') {
	echo "<td>Feloldás</td>";
}
echo "</tr>";


while($arr = mysql_fetch_assoc($query)) {

if($arr[disabled] == 1)
	$class = 'grey';
else
	$class = '';
	echo "<tr class='$class'>";
	
if($CURUSER[passengers_admin] == 1)
{
	echo "<td> <a href=\"?edit=$arr[pid]\"><img src='images/edit.png' alt='szerkeszt' title='szerkeszt' width='30'/></a></td>";
}
if($CURUSER[userclass] == 255)
{
		echo "<td>$arr[pid]</td>";
}
		echo "<td><b>$arr[company_name] $arr[hotel_name]</b> </td>";
		echo "<td>$arr[position]</td>";

$office = mysql_fetch_assoc($mysql->query("SELECT * FROM partner_offices WHERE id = $arr[office_id]"));
		echo "<td>$office[city] $office[address]</td>";


		if($arr[vatera] == 1)
			$vatera = "/images/check1.png";
		else
			$vatera = "/images/cross1.png";
			
		if($arr[lmb] == 1)
			$lmb = "/images/check1.png";
		else
			$lmb = "/images/cross1.png";
		
		if($arr[indulhatunk] == 1)
			$indulhatunk = "/images/check1.png";
		else
			$indulhatunk = "/images/cross1.png";
			
		if($arr[outlet] == 1)
			$outlet = "/images/check1.png";
		else
			$outlet = "/images/cross1.png";
			
		if($arr[accounting] == 1)
			$accounting = "/images/check1.png";
		else
			$accounting = "/images/cross1.png";

if($CURUSER[passengers_admin] == 1)
{
	
	
		if(@fopen("images/signature/$arr[username].png",'r') == TRUE)
			$logo = "<a href='/images/signature/$arr[username].png' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='25'/></a>";
		else
			$logo = "<img src='http://admin.indulhatunk.info/images/cross1.png' width='25'/>";
		
		if(@fopen("images/profile/$arr[username].jpg",'r') == TRUE)
			$profile = "<a href='/images/profile/$arr[username].jpg' target='_blank' rel='facebox'><img src='http://admin.indulhatunk.info/images/check1.png' width='25'/></a>";
		else
			$profile = "<img src='http://admin.indulhatunk.info/images/cross1.png' width='25'/>";
		
		
	echo "<td>$logo</td>";
	echo "<td>$profile</td>";
	
}
if($CURUSER[userclass] == 255)
{
	/*	echo "<td align='center'><img src=\"$vatera\" width='30' /></td>";
		echo "<td align='center'><img src=\"$lmb\" width='30' /></td>";
		echo "<td align='center'><img src=\"$indulhatunk\" width='30' /></td>";
		echo "<td align='center'><img src=\"$outlet\" width='30' /></td>";
		echo "<td align='center'><img src=\"$accounting\" width='30' /></td>";*/
		echo "<td>$arr[username]</td>";
		echo "<td>$arr[password]</td>";
}		
		echo "<td><a href='mailto:$arr[email]'>$arr[email]</a></td>";
		echo "<td><a href='mailto:$arr[reception_email]'>$arr[reception_email]</a></td>";
		echo "<td align='right' width='100'>$arr[phone]</td>";
	
if($CURUSER[userclass] > 50)
		echo "<td width='100'>$arr[reception_phone]</td>";

if($CURUSER[passengers_admin] == 1 || $CURUSER[username] == 'angela') {
	
	$now = time();
	$left = round(($now - strtotime($arr[last_offer_request]))/60/60,2);
					
	if($left > 168)
		$left = '-';
				
				
	echo "<td width='100'><a href='?reset=$arr[pid]'>$left órája kért &raquo;</a></td>";
}
	echo "</tr>";
	
}
echo "</table>";
foot();
?>