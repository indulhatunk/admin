<?php
@header('Content-type: text/html; charset=utf-8');
include("../inc/config.inc.php");
include("../inc/mysql.class.php");
include("../inc/functions.inc.php");


require_once('lib/config.inc.php');
require_once(HTML2PS_DIR.'pipeline.factory.class.php');
parse_config_file(HTML2PS_DIR.'html2ps.config');

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();


if($CURUSER[reseller_print_disabled] == 1)
{
	head("Hozzáférés letiltva");
		echo message("A hozzáférését ideiglenesen letiltottuk. <br/><br/>Kérjük keresse fel ügyfélszolgálatunkat!");
	foot();
	die;
}
//head();



if($CURUSER[userclass] == 5)
	$reseller = '&reseller=1';
	
	
$sure = $_GET[sure];

$cid = $_GET[cid];

$type = $_GET[type];

$archive = $_GET[archive];

$hash = $_GET[hash];

if($type == '' && $cid == '' && $CURUSER[userclass] > 50)
{
	head();
		echo "<!-- <a class='button red' href='?type=viapost'>Utánvétes Voucherek nyomtatása</a> -->
					<fieldset>
						<legend>Mindhárom gombot használni kell!</legend>
					<a class='button blue' href='?type=normal' rel='facebox iframe'>Normál voucherek nyomtatása</a>
					<a class='button blue' href='?type=normal&qr=1' rel='facebox iframe'>QR voucherek nyomtatása</a>
					<a class='button blue' href='?type=normal&special=1' rel='facebox iframe'>Speciális voucherek nyomtatása</a>
					</fieldset>

					<center><a class='button grey' href='archive.php'>Archiv utalványok nyomtatása</a></center>
					<div class='cleaner'></div>";

	foot();
	die;
}
if($sure <> 1)
{
	
	echo "<script>
	$(document).ready(function() {

	 $('.closefacebox').click(function()
 	 {
		$(document).trigger('close.facebox');
		return false;
	});
	 $('.closefaceboxtrue').click(function()
 	 {
		$(document).trigger('close.facebox');
		return true;
	});
	});
	</script><h2>Biztosan ki szeretné nyomtatni az utalványt?</h2><br/>  <a href=\"/vouchers/print.php?sure=1&cid=$_GET[cid]&qr=$_GET[qr]&special=$_GET[special]&type=$type&hash=$hash&archive=$archive&lira=$_GET[lira]\" class='button green closefaceboxtrue'>Igen</a> <a href=\"?all=1\" class='button red closefacebox'>Nem</div>";

	die;
}


global $g_config;
$g_config = array(
                  'cssmedia'     => 'screen',
                  'renderimages' => true,
                  'renderforms'  => false,
                  'renderlinks'  => false,
                  'mode'         => 'html',
                  'debugbox'     => false,
                  'draw_page_border' => false
                  );

$media = Media::predefined('A4');
$media->set_landscape(false);
$media->set_margins(array('left'   => 0,
                          'right'  => 0,
                          'top'    => 0,
                          'bottom' => 0));
$media->set_pixels(870);

global $g_px_scale;
$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;

global $g_pt_scale;
$g_pt_scale = $g_px_scale * 1.43; 

$pipeline = PipelineFactory::create_default_pipeline("","");
$pipeline->configure($g_config);



if($_GET[test] == 1)
{
	//$pipeline->process("http://admin.indulhatunk.hu/test.php?cid=$cid", $media); 
	$pipeline->process(getProtocol() ."admin.indulhatunk.hu/rotest/test.php", $media); 
	die;
}


if($_GET[lira]  == 1)
{
	$pipeline->process(getProtocol() . "admin.indulhatunk.hu/vouchers/voucher_lira.php?cid=$cid&username=$CURUSER[username]$reseller", $media); 
}
elseif($_GET[lira]  == 2)
{
	$pipeline->process(getProtocol() . "admin.indulhatunk.hu/vouchers/voucher_taxi.php?cid=$cid&username=$CURUSER[username]$reseller", $media); 
}
elseif($cid <> "")
{
	$pipeline->process(getProtocol() . "admin.indulhatunk.hu/vouchers/voucher_v2.php?cid=$cid&username=$CURUSER[username]$reseller", $media); 
}
else
{
	if($hash == '')
		$hash = generateHash(32);
	
	$pipeline->process(getProtocol() . "admin.indulhatunk.hu/vouchers/voucher_v2.php?&username=$CURUSER[username]&type=$type&hash=$hash&archive=$archive&qr=$_GET[qr]&special=$_GET[special]", $media); 
}

?>