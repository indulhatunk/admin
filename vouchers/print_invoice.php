<?php
@header('Content-type: text/html; charset=utf-8');

include("../inc/config.inc.php");
include("../inc/mysql.class.php");
include("../inc/functions.inc.php");


require_once('lib/config.inc.php');
require_once(HTML2PS_DIR.'pipeline.factory.class.php');
parse_config_file(HTML2PS_DIR.'html2ps.config');

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

userlogin();

//head();

$sure = $_GET[sure];

$cid = $_GET[cid];
$type = $_GET[type];


global $g_config;
$g_config = array(
                  'cssmedia'     => 'screen',
                  'renderimages' => true,
                  'renderforms'  => false,
                  'renderlinks'  => false,
                  'mode'         => 'html',
                  'debugbox'     => false,
                  'draw_page_border' => false
                  );

$media = Media::predefined('A4');
$media->set_landscape(false);
$media->set_margins(array('left'   => 0,
                          'right'  => 0,
                          'top'    => 0,
                          'bottom' => 0));
$media->set_pixels(870);

global $g_px_scale;
$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;

global $g_pt_scale;
$g_pt_scale = $g_px_scale * 1.43; 

$pipeline = PipelineFactory::create_default_pipeline("","");
$pipeline->configure($g_config);

if($_GET[sure] <> 1)
{	
	head();
	echo "<a class='button blue' href='?sure=1&type=$_GET[type]&cid=$_GET[cid]'>Bizonylat nyomtatás</a><div class='cleaner'></div>";
	foot();
	die;
}
else
{
$pipeline->process(getProtocol() ."admin.indulhatunk.info/vouchers/invoice.php?cid=$cid&type=$type", $media); 
}
?>