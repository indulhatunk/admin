<?php

header('Content-type: text/html; charset=utf-8');

require_once('lib/config.inc.php');
require_once(HTML2PS_DIR.'pipeline.factory.class.php');
parse_config_file(HTML2PS_DIR.'html2ps.config');


$sure = $_GET[sure];

$cid = $_GET[pid];
$sent = $_GET[sent];


if($sure <> 1)
{
	echo "Biztosan ki szeretne nyomtatni a listát? 
	<a href=\"?sure=1&pid=$_GET[pid]&sent=$_GET[sent]\">Igen</a> - <a href='../checkout.php?type=2'>Nem</a>";
	die;
}



global $g_config;
$g_config = array(
                  'cssmedia'     => 'screen',
                  'renderimages' => true,
                  'renderforms'  => false,
                  'renderlinks'  => false,
                  'mode'         => 'html',
                  'debugbox'     => false,
                  'draw_page_border' => false
                  );

$media = Media::predefined('A4');
$media->set_landscape(false);
$media->set_margins(array('left'   => 0,
                          'right'  => 0,
                          'top'    => 0,
                          'bottom' => 0));
$media->set_pixels(870);

global $g_px_scale;
$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;

global $g_pt_scale;
$g_pt_scale = $g_px_scale * 1.43; 

$pipeline = PipelineFactory::create_default_pipeline("","");
$pipeline->configure($g_config);

if($cid <> "")
{
	$pipeline->process(getProtocol() ."admin.indulhatunk.info/vouchers/list.php?pid=$cid&sent=$sent", $media); 
}
if($sent <> "")
{
	echo "";
}


?>