<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */

include("../inc/init.inc.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();


$id = (int)$_GET[id];
$affil = (int)$_GET[affil];

if($_GET[tour] == 1)
{
	$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM outlet_offers WHERE id = $id LIMIT 1"));
	$folder = "offers-tour";
	
	$partner[hotel_name] = $offer[hotel_name];
	$partner[city] = $offer[city];
	
}
else
{
	$offer = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE id = $id LIMIT 1"));
	
	if($offer[sub_partner_id] > 0)
		$partner = $offer[sub_partner_id];
	else
		$partner = $offer[partner_id];
		
	$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $partner LIMIT 1"));
	
	$affiliate = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $affil AND pid <> 3 LIMIT 1"));
	$folder = "offers";

}


if($_GET[color] == 'blue' || $_GET[color] == '')
{
	$color = '';
	$ccode = '#1482ce';
	$shadow = '#0c4e7c';
}
elseif($_GET[color] == 'purple')
{
	$color = 'purple';
	$ccode = '#6f2b90';
	$shadow = '#0c4e7c';
	
}
elseif($_GET[color] == 'red')
{
	$color = 'red';
	$ccode = '#de0000';
	$shadow = '#850000';
	
}
elseif($_GET[color] == 'green')
{
	$color = 'green';
	$ccode = '#7eaf08';
	$shadow = '#4c6905';
}	

$cf = $color;

if($_GET[tour] == 1 && $_GET[color] == '')
	$cf = "b";


if($_GET[tour] == 1 && $_GET[color] == 'blue')
	$cf = "b";

if($_GET[tour] == 1 && $_GET[color] == 'red')
	$cf = "r";
	
if($_GET[tour] == 1 && $_GET[color] == 'green')
	$cf = "g";
	
if($_GET['print'] == 1)
{

	$file = "/var/www/lighttpd/admin.indulhatunk.info/vouchers/cache/offer-$affil-$id.pdf";

	$t = $_GET[tour];
	$command = "wkhtmltopdf -q 'http://admin.indulhatunk.info/vouchers/print_offer_new.php?id=$id&affil&color=$color&affil=$affil&tour=$t' $file";

#echo $command;

passthru("$command");

	if ($fd = fopen ($file, "r")) {
    $fsize = filesize($file);
    $path_parts = pathinfo($file);
    $ext = strtolower($path_parts["extension"]);
     header("Content-type: application/pdf"); // add here more headers for diff. extensions
     header("Content-Disposition: attachment; filename=\"offer-".$path_parts["basename"]."\""); // use 'attachment' to force a download
     header("Content-length: $fsize");
   	 header("Cache-control: private"); //use this to open files directly
    while(!feof($fd)) {
        $buffer = fread($fd, 2048);
        echo $buffer;
    }
	}
	fclose ($fd);
	//echo $command;
	//die;

	die;
}

if($_GET['print'] <> 1)
{
	echo "";
	
	$url = bitly("http://www.akciós-szállás.hu/?offer=$offer[id]&affil=$affil");
		//$body = str_replace(formatPrice($arr[actual_price]),formatPrice($arr[reseller_price]),$offer[main_description]);
		//$body = str_replace(formatPrice($arr[actual_price],2),formatPrice($arr[reseller_price]),$body);
		//$body = str_replace("#PRICE#",formatPrice($arr[reseller_price]),$body);
		
	if($offer[pdf_image] == 0 && $_GET[tour] <> 1)
	{
		
		$image = mysql_fetch_assoc($mysql->query("SELECT * FROM offers WHERE partner_id = $partner[pid] AND pdf_image = 1 ORDER BY id DESC LIMIT 1"));
		
		$image = $image[id];
		
		if($image == '')
			die("ÉRVÉNYTELEN PDF FILE");
		//print_r($image);
	}
	else
		$image = $offer[id];

//echo $cf."<hr>";
?>


<html>
<head>
    <meta charset="UTF-8">

    <style>
	    


        html, body, div, table, thead, tbody, tr, td, p, span  {
            margin: 0;
            padding: 0;
        }

        body {
            background: #ffffff;
            font-family: 'Arial', sans-serif;
        }

        .wrapper {
            position: relative;
            width: 800px;
            height: 1150px;
            background: transparent url("/images/offers/bg.png") no-repeat 0 0;
            background-size: contain;
        }

        .header {
            height: 430px;
            background: transparent url(http://admin.indulhatunk.info/images/<?=$folder?>/<?=$image?>/1.jpg) no-repeat 0 0;
            background-size: contain;
            overflow: hidden;
        }

        .header p {
            width: 400px;
            font-weight: bold;
            font-size: 35px;
            line-height: 48px;
            color: #007bab;
            text-align: center;
            text-shadow: 5px 0px 3px #ffffff , 0px 5px 3px #ffffff, -5px 5px 3px #ffffff, -5px -5px 3px #ffffff, 0px -5px 3px #ffffff, 5px 5px 5px #ffffff;
            margin: 65px 85px;
        }

        .header p span {
            font-size: 48px;
            display: block;
        }

		.content { 
           height: 700px;
			
		}
        .content table {
            width: 765px;
            margin-left: 35px;
        }

        .content table th {
            font-weight: bold;
            font-size: 18px;
            text-transform: uppercase;
        }

        .content table th.left {
            text-align: left;
        }

        .content table th.center {
            text-align: center;
        }

        .content table .black {
            color: #252525;
        }

        .content table .green {
            color: #71ac00;
        }

        .content table .price {
            width: 130px;
            max-width: 130px;
        }

        .content table .discount {
            width: 130px;
            max-width: 130px;
        }

        .content table .flag {
            width: 94px;
            height:50px;
            max-width: 94px;
        }

        .content table td {
            font-weight: normal;
            font-size: 16px;
            padding-top: 15px;
        }

        .content table td.name {
            text-align: left;
        }

        .content table td.price {
            text-align: right;
            padding-right: 30px;
        }

        .content table td.discount {
            font-weight: bold;
            text-align: right;
            padding-right: 30px;
        }

        .content table td.flag {
            font-weight: normal;
            background: transparent url("/images/offers/bg-flag.png") no-repeat 0 13px;
            font-size: 24px;
            color: #fefefe;
            padding-left: 35px;
        }

        .footer {
            height: 180px;
            position: absolute;
            width: 100%;
            bottom: 0;
        }

        .footer .box {
            font-weight: normal;
            background: #d50000;
            border: 5px solid #ffffff;
            color: #ffffff;
            width: 745px;
            margin: 25px auto 0;
            padding: 20px;
            box-sizing: border-box;
            text-align: center;
            font-size: 20px;
            text-transform: uppercase;
        }

        .footer .box span {
            font-weight: bold;
        }

        .footer .info {
            font-weight: normal;
            color: #007bab;
            font-size: 16px;
            text-align: center;
            margin-top: 10px;
        }
    </style>
</head>
<body>

    <div class="wrapper">

        <div class="header">
          <!--  <p>Utazások<span>75  000 - 100 000 Ft</span>között</p>-->
        </div>

        <div class="content">

          <?=$offer[pdf_description]?>
          
        </div>

        <div class="footer">

            <p class="box"><?=$offer[pdf_short_title]?></p>
            <p class="info"><?=$offer[pdf_offer_name]?></p>

        </div>

    </div>

</body>
</html>
<?
}
?>