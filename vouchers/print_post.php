<?php
header('Content-type: text/html; charset=utf-8');





require_once('lib/config.inc.php');
require_once(HTML2PS_DIR.'pipeline.factory.class.php');
parse_config_file(HTML2PS_DIR.'html2ps.config');

include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");

$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();



global $g_config;
$g_config = array(
                  'cssmedia'     => 'screen',
                  'renderimages' => true,
                  'renderforms'  => false,
                  'renderlinks'  => false,
                  'mode'         => 'html',
                  'debugbox'     => false,
                  'draw_page_border' => false
                  );

$media = Media::predefined("Postagepage");
$media->set_landscape(true);
$media->set_margins(array('left'   => 0,
                          'right'  => 0,
                          'top'    => 0,
                          'bottom' => 0));
$media->set_pixels(870);

global $g_px_scale;
$g_px_scale = mm2pt($media->width() - $media->margins['left'] - $media->margins['right']) / $media->pixels;

global $g_pt_scale;
$g_pt_scale = $g_px_scale * 1.43; 

$pipeline = PipelineFactory::create_default_pipeline("","");
$pipeline->configure($g_config);

$cid = $_GET[cid];

$hash = $_GET[hash];
if($cid == 'viapost')
{
	$pipeline->process(getProtocol() ."admin.indulhatunk.info/vouchers/post.php?cid=viapost", $media); 
}
elseif($cid == 'archive')
{
	$pipeline->process(getProtocol() ."admin.indulhatunk.info/vouchers/post.php?cid=archive&hash=$hash", $media); 
}
elseif($cid == 'normal')
{
	$pipeline->process(getProtocol() ."admin.indulhatunk.info/vouchers/post.php", $media); 
}
else
{
	head();
	
	echo "<a href='?cid=normal'>Normál feladóvevény nyomtatása</a><br/><a href='?cid=viapost'>Utánvétes feladóvevény nyomtatása</a> ";

	foot();
}

?>