<?
header('Content-type: text/html; charset=utf-8');

include("../inc/config.inc.php");
include("../inc/functions.inc.php");
include("../inc/mysql.class.php");


$mysql = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$mysql->connect();

$reseller = $_GET[reseller];

function generatePdflira($id=1,$reseller = 0) {
	global $mysql;
	
	$voucherData = $mysql->query("SELECT customers.type,customers.company_invoice,customers.payment,customers.invoice_name,customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE customers.cid = $id LIMIT 1");
	$voucherArr = mysql_fetch_assoc($voucherData);
	
	if($voucherArr[sub_pid] > 0)
	{
		$voucherData = $mysql->query("SELECT customers.type,customers.*,customers.offers_id,customers.not_include,customers.validity,customers.orig_price,customers.offer_id,partners.zip,partners.city,partners.address,partners.phone,partners.coredb_id,partners.hotel_name,customers.bid_name,partners.reception_phone,partners.reception_email FROM customers INNER JOIN partners ON partners.pid = customers.sub_pid WHERE customers.cid = $id LIMIT 1");
		$voucherArr = mysql_fetch_assoc($voucherData);
	
	}
	$offerData = $mysql->query("SELECT * FROM offers  WHERE id = $voucherArr[offers_id] LIMIT 1");
	$offerArr = mysql_fetch_assoc($offerData);
	$month = date("n");
	
	
	
	//https://admin.indulhatunk.info/vouchers/partners2/<?=$voucherArr[coredb_id].jpg
	
	if($voucherArr[sub_pid] > 0)
	{
	
		$logopic = $voucherArr[pid];
	}
	else
	{
		$logopic = $voucherArr[coredb_id];
	}
	if(!file_exists("/var/www/lighttpd/admin.indulhatunk.info/vouchers/partners2/$logopic.jpg"))
	{
		$image = 'http://admin.indulhatunk.info/vouchers/partners2/nopic.jpg';
	}
	else
		$image = "http://admin.indulhatunk.info/vouchers/partners2/$logopic.jpg";
		//die('Az utalvány nem tölthető jelenleg. Kérjük értesítsen minket a problémáról a vtl@indulhatunk.hu e-mail címen. Köszönjük!');
	
	if($offerArr[language] == 'en')
	{
		$language = array();
		$language[voucher] = '<div style="margin:0 0 0 110px">Voucher</div>';
		$language[id] = "Nr.";
		$language[price] = "PRICE";
		
		$language[name] = "en_name";
		$language[shortname] = "en_shortname";
		$language[plus_room1_name] = "en_plus_room1_name";
		$language[plus_room2_name] = "en_plus_room2_name";
		$language[plus_room3_name] = "en_plus_room3_name";
		$language[plus_other1_name] = "en_plus_other1_name";
		$language[plus_other2_name] = "en_plus_other2_name";
		$language[plus_other3_name] = "en_plus_other3_name";
		$language[plus_other4_name] = "en_plus_other4_name";
		$language[plus_other5_name] = "en_plus_other5_name";
		$language[plus_other6_name] = "en_plus_other6_name";
		$language[plus_validity] = "en_validity";
		$language[plus_not_include] = "en_not_include";

		$language[validity] = "en_validity";
		$language[not_include] = "en_not_include";
		$language[services] = "Paid extra services";
		$language[child] = "child";
		$language[booking] = "Booking";
		$language[booking_text] = "Please refer to the Nr. of the voucher, and do not forget to present voucher at the reception at the time of the check-in.";
		$language[validity] = "Validity";
		$language[not_include] = "Other extras to be paid at the hotel reception";
		
		$language[plus_weekend] = "Weekend extra price";
		$language[plus_extra_bed] = "Extra bed extra price";
		$language[night] = "night";
		$language[plus_one_night] ="+1 night stay additional price";
		$language[package] = "package";
	}
	else
	{
		$language = array();
		$language[voucher] = 'Ajándékutalvány';
		$language[id] = "SORSZÁM";
		$language[price] = "ÁRA";
		
		$language[name] = "name";
		$language[shortname] = "shortname";
		$language[plus_room1_name] = "plus_room1_name";
		$language[plus_room2_name] = "plus_room2_name";
		$language[plus_room3_name] = "plus_room3_name";
		$language[plus_other1_name] = "plus_other1_name";
		$language[plus_other2_name] = "plus_other2_name";
		$language[plus_other3_name] = "plus_other3_name";
		$language[plus_other4_name] = "plus_other4_name";
		$language[plus_other5_name] = "plus_other5_name";
		$language[plus_other6_name] = "plus_other6_name";
		$language[plus_validity] = "validity";
		$language[plus_not_include] = "not_include";
		$language[package] = "csomag";
		
		$language[validity] = "validity";
		$language[not_include] = "not_include";
		$language[services] = "Az ajándékutalvány a következő plusz szolgáltatásokra jogosítja";
		$language[child] = "gyermek";
		$language[booking] = "Foglalás";
		$language[booking_text] = "Kérjük, hivatkozzon a lapon található sorszámra, illetve, az utalványt szíveskedjék magával hozni és érkezéskor a szálloda recepcióján leadni.";
		$language[validity] = "Érvényesség";
		$language[not_include] = "Külön fizetendő";
		$language[plus_weekend] = "Hétvégi felár";
		$language[plus_extra_bed] = "Pótágy felár";
		$language[night] = "éj";
		$language[plus_one_night] ="Maradjon még egy éjszakát";
	}
	
	$monthArray = array(
		1 => 'JANUÁR',
		2 => 'FEBRUÁR',
		3 => 'MÁRCIUS',
		4 => 'ÁPRILIS',
		5 => 'MÁJUS',
		6 => 'JÚNIUS',
		7 => 'JÚLIUS',
		8 => 'AUGUSZTUS',
		9 => 'SZEPTEMBER',
		10 => 'OKTÓBER',
		11 => 'NOVEMBER',
		12 => 'DECEMBER',
	);
	$year = date("Y");
	$day = date("d");
	$validity = str_replace("|","",$offerArr[$language[plus_validity]]);
	$not_include = str_replace("|","",$offerArr[$language[plus_not_include]]);;

	
if($reseller == 1)
{
	$file = 'empty';
}
else
{
	if($voucherArr[type] == 1 || $voucherArr[type] == 2 || $voucherArr[type] == 6 || $voucherArr[type] == 7)
		$file = '4';
	else
		$file = '5';
}

$file = 6;
$affiliate = 1;
$reseller = 1;


?>
<div id="container" style="background:url('http://admin.indulhatunk.info/vouchers/<?=$file?>.jpg') no-repeat;">
	<div id="header">
		<div id="voucher" style='height:130px;'><? if($affiliate <> 1) echo $language[voucher];?></div>
		<div id="descr">
			<div id="date"><?=$year;?>. <?=$monthArray[$month];?> <?=$day;?>.</div>
			<div id="serial"></div>
			<div id="price"><?=$language[id]?>: N&deg;<?=$voucherArr[offer_id];?></div>		
			<div class="cleaner"></div>
		</div>
	</div>
	<div id="title">
		<?=str_replace("|","",$voucherArr[hotel_name]);?>	
		<div id="voucherImageAddress"><?=$voucherArr[zip];?> <?=$voucherArr[city];?> <?=$voucherArr[address];?> tel.: <?=$voucherArr[reception_phone];?>/<?=$voucherArr[reception_email];?> </div>
	
	</div>
	<div id="subTitle">
		<div class="bid_title"><?=$offerArr[$language[shortname]]?>	</div>
		<?=$offerArr[$language[name]]?>	
	</div>
	<div id="voucherContent">
	<div id="info">
	
	<div style='padding:0 90px 0 90px;font-size:11px;'>
	<div style='height:165px;border-bottom:1px solid #0f0f0f;'>
		<?=$offerArr[main_description]?>
		
		<ul>
			<?if($voucherArr[plus_child1_value]<>0) {?>
<li>1. <?=$language[child]?>
	<?
	if($voucherArr[plus_child1_value]==1 || ($voucherArr[plus_child1_value] >1 && $voucherArr[plus_child1_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig:";
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között";
	elseif($voucherArr[plus_child1_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett";
	?>
</li>
<?}?>

<?if($voucherArr[plus_child2_value]<>0) {?>
<li>2. <?=$language[child]?>
	<?
	if($voucherArr[plus_child2_value]==1 || ($voucherArr[plus_child2_value] >1 && $voucherArr[plus_child2_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig ";
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között";
	elseif($voucherArr[plus_child2_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett";
	?>
</li>
<?}?>

<?if($voucherArr[plus_child3_value]<>0) {?>
<li>3. <?=$language[child]?>
	<?
	if($voucherArr[plus_child3_value]==1  || ($voucherArr[plus_child3_value] >1 && $voucherArr[plus_child3_value]==$offerArr[plus_child1_value]))
		echo	"0-$offerArr[plus_child1_name] éves korig";
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child2_value])
		echo "$offerArr[plus_child1_name]-$offerArr[plus_child2_name] kor között";
	elseif($voucherArr[plus_child3_value]==$offerArr[plus_child3_value])
		echo "$offerArr[plus_child2_name] éves kor felett";
	?>	
</li>
<?}?>
		</ul>
	</div>
	<div style='padding:10px 0 0 0;'>
		<b><?=$language[booking]?>:</b> <?=$language[booking_text]?>

	<div style='padding:5px 0 5px 0;'>
		<b><?=$language[validity]?>:</b> <?=$validity?>
	</div>
	
	<div style='padding:5px 0 0 0'>
		<b><?=$language[not_include]?>:</b><?=$not_include?>
	</div>
	</div>
	</div>

	
		<div class="cleaner"></div>
	</div>
	
	<div id="voucherImage"><img src="<?=$image?>"/></div>
	
		<div id="footer">
		AZ UTALVÁNY KÉSZPÉNZRE NEM VÁLTHATÓ
	<div class='notice'>
		A Hotel Outlet Kft. a jelen voucheren feltüntetett szálloda bizományosi értékesítője, <br/>így kizárja minden felelősségét a szállodai szolgáltatás bármilyen okból történő elmaradása esetén.<br/>
		A voucher megvásárlója tudomásul veszi, hogy visszafizetési, vagy kártérítési igényét kizárólag <br/>és közvetlenül a voucheren feltüntetett szállodával szemben érvényesítheti. <br/>
		A bizományos felelőssége kizárólag a voucher ellenértékének a szálloda részére történő befizetésére terjed ki. <br/>
		A szállodai szolgáltatási jogviszony a voucheren feltüntetett szálloda és a voucher tulajdonosa között jön létre.
	</div>
	</div>
	</div>
	
	<? if($affiliate <> 1 ) { ?>
	<div id="outlet">
	<? if($reseller == 0 ) { ?>
		<? 	if($voucherArr[type] == 1 || $voucherArr[type] == 2 || $voucherArr[type] == 6 || $voucherArr[type] == 7) { ?>	
			www.lealkudtuk.hu
		<? } else { ?>
			www.szállásoutlet.hu
		<? } ?>
	<? } ?>
	</div>
	
	
		<div id="copyright">printed by indulhatunk.info &copy;<?=date("Y")?> - All rights reserved. (#<?=$voucherArr[cid]?>)</div>
	<? } ?>
</div>

<?
}


?>
<style>
	#voucher { font-family: bauh; font-size:60px; color:#0f0f0f; margin:120px 0 0 130px; height:60px;}
	#outlet { font-family:bauh; color:#d9cfc3; font-size:72px; text-align:center; height:110px;}
	body { margin:0; padding:0; font-size:12px; font-family:Arial; }
	#container { margin:30px 0 0 35px;width:800px;height:1176px; }
	.cleaner { clear:both; }
	#descr { margin:0 0 0 110px; }
	.bigPrice { font-size:18px; }
	#date {  padding:20px 0 0 0; float:left; width:160px; text-transform: uppercase; }
	#header { padding:73px 0 0 70px; color:#0f0f0f; font-weight:bold; }
	#serial { padding:20px 0 0 0; float:left; width:150px; text-transform: uppercase; }
	#price {  padding:14px 0 0 0; float:left;}
	#title {  height:72px;font-size:38px; color:#8a3350; text-align:center; padding:23px 0 0 0; font-family:arial;}
	#subTitle {  font-size:24px; text-align:center; height:55px; padding:0 45px 0 45px;color:#8a3350;   }
	.bid_title { color:#0f0f0f; font-size:27px;}
	#voucherImage { padding:0 0 0 130px; height:182px; }
	#voucherImageTitle { padding:8px 0 0 0; text-align:center; color:#0f0f0f; font-weight:bold; }
	#voucherImageAddress { padding:5px 0 0 0; text-align:center; color:#0f0f0f; font-weight:normal; font-size:14px; }
	#room {  font-weight:normal; color:#0f0f0f; }
	#roomTitle { padding:30px 0 0 0; text-align:center; color:#0f0f0f; font-size:14px; font-weight:bold;  }
	#validity {  padding:0 0 10px 0;; color:#0f0f0f; }
	#notIncludeTitle { font-weight:bold; color:#0f0f0f; }
	#notInclude {  color:#0f0f0f;}
	#footer { padding:10px 0 0 10px; color:#0f0f0f; font-size:9px; text-align:center;font-weight:bold; text-transform:uppercase;}
	#voucherContent { height:585px; }
	#copyright { text-align:center; color:#0f0f0f; padding:30px 0 0 0; }
	#subT { font-weight:bold; color:#0f0f0f; margin:-12px 0 0 0;  }
	#st { font-weight:bold; color:#0f0f0f; padding:10px 0 0 0;  }
	#info {  padding:25px 0 0 0; color:#0f0f0f; font-size:12px; height:263px;}
	
	#info ul { padding:0 0 0 20px; margin:0; }
	.bold { font-weight:bold; color:green;}
	.strike { text-decoration: line-through; }
	.plusService { margin:0; padding:10px 0 0 10px; list-style:none; list-style-type:circle; font-size:11px; }
	.notice { padding:10px 10px 10px 10px; width:550px; margin:0 auto;  font-size:8px;  font-weight:normal; }
</style>
<body>

<?

$username = $_GET[username];
$cid = $_GET[cid];
$type = $_GET[type];
$hash = trim($_GET[hash]);


$multiple = explode(",",$cid);

if($multiple[0] == 'multiple' && $multiple[1] > 0)
{
	$multipleprint = 1;
	$cid = 1;

}

if($cid > 0 && $cid <> 'pult') 
{
	
		if($multipleprint == 1)
		{
			
			for($i=1;$i<count($multiple);$i++)
			{	
			
				$cid = $multiple[$i];
				generatePdfV2($cid,$reseller);
				
				if($i <> (count($multiple)-1))
					echo "<pagebreak/>";
				
				$voucherid = mysql_fetch_assoc($mysql->query("SELECT offer_id FROM customers WHERE cid = $cid"));
				writelog("$username $voucherid[offer_id] printed ($hash)");
				$data[voucher] = 1;
				$data[print_id] = $hash;
				if($_GET[paid] == 1)
				{
					$data[print_date] = 'NOW()';
				}
				$mysql->query_update("customers",$data,"cid=$cid");
				
			}
		
		}
		else
		{
			
			generatePdflira($cid,$reseller);
			
			$voucherid = mysql_fetch_assoc($mysql->query("SELECT offer_id FROM customers WHERE cid = $cid"));
			writelog("$username $voucherid[offer_id] printed ($hash)");
			$data[voucher] = 1;
			$data[print_id] = $hash;
			if($_GET[paid] == 1)
			{
				$data[print_date] = 'NOW()';
			}
			$mysql->query_update("customers",$data,"cid=$cid");

		}
}
else
{
	//echo "$cid";
	if($cid == "pult")
	{
		//echo "pult";
		$qr = $mysql->query("SELECT * FROM customers WHERE  type = 5 AND voucher = 0 ORDER BY cid DESC LIMIT 10");
	}
	elseif($type == 'viapost')
	{
		$qr = $mysql->query("SELECT * FROM customers WHERE  paid = 0 AND voucher = 0 AND payment = 3 ORDER BY cid DESC LIMIT 10");
	}
	elseif($type == 'archive')
	{
		$qr = $mysql->query("SELECT * FROM customers WHERE print_id = '$hash' ORDER BY cid DESC");
	}
	else
	{
		//echo "pult2";
		$qr = $mysql->query("SELECT * FROM customers WHERE  paid = 1 AND voucher = 0 ORDER BY cid DESC LIMIT 10");
	}
	while($arr = mysql_fetch_assoc($qr))
	{
		generatePdfV2($arr[cid],$reseller);
		//echo "<div class='cleaner'></div>";
		echo "<pagebreak/>";
		$data[voucher] = 1;
		$data[print_date] = 'NOW()';
		$data[print_id] = $hash;
	
		if($type <> 'archive')
			$mysql->query_update("customers",$data,"cid=$arr[cid]");

		writelog("$username $arr[cid] voucher printed ($hash) / $type");
	}
	
	//if($type <> 'archive')
		echo generatePdfStats($hash);
}
?>
</body>