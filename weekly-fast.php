<?
/*
 * weekly.php 
 *
 * the weekly stats page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");

//check if user is logged in or not
userlogin();

if($CURUSER["userclass"] < 100) {
	header("location: customers.php");
}


$company_name = $_GET[company];
if($company_name == 'indulhatunk')
{
	$company_name = 'indulhatunk';
	$logo = 'ilogo_small.png';
}
elseif($company_name == 'szallasoutlet')
{
	$company_name = 'szallasoutlet';
	$logo = 'szo_logo.png';
}
else
{
	$company_name = 'hoteloutlet';
	$logo = 'ologo_small.png';
}	
//last_week & year
if($_GET[week] == '' || $_GET[year] == '')
{
	$week = date("W");
	$year = date("Y");
	
	$weekselect = $week;
	$yearselect = $year;
	
	$invoiceweek = $week-1;
	$invoiceyear = $year;
}
else
{
	$week = $_GET[week];
	$year = $_GET[year];
	
	$weekselect = $week+1;
	if($weekselect == 53)
	{
		$yearselect = $year;
		$weekselect = 1;
	}
	else
	{
		$yearselect = $year;
	}
	
	$invoiceweek = $week;
	
	if($invoiceweek == 53)
	{
		$invoiceweek = 1;
		$invoiceyear = $year+1;
	}
	else
	{
		$invoiceyear = $year;
	}
		
}
//tax changes

if($week == 52 && $year == 2011)
	$tax = 1.27;
elseif($invoiceyear >= 2012)
	$tax = 1.27;
else
	$tax = 1.25;
	


$content.= "<div style='float:left'><form method='get'>";
	$content.= "<select name='year'>";
	
	
	if($year == 2011)
		$selected2 = 'selected';
	
	if($year == 2012)
		$selected3 = 'selected';
	
	if($year == 2013)
		$selected4 = 'selected';
		
	if($year == 2014)
		$selected5 = 'selected';
		
		$content.= "<option value='2011' $selected2>2011</option>";
		$content.= "<option value='2012' $selected3>2012</option>";
		$content.= "<option value='2013' $selected4>2013</option>";
		$content.= "<option value='2014' $selected5>2014</option>";

	$content.= "</select>";
	
	if($year == '')
		$year = date("Y");
	$content.= "<select name='week'>";
		for($i=1;$i<=53;$i++)
		{
			if($invoiceweek == $i)
				$weekselected = 'selected';
			else
				$weekselected = '';
				
			$content.= "<option value='$i' $weekselected>$i</option>";
		}
	$content.= "</select>";
	
	if($_GET[company] == 'hoteloutlet')
		$trlink = "<a href='weekly-transfer.php'><b>$invoiceweek. heti átutalási megbízás letöltése</b></a>";
	elseif($_GET[company] == 'szallasoutlet')
		$trlink = "<a href='weekly-transfer.php?company=szallasoutlet'><b>$invoiceweek. heti átutalási megbízás letöltése</b></a>";
	else
		$trlink = "<a href='?showtransfer=1&year=$year&week=$week&company=$_GET[company]'><b>$invoiceweek. heti átutalási megbízás letöltése</b></a>";



	$content.= "<input type='hidden' name='company' value='$_GET[company]'/><input type='submit' value='mutasd'/>";
$content.= "</form></div><div style='float:left;padding:5px 0 0 10px;'> $trlink | <a href='?showletter=1&mail=1'><b>Leveleket elküld</b></a>  </div><div class='cleaner'></div><hr/>";

//$content.= "SELECT * FROM customers WHERE WEEK(invoice_date,3) = '$weekselect' AND YEAR(invoice_date) = '$yearselect' AND company_invoice = '$company_name' GROUP BY invoice_number ORDER BY invoice_number ASC";


$query = $mysql->query("SELECT * FROM customers WHERE WEEK(invoice_date,3) = '$weekselect' AND YEAR(invoice_date) = '$yearselect' AND company_invoice = '$company_name' AND invoice_number <> '' GROUP BY invoice_number ORDER BY invoice_number ASC");

$header = "<table border='1'>";
	$header.= "<tr class='header'>";
	$header.= "<td align='center' colspan='2'>-</td>";
	$header.= "<td colspan='2'>Cég neve</td>";
	$header.= "<td align='right'>DB</td>";
	$header.= "<td align='right'>Összesen</td>";
	$header.= "<td align='right'>Helyszínen</td>";
	$header.= "<td align='right'>ÜCS</td>";
	$header.= "<td align='right'>Jutalék</td>";
	$header.= "<td align='right' class='green'>Utalandó</td>";
	$header.= "<td align='right'>Számlaszám</td>";
	$header.= "<td align='right'>Elszámolás</td>";
	$header.= "</tr>";
	
	$content.= $header;
	
	$i = 1;
	while($arr = mysql_fetch_assoc($query))
	{
		$company = mysql_fetch_assoc($mysql->query("SELECT coredb_id,account_no,address,company_name, yield_vtl, hotel_name,pid FROM partners WHERE pid = '$arr[pid]'"));
		
		$income = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE invoice_number = '$arr[invoice_number]'"));
		
		$place = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE invoice_number = '$arr[invoice_number]' AND payment = 6"));

		$check = mysql_fetch_assoc($mysql->query("SELECT sum(check_value) as total FROM customers WHERE invoice_number = '$arr[invoice_number]' AND payment = 5"));

		$count = mysql_fetch_assoc($mysql->query("SELECT count(cid) as count FROM customers WHERE invoice_number = '$arr[invoice_number]'"));

		$income = $income[total];		
		$place = $place[total];
		$check = $check[total];
		$yield = $income*($company[yield_vtl]/100)*$tax;
		$transfer = $income - $place - $yield;
		
		if($transfer <= 0)
		{
			$transfer = 0;
			$class = 'red';
		}	
		else
		{
			$class = ''; 
			
			if($company[pid] <> 3003 && $company[pid] <> 2989)
			{
					$transaction_item[value] = $transfer;
					$transaction_item[account_no] = $company[account_no];
					$transaction_item[partner_id] = $company[coredb_id];
					$transaction_item[partner_name] = removeSpecialChars(trim($company[company_name]));
					$transaction_item[partner_address] = removeSpecialChars($company[address]);
					$transaction_item[comment] = "heti elszamolas / $arr[invoice_number]";
					$tritems[] = $transaction_item;
			}
			
			
		}
		$count = $count[count];
		
		$totalincome = $totalincome + $income;
		$totalplace = $totalplace + $place;
		$totalyield = $totalyield + $yield;
		$totalcheck = $totalcheck + $check;
		$totaltransfer = $totaltransfer + $transfer;
		$totalcount = $totalcount + $count; 
		
		$row = "<tr class='$class'>";
		
		$row.= "<td align='center'>$i.</td>";
		$row.= "<td><img src='/images/$logo'/></td>";

		$row.= "<td>$company[company_name] / $arr[pid]</td>";
		$row.= "<td>$company[hotel_name]</td>";
		$row.= "<td align='center'>$count</td>";
		$row.= "<td align='right'>".formatPrice($income,0,1)."</td>";
		$row.= "<td align='right'>".formatPrice($place,0,1)."</td>";
		$row.= "<td align='right'>".formatPrice($check,0,1)."</td>";
		$row.= "<td align='right'>".formatPrice($yield,0,1)."</td>";
		$row.= "<td align='right' class='green'>".formatPrice($transfer,0,1)."</td>";
		$row.= "<td align='right'><a href='/invoices/vatera/".str_replace("/","_",$arr[invoice_number]).".pdf' target='_blank'>$arr[invoice_number]</a> ($tax)</td>";
		$row.= "<td align='right'><a href='/info/show_accounting.php?invoice_number=$arr[invoice_number]&pid=$arr[pid]'rel='facebox'>Elszámolás</a></td>";

		$row.= "</tr>";
		

	
	
	
		$content.= "$row";
		
		if($_GET[mail] == 1)
		{
		
		//$wk = $week-1;
			$wk = $week;
			$body =  "
	<b>Kedves Partnerünk!</b><br/><br/>

<b>FONTOS! VÁLTOZÁS a Hotel Outlet Kft elszámolásaival és az utalásokkal kapcsolatban</b><br/><br/>

<b>Elszámolások és számlázás, utalványszámok:</b><br/><br/>

<b>Utalványszámok:</b><br/><br/>

Az indulhatunk.hu kft-ből átszervezzük az összes jutalékos rendszerű együttműködéseinket a Hotel Outlet Kft-ba, melyekre már megkötöttük Önökkel is a szerződéseket.<br/>
Sajnos az átállás miatt közösen kell egy picit jobban odafigyelnünk, hiszen lesznek olyan tételek, melyeket még az indulhatunk.hu kft felé várunk és lesznek már olyanok is, amiket már nem.<br/><br/>

Segítségül a régi sorszámokat 'VTL' megváltoztattuk 'SZO'-ra, így jobban lehet majd a végszámláknál látni, hogy a 'VTL' kezdetűeket az indulhatunk.hu kft felé kell számlázni, míg az 'SZO' kezdetűeket már a Hotel Outlet Kft felé kell majd számlázni.<br/><br/>

<b>Utalások:</b><br/>
Ezután is minden héten csütörtökön van az utalás, azzal a változtatással, hogy csak a részünkre hibátlanul elküldött számlák alapján fogunk teljesíteni.<br/>
A számláknak nem kell postán az utalás napjáig megérkezni, de faxon, vagy e-mailen keresztül meg kell kapnunk.<br/><br/>

Kérjük, hogy a számlákat a Hotel Outlet Kft nevére állítsák majd ki.<br/><br/>


<hr/>
				
A $partnerArr[invoice_number] számú jutalékszámlát az alábbi linkre kattintva töltheti le: <a href='https://admin.indulhatunk.hu/invoices/dl/".md5($arr[invoice_number]."#FWf#CEFA#SDF343sf#")."'><b>$arr[invoice_number] sz. számla letöltése</b></a><br/><br/>

A $wk. heti elszámolást megtekintheti az adminisztrációs felületünk '<b>Számlák, elszámolások / Szállás Outlet</b>' menüpontjában. (<a href='http://admin.indulhatunk.hu'>http://admin.indulhatunk.hu</a>)
				
<br/><br/>
Kérjük, hogy mihamarabb készítsék el számlájukat, melyet postán, faxon vagy emailban várunk az <a href='mailto:attila.forro@indulhatunk.hu'>attila.forro@indulhatunk.hu</a> címre.<br/>
A gyorsabb ügyintézés érdekében az a levél tárgyában hivatkozzon az alábbi számlaszámra: $arr[invoice_number].
<br/><br/>
Üdvözlettel:<br/>
Forró Tamás<br/>
<a href='http://indulhatunk.hu'>Hotel Outlet Kft</a>
			";
			
			echo message("$wk. heti elszámolás a(z) $company[hotel_name] részére");
		sendEmail("$wk. heti elszámolás a(z) $company[hotel_name] részére",$body,"it@indulhatunk.hu",$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		//sendEmail("$week. heti elszámolás a(z) $company[hotel_name] részére",$body,"billing@indulhatunk.hu",$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		sendEmail("$wk. heti elszámolás a(z) $company[hotel_name] részére",$body,$company[email],$name='Partnerünk',$fromemail = 'info@indulhatunk.hu', $fromname = 'Forró Tamás' );
		
			//$content.= " $company[hotel_name]  / $body<hr/>";
		}
		
		$i++;
	}
	
	



	
		$tc = $totalcheck*0.073; 
		$footer = "<tr class='header'>";
		$footer.= "<td colspan='4'>Összesen</td>";
		$footer.= "<td align='right' align='center'>$totalcount</td>";
		$footer.= "<td align='right'>".formatPrice($totalincome,0,1)."</td>";
		$footer.= "<td align='right'>".formatPrice($totalplace,0,1)."</td>";
		$footer.= "<td align='right'>".formatPrice($totalcheck,0,1)." (".formatPrice($tc,0,1).")</td>";
		$footer.= "<td align='right'>".formatPrice($totalyield-$tc,0,1)."</td>";
		$footer.= "<td align='right' class='grey'>".formatPrice($totaltransfer,0,1)."</td>";
		$footer.= "<td colspan='2' align='center'>-</td>";
		$footer.= "</tr>";
		
		$content.= "$footer";

	
		//add our own yield
	$transaction_item[value] = $totalyield-$tc;
	$transaction_item[account_no] = "11600006-00000000-38168594";
	$transaction_item[partner_id] = 1;
	$transaction_item[partner_name] = "Indulhatunk.hu Kft.";
	$transaction_item[partner_address] = "Váci utca 9.";
	$transaction_item[comment] = "$week heti elszamolas";
	$tritems[] = $transaction_item;
	
	

	if($_GET[showtransfer] == 1)
	{
  	  $data = generateTransferFile('indulhatunk',$tritems)."";
  	 
		header("Content-type: text/plain");
		header("Content-Disposition: attachment;filename=atutal-$week.121");
		header('Pragma: no-cache');
		header('Expires: 0');
		echo $data;
		//die;
	}
	else
	{
		head("$invoiceweek. heti Hotel Outlet elszámolás");

	echo "<h1>".$week.". heti elszámolás</h1>";
?>

<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					
		<li><a href="?company=indulhatunk" class="<? if($_GET[company] == 'indulhatunk') echo "current";?>">Indulhatunk.hu Kft.</a></li>
		<li><a href="?company=hoteloutlet"  class="<? if($_GET[company] == 'hoteloutlet' || $_GET[company] == '') echo  "current";?>">Hotel Outlet Kft.</a></li>
				<li><a href="?company=szallasoutlet"  class="<? if($_GET[company] == 'szallasoutlet') echo  "current";?>">SzállásOutlet Kft.</a></li>

		<li><a href="weekly-transfer.php">Hotel Outlet Kft. utalások</a></li>
		<li><a href="weekly-transfer.php?company=szallasoutlet">SzállásOutlet Kft. utalások</a></li>


		<div class="clear"></div>
</div>
<div class='contentpadding'>

	<?	echo $content;
		echo "</table></div></div>";	
		foot();
	}
?>