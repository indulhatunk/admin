<?
/*
 * weekly.php 
 *
 * the weekly stats page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
include("invoice/invoice.php");

//check if user is logged in or not
userlogin();


$lockfile  = "imap/company2-billing.lock";
$tax = 1.27;

if (file_exists($lockfile))
{
    die("ERROR - lock file exists");
}

/****
SELECT cid
FROM customers
INNER JOIN offers ON offers.id = customers.offers_id
WHERE (
offers.is_qr =1
)
AND customers.is_qr =0*/


if($_GET[update] == 1)
{
	$qr = $mysql->query("SELECT cid FROM customers INNER JOIN offers ON offers.id = customers.offers_id WHERE (offers.is_qr =1)");
	
	while($arr = mysql_fetch_assoc($qr))
	{
		$mysql->query("UPDATE customers SET is_qr = 1 WHERE cid = $arr[cid]");
	}
	
	echo "done";
	die;
}

$file = fopen($lockfile, 'w') or die("can't open file");
fclose($file);


if($_GET[qr] == 1)
{
	$extra = "AND is_qr = 1";
	$pselect = "is_qr = 1 ";
	$yieldp = 'yield_qr';
}
else
{
	$pselect = "post_balance = 1 ";
	$yieldp = 'yield_vtl';
}

if($CURUSER["userclass"] <> 255) {
	header("location: customers.php");
}



	$company_name = $_GET[company];
	
	//if($company_name <> 'hoteloutlet')
	//	$company_name = $_GET[company_name];
		
	$logo = 'ologo_small.png';

head("Heti Hotel Outlet elszámolás számlázatlan tételek");



$pquery = $mysql->query("SELECT * FROM partners WHERE $pselect");



echo "<table>";

echo "<tr class='header'>";
		echo "<td>-</td>";
		echo "<td colspan='3'>Cég neve</td>";
		echo "<td align='right'>Heti bevétel összesen</td>";
		echo "<td align='right'>Bevétel eltolt fizetendő</td>";
		echo "<td align='right'>Bevétel eltolt hátralevő</td>";
		echo "<td align='right'>Bevétel eltolt összes</td>";
		echo "<td align='right'>Bevétel SZÉP  hátralevő</td>";

		echo "<td align='right'>Nem eltolt fizetett összes</td>";

		echo "<td align='right'>Bevétel ÜCS</td>";
		echo "<td align='right'>Bevétel helyszínen</td>";
		echo "<td align='right'>Utalandó</td>";
		echo "<td align='right'>Számlázandó jut.</td>";
		echo "</tr>";
		
		
while($list = mysql_fetch_assoc($pquery))
{

echo "SELECT * FROM customers WHERE paid = 1 AND invoice_number = ''  AND company_invoice = '$company_name' AND pid = $list[pid] $extra GROUP BY pid ORDER BY pid ASC";

$query = "SELECT * FROM customers WHERE paid = 1 AND invoice_number = ''  AND company_invoice = '$company_name' AND pid = $list[pid] $extra GROUP BY pid ORDER BY pid ASC"; //AND week(paid_date,3) = $week AND

$query = $mysql->query($query);

$i=1;	
while($arr = mysql_fetch_assoc($query))
{
	
	$company = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$arr[pid]'"));
	
	
	
	$income = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00'  AND  pid = '$arr[pid]' AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' $extra"));
	
	
	$szep = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND pid = '$arr[pid]' AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00' AND facebook = 0 AND company_invoice = '$company_name' $extra"));


	$szepcurrent = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND pid = '$arr[pid]' AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'   AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00' AND WEEK(paid_date,3) < 13 AND YEAR(paid_date) = 2013 $extra"));

	$place = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND pid = '$arr[pid]' AND payment = 6 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00' $extra"));


	//standard
	//place
	//ucs
	//postponed
	
	$invoice_total = $income[total]+$postponed_items[total]+$check[total]+$szepcurrent[total];
		$yield_total = $invoice_total*($company[$yieldp]/100)*$tax;
	
	$transfer = $invoice_total-$yield_total-$place[total];
	$totaltransfer = $totaltransfer + $transfer;
	
	$totalback = $totalback + $postponed[total];
	$totalbackleft = $totalbackleft + $postponed_items_left[total];
	$totalyield = $totalyield + $yield_total;
	
	$sztotal = $sztotal+$szep[total];
	
	if($company[without_vat] == 1)
		$cls = 'orange';
	else
		$cls = '';
		
	echo "<tr class='$cls'>";
		echo "<td align='center'>$i.</td>";
		echo "<td>$company[company_name] ($company[pid])</td>";
		echo "<td>$company[hotel_name]</td>";
		echo "<td>$company[tax_no]</td>";
		echo "<td align='right'>".formatPrice($income[total],0,1)."</td>";
		
		echo "<td align='right' class='grey'>".formatPrice($szep[total],0,1)."</td>";
		echo "<td align='right'>".formatPrice($place[total],0,1)."</td>";
		echo "<td align='right'>".formatPrice($transfer,0,1)."</td>";
		echo "<td align='right' class='green'>".formatPrice($yield_total,0,1)."</td>";
		echo "<td align='right' class='green'>$tax / ".formatPrice($invoice_total*($company[$yieldp]/100),0,1)."</td>";
	echo "</tr>";
	
	//echo "<tr ><td colspan='10'>SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1 AND postpone_date < '2013-04-07 00:00:00' AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'</td></tr>";
	
	/*** generate data for the invoice **/
	$partner = array();
	$partner[adoszam] = $company[tax_no];
	$partner[email] = $company[email];
	$partner[megjegy] = $company[account_no];
	
	if($company[invoice_name] <> '')
		$partner[nev] = "$company[invoice_name]|$company[hotel_name]";
	else
		$partner[nev] = "$company[company_name]|$company[hotel_name]";

	if($company[invoice_zip] <> '')
	{
		$partner[irsz] = $company[invoice_zip]; 
		$partner[varos] = $company[invoice_city];
		$partner[utca] = $company[invoice_address];
	}
	else
	{
		$partner[irsz] = $company[zip]; 
		$partner[varos] = $company[city];
		$partner[utca] = $company[address];
	}
	
	$items = array();
	
	
	//weekly not postponed items
	$income = $mysql->query("SELECT offer_id, name, phone, orig_price,customer_left FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00' AND  pid = '$arr[pid]' AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' $extra ORDER BY name ASC");
	while($singleitem = mysql_fetch_assoc($income))
	{
		
		//echo "$singleitem[offer_id] > $singleitem[name] > $singleitem[phone] > $singleitem[orig_price] > $singleitem[customer_left]<br/>";
		$item[megnev] = "$singleitem[offer_id] / $singleitem[name] - ".formatPrice($singleitem[orig_price])." után járó jutalék";
		$item[netto_egysegar] = $singleitem[orig_price]*($company[$yieldp]/100);
		$items[] = $item;
	}
	
	// AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12
	
/*
	
	//SZEP query
	$income = $mysql->query("SELECT offer_id, name, orig_price FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND pid = '$arr[pid]' AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00' AND WEEK(paid_date,3) < 13 AND YEAR(paid_date) = 2013  ORDER BY name ASC");
	while($singleitem = mysql_fetch_assoc($income))
	{
		$item[megnev] = "$singleitem[offer_id] / $singleitem[name] - ".formatPrice($singleitem[orig_price])." után járó jutalék";
		$item[netto_egysegar] = $singleitem[orig_price]*($company[$yieldp]/100);
		$items[] = $item;
	}
	*/

	//$place = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 0 AND WEEK(paid_date,3) = $week AND YEAR(paid_date) = $year AND pid = '$arr[pid]' AND payment = 6 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'"));

	//if there is something to create invoice of
	if($yield_total > 0 && $company[tax_no] <> '' && $company[without_vat] == 0)
	{

	
	echo "<hr/>$company[company_name] $company[email]<hr/>";
 
/*
	$invoice_number = create_invoice($partner,$items,0,$company_name);
	
	//update created invoices
	echo "<hr/>$invoice_number<hr/>";
	
	if($invoice_number <> '') 
	{
	
	
	//$income = $mysql->query("SELECT offer_id, name, orig_price FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00' AND  pid = '$arr[pid]' AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' $extra ORDER BY name ASC");
	
	
	$mysql->query("UPDATE customers SET invoice_created = 1,invoice_date = NOW(), invoice_number = '$invoice_number'  WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND customer_left < NOW() AND customer_left <> '0000-00-00 00:00:00'  AND pid = '$arr[pid]' AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' $extra");


	}
echo "<hr/><hr/>";

	/*** generate data for the invoice **/
	
	//die;
	}
	$i++;
	
	}

}
	echo "<tr class='header'>";
		echo "<td colspan='3'>Összesen</td>";
		echo "<td align='right'>".formatPrice('',0,1)."</td>";
		echo "<td align='right' class='grey'>".formatPrice('',0,1)."</td>";
		echo "<td align='right'>".formatPrice($totalbackleft,0,1)."</td>";
		echo "<td align='right'>".formatPrice($totalback,0,1)."</td>";
		echo "<td align='right'>".formatPrice('',0,1)."</td>";
		echo "<td align='right'> !!! ".formatPrice($sztotal,0,1)."</td>";
		echo "<td align='right'>".formatPrice('',0,1)."</td>";
		echo "<td align='right'>".formatPrice($totaltransfer,0,1)."</td>";
		echo "<td align='right'>".formatPrice($totalyield,0,1)."</td>";
	echo "</tr>";
	
	
	
	
echo "</table>";

	foot();
	
	unlink($lockfile);
?>