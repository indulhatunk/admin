<?
/*
 * weekly.php 
 *
 * the weekly stats page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");
include("invoice/invoice.php");

//check if user is logged in or not
userlogin();


$lockfile  = "imap/company-billing.lock";


if (file_exists($lockfile))
{
    die("ERROR - lock file exists");
}


$file = fopen($lockfile, 'w') or die("can't open file");
fclose($file);




if($CURUSER["userclass"] <> 255) {
	header("location: customers.php");
}


$company_name = $_GET[company];
if($company_name == 'indulhatunk')
{
	$company_name = 'indulhatunk';
	$logo = 'ilogo_small.png';
}
elseif($company_name == 'szallasoutlet')
{
	$company_name = 'szallasoutlet';
	$logo = 'ilogo_small.png';
}
elseif($company_name == 'professional')
{
	$company_name = 'professional';
	$logo = 'ilogo_small.png';
}
elseif($company_name == 'indusz')
{
	$company_name = 'indusz';
	$logo = 'ilogo_small.png';
}
else
{
	$company_name = 'hoteloutlet';
	$logo = 'ologo_small.png';
}	

$week = $_GET[week];
$year = $_GET[year];

$curweek = $week - 2;
$week = $week - 3; 

/*
$curweek = $week;
$week = $week - 1;
*/

$weekminus = $week - 2;

if($week == 52 && $year == 2011)
	$tax = 1.27;
elseif($year >= 2012)
	$tax = 1.27;
else
	$tax = 1.25;
		
if($week == 0)
{
	$week = 52;
	$year = $year - 1;
}
head("$week. heti $company_name elszámolás számlázatlan tételek");


$szepweekorig = 37;
$pp_dateorig = 38;

$szepweekorig3 = 39;
$pp_dateorig3 = 40;


$query = "SELECT * FROM customers WHERE paid = 1 AND invoice_number = ''  AND pid <> 3003 AND pid <> 3121 and pid <> 3452 AND company_invoice = '$company_name' AND inactive = 0 ANd pid <> 3589 ANd pid <> 3542 GROUP BY pid ORDER BY pid ASC"; //AND week(paid_date,3) = $week AND


$query = $mysql->query($query);

echo "<table>";

echo "<tr class='header'>";
		echo "<td>-</td>";
		echo "<td colspan='3'>Cég neve</td>";
		echo "<td align='right'>Heti bevétel összesen</td>";
		echo "<td align='right'>Bevétel eltolt fizetendő</td>";
		echo "<td align='right'>Bevétel eltolt hátralevő</td>";
		echo "<td align='right'>Bevétel eltolt összes</td>";
		echo "<td align='right'>Bevétel SZÉP  hátralevő</td>";

		//echo "<td align='right'>Nem eltolt fizetett összes</td>";

		//echo "<td align='right'>Bevétel ÜCS</td>";
		echo "<td align='right'>Bevétel helyszínen</td>";
		echo "<td align='right'>Utalandó</td>";
		echo "<td align='right'>Számlázandó jut.</td>";
		echo "</tr>";
$i=1;	
while($arr = mysql_fetch_assoc($query))
{
	$company = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = '$arr[pid]'"));
	
	if($company[post_balance] == 0 && $company[has_foreign] == 0)
	{
	
		if($company[postpone_category] == 3)
		{
			
			$szepweek = $szepweekorig3;
			$pp_date = $pp_dateorig3;
		}
		else
		{
			$szepweek = $szepweekorig;
			$pp_date = $pp_dateorig;
			
		}
		
		if($szepweek > $week)
			$szepyear = date("Y")-1;
		else
			$szepyear = date("Y");
			
		if($pp_date > $week)
			$ppyear = date("Y")-1;
		else
			$ppyear = date("Y");
			
	$income = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 0 AND (WEEK(paid_date,3) <= $week OR (WEEKDAY(paid_date) = 0 AND  WEEK(paid_date,3) = $curweek)) AND YEAR(paid_date) = $year AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' AND is_qr = 0"));
	
	

	$ttotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 0 AND WEEK(paid_date,3) < $week AND YEAR(paid_date) = $year AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0"));


	$postponed = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1  AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0")); //total postponed items


	$postponed_items = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1 AND WEEK(postpone_date,3) < $pp_date AND YEAR(postpone_date) = $ppyear AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0"));
	
	$postponed_items_left = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1 AND postpone_date >= '2012-03-21 00:00:00' AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0"));

	$check = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND  pid = '$arr[pid]' AND payment = 5 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0"));

	$szep = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND pid = '$arr[pid]' AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0"));


	$szepcurrent = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND pid = '$arr[pid]' AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND WEEK(paid_date,3) < $szepweek AND YEAR(paid_date) = $szepyear  AND is_qr = 0"));

	$place = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 0 AND WEEK(paid_date,3) = $week AND YEAR(paid_date) = $year AND pid = '$arr[pid]' AND payment = 6 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' AND is_qr = 0"));


	//standard
	//place
	//ucs
	//postponed
	
	$invoice_total = $income[total]+$postponed_items[total]+$check[total]+$szepcurrent[total];
	$yield_total = $invoice_total*($company[yield_vtl]/100)*$tax;
	
	$transfer = $invoice_total-$yield_total-$place[total];
	$totaltransfer = $totaltransfer + $transfer;
	
	$totalback = $totalback + $postponed[total];
	$totalbackleft = $totalbackleft + $postponed_items_left[total];
	$totalyield = $totalyield + $yield_total;
	
	$sztotal = $sztotal+$szep[total];
	
	if($company[tax_no] == '')
		$missingtax.= "<tr><td>$company[company_name] / $company[hotel_name]</td></tr>";
		
	if($company[without_vat] == 1)
		$cls = 'orange';
	else
		$cls = '';

	echo "<tr class='$cls'>";
		echo "<td align='center'>$i.</td>";
		echo "<td>$company[company_name] ($company[pid])</td>";
		echo "<td>$company[hotel_name]</td>";
		echo "<td>$company[tax_no]</td>";
		echo "<td align='right'>".formatPrice($income[total],0,1)."</td>";
		echo "<td align='right'>".formatPrice($postponed_items[total],0,1)."</td>";
		echo "<td align='right' class='grey'>".formatPrice($postponed_items_left[total],0,1)."</td>";
		echo "<td align='right' class='grey'>".formatPrice($postponed[total],0,1)."</td>";
		
		echo "<td align='right' class='grey'>".formatPrice($szep[total],0,1)."</td>";


		//echo "<td align='right' class='grey'>".formatPrice($szepcurrent[total],0,1)."</td>";


		///echo "<td align='right' class='grey'>".formatPrice($ttotal[total],0,1)."</td>";

		//echo "<td align='right'>".formatPrice($check[total],0,1)."</td>";
		echo "<td align='right'>".formatPrice($place[total],0,1)."</td>";
		echo "<td align='right'>".formatPrice($transfer,0,1)."</td>";
		echo "<td align='right' class='green'>".formatPrice($yield_total,0,1)."</td>";
		//echo "<td align='right' class='green'>$tax / ".formatPrice($invoice_total*($company[yield_vtl]/100),0,1)."</td>";
		//echo "<td align='right' class='green'>$arr[invoice_number]</td>";
	echo "</tr>";
	
	//echo "<tr ><td colspan='10'>SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1 AND WEEK(postpone_date,3) < $pp_date AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'</td></tr>";
	
	/*** generate data for the invoice **/
	$partner = array();
	$partner[adoszam] = $company[tax_no];
	$partner[email] = $company[email];
	$partner[megjegy] = $company[account_no];
	
	if($company[invoice_name] <> '')
		$partner[nev] = "$company[invoice_name]|$company[hotel_name]";
	else
		$partner[nev] = "$company[company_name]|$company[hotel_name]";

	if($company[invoice_zip] <> '')
	{
		$partner[irsz] = $company[invoice_zip]; 
		$partner[varos] = $company[invoice_city];
		$partner[utca] = $company[invoice_address];
	}
	else
	{
		$partner[irsz] = $company[zip]; 
		$partner[varos] = $company[city];
		$partner[utca] = $company[address];
	}
	
	$items = array();
	
		
	//weekly not postponed items
	$income = $mysql->query("SELECT offer_id, name, orig_price FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 0 AND  (WEEK(paid_date,3) <= $week OR (WEEKDAY(paid_date) = 0 AND  WEEK(paid_date,3) = $curweek))  AND YEAR(paid_date) = $year AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0 ORDER BY name ASC");
	while($singleitem = mysql_fetch_assoc($income))
	{
		$item[megnev] = "$singleitem[offer_id] / $singleitem[name] - ".formatPrice($singleitem[orig_price])." után járó jutalék";
		$item[netto_egysegar] = $singleitem[orig_price]*($company[yield_vtl]/100);
		$items[] = $item;
	}
	
	//check query
	$income = $mysql->query("SELECT offer_id, name, orig_price FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND  pid = '$arr[pid]' AND payment = 5 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0 ORDER BY name ASC");
	while($singleitem = mysql_fetch_assoc($income))
	{
		$item[megnev] = "$singleitem[offer_id] / $singleitem[name] - ".formatPrice($singleitem[orig_price])." után járó jutalék";
		$item[netto_egysegar] = $singleitem[orig_price]*($company[yield_vtl]/100);
		$items[] = $item;
	}

	
	//SZEP query
	$income = $mysql->query("SELECT offer_id, name, orig_price FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND pid = '$arr[pid]' AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0 AND WEEK(paid_date,3) < $szepweek AND YEAR(paid_date) = $szepyear  ORDER BY name ASC");
	while($singleitem = mysql_fetch_assoc($income))
	{
		$item[megnev] = "$singleitem[offer_id] / $singleitem[name] - ".formatPrice($singleitem[orig_price])." után járó jutalék";
		$item[netto_egysegar] = $singleitem[orig_price]*($company[yield_vtl]/100);
		$items[] = $item;
	}
	
	//postponed items
	$income = $mysql->query("SELECT offer_id, name, orig_price FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1 AND WEEK(postpone_date,3) < $pp_date  AND YEAR(postpone_date) = $ppyear AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12  AND is_qr = 0 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name' ORDER BY name ASC");
	while($singleitem = mysql_fetch_assoc($income))
	{
		$item[megnev] = "$singleitem[offer_id] / $singleitem[name] - ".formatPrice($singleitem[orig_price])." után járó jutalék";
		$item[netto_egysegar] = $singleitem[orig_price]*($company[yield_vtl]/100);
		$items[] = $item;
	}


	//$place = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total FROM customers WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 0 AND WEEK(paid_date,3) = $week AND YEAR(paid_date) = $year AND pid = '$arr[pid]' AND payment = 6 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'"));

	//if there is something to create invoice of
	if($yield_total > 0 && $company[tax_no] <> '' && $company[without_vat] == 0) // && $company[pid] == 626
	{

	echo "<hr/>$i $company[company_name] $company[email] $company_name";
	
	
	$message = "Az eheti elszámolás a számlaszolgáltatónk technikai problémája miatt sajnos még nem készült el! A hiba elhárításán jelen pillanatban is dolgoznak,
s amint lehetséges, elkészítjük az esedékes jutalékszámlákat.<br/><br/>

A hiba természetesen az utalásokat nem érinti!<br/><br/>

Köszönjük megértésüket!<br/><br/>

Tisztelettel:<br/><br/>

Forró Tamás<br/>
Hotel Outlet Kft.
";
//	sendEmail("Szállás Outlet elszámolás",$message,$company[email],"Partnerünk");



/*
	$invoice_number = create_invoice($partner,$items,0,$company_name);
	
	//update created invoices
	echo "<hr/>$invoice_number<hr/>";
	
	if($invoice_number <> '') 
	{
	
	$mysql->query("UPDATE customers SET invoice_created = 1,invoice_date = NOW(), invoice_number = '$invoice_number'  WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 0  AND (WEEK(paid_date,3) <= $week OR (WEEKDAY(paid_date) = 0 AND  WEEK(paid_date,3) = $curweek))  AND YEAR(paid_date) = $year AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0");
	$mysql->query("UPDATE customers SET invoice_created = 1,invoice_date = NOW(), invoice_number = '$invoice_number'  WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND  pid = '$arr[pid]' AND payment = 5 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0");
	$mysql->query("UPDATE customers SET invoice_created = 1,invoice_date = NOW(), invoice_number = '$invoice_number'  WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1 AND WEEK(postpone_date,3) < $pp_date  AND YEAR(postpone_date) = $ppyear AND pid = '$arr[pid]' AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND is_qr = 0");

	$mysql->query("UPDATE customers SET invoice_created = 1,invoice_date = NOW(), invoice_number = '$invoice_number'  WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND facebook = 0 AND company_invoice = '$company_name'  AND WEEK(paid_date,3) < $szepweek AND YEAR(paid_date) = $szepyear  AND pid = '$arr[pid]'  AND is_qr = 0");
	}
echo "<hr/><hr/>";

	/*** generate data for the invoice **/
	
	//die;
	}
	$i++;
	
	}
}


	echo "<tr class='header'>";
		echo "<td colspan='4'>Összesen</td>";
		echo "<td align='right'>".formatPrice('',0,1)."</td>";
		echo "<td align='right' class='grey'>".formatPrice('',0,1)."</td>";
		echo "<td align='right'>".formatPrice($totalbackleft,0,1)."</td>";
		echo "<td align='right'>".formatPrice($totalback,0,1)."</td>";
		echo "<td align='right'>".formatPrice($sztotal,0,1)."</td>";
		echo "<td align='right'>".formatPrice('',0,1)."</td>";
		echo "<td align='right'>".formatPrice($totaltransfer,0,1)."</td>";
		echo "<td align='right'>".formatPrice($totalyield,0,1)."</td>";
	echo "</tr>";
	
	
	
	
echo "</table>";

echo "<br/><br/>";
echo "<h2>Hiányzó adószám</h2>";
echo "<table>$missingtax</table>";
	foot();
	
	unlink($lockfile);
?>