<?
/*
 * invoices.php 
 *
 * the invoice list page
 *
*/

/* bootstrap file */
include("inc/init.inc.php");


userlogin();

if($CURUSER[userclass] < 255 && ($CURUSER[username] != 'orsolya' &&  $CURUSER[username] <> 'berki.janos' &&  $CURUSER[username] <> 'konyveles' &&  $CURUSER[username] <> 'paranyipetra' &&  $CURUSER[username] <> 'mraz.gergo'))
	header("location: index.php");
	
if($_GET[company] == 'szallasoutlet')
{
	$companies = array( 'szallasoutlet' => "SzállásOutlet Kft. által kiállított számlák - <a href='?download=1&company=szallasoutlet'>[Utalási lista letöltése]</a>");
	$cname = 'SzallasOutlet Kft';
	$cname_orig = 'SzállásOutlet Kft.';
	$account = "116000060000000051899097";
	$taxno = "23686355";
	$fname = "szo-";
}
elseif($_GET[company] == 'professional')
{
	$companies = array( 'professional' => "Professional Outlet Kft. által kiállított számlák - <a href='?download=1&company=professional'>[Utalási lista letöltése]</a>");
	$cname = 'Professional Outlet Kft';
	$cname_orig = 'Professional Outlet Kft.';
	$account = "116000060000000076663561";
	$taxno = "25520103";
	$fname = "pr-";
}
elseif($_GET[company] == 'indusz')
{
	$companies = array( 'indusz' => "Indulhatunk utazásszervező Kft. által kiállított számlák - <a href='?download=1&company=indusz'>[Utalási lista letöltése]</a>");
	$cname = 'Indulhatunk Uszerv Kft';
	$cname_orig = 'Indulhatunk Utazásszervező Kft.';
	$account = "116000060000000065875957";
	$taxno = "24856850";
	$fname = "in-";
}
else
{
	$companies = array('hoteloutlet' => "Hotel Outlet Kft. által kiállított számlák - <a href='?download=1'>[Utalási lista letöltése]</a>",);
	$cname = 'Hotel Outlet Kft';
	$cname_orig = 'Hotel Outlet Kft.';
	$account = "116000060000000049170520";
	$taxno = "23417555";
	$fname = "ho-";

}



if($_GET[transfer_sent] <> '' && $_GET[sure] <> 1)
{
	echo "<h2>Biztosan elutalta a <b><font color='red'>$_GET[transfer_sent] / $_GET[company]</font></b> tételeket?</h1><br/>  <a href=\"?transfer_sent=1&sure=1&company=$_GET[company]\" class='button green'>$lang[yes]</a> <a href=\"weekly-transfer.php\" class='button red'>$lang[no]</a><div class='cleaner'></div>";
	die;
//	$mysql->query("UPDATE customers SET transfer_sent = '2012-01-12 10:00:00' WHERE invoice_number  = '$_GET[transfer_sent]'");
	
}
if($_GET[transfer_clear] <> '' && $_GET[sure] <> 1)
{
	echo "<h2>Biztosan elutalta a <b><font color='red'>$_GET[transfer_clear]</font></b> tételeket?</h1><br/>  <a href=\"?transfer_clear=$_GET[transfer_clear]&sure=1&company=$_GET[company]\" class='button green'>$lang[yes]</a> <a href=\"weekly-transfer.php\" class='button red'>$lang[no]</a><div class='cleaner'></div>";
	die;
//	$mysql->query("UPDATE customers SET transfer_sent = '2012-01-12 10:00:00' WHERE invoice_number  = '$_GET[transfer_sent]'");
	
}

if($_GET[transfer] == 0 || $_GET[transfer] == '')
{
	$extraselect = "AND transfer_sent = '0000-00-00 00:00:00' AND invoice_cleared <> '0000-00-00 00:00:00'";
}
elseif($_GET[transfer] == 1)
{
	$extraselect = "AND transfer_sent = '0000-00-00 00:00:00' AND invoice_cleared = '0000-00-00 00:00:00'";
}
elseif($_GET[transfer] == 2)
{
	$extraselect = "AND transfer_sent <> '0000-00-00 00:00:00' AND invoice_cleared <> '0000-00-00 00:00:00'";
}
elseif($_GET[transfer] == 4)
{
	$extraselect = "AND cid = 0";
}

if($_GET[cmp] > 0)
	$clr = "AND customers.pid = $_GET[cmp]";
elseif($_GET[hotel] > 0)
	$clr = "AND customers.pid = $_GET[hotel]";
	
	
$pid = ' cid > 0';

/***transfer header, foter *////

	$date = date("Ymd");
	
	$code = "PEA";
	$from_company = str_pad("$cname", 35, " ", STR_PAD_RIGHT); 
	$comment_main = str_pad("$week heti elszamolas", 70, " ", STR_PAD_RIGHT); 
	$head.= "01ATUTAL0A$taxno    ".$date."0002".$account."".$date."".$code."".$from_company."".$comment_main."\r\n";
	
	
/*******/

//$content.=$pid;


mysql_query("SET NAMES 'utf8'"); 

$total_income = 0;
$total_yield = 0;
$total_must_pay = 0;







foreach($companies as $company => $cname)
{

	if($company == 'indusz')
		$eselect = "AND invoice_number NOT LIKE '%JT-%'";
	if($company == 'szallasoutlet')
		$eselect2 = "OR (invoice_number LIKE '%JT-%' AND company_invoice = 'indusz')";
		
	$statQuery = $mysql->query("SELECT * FROM customers
	 WHERE invoice_created = 1 AND (company_invoice = '$company' $eselect2) AND $pid $extraselect $eselect $clr AND pid <> 3003 AND pid <> 3121 AND invoice_number <> '' 
	 AND customers.pid <> 3003 AND customers.pid <> 3121 AND customers.pid <> 3452 AND customers.pid <> 3589 AND customers.pid <> 3915 
	 GROUP BY invoice_number ORDER BY pid, invoice_date DESC");



$query = $mysql->query("SELECT * FROM customers WHERE company_invoice = 'indusz' and indusz_szo_account = 0 and paid = 1 and inactive = 0 and (payment = 10 or payment = 11 or payment = 12) AND invoice_number <> '' ORDER BY paid_date asc");
$i = 1;
while($arr = mysql_fetch_assoc($query))
{
		$partner = mysql_fetch_assoc($mysql->query("SELECT * FROM partners WHERE pid = $arr[pid] LIMIT 1"));

	if($partner[without_vat] == 0)
	{
		
	//	echo "UPDATE customers SET indusz_szo_account = 2, indusz_szo_date = NOW() WHERE cid = '$arr[cid]';<br/>";	
	$total = $total + $arr[orig_price];
	$i++;
	}
}
$sql = "SELECT sum(orig_price) as total,indusz_szo_account FROM customers WHERE company_invoice = 'indusz' AND indusz_szo_account > 0 GROUP by indusz_szo_account ORDER BY indusz_szo_account desc";
print $sql;
$query = $mysql->query($sql);
$i = 1;
while($arr = mysql_fetch_assoc($query))
{


	$erows.="<tr>
	<td>$arr[indusz_szo_account]</td>
	<td align='right'>".formatPrice($arr[total])."</td>
	<td align='center'>4%+Áfa</td>
	<td align='right'>".formatPrice($arr[total]*0.04*1.27)."</td>
	<td align='right'>".formatPrice($arr[total]-$arr[total]*0.04*1.27)."</td>
	</tr>";

	$i++;
}

if($CURUSER[userclass] == 255)
{

$content.="<table>
<tr class='header'>
	<td colspan='6'>Indulhatunk utszerv - Szo átvezetés</td>
</tr>
<tr class='header'>
	<td width='20'>ID</td>
	<td>Bevétel</td>
	<td>Jutalék%</td>
	<td>Jutalék</td>
	<td>Utalandó</td>
</tr>
<tr>
	<td></td>
	<td align='right'>".formatPrice($total)."</td>
	<td align='center'>4%+Áfa</td>
	<td align='right'>".formatPrice($total*0.04*1.27)."</td>
	<td align='right'>".formatPrice($total-$total*0.04*1.27)."</td>
</tr>
$erows
</table>";

$content.="<hr/>";

}


$content.="<table>";
$content.="<tr class='header'>";
	$content.="<td colspan='2'>Cég</td>";
	$content.="<td width='35'>Hét</td>";
	$content.="<td>Számla dátum (elszámolás elkészülése)</td>";
	$content.="<td>Számlaszám</td>";
	//$content.="<td>Bankszámlaszám</td>";
	$content.="<td>Partner neve</td>";
	$content.="<td>Bevétel</td>";
	$content.="<td>Helyszínen</td>";

	$content.="<td>Jutalék</td>";
	$content.="<td>ÁFA</td>";
	//$content.="<td>Eltérés</td>";
	$content.="<td>Utalandó</td>";
	$content.="<td>Elszámolás</td>";
	//$content.="<td>Jóváhagyva</td>";
	$content.="<td>Utalás dátuma</td>";
	$content.="<td>Jóváhagyás dátuma (számla beérkezett)</td>";
	$content.="<td>Utalva</td>";

$content.="</tr>";
//$content.=$dateArr[invoice_date];

	 
	 $z = 1;
	 $h = 1;
while($arr = mysql_fetch_assoc($statQuery)) {
		$wk = date("W",strtotime($arr[invoice_date]))-1;
		$year = date("Y",strtotime($arr[invoice_date]));
		
		
		if($wk == 0)
			$wk = 52;
if($year == 2011)
	$tax = 1.25;
elseif($year >= 2012)
	$tax = 1.27;
else
	$tax = 1.25;
	
	
	
		$partnerQuery = $mysql->query("SELECT * FROM partners WHERE pid = '$arr[pid]'");
		$partnerArr = mysql_fetch_assoc($partnerQuery);
		
		$sumQuery = $mysql->query("SELECT sum(orig_price) AS sumPrice FROM customers WHERE invoice_number = '$arr[invoice_number]' AND payment <> 5 AND payment <> 6 AND inactive = 0 AND pid = '$arr[pid]'");
		$sumArr = mysql_fetch_assoc($sumQuery);
	
		$sumTotal = $mysql->query("SELECT sum(orig_price) AS sumPrice FROM customers WHERE invoice_number = '$arr[invoice_number]' AND inactive = 0 AND pid = '$arr[pid]'");
		$sumTotalArr = mysql_fetch_assoc($sumTotal);
		
		$placeTotal = $mysql->query("SELECT sum(orig_price) AS sumPrice FROM customers WHERE invoice_number = '$arr[invoice_number]' AND payment = 6 AND inactive = 0 AND pid = '$arr[pid]'");
		$placeTotal = mysql_fetch_assoc($placeTotal);
		
		$checkqr = mysql_fetch_assoc($mysql->query("SELECT is_qr FROM customers WHERE invoice_number = '$arr[invoice_number]' AND pid = '$arr[pid]' LIMIT 1"));
		
		if($checkqr[is_qr] == 1)
		{
			$partnerArr[yield_vtl] = $partnerArr[yield_qr];
		}else
			$partnerArr[yield_vtl] = $partnerArr[yield_vtl];
		
		if($partnerArr[unique_partner] == 1 || $partnerArr[has_foreign] == 1)
		{
			continue;
			//break;
				//echo "hidden";
			
		}
		$realIncome = $sumTotalArr[sumPrice]; 
		if(str_replace("-","",$arr[invoice_date]) > 20110401)
	 	{
	 		$royalty = number_format($sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*$tax, 0, ',', ' '). " Ft";
	 		
	 		$roy = $sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*$tax;
	 		
	 		$cmp = $sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*0.02;
	 			
			$income = $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*$tax;
			
		
		}
		else
		{
			$royalty = number_format($sumTotalArr[sumPrice]*($partnerArr['yield']/100)*$tax, 0, ',', ' '). " Ft";
			$income = $sumArr[sumPrice] - $sumTotalArr[sumPrice]*($partnerArr['yield']/100)*$tax;

		}
		//$income = number_format($income, 0, ',', ' '). " Ft";
		
		
	$total_income = $total_income + $sumTotalArr[sumPrice] - $placeTotal[sumPrice];
	
	$placetot = $placetot + $placeTotal[sumPrice];


	$total_yield = $total_yield + $sumTotalArr[sumPrice]*($partnerArr[yield_vtl]/100)*$tax;
	//$total_must_pay = $total_must_pay + $sumArr[sumPrice] - ($sumTotalArr[sumPrice]*($partnerArr['yield']/100))*$tax;
	
	

	if($arr[invoice_date] > '2011-04-05')
		{
			$letter = "<b><a href='info/show_accounting.php?invoice_number=$arr[invoice_number]&pid=$arr[pid]' rel='facebox'>Elszámolás megtekintése</a></b>";
			if($arr[invoice_cleared] <> '0000-00-00 00:00:00')
			{
				$invoice = '<img src="/images/check1.png" width="25"/>';
				
				if($arr[transfer_sent] <> '0000-00-00 00:00:00')
				{
					$class='grey';
				}
				else
				{
					$class = '';
				}
			
			}
			else
			{
				$invoice = '<img src="/images/cross1.png" width="25" alt="Kérjük a számlát a lehető leghamarabb juttassa el hozzánk!" title="Kérjük a számlát a lehető leghamarabb juttassa el hozzánk!"/>';
				$class = 'red';
			}
		}
		else
		{
			$letter = '';
			$invoice = '';
		}
	
	
	if($_GET[transfer] == 1 && $_GET[showpostpone] == 1)
	{
		if($prevpid <> $arr[pid])
		{
			$pponedtotal = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) AS totalsum FROM customers WHERE invoice_created = 0 AND postpone = 1 AND pid = $arr[pid] AND inactive = 0"));		
			$content.="<tr><td colspan='12'>$partnerArr[hotel_name]</td><td>".formatPrice($pponedtotal[totalsum])."</td></tr>";
			$pponed = $pponed + $pponedtotal[totalsum];
		}
	}
	
	$prevpid = $arr[pid];
	
		
	$content.="<tr class='$class'>";
	
		if($arr[company_invoice] == 'indulhatunk')
			$logo = 'ilogo_small.png';
		elseif($arr[company_invoice] == 'szallasoutlet')
			$logo = 'szo_logo.png';
		else
			$logo = 'ologo_small.png';
			
		$content.="<td align='center'>$z.</td>";
		$content.="<td align='center'><img src='/images/$logo'/></td>";
		$content.="<td align='center'>".$wk.". hét</td>";
		$content.="<td align='center'>$arr[invoice_date]</td>";

		$content.="<td><a href=' https://admin.indulhatunk.hu/invoices/dl/".md5($arr[invoice_number]."#FWf#CEFA#SDF343sf#")."'>$arr[invoice_number]</a></td>";
		$content.="<td>$partnerArr[company_name] $partnerArr[hotel_name]</td>";
		$content.="<td align='right'>".formatPrice($realIncome)."</td>";
		$content.="<td align='right'>".formatPrice($placeTotal[sumPrice])."</td>";
		$content.="<td align='right'>$royalty</td>";
		$content.="<td align='right'>$tax</td>";
		
	//	$content.="<td align='right'>$compensation</td>";
		$content.="<td align='right'>".formatPrice($realIncome-$roy-$placeTotal[sumPrice])."</td>";
		
		
		$content.="<td>$letter</td>";
		
		//$content.="<td align='center'>$invoice</td>";
		
		
		if($arr[transfer_sent] == '0000-00-00 00:00:00')
		{
		
			
			
			$transfer_date = '-';
			if($arr[invoice_cleared] <> '0000-00-00 00:00:00')
			{
			
			$partnerArr[company_name] = trim(str_replace("Indulhatunk promóció április-május",'',$partnerArr[company_name]).$partnerArr[hotel_name]);
		//	$content.="$partnerArr[company_name]<hr/>"; 
	
			$totalinv = str_pad(round($realIncome-$roy-$placeTotal[sumPrice]), 10, "0", STR_PAD_LEFT);
			$inner_id = str_pad($h, 6, "0", STR_PAD_LEFT); 
			$account_no = str_replace("-",'',trim($partnerArr[account_no]));
			$partner_id = str_pad(substr(trim($partnerArr[coredb_id]),0,22), 24, " ", STR_PAD_RIGHT); 
			$partner_name = str_pad(substr(removeSpecialChars(trim($partnerArr[company_name])),0,32), 35, " ", STR_PAD_RIGHT); 
			$partner_address = str_pad(substr(trim(removeSpecialChars($partnerArr[address])),0,32), 35, " ", STR_PAD_RIGHT); 
			$cmnt = str_pad(substr("$week / heti elszamolas ($arr[invoice_number])",0,65), 70, " ", STR_PAD_RIGHT); 

			if(round($realIncome-$roy-$placeTotal[sumPrice]) > 0)
			{
				$sum = $sum+round($realIncome-$roy-$placeTotal[sumPrice]);
				$transactions.="02".$inner_id."00000000".$totalinv."".$account_no."".$partner_id."".$partner_name."".$partner_address."".$partner_name."".$cmnt."\r\n";
			}
			
			$h++;
			}
		}
		else
		{
			$transfer_date = $arr[transfer_sent];
		}
		
		$tdate = explode(" ",$arr[invoice_cleared]);
		if($tdate[0] == '2014-02-20')
			$ccc = 'red';
		else
			$ccc = '';
		
		$content.="<td align='center'>$transfer_date</td>";
		
		$content.="<td align='center' class='$ccc'>$arr[invoice_cleared]</td>";
		
		$content.="<td align='center'><a href='?transfer_clear=$arr[invoice_number]&company=$_GET[company]' rel='facebox'><b>Elutalva</b></td>";


		//$content.="<td>$income</td>"; // https://admin.indulhatunk.hu/invoices/dl/".md5($arr[invoice_number]."#FWf#CEFA#SDF343sf#")."
	$content.="</tr>";
	
	if($_GET[transfer_clear] == $arr[invoice_number] && $_GET[sure] == 1)
	{
		$invdate = explode(" ",$arr[invoice_cleared]);
		$invdate = $invdate[0];
		$invdate = str_replace("-",'',$invdate);
	
		//if($invdate < 20120128)
		//{
		
	//	if($arr[invoice_number] <> 'HO-M2012/000170' && $arr[invoice_number] <> 'HO-M2012/000183')
			$mysql->query("UPDATE customers SET transfer_sent = NOW() WHERE invoice_number  = '$arr[invoice_number]'");
			writelog("$CURUSER[username] SET manually transfer sent ON invoice $arr[invoice_number]");
			//	$mysql->query("UPDATE customers SET transfer_sent = '2012-02-02 13:30:00' WHERE invoice_number  = '$arr[invoice_number]'");
		//}
	
	}
	
	
	if($_GET[transfer_sent] <> '' && $_GET[sure] == 1)
	{
		$invdate = explode(" ",$arr[invoice_cleared]);
		$invdate = $invdate[0];
		$invdate = str_replace("-",'',$invdate);
	
		//if($invdate < 20120128)
		//{
		
	//	if($arr[invoice_number] <> 'HO-M2012/000170' && $arr[invoice_number] <> 'HO-M2012/000183')
			$mysql->query("UPDATE customers SET transfer_sent = NOW() WHERE invoice_number  = '$arr[invoice_number]'");
			writelog("$CURUSER[username] SET manually transfer sent ON invoice $arr[invoice_number]");
			//	$mysql->query("UPDATE customers SET transfer_sent = '2012-02-02 13:30:00' WHERE invoice_number  = '$arr[invoice_number]'");
		//}
	
	}
		//$content.="<hr/>";
		
		$z++;
	}
	
$content.="<tr class='header'>";
	$content.="<td colspan='6'>Összesen</td>";
	$content.="<td>".formatPrice($total_income,0,1)."</td>";
		$content.="<td>-</td>";

	$content.="<td>".formatPrice($total_yield,0,1)."</td>";
			$content.="<td>-</td>";

	$content.="<td>".formatPrice($total_income-$total_yield,0,1)."</td>";
	$content.="<td colspan='4'></td>";
$content.="</tr>";

	if($_GET[transfer] == 1 && $_GET[showpostpone] == 1)
	{
	$content.="<tr class='header'>";
	$content.="<td colspan='5'>Eltolt összes</td>";
	$content.="<td>".formatPrice($pponed,0,1)."</td>";
$content.="</tr>";
	}


	

$content.="</table><br/><br/>";
//}

}


/*** transfer footer *////

	$totalrows = $h-1;
	$count = str_pad($totalrows, 6, "0", STR_PAD_LEFT); 
	$totaltransfer = str_pad($sum, 16, "0", STR_PAD_LEFT); 
	$foot= "03".$count."".$totaltransfer;

	$transfer =  $head;
	$transfer.= $transactions;
	$transfer.= $foot."\r\n";
/****/


if($_GET[download] <> 1)
{
	head("Heti $cname_orig utalások");
?>



<div class='content-box'>
<div class='content-box-header'>
					<ul class="content-box-tabs">
					
		<li><a href="?transfer=0&company=<?=$_GET[company]?>" class="<? if($_GET[transfer] == 0 || $_GET[transfer] == '') echo "current";?>">Utalandó</a></li>
		<li><a href="?transfer=1&company=<?=$_GET[company]?>"  class="<? if($_GET[transfer] == 1) echo "current";?>">Utalásra vár</a></li>

		<? if($CURUSER[userclass] == 255) { ?>
					<li><a href="?transfer=2&company=<?=$_GET[company]?>"  class="<? if($_GET[transfer] == 2) echo "current";?>">Utalt</a></li>
			<li><a href="?transfer=4&company=<?=$_GET[company]?>"  class="<? if($_GET[transfer] == 4 && $_GET[szep] == '') echo "current";?>">Eltolt</a></li>
			<li><a href="?transfer=4&szep=1&company=<?=$_GET[company]?>"  class="<? if($_GET[transfer] == 4 && $_GET[szep] == 1) echo "current";?>">SZÉP számlázatlan</a></li>
			<li><a href="?transfer=4&szep=2"  class="<? if($_GET[transfer] == 4 && $_GET[szep] == 2) echo "current";?>">ÜCS számlázatlan</a></li>
			<? } ?>
		<li><a href="?download=1&company=<?=$_GET[company]?>">Heti lista letöltése</a></li>
			<li><a href="?transfer_sent=1&company=<?=$_GET[company]?>" rel='facebox'>Elutalva</a></li>

		<div class="clear"></div>
</div>
<div class='contentpadding'>

<div class='buttons' style='margin:0 0 10px 0;'>
	<a href='?company=szallasoutlet' class='changecompany' company='szallasoutlet' style='display:block; width:25%; padding:20px 0 20px 0; text-align:center; <? if($_GET[company] == 'szallasoutlet') echo "background-color:#E56E94;color:white;"; else echo "background-color:#e7e7e7;color:black;" ?>float:left;' >SzállásOutlet Kft számlái</a>
	<a href='?company=' class='changecompany' company='hoteloutlet' style='display:block; width:25%;padding:20px 0 20px 0; text-align:center; <? if($_GET[company] == '') echo "background-color:#E56E94;color:white;"; else echo "background-color:#e7e7e7;color:black;" ?>float:left;'>Hotel Outlet Kft számlái</a>
	<a href='?company=indusz' class='changecompany' company='indusz' style='display:block; width:25%;padding:20px 0 20px 0; text-align:center; <? if($_GET[company] == 'indusz') echo "background-color:#E56E94;color:white;"; else echo "background-color:#e7e7e7;color:black;" ?>float:left;'>Indusz Kft számlái</a>
		<a href='?company=professional' class='changecompany' company='professional' style='display:block; width:25%;padding:20px 0 20px 0; text-align:center; <? if($_GET[company] == 'professional') echo "background-color:#E56E94;color:white;"; else echo "background-color:#e7e7e7;color:black;" ?>float:left;'>Professional Kft számlái</a>

	<div class='cleaner'></div>
</div>	

<form method='get'>
<input type='hidden' name='transfer' value='<?=$_GET[transfer]?>'/>
<input type='hidden' name='company' value='<?=$_GET[company]?>'/>

<select name='cmp' onchange='submit()' style='width:430px;'>
	<option value='0'>Cég neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(company_name) <> '' order by company_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[company_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[company_name]</option>";
		}
	?>
</select>
<select name='hotel' onchange='submit()'  style='width:430px;'>
	<option value='0'>Hotel neve</option>
	<? $partners = $mysql->query("SELECT * FROM partners WHERE userclass < 10 AND country = '' OR country = 'hu' AND trim(hotel_name) <> '' order by hotel_name ASC");
	
		while($arr = mysql_fetch_assoc($partners))
		{
			if($arr[hotel_name] <> '')
				echo "<option value='$arr[pid]' $selected>$arr[hotel_name]</option>";
		}
	?>
</select>
</form>
<hr/>

<?	
	
	
	if($_GET[transfer] == 4)
	{
		$total = 0;
		
//szep total
	$dt1 = $mysql->query("SELECT week(paid_date,3) as wk FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = '' AND foreign_invoice_number = '' AND invoice_created = 0 AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND partners.has_foreign = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 GROUP BY week(paid_date,3) ORDER BY week(paid_date,3) ASC ");

			$weektotalquery1 = "SELECT sum(orig_price) as total FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = ''  AND foreign_invoice_number = '' AND invoice_created = 0 AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND partners.has_foreign = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 AND week(paid_date,3) = #WEEK#";
//ucs total
		
				$dt2 = $mysql->query("SELECT week(paid_date) as wk FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = ''  AND foreign_invoice_number = '' AND invoice_created = 0 AND payment = 5 AND inactive = 0 AND facebook = 0 AND company_invoice = 'hoteloutlet' AND partners.has_foreign = 0 AND partners.unique_partner = 0 GROUP BY week(paid_date,3) ORDER BY week(paid_date,3) ASC ");

			$weektotalquery2 = "SELECT sum(orig_price) as total FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = ''  AND foreign_invoice_number = '' AND invoice_created = 0 AND payment = 5 AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' partners.has_foreign = 0 AND partners.unique_partner = 0 AND week(paid_date,3) = #WEEK# ";
			
//postponed total
	$dt3 = $mysql->query("SELECT week(postpone_date,3) as wk FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = ''  AND foreign_invoice_number = '' AND invoice_created = 0 AND postpone = 1  AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND  partners.has_foreign = 0 AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 GROUP BY week(postpone_date,3) ORDER BY  week(postpone_date,3) ASC ");

			$weektotalquery3 = "SELECT sum(orig_price) as total FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = '' AND foreign_invoice_number = '' AND invoice_created = 0 AND postpone = 1  AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND  partners.has_foreign = 0 AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 and week(postpone_date,3) = #WEEK# ";
			
//not szep, not ucs, not posponed total
	$dt4 = $mysql->query("SELECT week(paid_date,3) as wk FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = ''  AND foreign_invoice_number = ''AND invoice_created = 0 AND postpone = 0 AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND  partners.has_foreign = 0 AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 GROUP BY week(paid_date,3) ORDER BY  week(paid_date,3) ASC ");

			$weektotalquery4 = "SELECT sum(orig_price) as total FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = ''  AND foreign_invoice_number = '' AND invoice_created = 0 AND postpone = 0  AND payment <> 5 AND payment <> 10 AND payment <> 11 AND payment <> 12 AND  partners.has_foreign = 0 AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 and week(paid_date,3) = #WEEK# ";


		
		
		if($_GET[szep] == 1)
		{
			$query = $mysql->query("SELECT sum(orig_price) as total, partners.hotel_name, partners.company_name, partners.pid FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = ''  AND foreign_invoice_number = '' AND invoice_created = 0 AND (payment = 10 OR payment = 11 OR payment = 12) AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 AND partners.has_foreign = 0  GROUP BY customers.pid ORDER BY total DESC ");
				
				
		
		}
		elseif($_GET[szep] == 2)
		{
			$query = $mysql->query("SELECT sum(orig_price) as total, partners.hotel_name, partners.company_name FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0  AND foreign_invoice_number = ''  partners.has_foreign = 0 AND AND payment = 5 AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 GROUP BY customers.pid ORDER BY total DESC ");
			}
		else
		{
		$query = $mysql->query("SELECT sum(orig_price) as total, partners.hotel_name, partners.company_name FROM customers INNER JOIN partners ON partners.pid = customers.pid WHERE paid = 1 AND invoice_number = '' AND invoice_created = 0 AND postpone = 1  AND payment <> 5 AND payment <> 10   AND  partners.has_foreign = 0 AND foreign_invoice_number = '' AND payment <> 11 AND payment <> 12 AND inactive = 0 AND facebook = 0 AND company_invoice = '$_GET[company]' AND partners.unique_partner = 0 GROUP BY customers.pid ORDER BY total DESC ");
		
		
		}
		
		echo "<table style='width:190px;margin:0 10px 0 0;float:left;'>";
		echo "<tr><td colspan='2' class='header'>SZÉP Heti bontásban</td></tr>";
		$tt = 0;
		while($r = mysql_fetch_assoc($dt1))
		{
			$weeks[] = $r[wk];
			
			$weektotal = mysql_fetch_assoc($mysql->query(str_replace("#WEEK#",$r[wk],$weektotalquery1)));
			echo "<tr>";
				echo "<td class='lablerow' style='width:50px;'>$r[wk]. hét</td>";
				echo "<td align='right'>".formatprice($weektotal[total],0,1)."</td>";
			echo "</tr>";
			$tt = $tt+$weektotal[total];
		}
		echo "<tr><td class='lablerow'>Összesen</td><td align='right' class='lablerow'>".formatPrice($tt)."</td></tr>";
		echo "</table>";
		
		echo "<table style='width:190px;margin:0 10px 0 0;float:left;'>";
		echo "<tr><td colspan='2' class='header'>ÜCS Heti bontásban</td></tr>";
		$tt = 0;
		while($r = mysql_fetch_assoc($dt2))
		{
			$weeks[] = $r[wk];
			
			$weektotal = mysql_fetch_assoc($mysql->query(str_replace("#WEEK#",$r[wk],$weektotalquery2)));
			echo "<tr>";
				echo "<td class='lablerow' style='width:50px;'>$r[wk]. hét</td>";
				echo "<td align='right'>".formatprice($weektotal[total],0,1)."</td>";
			echo "</tr>";
			$tt = $tt+$weektotal[total];
		}
		echo "<tr><td class='lablerow'>Összesen</td><td align='right' class='lablerow'>".formatPrice($tt)."</td></tr>";
		echo "</table>";
		
		echo "<table style='width:190px;margin:0 10px 0 0;float:left;'>";
		echo "<tr><td colspan='2' class='header'>Eltolt Heti bontásban</td></tr>";
		$tt = 0;
		while($r = mysql_fetch_assoc($dt3))
		{
			$weeks[] = $r[wk];
			
			$weektotal = mysql_fetch_assoc($mysql->query(str_replace("#WEEK#",$r[wk],$weektotalquery3)));
			echo "<tr>";
				echo "<td class='lablerow' style='width:50px;'>$r[wk]. hét</td>";
				echo "<td align='right'>".formatprice($weektotal[total],0,1)."</td>";
			echo "</tr>";
			$tt = $tt+$weektotal[total];
		}
		echo "<tr><td class='lablerow'>Összesen</td><td align='right' class='lablerow'>".formatPrice($tt)."</td></tr>";
		echo "</table>";
		
			echo "<table style='width:190px;margin:0 10px 0 0;float:left;'>";
		echo "<tr><td colspan='2' class='header'>Hetenkénti heti bontásban</td></tr>";
		$tt = 0;
		while($r = mysql_fetch_assoc($dt4))
		{
			$weeks[] = $r[wk];
			
			$weektotal = mysql_fetch_assoc($mysql->query(str_replace("#WEEK#",$r[wk],$weektotalquery4)));
			echo "<tr>";
				echo "<td class='lablerow' style='width:50px;'>$r[wk]. hét</td>";
				echo "<td align='right'>".formatprice($weektotal[total],0,1)."</td>";
			echo "</tr>";
			$tt = $tt+$weektotal[total];
		}
		echo "<tr><td class='lablerow'>Összesen</td><td align='right' class='lablerow'>".formatPrice($tt)."</td></tr>";
		echo "</table>";


		echo "<div class='cleaner'></div><hr/>";
		
		echo "<table>";
			echo "<tr class='header'>";
				echo "<td>Cég neve</td>";
				echo "<td>Hotel neve</td>";
				echo "<td>Eltolt összeg</td>";
				echo "</tr>";



		while($arr = mysql_fetch_assoc($query))
		{
			echo "<tr>";
				echo "<td>$arr[company_name]</td>";
				echo "<td>$arr[hotel_name]</td>";
				echo "<td align='right'>".formatPrice($arr[total],0,1)."</td>";				
			echo "</tr>";
			$total = $total + $arr[total];
		}
		echo "<tr class='header'>";
				echo "<td colspan='2'>Összesen</td>";
				echo "<td align='right'>".formatPrice($total,0,1)."</td>";
			echo "</tr>";
		echo "</table><br/><br/>";

	}
	else{
		echo $content;
	}
	echo "</div>";
	foot();
}
else
{
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment;filename=atutal-$fname".date("Ymd-W").".121");
	header('Pragma: no-cache');
	header('Expires: 0');
	echo $transfer;
}
?>