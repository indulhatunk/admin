<?
/*
 * index.php 
 *
 * the main login page
 *
*/

/* bootstrap file */

$start = microtime(true);

ob_start();

include("inc/init.inc.php");
userlogin();

if($CURUSER[userclass] < 255)
	header("location: index.php");
	
head('Jutalék statisztikák');

	$year = (int)$_GET[year];
	if($year == '')
		$year = 2016;

?>
<div class='content-box'>
<div class='content-box-header'>
	<ul class="content-box-tabs">
		<li><a href="+" class="<? if($_GET[type] == 0) echo "current";?>">Jutalékszámla statisztika</a></li>

<!--
		<li><a href="?type=0" class="<? if($_GET[type] == 0) echo "current";?>">Fizetési statisztika</a></li>
		<li><a href="?type=1" class="<? if($_GET[type] == 1) echo "current";?>">Eladási statisztika</a></li>
		<li><a href="?type=4" class="<? if($_GET[type] == 4) echo "current";?>">Fizetési statisztika éves</a></li>
		<li><a href="?type=3" class="<? if($_GET[type] == 3) echo "current";?>">Eladási statisztika éves</a></li>
-->

	</ul>
	<div class="clear"></div>
</div>
<div class='contentpadding'>
	

<h3>Elszámolt tételek hónaponként</h3>

<table>
	
<?
	
	$companies = $mysql->query("SELECT * FROM companies WHERE has_outlet = 1 ORDER BY id ASC");
	

	
	echo "<tr class='header'>";
		echo "<td>$year</td>";
		for($i = 1; $i <= 12; $i++)
		{
			echo "<td colspan='7'>$i. hó</td>";	
		}
	echo "</tr>";
	
	
	echo "<tr class='header'>";
		echo "<td></td>";
		for($i = 1; $i <= 12; $i++)
		{
			echo "<td>Utalv össz.</td>";	
			echo "<td>Ebből QR.</td>";	
			echo "<td>Bruttó jut.</td>";
			echo "<td>SZEP<br/>(b. 1.2%).</td>";
			echo "<td>Kartya<br/>(b. 1.58%)</td>";		
			echo "<td>Kedv.</td>";	
			echo "<td>Nem F. QR</td>";		
		}
	echo "</tr>";

	while($c = mysql_fetch_assoc($companies))
	{
		echo "<tr>";
		echo "<td class='header'>$c[company_name]</td>";
		
		for($i = 1; $i <= 12; $i++)
		{
			
			if($c[company_name] == 'szallasoutlet')
				$criteria = "AND invoice_number like '%JT-%'";
			elseif($c[company_name] == 'indusz')
				$criteria = "AND invoice_number like '%IN2%'";
			else
				$criteria = "AND company_invoice = '$c[company_name]'";
			
			$arr = $mysql->query("SELECT year(invoice_date) as y, month(invoice_date) as m, sum(orig_price) as total, sum(discount_value) as discount_value, pid, invoice_number, is_qr FROM customers WHERE year(invoice_date) = $year AND month(invoice_date) = $i $criteria GROUP BY invoice_number");
			
			$ptotal = 0;
			$ytotal = 0;
			$discount = 0;
			while($req = mysql_fetch_assoc($arr))
			{
				$partner = mysql_fetch_assoc($mysql->query("SELECT yield_vtl, yield_qr, hotel_name FROM partners WHERE pid = $req[pid]"));
					
				if($req[is_qr] == 1)
					$ytotal = $ytotal + $req[total] * ($partner[yield_qr] / 100 * 1.27);
				else
					$ytotal = $ytotal + $req[total] * ($partner[yield_vtl] / 100 * 1.27);
				
				//echo "$req[invoice_number]<hr/>";
				$ptotal = $ptotal + $req[total];
				$discount = $discount + $req[discount_value];
			}
			$qr = mysql_fetch_assoc($mysql->query("SELECT year(invoice_date) as y, month(invoice_date) as m, sum(orig_price) as total FROM customers WHERE year(invoice_date) = $year AND month(invoice_date) = $i $criteria AND is_qr = 1 GROUP BY year(invoice_date), month(invoice_date)"));

			$szep = mysql_fetch_assoc($mysql->query("SELECT year(invoice_date) as y, month(invoice_date) as m, sum(orig_price) as total FROM customers WHERE year(invoice_date) = $year AND month(invoice_date) = $i $criteria and paid = 1 and payment > 9 GROUP BY year(invoice_date), month(invoice_date)"));

			$credit = mysql_fetch_assoc($mysql->query("SELECT year(invoice_date) as y, month(invoice_date) as m, sum(orig_price) as total FROM customers WHERE year(invoice_date) = $year AND month(invoice_date) = $i $criteria AND paid = 1 and payment = 9 GROUP BY year(invoice_date), month(invoice_date)"));

			$expired = mysql_fetch_assoc($mysql->query("SELECT count(cid) as cnt, SUM( orig_price ) as total
FROM customers INNER JOIN offers ON offers.id = customers.offers_id
WHERE (offers.is_qr =1 OR customers.pid =3466) AND paid = 1 AND inactive = 0 AND month(offers.expiration_date) = $i AND year(offers.expiration_date) = $year AND customer_left = '0000-00-00 00:00:00' AND customers.company_invoice = '$c[company_name]'"));

	
			$price = $ptotal == 0 ? "" : round($ptotal);
			$qprice = $qr[total] == 0 ? "" : round($qr[total]);
			$yield = $ytotal == 0 ? "" : round($ytotal);
			$discount = $discount == 0 ? "" : round($discount);
			
			$szep = $szep[total] == 0 ? "" : round($szep[total]*1.2/100);
			$credit = $credit[total] == 0 ? "" : round($credit[total]*1.58/100);
			$expired = $expired[total] == 0 ? "" : round($expired[total]);

			echo "<td align='right'>$price</td>";
			echo "<td align='right'>$qprice</td>";
			echo "<td align='right'>$yield</td>";
			echo "<td align='right' class='green'>$szep</td>";
			echo "<td align='right' class='green'>$credit</td>";
			echo "<td align='right' class='green'>$discount</td>";
			echo "<td align='right' class='yellow'>$expired</td>";	
	
		}

		echo "</tr>";
	}
	
	//bankkartya, szep kartya jutalek
	
?>
</table>

<table>
	
<?
	
	$companies = $mysql->query("SELECT * FROM companies WHERE has_outlet = 1 ORDER BY id ASC");
	
	$year = (int)$_GET[year];
	if($year == '')
		$year = 2016;
	
	

	
	

	
	echo "</table><hr/><h3>Fizetett QR / NEM qr kimutatas</h3><table>";
	
		echo "<tr class='header'>";
		echo "<td>$year</td>";
		for($i = 1; $i <= 12; $i++)
		{
			echo "<td colspan='4'>$i. hó</td>";	
		}
	echo "</tr>";
	
	echo "<tr class='header'>";
	echo "<td></td>";
	for($i = 1; $i <= 12; $i++)
	{
		echo "<td>QR db</td>";	
		echo "<td>QR</td>";	
		echo "<td>NEM QR db</td>";	
		echo "<td>NEM QR</td>";	
	}
	echo "</tr>";
	
	while($c = mysql_fetch_assoc($companies))
	{
		echo "<tr>";
		echo "<td class='header'>$c[company_name]</td>";
		
		for($i = 1; $i <= 12; $i++)
		{
			
			$qrt = '';
			$nqr = '';
			$qr = '';
			$notqr = '';	
			
			$qr = mysql_fetch_assoc($mysql->query("SELECT year(paid_date) as y, month(paid_date) as m, sum(orig_price) as total, count(cid) as cnt FROM customers WHERE year(paid_date) = $year AND month(paid_date) = $i AND inactive = 0 AND company_invoice = '$c[company_name]' AND is_qr = 1 GROUP BY year(paid_date), month(paid_date)"));

			$notqr = mysql_fetch_assoc($mysql->query("SELECT year(paid_date) as y, month(paid_date) as m, sum(orig_price) as total, count(cid) as cnt FROM customers WHERE year(paid_date) = $year AND month(paid_date) = $i AND inactive = 0 AND company_invoice = '$c[company_name]' AND is_qr = 0 GROUP BY year(paid_date), month(paid_date)"));

			
	
			$qrt = $qr[total] == 0 ? "" : round($qr[total]);
			$nqr = $notqr[total] == 0 ? "" : round($notqr[total]);

			echo "<td align='right'>$qr[cnt]</td>";
			echo "<td align='right'>$qrt</td>";
			echo "<td align='right' class='green'>$notqr[cnt]</td>";
			echo "<td align='right' class='green'>$nqr</td>";
			
	
		}

		echo "</tr>";
	}
	
	//bankkartya, szep kartya jutalek
	
?>
</table>


<hr/>
<h3>Áfamentes partner kimutatás</h3>
<table>
	
	<?
	
	
	
	$companies = $mysql->query("SELECT * FROM partners WHERE without_vat = '1'");
	echo "<tr class='header'>";
			echo "<td>Hotel</td>";
			echo "<td>QR</td>";
			echo "<td>NEM QR</td>";
			
		echo "</tr>";
	
	while($c = mysql_fetch_assoc($companies))
	{
		
			$qr = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total, count(cid) as cnt FROM customers WHERE year(paid_date) = $year AND inactive = 0 AND is_qr = 1 and pid = $c[pid] GROUP BY year(paid_date) "));

			$notqr = mysql_fetch_assoc($mysql->query("SELECT sum(orig_price) as total, count(cid) as cnt FROM customers WHERE year(paid_date) = $year AND inactive = 0 AND is_qr = 0 AND pid = $c[pid] GROUP BY year(paid_date) "));


		echo "<tr>";
			echo "<td>$c[hotel_name]</td>";
			echo "<td align='right'>$qr[total]</td>";
			echo "<td align='right'>$notqr[total]</td>";
			
		echo "</tr>";
		$qrtotal = $qrtotal + $qr[total];
		$notqrtotal = $notqrtotal + $notqr[total];
	}
	
	echo "<tr>";
			echo "<td>Total</td>";
			echo "<td align='right'>$qrtotal</td>";
			echo "<td align='right'>$notqrtotal</td>";
			
		echo "</tr>";
	//bankkartya, szep kartya jutalek
	
?>
</table>

</div>

</div>
<?


foot();
print microtime(true) - $start;
?>